package net.qpaysolutions.QPay.Deal;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 3/2/17.
 */

public class ReviewViewHolder extends RecyclerView.ViewHolder {
ImageView review_image;
    TextView name_review,date_review,description_review,rate_review;

    public ReviewViewHolder(View itemView) {
        super(itemView);
        review_image=(ImageView)itemView.findViewById(R.id.review_image);
        name_review=(TextView)itemView.findViewById(R.id.name_review);
        date_review=(TextView)itemView.findViewById(R.id.date_review);
        description_review=(TextView)itemView.findViewById(R.id.description_review);
        rate_review=(TextView)itemView.findViewById(R.id.rate_review);
    }
}
