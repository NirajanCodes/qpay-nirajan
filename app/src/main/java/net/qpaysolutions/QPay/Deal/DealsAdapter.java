package net.qpaysolutions.QPay.Deal;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Card.ebanking.EbankModel;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by QPay on 6/12/2018.
 */

public class DealsAdapter extends RecyclerView.Adapter<DealsAdapter.BankCustomViewHolder> {
    private Context context;
    private ArrayList<EbankModel> bankArrayList;
    private OnBankItemClick onBankItemClick;

    public interface OnBankItemClick {
        void onItemClickBank(int position);
    }

    public DealsAdapter(Context context, ArrayList<EbankModel> bankArrayList, OnBankItemClick onBankItemClick) {
        this.context = context;
        this.bankArrayList = bankArrayList;
        this.onBankItemClick = onBankItemClick;
    }

    @NonNull
    @Override
    public BankCustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BankCustomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bank, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BankCustomViewHolder holder, final int position) {
        holder.name.setText(bankArrayList.get(position).getName());
        Picasso.get().load(bankArrayList.get(position).getLogoUrl()).placeholder(R.drawable.ic_bank_default).into(holder.logo);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBankItemClick.onItemClickBank(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return bankArrayList.size();
    }

    class BankCustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView logo;
        private TextView name;
        private CardView cardView;

        public BankCustomViewHolder(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.bankImageView);
            name = itemView.findViewById(R.id.bankTitle);
            cardView = itemView.findViewById(R.id.bankCardView);
        }
    }
}
