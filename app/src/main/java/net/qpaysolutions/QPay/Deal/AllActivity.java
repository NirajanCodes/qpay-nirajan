package net.qpaysolutions.QPay.Deal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 7/6/16.
 */
public class AllActivity extends AppCompatActivity {
    NewDealAdapter adapter;
    RecyclerView recyclerView;
    private Toolbar toolbar;
    private String category_name;
    private String cust_id, app_id;
    private net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase QPayCustomerDatabase;
    private LinearLayout mToolbarContainer;
    private int mToolbarHeight;
    private FusedLocationProviderClient mFusedLocationClient;
    private QPayProgressDialog progressDialog;
    private ArrayList<GetdealData> getdealDataListFashion = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListFood = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListEnt = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListmisc = new ArrayList<>();
    private ArrayList<GetdealData> getdealDatasFeatured = new ArrayList<>();
    private ArrayList<GetdealData> getdealDatasFeaturedFashion = new ArrayList<>();
    private ArrayList<GetdealData> getGetdealDatasFeaturedEnt = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main_deals);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        QPayCustomerDatabase = new QPayCustomerDatabase(AllActivity.this);
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        category_name = getIntent().getStringExtra("category_name");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText(category_name);
        mToolbarHeight = Utility.getToolbarHeight(this);
        mToolbarContainer = (LinearLayout) findViewById(R.id.toolbar_container);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        new GetPost().execute();
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!Utility.hasExternalStoragePermission(AllActivity.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(AllActivity.this, PERMISSIONS, Tags.DEAL);
            } else {
                setLocation();
            }
        } else {
            setLocation();

        }

//        if (getdealDataListFood != null) {
//            Collections.sort(getdealDataListFood, new Comparator<GetdealData>() {
//                @Override
//                public int compare(GetdealData lhs, GetdealData rhs) {
//                    return Integer.compare(rhs.getFeatureFlag(), lhs.getFeatureFlag());
//                }
//            });
//            adapter = new NewDealAdapter(AllActivity.this, getdealDataListFood);
//            recyclerView.setAdapter(adapter);
//            final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
//            recyclerView.addItemDecoration(headersDecor);// Add decoration for dividers between list items
//            recyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                @Override
//                public void onChanged() {
//                    headersDecor.invalidateHeaders();
//                }
//            });
//        }
    }

    public void setLocation() {
        if (ActivityCompat.checkSelfPermission(AllActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AllActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(AllActivity.this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(final Location location) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (location != null) {
                                        LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                        LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                        new GetPost().execute();
                                    } else {
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    }
                                }
                            }, 500);
                        }
                    });

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void call(String call) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + call));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    public class GetPost extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String result = null;
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("rad", "1000");
                Log.d("dinesh", "get near by deal : " + jsonObject.toString());
                result = networkAPI.sendHTTPData(Constants.NEAR_BY_DEALS, jsonObject);
                Log.d("dinesh", "get near by deal : " + result);
                JSONObject jsonObject1 = new JSONObject(result);
                JSONObject jsonObject2 = jsonObject1.getJSONObject("GetNearByDealsResult");
                JSONArray jsonArray = jsonObject2.getJSONArray("nearByDeals");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                    String category = jsonObject3.getString("category");
                    int featureFlag = jsonObject3.getInt("featureFlag");

                    if (category.endsWith("Food")) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListFood.add(getData);
                        adapter = new NewDealAdapter(AllActivity.this, getdealDataListFood);
                        recyclerView.setAdapter(adapter);

//                        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
//                        recyclerView.addItemDecoration(headersDecor);// Add decoration for dividers between list items
//                        recyclerView.setLayoutManager(new LinearLayoutManager(AllActivity.this));

//                        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                            @Override
//                            public void onChanged() {
//                                headersDecor.invalidateHeaders();
//                            }
//                        });
                    }
//                    else if (category.endsWith("Food") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListFood.add(getData);
//                        adapter = new NewDealAdapter(AllActivity.this, getdealDataListFood);
//                        recyclerView.setAdapter(adapter);
//                        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
//                        recyclerView.addItemDecoration(headersDecor);// Add decoration for dividers between list items
//                        recyclerView.setLayoutManager(new LinearLayoutManager(AllActivity.this));
//
//                        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                            @Override
//                            public void onChanged() {
//                                headersDecor.invalidateHeaders();
//                            }
//                        });
//                    } else if (category.equals("Fashion") && featureFlag == 0) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListFashion.add(getData);
//                    } else if (category.equals("Fashion") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListFashion.add(getData);
//                    } else if (category.equals("Entertainment") && featureFlag == 0) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListEnt.add(getData);
//                    } else if (category.equals("Entertainment") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListEnt.add(getData);
//                    } else if (category.equals("Miscellaneous") || category.equals("") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListmisc.add(getData);
//                    } else if (category.equals("Miscellaneous") || category.equals("") && featureFlag == 0) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListmisc.add(getData);
//                    }

                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(AllActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progressDialog.dismiss();
                progressDialog.hide();
                Log.d(Utility.TAG, String.valueOf(getdealDataListFood.size()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}