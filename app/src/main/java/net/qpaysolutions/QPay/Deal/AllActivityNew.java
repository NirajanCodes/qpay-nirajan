package net.qpaysolutions.QPay.Deal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by deadlydragger on 7/6/16.
 */
public class AllActivityNew extends AppCompatActivity {
    NewDealAdapter adapter;
    RecyclerView recyclerView;
    private Toolbar toolbar;
    private String category_name;
    private String cust_id, app_id;
    private net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase QPayCustomerDatabase;
    private LinearLayout mToolbarContainer;
    private int mToolbarHeight;
    private FusedLocationProviderClient mFusedLocationClient;
    //    private QPayProgressDialog progressDialog;
    private ArrayList<GetdealData> getdealDataListFashion = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListFood = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListEnt = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListmisc = new ArrayList<>();
    private ArrayList<GetdealData> getdealDatasFeatured = new ArrayList<>();
    private ArrayList<GetdealData> getdealDatasFeaturedFashion = new ArrayList<>();
    private ArrayList<GetdealData> getGetdealDatasFeaturedEnt = new ArrayList<>();
    LinearLayout progressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main_deals);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressLayout = findViewById(R.id.progressLayout);
        setSupportActionBar(toolbar);
        QPayCustomerDatabase = new QPayCustomerDatabase(AllActivityNew.this);
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        category_name = getIntent().getStringExtra("category_name");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText(category_name);
        mToolbarHeight = Utility.getToolbarHeight(this);
        mToolbarContainer = (LinearLayout) findViewById(R.id.toolbar_container);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        TextView headerText = findViewById(R.id.header_text);
        headerText.setText("Spend NPR 1000 get NPR 200 off on these locations.");
        new GetPost().execute();
   /*     if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!Utility.hasExternalStoragePermission(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, Tags.DEAL);
            } else {
                setLocation();
            }
        } else {
            setLocation();

        }*/

//        if (getdealDataListFood != null) {
//            Collections.sort(getdealDataListFood, new Comparator<GetdealData>() {
//                @Override
//                public int compare(GetdealData lhs, GetdealData rhs) {
//                    return Integer.compare(rhs.getFeatureFlag(), lhs.getFeatureFlag());
//                }
//            });
//            adapter = new NewDealAdapter(AllActivity.this, getdealDataListFood);
//            recyclerView.setAdapter(adapter);
//            final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
//            recyclerView.addItemDecoration(headersDecor);// Add decoration for dividers between list items
//            recyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                @Override
//                public void onChanged() {
//                    headersDecor.invalidateHeaders();
//                }
//            });
//        }
    }

    public void setLocation() {
        if (ActivityCompat.checkSelfPermission(AllActivityNew.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AllActivityNew.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(AllActivityNew.this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(final Location location) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (location != null) {
                                        LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                        LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                        new GetPost().execute();
                                    } else {
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    }
                                }
                            }, 500);
                        }
                    });

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public class GetPost extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String result = null;
            try {
//                jsonObject.put("app_id", "AB00000168305qp");
//                jsonObject.put("cust_id", "amhivjwGt/q/+Q08PvnbUg==");
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("rad", "1000");
                Log.d("dinesh", "get near by deal : " + jsonObject.toString() + "..." + Constants.NEAR_BY_DEALS);
                result = networkAPI.sendHTTPData(Constants.NEAR_BY_DEALS, jsonObject);
                Log.d("dinesh", "get near by deal : " + result);
                JSONObject jsonObject1 = new JSONObject(result);
                JSONObject jsonObject2 = jsonObject1.getJSONObject("GetNearByDealsResult");
                JSONArray jsonArray = jsonObject2.getJSONArray("nearByDeals");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject3 = jsonArray.getJSONObject(i);
//                    String category = jsonObject3.getString("category");
//                    int featureFlag = jsonObject3.getInt("featureFlag");
//
//                    if (category.endsWith("Food")) {
                    GetdealData getData = new GetdealData();
                    String dealDesc = jsonObject3.getString("dealDesc");
                    String dealId = jsonObject3.getString("dealId");
                    String distance = jsonObject3.getString("distance");
                    String estbName = jsonObject3.getString("estbName");
                    String imgUrl1 = jsonObject3.getString("imgUrl1");
                    String imgUrl2 = jsonObject3.getString("imgUrl2");
                    String imgUrl3 = jsonObject3.getString("imgUrl3");
                    String imgUrl4 = jsonObject3.getString("imgurl4");
                    String imgUrl5 = jsonObject3.getString("imgurl5");
                    String imgUrl6 = jsonObject3.getString("imgurl6");
                    String imgUrl7 = jsonObject3.getString("imgurl7");
                    String imgUrl8 = jsonObject3.getString("imgurl8");
                    String locLat = jsonObject3.getString("locLat");
                    String locLng = jsonObject3.getString("locLng");
                    String locPhone = jsonObject3.getString("locPhone");
                    String website = jsonObject3.getString("website");
                    String disc_rate = jsonObject3.getString("disc_rate");
                    String rating = jsonObject3.getString("rating");
                    String mon_fri_close = jsonObject3.getString("sun_thu_close");
                    String mon_fri_open = jsonObject3.getString("sun_thu_open");
                    String fri_sat_close = jsonObject3.getString("fri_sat_close");
                    String fri_sat_open = jsonObject3.getString("fri_sat_open");
                    String address = jsonObject3.getString("address");
                    String special_today = jsonObject3.getString("dealSpecial");
                    String monFriOpenTime = mon_fri_open + " - " + mon_fri_close;
                    String friSatOpenTime = fri_sat_open + " - " + fri_sat_close;
                    getData.setAddress(address);
                    getData.setDealSpecial(special_today);
                    getData.setMon_fri_opening_time(monFriOpenTime);
                    getData.setFri_sat_opening_time(friSatOpenTime);
                    getData.setRating(rating);
                    getData.setDealDesc(dealDesc);
                    getData.setDealId(dealId);
                    getData.setDistance(new Decimalformate().decimalFormate(distance));
                    getData.setEstbName(estbName);
                    getData.setImgUrl1(imgUrl1);
                    getData.setImgUrl2(imgUrl2);
                    getData.setImgUrl3(imgUrl3);
                    getData.setLocLat(locLat);
                    getData.setLocLng(locLng);
                    getData.setLocPhone(locPhone);
                    getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
                    getData.setImgUrl4(imgUrl4);
                    getData.setImgUrl5(imgUrl5);
                    getData.setImgUrl6(imgUrl6);
                    getData.setImgUrl7(imgUrl7);
                    getData.setImgUrl8(imgUrl8);
                    getData.setDisc_rate(disc_rate);
                    getdealDataListFood.add(getData);
//                        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
//                        recyclerView.addItemDecoration(headersDecor);// Add decoration for dividers between list items
//                        recyclerView.setLayoutManager(new LinearLayoutManager(AllActivity.this));

//                        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                            @Override
//                            public void onChanged() {
//                                headersDecor.invalidateHeaders();
//                            }
//                        });
//                    }
//                    else if (category.endsWith("Food") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListFood.add(getData);
//                        adapter = new NewDealAdapter(AllActivity.this, getdealDataListFood);
//                        recyclerView.setAdapter(adapter);
//                        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
//                        recyclerView.addItemDecoration(headersDecor);// Add decoration for dividers between list items
//                        recyclerView.setLayoutManager(new LinearLayoutManager(AllActivity.this));
//
//                        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                            @Override
//                            public void onChanged() {
//                                headersDecor.invalidateHeaders();
//                            }
//                        });
//                    } else if (category.equals("Fashion") && featureFlag == 0) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListFashion.add(getData);
//                    } else if (category.equals("Fashion") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListFashion.add(getData);
//                    } else if (category.equals("Entertainment") && featureFlag == 0) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListEnt.add(getData);
//                    } else if (category.equals("Entertainment") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListEnt.add(getData);
//                    } else if (category.equals("Miscellaneous") || category.equals("") && featureFlag == 1) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListmisc.add(getData);
//                    } else if (category.equals("Miscellaneous") || category.equals("") && featureFlag == 0) {
//                        GetdealData getData = new GetdealData();
//                        String dealDesc = jsonObject3.getString("dealDesc");
//                        String dealId = jsonObject3.getString("dealId");
//                        String distance = jsonObject3.getString("distance");
//                        String estbName = jsonObject3.getString("estbName");
//                        String imgUrl1 = jsonObject3.getString("imgUrl1");
//                        String imgUrl2 = jsonObject3.getString("imgUrl2");
//                        String imgUrl3 = jsonObject3.getString("imgUrl3");
//                        String imgUrl4 = jsonObject3.getString("imgurl4");
//                        String imgUrl5 = jsonObject3.getString("imgurl5");
//                        String imgUrl6 = jsonObject3.getString("imgurl6");
//                        String imgUrl7 = jsonObject3.getString("imgurl7");
//                        String imgUrl8 = jsonObject3.getString("imgurl8");
//                        String locLat = jsonObject3.getString("locLat");
//                        String locLng = jsonObject3.getString("locLng");
//                        String locPhone = jsonObject3.getString("locPhone");
//                        String website = jsonObject3.getString("website");
//                        String disc_rate = jsonObject3.getString("disc_rate");
//                        String rating = jsonObject3.getString("rating");
//                        getData.setRating(rating);
//                        getData.setDealDesc(dealDesc);
//                        getData.setDealId(dealId);
//                        getData.setDistance(new Decimalformate().decimalFormate(distance));
//                        getData.setEstbName(estbName);
//                        getData.setImgUrl1(imgUrl1);
//                        getData.setImgUrl2(imgUrl2);
//                        getData.setImgUrl3(imgUrl3);
//                        getData.setLocLat(locLat);
//                        getData.setLocLng(locLng);
//                        getData.setLocPhone(locPhone);
//                        getData.setWebsite(website);
//                        getData.setFeatureFlag(featureFlag);
//                        getData.setImgUrl4(imgUrl4);
//                        getData.setImgUrl5(imgUrl5);
//                        getData.setImgUrl6(imgUrl6);
//                        getData.setImgUrl7(imgUrl7);
//                        getData.setImgUrl8(imgUrl8);
//                        getData.setDisc_rate(disc_rate);
//                        getdealDataListmisc.add(getData);
//                    }

                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new QPayProgressDialog(AllActivityNew.this);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progressLayout.setVisibility(View.GONE);
//                progressDialog.dismiss();
//                progressDialog.hide();
                Log.d(Utility.TAG, String.valueOf(getdealDataListFood.size()));
//                adapter = new NewDealAdapter(AllActivityNew.this, getdealDataListFood);
//                recyclerView.setAdapter(adapter);
                if (getdealDataListFood != null) {
                    Collections.sort(getdealDataListFood, new Comparator<GetdealData>() {
                        @Override
                        public int compare(GetdealData lhs, GetdealData rhs) {
                            return Integer.compare(rhs.getFeatureFlag(), lhs.getFeatureFlag());
                        }
                    });
                    adapter = new NewDealAdapter(AllActivityNew.this, getdealDataListFood);
                    recyclerView.setAdapter(adapter);
//                    final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(adapter);
//                    recyclerView.addItemDecoration(headersDecor);// Add decoration for dividers between list items
                    recyclerView.setLayoutManager(new LinearLayoutManager(AllActivityNew.this));

//                    adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                        @Override
//                        public void onChanged() {
//                            headersDecor.invalidateHeaders();
//                        }
//                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void call(String call) {
        String phone = call;
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }
//    public void Share(String estbName, String dealDesc, LinearLayout main_container) {
//        LinearLayout screenshot = (LinearLayout) findViewById(R.id.main_container);
//        Bitmap bm = screenShot(screenshot.getRootView());
//        File file = saveBitmap(bm, "mantis_image.png");
//        Log.i("chase", "filepath: " + file.getAbsolutePath());
//        Uri uri;
//        uri = FileProvider.getUriForFile(this,this.getApplicationContext().getPackageName() + ".my.package.name.provider",new File(file.getAbsolutePath()));
//        Intent shareIntent = new Intent();
//        shareIntent.setAction(Intent.ACTION_SEND);
////                shareIntent.putExtra(Intent.EXTRA_TEXT, description_data_share);
//        shareIntent.putExtra("android.intent.extra.SUBJECT", estbName);
//        shareIntent.putExtra("android.intent.extra.TEXT", estbName + "\n\n" + dealDesc);
//        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//        shareIntent.setType("text/image/*");
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivity(Intent.createChooser(shareIntent, "share via"));
//    }
//
//
//    private Bitmap screenShot(View view) {
//        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        view.draw(canvas);
//        return bitmap;
//    }
//
//    private static File saveBitmap(Bitmap bm, String fileName) {
//        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
//        File dir = new File(path);
//        if (!dir.exists())
//            dir.mkdirs();
//        File file = new File(dir, fileName);
//        try {
//            FileOutputStream fOut = new FileOutputStream(file);
//            bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
//            fOut.flush();
//            fOut.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return file;
//    }
}