package net.qpaysolutions.QPay.Deal;

import java.io.Serializable;

/**
 * Created by deadlydragger on 7/6/16.
 */
public class GetdealData implements Serializable {
    String category;
    String dealDesc;
    String dealId;
    String distance;
    String estbName;
    String imgUrl1;
    String imgUrl2;
    String imgUrl3;
    String locLat;
    String locLng;
    String locPhone;
    String website;
    String imgUrl5;
    String imgUrl6;
    String imgUrl7;
    String imgUrl4;
    String imgUrl8;
    int featureFlag;
    String disc_rate;
    String rating;

    public String getMon_fri_opening_time() {
        return mon_fri_opening_time;
    }

    public void setMon_fri_opening_time(String mon_fri_opening_time) {
        this.mon_fri_opening_time = mon_fri_opening_time;
    }

    public String getFri_sat_opening_time() {
        return fri_sat_opening_time;
    }

    public void setFri_sat_opening_time(String fri_sat_opening_time) {
        this.fri_sat_opening_time = fri_sat_opening_time;
    }

    String mon_fri_opening_time;
    String fri_sat_opening_time;

    public String getDealSpecial() {
        return dealSpecial;
    }

    public void setDealSpecial(String dealSpecial) {
        this.dealSpecial = dealSpecial;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String dealSpecial;
    String address;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public  String getCategory(){
        return category;
    }
    public String getDealDesc(){
        return dealDesc;
    }
    public String getDealId(){
        return dealId;
    }
    public String getDistance(){
        return distance;
    }
    public String getEstbName(){
        return estbName;
    }
    public  String  getImgUrl1(){
        return imgUrl1;
    }
    public String getImgUrl2(){
        return imgUrl2;
    }
    public String getImgUrl3(){
        return imgUrl3;
    }
    public String getLocLat(){
        return locLat;
    }
    public String getLocLng(){
        return locLng;
    }
    public String getLocPhone(){
        return locPhone;
    }
    public String getWebsite(){
        return website;
    }
    public  String getImgUrl4(){
        return imgUrl4;
    }
    public String getImgUrl5(){
        return  imgUrl5;
    }
    public  String getImgUrl6(){
        return imgUrl6;
    }
    public  String getImgUrl7(){
        return imgUrl7;
    }
    public String getImgUrl8(){
        return imgUrl8;
    }
    public  int getFeatureFlag(){
        return featureFlag;
    }
    public  String getClosingTime(){
        return fri_sat_opening_time;
    }
    public  String getOpeningTime(){
        return mon_fri_opening_time;
    }
    public  String getDisc_rate(){
        return disc_rate;
    }
    public void setDisc_rate(String disc_rate){
        this.disc_rate=disc_rate;
    }

    public void setCategory(String category){
        this.category=category;
    }
    public void setDealDesc(String dealDesc){
        this.dealDesc=dealDesc;
    }
    public void setOpeningTime(String opening_time){
        this.mon_fri_opening_time =opening_time;
    }
    public void setClosingTime(String closing_time){
        this.fri_sat_opening_time =closing_time;
    }
    public void setDealId(String dealId){
        this.dealId=dealId;
    }
    public void setDistance(String distance){
        this.distance=distance;
    }
    public void setEstbName(String estbName){
        this.estbName=estbName;
    }
    public void setImgUrl1(String imgUrl1){
        this.imgUrl1=imgUrl1;
    }
    public void setImgUrl2(String imgUrl2){
        this.imgUrl2=imgUrl2;
    }
    public void setImgUrl3(String imgUrl3){
        this.imgUrl3=imgUrl3;
    }
    public void setLocLat(String locLat){
        this.locLat=locLat;
    }
    public void setLocLng(String locLng){
        this.locLng=locLng;
    }
    public void setLocPhone(String locPhone){
        this.locPhone=locPhone;
    }
    public void setWebsite(String website){
        this.website=website;
    }
    public  void setImgUrl4(String imgUrl4){
        this.imgUrl4=imgUrl4;
    }
    public  void setImgUrl5(String imgUrl5){
        this.imgUrl5=imgUrl5;
    }
    public  void setImgUrl6(String imgUrl6){
        this.imgUrl6=imgUrl6;
    }
    public void setImgUrl7(String imgUrl7){
        this.imgUrl7=imgUrl7;
    }
    public  void setImgUrl8(String imgUrl8){
        this.imgUrl8=imgUrl8;
    }
    public  void  setFeatureFlag(int featureFlag){
        this.featureFlag=featureFlag;
    }
}