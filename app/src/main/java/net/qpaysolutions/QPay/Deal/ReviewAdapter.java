package net.qpaysolutions.QPay.Deal;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by deadlydragger on 3/2/17.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder> {
    Context context ;
    ArrayList<ReviewModel> reviewModelArrayList;
    public ReviewAdapter(Context context , ArrayList<ReviewModel> reviewModels){
        this.context=context;
        this.reviewModelArrayList=reviewModels;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_list_item,parent,false);
        ReviewViewHolder reviewViewHolder = new ReviewViewHolder(view);
        return reviewViewHolder;
    }
    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        try {
            holder.rate_review.setText(String.valueOf(reviewModelArrayList.get(position).getRating()));
        }catch (Exception e){
            e.printStackTrace();
        }

        if (!reviewModelArrayList.get(position).getImageUrl().isEmpty()){
            Picasso.get()
                    .load(reviewModelArrayList.get(position).getImageUrl())
                    .placeholder(R.drawable.header_placeholder)
                    .error(R.drawable.header_placeholder)
                    .into(holder.review_image);
        }

        holder.description_review.setText(reviewModelArrayList.get(position).getReview());

        holder.name_review.setText(reviewModelArrayList.get(position).getName());

        String mydate =reviewModelArrayList.get(position).getDate();
        SimpleDateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date_set = srcDf.parse(mydate);
            SimpleDateFormat destDf = new SimpleDateFormat("MMM dd, yyyy");// hh:mm a
            mydate = destDf.format(date_set);
            holder.date_review.setText(mydate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int getItemCount() {
        return reviewModelArrayList.size();
    }
}
