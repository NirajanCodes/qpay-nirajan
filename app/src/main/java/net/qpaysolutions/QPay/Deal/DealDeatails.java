package net.qpaysolutions.QPay.Deal;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.glide.slider.library.Animations.BaseAnimationInterface;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.TextSliderView;

import net.qpaysolutions.QPay.CheckInternetConnection.Networkavailable;
import net.qpaysolutions.QPay.CustumClasses.ChildCarouselAnimation;
import net.qpaysolutions.QPay.CustumClasses.FontTextViewRegular;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by deadlydragger on 7/10/16.
 */
public class DealDeatails extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private TextView name, website, description, deal_description, deal_valid, distance;
    private Button phone;
    private String call;
    private SliderLayout carouselView;
    //    private CircularTextView discount_rate_details;
    private ImageView map_view;
    private String TAG = "dinesh";
    private Networkavailable networkavailable = new Networkavailable();
    private String getlat, getlng;
    private ArrayList<GetdealData> image_valid = new ArrayList<>();
    private int viewWidth, viewHeight;
    private double lat;
    private double lon;
    private String cust_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    //    private QPayProgressDialog progressDialog;
    private String dealId;
    private String friSatOpenTime;
    private String monFriOpenTime, app_id;
    private LinearLayout mainContainer;
    String description_data_share, deal_center;
    //    RatingBar library_normal_ratingbar;
    TextView rate_now;
    String deal_name_resturent = "", special_today = "";
    TextView special, txt_special_today;
    String locPhone, website_res;
    String rating = "", yourRating = "";
    TextView address;
    String address_res = "";
    LinearLayout web_visibility, makecall, location_visibility, show_review;
    EditText review;
    ArrayList<ReviewModel> reviewModelArrayList = new ArrayList<>();
    RecyclerView recycle_review;
    ReviewAdapter reviewAdapter;
    String intent_name, intent_desc, intent_img3, intent_phone, intent_website, intent_mon_fri_opening_time, intent_fri_sat_opening_time, intent_dealSpecial, intent_address, intent_img1, intent_img2;
    String intent_img4, intent_img5, intent_img6, intent_img7, intent_img8, intent_lat, intent_lng;
    LinearLayout progressLayout;
    private GetdealData getdealData;
    private TextView addressTextview, kmTextview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.iteam_deal_details);
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            progressLayout = findViewById(R.id.progressLayout);
            name = (TextView) findViewById(R.id.details_name);
            website = (TextView) findViewById(R.id.details_web);
            description = (TextView) findViewById(R.id.details_description_name);
//            distance = (TextView) findViewById(R.id.distance);
            carouselView = findViewById(R.id.slider);
//            discount_rate_details = (CircularTextView) findViewById(R.id.discount_rate_details);
            map_view = (ImageView) findViewById(R.id.map_view);
            map_view.setVisibility(View.VISIBLE);
            mainContainer = (LinearLayout) findViewById(R.id.main_container);
            mainContainer.setVisibility(View.INVISIBLE);
//            library_normal_ratingbar = (RatingBar) findViewById(R.id.library_normal_ratingbar);
            rate_now = (TextView) findViewById(R.id.rate_now);
            special = (TextView) findViewById(R.id.special);
            txt_special_today = (TextView) findViewById(R.id.special_today);
            address = (TextView) findViewById(R.id.address);
            location_visibility = (LinearLayout) findViewById(R.id.location_visibility);
            makecall = (LinearLayout) findViewById(R.id.make_call_layout);
            web_visibility = (LinearLayout) findViewById(R.id.web);
            recycle_review = (RecyclerView) findViewById(R.id.recycle_review);
            addressTextview = findViewById(R.id.address_textview);
            kmTextview = findViewById(R.id.km_textview);
            recycle_review.setHasFixedSize(true);
            recycle_review.setLayoutManager(new LinearLayoutManager(this));
            rate_now.setOnClickListener(this);
            QPayCustomerDatabase = new QPayCustomerDatabase(this);
            QPayCustomerDatabase.getReadableDatabase();
            cust_id = QPayCustomerDatabase.getCustomerID();
            app_id = QPayCustomerDatabase.getKeyAppId();
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            final ArrayList<GetdealData> myList = (ArrayList<GetdealData>) getIntent().getSerializableExtra("details");
            int position = getIntent().getIntExtra("position", 0);
            getdealData = myList.get(position);
            dealId = getdealData.getDealId();
//            setData();
            new GetPost().execute();

            monFriOpenTime = getdealData.getMon_fri_opening_time();
            friSatOpenTime = getdealData.getFri_sat_opening_time();

            special_today = getdealData.getDealSpecial();
            locPhone = getdealData.getLocPhone();
            website_res = getdealData.getWebsite();
            getlat = getdealData.getLocLat();
            getlng = getdealData.getLocLng();
            address_res = getdealData.getAddress();
            try {
                mainContainer.setVisibility(View.VISIBLE);
                setOpeningTime();
                progressLayout.setVisibility(View.GONE);

                if (website_res.isEmpty()) {
                    web_visibility.setVisibility(View.GONE);
                }
                if (locPhone.isEmpty()) {
                    makecall.setVisibility(View.GONE);
                }
//                special.setText(special_today);


                website.setText(website_res);
                ((FontTextViewRegular) findViewById(R.id.phone_num_text_view)).setText(locPhone);

            } catch (Exception e) {
                e.printStackTrace();
            }
            addressTextview.setText((getdealData.getAddress() != null) ? getdealData.getAddress() : " ");
            kmTextview.setText((getdealData.getDistance() != null) ? getdealData.getDistance() + " KM" : " ");

            if (!getdealData.getImgUrl1().isEmpty()) {
                TextSliderView textSliderView = new TextSliderView(this);
                textSliderView
                        .image(getdealData.getImgUrl1());
                        /*.setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
*/                carouselView.addSlider(textSliderView);
            }
            if (!getdealData.getImgUrl2().isEmpty()) {
                TextSliderView textSliderView2 = new TextSliderView(this);
                textSliderView2
                        .image(getdealData.getImgUrl2());
                /*        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);*/
                carouselView.addSlider(textSliderView2);
            }
            if (!getdealData.getImgUrl3().isEmpty()) {
                TextSliderView textSliderView3 = new TextSliderView(this);
                textSliderView3
                        .image(getdealData.getImgUrl3())
                        ;
                carouselView.addSlider(textSliderView3);
            }
            if (!getdealData.getImgUrl4().isEmpty()) {
                TextSliderView textSliderView4 = new TextSliderView(this);
                textSliderView4.image(getdealData.getImgUrl4())
                       ;
                carouselView.addSlider(textSliderView4);
            }
            if (!getdealData.getImgUrl5().isEmpty()) {
                TextSliderView textSliderView5 = new TextSliderView(this);
                textSliderView5.image(getdealData.getImgUrl5())
                       ;
                carouselView.addSlider(textSliderView5);
            }
            if (!getdealData.getImgUrl6().isEmpty()) {
                TextSliderView textSliderView6 = new TextSliderView(this);
                textSliderView6.image(getdealData.getImgUrl6())
                       ;
                carouselView.addSlider(textSliderView6);
            }
            if (!getdealData.getImgUrl7().isEmpty()) {
                TextSliderView textSliderView7 = new TextSliderView(this);
                textSliderView7.image(getdealData.getImgUrl7())
                     ;
                carouselView.addSlider(textSliderView7);
            }
            if (!getdealData.getImgUrl8().isEmpty()) {
                TextSliderView textSliderView8 = new TextSliderView(this);
                textSliderView8.image(getdealData.getImgUrl7());
                carouselView.addSlider(textSliderView8);
            }
            lat = Double.parseDouble(LatLngPref.getLat());
            lon = Double.parseDouble(LatLngPref.getLng());


            carouselView.setCustomAnimation((BaseAnimationInterface) new ChildCarouselAnimation());
            carouselView.startAutoCycle();

            Log.d(Utility.TAG, "load image : " + getdealData.getImgUrl1() + getdealData.getImgUrl2() + getdealData.getImgUrl3());


            name.setText(getdealData.getEstbName().substring(0, 1).toUpperCase() + getdealData.getEstbName().substring(1));

            deal_name_resturent = getdealData.getEstbName();
//            description.setText(getdealData.getDealDesc());
            description_data_share = getdealData.getDealDesc();
            deal_center = getdealData.getEstbName();
//            distance.setText(getdealData.getDistance() + "Km");
//            discount_rate_details.setStrokeWidth(1);
//            discount_rate_details.setSolidColor("#ABD84E");
            ViewTreeObserver viewTreeObserver = map_view.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        map_view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        viewWidth = map_view.getWidth();
                        viewHeight = map_view.getHeight();
                        Log.d(Utility.TAG, "map view height width : " + viewHeight + viewWidth);
                        new DownloadImage().execute();


                    }
                });
            }
            website.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = website_res;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });

            findViewById(R.id.getDirectionLayout).setOnClickListener(this);
            LinearLayout mCallButton = (LinearLayout) findViewById(R.id.make_call_layout);
            mCallButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = ((FontTextViewRegular) findViewById(R.id.phone_num_text_view)).getText().toString();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                }
            });
//            discount_rate_details.setText(getdealData.getDisc_rate().substring(0, 2).replace(".", "") + "%\noff");
            map_view.setOnClickListener(this);

            website.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String urlStr = website.getText().toString();
                   /* if (!urlStr.contains("http://")) {
                        urlStr = urlStr.replace("www.", "");
                        urlStr = "http://www." + urlStr;
                    }*/
                    Uri url = Uri.parse(urlStr);
                    startActivity(new Intent(Intent.ACTION_VIEW, url));
                }
            });
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
            toolbar_text.setText(getdealData.getEstbName().substring(0, 1).toUpperCase() + getdealData.getEstbName().substring(1));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   /* private void setData() {
        try {
            if (intent_img1.length() > 0) {
                TextSliderView textSliderView = new TextSliderView(this);
                textSliderView
                        .image(intent_img1)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView);
            }
            if (intent_img2.length() > 0) {
                TextSliderView textSliderView2 = new TextSliderView(this);
                textSliderView2
                        .image(intent_img2)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView2);
            }
            if (intent_img3.length() > 0) {
                TextSliderView textSliderView3 = new TextSliderView(this);
                textSliderView3
                        .image(intent_img3)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView3);
            }
            if (intent_img4.length() > 0) {
                TextSliderView textSliderView4 = new TextSliderView(this);
                textSliderView4.image(intent_img4)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView4);
            }
            if (intent_img5.length() > 0) {
                TextSliderView textSliderView5 = new TextSliderView(this);
                textSliderView5.image(intent_img5)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView5);
            }
            if (intent_img6.length() > 0) {
                TextSliderView textSliderView6 = new TextSliderView(this);
                textSliderView6.image(intent_img6)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView6);
            }
            if (intent_img7.length() > 0) {
                TextSliderView textSliderView7 = new TextSliderView(this);
                textSliderView7.image(intent_img7)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView7);
            }
            if (intent_img8.length() > 0) {
                TextSliderView textSliderView8 = new TextSliderView(this);
                textSliderView8.image(intent_img8)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .error(R.drawable.image_error)
                        .empty(R.drawable.image_error);
                carouselView.addSlider(textSliderView8);
            }
            lat = ((CustomerApplication) this.getApplication()).getLat();
            lon = ((CustomerApplication) this.getApplication()).getLng();

            carouselView.setCustomAnimation(new ChildCarouselAnimation());
            carouselView.stopAutoCycle();


            name.setText(intent_name.substring(0, 1).toUpperCase() + intent_desc.substring(1));

            deal_name_resturent = intent_name;
            description.setText(intent_desc);
            description_data_share = intent_desc;
            deal_center = intent_name;
//                distance.setText(getdealData.getDistance() + "Km");
//            discount_rate_details.setStrokeWidth(1);
////            discount_rate_details.setSolidColor("#ABD84E");
            ViewTreeObserver viewTreeObserver = map_view.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        map_view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        viewWidth = map_view.getWidth();
                        viewHeight = map_view.getHeight();
                        Log.d(Utility.TAG, "map view height width : " + viewHeight + viewWidth);
                        new DownloadImage().execute();
                    }
                });
            }
            website.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = "http://" + intent_website;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });
            LinearLayout mCallButton = (LinearLayout) findViewById(R.id.make_call_layout);
            mCallButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = ((FontTextViewRegular) findViewById(R.id.phone_num_text_view)).getText().toString();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", intent_phone, null));
                    startActivity(intent);
                }
            });
//            discount_rate_details.setText(getdealData.getDisc_rate().substring(0, 2).replace(".", "") + "%\noff");
//            map_view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    final Intent intent = new
//                            Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" +
//                            "saddr=" + lat + "," + lon + "&daddr=" + Double.parseDouble(String.valueOf(intent_lat)) + "," +
//                            Double.parseDouble(String.valueOf(intent_lng))));
//                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                    startActivity(intent);
//
//                }
//            });
            website.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String urlStr = intent_website;
                   *//* if (!urlStr.contains("http://")) {
                        urlStr = urlStr.replace("www.", "");
                        urlStr = "http://www." + urlStr;
                    }*//*
                    Uri url = Uri.parse(urlStr);
                    startActivity(new Intent(Intent.ACTION_VIEW, url));
                }
            });
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
            toolbar_text.setText(intent_name.substring(0, 1).toUpperCase() + intent_name.substring(1));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
*/

    public void call() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + call));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.rate_now:
////
//                try {
//                    rateApp(Float.valueOf(yourRating), deal_name_resturent);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                break;

            case R.id.map_view:
            case R.id.getDirectionLayout:
                Intent intent = new
                        Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" +
                        "saddr=" + lat + "," + lon + "&daddr=" + Double.parseDouble(getdealData.getLocLat()) + "," +
                        Double.parseDouble(getdealData.getLocLng())));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
                break;
        }
    }


    public class PostDealReview extends AsyncTask<String, String, String> {
        float rating;
        String review;

        public PostDealReview(float rating, String review) {
            this.rating = rating;
            this.review = review;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String get_review = "";
            try {
                jsonObject.put("appId", app_id);
                jsonObject.put("custId", cust_id);
                jsonObject.put("dealId", dealId);
                jsonObject.put("review", review);
                jsonObject.put("rating", rating);
                jsonObject.put("lat", ((CustomerApplication) DealDeatails.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) DealDeatails.this.getApplication()).getLng());

                Log.d(Utility.TAG, "rate post : " + jsonObject);
                get_review = networkAPI.sendHTTPData(Constants.POST_DEAL_REVIEW, jsonObject);
                Log.d(Utility.TAG, "rate post  response : " + get_review);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return get_review;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressLayout.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject GetDealReviewResult = jsonObject.getJSONObject("GetDealReviewResult");
                boolean success = GetDealReviewResult.getBoolean("success");
//                if (success) {
//
//                } else {
//                    rateApp(0, deal_name_resturent);
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressLayout.setVisibility(View.VISIBLE);
//            progressDialog = new QPayProgressDialog(DealDeatails.this);
//            progressDialog.show();
        }
    }

    public void rateApp(float value, String resturent_name) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(DealDeatails.this, R.style.Custom_Dialog);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_rate_app, null);
        final TextView current_rate = (TextView) v.findViewById(R.id.current_rate);
        final EditText reviw = (EditText) v.findViewById(R.id.review);
        TextView next = (TextView) v.findViewById(R.id.ok);
        current_rate.setText(String.valueOf(value));
//        final me.zhanghai.android.materialratingbar.MaterialRatingBar materialRatingBar = (me.zhanghai.android.materialratingbar.MaterialRatingBar) v.findViewById(R.id.library_normal_ratingbar);
//        materialRatingBar.setRating(value);
//
//        materialRatingBar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
//            @Override
//            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
//                current_rate.setText(String.valueOf(rating));
//            }
//        });
        builder.setView(v);

        final Dialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        reviw.requestFocus();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        final WindowManager.LayoutParams wlmp = dialog.getWindow()
                .getAttributes();
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.gravity = Gravity.CENTER;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                new PostDealReview(materialRatingBar.getRating(), reviw.getText().toString()).execute();
//                Log.d(Utility.TAG, "  rating away: " + materialRatingBar.getRating());
//                dialog.dismiss();


            }
        });

        v.findViewById(R.id.not).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }

    //Downloading an Image
    class DownloadImage extends AsyncTask<String, String, Bitmap> {
        String imageURL = "http://maps.google.com/maps/api/staticmap?center=" + getdealData.getLocLat() + "," + getdealData.getLocLng() + "&zoom=18&size=" + viewWidth + "x" + viewHeight +
                "&sensor=true" + "&markers=color:red%7Clabel:R%7C" + getdealData.getLocLat() + "," + getdealData.getLocLng() + "&key=AIzaSyBp5tlO2k9FXslVhU3hMyMUcrdTpmR06_M";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {

            Bitmap bmp = null;
            try {
                Log.d(Utility.TAG, "map code : " + imageURL);
                URL ulrn = new URL(imageURL);
                HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
                InputStream is = con.getInputStream();
                bmp = BitmapFactory.decodeStream(is);
                if (null != bmp)
                    return bmp;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap s) {
            if (s != null) {
                map_view.setImageBitmap(s);
            }
        }
    }

    public class GetPost extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lng", lon);
                jsonObject.put("lat", lat);
                jsonObject.put("deal_id", dealId);
                Log.d(TAG, "get near by deal : " + jsonObject.toString());
                String result = networkAPI.sendHTTPData(Constants.NEAR_BY_DEALS_BY_ID, jsonObject);
                Log.d(TAG, "response near by deal : " + result);
                JSONObject jsonObject1 = new JSONObject(result);
                JSONObject jsonObject2 = jsonObject1.getJSONObject("GetDealWithIDResult");
                JSONArray jsonArray = jsonObject2.getJSONArray("nearByDeals");
                JSONArray dealReview = jsonObject2.getJSONArray("dealReview");
                JSONObject jsonObject3 = jsonArray.getJSONObject(0);
                String mon_fri_close = jsonObject3.getString("sun_thu_close");
                String mon_fri_open = jsonObject3.getString("sun_thu_open");
                String fri_sat_close = jsonObject3.getString("fri_sat_close");
                String fri_sat_open = jsonObject3.getString("fri_sat_open");
                address_res = jsonObject3.getString("address");
                monFriOpenTime = mon_fri_open + " - " + mon_fri_close;
                friSatOpenTime = fri_sat_open + " - " + fri_sat_close;
                rating = jsonObject3.getString("rating");
                special_today = jsonObject3.getString("dealSpecial");
                locPhone = jsonObject3.getString("locPhone");
                website_res = jsonObject3.getString("website");
                yourRating = jsonObject3.getString("yourRating");
                getlat = jsonObject3.getString("locLat");
                getlng = jsonObject3.getString("locLng");
                for (int i = 0; i < dealReview.length(); i++) {
                    JSONObject jsonObject4 = dealReview.getJSONObject(i);
                    ReviewModel reviewModel = new ReviewModel();
                    reviewModel.setDate(jsonObject4.getString("Date"));
                    reviewModel.setImageUrl(jsonObject4.getString("ImageUrl"));
                    reviewModel.setName(jsonObject4.getString("Name"));
                    reviewModel.setRating(jsonObject4.getDouble("Rating"));
                    reviewModel.setReview(jsonObject4.getString("Review"));
                    reviewModelArrayList.add(reviewModel);

                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return rating;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressLayout.setVisibility(View.VISIBLE);
//            progressDialog = new QPayProgressDialog(DealDeatails.this);
//            progressDialog.show();


        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            try {
//                reviewAdapter = new ReviewAdapter(DealDeatails.this, reviewModelArrayList);
//                recycle_review.setAdapter(reviewAdapter);
                mainContainer.setVisibility(View.VISIBLE);
                setOpeningTime();
                progressLayout.setVisibility(View.GONE);
//                progressDialog.dismiss();
//                progressDialog.hide();
                if (address_res.isEmpty()) {
                    location_visibility.setVisibility(View.GONE);
                }
                if (website_res.isEmpty()) {
                    web_visibility.setVisibility(View.GONE);
                }
                if (locPhone.isEmpty()) {
                    makecall.setVisibility(View.GONE);
                }
                address.setText(address_res);
//                if(special_today.length()>0){
                special.setText(special_today);
//                }else{
//                    txt_special_today.setVisibility(View.GONE);
//                }

                website.setText(website_res);
                ((FontTextViewRegular) findViewById(R.id.phone_num_text_view)).setText(locPhone);

//                DealDeatails.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        float rate = Float.parseFloat(s);
//                        library_normal_ratingbar.setRating(rate);
//                    }
//                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void setOpeningTime() {
        FontTextViewRegular monFriOpenTextView = (FontTextViewRegular) findViewById(R.id.mon_fri_open_textview);
        monFriOpenTextView.setText(monFriOpenTime);

        FontTextViewRegular friSatOpenTextView = (FontTextViewRegular) findViewById(R.id.fri_sat_open_textview);
        friSatOpenTextView.setText(friSatOpenTime);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.deat_deatilas_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        switch (item.getItemId()) {
            case R.id.share:
                LinearLayout screenshot = (LinearLayout) findViewById(R.id.main_container);
                Bitmap bm = screenShot(screenshot.getRootView());
                File file = saveBitmap(bm, "mantis_image.png");
                Log.i("chase", "filepath: " + file.getAbsolutePath());
                Uri uri;
                uri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".my.package.name.provider", new File(file.getAbsolutePath()));
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
//                shareIntent.putExtra(Intent.EXTRA_TEXT, description_data_share);
                shareIntent.putExtra("android.intent.extra.SUBJECT", deal_center);
                shareIntent.putExtra("android.intent.extra.TEXT", deal_center + "\n\n" + description_data_share);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("text/image/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "share via"));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(DealDeatails.this,DealActivityFinal.class));
    }

    private Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private static File saveBitmap(Bitmap bm, String fileName) {
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }


}

