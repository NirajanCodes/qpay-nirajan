package net.qpaysolutions.QPay.Deal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Environment;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.CustumClasses.FontTextViewRegular;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by Owner on 8/16/2016.
 */
public class NewDealAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdapterView.OnItemClickListener {
    private ArrayList<GetdealData> items = new ArrayList<>();
    private Context mContext;
    private GestureDetector gestureDetector;

    public NewDealAdapter(Context context, ArrayList<GetdealData> list) {
        setHasStableIds(true);
        this.mContext = context;
        this.items = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        RecyclerView.ViewHolder viewHold;
        switch (viewType) {
            case 0:
                itemView = LayoutInflater.
                        from(parent.getContext()).
                        inflate(R.layout.list_iteam_dealfinal_feture, parent, false);
                viewHold = new FlagViewHolder(itemView);
                break;

            default:
                itemView = LayoutInflater.
                        from(parent.getContext()).
                        inflate(R.layout.list_iteam_dealfinal, parent, false);
                viewHold = new Myviewholder(itemView);
                break;
        }
        return viewHold;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final GetdealData getdealData = items.get(position);
        try {

            if (getdealData.getFeatureFlag() == 1) {

                BaseSliderView.OnSliderClickListener sliderClickListener = new BaseSliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(BaseSliderView slider) {
                        startDetailsActivity(position, getdealData);
                    }
                };

                final FlagViewHolder dataViewHolder = (FlagViewHolder) holder;

                dataViewHolder.title.setText(getdealData.getAddress());
                if (getdealData.getDistance() != null && !TextUtils.isEmpty(getdealData.getDistance())) {
                    dataViewHolder.milesLayout.setVisibility(View.VISIBLE);
                    dataViewHolder.milesTextview.setText(getdealData.getDistance() + " KM");
                } else {
                    dataViewHolder.milesLayout.setVisibility(View.GONE);
                }
                dataViewHolder.share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Share(getdealData.getEstbName(), getdealData.getDealDesc(), dataViewHolder.main_container);
                    }
                });
                dataViewHolder.name_flg.setText(getdealData.getEstbName().substring(0, 1).toUpperCase() + getdealData.getEstbName().substring(1));
                dataViewHolder.phoneNumberTextView.setText(getdealData.getLocPhone());
                dataViewHolder.make_call_flg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((AllActivityNew) mContext).call(getdealData.getLocPhone());
                    }
                });

                dataViewHolder.title.setOnClickListener(v -> startDetailsActivity(position, getdealData));
                dataViewHolder.name_flg.setOnClickListener(v -> startDetailsActivity(position, getdealData));


            } else {
                final Myviewholder myviewholder = (Myviewholder) holder;
                myviewholder.title.setText(getdealData.getAddress());
                myviewholder.content.setText(getdealData.getLocPhone());
                Picasso.get()
                        .load(getdealData.getImgUrl1()).into(myviewholder.mainImage);
                if (getdealData.getDistance() != null && !TextUtils.isEmpty(getdealData.getDistance())) {
                    myviewholder.milesLayout.setVisibility(View.VISIBLE);
                    myviewholder.milesTextview.setText(getdealData.getDistance() + " KM");
                } else {
                    myviewholder.milesLayout.setVisibility(View.GONE);
                }

                myviewholder.name.setText(getdealData.getEstbName().substring(0, 1).toUpperCase() + getdealData.getEstbName().substring(1));

                myviewholder.title.setOnClickListener(v -> startDetailsActivity(position, getdealData));
                myviewholder.name.setOnClickListener(v -> startDetailsActivity(position, getdealData));

                myviewholder.mainImage.setOnClickListener(v -> startDetailsActivity(position, getdealData));

                myviewholder.make_call.setOnClickListener(v -> ((AllActivityNew) mContext).call(getdealData.getLocPhone()));

                myviewholder.share.setOnClickListener(v -> Share(getdealData.getEstbName(), getdealData.getDealDesc(), myviewholder.main_container));

                Log.d(Utility.TAG, "dinscount rate : " + getdealData.getDisc_rate().substring(0, 2).replace(".", "") + "%\nOFF");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GetdealData getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        //More to come
        if (items.get(position).getFeatureFlag() == 1) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    public class Myviewholder extends RecyclerView.ViewHolder {
        ImageView mainImage;
        TextView title, content, milesTextview;

        ImageView share;
        SliderLayout mDemoSlider;
        LinearLayout make_call;
        TextView name;
        LinearLayout main_container, milesLayout;

        public Myviewholder(View itemView) {
            super(itemView);
//            mDemoSlider_flg = (SliderLayout) itemView.findViewById(R.id.slider_flg);
            mainImage = (ImageView) itemView.findViewById(R.id.mainImage);
            title = (TextView) itemView.findViewById(R.id.title);
            content = (TextView) itemView.findViewById(R.id.no_flag);
            share = (ImageView) itemView.findViewById(R.id.share);
            name = (TextView) itemView.findViewById(R.id.name);
            make_call = (LinearLayout) itemView.findViewById(R.id.make_call);
            main_container = (LinearLayout) itemView.findViewById(R.id.main_container);
            milesLayout = itemView.findViewById(R.id.milesLayout);
            milesTextview = itemView.findViewById(R.id.milesTextView);
        }
    }

    public class FlagViewHolder extends RecyclerView.ViewHolder {
        ImageView mainImage_flg;
        TextView title, content_flg, milesTextview;
        ImageView share;
        LinearLayout make_call_flg;
        TextView name_flg;
        FontTextViewRegular phoneNumberTextView;
        LinearLayout main_container, milesLayout;


        public FlagViewHolder(View itemView) {
            super(itemView);
            mainImage_flg = (ImageView) itemView.findViewById(R.id.mainImage);
            share = (ImageView) itemView.findViewById(R.id.share);
            title = (TextView) itemView.findViewById(R.id.title);
            name_flg = (TextView) itemView.findViewById(R.id.name);
            make_call_flg = (LinearLayout) itemView.findViewById(R.id.make_call_flag);
            phoneNumberTextView = (FontTextViewRegular) itemView.findViewById(R.id.no_flag);
            main_container = (LinearLayout) itemView.findViewById(R.id.main_container);
            milesLayout = itemView.findViewById(R.id.milesLayout);
            milesTextview = itemView.findViewById(R.id.milesTextView);
        }
    }


    private void startDetailsActivity(int position, GetdealData getdealData) {
        Intent intent = new Intent(mContext, DealDeatails.class);
        intent.putExtra("details", items);
        intent.putExtra("position", position);
        mContext.startActivity(intent);
    }

    public void Share(String estbName, String dealDesc, LinearLayout main_container) {
        Bitmap bm = screenShot(main_container.getRootView());
        File file = saveBitmap(bm, "mantis_image.png");
        Log.i("chase", "filepath: " + file.getAbsolutePath());
        Uri uri;
        uri = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".my.package.name.provider", new File(file.getAbsolutePath()));
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra("android.intent.extra.SUBJECT", estbName);
        shareIntent.putExtra("android.intent.extra.TEXT", estbName + "\n\n" + dealDesc);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("text/image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        mContext.startActivity(Intent.createChooser(shareIntent, "share via"));
    }

    private Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private static File saveBitmap(Bitmap bm, String fileName) {
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 90, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
}