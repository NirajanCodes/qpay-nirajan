package net.qpaysolutions.QPay.Deal;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.NearByAtms.NearbyAtmsFragment;
import net.qpaysolutions.QPay.NearByTaxi.NearTaxiActivity;
import net.qpaysolutions.QPay.Resturant.ResturantActivity;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DealFragment extends Fragment implements View.OnClickListener {
    private String TAG = "dinesh";
    private QPayProgressDialog progressDialog;
    private ArrayList<GetdealData> getdealDataListFashion = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListFood = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListEnt = new ArrayList<>();
    private ArrayList<GetdealData> getdealDataListmisc = new ArrayList<>();
    private  ArrayList<GetdealData> getdealDatasFeatured = new ArrayList<>();
    private ArrayList<GetdealData> getdealDatasFeaturedFashion = new ArrayList<>();
    private  ArrayList<GetdealData> getGetdealDatasFeaturedEnt = new ArrayList<>();
    private  LinearLayout food, fashion, ent, misc;
    private TextView balance;
    private String cust_id, app_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private TextView food_array, ent_array, misc_array, fashion_array;
    private AHBottomNavigation bottomNavigation;
    private FusedLocationProviderClient mFusedLocationClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_main_dealfinal, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       /* ((MainActivity) getActivity()).setToolbar_text("Nearby Deals");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(false);*/
        food = (LinearLayout) view.findViewById(R.id.food);
        fashion = (LinearLayout) view.findViewById(R.id.fashion);
        ent = (LinearLayout) view.findViewById(R.id.ent);
        misc = (LinearLayout) view.findViewById(R.id.misc);
        balance = (TextView) view.findViewById(R.id.balance_daeal);
        food_array = (TextView) view.findViewById(R.id.food_array);
        ent_array = (TextView) view.findViewById(R.id.entraint_array);
        misc_array = (TextView) view.findViewById(R.id.others_array);
        fashion_array = (TextView) view.findViewById(R.id.fashion_array);
        food.setOnClickListener(this);
        fashion.setOnClickListener(this);
        ent.setOnClickListener(this);
        misc.setOnClickListener(this);
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
        balance.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
      //  buttonBar(view);
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!Utility.hasExternalStoragePermission(getContext(), PERMISSIONS)) {
                ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, Tags.DEAL);
            } else {
                setLocation();
            }
        } else {
            setLocation();

        }

    }

    public  void  setLocation(){
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(final Location location) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (location != null) {
                                        LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                        LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                        new GetPost().execute();
                                    } else {
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    }
                                }
                            }, 500);
                        }
                    });

        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.food:
                if (getdealDataListFood.size() > 0) {
                    Intent intent = new Intent(getActivity(), AllActivity.class);
                    intent.putExtra("mylist", getdealDataListFood);
                    intent.putExtra("feature", getdealDatasFeatured);
                    intent.putExtra("category_name", "Food");
                    startActivity(intent);
                } else {
                    custumdialogfailure("No deal found!", "There are no nearby deals found around you.");
                }
                break;
            case R.id.fashion:
                if (getdealDataListFashion.size() > 0) {
                    Intent fashion = new Intent(getActivity(), AllActivity.class);
                    fashion.putExtra("mylist", getdealDataListFashion);
                    fashion.putExtra("category_name", "Fashion");
                    fashion.putExtra("feature", getdealDatasFeaturedFashion);
                    startActivity(fashion);
                } else {
                    custumdialogfailure("No deal found!", "There are no nearby deals found around you.");
                }
                break;
            case R.id.ent:
                if (getdealDataListEnt.size() > 0) {
                    Intent ent = new Intent(getActivity(), AllActivity.class);
                    ent.putExtra("mylist", getdealDataListEnt);
                    ent.putExtra("category_name", "Entertainment");
                    ent.putExtra("feature", getGetdealDatasFeaturedEnt);
                    startActivity(ent);
                } else {
                    custumdialogfailure("No deal found!", "There are no nearby deals found around you.");
                }
                break;
            case R.id.misc:
                if (getdealDataListmisc.size() > 0) {
                    Intent misc = new Intent(getActivity(), AllActivity.class);
                    misc.putExtra("mylist", getdealDataListmisc);
                    misc.putExtra("category_name", "Miscellaneous");
                    startActivity(misc);
                } else {
                    custumdialogfailure("No deal found!", "There are no nearby deals found around you.");
                }
                break;
        }
    }

    public class GetPost extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String result = null;
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("rad", "1000");
                Log.d(TAG, "get near by deal : " + jsonObject.toString());
                result = networkAPI.sendHTTPData(Constants.NEAR_BY_DEALS, jsonObject);
                Log.d(TAG, "get near by deal : " + result);
                JSONObject jsonObject1 = new JSONObject(result);
                JSONObject jsonObject2 = jsonObject1.getJSONObject("GetNearByDealsResult");
                JSONArray jsonArray = jsonObject2.getJSONArray("nearByDeals");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                    String category = jsonObject3.getString("category");
                    int featureFlag = jsonObject3.getInt("featureFlag");

                    if (category.endsWith("Food") && featureFlag == 0) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        String mon_fri_close = jsonObject3.getString("sun_thu_close");
                        String mon_fri_open = jsonObject3.getString("sun_thu_open");
                        String fri_sat_close = jsonObject3.getString("fri_sat_close");
                        String fri_sat_open = jsonObject3.getString("fri_sat_open");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListFood.add(getData);
                    } else if (category.endsWith("Food") && featureFlag == 1) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListFood.add(getData);
                    } else if (category.equals("Fashion") && featureFlag == 0) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListFashion.add(getData);
                    } else if (category.equals("Fashion") && featureFlag == 1) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListFashion.add(getData);
                    } else if (category.equals("Entertainment") && featureFlag == 0) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListEnt.add(getData);
                    } else if (category.equals("Entertainment") && featureFlag == 1) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListEnt.add(getData);
                    } else if (category.equals("Miscellaneous") || category.equals("") && featureFlag == 1) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListmisc.add(getData);
                    } else if (category.equals("Miscellaneous") || category.equals("") && featureFlag == 0) {
                        GetdealData getData = new GetdealData();
                        String dealDesc = jsonObject3.getString("dealDesc");
                        String dealId = jsonObject3.getString("dealId");
                        String distance = jsonObject3.getString("distance");
                        String estbName = jsonObject3.getString("estbName");
                        String imgUrl1 = jsonObject3.getString("imgUrl1");
                        String imgUrl2 = jsonObject3.getString("imgUrl2");
                        String imgUrl3 = jsonObject3.getString("imgUrl3");
                        String imgUrl4 = jsonObject3.getString("imgurl4");
                        String imgUrl5 = jsonObject3.getString("imgurl5");
                        String imgUrl6 = jsonObject3.getString("imgurl6");
                        String imgUrl7 = jsonObject3.getString("imgurl7");
                        String imgUrl8 = jsonObject3.getString("imgurl8");
                        String locLat = jsonObject3.getString("locLat");
                        String locLng = jsonObject3.getString("locLng");
                        String locPhone = jsonObject3.getString("locPhone");
                        String website = jsonObject3.getString("website");
                        String disc_rate = jsonObject3.getString("disc_rate");
                        String rating = jsonObject3.getString("rating");
                        getData.setRating(rating);
                        getData.setDealDesc(dealDesc);
                        getData.setDealId(dealId);
                        getData.setDistance(new Decimalformate().decimalFormate(distance));
                        getData.setEstbName(estbName);
                        getData.setImgUrl1(imgUrl1);
                        getData.setImgUrl2(imgUrl2);
                        getData.setImgUrl3(imgUrl3);
                        getData.setLocLat(locLat);
                        getData.setLocLng(locLng);
                        getData.setLocPhone(locPhone);
                        getData.setWebsite(website);
                        getData.setFeatureFlag(featureFlag);
                        getData.setImgUrl4(imgUrl4);
                        getData.setImgUrl5(imgUrl5);
                        getData.setImgUrl6(imgUrl6);
                        getData.setImgUrl7(imgUrl7);
                        getData.setImgUrl8(imgUrl8);
                        getData.setDisc_rate(disc_rate);
                        getdealDataListmisc.add(getData);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progressDialog.dismiss();
                progressDialog.hide();
                food_array.setText(String.valueOf(getdealDataListFood.size()));
                fashion_array.setText(String.valueOf(getdealDataListFashion.size()));
                ent_array.setText(String.valueOf(getdealDataListEnt.size()));
                misc_array.setText(String.valueOf(getdealDataListmisc.size()));
                Log.d(Utility.TAG, String.valueOf(getdealDataListFood.size()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void buttonBar(View view) {
        bottomNavigation = (AHBottomNavigation) view.findViewById(R.id.bottom_navigation);
        AHBottomNavigationItem item = new AHBottomNavigationItem(R.string.tab_0, R.drawable.ic_action_footer_home_tab, R.color.colorPrimary);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_action_chat_tab, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_action_button_car, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_nearby_atm_tab, R.color.colorPrimary);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_action_restaurant_tab, R.color.colorPrimary);
        bottomNavigation.addItem(item);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item5);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item4);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setForceTint(false);
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setColored(true);
        try {
            if (GeneralPref.getChatCount() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getChatCount()), 1);
            }
            if (GeneralPref.getTaxi() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getTaxi()), 4);
            }
            if (GeneralPref.getAtm() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getAtm()), 3);
            }
            if (GeneralPref.getNoofRestro() > 2) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getNoofRestro() ), 2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                Log.d("dinesh", "tab selected" + position);
                if (position == 0) {
                    Intent history = new Intent(getActivity(), MainActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    DealFragment.this.startActivity(history, bndlanimation);
                } else if (position == 1) {
                    try {
                        Intent taxi = new Intent(getActivity(), ChatActivity.class);
                        Bundle animationtaxi = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                        getActivity().startActivity(taxi, animationtaxi);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                } else if (position == 2) {
                    Intent history = new Intent(getActivity(), ResturantActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    DealFragment.this.startActivity(history, bndlanimation);

                } else if (position == 3) {

                    NearbyAtmsFragment cardProfileMain = new NearbyAtmsFragment();
                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.animation_one_activity, R.anim.animation_two_activity)
                            .replace(R.id.fragment, cardProfileMain)
                            .commit();
                } else if (position == 4) {
                    Intent history = new Intent(getActivity(), NearTaxiActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    startActivity(history, bndlanimation);

                }
                return true;
            }
        });
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final TextView proceed_txt = (TextView) v.findViewById(R.id.proceed_txt);
        proceed_txt.setText("OK");
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onResume() {

        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().onBackPressed();
                    /*MainFragmentNotCall najirEnglish = new MainFragmentNotCall();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                    getActivity().setTitle("QPay");*/
                    return true;
                }
                return false;
            }
        });
    }
}