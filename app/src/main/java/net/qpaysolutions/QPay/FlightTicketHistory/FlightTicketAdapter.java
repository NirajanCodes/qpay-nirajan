package net.qpaysolutions.QPay.FlightTicketHistory;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 11/23/17.
 */

public class FlightTicketAdapter extends RecyclerView.Adapter<TicketHolder> {
    ArrayList<FlightHistoryModel> flightLists;
    FlightHistoryFragment context;

    public FlightTicketAdapter(ArrayList<FlightHistoryModel> flightLists, FlightHistoryFragment context) {
        this.flightLists = flightLists;
        this.context = context;
    }

    @Override
    public TicketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutInflater = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flight_ticket, parent, false);

        return new TicketHolder(layoutInflater);
    }

    @Override
    public void onBindViewHolder(TicketHolder holder, int position) {
        final FlightHistoryModel flightHistoryModel = flightLists.get(position);
        holder.departureDetails(flightHistoryModel, context.getContext());
       /* holder.downloadReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GeneratePDF(flightHistoryModel.getStan(), flightHistoryModel.getCrrn(), context).execute();
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return flightLists.size();
    }
}
