package net.qpaysolutions.QPay.FlightTicketHistory;

import android.os.AsyncTask;
import android.widget.Toast;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/21/18.
 */

public class FlightticketAPI extends AsyncTask<String,String,String> {
    FlightHistoryFragment flightHistoryFragment;

    public FlightticketAPI(FlightHistoryFragment flightHistoryFragment) {
        this.flightHistoryFragment = flightHistoryFragment;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id",GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            return NetworkAPI.sendHTTPData(Constants.TICKETS_HISTORY,jsonObject);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        flightHistoryFragment.hideDialog();
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("00")){
                ArrayList<FlightHistoryModel> flightHistoryModels= new ArrayList<>();
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject object= jsonArray.getJSONObject(i);
                    FlightHistoryModel flightList = new FlightHistoryModel();
                    flightList.setRefundable(object.getString("Refundable"));
                    flightList.setArrivalTime(object.getString("ArrivalTime"));
                    flightList.setDeparture(object.getString("Departure"));
                    flightList.setFare(object.getString("Fare"));
                    flightList.setClassCode(object.getString("ClassCode"));
                    flightList.setFlightTime(object.getString("FlightTime"));
                    flightList.setFlightNumber(object.getString("FlightNumber"));
                    flightList.setGender(object.getString("Gender"));
                    flightList.setTax(object.getString("Tax"));
                    flightList.setFreeBaggage(object.getString("FreeBaggage"));
                    flightList.setReportingTime(object.getString("ReportingTime"));
                    flightList.setPnrNo(object.getString("PnrNo"));
                    flightList.setArrival(object.getString("Arrival"));
                    flightList.setIssueDate(object.getString("IssueDate"));
                    flightList.setFlightDate(object.getString("FlightDate"));
                    flightList.setAirlineName(object.getString("AirlineName"));
                    flightList.setAirlineCode(object.getString("AirlineCode"));
                    flightList.setSector(object.getString("Sector"));
                    flightList.setName(object.getString("Name"));
                    flightList.setPaxType(object.getString("PaxType"));
                    flightList.setTicketNumber(object.getString("TicketNumber"));
                    flightList.setCurrency(object.getString("Currency"));
                    flightList.setNationality(object.getString("Nationality"));
                    flightHistoryModels.add(flightList);
                   }
                   flightHistoryFragment.setListFlight(flightHistoryModels);
            }else {
                Toast.makeText(flightHistoryFragment.getContext(),"No History Found!",Toast.LENGTH_LONG).show();
            }

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(flightHistoryFragment.getContext(),"No History Found!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        flightHistoryFragment.showDialog();
    }
}
