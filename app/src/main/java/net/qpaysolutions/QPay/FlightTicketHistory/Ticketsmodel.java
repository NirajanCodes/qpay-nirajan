package net.qpaysolutions.QPay.FlightTicketHistory;

/**
 * Created by deadlydragger on 11/23/17.
 */

public class Ticketsmodel {
    String Refundable,ArrivalTime,Departure,Fare,ClassCode,FlightTime,FlightNumber,Gender,Tax,FreeBaggage,ReportingTime,PnrNo,Arrival,IssueDate,FlightDate,AirlineName,AirlineCode,Sector,Name,PaxType,TicketNumber,Currency,Nationality;

    public String getRefundable() {
        return Refundable;
    }

    public void setRefundable(String refundable) {
        Refundable = refundable;
    }

    public String getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        ArrivalTime = arrivalTime;
    }

    public String getDeparture() {
        return Departure;
    }

    public void setDeparture(String departure) {
        Departure = departure;
    }

    public String getFare() {
        return Fare;
    }

    public void setFare(String fare) {
        Fare = fare;
    }

    public String getClassCode() {
        return ClassCode;
    }

    public void setClassCode(String classCode) {
        ClassCode = classCode;
    }

    public String getFlightTime() {
        return FlightTime;
    }

    public void setFlightTime(String flightTime) {
        FlightTime = flightTime;
    }

    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getFreeBaggage() {
        return FreeBaggage;
    }

    public void setFreeBaggage(String freeBaggage) {
        FreeBaggage = freeBaggage;
    }

    public String getReportingTime() {
        return ReportingTime;
    }

    public void setReportingTime(String reportingTime) {
        ReportingTime = reportingTime;
    }

    public String getPnrNo() {
        return PnrNo;
    }

    public void setPnrNo(String pnrNo) {
        PnrNo = pnrNo;
    }

    public String getArrival() {
        return Arrival;
    }

    public void setArrival(String arrival) {
        Arrival = arrival;
    }

    public String getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(String issueDate) {
        IssueDate = issueDate;
    }

    public String getFlightDate() {
        return FlightDate;
    }

    public void setFlightDate(String flightDate) {
        FlightDate = flightDate;
    }

    public String getAirlineName() {
        return AirlineName;
    }

    public void setAirlineName(String airlineName) {
        AirlineName = airlineName;
    }

    public String getAirlineCode() {
        return AirlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        AirlineCode = airlineCode;
    }

    public String getSector() {
        return Sector;
    }

    public void setSector(String sector) {
        Sector = sector;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPaxType() {
        return PaxType;
    }

    public void setPaxType(String paxType) {
        PaxType = paxType;
    }

    public String getTicketNumber() {
        return TicketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        TicketNumber = ticketNumber;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }
}
