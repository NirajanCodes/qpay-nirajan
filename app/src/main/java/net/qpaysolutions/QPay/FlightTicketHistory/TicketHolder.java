package net.qpaysolutions.QPay.FlightTicketHistory;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Constants;

/**
 * Created by deadlydragger on 11/23/17.
 */

public class TicketHolder extends RecyclerView.ViewHolder {
 private    TextView airLinesName,departureDate,departureTime,departureFrom,arrivalDate,arrivalTime,arrivalFrom,type_class,flightId,totalAmount,type_flight,namePassenger,typePassenger,pnrPassenger;
   private ImageView logoAirlines;
   private TextView ticket,pnr;
    Button downloadReceipt;
    public TicketHolder(View itemView) {
        super(itemView);
        airLinesName = (TextView) itemView.findViewById(R.id.airLinesName);
        departureDate = (TextView) itemView.findViewById(R.id.departureDate);
        departureTime = (TextView) itemView.findViewById(R.id.departureTime);
        departureFrom = (TextView) itemView.findViewById(R.id.departureFrom);
        arrivalDate = (TextView) itemView.findViewById(R.id.arrivalDate);
        arrivalTime = (TextView) itemView.findViewById(R.id.arrivalTime);
        arrivalFrom = (TextView) itemView.findViewById(R.id.arrivalFrom);
        type_class = (TextView) itemView.findViewById(R.id.type_class);
        flightId = (TextView) itemView.findViewById(R.id.flightId);
        totalAmount = (TextView) itemView.findViewById(R.id.totalAmount);
        logoAirlines = (ImageView) itemView.findViewById(R.id.logoAirlines);
        type_flight=(TextView)itemView.findViewById(R.id.type_flight);
        namePassenger=(TextView)itemView.findViewById(R.id.namePassenger);
        typePassenger=(TextView)itemView.findViewById(R.id.typePassenger);
        pnrPassenger=(TextView)itemView.findViewById(R.id.pnrPassenger);
        ticket=(TextView)itemView.findViewById(R.id.ticket);
        pnr=(TextView)itemView.findViewById(R.id.pnr);
    }



    public void departureDetails(FlightHistoryModel departureList, Context context) {
        airLinesName.setText(departureList.getAirlineName());
        departureDate.setText(departureList.getFlightDate());
        departureTime.setText(departureList.getFlightTime());
        departureFrom.setText(departureList.getDeparture());
        arrivalDate.setText(departureList.getFlightDate());
        arrivalTime.setText(departureList.getArrivalTime());
        arrivalFrom.setText(departureList.getArrival());
        type_class.setText("Class: "+departureList.getClassCode());
        flightId.setText("Flight Number: "+departureList.getFlightNumber());
        totalAmount.setText(departureList.getCurrency()+" "+departureList.getFare());
        type_flight.setText(departureList.getRefundable());
        namePassenger.setText(departureList.getName());
        typePassenger.setText(departureList.getPaxType());
        pnrPassenger.setText(departureList.getPnrNo());
        ticket.setText(departureList.getTicketNumber());
        pnr.setText(departureList.getPnrNo());

       /* airLinesName.setText(departureList.getDeparture() + " -> " + departureList.getArrival());
        departureDate.setText("Flight No : " + departureList.getFlightNumber());
        payment_date_departure.setText(FlightsUtils.getDate());
        payment_flight_class.setText("Class " + departureList.getClassCode());
        payment_departure_date.setText(departureList.getFlightDate());
        payment_departure_land.setText(departureList.getFlightTime());
        payment_dep_from.setText(departureList.getDeparture());
        payment_dep_to.setText(departureList.getArrival());
        payment_airline_name.setText(departureList.getAirlineName());
        payment_fund_type.setText(departureList.getRefundable());*/
        Picasso.get()
                .load(Constants.FLIGHT_IMAGE + departureList.getAirlineCode() + ".png").placeholder(R.drawable.flights).error(R.drawable.flights).into(logoAirlines);

    }
}
