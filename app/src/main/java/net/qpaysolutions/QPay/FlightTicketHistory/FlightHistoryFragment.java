package net.qpaysolutions.QPay.FlightTicketHistory;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 11/23/17.
 */

public class FlightHistoryFragment extends Fragment implements DialogInterface,FlightHistoryInterface {
    private QPayProgressDialog qPayProgressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.flight_ticket, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        qPayProgressDialog = new QPayProgressDialog(getContext());
        new FlightticketAPI(FlightHistoryFragment.this).execute();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showDialog() {
        qPayProgressDialog.show();
    }

    @Override
    public void hideDialog() {
        qPayProgressDialog.dismiss();
    }

    @Override
    public void setListFlight(ArrayList<FlightHistoryModel> listFlight) {
        setAdapter(listFlight);
    }
    private void setAdapter(ArrayList<FlightHistoryModel> arrayList){
        RecyclerView flightTickets = (RecyclerView) getView().findViewById(R.id.flightTickets);
        flightTickets.setHasFixedSize(true);
        flightTickets.setLayoutManager(new LinearLayoutManager(getContext()));
        FlightTicketAdapter flightTicketAdapter = new FlightTicketAdapter(arrayList,FlightHistoryFragment.this);
        flightTickets.setAdapter(flightTicketAdapter);
    }
}
