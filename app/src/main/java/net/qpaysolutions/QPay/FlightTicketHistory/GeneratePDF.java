package net.qpaysolutions.QPay.FlightTicketHistory;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by deadlydragger on 3/21/18.
 */

public class  GeneratePDF extends AsyncTask<String,String,String> {
    String stan,crrn;
    FlightHistoryFragment flightHistoryFragment;

    public GeneratePDF(String stan, String crrn,FlightHistoryFragment flightHistoryFragment) {
        this.stan = stan;
        this.crrn = crrn;
        this.flightHistoryFragment = flightHistoryFragment;

    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id",GeneralPref.getCustId());
            jsonObject.put("stan",stan);
            jsonObject.put("crrn",crrn);
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            NetworkAPI.downloadFileFromeUrl(Constants.TICKETS_DOWNLOAD,jsonObject, flightHistoryFragment.getActivity(),"qpay_flights_tickets"+"_"+crrn);


        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        flightHistoryFragment.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        flightHistoryFragment.hideDialog();
        try{
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/qpay/"+"qpay_flights_tickets"+"_"+crrn+".pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
            flightHistoryFragment.startActivity(Intent.createChooser(intent, "Open PDF"));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}