package net.qpaysolutions.QPay.DepositHistory;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.History.historyInterface.OnLoadMoreListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by deadlydragger on 2/8/17.
 */

public class DepositeExpandableAdapter extends BaseExpandableListAdapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    private ArrayList<DepositHistoryModel> groups;
    private HistoryScrollListener historyScrollListener;
    private boolean loading = false;
    private boolean isallDataLoaded = false;
    private OnLoadMoreListener onLoadMoreListener;
    private int preLast;
    private String trnType;

    public DepositeExpandableAdapter(Context context, String trnType, ArrayList<DepositHistoryModel> groups, ExpandableListView expandableListView, OnLoadMoreListener onLoadMoreListener) {
        this.context = context;
        this.groups = groups;
        this.onLoadMoreListener = onLoadMoreListener;
        historyScrollListener = new HistoryScrollListener();
        this.trnType = trnType;
        expandableListView.setOnScrollListener(historyScrollListener);
    }
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<DepositHistoryChild> chList = groups.get(groupPosition).getItems();
        return chList.get(childPosition);
    }
    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<DepositHistoryChild> chList = groups.get(groupPosition).getItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }



    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public int getGroupTypeCount() {
        return 2;
    }

    @Override
    public int getGroupType(int groupPosition) {
        if (groups.get(groupPosition) != null) {
            return VIEW_ITEM;
        } else {
            return VIEW_PROG;
        }
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        boolean isStillUp=false;

        final DepositHistoryModel group = (DepositHistoryModel) getGroup(groupPosition);
        int type = getGroupType(groupPosition);
        if (convertView == null) {

            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            if (type == VIEW_ITEM) {
                convertView = inf.inflate(R.layout.deposit_history_row, parent, false);
            } else {
                convertView = inf.inflate(R.layout.progress_item, parent, false);
            }
        }

        if (type == VIEW_ITEM) {
            ImageView img_url;
            TextView bank_name,bank_branch,account_number,acc_holder_name,amount,date,tran_id;
            img_url=(ImageView)convertView.findViewById(R.id.img_url);
            bank_name=(TextView)convertView.findViewById(R.id.bank_name);
            bank_branch=(TextView)convertView.findViewById(R.id.bank_branch);
            amount=(TextView)convertView.findViewById(R.id.amount);
            date=(TextView)convertView.findViewById(R.id.date);
//            tran_id=(TextView)convertView.findViewById(R.id.tran_id);

            Picasso.get()
                    .load(group.getImageUrl())
                    .placeholder(R.drawable.ic_sct_card)
                    .error(R.drawable.ic_sct_card)
                    .noFade()
                    .into(img_url);
            String substring = group.getAccountNumber().substring(Math.max(group.getAccountNumber().length() - 4, 0));
          /*  acc_holder_name.setText(group.getAccountName());
            account_number.setText("XXXX-XXXX-"+substring);*/
            bank_branch.setText(group.getBranchName());
            bank_name.setText(group.getInstitutionName());
            amount.setText("NPR "+group.getAmount());
//            tran_id.setText("TrnId: "+group.getTrnId());



            String mydate = group.getDate();
            SimpleDateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                Date date_set = srcDf.parse(mydate);
                SimpleDateFormat destDf = new SimpleDateFormat("MMM dd, yyyy ");
                mydate = destDf.format(date_set);
                date.setText(mydate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar1);
            progressBar.getIndeterminateDrawable().setColorFilter(0xFF000000, android.graphics.PorterDuff.Mode.MULTIPLY);
            progressBar.setIndeterminate(true);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        int type = getGroupType(groupPosition);
        if (type == VIEW_ITEM) {
            DepositHistoryChild child = (DepositHistoryChild) getChild(groupPosition, childPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.deposite_child, null);
            }

            TextView account_number = (TextView) convertView.findViewById(R.id.account_number);
            TextView acc_holder_name = (TextView) convertView.findViewById(R.id.acc_holder_name);

            account_number.setText("XXXX-XXXX-XXXX-"+child.getAccountNumber());
            acc_holder_name.setText(child.getAccountName());

            return convertView;
        }
        return null;
    }

    public void setLoaded() {
        loading = false;
    }

    public void setIsAllLoaded() {
        isallDataLoaded = true;
    }

    public HistoryScrollListener getScrollListener() {
        return historyScrollListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Inner Class: HistoryScrollListener">
    public class HistoryScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            Log.d("sangharsha", "onScrollStateChanged: ");
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            final int lastItem = firstVisibleItem + visibleItemCount;
            if (lastItem == totalItemCount) {
                int lastVisibleItem = view.getLastVisiblePosition();
                Log.d("sangharsha", "onScroll: ");
                if (preLast != lastItem) {
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleItemCount - 1) && !isallDataLoaded) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                    }
                    preLast = lastItem;
                    loading = true;
                }
            }
        }
    }

}
