package net.qpaysolutions.QPay.DepositHistory;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 1/25/17.
 */

public class DepositHistoryModel {
    String Date,AccountName,AccountNumber,BranchName,ImageUrl,InstitutionName,TrnId;
    String Amount;
    private ArrayList<DepositHistoryChild> Items;

    public ArrayList<DepositHistoryChild> getItems() {
        return Items;
    }

    public void setItems(ArrayList<DepositHistoryChild> items) {
        Items = items;
    }

    public String getAccountName() {
        return AccountName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public String getInstitutionName() {
        return InstitutionName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public String getAmount() {
        return Amount;
    }

    public String getDate() {
        return Date;
    }

    public String getTrnId() {
        return TrnId;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public void setInstitutionName(String institutionName) {
        InstitutionName = institutionName;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public void setAccountName(String accountName) {
        AccountName = accountName;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setTrnId(String trnId) {
        TrnId = trnId;
    }

}
