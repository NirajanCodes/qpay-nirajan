package net.qpaysolutions.QPay.DepositHistory;

/**
 * Created by deadlydragger on 2/8/17.
 */

public class DepositHistoryChild {
    String AccountName,AccountNumber;

    public String getAccountName() {
        return AccountName;
    }

    public void setAccountName(String accountName) {
        AccountName = accountName;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }
}
