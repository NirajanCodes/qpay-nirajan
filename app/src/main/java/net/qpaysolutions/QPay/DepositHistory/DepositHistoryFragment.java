package net.qpaysolutions.QPay.DepositHistory;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.FragmentCollection.MainFragmentNotCall;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.History.historyInterface.OnLoadMoreListener;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 1/24/17.
 */

public class DepositHistoryFragment extends Fragment {
    String app_id,term_id;
    QPayCustomerDatabase qpayMerchantDatabase;
    QPayProgressDialog qPayProgressDialog;
    ArrayList<DepositHistoryModel> depositHistoryModelArrayList = new ArrayList<>();

    DepositeExpandableAdapter  depositeExpandableAdapter;
    TextView total_deposit;
    LinearLayout empty_history;
    TextView wallet_amount;
    SharedPreferences sharedPreferences;
    static String MY_FILE_SHARED = "Mybalance";
    String balance_merch;
    private ExpandableListView ExpandList;
    private String trnType = "BP";
    private int lastExpandedPosition = -1;
    private boolean isAllDataLoaded = false;
    int peg=1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.deposit_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity)getActivity()). setToolbar_text("Deposit History");
        ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);
        new History(peg).execute();
        sharedPreferences = getActivity().getSharedPreferences(MY_FILE_SHARED, Context.MODE_PRIVATE);
        balance_merch = sharedPreferences.getString("balance", null);
       /* recycler_view=(RecyclerView)view.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));*/
        total_deposit=(TextView)view.findViewById(R.id.total_deposit);
        empty_history=(LinearLayout)view.findViewById(R.id.empty_history);
        wallet_amount=(TextView)view.findViewById(R.id.wallet_amount);
        ExpandList = (ExpandableListView) view.findViewById(R.id.exp_list);
        wallet_amount.setText("NPR "+ BalancePref.getAvailableAmount());
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        qpayMerchantDatabase.getReadableDatabase();
        app_id=qpayMerchantDatabase.getKeyAppId();
        term_id=qpayMerchantDatabase.getCustomerID();
    }

    public  class History extends AsyncTask<String,String,String>{
        String stringURL = "https://testportal.qpaysolutions.net/api/bankdeposit/History";
        int page;

        public History(int page)
        {
            this.page=page;
        }

        @Override
        protected String doInBackground(String... params) {
            String deposit_response="";
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("AppId",app_id);
                jsonObject.put("Id",term_id);
                jsonObject.put("page",page);
                jsonObject.put("offset",20);
                Log.d(Utility.TAG,"history account post : " + jsonObject);
                deposit_response = httpUrlConnectionPost.sendHTTPData(stringURL,jsonObject);
                Log.d(Utility.TAG,"history account response : " + deposit_response);

            }catch (Exception e){
                e.printStackTrace();
            }
            return deposit_response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject2 = new JSONObject(s);
                boolean success = jsonObject2.getBoolean(Tags.SUCESS);

                if (success){
                    JSONObject jsonObject= jsonObject2.getJSONObject("data");
                    JSONArray jsonArray = jsonObject.getJSONArray("History");
                    String TotalDeposit= new Decimalformate().decimalFormate(jsonObject.getString("TotalDeposit"));
                    total_deposit.setText(TotalDeposit);
                    for (int i=0;i<jsonArray.length();i++){
                        ArrayList<DepositHistoryChild> ch_list = new ArrayList<>();
                        DepositHistoryModel group = new DepositHistoryModel();
                        DepositHistoryChild child = new DepositHistoryChild();
                        DepositHistoryModel depositHistoryModel = new DepositHistoryModel();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        depositHistoryModel.setAccountName(jsonObject1.getString("AccountName"));
                        depositHistoryModel.setAccountNumber(jsonObject1.getString("AccountNumber"));
                        depositHistoryModel.setDate(jsonObject1.getString("Date"));
                        depositHistoryModel.setAmount(new Decimalformate().decimalFormate(jsonObject1.getString("Amount")));
                        depositHistoryModel.setBranchName(jsonObject1.getString("BranchName"));
                        depositHistoryModel.setImageUrl(jsonObject1.getString("ImageUrl"));
                        depositHistoryModel.setInstitutionName(jsonObject1.getString("InstitutionName"));
                        depositHistoryModel.setTrnId(jsonObject1.getString("TrnId"));
                        child.setAccountName(jsonObject1.getString("AccountName"));
                        child.setAccountNumber(jsonObject1.getString("AccountNumber"));
                        ch_list.add(child);
                        depositHistoryModel.setItems(ch_list);
                        depositHistoryModelArrayList.add(depositHistoryModel);

                    }
                    depositeExpandableAdapter = new DepositeExpandableAdapter(
                            getActivity(), trnType, depositHistoryModelArrayList, ExpandList, onLoadMoreListenerListener);
                    ExpandList.setAdapter(depositeExpandableAdapter);
                    ExpandList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                        @Override
                        public void onGroupExpand(int groupPosition) {
                            ExpandList.setChildDivider(getResources().getDrawable(R.color.white));
                            if (lastExpandedPosition != -1
                                    && groupPosition != lastExpandedPosition) {
                                ExpandList.collapseGroup(lastExpandedPosition);
                            }
                            lastExpandedPosition = groupPosition;
                        }
                    });
                }else {
                    empty_history.setVisibility(View.VISIBLE);
                }
            }catch (Exception e){
                e.printStackTrace();
                empty_history.setVisibility(View.VISIBLE);
            }
        }
    }
    OnLoadMoreListener onLoadMoreListenerListener = new OnLoadMoreListenerImpl() {
        @Override
        public void onLoadMore() {
            if(depositHistoryModelArrayList.size() < Utility.OFFSET){
                isAllDataLoaded = true;
            }
            if(depositeExpandableAdapter != null && !isAllDataLoaded) {
                depositHistoryModelArrayList.add(null);
                depositeExpandableAdapter.notifyDataSetChanged();
                peg++;
                new History(peg).execute();
            }
        }
    };
    abstract class OnLoadMoreListenerImpl implements OnLoadMoreListener {

    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    MainFragmentNotCall najirEnglish = new MainFragmentNotCall();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();


                    return true;
                }
                return false;
            }
        });
    }
}
