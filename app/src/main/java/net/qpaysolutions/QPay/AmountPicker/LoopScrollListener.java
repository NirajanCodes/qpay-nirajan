package net.qpaysolutions.QPay.AmountPicker;

/**
 * Created by CSharp05-user on 19/08/2016.
 */
public interface LoopScrollListener {
    void onItemSelect(int item);
}