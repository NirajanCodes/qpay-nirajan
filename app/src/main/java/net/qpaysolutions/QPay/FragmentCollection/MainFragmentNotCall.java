package net.qpaysolutions.QPay.FragmentCollection;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Billpayment.BillPaymentActivity;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.NearByAtms.NearbyAtmsFragment;
import net.qpaysolutions.QPay.NearByTaxi.NearTaxiActivity;
import net.qpaysolutions.QPay.Register.SuspendedUserFragment;
import net.qpaysolutions.QPay.Register.Unauthorizeuser;
import net.qpaysolutions.QPay.Resturant.ResturantActivity;
import net.qpaysolutions.QPay.SalesMerchants.ReadMerchantQRActivity;
import net.qpaysolutions.QPay.Send.SendMainActivity;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 9/29/16.
 */
public class MainFragmentNotCall extends Fragment implements View.OnClickListener {
    private QPayCustomerDatabase getQPayCustomerDatabase;
    private LinearLayout pay_fragment, nearby, billpayment, send;
    private TextView balance;
    private String cust_id, app_id;
    private String balance_amount;
    private SQLiteDatabase db;
    private String enc;
    private int taxi;
    private AHBottomNavigation bottomNavigation;
    private FrameLayout progressBar;
    private boolean isVisible = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_main_dashbord, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).setToolbar_text("");
        ((MainActivity) getActivity()).showHomeLogo(false);
        ((MainActivity) getActivity()).setRightIcon(true);
        progressBar =  view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        if (GeneralPref.getKYC().equals("R") || GeneralPref.getKYC().equals("NA")) {
            ((MainActivity) getActivity()).disableKYC(true);
        } else {
            ((MainActivity) getActivity()).disableKYC(false);
        }
        balance = (TextView) view.findViewById(R.id.balance_main);
        pay_fragment = (LinearLayout) view.findViewById(R.id.pay_frame);
        nearby = (LinearLayout) view.findViewById(R.id.bill_payment);
        billpayment = (LinearLayout) view.findViewById(R.id.bill_payment);
        nearby = (LinearLayout) view.findViewById(R.id.nearby);
        send = (LinearLayout) view.findViewById(R.id.send);
        taxi = ((CustomerApplication) getActivity().getApplication()).getTaxiCount();
        pay_fragment.setOnClickListener(this);
        nearby.setOnClickListener(this);
        billpayment.setOnClickListener(this);
        send.setOnClickListener(this);
        balance.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
        bottomNavigation = (AHBottomNavigation) view.findViewById(R.id.bottom_navigation);
        AHBottomNavigationItem item = new AHBottomNavigationItem(R.string.tab_0, R.drawable.ic_tab_home, R.color.white);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_tab_chat, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_tab_atm, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_tab_nearbytaxi, R.color.colorPrimary);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_tab_restaurant, R.color.colorPrimary);
        bottomNavigation.addItem(item);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item5);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item4);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setForceTint(false);
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setColored(true);
        try {
            if (GeneralPref.getChatCount() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getChatCount()), 1);
            }
            if (GeneralPref.getTaxi() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getTaxi()), 4);
            }
            if (GeneralPref.getAtm() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getAtm()), 3);
            }
            if (GeneralPref.getNoofRestro() > 2) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getNoofRestro()), 2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                Log.d("dinesh", "tab selected" + position);
                if (position == 0) {
                } else if (position == 1) {
                    startActivity(new Intent(getActivity(), ChatActivity.class));

                } else if (position == 2) {
                    try {
                        Intent taxi = new Intent(getActivity(), ResturantActivity.class);
                        Bundle animationtaxi = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                        getActivity().startActivity(taxi, animationtaxi);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } else if (position == 3) {

                    NearbyAtmsFragment cardProfileMain = new NearbyAtmsFragment();
                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.animation_one_activity, R.anim.animation_two_activity)
                            .replace(R.id.fragment, cardProfileMain)
                            .commit();
                } else if (position == 4) {
                    try {
                        Intent taxi = new Intent(getActivity(), NearTaxiActivity.class);
                        Bundle animationtaxi = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                        getActivity().startActivity(taxi, animationtaxi);


                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        });
        view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getQPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        getQPayCustomerDatabase.getReadableDatabase();
        getQPayCustomerDatabase.getWritableDatabase();
        db = getQPayCustomerDatabase.getWritableDatabase();
        cust_id = getQPayCustomerDatabase.getCustomerID();
        app_id = getQPayCustomerDatabase.getKeyAppId();
        enc = getQPayCustomerDatabase.getEncryptedCustomerId();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_refresh, menu);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                new CheckStatus().execute();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_frame:
                startActivity(new Intent(getActivity(),ReadMerchantQRActivity.class));

                break;
            case R.id.send:
                Intent intent = new Intent(getActivity(),SendMainActivity.class);
                intent.putExtra("friend_list", "0");
                startActivity(intent);
                break;
            case R.id.bill_payment:
                startActivity(new Intent(getActivity(),BillPaymentActivity.class));
                break;
            case R.id.nearby:
                startActivity(new Intent(getActivity(), WithDrawablActivity.class));
                break;
        }
    }


    public class CheckStatus extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            String status_check;
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                status_check = new NetworkAPI().sendHTTPData(Constants.CHECK_MY_STATUS, jsonObject);
                return status_check;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isVisible == false) {
                isVisible = true;
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            isVisible = false;
            progressBar.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject checkmystatusResult = jsonObject.getJSONObject("checkmystatusResult");
                String status = checkmystatusResult.getString("status");
                if (status.equals("00")) {
                    String res_code = checkmystatusResult.getString("resp_code");
                    if (res_code.equals("00")) {
                        balance.setText(checkmystatusResult.getString("avail_bal"));
                        BalancePref.setWalletAmount(checkmystatusResult.getString("avail_bal"));
                        BalancePref.setAvailableAmount(new Decimalformate().decimalFormate(checkmystatusResult.getString("avail_bal")));
                        BalancePref.setDepositeProgress("0.0");
                        BalancePref.setHoldAmount("0.0");
                    } else if (res_code.equals("NB")) {
                       // ((MainActivity) getActivity()).setDrawaerEnable(false);
                        try {
                            Negativebalancefragment negativebalancefragment = new Negativebalancefragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment, negativebalancefragment)
                                    .commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (res_code.equals("UA")) {
                       // ((MainActivity) getActivity()).setDrawaerEnable(false);
                        try {
                            Unauthorizeuser blockedfragment = new Unauthorizeuser();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment, blockedfragment)
                                    .commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (res_code.equals("SS")) {
                       // ((MainActivity) getActivity()).setDrawaerEnable(false);
                        try {
                            SuspendedUserFragment blockedfragment = new SuspendedUserFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment, blockedfragment)
                                    .commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        bottomNavigation.setCurrentItem(0);

        super.onResume();
    }
}
