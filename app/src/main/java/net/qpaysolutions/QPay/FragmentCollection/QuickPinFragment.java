package net.qpaysolutions.QPay.FragmentCollection;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Billpayment.QuickPayActivity;
import net.qpaysolutions.QPay.Utils.Utility;

/**
 * Created by deadlydragger on 8/18/16.
 */
public class QuickPinFragment extends Fragment implements AdapterView.OnItemClickListener {
    LinearLayout empty_quick;
    private ListView listView;
    private QPayCustomerDatabase QPayCustomerDatabase;
    Cursor todoCursor;
    private SQLiteDatabase db;
    int count;
    QuickPayAdapter quickPayAdapter;
    TextView total_quick;
    String quick_code,quick_number;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();
        db = QPayCustomerDatabase.getWritableDatabase();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quick_pay,container,false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity)getActivity()). setToolbar_text("Quick Pay");

        ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);
        empty_quick=(LinearLayout)view.findViewById(R.id.empty_quick);
        listView=(ListView)view.findViewById(R.id.list_quick);
        total_quick=(TextView)view.findViewById(R.id.total_quick);
        todoCursor = db.rawQuery("SELECT *  FROM quick_pay order by quick_date desc ", null);
        count = todoCursor.getCount();
        if (count > 0){
            empty_quick.setVisibility(View.GONE);
            quickPayAdapter = new QuickPayAdapter(getContext(), todoCursor,QuickPinFragment.this);
            listView.setAdapter(quickPayAdapter);
            listView.setOnItemClickListener(this);
            total_quick.setText(count+" Quick Pay Account");
            Log.d(Utility.TAG,"total quick account : ");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment,najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id",1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent= new Intent(getActivity(), QuickPayActivity.class);
        intent.putExtra("_id",String.valueOf(id));
        startActivity(intent);

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_notification, menu);
        if (count > 0){
            inflater.inflate(R.menu.menu_notification,menu);

        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notification_delet:
                Cursor todoCursor = db.rawQuery("SELECT *  FROM quick_pay order by quick_date desc ", null);
                count = todoCursor.getCount();
                total_quick.setText(count + " Quick Pay Account");
                if (count > 0) {
                    deletQuickPay();
                } else {
                    empty_quick.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), "You don't have any Quick pay account yet.", Toast.LENGTH_LONG).show();
                }
                break;
        }
        return true;
    }


    public void deletQuickPay() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title=(TextView)v.findViewById(R.id.text_title);
        final ImageView img_icon =(ImageView)v.findViewById(R.id.img_icon);
        img_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning));
        img_icon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.color_yellow), android.graphics.PorterDuff.Mode.MULTIPLY);
        title.setText("Are You Sure?");
        dialog_balance.setText("Do you want to delete all list ?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    QPayCustomerDatabase.deletQuickpay();
                    Cursor newCursor = db.rawQuery(" select * from pin ", null);
                    quickPayAdapter.changeCursor(newCursor);
                    quickPayAdapter.notifyDataSetChanged();
                    if (newCursor.getCount() > 0){

                    }else {
                        empty_quick.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


/*
    public void deletQuickPay() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Won't be able to recover this list again!")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        sDialog.hide();
                      *//*  sDialog.setTitleText("Cancelled!")
                                .setContentText("Your pin list is safe.)")
                                .setConfirmText("OK")
                                .showCancelButton(false)
                                .setCancelClickListener(null)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);*//*
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        try {
                            qpayDatabases.deletQuickpay();
                            Cursor newCursor = db.rawQuery(" select * from pin ", null);
                            quickPayAdapter.changeCursor(newCursor);
                            quickPayAdapter.notifyDataSetChanged();
                            if (newCursor.getCount() > 0){

                            }else {
                                empty_quick.setVisibility(View.VISIBLE);
                            }
                            sDialog.setTitleText("Deleted!")
                                    .setContentText("Your quick pay list has been deleted!")
                                    .setConfirmText("OK")
                                    .showCancelButton(false)
                                    .setCancelClickListener(null)
                                    .setConfirmClickListener(null)
                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                })
                .show();
    }*/

    public void deletpin(String id){
        try {
            QPayCustomerDatabase.deletquickIteam(id);
            todoCursor.requery();
            quickPayAdapter.notifyDataSetChanged();
            int count = 0;
            Cursor todoCursor = db.rawQuery("SELECT *  FROM quick_pay order by quick_date desc ", null);
            count = todoCursor.getCount();
            if (count > 0){
                total_quick.setText(count + " Quick pay accounts");
            }else {
                empty_quick.setVisibility(View.VISIBLE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void custumeditTitle(final String id,String quick_code, String number){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_edittitle ,null);
        final EditText enter_title = (EditText)v.findViewById(R.id.enter_title);
        final ImageView logo_setname = (ImageView)v.findViewById(R.id.logo_setname);
        final TextView provider_name = (TextView)v.findViewById(R.id.provider_name);
        final TextView provider_cell= (TextView)v.findViewById(R.id.provider_cell);
        if (quick_code.equals("201")){
            logo_setname.setImageResource(R.drawable.ncell);
            provider_name.setText("Ncell");
        } else if (quick_code.equals("203")) {
            provider_name.setText("NTC Prepaid");
            logo_setname.setImageResource(R.drawable.nct_logo);
        } else if (quick_code.equals("205")) {
            provider_name.setText("NTC Postpaid");
            logo_setname.setImageResource(R.drawable.nct_logo);
        } else if (quick_code.equals("204")) {
            provider_name.setText("ADSL Landline");
            logo_setname.setImageResource(R.drawable.adsl);
        } else if (quick_code.equals("206")) {
            provider_name.setText("UTL");
            logo_setname.setImageResource(R.drawable.utl);
        } else if (quick_code.equals("301")) {
            provider_name.setText("Subisu");
            logo_setname.setImageResource(R.drawable.subisu);

        } else if (quick_code.equals("304")) {
            provider_name.setText("ADSL Unlimited");
            logo_setname.setImageResource(R.drawable.adsl);

        } else if (quick_code.equals("305")) {
            provider_name.setText("ADSL Volume");
            logo_setname.setImageResource(R.drawable.adsl);

        } else if (quick_code.equals("306")) {
            provider_name.setText("DishHome");
            logo_setname.setImageResource(R.drawable.dishhome);
        }
        provider_cell.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (enter_title.getText().toString().trim().length()>0){
                    try {
                        dialog.dismiss();
                        QPayCustomerDatabase.updateTitle(enter_title.getText().toString(),id);
                        QuickPinFragment quickPinFragment = new QuickPinFragment();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragment,quickPinFragment)
                                .commit();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    enter_title.setError("Enter title");
                }


            }
        });
        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

}
