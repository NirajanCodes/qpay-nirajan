package net.qpaysolutions.QPay.FragmentCollection;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import net.qpaysolutions.QPay.R;
import java.util.ArrayList;

/**
 * Created by deadlydragger on 9/18/17.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {
    ArrayList<ContactModel> contactModels;
    ContactUsFragment contactFragment;

    public ContactAdapter(ArrayList<ContactModel> contactModels, ContactUsFragment contactFragment) {
        this.contactModels = contactModels;
        this.contactFragment = contactFragment;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item,parent,false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        holder.contact_item.setText(contactModels.get(position).getPhone());
        holder.contact_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactFragment.phoneCall(contactModels.get(position).getPhone());
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return contactModels.size();
    }

    public class  ContactViewHolder extends RecyclerView.ViewHolder{
    public TextView contact_item;

        public ContactViewHolder(View itemView) {
            super(itemView);
            contact_item=(TextView)itemView.findViewById(R.id.contact_item);
        }
    }

}
