package net.qpaysolutions.QPay.FragmentCollection;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

/**
 * Created by deadlydragger on 8/21/16.
 */
public class QuickPayAdapter extends CursorAdapter {
    QuickPinFragment notificationViewer;

    public QuickPayAdapter(Context context, Cursor c, QuickPinFragment notificationViewer) {
        super(context, c, 0);
        this.notificationViewer = notificationViewer;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_iteam_quick_pay, parent, false);
    }
    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final TextView title = (TextView) view.findViewById(R.id.title);
        final TextView amount = (TextView) view.findViewById(R.id.amount_card);
        TextView phone_quick = (TextView) view.findViewById(R.id.phone_quick);
        TextView trash = (TextView) view.findViewById(R.id.trash);
        TextView date = (TextView) view.findViewById(R.id.date);
        ImageView logo = (ImageView) view.findViewById(R.id.logo);
        TextView edit_name = (TextView) view.findViewById(R.id.edit_name);
        try {
            final String quick_number = cursor.getString(cursor.getColumnIndexOrThrow("quick_number"));
            final String date_database = cursor.getString(cursor.getColumnIndexOrThrow("quick_code"));
            String amount_db = cursor.getString(cursor.getColumnIndexOrThrow("quick_date"));
            final String id = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
            final String quick_title = cursor.getString(cursor.getColumnIndexOrThrow("quick_title"));
            if (quick_title != null){
                edit_name.setText(quick_title);
            }else {
                edit_name.setText("Set Name");
            }
            phone_quick.setText(quick_number);
            date.setText("Added:"+amount_db);
            Log.d(Utility.TAG, "bill pay code  " + date_database);
            if (date_database.equals("201")) {
                title.setText("Ncell Recharge");
                logo.setImageResource(R.drawable.ncell);
            } else if (date_database.equals("203")) {
                title.setText("NTC Prepaid Recharge");
                logo.setImageResource(R.drawable.nct_logo);
            } else if (date_database.equals("205")) {
                title.setText("NTC Postpaid Recharge");
                logo.setImageResource(R.drawable.nct_logo);
            } else if (date_database.equals("204")) {
                title.setText("NTC Landline Recharge");
                logo.setImageResource(R.drawable.adsl);
            } else if (date_database.equals("206")) {
                title.setText("UTL Recharge");
                logo.setImageResource(R.drawable.utl);
            } else if (date_database.equals("301")) {
                title.setText("Subisu");
                logo.setImageResource(R.drawable.subisu);
            } else if (date_database.equals("304")) {
                title.setText("ADSL Unlimited");
                logo.setImageResource(R.drawable.adsl);
            } else if (date_database.equals("305")) {
                title.setText("ADSL Volume");
                logo.setImageResource(R.drawable.adsl);
            } else if (date_database.equals("306")) {
                title.setText("DishHome");
                logo.setImageResource(R.drawable.dishhome);
            }
            trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notificationViewer.deletpin(id);
                }
            });
            edit_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notificationViewer.custumeditTitle(id,date_database,quick_number);
                    Log.d(Utility.TAG,"title set : " + id+ date_database+quick_number);
//                    Toast.makeText(context,"click", Toast.LENGTH_LONG).show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
