package net.qpaysolutions.QPay.FragmentCollection;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Card.CardProfileMain;

/**
 * Created by deadlydragger on 9/2/16.
 */
public class Negativebalancefragment extends Fragment implements View.OnClickListener {
    Button goto_pay;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Negative Balance");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.negative_bill,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        goto_pay=(Button)view.findViewById(R.id.goto_pay);
        goto_pay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.goto_pay:
                try {
                    CardProfileMain cardProfileMain = new CardProfileMain();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment,cardProfileMain)
                            .commit();

                }catch (Exception e){
                    e.printStackTrace();
                }
        }
    }
}
