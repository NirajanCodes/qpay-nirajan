package net.qpaysolutions.QPay.FragmentCollection;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 8/18/16.
 */
public class InviteFriends extends Fragment implements View.OnClickListener {
    private EditText phone_no;
    private Button send;
    private String app_id, cust_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog progressDialog;
    private ImageView contact_picker;
    private String number;
    static final int PICK_CONTACT = 1;
    private String cNumber, name;
    private TextInputLayout phone_error;
    private String name_invited;
    private Context mContext;
    private String sms;
    private static final int REQUEST = 112;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*((MainActivity) getActivity()).setToolbar_text("Invite Friend");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(false);*/
        phone_no = (EditText) view.findViewById(R.id.phone);
        send = (Button) view.findViewById(R.id.send);
        contact_picker = (ImageView) view.findViewById(R.id.contact_picker);
        phone_error = (TextInputLayout) view.findViewById(R.id.phone_error);
        send.setOnClickListener(this);
        contact_picker.setOnClickListener(this);
        phone_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 10) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(phone_no.getWindowToken(), 0);
                    phone_error.setError(null);
                }
            }
        });
        try {
            number = getArguments().getString("number");
            if (number.length() == 10) {
                phone_no.setText(number);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.invite_friends, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        app_id = QPayCustomerDatabase.getKeyAppId();
        cust_id = QPayCustomerDatabase.getCustomerID();
        name_invited = QPayCustomerDatabase.getUserNameLast() + " " + QPayCustomerDatabase.lastName();
        mContext = getContext();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send:
                try {
                    String phone = phone_no.getText().toString();
//                  sendSMS(phone, getMsg());
                    if (phone.length() == 10) {
                        new InviteFriendsasy(phone).execute();
                    } else {
                        phone_error.setError("Enter valid number");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.contact_picker:
                if (Build.VERSION.SDK_INT >= 23) {
                    MainActivity mainActivity = new MainActivity();
                    String[] PERMISSIONS = {android.Manifest.permission.READ_CONTACTS};
                    if (!mainActivity.hasPermissions(mContext, PERMISSIONS)) {
                        ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, REQUEST);
                    } else {
                        pickContact();
                    }
                } else {
                    pickContact();
                    break;
                }
        }
    }

    public String getMsg() {
        String msg = name_invited + " has sent you QPay invitation, you can accept invitation by following the links for android https://play.google.com/store/apps/details?id=net.qpaysolutions.QPay  and iOs : https://itunes.apple.com/np/app/qpay-nepal/id1127765416?mt=8";
        return msg;
    }


    public void sendSMS() {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> parts = smsManager.divideMessage(sms);
            smsManager.sendMultipartTextMessage(phone_no.getText().toString(), null, parts, null, null);
            Toast.makeText(getContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public static boolean isSimSupport(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return !(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {

                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {

                            Cursor phones = getActivity().getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            cNumber = phones.getString(phones.getColumnIndex("data1"));
                            Log.d(Utility.TAG, "number is : " + cNumber);

                        }
                        name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        if (cNumber != null && !cNumber.isEmpty()) {
                            phone_no.setText(cNumber.replace("+977", "").replace("-", "").trim());
                        }

                    }
                }
                break;
        }
    }

    public void custumdialogSucess(String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Your friend with phone number " + Html.fromHtml("<b>" + number + "</b>") + " successfully invited.");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public class InviteFriendsasy extends AsyncTask<String, String, String> {
        String phone;

        public InviteFriendsasy(String phone) {
            this.phone = phone;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(getActivity());
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progressDialog.hide();
                progressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("InviteUserResult");
//                boolean success = jsonObject1.getBoolean("success");
                String resp_code = jsonObject1.getString("resp_code");
                String status = jsonObject1.getString("status");
                String message = jsonObject1.getString("message");
                if (status.equals("00")) {
                    if (resp_code.equals("00")) {
                        sms = jsonObject1.getString("sms");
                        if (isSimSupport(getContext())) {

                            if (Build.VERSION.SDK_INT >= 23) {
                                String[] PERMISSIONS = {Manifest.permission.SEND_SMS};
                                if (!((MainActivity) getActivity()).hasSMSPermission(mContext, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, Tags.SMS);
                                } else {
                                    sendSMS();
                                }
                            } else {
                                sendSMS();
                            }
                        } else {
                            new Utility().showSnackbar(send, "No SIM card found on your device.");
                        }
                        custumdialogSucess(phone);
                        phone_no.getText().clear();
                    } else if (resp_code.equals("AI")) {
                        custumdialogfailure("Already invited", message);
                        phone_no.getText().clear();
                    } else if (resp_code.equals("AR")) {
                        custumdialogfailure("Already register", message);
                        phone_no.getText().clear();
                    }
                } else {
                    custumdialogfailure("Failed", message);
                    phone_no.getText().clear();
                }
            } catch (Exception e) {
                progressDialog.hide();
                progressDialog.dismiss();
                custumdialogfailure("Failed", "Failed due to internal server problem please try again later,Thank you.");
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String invite = null;
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("referredTo", phone);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "post json " + jsonObject);
                invite = networkAPI.sendHTTPData(Constants.INVITE_FRIENDS, jsonObject);
                Log.d(Utility.TAG, "get json : " + invite);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return invite;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().onBackPressed();
                    /*
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment,najirEnglish)
                            .commit();
                    getActivity().setTitle("QPay");*/
                    return true;
                }
                return false;
            }
        });
    }

    public void pickContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
        phone_no.setText("");
    }

}
