package net.qpaysolutions.QPay.FragmentCollection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CSharp05-user on 24/07/2016.
 */
public class ContactUsFragment extends Fragment {

    public static String FACEBOOK_URL = "https://www.facebook.com/";
    public static String FACEBOOK_PAGE_ID = "dinesh.rijal";

    ImageView mFacebook, mTwitter;
    LinearLayout mWebsiteLayout, mEmailInfoLayout, mCallPhoneLayout, mInformation;

    private GoogleApiClient client;
    private TextView url_get, phone_get, email_get;
    private String app_id, cust_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String url_contact, phone_contact, email_contact;
    private QPayProgressDialog qPayProgressDialog;
    private ArrayList<ContactModel> modelArrayList = new ArrayList<>();
    private Dialog dialog;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getWritableDatabase();
        app_id = QPayCustomerDatabase.getKeyAppId();
        cust_id = QPayCustomerDatabase.getCustomerID();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
     /*
        ((MainActivity) getActivity()).setToolbar_text("Contact Us");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(false);
        */
        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        mInformation = view.findViewById(R.id.information);


        url_get = (TextView) view.findViewById(R.id.url);
        phone_get = (TextView) view.findViewById(R.id.phone);
        email_get = (TextView) view.findViewById(R.id.email);
        new GetContactUsAsynk(app_id, cust_id, getActivity()).execute();
        mWebsiteLayout = (LinearLayout) view.findViewById(R.id.contact_us_website);
        mWebsiteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(url_contact);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        mEmailInfoLayout = (LinearLayout) view.findViewById(R.id.contact_us_email_info);
        mEmailInfoLayout.setOnClickListener(view1 -> {
            Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email_contact, null));
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{email_contact});
            startActivity(Intent.createChooser(i, "Send email"));
        });

        mCallPhoneLayout = (LinearLayout) view.findViewById(R.id.contact_us_call_phone);
        mCallPhoneLayout.setOnClickListener(view12 -> {
            custumDialogSelect(modelArrayList);
            /*    */

        });
        mFacebook = (ImageView) view.findViewById(R.id.contact_qpay_facebook_id);
        mFacebook.setOnClickListener(view13 -> {

            Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
            String facebookUrl = getFacebookPageURL(getActivity());
            facebookIntent.setData(Uri.parse(facebookUrl));
            startActivity(facebookIntent);
        });
        (mTwitter = (ImageView) view.findViewById(R.id.contact_qpay_twitter_id)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://twitter.com"));
                    startActivity(intent);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://twitter.com")));
                }
            }
        });
    }

    public void phoneCall(String no) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", no, null));
        startActivity(intent);
        dialog.dismiss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contact_us, container, false);
    }

    public void custumDialogSelect(ArrayList<ContactModel> contactModelArrayList) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_select_phone, null);
        final RecyclerView select_phone_recycle = (RecyclerView) v.findViewById(R.id.select_phone_recycle);
        select_phone_recycle.setHasFixedSize(true);
        select_phone_recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        final ContactAdapter contactAdapter = new ContactAdapter(contactModelArrayList, ContactUsFragment.this);
        select_phone_recycle.setAdapter(contactAdapter);
        builder.setView(v);
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(view -> dialog.dismiss());
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getActivity().getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public String getFacebookPageURL(Context context){
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    public class GetContactUsAsynk extends AsyncTask<String, String, String> {
        String app_id, cust_id;
        Activity activity;

        public GetContactUsAsynk(String app_id, String cust_id, Activity activity) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.activity = activity;
        }

        @Override
        protected String doInBackground(String... uri) {
            NetworkAPI httpResponse = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String resukt = "";
            try {
                jsonObject.put("appId", app_id);
                jsonObject.put("id", cust_id);
                jsonObject.put("lat", ((CustomerApplication) activity.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) activity.getApplication()).getLng());
                resukt = httpResponse.sendHTTPData(Constants.GET_CONTACT_US, jsonObject);
                Log.d("dinesh", "doInBackground: " + resukt);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resukt;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            phone_get.setVisibility(View.VISIBLE);

            try {
                JSONObject jsonObject1 = new JSONObject(s);
                JSONObject jsonObject = jsonObject1.getJSONObject("GetMobileContentResult");
                boolean Success = jsonObject.getBoolean("Success");
                JSONObject Data = jsonObject.getJSONObject("Data");
                if (Success == true) {
                    String url = Data.getString("WebUrl");
                    String TermOfUseUrl = Data.getString("TermOfUseUrl");
                    String PrivacyUrl = Data.getString("PrivacyUrl");
                    String About = Data.getString("About");
                    JSONArray phone_object = Data.getJSONArray("Phone");
                    JSONArray Email_object = Data.getJSONArray("Email");
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < phone_object.length(); i++) {
                        ContactModel contact = new ContactModel();
                        String phone = phone_object.getString(i);
                        contact.setPhone(phone);
                        stringBuilder.append(phone + "\n");
                        phone_contact = phone;
                        Log.d(Utility.TAG, "phone : " + phone);
                        modelArrayList.add(contact);
                    }
                    phone_get.setText(stringBuilder);
                    for (int i = 0; i < Email_object.length(); i++) {
                        String email = Email_object.getString(0);
                        Log.d(Utility.TAG, "mail : " + email);
                        email_get.setText(email);
                        email_contact = email;
                    }
                    url_contact = url;
                    url_get.setText(url);
                    Log.d(Utility.TAG, "termurl : " + TermOfUseUrl + " privacy : " + PrivacyUrl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            phone_get.setVisibility(View.GONE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                getActivity().onBackPressed();
                /*MainFragment najirEnglish = new MainFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment, najirEnglish);
                Bundle bundle = new Bundle();
                bundle.putInt("id", 1);
                najirEnglish.setArguments(bundle);
                fragmentTransaction.commit();
              *//*  getFragmentManager().beginTransaction()
                        .replace(R.id.fragment,najirEnglish)
                        .commit();*//*
                getActivity().setTitle("QPay");*/
                return true;
            }
            return false;
        });
    }


}