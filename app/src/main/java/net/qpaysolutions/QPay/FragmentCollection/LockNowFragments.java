package net.qpaysolutions.QPay.FragmentCollection;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 8/21/16.
 */
public class LockNowFragments extends Fragment {
    private net.qpaysolutions.QPay.CustumClasses.PassCodeView PassCodeView;
    TextView promptView;
    private QPayCustomerDatabase QPayCustomerDatabase;
    String pin;
    LinearLayout passcode_background;
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        pin= QPayCustomerDatabase.getPin();
//        qpayDatabases.updateFlagPin(" '2' ");
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.set_passcode_main,container,false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
        PassCodeView = (net.qpaysolutions.QPay.CustumClasses.PassCodeView) view.findViewById(R.id.set_passcode);
        promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        PassCodeView.setTypeFace(typeFace);
        PassCodeView.setKeyTextColor(R.color.black_shade);
        promptView.setTypeface(typeFace);
        bindEvents();
        promptView.setText("Enable Passcode");
        getActivity().setTitle("Lock Now");
    }
    private void bindEvents() {
        PassCodeView.setOnTextChangeListener(new net.qpaysolutions.QPay.CustumClasses.PassCodeView.TextChangeListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onTextChanged(String text) {
                try {
                    if (text.length() == 4) {
                       if (text.equals(pin)){
                           QPayCustomerDatabase.updateFlagPin(" '1' ");
//                           System.exit(0);
                           System.runFinalizersOnExit(true);
                           getActivity().finishAffinity();
                       }else {
                           PassCodeView.setError(true);
                       }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        getView().requestFocus();
        getView().setFocusableInTouchMode(false);

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    try {
                        MainFragment najirEnglish = new MainFragment();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragment, najirEnglish)
                                .commit();
                        getActivity().setTitle("QPay");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    return true;
                }
                return false;
            }
        });
    }

}
