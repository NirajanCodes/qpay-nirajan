package net.qpaysolutions.QPay.FragmentCollection;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;

import org.json.JSONObject;

/**
 * Created by CSharp05-user on 26/07/2016.
 */

public class AboutUsFragment extends Fragment {
    TextView mTermsUse, mPrivacyPolicy, About_us;
    String termofuse, privecypolicy, about;
    private String app_id, cust_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private View view;
    private QPayProgressDialog qPayProgressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getWritableDatabase();
        app_id = QPayCustomerDatabase.getKeyAppId();
        cust_id = QPayCustomerDatabase.getCustomerID();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_us, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      /*  ((MainActivity) getActivity()).setToolbar_text("About Us");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(false);*/
        mTermsUse = (TextView) view.findViewById(R.id.about_us_terms_of_use_id);
        About_us = (TextView) view.findViewById(R.id.About);
        mPrivacyPolicy = (TextView) view.findViewById(R.id.about_us_privacy_policy_id);
        new GetContactUsAsynk(app_id, cust_id, getActivity()).execute();
    }

    public class GetContactUsAsynk extends AsyncTask<String, String, String> {
        String app_id, cust_id;
        Activity activity;

        public GetContactUsAsynk(String app_id, String cust_id, Activity activity) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.activity = activity;
        }

        @Override
        protected String doInBackground(String... uri) {
            NetworkAPI httpResponse = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String resukt = "";
            try {

                jsonObject.put("appId", app_id);
                jsonObject.put("id", cust_id);
                jsonObject.put("lat", ((CustomerApplication) activity.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) activity.getApplication()).getLng());
                Log.d("dinesh", "doInBackground: "+jsonObject.toString());
                resukt = httpResponse.sendHTTPData(Constants.GET_CONTACT_US, jsonObject);
                Log.d("dinesh", "doInBackground: "+resukt);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resukt;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject1 = new JSONObject(s);
                JSONObject jsonObject = jsonObject1.getJSONObject("GetMobileContentResult");
                boolean Success = jsonObject.getBoolean("Success");
                JSONObject Data = jsonObject.getJSONObject("Data");
                if (Success == true) {
                    String url = Data.getString("WebUrl");
                    final String TermOfUseUrl = Data.getString("TermOfUseUrl");
                    final String PrivacyUrl = Data.getString("PrivacyUrl");
                    final String About = Data.getString("About");

                    mTermsUse.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Uri uri = Uri.parse(TermOfUseUrl);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    });


                    mPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Uri uri = Uri.parse(PrivacyUrl);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    });
                    About_us.setText(About);

                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog=new QPayProgressDialog(getActivity());
            qPayProgressDialog.show();

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                  /*  MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();*/
                    getActivity().onBackPressed();
                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;
            }
        });
    }
}
