package net.qpaysolutions.QPay.Firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import net.qpaysolutions.QPay.Utils.CustomerApplication;

/**
 * Created by deadlydragger on 12/8/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
    }
}