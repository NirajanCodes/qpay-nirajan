package net.qpaysolutions.QPay.Firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import net.qpaysolutions.QPay.Billpayment.BillPayActivity;
import net.qpaysolutions.QPay.Cache.AppCache;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.Message;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.User;
import net.qpaysolutions.QPay.Chat.library.AppData;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by deadlydragger on 12/8/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "dinesh";
    private QPayCustomerDatabase QPayCustomerDatabase;
    String au;
    static NotificationAlert notificationAlert;
    public static final String MY_PREFS_NAME = "token";
    SharedPreferences sharePreference;
    String refreshedToken;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> data = remoteMessage.getData();
            if (remoteMessage.getData().size() > 0) {

                String title = data.get("title");
                String msg = data.get("body");
                JSONObject jsonObject = new JSONObject(msg);
                String Messg = jsonObject.getString("Message");

                Log.d(TAG, "onMessageReceived: " + data.toString());
                if (title.equals("QpayChat")) {
                    if (((CustomerApplication) getApplication()).isActivityVisible()) {
                        handleDataMessage(remoteMessage.getData().toString());
                    } else {
                        JSONObject jsonObjectt = new JSONObject(remoteMessage.getData().toString());
                        JSONObject body = jsonObjectt.getJSONObject("body");
                        String Message = body.getString("Message");
                        saveImageUrl(remoteMessage.getData().toString());
                        sendNotificationChat(Message, body.getString("SenderName"), remoteMessage.getData().toString());
                    }
                } else {
                    sendNotification(Messg, title);
                }

            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            refreshedToken = instanceIdResult.getToken();
            sharePreference = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharePreference.edit();
            editor.putString("fcm_token", refreshedToken);
            editor.apply();
            String first_sisteen_char_token = refreshedToken.substring(0, Math.min(refreshedToken.length(), 16));
            ((CustomerApplication)getApplication()).setDevtokenVariable(first_sisteen_char_token);
        });
    }


    private void saveImageUrl(String s) {
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONObject body = jsonObject.getJSONObject("body");
            boolean isImage = body.getBoolean("IsImage");
            final String url = body.getString("Message");
            final String phoneNo = body.getString("PhoneNo");
            String senderName = body.getString("SenderName");
            String senderId = body.getString("SenderId");
            String messageID = body.getString("MessageId");

            if (isImage) {
                AppCache.getMessageList().clear();
                User user;
                try {
                    user = AppCache.getmUsers().get(1);
                } catch (Exception e) {
                    user = new User(messageID, senderName, null);
                }
                Message message = new Message.Builder()
                        .setUsernameVisibility(false)
                        .hideIcon(false)
                        .setRightMessage(false)
                        .setUser(user)
                        .setIsBitmap(false)
                        .setImageUrl(url)
                        .setMessageText(Message.Type.PICTURE.name())
                        .setType(Message.Type.PICTURE)
                        .build();

                AppCache.setMessageList(AppData.getMessageList(CustomerApplication.getContext(), phoneNo));
                AppCache.getMessageList().add(message);

                AppData.putMessageList(CustomerApplication.getContext(),
                        AppCache.getMessageList(), String.valueOf(phoneNo));

            } else {
                AppCache.getMessageList().clear();
                User user;
                try {
                    user = AppCache.getmUsers().get(1);
                } catch (Exception e) {
                    user = new User(messageID, senderName, null);
                }
                Message receivedMessage = new Message.Builder()
                        .setUsernameVisibility(false)
                        .hideIcon(false)
                        .setUser(user)
                        .setRightMessage(false)
                        .setMessageText(url)
                        .build();

                AppCache.setMessageList(AppData.getMessageList(CustomerApplication.getContext(), phoneNo));
                AppCache.getMessageList().add(receivedMessage);

                AppData.putMessageList(CustomerApplication.getContext(),
                        AppCache.getMessageList(), String.valueOf(phoneNo));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void handleDataMessage(String message) {
        try {

            JSONObject jsonObject = new JSONObject(message);
            JSONObject body = jsonObject.getJSONObject("body");
            String Message = body.getString("Message");
            String ReceiverId = body.getString("ReceiverId");
            String SenderId = body.getString("SenderId");
            String MessageId = body.getString("MessageId");
            String Date = body.getString("Date");
            final String PhoneNo = body.getString("PhoneNo");
            String imageUrl = body.getString("ImageUrl");
            DateFormat df = new SimpleDateFormat("hh:mm:ss a");
            boolean isImage = body.getBoolean("IsImage");
            String NoOfUnreadMsg = body.getString("NoOfUnreadMsg");
            String date = df.format(Calendar.getInstance().getTime());
            String name = body.getString("SenderName");
            boolean IsImage = body.getBoolean("IsImage");

            Intent intent = new Intent("custom-event-name");
            intent.putExtra("MessageId", MessageId);
            intent.putExtra("Message", Message);
            intent.putExtra("SenderId", SenderId);
            intent.putExtra("PhoneNumber", PhoneNo);
            intent.putExtra("date", date);
            intent.putExtra("TotalUnreadMsg", "0");
            intent.putExtra("ImageUrl", imageUrl);
            intent.putExtra("isImage", isImage);
            intent.putExtra("messageid", MessageId);
            intent.putExtra("NoOfUnreadMsg", NoOfUnreadMsg);
            intent.putExtra("SenderName", name);
            intent.putExtra("Check", 8);
            intent.putExtra("ReceiverId", ReceiverId);

            if (IsImage) {
                intent.putExtra("IsImage", "1");
            } else {
                intent.putExtra("IsImage", "0");
            }
            sendBroadcast(intent);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(String message, String title) {

        QPayCustomerDatabase = new QPayCustomerDatabase(this);
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();
        au = QPayCustomerDatabase.getFlag();
        DateFormat df = new SimpleDateFormat("E,MMM d, yyyy HH:mm a");
        String date = df.format(Calendar.getInstance().getTime());
        date = date.replace("+", "");
        Log.d(Utility.TAG, "date in 24 hour format : " + date);
        QPayCustomerDatabase.insertNotification(message, date, title);
       /* SharedPreferences sharedPreferences = getSharedPreferences("key_intent",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("key_intent",3);
        editor.commit();*/
        Intent intent = new Intent(this, BillPayActivity.class);
        intent.putExtra("Check", 3);
        intent.putExtra("id", "notif");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this, CustomerApplication.CHANNEL_1_ID)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setContentTitle(title)
                .setSmallIcon(getNotificationIcon(), 2)
                .setContentText(message)
                .setPriority(Notification.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            noBuilder.setColor(getResources().getColor(R.color.colorPrimary));
        }
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;
        noBuilder.setDefaults(defaults);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, noBuilder.build());

    }

    private void sendNotificationChat(String message, String title, String data) {

        try {

            JSONObject jsonObject = new JSONObject(data);
            JSONObject body = jsonObject.getJSONObject("body");
            String Message = body.getString("Message");
            final String PhoneNo = body.getString("PhoneNo");
            boolean IsImage = body.getBoolean("IsImage");
            String name = body.getString("SenderName");
            String MessageId = body.getString("MessageId");

            if (IsImage) {

                Intent intent = new Intent("image_chat");
                intent.putExtra("PhoneNumber", PhoneNo);
                intent.putExtra("Message", Message);
                intent.putExtra("MessageId", MessageId);
                intent.putExtra("SenderName", name);
                sendBroadcast(intent);

            }

            DateFormat df = new SimpleDateFormat("E,MMM d, yyyy HH:mm a");
            String date = df.format(Calendar.getInstance().getTime());
            date = date.replace("+", "");
            Log.d(Utility.TAG, "date in 24 hour format : " + date);
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("Check", 8);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            int requestCode = 0;
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder noBuilder;
            if (IsImage) {
                noBuilder = new NotificationCompat.Builder(this, CustomerApplication.CHANNEL_1_ID)
                        .setContentTitle(title)
                        .setSmallIcon(R.drawable.ic_high_res_icon, 1)
                        //.setSmallIcon(R.drawable.ic_high_res_icon)
                        .setContentText("You Received an Image.")
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);
            } else {
                noBuilder = new NotificationCompat.Builder(this, CustomerApplication.CHANNEL_1_ID)
                        .setContentTitle(title)
                        .setSmallIcon(R.drawable.ic_high_res_icon)
                        .setContentText(message)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);
            }

            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;
            noBuilder.setDefaults(defaults);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, noBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface NotificationAlert {
        void alert(Intent intent);
    }

    public static void setListener(NotificationAlert notificationAlert1) {
        notificationAlert = notificationAlert1;
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O);
        return useWhiteIcon ? R.drawable.ic_high_res_icon_bw : R.drawable.ic_high_res_icon;
    }
}