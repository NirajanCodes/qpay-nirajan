package net.qpaysolutions.QPay.NearByTaxi;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 1/21/18.
 */
public class TaxiAdapter extends RecyclerView.Adapter<TaxiAdapter.ViewHolder> {
    ArrayList<Taxi> itemsData;
    NearTaxiActivity nearTaxiActivity;


    public TaxiAdapter(ArrayList<Taxi> itemsData,NearTaxiActivity nearTaxiActivity) {
        this.itemsData = itemsData;
        this.nearTaxiActivity=nearTaxiActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.txtViewTitle.setText(itemsData.get(position).getMerch_name());
        viewHolder.distance.setText(itemsData.get(position).getDistance());
        viewHolder.number.setText(itemsData.get(position).getCell_phone());
        viewHolder.makeCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + itemsData.get(position).getCell_phone()));
                if (ActivityCompat.checkSelfPermission(nearTaxiActivity, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    nearTaxiActivity.startActivity(callIntent);
                }else {
                    nearTaxiActivity.requestCallPermission(itemsData.get(position).getCell_phone());
                }

            }
        });


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtViewTitle, distance, number;
        private ImageView makeCall;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            distance = (TextView) itemLayoutView.findViewById(R.id.item_distance);
            number = (TextView) itemLayoutView.findViewById(R.id.item_number);
            makeCall = (ImageView) itemLayoutView.findViewById(R.id.item_makecall);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }


}