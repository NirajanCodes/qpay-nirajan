package net.qpaysolutions.QPay.NearByTaxi;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.NearByShop.NearbyShopListActivity;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by deadlydragger on 7/14/16.
 */
public class NearTaxiActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private ArrayList<Taxi> listTaxi = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private TaxiAdapter myAdapter;
    private String TAG = "dinesh";
    private int taxiCount;
    private TextView taxi_near_you;
    private String dev_toke;
    private String custId, app_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String address, city;
    private TextView my_location;
    private TextView size;
    private FusedLocationProviderClient mFusedLocationClient;
    private String phoneCall;
    private FrameLayout progressLayout;
    private boolean showDialog = true;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    LocationManager lm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        taxi_near_you = (TextView) findViewById(R.id.taxi_near_you);
        my_location = (TextView) findViewById(R.id.my_location);
        size = (TextView) findViewById(R.id.size);
        size.setText("Taxi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText("Nearby Taxi");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        QPayCustomerDatabase = new QPayCustomerDatabase(this);
        QPayCustomerDatabase.getReadableDatabase();
        custId = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        dev_toke = ((CustomerApplication) this.getApplication()).getDevtokenVariable();
        progressLayout = (FrameLayout) findViewById(R.id.progressBar);
        progressLayout.setVisibility(View.VISIBLE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
    }

    private void Map() {
        try {
            if (googleMap == null) {
                MapFragment mapFragment = (MapFragment) getFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setMap(googleMap);
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give location permission.", Toast.LENGTH_LONG).show();
                }

                break;
            case Tags.CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callToTaxi();

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give phone permission.", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMapTaxi) {
        googleMap = googleMapTaxi;
        progressLayout.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!Utility.hasExternalStoragePermission(NearTaxiActivity.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(NearTaxiActivity.this, PERMISSIONS, Tags.LOCATION);
            } else {
                setMap(googleMapTaxi);
            }
        } else {
            setMap(googleMapTaxi);
        }
    }

    private void callToTaxi() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneCall));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            startActivity(callIntent);
        }
    }

    public void requestCallPermission(String phone) {
        phoneCall = phone;
        String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};

        ActivityCompat.requestPermissions(NearTaxiActivity.this, PERMISSIONS, Tags.CALL);
    }

    private void setMap(final GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        mFusedLocationClient.getLastLocation()
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                if (location != null) {
                                    LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                    LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                    new AddressCurrentAddress(LatLngPref.getLat(), LatLngPref.getLng()).execute();
                                    new TaxiNearby(googleMap).execute();
                                        /*} else if (Build.VERSION.SDK_INT >= 23) {
                                            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                                            if (!Utility.hasExternalStoragePermission(NearTaxiActivity.this, PERMISSIONS)) {
                                                ActivityCompat.requestPermissions(NearTaxiActivity.this, PERMISSIONS, Tags.LOCATION);
                                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                                startActivity(intent);
                                            }*/
                                } else if (!gps_enabled && !network_enabled) {
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }
                            }


                        }
                );
    }

    public class AddressCurrentAddress extends AsyncTask<String, String, String> {
        String latitude, longitude;

        public AddressCurrentAddress(String latitude, String longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        protected String doInBackground(String... params) {

            return getAdress(Double.valueOf(latitude), Double.valueOf(longitude));
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            my_location.setText(s);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  Intent intent = new Intent(NearTaxiActivity.this, MainActivity.class);
        intent.putExtra("Check",6);
        startActivity(intent);*/
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showDialog = true;
        Map();
    }

    @Override
    protected void onPause() {
        super.onPause();
        showDialog = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showDialog = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        showDialog = false;
    }

    protected void createMarker(GoogleMap googleMap, double latitude, double longitude) {

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker))
        );
    }

    public class TaxiNearby extends AsyncTask<String, String, String> {
        GoogleMap googleMap;

        public TaxiNearby(GoogleMap googleMap) {
            this.googleMap = googleMap;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String taxi_result = null;
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", custId);
                jsonObject.put("dev_token", dev_toke);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                taxi_result = networkAPI.sendHTTPData(Constants.GET_TAXI_NEAR, jsonObject);
                Log.d(Utility.TAG, "post data : " + jsonObject.toString());
                Log.d(Utility.TAG, "taxi result :" + taxi_result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return taxi_result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressLayout.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("GetNearByTaxisResult");
                String status = jsonObject1.getString("status");
                JSONArray taxiDetails = jsonObject1.getJSONArray("taxiDetails");
                if (status.equals("00")) {
                    for (int i = 0; i < taxiDetails.length(); i++) {
                        Taxi taxi = new Taxi();
                        JSONObject jsonObject2 = taxiDetails.getJSONObject(i);
                        String cell_phone = jsonObject2.getString("cell_phone");
                        String distance = jsonObject2.getString("distance");
                        double loc_lat = jsonObject2.getDouble("loc_lat");
                        double loc_lng = jsonObject2.getDouble("loc_lng");
                        String merch_name = jsonObject2.getString("merch_name");
                        taxi.setCell_phone(cell_phone);
                        taxi.setDistance(new Decimalformate().decimalFormate(distance));
                        taxi.setLoc_lat(loc_lat);
                        taxi.setLoc_lng(loc_lng);
                        taxi.setMerch_name(merch_name);
                        listTaxi.add(taxi);
                        createMarker(googleMap, listTaxi.get(i).getLoc_lat(), listTaxi.get(i).getLoc_lng());
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(listTaxi.get(0).getLoc_lat(), listTaxi.get(0).getLoc_lng())).zoom(12).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                    myAdapter = new TaxiAdapter(listTaxi, NearTaxiActivity.this);
                    taxiCount = listTaxi.size();
                    taxi_near_you.setText(taxiCount + "");
                    Log.d(Utility.TAG, "taxi near you : " + taxiCount);
                    mRecyclerView.setAdapter(myAdapter);
                    mRecyclerView.addOnItemTouchListener(new NearbyShopListActivity.RecyclerTouchListener(NearTaxiActivity.this, mRecyclerView, new NearbyShopListActivity.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(listTaxi.get(position).getLoc_lat(), listTaxi.get(position).getLoc_lng())).zoom(18).build();
                            Log.d(Utility.TAG, "taxi : " + listTaxi.get(position).getLoc_lat() + " lng : " + listTaxi.get(position).getLoc_lng() + " taxi namme : " + listTaxi.get(position).getMerch_name());
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }

                        @Override
                        public void onLongClick(View view, int position) {
                        }
                    }));
                } else {
                    if (showDialog) {
                        dialogFailure("No Taxi Found!", "Taxi not found near by you.");
                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
                if (showDialog) {
                    dialogFailure("No Taxi Found!", "There are no taxi found around you..");
                }

            }

        }
    }

    public void dialogFailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(NearTaxiActivity.this);
        LayoutInflater inflater = NearTaxiActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final TextView proceed_txt = (TextView) v.findViewById(R.id.proceed_txt);
        proceed_txt.setText("OK");
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public String getAdress(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getLocality();
            return address + "," + city;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}