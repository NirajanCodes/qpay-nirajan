package net.qpaysolutions.QPay.NFC;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.CustumClasses.FontTextViewbold;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class EnterAmmountForCard extends AppCompatActivity implements View.OnClickListener {
    private Button mSendMoney;

    //    private EditText enter_ammount;
    private String cust_id, app_id;
    private TextView card_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog pDialog;
    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    String nfc_id;
    Spinner spinner;
    private String bytecode, amount;
    public static final int REQUEST_NFC_PERMISSION = 501;
    double latitude,currentLat;
    double longitude,currentLng;
    private FusedLocationProviderClient mFusedLocationClient;
    FontTextViewbold tapmessage;
    LinearLayout enter_amount_field;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_enter_money);
        nfc_id = getIntent().getStringExtra("nfc_id");
        QPayCustomerDatabase = new QPayCustomerDatabase(EnterAmmountForCard.this);
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
        pDialog = new QPayProgressDialog(EnterAmmountForCard.this);
//        initViews();
        mAdapter = NfcAdapter.getDefaultAdapter(EnterAmmountForCard.this);
        if (mAdapter == null) {


        }

        mPendingIntent = PendingIntent.getActivity(EnterAmmountForCard.this, 0,
                new Intent(EnterAmmountForCard.this, this.getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        Log.d("check", "" + cust_id + "..." + app_id+".."+mPendingIntent);
        Toast.makeText(this, "Tap nfc card back of your device again", Toast.LENGTH_SHORT).show();

        try {
            enter_amount_field.setVisibility(View.GONE);
        }catch (Exception e){

        }
        getCurrentLatLng();
    }
    private void getCurrentLatLng() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (location != null) {
                                            currentLat=location.getLatitude();
                                            currentLng=location.getLongitude();
                                        } else {

                                        }
                                    }
                                }, 500);

                            }
                        }

                );

    }

/*
    private void initViews() {
        spinner = (Spinner) findViewById(R.id.amount);
        mSendMoney = (Button) findViewById(R.id.confirm_money);
        tapmessage= findViewById(R.id.tapmessage);
        enter_amount_field= findViewById(R.id.enter_amount_field);
        mSendMoney.setOnClickListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(EnterAmmountForCard.this,
                R.array.card_topup, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                amount = parent.getItemAtPosition(position).toString();
                switch (parent.getItemAtPosition(position).toString()) {
                    case "100.00":
                        bytecode = "612";
                        break;
                    case "200.00":
                        bytecode = "606";
                        break;
                    case "500.00":
                        bytecode = "607";
                        break;
                    case "1000.00":
                        bytecode = "608";
                        break;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
*/

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_money:
                try {
                    new loadAmmountToCard(bytecode).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdapter == null) {
            //no nfc on the device
            showWirelessSettings();
        } else {
            checkPermission();
            if (!mAdapter.isEnabled()) {
                //nfc is disabled
                showWirelessSettings();
            } else {
            }
            mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
        }
    }

    public void checkPermission() {
        //TODO: check for permissions in the runtime and ask if not granted
        int permission
                = ActivityCompat.checkSelfPermission(EnterAmmountForCard.this, Manifest.permission.NFC);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            //the permission is granted, proceed
        } else {
            //the persmission is not granted, ask for permission
            ActivityCompat.requestPermissions(EnterAmmountForCard.this, new String[]{Manifest.permission.NFC}, REQUEST_NFC_PERMISSION);
        }

    }

    private void showWirelessSettings() {
        Toast.makeText(this, "You need to enable NFC", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
        Log.d("check1", "" + intent);
    }

    private void resolveIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs;

                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                byte[] payload = dumpTagData(tag).getBytes();
                Log.d("check2",""+tag);



        }else{
            Log.d("else","else");
        }
    }

    private String dumpTagData(Tag tag) {
        StringBuilder sb = new StringBuilder();
        byte[] id = tag.getId();
        sb.append("ID (hex): ").append(Utility.toHex(id)).append('\n');
        Log.d("dumpTagData",""+tag+"...."+Utility.toHex(id));
        sb.append("ID (reversed hex): ").append(Utility.toReversedHex(id)).append('\n');
        sb.append("ID (dec): ").append(Utility.toDec(id)).append('\n');
        sb.append("ID (reversed dec): ").append(Utility.toReversedDec(id)).append('\n');
        Log.d("nfc_id", "" + Utility.toDec(id)+"...."+Utility.toHex(id));
        nfc_id = String.valueOf(Utility.toDec(id));
        mSendMoney.setVisibility(View.VISIBLE);
        tapmessage.setVisibility(View.GONE);
        try {
            enter_amount_field.setVisibility(View.VISIBLE);
        }catch (Exception e){

        }
        String prefix = "android.nfc.tech.";
        sb.append("Technologies: ");
        for (String tech : tag.getTechList()) {
            sb.append(tech.substring(prefix.length()));
            sb.append(", ");
        }

        sb.delete(sb.length() - 2, sb.length());

        for (String tech : tag.getTechList()) {
            if (tech.equals(MifareClassic.class.getName())) {
                sb.append('\n');
                String type = "Unknown";

                try {
                    MifareClassic mifareTag = MifareClassic.get(tag);

                    switch (mifareTag.getType()) {
                        case MifareClassic.TYPE_CLASSIC:
                            type = "Classic";
                            break;
                        case MifareClassic.TYPE_PLUS:
                            type = "Plus";
                            break;
                        case MifareClassic.TYPE_PRO:
                            type = "Pro";
                            break;
                    }
                    sb.append("Mifare Classic type: ");
                    sb.append(type);
                    sb.append('\n');

                    sb.append("Mifare size: ");
                    sb.append(mifareTag.getSize() + " bytes");
                    sb.append('\n');

                    sb.append("Mifare sectors: ");
                    sb.append(mifareTag.getSectorCount());
                    sb.append('\n');

                    sb.append("Mifare blocks: ");
                    sb.append(mifareTag.getBlockCount());
                } catch (Exception e) {
                    sb.append("Mifare classic error: " + e.getMessage());
                }
            }

            if (tech.equals(MifareUltralight.class.getName())) {
                sb.append('\n');
                MifareUltralight mifareUlTag = MifareUltralight.get(tag);
                String type = "Unknown";
                switch (mifareUlTag.getType()) {
                    case MifareUltralight.TYPE_ULTRALIGHT:
                        type = "Ultralight";
                        break;
                    case MifareUltralight.TYPE_ULTRALIGHT_C:
                        type = "Ultralight C";
                        break;
                }
                sb.append("Mifare Ultralight type: ");
                sb.append(type);
            }
        }
        return sb.toString();
    }

    private class loadAmmountToCard extends AsyncTask<Void, Void, String> {
        String bytecode;

        private loadAmmountToCard(String bytecode) {
            this.bytecode = bytecode;
        }

        @Override
        protected void onPreExecute() {
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", app_id);
                jsonObject.put("id", cust_id);
                jsonObject.put("nfcCardId", nfc_id);
                jsonObject.put("lat", currentLat);
                jsonObject.put("lng", currentLng);
                jsonObject.put("amount", amount);
                Log.d("jsonObject",""+jsonObject);
                return NetworkAPI.sendHTTPDataPOST("https://node.qpaysolutions.net/QPay.svc/nfccardtopup", jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String args) {
            super.onPostExecute(args);
            pDialog.dismiss();
            Log.d("response",""+args);
            if (args != null) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(args));
                    if (jsonObject.get("status").equals("00")) {
                        Toast.makeText(EnterAmmountForCard.this, "" + jsonObject.get("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_NFC_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //nfc  permission granted
                } else {
                    //nfc permission not granted
                }
                break;
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getCurrentLatLng();

                } else {
                }

                break;
        }
    }
}
