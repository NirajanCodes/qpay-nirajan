package net.qpaysolutions.QPay.Register;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 8/5/16.
 */
public class CheckPhoneFragment extends Fragment implements View.OnClickListener {
    private EditText phone, confirm_phone;
    private Button submit;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog progressDialog;
    private String cust_id, app_id;
    private JSONObject jsonObject;
    private JSONObject Response;
    public static final String MY_PREFS_NAME = "token";
    private SharedPreferences sharePreference;
    private LinearLayout register_back;
    private TextInputLayout phone_first, phone_second;
    private boolean showDialog = true;

    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        phone = (EditText) view.findViewById(R.id.cell_text);
        confirm_phone = (EditText) view.findViewById(R.id.cell_text_confirm);
        register_back = (LinearLayout) view.findViewById(R.id.confirm_phone_layout);
        submit = (Button) view.findViewById(R.id.phone_submit);
        confirm_phone.addTextChangedListener(mDateEntryWatcher);
        phone_first = (TextInputLayout) view.findViewById(R.id.phone_first);
        phone_second = (TextInputLayout) view.findViewById(R.id.phone_second);
        phone.addTextChangedListener(textWatcher);
        phone.requestFocus();
        submit.setOnClickListener(this);
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                register_back.setBackground(getAssetImage(getContext(), "registration.png"));
            } else {
                register_back.setBackgroundDrawable(getAssetImage(getContext(), "registration.png"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        showkeyword();
//        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (Utility.isLocationEnabled(getContext()) == false) {
            warningDialog("GPS Disabled!", "GPS is disabled in your device. Enabling GPS is recommended to get glitch free service.");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        showDialog = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        showDialog = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        showDialog = false;
    }

    public void warningDialog(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView) v.findViewById(R.id.dialog_icon);
        title.setText(title_display);
        dialog_balance.setText(number);
        dialog_icon.setImageResource(R.drawable.ic_bill_payment_warning);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (showDialog) {
            dialog.show();
        }
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.phone_confirmation_layout, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone_submit:
                new Utility().hideSoftkey(getActivity());
                String phoneNumber = phone.getText().toString();
                try {
                    if (phoneNumber.equals(confirm_phone.getText().toString()) && phoneNumber.length() == 10
                            && phoneNumber.startsWith("9")) {
                        String android_id = Settings.Secure.getString(getContext().getContentResolver(),Settings.Secure.ANDROID_ID);
                        new CheckCellPhoneAsynkTask(phone.getText().toString(), android_id).execute();
                    } else {
                        phone_first.setError("Enter Valid Phone");
                        phone_second.setError("Enter Valid Phone");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public class CheckCellPhoneAsynkTask extends AsyncTask<String, String, String> {
        String cellphone, android_id;

        public CheckCellPhoneAsynkTask(String cellphone, String android_id) {
            this.cellphone = cellphone;
            this.android_id = android_id;
        }

        @Override
        protected String doInBackground(String... params) {
            String cell_phone_result;
            String status = null;
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject.put("cellphone", cellphone);
                jsonObject.put("new_cust_id", cust_id);
                jsonObject.put("app_id", app_id);
                jsonObject.put("deviceId", android_id);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "check cell phone post : " + jsonObject);
                cell_phone_result = networkAPI.sendHTTPData(Constants.CHECK_PHONE, jsonObject);
                Log.d(Utility.TAG, "check cell phone response : " + cell_phone_result);
                JSONObject jsonObject2 = new JSONObject(cell_phone_result);
                JSONObject jsonObject1 = jsonObject2.getJSONObject("checkphoneResult");
                status = jsonObject1.getString("status");
                return status;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(getActivity());
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                if (s != null && !s.isEmpty() && s.equals("00")) {
                    RegisterUserFragment register = new RegisterUserFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("cell_phone", phone.getText().toString());
                    bundle.putString("act_code", "");
                    register.setArguments(bundle);
                    fragmentTransaction.replace(R.id.fragment, register);
                    fragmentTransaction.commit();
                } else if (s != null && !s.isEmpty() && s.equals("A")) {
                    EnterVerificationCodeReactivationFragment fr = new EnterVerificationCodeReactivationFragment();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    Bundle args = new Bundle();
                    args.putString("cell_phone", phone.getText().toString());
                    args.putString("act_code", "");
                    fr.setArguments(args);
                    ft.replace(R.id.fragment, fr);
                    ft.commit();
                } else if (s != null && !s.isEmpty() && s.equals("R")) {
                    CheckPhoneFragment registerPhoneFragment = new CheckPhoneFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, registerPhoneFragment);
                    fragmentTransaction.commit();
                    Utility.custumdialogfailureInsufficient("Sorry!", "You recently registered in QPay account. Please try again after five minutes.", getActivity());
                } else if (s != null && !s.isEmpty() && s.equals("S")) {
                    SuspendedUserFragment fr = new SuspendedUserFragment();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    Bundle args = new Bundle();
                    args.putString("cell_phone", phone.getText().toString());
                    args.putString("act_code", "");
                    fr.setArguments(args);
                    ft.replace(R.id.fragment, fr);
                    ft.commit();
                } else if (s != null && !s.isEmpty() && s.equals("92")) {
                    phone.getText().clear();
                    confirm_phone.getText().clear();
                    Utility.custumdialogfailureInsufficient("Sorry!", "You have reached the maximum allowed registrations from your device. Please contact QPay administration for further details.", getActivity());
                } else if (s != null && !s.isEmpty() && s.equals("91")) {
                    sharePreference = getActivity().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
                    String token = sharePreference.getString("fcm_token", null);
                    new GetFirstCutIdSecond(token, cellphone, "").execute();
                } else {
                    Toast.makeText(getContext(), "Unable to process your request", Toast.LENGTH_LONG).show();
                    phone.getText().clear();
                    confirm_phone.getText().clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private TextWatcher mDateEntryWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == 10) {
                phone_first.setError(null);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(confirm_phone.getWindowToken(), 0);
            }
            if (s.length() > 10) {
                phone_second.setError("Enter Valid Phone");
            } else if (s.length() < 10) {
                phone_second.setError("Numbers not matching");
            } else {
                phone_second.setError(null);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
    };

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == 10) {
                phone_second.requestFocus();
            } else if (s.length() > 10) {
                phone_first.setError("Enter Valid Phone");
            } else {
                phone_first.setError(null);
            }
        }
    };

    public void showkeyword() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private class GetFirstCutIdSecond extends AsyncTask<Void, Void, JSONObject> {
        String token, phone, act_code;

        private GetFirstCutIdSecond(String token, String phone, String act_code) {
            this.token = token;
            this.phone = phone;
            this.act_code = act_code;
        }

        @Override
        protected void onPreExecute() {

            progressDialog = new QPayProgressDialog(getContext());
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            String line = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("dev_token", token);
                jsonObject.put("device_type", "AND");
                String data = jsonObject.toString();
                String yourURL = Constants.FIRST_GCM_CALL_URL;
                String result = new NetworkAPI().sendHTTPData(yourURL, jsonObject);
                Log.d(Utility.TAG, "Response from php = " + result);
                Response = new JSONObject(result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            super.onPostExecute(args);
            progressDialog.dismiss();
            try {
                JSONObject jsonObject1 = null;
                jsonObject1 = args.getJSONObject("getcustidResult");
                String cust_id = jsonObject1.getString("cust_id");
                String app_id = jsonObject1.getString("app_id");
                String s = jsonObject1.getString("status");
                if (s != null && !s.isEmpty() && !s.equals("null") && s.equals("00")) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    QPayCustomerDatabase.updateCustIDAppId(cust_id, app_id);
                    RegisterUserFragment fr = new RegisterUserFragment();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    Bundle argss = new Bundle();
                    argss.putString("cell_phone", phone);
                    argss.putString("act_code", act_code);
                    fr.setArguments(argss);
                    ft.replace(R.id.fragment, fr);
                    ft.commit();
                } else {
                    new Utility().showSnackbar(register_back, "Failed to get response.");
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                progressDialog.hide();
                e.printStackTrace();
                getActivity().finish();
                new Utility().showSnackbar(register_back, "Failed to get response.");
            }
        }
    }
}
