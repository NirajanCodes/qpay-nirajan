package net.qpaysolutions.QPay.Register;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Created by deadlydragger on 6/13/16.
 */
public class EnterVerificationCodeReactivationFragment extends Fragment implements View.OnClickListener, TextWatcher {
    private TextView verify_sms;
    public RegisterUserFragment regiser;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog pDialog;
    private String id, app_id;
    private String old_cell;
    private LinearLayout background_dummy;
    private LinearLayout resend;
    private String flag_pin, pin_len;
    String phone = "";
    private EditText code_first;
    private TextInputLayout name_first;

    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
            QPayCustomerDatabase.getReadableDatabase();
            id = QPayCustomerDatabase.getCustomerID();
            app_id = QPayCustomerDatabase.getKeyAppId();
            flag_pin = QPayCustomerDatabase.getFlagPin();
            pin_len = QPayCustomerDatabase.getPin();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            old_cell = getArguments().getString("cell_phone");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inflater.inflate(R.layout.enter_verification_code, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            background_dummy = (LinearLayout) view.findViewById(R.id.user_layout_containers);
            resend = (LinearLayout) view.findViewById(R.id.resend);
            try {

                background_dummy.setBackground(getAssetImage(Objects.requireNonNull(getContext()), "registration.png"));

            } catch (IOException e) {
                e.printStackTrace();
            }

            verify_sms = (TextView) view.findViewById(R.id.submit);
            code_first = (EditText) view.findViewById(R.id.code_first);
            name_first=(TextInputLayout)view.findViewById(R.id.name_first);
            code_first.requestFocus();
            code_first.addTextChangedListener(this);

            verify_sms.setOnClickListener(this);
            resend.setOnClickListener(this);

            new Utility().showKeyboard(getActivity());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                String deviceId = Settings.Secure.getString(getContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                if (code_first.getText().toString()!=null  && !code_first.getText().toString().isEmpty()&& code_first.getText().toString().length()==6){
                    sendButton(deviceId);
                    new Utility().hideSoftkey(getActivity());
                }else {
                    name_first.setError("Enter Code");
                }
                break;
            case R.id.resend:
                code_first.setText("");
                resend.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        resend.setVisibility(View.VISIBLE);
                    }
                },5000);
                new Resendcode().execute();
                break;
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        name_first.setError(null);
        if (editable == code_first.getEditableText()) {
            if (editable.length() == 6) {
                new Utility().hideSoftkey(getActivity());
            }

            // DO STH
        }
    }

    public class Resendcode extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result_resend = null;
            try {
                jsonObject.put("cell_phone", old_cell);
                jsonObject.put("app_id", app_id);
                jsonObject.put("custId", id);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "resend code post : " + jsonObject.toString());
                result_resend = networkAPI.sendHTTPData(Constants.RESEND_CODE, jsonObject);
                Log.d(Utility.TAG, "response from resend : " + result_resend);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result_resend;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                pDialog.dismiss();
                pDialog.hide();
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("resendactivationcodeResult");
                String status = jsonObject1.getString("status");
                if (status.equals("00")) {
                    Toast.makeText(getContext(), "Your resend request for activation code is complete please check your phone for SMS.", Toast.LENGTH_SHORT).show();
                } else if (status.equals("99")) {
                    Toast.makeText(getContext(), "Failed to verify due to internal network problem. Please try again later.", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                pDialog.dismiss();
                pDialog.hide();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                pDialog.dismiss();
                pDialog.hide();
                e.printStackTrace();
            }
        }
    }


    public void sendButton(String deviceId) {
        String text_from_editext = code_first.getText().toString();
        new AuthenicationTask(id, text_from_editext, app_id,deviceId).execute();


    }

    public class AuthenicationTask extends AsyncTask<String, String, JSONObject> {
        String id, act_code, app_id,deviceId;

        public AuthenicationTask(String id, String act_code, String app_id,String deviceId) {
            this.id = id;
            this.act_code = act_code;
            this.app_id = app_id;
            this.deviceId=deviceId;
        }

        @Override
        protected JSONObject doInBackground(String... uri) {
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String resukt;
            try {
                jsonObject.put("cell_phone", old_cell);
                jsonObject.put("new_cust_id", id);
                jsonObject.put("app_id", app_id);
                jsonObject.put("act_code", act_code);
                jsonObject.put("deviceId",deviceId);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                resukt = networkAPI.sendHTTPData(Constants.REACT_ACTIVATE, jsonObject);
                Log.d(Utility.TAG, "reactive custumer result : " + jsonObject.toString());
                jsonObject = new JSONObject(resukt);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(JSONObject s) {
            super.onPostExecute(s);
            try {

                JSONObject jsonObject1 = null;
                jsonObject1 = s.getJSONObject("reactivatecustomerResult");
                String status = jsonObject1.getString("status");
                String enc_cust_id = jsonObject1.getString("enc_cust_id");
                String cell_phone = jsonObject1.getString("cell_phone");
                String first_name = jsonObject1.getString("first_name");
                String last_name = jsonObject1.getString("last_name");

                GeneralPref.setEmailUser(jsonObject1.getString("email_id"));
                String c_code = " \"" + jsonObject1.getString("c_code") + "\"";
                String c_type = " \"" + jsonObject1.getString("c_type") + "\"";
                Log.d(Utility.TAG, "reactive custumer " + jsonObject1.toString());
                if (status != null && !status.isEmpty() && !status.equals("null") && status.equals("00")) {
                    String update_first = " \'" + first_name + "\' ";
                    String update_second = " \'" + last_name + "\' ";
                    QPayCustomerDatabase.updateCust_phone(cell_phone);
                    QPayCustomerDatabase.updateFLname(update_first, update_second);
                    QPayCustomerDatabase.updateDevicetoken(" \'" + enc_cust_id + "\' ");
                    QPayCustomerDatabase.updateCtypecode(c_type, c_code);
                    new DeletoldappId(enc_cust_id).execute();
                }else {
                    pDialog.dismiss();
                    new Utility().showSnackbar(code_first,"Failed to reactivate.");
                }
            } catch (Exception e) {
                pDialog.dismiss();
                e.printStackTrace();
            }
        }
    }

    public class DeletoldappId extends AsyncTask<String, String, String> {
        String old_cust_id;

        public DeletoldappId(String old_cust_id) {
            this.old_cust_id = old_cust_id;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result_app_delet = null;
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("old_cust_id", old_cust_id);
                jsonObject.put("new_cust_id", id);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "post delet app id : " + jsonObject);
                result_app_delet = networkAPI.sendHTTPData(Constants.DELET_NEW_APPID, jsonObject);
                Log.d(Utility.TAG, "post delet app id : " + result_app_delet);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result_app_delet;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                pDialog.dismiss();
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("DeleteNewCustInfoResult");
                String status = jsonObject1.getString("status");
                if (status.equals("00")) {

                    QPayCustomerDatabase.updateFlag(" 'A' ");
                    QPayCustomerDatabase.updateCust_id(" \'" + old_cust_id + "\' ");
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("Check", 5);
                    startActivity(intent);


                } else {
                    Toast.makeText(getContext(), "Invalid Activation Code.", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                pDialog.dismiss();
                Toast.makeText(getContext(), "Failed to verify due to internal network problem. Please try again later.", Toast.LENGTH_LONG).show();                e.printStackTrace();
            }
        }
    }

}
