package net.qpaysolutions.QPay.Register;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 6/13/16.
 */
public class EnterVerificationCodeFragment extends Fragment implements View.OnClickListener, TextWatcher {

    private Button verify_sms;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog pDialog;
    private String id, app_id;
    private String flag_condtion;
    private LinearLayout background_dummy;
    private LinearLayout resend;
    private String phone = "";
    private EditText code_first;
    private boolean isClick = false;
    private SharedPreferences sharePreference;
    public static final String MY_PREFS_NAME = "token";
    private String token;
    private TextInputLayout name_first;


    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
            QPayCustomerDatabase.getReadableDatabase();
            id = QPayCustomerDatabase.getCustomerID();
            app_id = QPayCustomerDatabase.getKeyAppId();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            sharePreference = getActivity().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
            token = sharePreference.getString("fcm_token", "");
            verify_sms = (Button) view.findViewById(R.id.submit);
            resend = (LinearLayout) view.findViewById(R.id.resend);
            code_first = (EditText) view.findViewById(R.id.code_first);

            code_first.requestFocus();
            code_first.addTextChangedListener(this);
            name_first = (TextInputLayout) view.findViewById(R.id.name_first);

            verify_sms.setOnClickListener(this);
            resend.setOnClickListener(this);
            background_dummy = (LinearLayout) view.findViewById(R.id.user_layout_containers);
            try {
                if (Build.VERSION.SDK_INT >= 16) {
                    background_dummy.setBackground(getAssetImage(getContext(), "registration.png"));
                } else {
                    background_dummy.setBackgroundDrawable(getAssetImage(getContext(), "registration.png"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            showkeyword();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            phone = getArguments().getString("cell_phone");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return inflater.inflate(R.layout.enter_verification_code, container, false);
    }

    public void recivedSms(String message) {
        try {
            Log.d(Utility.TAG, "message from qpay :" + message);
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                try {
                    @SuppressLint("HardwareIds") String deviceId = Settings.Secure.getString(getContext().getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    if (isClick == false) {
                        isClick = true;
                        if (code_first.getText().toString() != null && !code_first.getText().toString().isEmpty() && code_first.getText().toString().length() == 6) {

                            sendButton(deviceId);
                            hidekeyword(verify_sms);
                        } else {
                            isClick = false;
                            name_first.setError("Enter Code");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.resend:
                code_first.setText("");
                resend.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        resend.setVisibility(View.VISIBLE);
                    }
                }, 5000);
                new ResendCode().execute();
                break;
        }
    }

    public void showkeyword() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void hidekeyword(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        name_first.setError(null);
        if (editable == code_first.getEditableText()) {
            if (editable.length() == 6) {
                hidekeyword(code_first);
            }
            // DO STH

        }
    }

    public class ResendCode extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result_resend = null;
            try {
                jsonObject.put("cell_phone", phone);
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", id);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "resend code post : " + jsonObject.toString());
                result_resend = networkAPI.sendHTTPData(Constants.RESEND_CODE, jsonObject);
                Log.d(Utility.TAG, "response from resend : " + result_resend);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result_resend;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            pDialog.hide();
            try {

                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("resendactivationcodeResult");
                String status = jsonObject1.getString("status");
                if (status.equals("00")) {
                    Toast.makeText(getContext(), "Your resend request for activation code is complete please check your phone for SMS.", Toast.LENGTH_LONG).show();
                } else if (status.equals("99")) {
                    Toast.makeText(getContext(), "Failed to verify due to internal network problem. Please try again later.", Toast.LENGTH_LONG).show();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    public void sendButton(String deviceId) {
        String text_from_editext = code_first.getText().toString();
        new AuthTask(text_from_editext, deviceId).execute();


    }

    public class AuthTask extends AsyncTask<String, String, String> {
        String code, deviceId;

        public AuthTask(String code, String deviceId) {
            this.code = code;
            this.deviceId = deviceId;
        }

        @Override
        protected String doInBackground(String... uri) {
            String result;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", id);
                jsonObject.put("act_code", code);
                jsonObject.put("deviceId", deviceId);
                jsonObject.put("deviceToken", token);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                result = networkAPI.sendHTTPData(Constants.AUTH_WEB_CALl, jsonObject);
                JSONObject jsonObject2 = new JSONObject(result);
                JSONObject jsonObject1 = jsonObject2.getJSONObject("ActivateCustomerResult");
                for (int i = 0; i < 1; i++) {
                    String status = jsonObject1.getString("status");
                    String encreptedCustId = jsonObject1.getString("enc_cust_id");
                    if (status.equals("00")) {
                        QPayCustomerDatabase.updateFlag(" 'A' ");
                        QPayCustomerDatabase.updateCust_id(" \'" + encreptedCustId + "\' ");
                    } else {
                    }
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            isClick = false;
            try {
                flag_condtion = QPayCustomerDatabase.getFlag();
                if (flag_condtion.equals("A")) {
                    pDialog.dismiss();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("Check", 5);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    pDialog.dismiss();
                    Toast.makeText(getContext(), "Invalid Activation Code.", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    pDialog.dismiss();
                    pDialog.hide();
                    Toast.makeText(getContext(), "Failed to verify. Please try again later.", Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        }
    }

}
