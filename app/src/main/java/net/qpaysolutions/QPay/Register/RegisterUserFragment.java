package net.qpaysolutions.QPay.Register;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by dinesh on 5/19/16.
 */
public class RegisterUserFragment extends Fragment implements View.OnClickListener {
    private EditText email;
    private EditText first_name;
    private EditText last_name;
    private Button register;
    private Cursor cursor;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog pDialog;
    private String id;
    private String flag_condtion;
    private String cust_id, app_id;
    static final String TAG = "dinesh";
    private String phone;
    private String checkBoxText = "I agree to all the <a href='http://qpay.com.np/termsandcondition.html' > Terms and Conditions</a>";
    private CheckBox checkBox1;
    private TextView term_condition;
    private TextInputLayout input_first_name, input_last_name, email_last;


    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            phone = getArguments().getString("cell_phone");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return inflater.inflate(R.layout.register_main_test, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
            QPayCustomerDatabase.getReadableDatabase();
            cust_id = QPayCustomerDatabase.getCustomerID();
            app_id = QPayCustomerDatabase.getKeyAppId();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout register_back = (LinearLayout) view.findViewById(R.id.register);
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                register_back.setBackground(getAssetImage(getContext(), "registration.png"));
            } else {
                register_back.setBackgroundDrawable(getAssetImage(getContext(), "registration.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE| WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        email = (EditText) view.findViewById(R.id.email);
        first_name = (EditText) view.findViewById(R.id.first_test);
        last_name = (EditText) view.findViewById(R.id.last_test);
        register = (Button) view.findViewById(R.id.btn_test);
        checkBox1 = (CheckBox) view.findViewById(R.id.checkBox1);
        term_condition = (TextView) view.findViewById(R.id.term_condition);
        input_first_name = (TextInputLayout) view.findViewById(R.id.name_first);
        input_last_name = (TextInputLayout) view.findViewById(R.id.name_last);
        email_last = (TextInputLayout) view.findViewById(R.id.email_last);
        first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.toString().isEmpty() || s.toString().length() < 1) {
                        input_first_name.setError("Enter First Name");

                    } else {
                        input_first_name.setError(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.toString().isEmpty() || s.toString().length() < 1) {
                        input_last_name.setError("Enter Last Name");

                    } else {
                        input_last_name.setError(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.toString().isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()) {
                        email_last.setError("Enter Valid Email");
                    } else {

                        email_last.setError(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        term_condition.setOnClickListener(this);
        register.setOnClickListener(this);
    }

    public boolean validate() {
        boolean valid = true;
        try {
            String name = first_name.getText().toString();
            String name_last = last_name.getText().toString();
            String mail = email.getText().toString();
            if (name.isEmpty()) {
                input_first_name.setError("Enter First Name");
                return false;

            } else {
                input_first_name.setError(null);
            }
            if (name_last.isEmpty()) {
                input_last_name.setError("Enter Last Name");
                return false;
            } else {
                input_last_name.setError(null);
            }
            if (mail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
                email_last.setError("Enter Valid Email");
                return false;
            } else {

                email.setError(null);
            }
            if (checkBox1.isChecked()){

            }else {
                Toast.makeText(getContext(), "Please accept terms and condition", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }


        return valid;
    }


    public void isValid() {
        if (validate()==true) {
            try {
                String email_first = email.getText().toString();
                String name_first = first_name.getText().toString();
                String name_second = last_name.getText().toString();
                id = QPayCustomerDatabase.getCustomerID();
                String update_first = " \'" + name_first + "\' ";
                String update_second = " \'" + name_second + "\' ";
                QPayCustomerDatabase.updateCust_phone(phone);
                QPayCustomerDatabase.updateFLname(update_first, update_second);
                new Secondwebservicecall(cust_id, name_first, name_second, phone, email_first).execute();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_test:
               isValid();
                break;
            case R.id.term_condition:
                Uri uri = Uri.parse("http://qpay.com.np/termsandcondition.html");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

        }
    }

    class Secondwebservicecall extends AsyncTask<String, String, String> {

        String cust_id, fname, lname, cell, email;

        private Secondwebservicecall(String cust_id, String fname, String lname, String cell, String email) {
            this.cust_id = cust_id;
            this.fname = fname;
            this.lname = lname;
            this.cell = cell;
            this.email = email;
        }

        @Override
        protected String doInBackground(String... uri) {
            JSONObject jsonObject;
            String line = null;
            JSONObject Response = null;
            String status = null;
            jsonObject = new JSONObject();
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("fname", fname);
                jsonObject.put("lname", lname);
                jsonObject.put("cell", cell);
                jsonObject.put("email", email);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                NetworkAPI networkAPI = new NetworkAPI();
                Response = new JSONObject(networkAPI.sendHTTPData(Constants.SECOND_WEB_CALL, jsonObject));
                Log.d("dinesh",jsonObject.toString());
                Log.d("dinesh",Response.toString());
                if (Response != null) {
                    JSONObject jsonObject1 = Response.getJSONObject("registercustomerinfoResult");
                    String c_code = " \"" + jsonObject1.getString("c_code") + "\"";
                    String c_type = " \"" + jsonObject1.getString("c_type") + "\"";
                    status = jsonObject1.getString("status");
                    if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("00")) {
                        QPayCustomerDatabase.updateFlag(" 'R' ");
                        QPayCustomerDatabase.updateFlagPin(" '0' ");
                        QPayCustomerDatabase.updateCtypecode(c_type, c_code);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                pDialog.hide();
                pDialog.dismiss();
                if (s.equals("99") || s.equals("09")) {
                    Utils.customSnackBar(input_first_name, "Failed to register.");
                } else {
                    try {
                        flag_condtion = QPayCustomerDatabase.getFlag();
                        if (flag_condtion != null && !flag_condtion.isEmpty() && flag_condtion.equals("R")) {
                            GeneralPref.setEmailUser(email);
                            QPayCustomerDatabase.updateFlag(" 'I' ");
                            EnterVerificationCodeFragment fr = new EnterVerificationCodeFragment();
                            FragmentManager fm = getFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            Bundle args = new Bundle();
                            args.putString("act_code", "");
                            args.putString("cell_phone", phone);
                            fr.setArguments(args);
                            ft.replace(R.id.fragment, fr);
                            ft.commit();
                        } else {
                            Utils.customSnackBar(input_first_name, "Failed to register.");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.customSnackBar(input_first_name, "Failed to register.");
                    }
                }
            } catch (Exception e) {
                pDialog.hide();
                pDialog.dismiss();
                Utils.customSnackBar(input_first_name, "Failed to register.");
                e.printStackTrace();
            }
        }
    }

}
