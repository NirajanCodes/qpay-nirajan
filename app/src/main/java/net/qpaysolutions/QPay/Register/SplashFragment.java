package net.qpaysolutions.QPay.Register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.MyCode.EntercodeFragment;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

/**
 * Created by dinesh on 5/17/16.
 */
public class SplashFragment extends Fragment {
    private QPayCustomerDatabase db;
    private QPayProgressDialog pDialog;
    private JSONObject Response;
    private String flag_pin, pin_l;
    public static final String MY_PREFS_NAME = "token";
    private SharedPreferences sharePreference;
    private String token;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();
            db = new QPayCustomerDatabase(getActivity());
            db = new QPayCustomerDatabase(getContext());
            db.getWritableDatabase();
            flag_pin = db.getFlagPin();
            pin_l = db.getPin();
            hasRegistere();
        } catch (Exception e) {
            pDialog.dismiss();
            pDialog.hide();
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_slide_three, container, false);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.w("DealActivityFinal", "onPause");
    }

    public void hasRegistere() {
        try {
            int getCusturdata = db.checkHasCustData();
            int getRegistered = db.checkRegData();
            String get_reg_flag = db.getRegFlag();
            String pin_retries_count = db.getPin_retrive();

            if (getCusturdata == 0) {
                if (getRegistered == 0) {
                    try {
                        if (token == null) {
                            new GetCustomerId("f9MCAabFM90:APA91bFxQYbKx5v-jSx4XynG26cqkzbXiO067bMQMuuE3SfrRTHwtAHZQM6ruD_c8MQ6W03eoa2wA_qdq46c9sPfWXrkakrt48eTPXsC").execute();
                        } else {
                            new GetCustomerId(token).execute();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        new GetCustomerId("f9MCAabFM90:APA91bFxQYbKx5v-jSx4XynG26cqkzbXiO067bMQMuuE3SfrRTHwtAHZQM6ruD_c8MQ6W03eoa2wA_qdq46c9sPfWXrkakrt48eTPXsC").execute();
                    }
                }
            } else if (get_reg_flag.equals("A")) {
                if (pin_retries_count == null) {
                    pDialog.dismiss();
                    pDialog.hide();
                    if (flag_pin != null && !flag_pin.isEmpty() && flag_pin.equals("1")) {
                        try {
                            EntercodeFragment entercodeFragment = new EntercodeFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment, entercodeFragment)
                                    .commit();
                            getActivity().setTitle("Enter Passcode");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.putExtra("Check", 5);
                        startActivity(intent);
                        getActivity().finish();

                    }
                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    BlockFragment blockFragment = new BlockFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, blockFragment)
                            .commit();
                }
            } else if (get_reg_flag.equals("I")) {
                pDialog.dismiss();
                pDialog.hide();
                CheckPhoneFragment registerPhoneFragment = new CheckPhoneFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, registerPhoneFragment)
                        .commit();
            } else if (get_reg_flag.equals("R")) {
                pDialog.dismiss();
                pDialog.hide();
                EnterVerificationCodeReactivationFragment enterVerificationCodeReactivationFragment = new EnterVerificationCodeReactivationFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, enterVerificationCodeReactivationFragment)
                        .commit();
            } else {
                pDialog.dismiss();
                pDialog.hide();
                getActivity().finish();
            }

        } catch (Exception e) {
            pDialog.dismiss();
            e.printStackTrace();
        }
    }

    private class GetCustomerId extends AsyncTask<Void, Void, JSONObject> {
        String token;

        private GetCustomerId(String token) {
            this.token = token;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            try {
               JSONObject jsonObject = new JSONObject();
                jsonObject.put("dev_token", token);
                jsonObject.put("device_type", "AND");
                Response = new JSONObject(NetworkAPI.sendHTTPData(Constants.FIRST_GCM_CALL_URL,jsonObject));
                return Response;
            } catch (Exception e) {
                Log.d(Utility.TAG, "Error Encountered");
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            super.onPostExecute(args);
            pDialog.dismiss();
            pDialog.hide();
            try {
                JSONObject  jsonObject1 = args.getJSONObject("getcustidResult");
                String cust_id = jsonObject1.getString("cust_id");
                String app_id = jsonObject1.getString("app_id");
                String s = jsonObject1.getString("status");
                if (s != null && !s.isEmpty() && !s.equals("null") && s.equals("00")) {
                    db.insertCutid(cust_id, app_id, "1", "1");
                    db.createRegFlag("I");
                    CheckPhoneFragment registerPhoneFragment = new CheckPhoneFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, registerPhoneFragment)
                            .commit();
                }

            } catch (Exception e) {

                e.printStackTrace();
                getActivity().finish();
                Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        }
    }
}