package net.qpaysolutions.QPay.Register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Utility;

/**
 * Created by deadlydragger on 8/10/16.
 */
public class BlockFragment extends Fragment implements View.OnClickListener {
    EditText code;
    Button send;
    QPayCustomerDatabase QPayCustomerDatabase;
    String random;
    ImageView mCallPhone;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.suspended_main,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            code=(EditText)view.findViewById(R.id.code_master);
            send=(Button)view.findViewById(R.id.send_master);
            send.setOnClickListener(this);
            code.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length()==4){
                        InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(code.getWindowToken(),0);
                    }

                }
            });
            mCallPhone = (ImageView) view.findViewById(R.id.call_icon);
            mCallPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phone = "014414868";
                  Utility.callPhone(getActivity(),phone);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        random= QPayCustomerDatabase.getRandom();
        Log.d(Utility.TAG,"random : " + random);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.send_master:
                try {
                    if (code.getText().toString().trim().equals(random.trim())){
                        QPayCustomerDatabase.updatePin_retrive(null);
                        QPayCustomerDatabase.updatePin_l(null);
                        QPayCustomerDatabase.updateFlagPin(null);
                        startActivity(new Intent(getActivity(), MainActivity.class));
                    }else {
                        code.setError("Please Enter Valid Master Code");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
        }
    }
}

