package net.qpaysolutions.QPay.Billpayment.meroTv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 5/14/18.
 */

public class MeroTvAdapter  extends BaseAdapter {
    Context context;
    ArrayList<MeroTvPlan> arrayList;
    LayoutInflater inflter;

    public MeroTvAdapter(Context applicationContext, ArrayList<MeroTvPlan> plans) {
        this.context = applicationContext;
        this.arrayList = plans;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_spineer_merotv, null);
        TextView names = (TextView) view.findViewById(R.id.itemName);
        names.setText(arrayList.get(i).getPlanName());
        return view;
    }
}