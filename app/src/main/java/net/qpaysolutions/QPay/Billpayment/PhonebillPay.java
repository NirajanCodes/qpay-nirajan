package net.qpaysolutions.QPay.Billpayment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import net.qpaysolutions.QPay.BuildConfig;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.Regex;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * Created by deadlydragger on 6/28/16.
 */
public class PhonebillPay extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private LinearLayout ncell, ntc, adsl, bill_pay_utl, enter, recharge_ntc_postpaid, recharge_ntc_epin, recharge_smart_cell_epin, bill_pay_utl_epin,recharge_smart_cel;
    private ScrollView scrollView;
    private EditText phone_number;
    private Button confirm;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private  String cust_id, bytecode, balance = "";
    private double lat, lon;
    private   static final String TAG = "dinesh";
    private   Toolbar toolbar;
    private ImageView paying_to;
    private String selectedItem = "";
    private String app_id;
    private TextView amount_pay, provider, provider_bill, toolbar_text;
    private List<String> ncellRechargeAmountList;
    private  boolean allowBonus;
    private  ImageView conact_picker;
    private  static final int PICK_CONTACT = 1;
    private  boolean valid = false;
    private   String ncellRegex, ntc_prepaidRegex, ntc_postpaidRegex, utl_Regex,smart_REjex;
    private  LinearLayout picker_visibility, ll_parent;
    private  String cell_number = "";
    private boolean isBackpress = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            QPayCustomerDatabase = new QPayCustomerDatabase(this);
            QPayCustomerDatabase.getReadableDatabase();
            cust_id = QPayCustomerDatabase.getCustomerID();
            app_id = QPayCustomerDatabase.getKeyAppId();
            setContentView(R.layout.phone_bill);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            picker_visibility = (LinearLayout) findViewById(R.id.picker_visibility);
            ll_parent = (LinearLayout) findViewById(R.id.ll_parent);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar_text = (TextView) toolbar.findViewById(R.id.toolbar_text);
            toolbar_text.setText("Phone Bill");
            allowBonus = GeneralPref.getAllowBonus();
            ncell = (LinearLayout) findViewById(R.id.ncell_pay);
            ntc = (LinearLayout) findViewById(R.id.recharge_ntc_prepaid);
            adsl = (LinearLayout) findViewById(R.id.bill_pay_adsl);
            bill_pay_utl = (LinearLayout) findViewById(R.id.bill_pay_utl);
            recharge_ntc_postpaid = (LinearLayout) findViewById(R.id.recharge_ntc_postpaid);
            recharge_ntc_epin = (LinearLayout) findViewById(R.id.recharge_ntc_epin);
            recharge_smart_cell_epin = (LinearLayout) findViewById(R.id.recharge_smart_cell_epin);
            bill_pay_utl_epin = (LinearLayout) findViewById(R.id.bill_pay_utl_epin);
            recharge_smart_cel=(LinearLayout)findViewById(R.id.recharge_smart_cell);
            scrollView = (ScrollView) findViewById(R.id.scroll_view);
            enter = (LinearLayout) findViewById(R.id.enter_form);
            phone_number = (EditText) findViewById(R.id.phone);
            confirm = (Button) findViewById(R.id.confirm);
            paying_to = (ImageView) findViewById(R.id.pay_option_image);
            amount_pay = (TextView) findViewById(R.id.amount_pay);
            provider = (TextView) findViewById(R.id.provider);
            provider_bill = (TextView) findViewById(R.id.provider_bill);
            conact_picker = (ImageView) findViewById(R.id.contact_picker);
            recharge_smart_cel.setOnClickListener(this);
            bill_pay_utl_epin.setOnClickListener(this);
            recharge_smart_cell_epin.setOnClickListener(this);
            recharge_ntc_epin.setOnClickListener(this);
            recharge_ntc_postpaid.setOnClickListener(this);
            ncell.setOnClickListener(this);
            ntc.setOnClickListener(this);
            adsl.setOnClickListener(this);
            bill_pay_utl.setOnClickListener(this);
            confirm.setOnClickListener(this);
            conact_picker.setOnClickListener(this);
            scrollView.setVisibility(View.VISIBLE);
            enter.setVisibility(View.GONE);
            lat = ((CustomerApplication) this.getApplication()).getLat();
            lon = ((CustomerApplication) this.getApplication()).getLng();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.contact_picker:
                phone_number.setText("");
                Intent intent_contact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent_contact, PICK_CONTACT);
                break;
            case R.id.ncell_pay:
                if (allowBonus == true) {
                    showkeyword();
                    isBackpress = true;
                    phone_number.requestFocus();
                    phone_number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                            phone_number.removeTextChangedListener(this);
                            String number = charSequence.toString();
                            ncellRegex = Regex.NCELL_REGEX;
                            if (number.length() < 3) {
                                ncellRegex = ncellRegex.substring(0, number.length());
                            } else {
                                ncellRegex = Regex.NCELL_REGEX;
                            }
                            if (number.matches(ncellRegex)) {
                                phone_number.setTextColor(Color.BLACK);
                                valid = true;
                                phone_number.setError(null);
                            } else {
                                valid = false;
                                phone_number.setTextColor(Color.RED);
                                phone_number.setError("Enter valid Ncell number");
                            }
//                            phone_number.addTextChangedListener(this);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() == 10) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(PhonebillPay.this.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                            }
                        }
                    });
                    String[] ncellRechargeValues = getResources().getStringArray(R.array.ncell_recharge_amount);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(ncellRechargeValues));
                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });
                    bytecode = "201";
                    paying_to.setImageResource(R.drawable.ncell);
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("Ncell Recharge");
                    provider_bill.setText("Pay Ncell Prepaid/Postpaid bill");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;
            case R.id.recharge_ntc_prepaid:
                if (allowBonus == true) {
                    isBackpress = true;
                    showkeyword();
                    phone_number.requestFocus();
                    phone_number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            phone_number.removeTextChangedListener(this);
                            String number = charSequence.toString();
                             /*  ntc_prepaidRegex = Regex.NTC_PREPAID_REGEX;

                                ntc_prepaidRegex = ntc_prepaidRegex.substring(0, number.length());
                            } else {
                                ntc_prepaidRegex = Regex.NTC_PREPAID_REGEX;
                            }*/
                            if (number.length() >= 3) {
                                if (number.startsWith("984") || number.startsWith("986") || number.startsWith("974")) {
                                    phone_number.setTextColor(Color.BLACK);
                                    valid = true;
                                    phone_number.setError(null);
                                } else {
                                    valid = false;
                                    phone_number.setTextColor(Color.RED);
                                    phone_number.setError("Enter valid Ntc prepaid number");
                                }
                            }else {
                                phone_number.setTextColor(Color.BLACK);
                                valid = true;
                                phone_number.setError(null);
                            }
                            phone_number.addTextChangedListener(this);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() == 10) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(PhonebillPay.this.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                            }
                        }
                    });

                    String[] ntc_prepaid = getResources().getStringArray(R.array.ntc_prepaid);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(ntc_prepaid));

                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });
                    paying_to.setImageResource(R.drawable.nct_logo);
                    bytecode = "203";
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("NTC Prepaid");
                    provider_bill.setText("Pay NTC Prepaid bill");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;
            case R.id.recharge_ntc_postpaid:
                if (allowBonus == true) {
                    isBackpress = true;
                    showkeyword();
                    phone_number.requestFocus();
                    phone_number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            phone_number.removeTextChangedListener(this);
                            String number = charSequence.toString();
                            ntc_postpaidRegex = Regex.NTC_POSTPAID_REGEX;
                            /*if (number.length() < 3) {
                                ntc_postpaidRegex = ntc_postpaidRegex.substring(0, number.length());
                            } else {
                                ntc_postpaidRegex = Regex.NTC_POSTPAID_REGEX;
                            }*/
                            if (number.length()>=3){
                                if (number.startsWith("985")||number.startsWith("975")) {
                                    phone_number.setTextColor(Color.BLACK);
                                    valid = true;
                                    phone_number.setError(null);
                                } else {
                                    valid = false;
                                    phone_number.setTextColor(Color.RED);
                                    phone_number.setError("Enter valid Ntc postpaid number");
                                }
                            }

                            phone_number.addTextChangedListener(this);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() == 10) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(PhonebillPay.this.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                            }
                        }
                    });
                    String[] ntc_postpaid = getResources().getStringArray(R.array.ntc_postpaid);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(ntc_postpaid));

                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });

                    bytecode = "205";
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    paying_to.setImageResource(R.drawable.nct_logo);
                    provider.setText("NTC Postpaid");
                    provider_bill.setText("Pay NTC Postpaid bill");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;
            case R.id.bill_pay_adsl:

                if (allowBonus == true) {
                    isBackpress = true;
                    showkeyword();
                    phone_number.requestFocus();
                    phone_number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() == 10) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(PhonebillPay.this.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                            }
                        }
                    });
                    String[] adsl = getResources().getStringArray(R.array.landline);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(adsl));
                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });
                    bytecode = "204";
                    valid = true;
                    paying_to.setImageResource(R.drawable.nct_logo);
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("NTC Landline");
                    provider_bill.setText("Pay NTC Landline bill");

                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                valid = true;

                break;
            case R.id.bill_pay_utl:
                if (allowBonus == true) {
                    isBackpress = true;
                    showkeyword();
                    phone_number.requestFocus();
                    phone_number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            phone_number.removeTextChangedListener(this);
                            String number = charSequence.toString();
                            utl_Regex = Regex.UTL;
                            if (number.length() < 3) {
                                utl_Regex = utl_Regex.substring(0, number.length());
                            } else {
                                utl_Regex = Regex.UTL;
                            }
                            if (number.matches(utl_Regex)) {
                                phone_number.setTextColor(Color.BLACK);
                                valid = true;
                            } else {
                                valid = false;
                                phone_number.setTextColor(Color.RED);
                                phone_number.setError("Enter valid UTL number");
                            }
                            phone_number.addTextChangedListener(this);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() == 10) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(PhonebillPay.this.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                            }
                        }
                    });
                    String[] utl = getResources().getStringArray(R.array.utl);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(utl));

                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });
                    bytecode = "206";
                    paying_to.setImageResource(R.drawable.utl);
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("UTL Recharge");
                    provider_bill.setText("Pay UTL bill");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;
            case R.id.recharge_ntc_epin:
                if (allowBonus == true) {
                    isBackpress = true;
//                    showkeyword();
                    picker_visibility.setVisibility(View.GONE);
                    phone_number.setVisibility(View.GONE);

                    String[] ntc_epin = getResources().getStringArray(R.array.ntc_epin);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(ntc_epin));

                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                    if (selectedItem.equals("100.00")) {
                                        bytecode = "612";

                                    } else if (selectedItem.equals("200.00")) {
                                        bytecode = "606";
                                    } else if (selectedItem.equals("500.00")) {
                                        bytecode = "607";
                                    } else if (selectedItem.equals("1000.00")) {
                                        bytecode = "608";
                                    }
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });
                    paying_to.setImageResource(R.drawable.nct_logo);
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("NTC E-Recharge");
                    provider_bill.setText("Get NTC E-Recharge Pin");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;
            case R.id.recharge_smart_cell_epin:
                if (allowBonus == true) {
                    isBackpress = true;
//                    showkeyword();
//                    phone_number.requestFocus();
                    picker_visibility.setVisibility(View.GONE);
                    phone_number.setVisibility(View.GONE);
                    String[] smart_epin = getResources().getStringArray(R.array.smrt_epin);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(smart_epin));

                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                    if (selectedItem.equals("200.00")) {
                                        bytecode = "604";

                                    } else if (selectedItem.equals("500.00")) {
                                        bytecode = "605";
                                    }else if (selectedItem.equals("50.00")){
                                        bytecode = "631";
                                    }else if (selectedItem.equals("100.00")){
                                        bytecode = "603";
                                    } else  if (selectedItem.equals("1000.00")){
                                        bytecode = "638";
                                    }
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });
                    paying_to.setImageResource(R.drawable.smart_cell);
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("Smartcell E-Recharge");
                    provider_bill.setText("Get Smartcell E-Recharge Pin");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;
            case R.id.bill_pay_utl_epin:
                if (allowBonus == true) {
                    isBackpress = true;
//                    showkeyword();
//                    phone_number.requestFocus();
                    picker_visibility.setVisibility(View.GONE);
                    phone_number.setVisibility(View.GONE);
                    String[] utl_epin = getResources().getStringArray(R.array.utl_epin);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(utl_epin));

                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                    if (selectedItem.equals("100.00")) {
                                        bytecode = "609";

                                    } else if (selectedItem.equals("250.00")) {
                                        bytecode = "610";
                                    } else if (selectedItem.equals("500.00")) {
                                        bytecode = "611";
                                    }
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });

                    paying_to.setImageResource(R.drawable.utl);
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("UTL E-Recharge");
                    provider_bill.setText("Get UTL E-Recharge Pin");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;

            case R.id.recharge_smart_cell:
                if (allowBonus == true) {
                    isBackpress = true;
                    showkeyword();
                    phone_number.requestFocus();
                    phone_number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            phone_number.removeTextChangedListener(this);
                            String number = charSequence.toString();
                            smart_REjex = Regex.SMART_CELL;
                            if (number.length() < 3) {
                                smart_REjex = smart_REjex.substring(0, number.length());
                            } else {
                                smart_REjex = Regex.SMART_CELL;
                            }
                            if (number.matches(smart_REjex)) {
                                phone_number.setTextColor(Color.BLACK);
                                valid = true;
                            } else {
                                valid = false;
                                phone_number.setTextColor(Color.RED);
                                phone_number.setError("Enter valid SMart Cell number");
                            }
                            phone_number.addTextChangedListener(this);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() == 10) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(PhonebillPay.this.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                            }
                        }
                    });
                    String[] utl = getResources().getStringArray(R.array.smart_cell);
                    ncellRechargeAmountList = new ArrayList<>(Arrays.asList(utl));

                    amount_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideSoftkey(enter);
                            final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PhonebillPay.this, new DatePickerPopWin.OnDatePickedListener() {
                                @Override
                                public void onDatePickCompleted(int month, String dateDesc) {
                                    selectedItem = dateDesc;
                                    amount_pay.setText(dateDesc);
                                }
                            }).textConfirm("DONE")
                                    .textCancel("CANCEL")
                                    .btnTextSize(16)
                                    .viewTextSize(25)
                                    .setValues(ncellRechargeAmountList)
                                    .build();
                            pickerPopWin.showPopWin(PhonebillPay.this);
                        }
                    });
                    bytecode = "207";
                    paying_to.setImageResource(R.drawable.smart_cell);
                    scrollView.setVisibility(View.GONE);
                    enter.setVisibility(View.VISIBLE);
                    provider.setText("SMart Cell Recharge");
                    provider_bill.setText("Pay SMart Cell bill");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    custumdialogfailure();
                }
                break;

            case R.id.confirm:

                showkeyword();
                try {

                    cell_number = phone_number.getText().toString();
                    Log.d(Utility.TAG, "valid : " + valid);
                    Log.d(Utility.TAG, "money: " + selectedItem + " cell number : " + cell_number + "bye_code : " + bytecode + " balance : " + balance);
                    if (bytecode.equals("201") || bytecode.equals("203") || bytecode.equals("205") || bytecode.equals("206")) {
                        if (!cell_number.isEmpty() && cell_number.length() == 10 && selectedItem.length() > 1 && valid == true) {
                            isBackpress = false;
                            Intent intent = new Intent(PhonebillPay.this, BillpayList.class);
                            intent.putExtra("money", selectedItem);
                            intent.putExtra("cell_number", cell_number);
                            intent.putExtra("lat", lat);
                            intent.putExtra("lon", lon);
                            intent.putExtra("byt_code", bytecode);
                            intent.putExtra("balance", balance);
                            startActivity(intent);
                            finish();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);

                        } else {

                            new Utility().showSnackbar(v, "Enter Valid Input");
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);

                        }
                    } else if (bytecode.equals("204")) {
                        if (!cell_number.isEmpty() && cell_number.length() == 9 && selectedItem.length() > 1 && cell_number.startsWith("0") && valid == true) {
                            isBackpress = false;
                            Intent intent = new Intent(PhonebillPay.this, BillpayList.class);
                            intent.putExtra("money", selectedItem);
                            intent.putExtra("cell_number", cell_number);
                            intent.putExtra("lat", lat);
                            intent.putExtra("lon", lon);
                            intent.putExtra("byt_code", bytecode);
                            intent.putExtra("balance", balance);
                            startActivity(intent);
                            finish();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);

                        } else {

                            new Utility().showSnackbar(v, "Enter Valid Input");
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);

                        }
                    } else {
                        if (selectedItem.length() > 1) {
                            isBackpress = false;
                            Intent intent = new Intent(PhonebillPay.this, BillpayList.class);
                            intent.putExtra("money", selectedItem);
                            intent.putExtra("cell_number", "1");
                            intent.putExtra("lat", lat);
                            intent.putExtra("lon", lon);
                            intent.putExtra("byt_code", bytecode);
                            intent.putExtra("balance", balance);
                            startActivity(intent);
                            finish();
                            Log.d(Utility.TAG, "money: " + selectedItem + " cell number : " + cell_number + "bye_code : " + bytecode + " balance : " + balance);
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                        } else {
                            new Utility().showSnackbar(v, "Enter Valid Input");
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    new Utility().showSnackbar(v, "Enter Valid Input");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                }

                break;
        }
    }

    public void custumdialogfailure() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PhonebillPay.this);
        LayoutInflater inflater = PhonebillPay.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_custom_dialog_bill_payment_feature_unavailable, null);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(PhonebillPay.this, MainActivity.class));
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PhonebillPay.this, BillPaymentActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");

        if (isBackpress == false) {
            scrollView.setVisibility(View.VISIBLE);

            enter.setVisibility(View.GONE);
            isBackpress = false;
        }
    }


    public void showkeyword() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(enter.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void hideSoftkey(View editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            String cNumber = phones.getString(phones.getColumnIndex("data1")).replace("", BuildConfig.FLAVOR).replace("+977", BuildConfig.FLAVOR).replace("-", BuildConfig.FLAVOR);

                            phone_number.setText(cNumber.trim());
                            phone_number.clearFocus();
                            if (cNumber.trim().length()<11){

                                phone_number.setSelection(cNumber.trim().length());
                            }

                        }
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    }
                }
                break;
        }
    }



}
