package net.qpaysolutions.QPay.Billpayment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import net.qpaysolutions.QPay.Card.AddCardActivity;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by QPay on 6/10/2018.
 */

public class LoadFundActivity extends AppCompatActivity {
    private RecyclerView loadFundRecyclerView;
    private LoadFundAdapters mAdapter;
    private ArrayList<ProductModelEbanking> loadFundList;
    private Toolbar toolbar;
    private TextView toolbar_text;
    private ViewSwitcher viewSwitcher;
    private FrameLayout childLayout;
    private RelativeLayout recyclerLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_load_fund);
        initComponents();
    }

    private void initComponents() {
        loadFundRecyclerView = findViewById(R.id.loadFundRecyclerView);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText("Load Balance");
        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        childLayout = findViewById(R.id.childLayout);
        recyclerLayout = findViewById(R.id.recyclerLayout);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        loadFundData();
    }

    private void loadFundData() {
        loadFundList = new ArrayList<>();
        mAdapter = new LoadFundAdapters(this, loadFundList, new LoadFundAdapters.LoadFundOnClickListener() {
            @Override
            public void loadFundlistener(int position) {
                try {
                    if (position == 1) {
                        Intent intent = new Intent(LoadFundActivity.this, AddCardActivity.class);
//                        intent.putExtra("id", "cardTopup");
//                        intent.putExtra("id", "add_card");
                        startActivity(intent);
                    }
                    if (position == 2) {
                        Intent loadCardAtm = new Intent(LoadFundActivity.this, BillPayActivity.class);
                        loadCardAtm.putExtra("id", "eb");
                        startActivity(loadCardAtm);
                    }
//                    if (position == 3) {
////                        Intent topUpIntent = new Intent(LoadFundActivity.this, PrabhuWithdrawActivity.class);
////                        topUpIntent.putExtra("isCashLoad", true);
////                        startActivity(topUpIntent);
//                    }

                   /* if (position == 3) {
                        Intent connectIps = new Intent(LoadFundActivity.this, BillPayActivity.class);
                        connectIps.putExtra("id", "connect_ips");
//                        connectIps.putExtra("name", arrayList.get(0).getName());
//                        connectIps.putExtra("gateway", arrayList.get(0).getId());
//                        connectIps.putExtra("url", arrayList.get(0).getLogoUrl());
                        startActivity(connectIps);
                    }*/
                    if (position == 3) {
                        Intent addCard = new Intent(LoadFundActivity.this, BillPayActivity.class);
                        addCard.putExtra("id", "mobileBanking");
                        startActivity(addCard);
                    }
                    if (position == 4) {
//                        Intent addCard = new Intent(LoadFundActivity.this, BillPayActivity.class);
//                        addCard.putExtra("id", "add_card");
//                        startActivity(addCard);
                        Intent connectIps = new Intent(LoadFundActivity.this, BillPayActivity.class);
                        connectIps.putExtra("id", "connect_ips");
                        startActivity(connectIps);
                    }
                    if (position == 5) {
                        Intent myCodeIntent = new Intent(LoadFundActivity.this, BillPayActivity.class);
                        myCodeIntent.putExtra("id", "code");
                        myCodeIntent.putExtra("flag", "code");
                        myCodeIntent.putExtra("title", "Merchant TopUp");
                        startActivity(myCodeIntent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        loadFundRecyclerView.setHasFixedSize(true);
        loadFundRecyclerView.setNestedScrollingEnabled(false);
        loadFundRecyclerView.setLayoutManager(mLayoutManager);
        loadFundRecyclerView.setAdapter(mAdapter);

        ProductModelEbanking productModel = new ProductModelEbanking("1", R.drawable.ic_action_ebanking, "Load Via Card");
        loadFundList.add(productModel);

        ProductModelEbanking modelEbanking = new ProductModelEbanking("2", R.drawable.ic_action_card, "E-Banking");
        loadFundList.add(modelEbanking);


        ProductModelEbanking mobileBanking = new ProductModelEbanking("3", R.drawable.ic_mobile_banking, "Mobile Banking");
        loadFundList.add(mobileBanking);

//        ProductModelEbanking productModelEbanking = new ProductModelEbanking("3", R.drawable.ic_action_merchant_topup, "Prabhu Agent");
//        loadFundList.add(productModelEbanking);


        ProductModelEbanking addCard = new ProductModelEbanking("4", R.drawable.ic_connect_ips, "Connect IPS");
        loadFundList.add(addCard);

        ProductModelEbanking myCode = new ProductModelEbanking("5", R.drawable.ic_action_merchant_topup, "Merchant Topup");
        loadFundList.add(myCode);

        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
