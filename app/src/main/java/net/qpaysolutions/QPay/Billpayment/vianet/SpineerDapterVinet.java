package net.qpaysolutions.QPay.Billpayment.vianet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;


public class SpineerDapterVinet extends BaseAdapter {
    Context context;
    ArrayList<Vinetmodel> arrayList;
    LayoutInflater inflter;

    public SpineerDapterVinet(Context applicationContext, ArrayList<Vinetmodel> countryNames) {
        this.context = applicationContext;
        this.arrayList = countryNames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_flight_spineer, null);
        TextView names = (TextView) view.findViewById(R.id.itemName);
        names.setText(arrayList.get(i).getServiceType());
        return view;
    }
}