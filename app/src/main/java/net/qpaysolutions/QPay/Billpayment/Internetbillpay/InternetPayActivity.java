package net.qpaysolutions.QPay.Billpayment.Internetbillpay;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.GeneralPref;

/**
 * Created by deadlydragger on 6/15/17.
 */

public class InternetPayActivity extends AppCompatActivity {

    Toolbar toolbar_custum;
    public TextView titleText;
    private InternetFragment internetFragment;
    Boolean tv;
    FragmentManager fm;
    QPayProgressDialog qPayProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Utility.setStatusColor(this);
        setContentView(R.layout.internet_pay);
        qPayProgressDialog = new QPayProgressDialog(InternetPayActivity.this);
        tv = getIntent().getBooleanExtra("tv", false);
        toolbar_custum = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar_custum);
        fm = getSupportFragmentManager();
        titleText = (TextView) toolbar_custum.findViewById(R.id.title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (tv) {
            titleText.setText("Television Bill Pay");
            GeneralPref.setIsTv(tv);
        } else {
            titleText.setText("Internet Bill Pay");
            GeneralPref.setIsTv(tv);
        }

        internetFragment = new InternetFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("tv", tv);
        internetFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.internet_fragment, internetFragment)
                .commit();
//        if (!GeneralPref.getAllowBonus()) {
//            new Checkbonusflag().execute();
//
//        }
//        else {
//            new CashBackAPI(new DialogInterface() {
//                @Override
//                public void showDialog() {
//                    qPayProgressDialog.show();
//                }
//
//                @Override
//                public void hideDialog() {
//                    qPayProgressDialog.dismiss();
//                }
//            },this).execute();
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    //    public boolean alloBonus() {
//        return GeneralPref.getAllowBonus();
//    }
    public boolean isTv() {
        return GeneralPref.getIsTv();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void setVisibleFragment(String main) {

        SharedPreferences sharedPreferences = getSharedPreferences("isVisible", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("fragment", main);
        editor.apply();

    }

//    public class Checkbonusflag extends AsyncTask<String, String, String> {
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("appId", GeneralPref.getAppId());
//                jsonObject.put("id", GeneralPref.getCustId());
//                jsonObject.put("lat", LatLngPref.getLat());
//                jsonObject.put("lng", LatLngPref.getLng());
//                Log.d("dinesh", "doInBackground: " + jsonObject);
//                Log.d("dinesh", "doInBackground: " + new NetworkAPI().sendHTTPData(Constants.BASE_API + "checkbonusflag", jsonObject));
//                return new NetworkAPI().sendHTTPData(Constants.BASE_API + "checkbonusflag", jsonObject);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            qPayProgressDialog.show();
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            qPayProgressDialog.dismiss();
//            Log.d("bonus",""+s);
//            try {
//                JSONObject jsonObject = new JSONObject(s);
//                boolean success = jsonObject.getBoolean("success");
//                if (success) {
//                    JSONArray data = jsonObject.getJSONArray("data");
//                    JSONObject jsonObject1 = data.getJSONObject(0);
//                    boolean AllowBonus = jsonObject1.getBoolean("AllowBonus");
//                    GeneralPref.setAllowBonus(AllowBonus);
////                    new CashBackAPI(new DialogInterface() {
////                        @Override
////                        public void showDialog() {
////                            qPayProgressDialog.show();
////                        }
////
////                        @Override
////                        public void hideDialog() {
////                            qPayProgressDialog.dismiss();
////                        }
////                    },InternetPayActivity.this).execute();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

}
