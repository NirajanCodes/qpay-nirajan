package net.qpaysolutions.QPay.Billpayment.Internetbillpay

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import java.util.*

class ViewPagerAdapter(fm: FragmentManager, NumOfTabs: Int) : FragmentStatePagerAdapter(fm) {

    private val mNumOfTabs: Int = NumOfTabs

    var fragments: MutableList<Fragment> = ArrayList()
    var fragmentTitle: MutableList<String> = ArrayList()

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        fragmentTitle.add(title)
        fragments.add(fragment)

    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitle[position]
    }
}


