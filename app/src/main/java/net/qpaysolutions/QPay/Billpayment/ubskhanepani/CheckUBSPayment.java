package net.qpaysolutions.QPay.Billpayment.ubskhanepani;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Billpayment.ubskhanepani.QuickPayDto.QuickPayDto;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.util.Collections;
import java.util.Set;

/**
 * Created by deadlydragger on 3/18/18.
 */

public class CheckUBSPayment extends AsyncTask<String, String, String> {

    UBSWaterFragment ubsWaterFragment;
    private boolean add = true;

    public CheckUBSPayment(UBSWaterFragment ubsWaterFragment) {
        this.ubsWaterFragment = ubsWaterFragment;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            Utility.isBuild(ubsWaterFragment.getJson().toString());
            Log.d(Utility.TAG, "water request : " + ubsWaterFragment.getJson());
            return NetworkAPI.sendHTTPData(Constants.CHECKUBS_PAYMENT, ubsWaterFragment.getJson());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ubsWaterFragment.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        ubsWaterFragment.hideDialog();
        try {
            Log.d(Utility.TAG, "WATER" + s);
            JSONObject jsonObject = new JSONObject(s);
            if (jsonObject.getString("status").equals("00")) {
                try {
                    if (jsonObject.getJSONArray("data").getJSONObject(0).getString("RespCode").equals("00")) {
                        QuickPayDto quickPayDto = new QuickPayDto();
                        Set<String> strings = GeneralPref.getQuickWater();
                        Object[] data = null;
                        if (strings != null) {
                            data = strings.toArray();
                        }
                        if (strings == null) {
                            String s1 = null;
                            String officeId = ubsWaterFragment.getOfficeId();
                           /* if (officeId.contains(":")) {
                                int index = officeId.indexOf(":");
                                officeId = officeId.substring(0,index);
                            }*/
                            s1 = ubsWaterFragment.getOfficeName() + "," + ubsWaterFragment.getCustomerId() + ',' + GeneralPref.getWaterInfo() + "," + officeId;
                            Set<String> newstrings = Collections.singleton(s1);
                            GeneralPref.setQuickWater(newstrings);
                        } else {
                            String[] fetchedData, regionId, regionName, custId, splited;
                            fetchedData = new String[data.length];
                            regionId = new String[data.length];
                            regionName = new String[data.length];
                            custId = new String[data.length];
                            for (int i = 0; i < data.length; i++) {
                                fetchedData[i] = data[i].toString();
                                splited = fetchedData[i].split(",");
                                regionName[i] = splited[0];
                                regionId[i] = splited[3];
                                regionName[i] = splited[2];
                                custId[i] = splited[1];
                                Log.d(Utility.TAG, "  check ubs payment : " + fetchedData[i]);
                                Log.d(Utility.TAG, "  regionID : " + regionId[i]);
                                Log.d(Utility.TAG, "  customerId : " + custId[i]);
                            }
                            for (int i = 0; i < custId.length; i++) {
                                if (custId[i].equalsIgnoreCase(ubsWaterFragment.getCustomerId()) && regionId[i].equalsIgnoreCase(ubsWaterFragment.getOfficeId())) {
                                    add = false;
                                }
                            }
                            if (add) {
                                String officeId = ubsWaterFragment.getOfficeId();
                               /* if (officeId.contains(":")) {
                                    int index = officeId.indexOf(":");
                                    officeId = officeId.substring(0, index);
                                }*/
                                strings.add(ubsWaterFragment.getOfficeName() + "," + ubsWaterFragment.getCustomerId() + ',' + GeneralPref.getWaterInfo() + "," + officeId);
                                GeneralPref.setQuickWater(strings);
                                Log.d(Utility.TAG, "water list quick :" + GeneralPref.getQuickWater());
                            }
                        }
                        if (GeneralPref.getWaterInfo().equals("402")) {
                            ubsWaterFragment.showDetailsUBS(jsonObject.getJSONArray("data").getJSONObject(0));
                        } else {
                            ubsWaterFragment.showDetailsNepalKhanepani(jsonObject.getJSONArray("data").getJSONObject(0));
                        }
                    } else {
                        ubsWaterFragment.showError(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ubsWaterFragment.showError(jsonObject.getString("Operation Failed"));
                }
            } else if (jsonObject.getString("status").equals("99")) {
                ubsWaterFragment.showError(jsonObject.getString("message"));
            } else {
                ubsWaterFragment.showError(jsonObject.getString("message"));
            }
        } catch (Exception e) {

        }

    }
}

