package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 7/4/17.
 */

public class NeaPayment extends AsyncTask<String, String, String> {
    private String appId, id, billPayCode, opt1, opt2, opt3;
    private QPayProgressDialog qPayProgressDialog;
    private NeaFragment context;
    private double lat, lng;
    private QPayCustomerDatabase qPayCustomerDatabase;
    private ArrayList list;
    private String[] customerId;
    private boolean flag = true;

    public NeaPayment(NeaFragment context, String appId, String id, String billPayCode, double lat, double lng, String opt1, String opt2, String opt3, QPayProgressDialog qPayProgressDialog, QPayCustomerDatabase qPayCustomerDatabase) {
        this.context = context;
        this.appId = appId;
        this.id = id;
        this.billPayCode = billPayCode;
        this.lat = lat;
        this.lng = lng;
        this.opt1 = opt1;
        this.opt2 = opt2;
        this.opt3 = opt3;
        this.qPayProgressDialog = qPayProgressDialog;
        this.qPayCustomerDatabase = qPayCustomerDatabase;

    }

    @Override
    protected String doInBackground(String... params) {
        NetworkAPI networkAPI = new NetworkAPI();
        String nea_paymen_check = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", appId);
            jsonObject.put("id", id);
            jsonObject.put("billPayCode", billPayCode);
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("opt1", opt1);
            jsonObject.put("opt2", opt2);
            jsonObject.put("opt3", opt3);
            Log.d("dinesh", "doInBackground: " + jsonObject);
            nea_paymen_check = networkAPI.sendHTTPData(Constants.CHECK_WORDLINK_BILL, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nea_paymen_check;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        list = qPayCustomerDatabase.getQuickNeaInfo();
        customerId = (String[]) list.get(0);

        qPayProgressDialog.show();

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        qPayProgressDialog.dismiss();

        Log.d(Utility.TAG, "response nea: " + s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            boolean success = jsonObject.getBoolean("success");
            if (success) {

                if (customerId.length > 0) {
                    for (int i = 0; i < customerId.length; i++) {
                        Log.d(Utility.TAG, " length " + customerId);
                        if (customerId[i].equalsIgnoreCase(opt1)) {
                            flag = false;
                        }

                    }
                    if (flag) {
                        qPayCustomerDatabase.insertQuickInfoNea(opt1, opt2, opt3);
                    }
                } else {
                    qPayCustomerDatabase.insertQuickInfoNea(opt1, opt2, opt3);
                    qPayCustomerDatabase.getQuickNeaInfo();
                    Log.d(Utility.TAG, " string database " + qPayCustomerDatabase.getQuickNeaInfo().get(0));
                }

                NeaSearchResultFragment neaSearchResultFragment = new NeaSearchResultFragment();
                FragmentTransaction fragmentManager = context.getFragmentManager().beginTransaction();
                fragmentManager.replace(R.id.frameLayout, neaSearchResultFragment);
                Bundle bundle = new Bundle();
                bundle.putString("nea_result", s);
                bundle.putString("customer_id", opt1);
                bundle.putString("office_code", opt2);
                bundle.putString("sc_no", opt3);
                neaSearchResultFragment.setArguments(bundle);
                fragmentManager.commit();

            } else {

                Utility.showSnackbar(context.nea_customer, "Failed to get data");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
