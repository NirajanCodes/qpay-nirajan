package net.qpaysolutions.QPay.Billpayment.vianet;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;


public class Vianetcheckpayment extends AsyncTask<String, String, String> {
    ViaNetFragment viaNetFragment;

    public Vianetcheckpayment(ViaNetFragment viaNetFragment) {
        this.viaNetFragment = viaNetFragment;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("opt1", viaNetFragment.getUserId());
            jsonObject.put("billPayCode", "302");
            jsonObject.put("opt2", "");
            jsonObject.put("opt3", "");
            Utility.isBuild(jsonObject.toString());
            return NetworkAPI.sendHTTPData(Constants.CHECKUBS_PAYMENT, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        viaNetFragment.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        viaNetFragment.hideDialog();
        Log.d("dinesh","response: "+s);
        String message="Failed to get data" ;
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
             message= jsonObject.getString("message");
            if (status.equals("00")) {
                ArrayList<Vinetmodel> arrayList= new ArrayList<>();
                JSONArray data = jsonObject.getJSONArray("data");

                JSONObject jsonObject1= data.getJSONObject(0);
                GeneralPref.setUsernameWordlink(jsonObject1.getString("CustName"));
                for (int i=0;i<jsonObject1.getJSONArray("Service").length();i++){
                    JSONObject object=jsonObject1.getJSONArray("Service").getJSONObject(i);
                    Vinetmodel vinetmodel = new Vinetmodel();
                    vinetmodel.setAmount(object.getString("Amount"));
                    vinetmodel.setServiceType(object.getString("ServiceType"));
                    vinetmodel.setOfficeCode(object.getString("OfficeCode"));
                    arrayList.add(vinetmodel);
                }
                viaNetFragment.showLayout(arrayList,jsonObject1.getString("CustName"),jsonObject1.getString("CustId"));
            }else if (status.equals("99")){
                if(message!=null && !message.equalsIgnoreCase("")) {
                    viaNetFragment.showError(message);
                }else {
                    viaNetFragment.showError(" Failed to get data ");
                }
            }

            else {

                if(message!=null || !message.equalsIgnoreCase("")) {
                    viaNetFragment.showError(message);
                }else {
                    viaNetFragment.showError(" Failed to get data ");
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            viaNetFragment.showError(message);
        }
    }
}
