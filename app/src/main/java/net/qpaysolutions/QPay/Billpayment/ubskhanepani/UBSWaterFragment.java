package net.qpaysolutions.QPay.Billpayment.ubskhanepani;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputLayout;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.Billpayment.ubskhanepani.QuickPayAdapters.TopUpAdapter;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Set;

/**
 * Created by deadlydragger on 5/7/17.
 */

public class UBSWaterFragment extends Fragment implements DialogInterface, UbswaterofficeList, WaterInterface {
    private QPayProgressDialog qPayProgressDialog;
    private String officeId;
    private EditText customerId;
    private TextInputLayout inputLayoutId;
    private LinearLayout main;
    private boolean flag = true;
    private RelativeLayout top;
    private String quickCustId, quickOfficeId, officeName;
    private String[] fetchedData, regionName, custId, splited, byte_code, regionId, fetchedData402, regionName402, regionId402, custId402, splited402, byte_code402, fetchedData404, regionName404, regionId404, custId404, splited404, byte_code404;
    private RelativeLayout quickView;
    boolean firstime = true;
    private boolean billPayList = true;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ubs_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        main = view.findViewById(R.id.main);
        if (GeneralPref.getWaterInfo().equals("402")) {
            try {
                if (Tags.getPreferenceArrayKhanepani("402") != null) {
                    firstime = false;
                    setAdapter(Tags.getPreferenceArrayKhanepani("402"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (Tags.getPreferenceArrayKhanepani("404") != null) {
                    firstime = false;
                    setAdapter(Tags.getPreferenceArrayKhanepani("404"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        top = view.findViewById(R.id.top);
        qPayProgressDialog = new QPayProgressDialog(getContext());
        inputLayoutId = view.findViewById(R.id.input_layout_id);
        customerId = view.findViewById(R.id.customerId);
        customerId.setOnFocusChangeListener((v, hasFocus) -> {

            if (!hasFocus) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(customerId.getWindowToken(), 0);
            }

        });

        quickCustId = customerId.getText().toString();
        quickOfficeId = officeName;
        customerId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        bottomPopUp(view);
        new Ubswateroffice(UBSWaterFragment.this, firstime).execute();
        view.findViewById(R.id.searchButton).setOnClickListener(view1 -> {
            quickView.setVisibility(View.GONE);
            if (customerId.getText().toString() != null && !customerId.getText().toString().isEmpty()) {
                quickCustId = customerId.getText().toString();
                new Utility().hideSoftkey(getActivity());
                new CheckUBSPayment(UBSWaterFragment.this).execute();
            } else {
                try {
                    inputLayoutId.setVisibility(View.VISIBLE);
                    inputLayoutId.setError("Enter Customer Id");
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void showDialog() {
        qPayProgressDialog.show();
    }

    @Override
    public void hideDialog() {
        qPayProgressDialog.dismiss();
    }

    @Override
    public void getList(ArrayList<UBSModel> arrayList) {
        setAdapter(arrayList);
    }

    private void setAdapter(final ArrayList<UBSModel> arrayList) {

        final Spinner officeSelect = getView().findViewById(R.id.officeSelect);
        UBSSpineerDapter customAdapter = new UBSSpineerDapter(getActivity(), arrayList, UBSWaterFragment.this);
        officeSelect.setAdapter(customAdapter);
        officeSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                officeId = arrayList.get(i).getOfficeCode();
                officeName = arrayList.get(i).getOffice();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public JSONObject getJson() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("billPayCode", GeneralPref.getWaterInfo());
            jsonObject.put("opt1", quickCustId);
            jsonObject.put("opt2", officeId);
            jsonObject.put("opt3", "");
            Log.d("nirajan", "quick pay : " + jsonObject.toString());
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String getCustomerId() {
        return quickCustId;
    }


    protected String getOfficeName() {
        return officeName;
    }

    protected String getOfficeId() {
        return officeId;
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {

    /*
     if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                UtilityFragment najirEnglish = new UtilityFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.utility_fragment, najirEnglish);
                fragmentTransaction.commit();
                return true;
            }
            return false;

        }*/
            getActivity().onBackPressed();
            return true;

        });
    }

    @Override
    public void showDetailsUBS(final JSONObject object) {

        Log.d(Utility.TAG, "ubs reesponse : " + object.toString());
        LinearLayout linearLayout = getView().findViewById(R.id.paymentDetails);
        linearLayout.setVisibility(View.VISIBLE);
        main.setVisibility(View.GONE);
        TextView customerName = getView().findViewById(R.id.customerName);
        TextView customerMobile = getView().findViewById(R.id.customerMobile);
        TextView customerOffice = getView().findViewById(R.id.customerOffice);
        TextView customerPreviousDue = getView().findViewById(R.id.customerPreviousDue);
        TextView customerServiceCharge = (TextView) getView().findViewById(R.id.customerServiceCharge);
        TextView customerTotal = getView().findViewById(R.id.customerTotal);
        TextView customerAddress = getView().findViewById(R.id.customerAddress);
        try {
            customerName.setText(object.getString("CustomerName"));
            GeneralPref.setUsernameWordlink(object.getString("CustomerName"));
            customerAddress.setText(object.getString("CustomerAddress"));
            customerMobile.setText(object.getString("CustomerMobile"));
            customerOffice.setText(officeName);
            customerPreviousDue.setText(object.getString("PreviousDue"));
            customerServiceCharge.setText(object.getString("ServiceCharge"));
            customerTotal.setText(object.getString("Total"));
            //Set<String> strings = GeneralPref.getQuickWater();
            //GeneralPref.setQuickWater(strings);
            //String[] arrayString = (String[]) strings.toArray();

            Log.d("dinesh", GeneralPref.getUsernameWordlink());
        } catch (Exception e) {
            e.printStackTrace();
        }

        getView().findViewById(R.id.submitButton).setOnClickListener(view -> {
            String reward = GeneralPref.getRebate();
            Log.d(Utility.TAG, "rebate  : " + Utility.parseRebate("402"));
            GeneralPref.setRewardAmount(Utility.parseRebate("402"));
            try {
              /*  Intent intent = new Intent(getActivity(), BillpayList.class);
                intent.putExtra("money", new Decimalformate().decimalFormate(object.getString("Total")));
                intent.putExtra("byt_code", "402");
                intent.putExtra("cell_number", customerId.getText().toString());
                intent.putExtra("user_id", officeId);
                intent.putExtra("sc_no",object.getString("ServiceCharge"));
                startActivity(intent);
                getActivity().finish();*/

              /*  final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
                new CheckRewardAPI(object.getString("Total"), "402", new DialogInterface() {
                    @Override
                    public void showDialog() {
                        //qPayProgressDialog.show();
                    }

                    @Override
                    public void hideDialog() {*/
                // qPayProgressDialog.dismiss();
                try {
                    if (billPayList) {
                        billPayList = false;
                        GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("402")) / 100) * Double.valueOf(Decimalformate.decimalFormate(object.getString("Total")))));
                        Bundle bundle = new Bundle();
                        bundle.putString("money", Decimalformate.decimalFormate(object.getString("Total")));
                        bundle.putString("byt_code", "402");
                        bundle.putString("cell_number", customerId.getText().toString());
                        bundle.putString("user_id", officeId);
                        bundle.putString("sc_no", object.getString("ServiceCharge"));
                        BillPayListSheet billPayListSheet = new BillPayListSheet();
                        billPayListSheet.setArguments(bundle);
                        billPayListSheet.show(getActivity().getSupportFragmentManager(), billPayListSheet.getTag());
                    }
                    Handler handler = new Handler();
                    handler.postDelayed(() -> billPayList = true, 2000);

                    /*
                     Intent intent = new Intent(getActivity(), BillpayList.class);
                            intent.putExtra("money", new Decimalformate().decimalFormate(object.getString("Total")));
                            intent.putExtra("byt_code", "402");
                            intent.putExtra("cell_number", customerId.getText().toString());
                            intent.putExtra("user_id", officeId);
                            intent.putExtra("sc_no", object.getString("ServiceCharge"));
                            startActivity(intent);*/
                    //  getActivity().finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
//                        }
//                    }).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void showDetailsNepalKhanepani(final JSONObject object) {

        Log.d(Utility.TAG, "khanepani reesponse : " + object.toString());
        LinearLayout paymentDetailsNepalDrinkingWater = getView().findViewById(R.id.paymentDetailsNepalDrinkingWater);
        main.setVisibility(View.GONE);
        paymentDetailsNepalDrinkingWater.setVisibility(View.VISIBLE);
        TextView customerName = getView().findViewById(R.id.customerNameKhanepani);
        TextView area = getView().findViewById(R.id.areaKhanepani);
        TextView customerOffice = getView().findViewById(R.id.customerOfficeKhanepani);
        TextView officeCode = getView().findViewById(R.id.officeCodeKhanepani);
        TextView lagat = getView().findViewById(R.id.lagatKhanepani);
        TextView totalServiceCharge = getView().findViewById(R.id.totalServiceChargeKhanepani);
        TextView customerTotal = getView().findViewById(R.id.customerTotalKhanepani);
        TextView customerIdNameKhanepani = getView().findViewById(R.id.customerIdNameKhanepani);
        TextView TotalDueAmount = getView().findViewById(R.id.total_due_amount);
        try {
            GeneralPref.setUsernameWordlink(object.getString("CustomerName"));
            customerName.setText(object.getString("CustomerName"));
            area.setText(object.getString("Area"));
            customerOffice.setText(officeName);
            officeCode.setText(object.getString("OfficeCode"));
            lagat.setText(object.getString("Lagat"));
            TotalDueAmount.setText(object.getString("TotalDueAmount"));
            totalServiceCharge.setText(new Decimalformate().decimalFormate(object.getString("TotalServiceCharge")));
            customerTotal.setText(new Decimalformate().decimalFormate(object.getString("Total")));
            customerIdNameKhanepani.setText(object.getString("CustomerId"));
            JSONArray jsonArray = object.getJSONArray("BillDetail");
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            TextView billamount = getView().findViewById(R.id.billAmount);
            billamount.setText(jsonObject.getString("BillAmount"));
            TextView billFrom = getView().findViewById(R.id.bill_from);
            billFrom.setText(jsonObject.getString("BillFrom"));
            TextView billTo = getView().findViewById(R.id.bill_to);
            billTo.setText(jsonObject.getString("BillTo"));
            TextView discountAmount = getView().findViewById(R.id.discount_amount);
            discountAmount.setText(jsonObject.getString("DiscountAmount"));
            TextView fineAmount = getView().findViewById(R.id.fine_amount);
            fineAmount.setText(jsonObject.getString("FineAmount"));
            TextView MeterRent = getView().findViewById(R.id.meter_amount);
            MeterRent.setText(jsonObject.getString("MeterRent"));
            TextView PaybleAmount = getView().findViewById(R.id.payable_amount);
            PaybleAmount.setText(jsonObject.getString("PayableAmount"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        getView().findViewById(R.id.submitButtonKhanepani).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                   /*
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    intent.putExtra("money", new Decimalformate().decimalFormate(object.getString("Total")));
                    intent.putExtra("byt_code", "404");
                    intent.putExtra("cell_number", customerId.getText().toString());
                    intent.putExtra("user_id", officeId);
                    intent.putExtra("addAmount1",new Decimalformate().decimalFormate(object.getString("TotalServiceCharge")));
                    startActivity(intent);
                    getActivity().finish();*/
                    quickView.setVisibility(View.GONE);
                  /*  final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
                    new CheckRewardAPI(object.getString("Total"), "404", new DialogInterface() {
                        @Override
                        public void showDialog() {
                            qPayProgressDialog.show();
                        }

                        @Override
                        public void hideDialog() {
                            qPayProgressDialog.dismiss();
                            Intent intent = new Intent(getActivity(), BillpayList.class);
                            try {
                                intent.putExtra("money", new Decimalformate().decimalFormate(object.getString("Total")));
                                intent.putExtra("byt_code", "404");
                                intent.putExtra("cell_number", customerId.getText().toString());
                                intent.putExtra("user_id", officeId);
                                intent.putExtra("addAmount1", new Decimalformate().decimalFormate(object.getString("TotalServiceCharge")));
                                startActivity(intent);
                                // getActivity().finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute();*/
                    if (billPayList) {
                        billPayList = false;
                        String amount = Decimalformate.decimalFormate(object.getString("Total"));
                        GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("404")) / 100) * Double.valueOf(amount)));
                        Bundle bundle = new Bundle();
                        bundle.putString("money", amount);
                        bundle.putString("byt_code", "404");
                        bundle.putString("cell_number", customerId.getText().toString());
                        bundle.putString("user_id", officeId);
                        bundle.putString("addAmount1", new Decimalformate().decimalFormate(object.getString("TotalServiceCharge")));
                        BillPayListSheet billPayListSheet = new BillPayListSheet();
                        billPayListSheet.setArguments(bundle);
                        billPayListSheet.show(getActivity().getSupportFragmentManager(), billPayListSheet.getTag());
                    }
                    Handler handler = new Handler();
                    handler.postDelayed(() -> billPayList = true, 2000);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (qPayProgressDialog != null) {
            qPayProgressDialog.dismiss();
        }
    }

    public void showError(String number) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = v.findViewById(R.id.title);
        dialog_title.setText("Failed!");
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(view -> dialog.dismiss());
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    private void bottomPopUp(View view) {

        quickView = view.findViewById(R.id.quick_view);
        quickView.setVisibility(View.GONE);
        TextView textView = view.findViewById(R.id.btn_new);
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.quick_view));
        Set<String> strings = GeneralPref.getQuickWater();
        Object[] data = null;
        if (strings != null) {
            data = strings.toArray();
            fetchedData = new String[data.length];
            regionName = new String[data.length];
            regionId = new String[data.length];
            custId = new String[data.length];
            byte_code = new String[data.length];
            int length402 = 0;
            int length404 = 0;
            for (int i = 0; i < data.length; i++) {
                try {
                    fetchedData[i] = data[i].toString();
                    splited = fetchedData[i].split(",");
                    regionName[i] = splited[0];
                    regionId[i] = splited[3];
                    custId[i] = splited[1];
                    byte_code[i] = splited[2];
                    Log.d("nirajan", " data : " + fetchedData[i]);
                    if (byte_code[i].equalsIgnoreCase("402")) {
                        length402++;
                    } else {
                        length404++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            fetchedData402 = new String[length402];
            custId402 = new String[length402];
            regionName402 = new String[length402];
            regionId402 = new String[length402];
            splited402 = new String[length402];
            byte_code402 = new String[length402];

            fetchedData404 = new String[length404];
            custId404 = new String[length404];
            regionName404 = new String[length404];
            regionId404 = new String[length404];
            splited404 = new String[length404];
            byte_code404 = new String[length404];

            int j = 0;
            int k = 0;

////UBS ra Nepal Khanepani lai data seperate gareko duita agema chuttai chuttai aayos bhanera.

            for (int i = 0; i < data.length; i++) {

                try {
                    Log.d("nirajan","byte code : "+byte_code[i]);
                    if (byte_code[i].equalsIgnoreCase("402")) {
                        Log.d("nirajan","byte code 402 : "+byte_code[i]);
                        fetchedData402[j] = data[i].toString();
                        splited402 = fetchedData402[j].split(",");
                        regionName402[j] = splited402[0];
                        regionId402[j] = splited402[3];
                        custId402[j] = splited402[1];
                        byte_code402[j] = splited402[2];
                        j++;
                        if ((fetchedData402 != null || fetchedData402.length > 1) && GeneralPref.getWaterInfo().equals(("402"))) {
                            quickView.setVisibility(View.VISIBLE);
                        }

                    } else if (byte_code[i].equalsIgnoreCase("404")) {
                        Log.d("nirajan","byte code 404 : "+byte_code[i]);
                        fetchedData404[k] = data[i].toString();
                        splited404 = fetchedData404[k].split(",");
                        regionName404[k] = splited404[0];
                        regionId404[k] = splited404[3];
                        custId404[k] = splited404[1];
                        byte_code404[k] = splited404[2];
                        k++;
                        if ((fetchedData404 != null || fetchedData404.length > 1) && GeneralPref.getWaterInfo().equals(("404"))) {
                            quickView.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (strings == null || strings.size() < 1) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            quickView.setVisibility(View.GONE);
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

        top.setOnClickListener(v -> {
            Utility.hideSoftkey(Objects.requireNonNull(getActivity()));
            if (flag) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                flag = false;
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                flag = true;
            }
        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                if (bottomSheetBehavior.getState() == bottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(() -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED), 1000);

        textView.setOnClickListener(v -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN));

        if (data != null) {
            TopUpAdapter topUpAdapter = null;
            if (GeneralPref.getWaterInfo().equals("402")) {
                topUpAdapter = new TopUpAdapter(fetchedData402, onQuickPay);
            } else {
                topUpAdapter = new TopUpAdapter(fetchedData404, onQuickPay);
            }
            RecyclerView recyclerView = getView().findViewById(R.id.quick_recyler);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(topUpAdapter);
        }
    }

    OnQuickPay onQuickPay = new OnQuickPay() {
        @Override
        public void onQuickPay(int position) {
            quickView.setVisibility(View.GONE);
            if (GeneralPref.getWaterInfo().equals("402")) {
                officeName = regionName402[position];
                officeId = regionId402[position];
                quickCustId = custId402[position];

            } else if(GeneralPref.getWaterInfo().equals("404")){
                officeName = regionName404[position];
                quickCustId = custId404[position];
                officeId = regionId404[position];
            }
            new CheckUBSPayment(UBSWaterFragment.this).execute();
        }
    };
}
