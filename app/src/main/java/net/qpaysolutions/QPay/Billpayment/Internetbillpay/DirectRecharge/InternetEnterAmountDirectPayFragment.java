package net.qpaysolutions.QPay.Billpayment.Internetbillpay.DirectRecharge;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.InternetFragment;
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.InternetPayActivity;
import net.qpaysolutions.QPay.BuildConfig;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by deadlydragger on 4/9/17.
 */

public class InternetEnterAmountDirectPayFragment extends Fragment implements View.OnClickListener {
    SharedPreferences sharedPreferences;
    static String MY_FILE_SHARED = "Mybalance";
    String byte_code, provider_name, text, byte_code_received;
    String TAG = "dinesh";
    TextView amount_pay, paying_name, amountManual;
    List<String> ncellRechargeAmountList;
    EditText phone_number, id;
    Button confirm;
    String selectedItem, title_paying;
    ImageView img_url;
    boolean isPhone = false;
    private LinearLayout picker_visibility, amountAutoLayout, amountManualLayout;
    private static final int PICK_CONTACT = 1;
    Boolean isFromDishHome;
    Boolean isFromMeroTv;
    TextInputLayout textInputLayout;
    private boolean billPayList = true;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((InternetPayActivity) getActivity()).setVisibleFragment("submain");
        amount_pay = (TextView) view.findViewById(R.id.amount_pay);
        phone_number = (EditText) view.findViewById(R.id.phone);
        id = (EditText) view.findViewById(R.id.id);
        confirm = (Button) view.findViewById(R.id.confirm);
        img_url = (ImageView) view.findViewById(R.id.pay_option_image);
        paying_name = (TextView) view.findViewById(R.id.paying_name);
        amountAutoLayout = (LinearLayout) view.findViewById(R.id.amountAutoLayout);
        textInputLayout = (TextInputLayout) view.findViewById(R.id.text_input_layout);
        amountAutoLayout.setVisibility(View.VISIBLE);
        amountManualLayout = (LinearLayout) view.findViewById(R.id.amountManualLayout);
        amountManualLayout.setVisibility(View.GONE);
        picker_visibility = (LinearLayout) view.findViewById(R.id.picker_visibility);
        picker_visibility.setOnClickListener(this);
        amountManual = view.findViewById(R.id.amountManual);

        confirm.setOnClickListener(this);
        Log.d(TAG, "onCreate: " + byte_code + " " + provider_name + " text");
        SetTitelText(byte_code_received);
        switch (byte_code_received) {
            case "301":
                picker_visibility.setVisibility(View.GONE);
                id.setVisibility(View.VISIBLE);
                sobisu();
                isPhone = true;
                break;
            case "613":
                picker_visibility.setVisibility(View.GONE);
                phone_number.setVisibility(View.GONE);
                broodLink();
                isPhone = true;
                break;
            case "304":
                picker_visibility.setVisibility(View.VISIBLE);
                phone_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
                adslUnlimited();
                break;
            case "305":
                picker_visibility.setVisibility(View.VISIBLE);
                phone_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
                adslVolume();
                break;
            case "616":
                picker_visibility.setVisibility(View.GONE);
                phone_number.setVisibility(View.GONE);
                netTvEpin();
                break;
            case "618":
                picker_visibility.setVisibility(View.GONE);
                phone_number.setVisibility(View.GONE);
                disHomeEpin();
                break;
            case "306":
                picker_visibility.setVisibility(View.GONE);

                textInputLayout.setHint(getString(R.string.cashId));
                disHomepin();
                id.setVisibility(View.VISIBLE);
                phone_number.setVisibility(View.GONE);
                break;
            case "307":
                amountManualLayout.setVisibility(View.GONE);
                amountAutoLayout.setVisibility(View.VISIBLE);
                picker_visibility.setVisibility(View.GONE);
                id.setVisibility(View.VISIBLE);
                textInputLayout.setHint("Customer Id");
                phone_number.setVisibility(View.GONE);
                simTV();
                break;
            case "634":
                picker_visibility.setVisibility(View.GONE);
                phone_number.setVisibility(View.GONE);
                meroTvEpin();
                break;
        }
    }

    public void SetTitelText(String code) {
        switch (code) {
            case "201":
                title_paying = "Ncell";
                img_url.setImageResource(R.drawable.ncell);
                break;
            case "203":
            case "205":
            case "612":
            case "606":
            case "607":
            case "608":
                title_paying = "NTC";
                img_url.setImageResource(R.drawable.nct_logo);
                break;
            case "301":
                title_paying = "SUBISU";
                img_url.setImageResource(R.drawable.subisu);

                break;
            case "305":
                title_paying = "ADSL Volume";
                img_url.setImageResource(R.drawable.adsl);
                break;
            case "304":
                title_paying = "ADSL Unlimited";
                img_url.setImageResource(R.drawable.adsl);
                break;
            case "613":
            case "614":
            case "615":
                title_paying = "Broadlink";
                img_url.setImageResource(R.drawable.broodlink);
                break;
            case "306":
            case "618":
            case "619":
            case "620":
            case "621":
            case "622":
            case "623":
            case "624":
            case "625":
            case "626":
            case "632":
            case "637":
                title_paying = "DishHome";
                phone_number.setHint("Customer Id");
                img_url.setImageResource(R.drawable.dishhome);
                break;
            case "204":
                title_paying = "PSTN Landline";
                img_url.setImageResource(R.drawable.nct_logo);
                break;
            case "206":
            case "609":
            case "610":
            case "611":
                title_paying = "UTL";
                img_url.setImageResource(R.drawable.utl);
                break;
            case "616":
            case "617":

                title_paying = "Net TV";
                img_url.setImageResource(R.drawable.net_tv);
                break;
            case "604":
            case "605":
                title_paying = "Smart Cell";
                img_url.setImageResource(R.drawable.smart_cell);
                break;
            case "403":
                title_paying = "Traffic fine pay.";
                img_url.setImageResource(R.drawable.traffic);
                break;
            case "307":
                title_paying = "SIM TV pay.";
                img_url.setImageResource(R.drawable.sim_tv);
                break;
            case "634":
                title_paying = "Mero TV";
                img_url.setImageResource(R.drawable.mero_tv);
                break;

        }
        paying_name.setText(title_paying);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enter_amount, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        isFromDishHome = b.getBoolean("isFromDishHome");
        isFromMeroTv = b.getBoolean("isFromMeroTv");
        sharedPreferences = getActivity().getSharedPreferences(MY_FILE_SHARED, Context.MODE_PRIVATE);
        if (isFromDishHome) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("byte_code", "306");
            editor.putString("text", "DishHome bill pay");
            editor.putString("provider_name", "dishhome");
            editor.commit();
        } else if (isFromMeroTv) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("byte_code", "208");
            editor.putString("text", "Mero TV bill pay");
            editor.putString("provider_name", "MeroTv");
            editor.commit();
        }
        byte_code_received = sharedPreferences.getString("byte_code", "");
        provider_name = sharedPreferences.getString("provider_name", "");
        text = sharedPreferences.getString("text", "");

    }

    public void sobisu() {
        byte_code = byte_code_received;
        String[] sobisu = getResources().getStringArray(R.array.sobisu);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(sobisu));

        amount_pay.setFocusable(true);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    public void broodLink() {
        String[] sobisu = getResources().getStringArray(R.array.broodlink);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(sobisu));
        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                            if (selectedItem.equals("550.00")) {
                                byte_code = "629";
                            } else if (selectedItem.equals("565.00")) {
                                byte_code = "613";

                            } else if (selectedItem.equals("1200.00")) {
                                byte_code = "614";
                            } else if (selectedItem.equals("1500.00")) {
                                byte_code = "630";
                            } else if (selectedItem.equals("2260.00")) {
                                byte_code = "615";
                            }
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    public void adslUnlimited() {
        byte_code = byte_code_received;
        String[] adsl_unlimited = getResources().getStringArray(R.array.adsl_unlimited);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(adsl_unlimited));
        amountManualLayout.setVisibility(View.GONE);
        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    public void netTvEpin() {
        String[] net_tv_epin = getResources().getStringArray(R.array.net_tv_epin);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(net_tv_epin));
//        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                            Log.d("dinesh", "net tv test : " + selectedItem);
                            if (selectedItem.equals("50.00")) {
                                byte_code = "616";

                            } else if (selectedItem.equals("100.00")) {
                                byte_code = "617";
                            }
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });


    }

    @SuppressLint("ClickableViewAccessibility")
    public void simTV() {
        byte_code = byte_code_received;
        String[] dishome = getResources().getStringArray(R.array.simTv);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(dishome));

        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    public void disHomepin() {
        byte_code = byte_code_received;
        String[] dishome = getResources().getStringArray(R.array.dishome);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(dishome));
        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public void meroTvEpin() {
        String[] dishome_epin = getResources().getStringArray(R.array.merotv_epin);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(dishome_epin));
        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                            if (selectedItem.equals("350.00")) {
                                byte_code = "634";
                            } else if (selectedItem.equals("500.00")) {
                                byte_code = "635";

                            } else if (selectedItem.equals("650.00")) {
                                byte_code = "636";

                            }

                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    public void disHomeEpin() {
        String[] dishome_epin = getResources().getStringArray(R.array.dishome_epin);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(dishome_epin));

        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                            if (selectedItem.equals("350.00")) {
                                byte_code = "618";
                            } else if (selectedItem.equals("400.00")) {
                                byte_code = "619";

                            } else if (selectedItem.equals("500.00")) {
                                byte_code = "620";

                            } else if (selectedItem.equals("600.00")) {
                                byte_code = "632";

                            } else if (selectedItem.equals("2000.00")) {
                                byte_code = "621";

                            } else if (selectedItem.equals("2000.00")) {
                                byte_code = "622";

                            } else if (selectedItem.equals("3000.00")) {
                                byte_code = "623";

                            } else if (selectedItem.equals("4000.00")) {
                                byte_code = "624";

                            } else if (selectedItem.equals("5000.00")) {
                                byte_code = "625";
                            } else if (selectedItem.equals("6000.00")) {
                                byte_code = "633";

                            } else if (selectedItem.equals("7000.00")) {
                                byte_code = "626";

                            } else if (selectedItem.equals("600.00")) {
                                byte_code = "632";
                            } else if (selectedItem.equals("700.00")) {
                                byte_code = "637";
                            }

                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });


    }

    @SuppressLint("ClickableViewAccessibility")
    public void adslVolume() {
        byte_code = byte_code_received;
        String[] adsl_volume = getResources().getStringArray(R.array.adsl_volume);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(adsl_volume));

        amount_pay.setFocusable(false);
        amount_pay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(amount_pay.getWindowToken(), 0);
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            amount_pay.setText(dateDesc);
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(25)
                            .setValues(ncellRechargeAmountList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                }
                return true;
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                Log.d(TAG, "onClick: " + byte_code);
                try {
                    switch (byte_code_received) {
                        case "301":
                            isPhoneFalse(confirm);
                            break;
                        case "613":
                            isPhoneFalse(confirm);
                            break;
                        case "304":
                            isPhoneTrue(confirm);
                            break;
                        case "305":
                            isPhoneTrue(confirm);
                            break;
                        case "616":
                            isPhoneFalse(confirm);
                            break;
                        case "618":
                            isPhoneFalse(confirm);
                            break;
                        case "306":
                            disHome(confirm);
                            break;
                        case "307":
                            selectedItem = amount_pay.getText().toString();
                            isPhoneTrueSIM(confirm);
                            break;
                        case "634":
                            isPhoneFalse(confirm);
                            break;
                    }
                } catch (Exception e) {
                    new Utility().showSnackbar(v, "Enter valid input to proceed further");
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                }
                break;
            case R.id.picker_visibility:
                phone_number.setText("");
                Intent intent_contact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent_contact, PICK_CONTACT);
                break;
        }
    }

    public void isPhoneFalse(View v) {
        if (selectedItem.length() > 1) {
           /* Intent intent = new Intent(getActivity(), BillpayList.class);
            intent.putExtra("money", selectedItem);
            intent.putExtra("cell_number", phone_number.getText().toString());
            intent.putExtra("byt_code", byte_code);
            intent.putExtra("user_id", id.getText().toString());
            startActivity(intent);
            new Utility().hideSoftkey(getActivity());*/

         /*   final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
            new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                @Override
                public void showDialog() {
                    qPayProgressDialog.show();
                }

                @Override
                public void hideDialog() {
                    qPayProgressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    intent.putExtra("money", selectedItem);
                    intent.putExtra("cell_number", phone_number.getText().toString()"1");
                    intent.putExtra("byt_code", byte_code);
                    intent.putExtra("user_id", id.getText().toString());
                    startActivity(intent);
                    new Utility().hideSoftkey(getActivity());
                }
            }).execute();*/
            if (billPayList) {
                billPayList = false;
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(selectedItem)));

                Bundle bundle = new Bundle();
                bundle.putString("money", selectedItem);
                bundle.putString("byt_code", byte_code);
                bundle.putString("cell_number",  /*phone_number.getText().toString()*/"1");
                bundle.putString("user_id", "");
                BillPayListSheet billPayListSheet = new BillPayListSheet();
                billPayListSheet.setArguments(bundle);
                billPayListSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());
            }
            
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    billPayList=true;
                }
            },2000);

        } else {
            new Utility().showSnackbar(v, "Enter valid input to proceed further");
            new Utility().hideSoftkey(getActivity());
        }
    }

    public void isPhoneTrue(View v) {
        if (phone_number.getText().toString().length() > 8 && phone_number.getText().toString().length() < 11 && selectedItem.length() > 1) {
           /* Intent intent = new Intent(getActivity(), BillpayList.class);
            intent.putExtra("money", selectedItem);
            intent.putExtra("byt_code", byte_code);
            intent.putExtra("cell_number", phone_number.getText().toString());
            intent.putExtra("user_id", id.getText().toString());
            startActivity(intent);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);*/


         /*   final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
            new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                @Override
                public void showDialog() {
                    qPayProgressDialog.show();
                }

                @Override
                public void hideDialog() {
                    qPayProgressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    intent.putExtra("money", selectedItem);
                    intent.putExtra("byt_code", byte_code);
                    intent.putExtra("cell_number", phone_number.getText().toString());
                    intent.putExtra("user_id", id.getText().toString());
                    startActivity(intent);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                }
            }).execute();*/

            if (billPayList) {
                billPayList = false;
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(selectedItem)));
                Bundle bundle = new Bundle();
                bundle.putString("money", selectedItem);
                bundle.putString("byt_code", byte_code);
                bundle.putString("cell_number", id.getText().toString());
                bundle.putString("user_id", "");
                BillPayListSheet billPayListSheet = new BillPayListSheet();
                billPayListSheet.setArguments(bundle);
                billPayListSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
            }
            
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    billPayList=true;
                }
            },2000);

        } else if (id.getText().toString().length() > 1 && selectedItem.length() > 1) {
          /*  Intent intent = new Intent(getActivity(), BillpayList.class);
            intent.putExtra("money", selectedItem);
            intent.putExtra("byt_code", byte_code);
            intent.putExtra("cell_number", phone_number.getText().toString());
            intent.putExtra("user_id", id.getText().toString());
            startActivity(intent);
//            getActivity().finish();
            new Utility().hideSoftkey(getActivity());*/

         /*   GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(selectedItem)));
            Bundle bundle = new Bundle();
            bundle.putString("money", selectedItem);
            bundle.putString("byt_code", byte_code);
            bundle.putString("cell_number", phone_number.getText().toString());
            bundle.putString("user_id", id.getText().toString());
            BillPayListSheet billPayListSheet = new BillPayListSheet();
            billPayListSheet.setArguments(bundle);
            billPayListSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());


            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    billPayList=true;
                }
            },2000);
            */

            /*final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
            new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                @Override
                public void showDialog() {
                    qPayProgressDialog.show();
                }

                @Override
                public void hideDialog() {
                    qPayProgressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    intent.putExtra("money", selectedItem);
                    intent.putExtra("byt_code", byte_code);
                    intent.putExtra("cell_number", phone_number.getText().toString());
                    intent.putExtra("user_id", id.getText().toString());
                    startActivity(intent);
                }
            }).execute();*/
            if (billPayList) {
                billPayList = false;
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(selectedItem)));
                Bundle bundle1 = new Bundle();
                bundle1.putString("money", selectedItem);
                bundle1.putString("byt_code", byte_code);
                bundle1.putString("cell_number", id.getText().toString());
                bundle1.putString("user_id", "");
                BillPayListSheet billPayListSheet1 = new BillPayListSheet();
                billPayListSheet1.setArguments(bundle1);
                billPayListSheet1.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());
            }
            
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    billPayList=true;
                }
            },2000);
            
        } else {
            new Utility().showSnackbar(v, "Enter valid input to proceed further");
            new Utility().hideSoftkey(getActivity());
        }

    }

    private void disHome(View v) {
        if (phone_number.getText().toString().length() > 8 && phone_number.getText().toString().length() < 11 && selectedItem.length() > 1) {
           /* Intent intent = new Intent(getActivity(), BillpayList.class);
            intent.putExtra("money", selectedItem);
            intent.putExtra("byt_code", byte_code);
            intent.putExtra("cell_number", id.getText().toString());
            intent.putExtra("user_id", "");
            startActivity(intent);*/
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);

          /*  final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
            new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                @Override
                public void showDialog() {
                    qPayProgressDialog.show();
                }

                @Override
                public void hideDialog() {
                    qPayProgressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    intent.putExtra("money", selectedItem);
                    intent.putExtra("byt_code", byte_code);
                    intent.putExtra("cell_number", id.getText().toString());
                    intent.putExtra("user_id", "");
                    startActivity(intent);

                }
            }).execute();*/
            if (billPayList) {
                billPayList = false;
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(selectedItem)));
                Bundle bundle = new Bundle();
                bundle.putString("money", selectedItem);
                bundle.putString("byt_code", byte_code);
                bundle.putString("cell_number", id.getText().toString());
                bundle.putString("user_id", "");
                BillPayListSheet billPayListSheet = new BillPayListSheet();
                billPayListSheet.setArguments(bundle);
                billPayListSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());
            }
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    billPayList=true;
                }
            },2000);
            

        } else if (id.getText().toString().length() > 1 && selectedItem.length() > 1) {
           /* Intent intent = new Intent(getActivity(), BillpayList.class);
            intent.putExtra("money", selectedItem);
            intent.putExtra("byt_code", byte_code);
            intent.putExtra("cell_number", id.getText().toString());
            intent.putExtra("user_id", "");
            startActivity(intent);*/
//            getActivity().finish();
            new Utility().hideSoftkey(getActivity());

/*
            final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
            new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                @Override
                public void showDialog() {
                    qPayProgressDialog.show();
                }

                @Override
                public void hideDialog() {
                    qPayProgressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    intent.putExtra("money", selectedItem);
                    intent.putExtra("byt_code", byte_code);
                    intent.putExtra("cell_number", id.getText().toString());
                    intent.putExtra("user_id", "");
                    startActivity(intent);

                }
            }).execute();*/
            if (billPayList) {
                billPayList = false;
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(selectedItem)));
                Bundle bundle = new Bundle();
                bundle.putString("money", selectedItem);
                bundle.putString("byt_code", byte_code);
                bundle.putString("cell_number", id.getText().toString());
                bundle.putString("user_id", "");
                BillPayListSheet billPayListSheet = new BillPayListSheet();
                billPayListSheet.setArguments(bundle);
                billPayListSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());
            }
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    billPayList=true;
                }
            },2000);
            
        } else {
            new Utility().showSnackbar(v, "Enter valid input to proceed further");
            new Utility().hideSoftkey(getActivity());
        }
    }

    public void isPhoneTrueSIM(View v) {
        if (id.getText().toString().length() > 1 && selectedItem.length() > 1) {
            /*Intent intent = new Intent(getActivity(), BillpayList.class);
            intent.putExtra("money", amount_pay.getText().toString());
            intent.putExtra("byt_code", byte_code);
            intent.putExtra("cell_number", phone_number.getText().toString());
            intent.putExtra("user_id", id.getText().toString());
            startActivity(intent);*/
            new Utility().hideSoftkey(getActivity());


           /*  final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
           new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                @Override
                public void showDialog() {
                    qPayProgressDialog.show();
                }

                @Override
                public void hideDialog() {
                    qPayProgressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    intent.putExtra("money", amount_pay.getText().toString());
                    intent.putExtra("byt_code", byte_code);
                    intent.putExtra("cell_number", phone_number.getText().toString());
                    intent.putExtra("user_id", id.getText().toString());
                    startActivity(intent);

                }
            }).execute();*/

            if (billPayList) {
                billPayList=false;
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(amount_pay.getText().toString())));
                Bundle bundle = new Bundle();
                bundle.putString("money", amount_pay.getText().toString());
                bundle.putString("byt_code", byte_code);
                bundle.putString("cell_number", phone_number.getText().toString());
                bundle.putString("user_id", id.getText().toString());
                BillPayListSheet billPayListSheet = new BillPayListSheet();
                billPayListSheet.setArguments(bundle);
                billPayListSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());

            }
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    billPayList=true;
                }
            },2000);

        } else {
            new Utility().showSnackbar(v, "Enter valid input to proceed further");
            new Utility().hideSoftkey(getActivity());
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getActivity().getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            String cNumber = phones.getString(phones.getColumnIndex("data1")).replace("", BuildConfig.FLAVOR).replace("+977", BuildConfig.FLAVOR).replace("-", BuildConfig.FLAVOR);
                            phone_number.setText(cNumber.trim());
                            phone_number.clearFocus();

                            if (cNumber.trim().length() < 9) {
                                phone_number.setSelection(cNumber.trim().length());
                            }

                        }
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    }
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.internet_fragment, new InternetFragment());
                    fragmentTransaction.commit();

                    return true;
                }
                return false;
            }
        });
    }

    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getActivity().getWindow().getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
    }

    public void showDialog() {

    }
}
