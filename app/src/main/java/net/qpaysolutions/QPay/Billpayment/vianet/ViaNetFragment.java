package net.qpaysolutions.QPay.Billpayment.vianet;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.InternetFragment;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/26/18.
 */

public class ViaNetFragment extends Fragment implements DialogInterface {
    private QPayProgressDialog qPayProgressDialog;
    private TextInputLayout inputLayoutId;
    private EditText customerId;
    private LinearLayout searchResultLayout;
    private String amount, officeCode;
    private TextView amountText;
    private boolean billPayList = true;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        qPayProgressDialog = new QPayProgressDialog(getContext());
        inputLayoutId = (TextInputLayout) view.findViewById(R.id.inputLayoutId);
        customerId = (EditText) view.findViewById(R.id.customerId);
        amountText = (TextView) view.findViewById(R.id.amountText);
        customerId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutId.setError(null);
            }
        });
        view.findViewById(R.id.searchVianet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customerId.getText().toString().trim() != null && !customerId.getText().toString().trim().isEmpty()) {
                    new Utility().hideSoftkey(getActivity());
                    new Vianetcheckpayment(ViaNetFragment.this).execute();
                } else {
                    inputLayoutId.setError("Enter Customer Id");
                }
            }
        });
        searchResultLayout = (LinearLayout) view.findViewById(R.id.searchResultLayout);
        view.findViewById(R.id.idSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitTo(amount, officeCode);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.vianet_fragment, container, false);
    }

    @Override
    public void showDialog() {
        qPayProgressDialog.show();
    }

    @Override
    public void hideDialog() {
        qPayProgressDialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (qPayProgressDialog != null) {
            qPayProgressDialog.dismiss();
        }
    }

    public String getUserId() {
        return customerId.getText().toString();
    }

    public void showLayout(final ArrayList<Vinetmodel> jsonArray, String name, String id) {
        TextView customerName = (TextView) getView().findViewById(R.id.customerName);
        TextView custId = (TextView) getView().findViewById(R.id.custId);
        customerName.setText(name);
        custId.setText(id);
        searchResultLayout.setVisibility(View.VISIBLE);
        Spinner spinner = (Spinner) getView().findViewById(R.id.volumePackage);
        SpineerDapterVinet vianetAdapter = new SpineerDapterVinet(getContext(), jsonArray);
        spinner.setAdapter(vianetAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                amount = jsonArray.get(i).getAmount();
                officeCode = jsonArray.get(i).getOfficeCode();
                amountText.setText(amount);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void submitTo(final String amount, final String code) {

        /*Intent intent = new Intent(getActivity(), BillpayList.class);
        intent.putExtra("money",new Decimalformate().decimalFormate(amount));
        intent.putExtra("byt_code", "302");
        intent.putExtra("cell_number", customerId.getText().toString());
        intent.putExtra("user_id", code);
        startActivity(intent);*/

        /*final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
        new CheckRewardAPI(amount, "302", new DialogInterface() {
            @Override
            public void showDialog() {
                qPayProgressDialog.show();
            }

            @Override
            public void hideDialog() {
                qPayProgressDialog.dismiss();
                Intent intent = new Intent(getActivity(), BillpayList.class);
                intent.putExtra("money", new Decimalformate().decimalFormate(amount));
                intent.putExtra("byt_code", "302");
                intent.putExtra("cell_number", customerId.getText().toString());
                intent.putExtra("user_id", code);
                startActivity(intent);
            }
        }).execute();*/
        if (billPayList) {
            billPayList = false;
            GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("302")) / 100) * Double.valueOf(new Decimalformate().decimalFormate(amount))));
            Bundle bundle = new Bundle();
            bundle.putString("money", new Decimalformate().decimalFormate(amount));
            bundle.putString("byt_code", "302");
            bundle.putString("cell_number", customerId.getText().toString());
            bundle.putString("user_id", code);
            BillPayListSheet billPayListSheet = new BillPayListSheet();
            billPayListSheet.setArguments(bundle);
            billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                billPayList = true;
            }
        }, 2000);

    }

    public void showError(String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText("Failed!");
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.internet_fragment, new InternetFragment());
                    fragmentTransaction.commit();

                    return true;
                }
                return false;
            }
        });
    }
}
