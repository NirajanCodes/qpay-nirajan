package net.qpaysolutions.QPay.Billpayment.utility;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.QRcodegenerator.Contents;
import net.qpaysolutions.QPay.QRcodegenerator.QRCodeEncoder;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 5/7/17.
 */

public class TrafficFragment extends Fragment implements View.OnClickListener {
    String id, app_id;
    QPayCustomerDatabase QPayCustomerDatabase;
    String TAG = "dinesh";
    EditText chit_no;
    AppCompatButton lookup_chit;
    SharedPreferences sharedPreferences_location;
    String File = "location";
     String lat, lng;
    SharedPreferences last_traffic_pay;
    String traffic_code,traffic_date,traffic_name,traffic_chit_no,traffic_id_no,traffic_amount;

    ImageView iv_traffic_qr;
    TextView tv_date_traffic,tv_time_traffic,tv_traffic_name,tv_traffic_cheat_no,tv_traffic_id_no,tv_traffic_amount;

    boolean traffic_success;
    LinearLayout ll_traffic;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((UtilitybillPay) getActivity()).setToolbar_text("Traffic fine pay");
        chit_no=(EditText)view.findViewById(R.id.chit_no);
        lookup_chit=(AppCompatButton)view.findViewById(R.id.lookup_chit);
        iv_traffic_qr=(ImageView)view.findViewById(R.id.iv_traffic_qr);
        tv_date_traffic=(TextView)view.findViewById(R.id.tv_date_traffic);
        tv_time_traffic=(TextView)view.findViewById(R.id.tv_time_traffic);
        tv_traffic_name=(TextView)view.findViewById(R.id.tv_traffic_name);
        tv_traffic_cheat_no=(TextView)view.findViewById(R.id.tv_traffic_cheat_no);
        tv_traffic_id_no=(TextView)view.findViewById(R.id.tv_traffic_id_no);
        tv_traffic_amount=(TextView)view.findViewById(R.id.tv_traffic_amount);
        ll_traffic=(LinearLayout)view.findViewById(R.id.ll_traffic);

        lookup_chit.setOnClickListener(this);
        sharedPreferences_location = getActivity().getSharedPreferences(File, Context.MODE_PRIVATE);
        lat = sharedPreferences_location.getString("lat", "");
        lng = sharedPreferences_location.getString("lng", "");
        last_traffic_pay=getActivity().getSharedPreferences("last_pay",Context.MODE_PRIVATE);
        traffic_date=last_traffic_pay.getString("traffic_date","");
        traffic_name=last_traffic_pay.getString("traffic_name","");
        traffic_chit_no=last_traffic_pay.getString("traffic_chit_no","");
        traffic_id_no=last_traffic_pay.getString("traffic_id_no","");
        traffic_amount=last_traffic_pay.getString("traffic_amount","");
        traffic_success=last_traffic_pay.getBoolean("traffic_success",false);
        if (traffic_success){
            ll_traffic.setVisibility(View.VISIBLE);
            tv_date_traffic.setText(new Utils().dateConverter(traffic_date));
            tv_time_traffic.setText(new Utils().timeConverter(traffic_date));
            tv_traffic_name.setText(traffic_name);
            tv_traffic_cheat_no.setText(traffic_chit_no);
            tv_traffic_id_no.setText(traffic_id_no);
            tv_traffic_amount.setText(traffic_amount);
            create(iv_traffic_qr,traffic_chit_no);
        }else {
            ll_traffic.setVisibility(View.GONE);
        }




    }
    public void create(ImageView create_my_qrcode, String code) {
        String qrInputText = null;
        String encryptedMsg = code;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            //Find screen size
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                create_my_qrcode.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getWritableDatabase();
        QPayCustomerDatabase.getReadableDatabase();
        id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.traffic_pay, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    UtilityFragment najirEnglish = new UtilityFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.utility_fragment, najirEnglish);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();


                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lookup_chit:
                new Utils().hideSoftkey(lookup_chit,getActivity());
                if (!chit_no.getText().toString().isEmpty()){
                    new TrafficChitLookup(app_id,id,chit_no.getText().toString(),lat,lng).execute();
                }else {
                    new Utils().customSnackBar(lookup_chit,"Enter Chit Number");
                }

                break;
        }
    }


    public class TrafficChitLookup extends AsyncTask<String, String, String> {
        String appId, id, chitNumber;
        String lat, lng;
        QPayProgressDialog qPayProgressDialog;

        public TrafficChitLookup(String appId, String id, String chitNumber, String lat, String lng) {
            this.appId = appId;
            this.id = id;
            this.chitNumber = chitNumber;
            this.lat = lat;
            this.lng = lng;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String traffic_pay_result = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("chitNumber", chitNumber);
                jsonObject.put("lat", lat);
                jsonObject.put("lng", lng);
                Log.d(TAG, "doInBackground: " + jsonObject);
                traffic_pay_result = networkAPI.sendHTTPData(Constants.TRAFFIC_PAY_LOOKUP, jsonObject);
                Log.d(TAG, "doInBackground: " + traffic_pay_result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return traffic_pay_result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject TrafficBillPaymentResult = jsonObject.getJSONObject("TrafficChitLookUpResult");

                boolean success = TrafficBillPaymentResult.getBoolean("success");
                if (success) {
                    JSONArray data = TrafficBillPaymentResult.getJSONArray("data");
                    JSONObject jsonObject1 = data.getJSONObject(0);

                    String IsPaid = jsonObject1.getString("Status");
                    if (IsPaid.equals("P")){
                         paidTrafficDialog(chitNumber,"Scan above QR code by traffic app for confirmation of payment.");
                    }else if (IsPaid.equals("UP")){
                        TrafficResultFragment trafficResultFragment = new TrafficResultFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.utility_fragment,trafficResultFragment);
                        Bundle bundle = new Bundle();
                        bundle.putString("traffic_lookup_result",jsonObject1.toString());
                        bundle.putString("chit_id",chitNumber);
                        trafficResultFragment.setArguments(bundle);
                        fragmentTransaction.commit();
                    }else if (IsPaid.equals("C")){
                        paidTrafficDialog(chitNumber,"Your payment for this Cheat is already completed.");
                    }

                } else {
//                    new UtilityDialog().dialogFailure("Failure!", "Failed to pay traffic bill.", getActivity());
                   new Utility().showSnackbar(lookup_chit,"Failed to get details of this Cheat.");
                }


            } catch (Exception e) {
                e.printStackTrace();
                new Utility().showSnackbar(lookup_chit,"Failed to get details of this Cheat.");
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }
    }



public void paidTrafficDialog(String chit_id,String message_text){
    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    LayoutInflater inflater = getActivity().getLayoutInflater();
    View v = inflater.inflate(R.layout.traffic_paid_dialog, null);
    final TextView ok_traffic = (TextView) v.findViewById(R.id.ok_traffic);
    final ImageView qr_code =(ImageView)v.findViewById(R.id.qr_code);
    final  TextView message_textTextView=(TextView)v.findViewById(R.id.message_text);
    message_textTextView.setText(message_text);
    create(qr_code,chit_id);
    builder.setView(v);
    builder.setCancelable(false);
    final Dialog dialog = builder.create();
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    v.findViewById(R.id.ok_traffic).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            chit_no.setText("");
            dialog.dismiss();

        }
    });
    dialog.show();
    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    lp.copyFrom(dialog.getWindow().getAttributes());
    float density = getActivity().getResources().getDisplayMetrics().density;
    lp.width = (int) (320 * density);
    lp.height = (int) (340 * density);
    lp.gravity = Gravity.CENTER;
    dialog.getWindow().setAttributes(lp);
}

}
