package net.qpaysolutions.QPay.Billpayment.parbhuTv;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 6/27/18.
 */

public class PrabhuCheckPaymentAPI extends AsyncTask<String, String, String> {
    private String customerId, phone;
    private QPayProgressDialog progressDialog;
    private ParbhuTvFragment parbhuTvFragment;
    private LinearLayout progressBar;

    public PrabhuCheckPaymentAPI(String customerId, String phone, ParbhuTvFragment parbhuTvFragment, LinearLayout progressBar) {
        this.customerId = customerId;
        this.phone = phone;
        this.parbhuTvFragment = parbhuTvFragment;
        this.progressBar = progressBar;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("billPayCode", "310");
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("opt1", customerId);
            jsonObject.put("opt2", phone);
            jsonObject.put("opt3", "");
            Utility.isBuild(jsonObject.toString());
            return NetworkAPI.sendHTTPData(Constants.CHECKUBS_PAYMENT, jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progressDialog = new QPayProgressDialog(parbhuTvFragment.getContext());
//        progressDialog.show();
        progressBar.setVisibility(View.VISIBLE);
        parbhuTvFragment.setClickableFalse();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
//        progressDialog.dismiss();
        progressBar.setVisibility(View.GONE);
        Log.d("dinesh", "check" + s);
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            if (jsonObject.getString("status").equalsIgnoreCase("00")) {
                parbhuTvFragment.loadParbhuTvDetails(s, phone);
                parbhuTvFragment.setClickable();
            } else {
                parbhuTvFragment.dialogFailure("No Response!", "No response found from the server.");
                parbhuTvFragment.setClickable();
            }
        } catch (Exception e) {
            e.printStackTrace();
            parbhuTvFragment.setClickable();
            parbhuTvFragment.dialogFailure("No Response!", "No response found from the server.");
        }
    }
}
