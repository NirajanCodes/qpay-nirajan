package net.qpaysolutions.QPay.Billpayment.inusurance;

import android.os.AsyncTask;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 3/28/18.
 */

public class NepallifeCheckpayment extends AsyncTask<String,String,String> {
    NepalLifeInsuranceFragment nepalLifeInsuranceFragment;

    public NepallifeCheckpayment(NepalLifeInsuranceFragment nepalLifeInsuranceFragment) {
        this.nepalLifeInsuranceFragment = nepalLifeInsuranceFragment;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id",GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            jsonObject.put("billPayCode","701");
            jsonObject.put("opt1", nepalLifeInsuranceFragment.getPolicyNumber());
            jsonObject.put("opt2","");
            jsonObject.put("opt3","");
            Utility.isBuild(jsonObject.toString());
            return NetworkAPI.sendHTTPData(Constants.CHECKUBS_PAYMENT,jsonObject);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        nepalLifeInsuranceFragment.hideDialog();
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("00")){
                String RespCode=jsonObject.getJSONArray("data").getJSONObject(0).getString("RespCode");
                if (RespCode.equals("00")){
                    nepalLifeInsuranceFragment.showLayout(jsonObject.getJSONArray("data").getJSONObject(0));
                }else {
                    nepalLifeInsuranceFragment.showError(jsonObject.getString("message"));
                }

            }else if (status.equals("99")){
                nepalLifeInsuranceFragment.showError("Failed to get data check your connection!");
            }
            else {
                nepalLifeInsuranceFragment.showError(jsonObject.getString("message"));
            }

        }catch (Exception e){
            e.printStackTrace();
            nepalLifeInsuranceFragment.showError("Failed to get data check your connection!");
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        nepalLifeInsuranceFragment.showDialog();
    }
}
