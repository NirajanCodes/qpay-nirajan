package net.qpaysolutions.QPay.Billpayment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import net.qpaysolutions.QPay.Billpayment.ubskhanepani.UBSWaterFragment
import net.qpaysolutions.QPay.R
import net.qpaysolutions.QPay.Utils.GeneralPref

class KhanepaniActivity : AppCompatActivity() {

    private lateinit var ubsKhanepani: LinearLayout
    private lateinit var nepalKhanepani: LinearLayout
    private var toolbar: Toolbar? = null
    private lateinit var toolbarText: TextView
    private lateinit var fragment: LinearLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Utility.setStatusColor(this)
        setContentView(R.layout.activity_khanepani)
        fragment = findViewById(R.id.mainLayout)
        toolbar = findViewById(R.id.toolbar_khanepani)
        toolbarText = findViewById(R.id.title)
        //supportActionBar?.setIcon(R.drawable.ic_back)
        setSupportActionBar(toolbar)
        // supportActionBar?.setIcon(R.drawable.ic_back)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        ubsKhanepani = findViewById(R.id.ubs_khanepani)
        toolbarText.text = "Water Bill Payment"
        nepalKhanepani = findViewById(R.id.nepal_khanepani)
        ubsKhanepani.setOnClickListener(View.OnClickListener {
            val fragmentUbskhanepani: Fragment? = UBSWaterFragment()
            GeneralPref.setWaterInfo("402")
            supportFragmentManager.beginTransaction().replace(R.id.replacememt, fragmentUbskhanepani!!, "").addToBackStack("khanepani").commit()
            toolbarText.text = "UBS Khane Pani"
        })

        nepalKhanepani.setOnClickListener(View.OnClickListener {
            val fragmentUbskhanepani: Fragment? = UBSWaterFragment()
            GeneralPref.setWaterInfo("404")
            supportFragmentManager.beginTransaction().replace(R.id.replacememt, fragmentUbskhanepani!!, "").addToBackStack("khanepani").commit()
            toolbarText.text = "Nepal Khane Pani"
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()


    }
}

