package net.qpaysolutions.QPay.Billpayment.ubskhanepani;

/**
 * Created by deadlydragger on 3/18/18.
 */

public class UBSModel {
    String Office,OfficeCode;

    public String getOffice() {
        return Office;
    }

    public String getOfficeCode() {
        return OfficeCode;
    }

    public void setOffice(String office) {
        Office = office;
    }

    public void setOfficeCode(String officeCode) {
        OfficeCode = officeCode;
    }
}
