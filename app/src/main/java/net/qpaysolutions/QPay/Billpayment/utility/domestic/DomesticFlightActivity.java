package net.qpaysolutions.QPay.Billpayment.utility.domestic;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 5/24/17.
 */

public class DomesticFlightActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView toolbar_text;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.utility_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.utility_fragment,new DomesticFragment())
                .commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    public void  setToolbar_text(String name){
        toolbar_text.setText(name);
    }
}
