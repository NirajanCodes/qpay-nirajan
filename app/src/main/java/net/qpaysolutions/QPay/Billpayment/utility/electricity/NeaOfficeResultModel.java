package net.qpaysolutions.QPay.Billpayment.utility.electricity;

/**
 * Created by deadlydragger on 7/3/17.
 */

public class NeaOfficeResultModel {
    String Code,Name;

    public NeaOfficeResultModel() {
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

}
