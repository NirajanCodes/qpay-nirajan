package net.qpaysolutions.QPay.Billpayment.phonepay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.BuildConfig;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

/**
 * Created by deadlydragger on 1/31/18.
 */

public class MyPhoneFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private LinearLayout amountLayout;
    private TextView phone;
    private String byteCode;
    private String selectedItem = "";
    private static final int PICK_CONTACT = 1;
    private boolean isfine = false;
    private Context mContext;
    private static final int REQUEST = 112;
    private String myPhone;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private boolean billPayList = true;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        myPhone = QPayCustomerDatabase.getCustomerPhone();
        mContext = getActivity();
        amountLayout = (LinearLayout) view.findViewById(R.id.amountLayout);
        amountLayout.setVisibility(View.GONE);
        final Spinner spinner = (Spinner) getView().findViewById(R.id.amount);
        final TextView ncell = (TextView) view.findViewById(R.id.topupNcell);
        final TextView ntcPrepaid = (TextView) view.findViewById(R.id.topupNtc);
        final TextView smartCell = (TextView) view.findViewById(R.id.topupSmartcell);
        final TextView ntpostpaid = (TextView) view.findViewById(R.id.ntcPostPaid);
        final TextView ntcPSTN = (TextView) view.findViewById(R.id.ntcPSTN);
        final TextView utlTextView = (TextView) view.findViewById(R.id.utl);
        ImageView contact_picker = (ImageView) view.findViewById(R.id.contact_picker);
        contact_picker.setOnClickListener(this);
        Button confirm = (Button) view.findViewById(R.id.confirm);
        confirm.setOnClickListener(this);
        phone = (TextView) getView().findViewById(R.id.phone);
        phone.setText(myPhone);


        switch (myPhone.substring(0, 3)) {
            case "980":
            case "981":
            case "982":
                isfine = true;
                byteCode = "201";
                amountLayout.setVisibility(View.VISIBLE);
                ncell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_selected));
                ntcPrepaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                smartCell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntpostpaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPSTN.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                utlTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                new Utility().hideSoftkey(getActivity());
                setNcell(spinner);
                break;

            case "984":
            case "986":
            case "974":
                byteCode = "203";
                isfine = true;
                amountLayout.setVisibility(View.VISIBLE);
                ntcPrepaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_selected));

                ncell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                smartCell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntpostpaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPSTN.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                utlTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                new Utility().hideSoftkey(getActivity());
                setNTC(spinner);
                break;


            case "985":
            case "975":
                isfine = true;
                byteCode = "205";
                amountLayout.setVisibility(View.VISIBLE);
                ntpostpaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_selected));

                ncell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                smartCell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPrepaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPSTN.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                utlTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                new Utility().hideSoftkey(getActivity());
                setNTPostpaid(spinner);
                break;

            case "961":
            case "960":
            case "962":
                isfine = true;
                byteCode = "207";
                amountLayout.setVisibility(View.VISIBLE);
                smartCell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_selected));

                ncell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntpostpaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPrepaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPSTN.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                utlTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                new Utility().hideSoftkey(getActivity());
                setSmartcell(spinner);
                break;
            case "972":
                isfine = true;
                byteCode = "206";
                amountLayout.setVisibility(View.VISIBLE);
                utlTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_selected));

                ncell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntpostpaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPrepaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPSTN.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                smartCell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                new Utility().hideSoftkey(getActivity());
                setUTL(spinner);
                break;
            default:
                isfine = false;
                utlTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ncell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntpostpaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPrepaid.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                ntcPSTN.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                smartCell.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_topuup_nonselected));
                new Utility().hideSoftkey(getActivity());
                break;
        }


        spinner.setOnItemSelectedListener(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_myphone_recharge, container, false);
    }

    private void setNcell(Spinner spinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.ncell_recharge_amount, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

    }

    private void setNTC(Spinner spinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.ntc_prepaid, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void setSmartcell(Spinner spinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.smart_cell, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void setUTL(Spinner spinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.utl, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void setNTPostpaid(Spinner spinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.ntc_postpaid, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void setNTPSTN(Spinner spinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.landline, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedItem = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getActivity().getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            String cNumber = phones.getString(phones.getColumnIndex("data1")).replace("", BuildConfig.FLAVOR).replace("+977", BuildConfig.FLAVOR).replace("-", BuildConfig.FLAVOR);

                            phone.setText(cNumber.trim());
                            phone.clearFocus();
                            /*if (cNumber.trim().length() < 11) {

                                phone.setSelection(cNumber.trim().length());
                            }*/

                        }
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    }
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.contact_picker:
                phone.setText("");

                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {android.Manifest.permission.READ_CONTACTS};
                    if (!((PhonePayActivity) getActivity()).hasPermissions(mContext, PERMISSIONS)) {
                        ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                        startActivityForResult(intent, PICK_CONTACT);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, PICK_CONTACT);
                }
                break;
            case R.id.confirm:
                if (phone.getText().toString().length() >= 9 && isfine == true) {
                   /* Intent intent = new Intent(getContext(), BillpayList.class);
                    intent.putExtra("money", selectedItem);
                    intent.putExtra("cell_number", phone.getText().toString());
                    intent.putExtra("byt_code", byteCode);
                    startActivity(intent);
                    getActivity().finish();*/
                    System.out.println("Percentage:: " + Utility.parseRebate(byteCode));
                    try {
                        GeneralPref.setRewardAmount(String.valueOf(Double.parseDouble(selectedItem) * (Double.parseDouble(Utility.parseRebate(byteCode)) / 100)));


                    if (billPayList) {
                        billPayList = false;
                        GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byteCode)) / 100) * Double.valueOf(selectedItem)));
                        BillPayListSheet bottomSheetFragment = new BillPayListSheet();
                        Bundle bundle = new Bundle();
                        bundle.putString("money", selectedItem);
                        bundle.putString("cell_number", phone.getText().toString());
                        bundle.putString("byt_code", byteCode);
                        bottomSheetFragment.setArguments(bundle);
                        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                    }

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            billPayList = true;
                        }
                    }, 2000);
                    } catch (Exception e) {

                    }

//                    final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
//                    new CheckRewardAPI(selectedItem, byteCode, new DialogInterface() {
//                        @Override
//                        public void showDialog() {
//
//                            qPayProgressDialog.show();
//
//                        }
//
//                        @Override
//                        public void hideDialog() {
//                            qPayProgressDialog.dismiss();
//                            BillPayListSheet bottomSheetFragment = new BillPayListSheet();
//                            Bundle bundle = new Bundle();
//                            bundle.putString("money", selectedItem);
//                            bundle.putString("cell_number", phone.getText().toString());
//                            bundle.putString("byt_code", byteCode);
//                            bottomSheetFragment.setArguments(bundle);
//                            bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
////                            ntent intent = new Intent(getContext(), BillpayList.class);
////                            intent.putExtra("money", selectedItem);
////                            intent.putExtra("cell_number", phone.getText().toString());
////                            intent.putExtra("byt_code", byteCode);
////                            startActivity(intent);
////                            getActivity().finish();I
//                        }
//                    }).execute();

                } else {
                    phone.setError("Enter valid number");
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.top_of_all, new PhoneFragment());
                    fragmentTransaction.commit();

                    return true;
                }
                return false;
            }
        });
    }


}
