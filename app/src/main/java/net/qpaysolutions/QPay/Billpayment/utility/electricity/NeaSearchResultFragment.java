package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.CustumClasses.DecimalTextMasking;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 7/5/17.
 */

public class NeaSearchResultFragment extends Fragment implements View.OnClickListener {

    private String nea_result;
    private TextView customer_name, amount, service_charge, total_charge, due_details;
    private Button submit_nea_payment;
    private String total_price;
    private String customer_id, office_code, sc_no;
    private String ServiceCharge;
    private CheckBox checkboxAdvancePayment;
    private EditText advanceAmountNea;
    private String total_amount;
    private boolean billPayList = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
      /*
      if (b==true){
                    advanceAmountNea.setEnabled(true);
                    total_price="";
                }{
                    advanceAmountNea.setEnabled(false);
                }
      */

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customer_name = (TextView) view.findViewById(R.id.customer_name);
        amount = (TextView) view.findViewById(R.id.amount);
        service_charge = (TextView) view.findViewById(R.id.service_charge);
        total_charge = (TextView) view.findViewById(R.id.total_charge);
        due_details = (TextView) view.findViewById(R.id.due_details);
        nea_result = getArguments().getString("nea_result");
        customer_id = getArguments().getString("customer_id");
        office_code = getArguments().getString("office_code");
        sc_no = getArguments().getString("sc_no");
        Log.d("dinesh", "onViewCreated: " + nea_result);
        submit_nea_payment = (Button) view.findViewById(R.id.submit_nea_payment);
        due_details.setOnClickListener(this);
        submit_nea_payment.setOnClickListener(this);
        checkboxAdvancePayment = (CheckBox) view.findViewById(R.id.checkboxAdvancePayment);
        advanceAmountNea = (EditText) view.findViewById(R.id.advanceAmountNea);
        advanceAmountNea.setEnabled(false);
        advanceAmountNea.addTextChangedListener(new DecimalTextMasking(advanceAmountNea));
        checkboxAdvancePayment.setChecked(false);
        checkboxAdvancePayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    advanceAmountNea.setEnabled(true);
                } else {
                    advanceAmountNea.setEnabled(false);
                }
            }
        });

        try {
            JSONObject jsonObject = new JSONObject(nea_result);
            JSONArray data = jsonObject.getJSONArray("data");
            JSONObject jsonObject1 = data.getJSONObject(0);
            String CustName = jsonObject1.getString("CustName");
            int index = CustName.indexOf("No");
            if (index != -1) {
                Log.d(Utility.TAG, "index : " + index);
                CustName = (String) CustName.subSequence(0, index - 1);
            }
            String DueAmount = new Decimalformate().decimalFormate(jsonObject1.getString("DueAmount"));
            ServiceCharge = new Decimalformate().decimalFormate(jsonObject1.getString("ServiceCharge"));
            customer_name.setText(CustName);
            amount.setText("NPR " + DueAmount);
            service_charge.setText("NPR " + ServiceCharge);
            total_amount = new Decimalformate().decimalFormate(String.valueOf(Double.parseDouble(jsonObject1.getString("DueAmount")) + Double.parseDouble(jsonObject1.getString("ServiceCharge"))));
            total_charge.setText("NPR " + total_amount);
            total_price = new Decimalformate().decimalFormate(String.valueOf(Double.parseDouble(jsonObject1.getString("DueAmount")) + Double.parseDouble(jsonObject1.getString("ServiceCharge"))));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nea_result_fragment, container, false);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submit_nea_payment:
                try {
                    if (!checkboxAdvancePayment.isChecked()) {
                        advanceAmountNea.setText("0.00");

                    }
                    String payingAmout = "0.00";
                    if (!checkboxAdvancePayment.isChecked() && (advanceAmountNea.getText().toString() == null && Double.valueOf(advanceAmountNea.getText().toString()) < 1)) {
                        payingAmout = total_price;
                    } else if (checkboxAdvancePayment.isChecked()) {
                        payingAmout = advanceAmountNea.getText().toString();
                        payingAmout = payingAmout.replace(",", "");
                    } else {
                        payingAmout = total_price;
                    }
                    if (payingAmout == null || payingAmout.equalsIgnoreCase("")) {
                        payingAmout = "0.00";
                    }
                    GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("401")) / 100) * Double.valueOf(payingAmout)));
                    if ((Double.valueOf(payingAmout) > 0)) {
                        if (!checkboxAdvancePayment.isChecked()) {
                            Utility.isBuild("Price Default :" + total_price);
//                        final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
//                        new CheckRewardAPI(total_price, "401", new DialogInterface() {
//                            @Override
//                            public void showDialog() {
//                                qPayProgressDialog.show();
//                            }
//
//                            @Override
//                            public void hideDialog() {
//                                qPayProgressDialog.dismiss();
//                                Intent intent = new Intent(getActivity(), BillpayList.class);
//                                intent.putExtra("money", new Decimalformate().decimalFormate(total_price));
//                                intent.putExtra("byt_code", "401");
//                                intent.putExtra("cell_number", customer_id);
//                                intent.putExtra("user_id", office_code);
//                                intent.putExtra("cash_Id", sc_no);//sc_number
//                                intent.putExtra("addAmount1", ServiceCharge);
//                                startActivity(intent);
//                                getActivity().finish();
//                            }
//                        }).execute();

                            try {

                       /*     GeneralPref.setRewardAmount(String.valueOf(Double.parseDouble(new Decimalformate().decimalFormate(total_price)) *
                                    (Double.parseDouble(Utility.parseRebate("401")) / 100)));*/

                                if (billPayList) {
                                    billPayList = false;
                                    BillPayListSheet bottomSheetFragment = new BillPayListSheet();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("money", payingAmout);
                                    bundle.putString("byt_code", "401");
                                    bundle.putString("cell_number", customer_id);
                                    bundle.putString("user_id", office_code);
                                    bundle.putString("cash_Id", sc_no);
                                    bundle.putString("addAmount1", ServiceCharge);
                                    bottomSheetFragment.setArguments(bundle);
                                    bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(() -> billPayList = true, 2000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            if (Double.valueOf(payingAmout) > Double.valueOf(total_amount)) {
                                Utility.isBuild("Price Manual :" +payingAmout);
//                             final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
////                            new CheckRewardAPI(advanceAmountNea.getText().toString(), "401", new DialogInterface() {
//                                @Override
//                                public void showDialog() {
//                                    qPayProgressDialog.show();
//                                }
//
//                                @Override
//                                public void hideDialog() {
//                                    qPayProgressDialog.dismiss();
//                                    Intent intent = new Intent(getActivity(), BillpayList.class);
//                                    intent.putExtra("money", new Decimalformate().decimalFormate(advanceAmountNea.getText().toString()));
//                                    intent.putExtra("byt_code", "401");
//                                    intent.putExtra("cell_number", customer_id);
//                                    intent.putExtra("user_id", office_code);
//                                    intent.putExtra("cash_Id", sc_no);//sc_number
//                                    intent.putExtra("addAmount1", ServiceCharge);
//                                    startActivity(intent);
//                                    getActivity().finish();
//                                }
//                            }).execute();


                                try {

                         /*       GeneralPref.setRewardAmount(String.valueOf(Double.parseDouble((advanceAmountNea.getText().toString()))*
                                        (Double.parseDouble(Utility.parseRebate("401")) / 100)));*/
                                    if (billPayList) {
                                        billPayList = false;
                                        BillPayListSheet bottomSheetFragment = new BillPayListSheet();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("money", new Decimalformate().decimalFormate(payingAmout));
                                        bundle.putString("byt_code", "401");
                                        bundle.putString("cell_number", customer_id);
                                        bundle.putString("user_id", office_code);
                                        bundle.putString("cash_Id", sc_no);
                                        bundle.putString("addAmount1", ServiceCharge);
                                        bottomSheetFragment.setArguments(bundle);
                                        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                                    }
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            billPayList = true;
                                        }
                                    }, 2000);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                advanceAmountNea.setError("Enter advanced amount more than Total Amount ");
                            }
                        }
                    } else {
                        new Utility().showSnackbar(advanceAmountNea, "Enter Amount.");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    new Utility().showSnackbar(advanceAmountNea, " Techinical issue .");

                }

                break;
            case R.id.due_details:
                Intent dueIntent = new Intent(getActivity(), DueDetailsActivity.class);
                dueIntent.putExtra("nea_result", nea_result);

                startActivity(dueIntent);

                break;
        }
    }
}
