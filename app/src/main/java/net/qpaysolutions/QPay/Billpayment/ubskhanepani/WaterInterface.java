package net.qpaysolutions.QPay.Billpayment.ubskhanepani;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 3/26/18.
 */

public interface WaterInterface {
    void showDetailsUBS(JSONObject object);
    void showDetailsNepalKhanepani(JSONObject object);

}
