package net.qpaysolutions.QPay.Billpayment.wordlink;

/**
 * Created by deadlydragger on 6/19/17.
 */

public class VolumeModel
{
    String DiscountedRate,OptionId,Rate,VolumnPackage;
    boolean ischecked;

    public boolean isIschecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }

    public VolumeModel() {
    }

    public String getDiscountedRate() {
        return DiscountedRate;
    }

    public void setDiscountedRate(String discountedRate) {
        DiscountedRate = discountedRate;
    }

    public String getOptionId() {
        return OptionId;
    }

    public void setOptionId(String optionId) {
        OptionId = optionId;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getVolumnPackage() {
        return VolumnPackage;
    }

    public void setVolumnPackage(String volumnPackage) {
        VolumnPackage = volumnPackage;
    }
}
