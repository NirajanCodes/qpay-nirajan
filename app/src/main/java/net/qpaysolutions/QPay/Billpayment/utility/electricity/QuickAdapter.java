package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;

public class QuickAdapter extends RecyclerView.Adapter<QuickAdapter.ViewHolder> {

    ArrayList<String[]> arrayList;
    String[] customerId;
    NeaQuickInterface neaQuickInterface;

    public QuickAdapter(ArrayList<String[]> arrayList, NeaQuickInterface neaQuickInterface) {

        this.arrayList = arrayList;
        Log.d(Utility.TAG, "customerId:  from adapter " + customerId);
        try {
            customerId = (String[]) arrayList.get(0);
            String value = customerId[0].toString();
            this.neaQuickInterface = neaQuickInterface;
        } catch (Exception e) {

        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quick_nea, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(Utility.TAG, "position : " + position);
        if (customerId[position] != null) {
            holder.tvCustomerId.setText(customerId[position].toString());
        }
        holder.pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                neaQuickInterface.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {

        return customerId.length;

    }

    public class ViewHolder extends DueDetailsAdapter.ViewHolder {
        TextView tvCustomerId,pay;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCustomerId = itemView.findViewById(R.id.customer_id);
            pay=itemView.findViewById(R.id.pay);
        }
    }


}
