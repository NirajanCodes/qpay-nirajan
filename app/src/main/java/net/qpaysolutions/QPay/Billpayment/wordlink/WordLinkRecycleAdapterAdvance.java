package net.qpaysolutions.QPay.Billpayment.wordlink;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;


import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 6/25/17.
 */

public class WordLinkRecycleAdapterAdvance extends RecyclerView.Adapter<WordLinkRecycleAdapterAdvance.VolumeBasedViewHolder> {
    ArrayList<VolumeModel> volumeModels;
    AdvanceVolumeFragment volumeFragment;
    private static CheckBox lastChecked = null;
    private static int lastCheckedPos = 0;
    public WordLinkRecycleAdapterAdvance(ArrayList<VolumeModel> volumeModels, AdvanceVolumeFragment volumeFragment) {
        this.volumeModels = volumeModels;
        this.volumeFragment = volumeFragment;
    }

    @Override
    public VolumeBasedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.volume_item, parent, false);
        VolumeBasedViewHolder volumeBasedViewHolder = new VolumeBasedViewHolder(view);
        return volumeBasedViewHolder;
    }

    @Override
    public void onBindViewHolder(final VolumeBasedViewHolder holder, final int position) {
        holder.Rate.setText(volumeModels.get(position).getRate());
        holder.discountedRate.setText(volumeModels.get(position).getDiscountedRate());
        holder.volumePackage.setText(volumeModels.get(position).getVolumnPackage());

        //for default check in first item
        if(position == 0 && volumeModels.get(0).isIschecked() && holder.checkbox.isChecked())
        {
            lastChecked = holder.checkbox;
            lastCheckedPos = 0;
        }

        holder.checkbox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CheckBox cb = (CheckBox)v;
                int clickedPos =position;

                if(cb.isChecked())
                {
                    if(lastChecked != null)
                    {
                        lastChecked.setChecked(false);
                        volumeModels.get(lastCheckedPos).setIschecked(false);
                        notifyItemChanged(lastCheckedPos);
                    }

                    lastChecked = cb;
                    lastCheckedPos = clickedPos;
                    volumeModels.get(clickedPos).setIschecked(true);
                    volumeFragment.setAmount(volumeModels.get(clickedPos).getDiscountedRate());
                    volumeFragment.getOptionId(volumeModels.get(clickedPos).getOptionId());
                }
                else{
                    lastChecked = null;
                    volumeModels.get(clickedPos).setIschecked(false);
                }


                notifyItemChanged(clickedPos);

//                fonts.get(clickedPos).setSelected(cb.isChecked);

            }
        });



        if (volumeModels.get(position).isIschecked()){
            holder.checkbox.setChecked(true);

        }else {
            holder.checkbox.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return volumeModels.size();
    }

    class VolumeBasedViewHolder extends RecyclerView.ViewHolder  {
        TextView volumePackage, discountedRate, Rate;
        LinearLayout volume_based;
        CheckBox checkbox;


        public VolumeBasedViewHolder(View itemView) {
            super(itemView);
            volumePackage = (TextView) itemView.findViewById(R.id.volumePackage);
            discountedRate = (TextView) itemView.findViewById(R.id.DiscountedRate);
            Rate = (TextView) itemView.findViewById(R.id.Rate);
            volume_based = (LinearLayout) itemView.findViewById(R.id.volume_based);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);

        }
    }
}
