package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by deadlydragger on 5/7/17.
 */

public class NeaFragment extends Fragment implements View.OnClickListener {

    private String appId, id, location_duty, Code;
    private QPayCustomerDatabase qpayMerchantDatabase;
    private QPayProgressDialog qPayProgressDialog;
    EditText firs_three, nea_customer;
    private TextView reagion;
    private ArrayList<NeaOfficeResultModel> trafficPrecincts;
    private List<String> list = new ArrayList<>();
    private String[] stockArr;
    private String[] region;
    private String[] scNo;
    private String[] customerId;
    private Button check_payment;
    private View vieww;
    private QPayProgressDialog progressBar;
    private RecyclerView reclyclerViewQuickPay;
    private RelativeLayout quickLayout;
    LinearLayout main;
    private TextView textNew;
    RelativeLayout top;
    boolean flag = true;
    boolean firstTime = false;
    boolean added = false;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        vieww = view;
        firs_three = (EditText) view.findViewById(R.id.firs_three);
        nea_customer = (EditText) view.findViewById(R.id.nea_customer);
        check_payment = (Button) view.findViewById(R.id.check_payment);
        reclyclerViewQuickPay = view.findViewById(R.id.quick_recyler);
        progressBar = new QPayProgressDialog(getContext());
        new NeaOfficeResult().execute();
        top = view.findViewById(R.id.frameLayout);
        check_payment.setOnClickListener(this);
        quickLayout = view.findViewById(R.id.quick_view);
        main = view.findViewById(R.id.main);
        main.setVisibility(View.VISIBLE);
        try {
            trafficPrecincts = Tags.getPreferencesArray();
            if (trafficPrecincts != null) {
                firstTime = true;
            }
            for (int i = 0; i < trafficPrecincts.size(); i++) {
                list.add(trafficPrecincts.get(i).getName());
                added = true;
            }
            stockArr = new String[list.size()];
            stockArr = list.toArray(stockArr);
            setLocation_duty(vieww);
        } catch (Exception e) {

        }
        if (trafficPrecincts != null) {
            firstTime = false;
        }
        textNew = view.findViewById(R.id.btn_new);
        bottomPopUp();
        quickWork();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        qpayMerchantDatabase.getReadableDatabase();
        appId = qpayMerchantDatabase.getKeyAppId();
        id = qpayMerchantDatabase.getCustomerID();
        ArrayList<String[]> arrayList = qpayMerchantDatabase.getQuickNeaInfo();
        if (arrayList.get(0).length > 0) {
            customerId = arrayList.get(0);
            region = arrayList.get(1);
            scNo = arrayList.get(2);
        }
    }


    public void setLocation_duty(View view) {

        reagion = view.findViewById(R.id.reagion);
        reagion.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(getActivity(), WindowManager.LayoutParams.MATCH_PARENT);
            dialog.setContentView(R.layout.dialog_full_screen);
            RecyclerView recyclerView = dialog.findViewById(R.id.districtRecyclerView);
            SearchView searchView = dialog.findViewById(R.id.searchView);
            searchView.setIconifiedByDefault(true);
            searchView.setFocusable(true);
            searchView.setIconified(false);

            EditText search = searchView.findViewById(R.id.search_src_text);
            search.setTextColor(getResources().getColor(R.color.white));
            search.setHintTextColor(getResources().getColor(android.R.color.white));

            ImageView imageView = searchView.findViewById(R.id.search_close_btn);
            imageView.setImageResource(R.drawable.ic_action_cancel);

            ImageView closeBranch = searchView.findViewById(R.id.search_button);
            closeBranch.setImageResource(R.drawable.ic_search_black_24dp);

            try {

                Field mDrawable = SearchView.class.getDeclaredField("mSearchHintIcon");
                mDrawable.setAccessible(true);
                Drawable drawable = (Drawable) mDrawable.get(search);

                //removes the search hint while searching
                drawable.setBounds(0, 0, 0, 0);

            } catch (Exception e) {
                e.printStackTrace();
            }

            final NeaAdapter neaAdapter = new NeaAdapter(list, district -> {
                try {
                    reagion.setText(district);
                    dialog.dismiss();
                    location_duty = district;
                    for (int j = 0; j < trafficPrecincts.size(); j++) {
                        if (location_duty.equals(trafficPrecincts.get(j).getName())) {
                            Code = trafficPrecincts.get(j).getCode();
                            Log.d("dinesh", "onItemSelected: " + location_duty + " code " + Code);
                        }
                    }
                } catch (Exception e) {

                }
            });

            RecyclerView.LayoutManager mLayoutManagerTo = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManagerTo);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(neaAdapter);
            dialog.show();

            ImageView backArrowImageViews = dialog.findViewById(R.id.backArrowImageView);
            backArrowImageViews.setOnClickListener(v1 -> dialog.dismiss());
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    neaAdapter.getFilter().filter(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    neaAdapter.getFilter().filter(newText);
                    return true;
                }
            });
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nea_bill_pay, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.check_payment:
                new Utility().hideSoftkey(getActivity());
                quickLayout.setVisibility(View.GONE);
                String svc = firs_three.getText().toString();
                String nea_code = nea_customer.getText().toString();
                if (svc != null && !svc.isEmpty() && nea_code != null && !nea_code.isEmpty()) {
                    new NeaPayment(NeaFragment.this, appId, id, "401", Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng()), nea_code, Code, svc, progressBar, qpayMerchantDatabase).execute();
                } else {
                    new Utility().showSnackbar(check_payment, "Enter valid information.");
                }
                break;
        }
    }

    private void bottomPopUp() {
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(Objects.requireNonNull(getView()).findViewById(R.id.quick_view));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        if (customerId == null || customerId.length < 1) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            quickLayout.setVisibility(View.GONE);
        } else {
            quickLayout.setVisibility(View.VISIBLE);
        }
        textNew.setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            quickLayout.setVisibility(View.GONE);
        });
        top.setOnClickListener(v -> {
            if (flag) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                flag = false;
                quickLayout.setVisibility(View.GONE);
            } else {
                if (customerId == null || customerId.length < 1) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    quickLayout.setVisibility(View.GONE);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    quickLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }, 1000);
    }

    public void quickWork() {

        if (customerId != null && customerId.length > 0) {
            main.setVisibility(View.VISIBLE);
            quickLayout.setVisibility(View.VISIBLE);
        }

        final NeaQuickInterface neaQuickInterface = new NeaQuickInterface() {
            @Override
            public void onClick(int position) {
                Log.d("dinesh", " location duty" + location_duty + " code " + Code + " scNo " + scNo[0].toString() + " customer Id " + customerId[0].toString());
                quickLayout.setVisibility(View.GONE);
                new NeaPayment(NeaFragment.this, appId, id, "401", Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng()), customerId[position].toString(), region[position].toString(), scNo[position].toString(), progressBar, qpayMerchantDatabase).execute();
            }
        };
        QuickAdapter quickAdapter = new QuickAdapter(qpayMerchantDatabase.getQuickNeaInfo(), neaQuickInterface);
        reclyclerViewQuickPay.setAdapter(quickAdapter);
        reclyclerViewQuickPay.setLayoutManager(new LinearLayoutManager(getContext()));
        reclyclerViewQuickPay.setNestedScrollingEnabled(false);
        reclyclerViewQuickPay.setOverScrollMode(0);
    }

    public class NeaOfficeResult extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String nea_office = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("billPayCode", 401);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLng());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d("dinesh", "doInBackground: " + jsonObject);
                nea_office = networkAPI.sendHTTPData(Constants.NEA_OFFICE, jsonObject);
                Log.d("dinesh", "doInBackground nea office :" + nea_office);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return nea_office;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (firstTime) {
                    qPayProgressDialog.dismiss();
                }
                if (s != null || !s.isEmpty()) {
                    new Tags().setSharedPreferencesArray(s);
                }
                trafficPrecincts = Tags.getPreferencesArray();

                if (added == false) {
                    for (int i = 0; i < trafficPrecincts.size(); i++) {
                        list.add(trafficPrecincts.get(i).getName());
                    }
                }
                stockArr = new String[list.size()];
                stockArr = list.toArray(stockArr);
                setLocation_duty(vieww);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            if (firstTime) {
                qPayProgressDialog.show();
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                   /* FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.utility_fragment, new UtilityFragment());
                    fragmentTransaction.commit();*/
                getActivity().onBackPressed();
                return true;
            }
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                return false;
            }
            return true;


        });
    }
}
