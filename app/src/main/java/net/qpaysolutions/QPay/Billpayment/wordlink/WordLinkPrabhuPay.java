package net.qpaysolutions.QPay.Billpayment.wordlink;

import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatSpinner;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.Billpayment.sky.SpineerSkycableAdapter;
import net.qpaysolutions.QPay.Billpayment.vianet.Vinetmodel;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by deadlydragger on 11/15/18.
 */

public class WordLinkPrabhuPay extends Fragment {
    String amount = "", planeId = "", username = "", customerNameString = "";
    private boolean billPayList = true;
    ImageView logo;
    TextView title;
    LinearLayout payment_message_view, tvUserName,amountLayout;
    String userId, switchInternet;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String wordlink = getArguments().getString("wordlink");
        userId = getArguments().getString("user_id");
        switchInternet = getArguments().getString("switchInternet");
        Log.d("dinesh", wordlink);
        TextView customerId = view.findViewById(R.id.customerId);
        TextView customerName = view.findViewById(R.id.customerName);
        TextView billAmount = view.findViewById(R.id.billAmount);
        TextView tvAmount = view.findViewById(R.id.amount);
        logo = view.findViewById(R.id.iv_word_link);
        title = view.findViewById(R.id.iv_word_text);
        payment_message_view = view.findViewById(R.id.payment_message_view);
        amountLayout=view.findViewById(R.id.amountLayout);
        tvUserName = view.findViewById(R.id.user_name);
        LinearLayout billamountLayout = view.findViewById(R.id.billamountLayout);
        TextView message = view.findViewById(R.id.message);
        String tvbillAmount = "";
        try {
            JSONObject object1 = new JSONObject(wordlink);
            JSONArray jsonArray = object1.getJSONArray("data");
            JSONObject object = jsonArray.getJSONObject(0);
            customerName.setText(object.getString("CustomerName"));
            customerNameString = object.getString("CustomerName");
            if (switchInternet != null && Objects.requireNonNull(switchInternet.equalsIgnoreCase("WorldLink"))) {
                customerId.setText(object.getString("WlinkUserName"));
                amountLayout.setVisibility(View.GONE);
                username = object.getString("WlinkUserName");
                message.setText(object1.getString("message"));
                JSONArray RenewalPlans = object.getJSONArray("RenewalPlans");
                final ArrayList<Vinetmodel> list = new ArrayList<>();
                if (RenewalPlans.length() > 0) {
                    billamountLayout.setVisibility(View.GONE);
                    for (int i = 0; i < RenewalPlans.length(); i++) {
                        JSONObject jsonObject = RenewalPlans.getJSONObject(i);
                        Vinetmodel vinetmodel = new Vinetmodel();
                        vinetmodel.setServiceType(jsonObject.getString("PlanName"));
                        vinetmodel.setAmount(jsonObject.getString("PlanAmount"));
                        vinetmodel.setOfficeCode(jsonObject.getString("PlanId"));
                        list.add(vinetmodel);
                    }
                    AppCompatSpinner spinner = view.findViewById(R.id.choosePlane);
                    SpineerSkycableAdapter vianetAdapter = new SpineerSkycableAdapter(getContext(), list);
                    spinner.setAdapter(vianetAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            planeId = list.get(i).getOfficeCode();
                            amount = list.get(i).getAmount();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                } else {
                    amount=object.getString("BillAmount");
                    billamountLayout.setVisibility(View.VISIBLE);
                    billAmount.setText(object.getString("BillAmount"));
                }
            } else if (switchInternet != null && Objects.requireNonNull(switchInternet.equalsIgnoreCase("WebSurfer"))) {
                title.setText("Web Surfer");
                tvUserName.setVisibility(View.GONE);
                logo.setImageDrawable(getResources().getDrawable(R.drawable.websurfer));
                payment_message_view.setVisibility(View.GONE);
                JSONArray RenewalPlans = object.getJSONArray("Plan");
                final ArrayList<Vinetmodel> list = new ArrayList<>();
                if (RenewalPlans.length() > 0) {
                    billamountLayout.setVisibility(View.GONE);
                    for (int i = 0; i < RenewalPlans.length(); i++) {
                        JSONObject jsonObject = RenewalPlans.getJSONObject(i);
                        Vinetmodel vinetmodel = new Vinetmodel();
                        vinetmodel.setServiceType(jsonObject.getString("PlanName"));
                        vinetmodel.setAmount(jsonObject.getString("Amount"));
                        vinetmodel.setOfficeCode(jsonObject.getString("PlanId"));
                        list.add(vinetmodel);
                    }
                    AppCompatSpinner spinner = view.findViewById(R.id.choosePlane);
                    SpineerSkycableAdapter vianetAdapter = new SpineerSkycableAdapter(getContext(), list);
                    spinner.setAdapter(vianetAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            planeId = list.get(i).getOfficeCode();
                            amount = list.get(i).getAmount();
                            tvAmount.setText(amount);
                            Log.d(Utility.TAG, "amount : : " + list.get(0).getAmount() + "plan Id" + list.get(0).getOfficeCode());

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                            amount = list.get(0).getAmount();
                        }
                    });
                    // Log.d(Utility.TAG,"amount : : "+ list.get(0).getAmount() +"plan Id" + list.get(0).getOfficeCode());
                    tvAmount.setText(amount);
                    billAmount.setText(object.getString("BillAmount"));
                    billamountLayout.setVisibility(View.VISIBLE);
                } else {

                }
            } else if (switchInternet != null && Objects.requireNonNull(switchInternet.equalsIgnoreCase("Arrownet"))) {
                title.setText("Arrownet");
                customerId.setText(object.getString("ArrowNetUserName"));
                username = object.getString("ArrowNetUserName");
                //message.setText(object1.getString("message"));
                tvUserName.setVisibility(View.GONE);
                logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrownet));
                payment_message_view.setVisibility(View.GONE);
                JSONArray RenewalPlans = object.getJSONArray("Plan");
                final ArrayList<Vinetmodel> list = new ArrayList<>();
                if (RenewalPlans.length() > 0) {
                    billamountLayout.setVisibility(View.GONE);
                    for (int i = 0; i < RenewalPlans.length(); i++) {
                        JSONObject jsonObject = RenewalPlans.getJSONObject(i);
                        Vinetmodel vinetmodel = new Vinetmodel();
                        vinetmodel.setServiceType(jsonObject.getString("PlanName"));
                        vinetmodel.setAmount(jsonObject.getString("Amount"));
                        vinetmodel.setOfficeCode(jsonObject.getString("PlanId"));
                        list.add(vinetmodel);
                    }
                    AppCompatSpinner spinner = view.findViewById(R.id.choosePlane);
                    SpineerSkycableAdapter vianetAdapter = new SpineerSkycableAdapter(getContext(), list);
                    spinner.setAdapter(vianetAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            planeId = list.get(i).getOfficeCode();
                            amount = list.get(i).getAmount();
                            tvAmount.setText(amount);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    tvAmount.setText(amount);
                    billamountLayout.setVisibility(View.VISIBLE);
                    billAmount.setText(object.getString("BillAmount"));
                } else {
                    billamountLayout.setVisibility(View.VISIBLE);
                    billAmount.setText(object.getString("BillAmount"));
                    amount = object.getString("BillAmount");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        view.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToPay();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wordlink_prabhupay, container, false);
    }

    public void goToPay() {
        if (billPayList) {
            billPayList = false;
            Bundle bundle = new Bundle();
            if (switchInternet != null && Objects.requireNonNull(switchInternet.equalsIgnoreCase("WorldLink"))) {
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("303")) / 100) * Double.valueOf(amount)));
                bundle.putString("money", amount);
                bundle.putString("byt_code", "303");
                bundle.putString("cell_number", username);
                bundle.putString("user_id", planeId);
            } else if (switchInternet != null && Objects.requireNonNull(switchInternet.equalsIgnoreCase("WebSurfer"))) {
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("314")) / 100) * Double.valueOf(amount)));
                bundle.putString("money", amount);
                bundle.putString("byt_code", "314");
                bundle.putString("cell_number", userId);
                bundle.putString("user_id", planeId);
            } else if (switchInternet != null && Objects.requireNonNull(switchInternet.equalsIgnoreCase("Arrownet"))) {
                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("315")) / 100) * Double.valueOf(amount)));
                bundle.putString("money", amount);
                bundle.putString("byt_code", "315");
                bundle.putString("cell_number", username);
                bundle.putString("user_id", planeId);
            }
            BillPayListSheet billPayListSheet = new BillPayListSheet();
            billPayListSheet.setArguments(bundle);
            billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
        }
        Handler handler = new Handler();
        handler.postDelayed(() -> billPayList = true, 2000);
       /* final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
        new CheckRewardAPI(amount, "303", new DialogInterface() {
            @Override
            public void showDialog() {
                qPayProgressDialog.show();
            }

            @Override
            public void hideDialog() {
                qPayProgressDialog.dismiss();
                Intent intent = new Intent(getActivity(), BillpayList.class);
                intent.putExtra("money", amount);
                intent.putExtra("byt_code", "303");
                intent.putExtra("cell_number", username);
                intent.putExtra("user_id", planeId);
                startActivity(intent);
                getActivity().finish();
            }

        }).execute();*/
    }
}
