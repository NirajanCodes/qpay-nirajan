package net.qpaysolutions.QPay.Billpayment.vianet;

/**
 * Created by deadlydragger on 3/28/18.
 */

public class Vinetmodel {
    String Amount,OfficeCode,ServiceType;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getOfficeCode() {
        return OfficeCode;
    }

    public void setOfficeCode(String officeCode) {
        OfficeCode = officeCode;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }
}
