package net.qpaysolutions.QPay.Billpayment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.utility.electricity.NeaFragment;
import net.qpaysolutions.QPay.Card.AddCardFragment;
import net.qpaysolutions.QPay.Card.AddManageCardFragment;
import net.qpaysolutions.QPay.Card.CardListFragment;
import net.qpaysolutions.QPay.Card.CardProfileMain;
import net.qpaysolutions.QPay.Card.TopUpFragment;
import net.qpaysolutions.QPay.Card.ebanking.BankListFragment;
import net.qpaysolutions.QPay.Card.ebanking.DepositEBankingFragment;
import net.qpaysolutions.QPay.CustomerKYC.KYCDetailsModel;
import net.qpaysolutions.QPay.CustomerKYC.KYCFragment;
import net.qpaysolutions.QPay.CustomerKYC.KYC_check_fragment;
import net.qpaysolutions.QPay.Deal.DealFragment;
import net.qpaysolutions.QPay.Faq.FaqFragment;
import net.qpaysolutions.QPay.FragmentCollection.AboutUsFragment;
import net.qpaysolutions.QPay.FragmentCollection.ContactUsFragment;
import net.qpaysolutions.QPay.FragmentCollection.InviteFriends;
import net.qpaysolutions.QPay.MyCode.MyCodeFragment;
import net.qpaysolutions.QPay.MyCode.Resetpasscode;
import net.qpaysolutions.QPay.MyCode.SetCodeFragment;
import net.qpaysolutions.QPay.NearByAtms.NearbyAtmsFragment;
import net.qpaysolutions.QPay.Notification.NotificationViewer;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.RechargePin.RechargePin;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Withdrawal.WithdrawalFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by QPay on 6/10/2018.
 */

public class BillPayActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public TextView title;
    private String id;
    public static List<Fragment> fragmentList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_payment);
        toolbar = findViewById(R.id.toolbar);
        title = findViewById(R.id.title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        id = getIntent().getStringExtra("id");
        Log.d("id", "" + id);

//        if (id.equalsIgnoreCase("rechargePin")) {
//            title.setText("Recharge Pin History");
//            addFragmentToActivity(getSupportFragmentManager(), new RechargePin(), R.id.bill_payment_container, "sC");
//
//        }
//        if (id.equalsIgnoreCase("withdrawalPin")) {
//            title.setText("Withdrawal Pin History");
//            addFragmentToActivity(getSupportFragmentManager(), new MoneyTransferWithdrawFragment(), R.id.bill_payment_container, "sC");
//        }
//        if (id.equals("setCode")) {
//            title.setText("Set Code");
//            addFragmentToActivity(getSupportFragmentManager(), new SetCodeFragment(), R.id.bill_payment_container, "sC");
//        }

        //changed world link flow as required.
       /* if (id.equalsIgnoreCase("wordlink")) {
            title.setText("Worldlink Payment");
            String data = getIntent().getStringExtra("wordlink");
            Bundle bundle = new Bundle();
            bundle.putString("wordlink", data);
            WordLinkPrabhuPay fragobj = new WordLinkPrabhuPay();
            fragobj.setArguments(bundle);


            addFragmentToActivity(getSupportFragmentManager(),fragobj, R.id.bill_payment_container, "wordlink");
        }*/

        if (id.equals("quick")) {
            title.setText("Quick Pay");
            Intent quickPayActivity = new Intent(this, QuickPayActivity.class);
            startActivity(quickPayActivity);
        }

        if (id.equals("eb")) {
            title.setText("E-Banks");
            BankListFragment bankListFragment = new BankListFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEb", true);
            bankListFragment.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), bankListFragment, R.id.bill_payment_container, "eb");
        }
        if (id.equalsIgnoreCase("mobileBanking")) {
            title.setText("Mobile Banking");
            BankListFragment bankListFragment = new BankListFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEb", false);
            bankListFragment.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), bankListFragment, R.id.bill_payment_container, "mobileBanking");
        }

        if (id.equals("atm")) {
            title.setText("ATM");
            addFragmentToActivity(getSupportFragmentManager(), new NearbyAtmsFragment(), R.id.bill_payment_container, "atm");
        }

        if (id.equals("topoff")) {
            title.setText("Top-Up");
            TopUpFragment topUpFragment = new TopUpFragment();
            Bundle bundle = new Bundle();
            bundle.putString("id", String.valueOf(getIntent().getStringExtra("cardId")));
            topUpFragment.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), topUpFragment, R.id.bill_payment_container, "topoff");
        }

        if (id.equals("ebanking")) {
            title.setText("E-Banking");
            DepositEBankingFragment depositEBankingFragment = new DepositEBankingFragment();
            Bundle bundle = new Bundle();
            bundle.putString("gateway", getIntent().getStringExtra("gateway"));
            bundle.putString("name", getIntent().getStringExtra("name"));
            bundle.putString("url", getIntent().getStringExtra("url"));
            depositEBankingFragment.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), depositEBankingFragment, R.id.bill_payment_container, "eb");
        }

        if (id.equals("mbanking")) {
            title.setText("Mobile Banking");
            DepositEBankingFragment depositEBankingFragment = new DepositEBankingFragment();
            Bundle bundle = new Bundle();
            bundle.putString("gateway", getIntent().getStringExtra("gateway"));
            bundle.putString("name", getIntent().getStringExtra("name"));
            bundle.putString("url", getIntent().getStringExtra("url"));
            depositEBankingFragment.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), depositEBankingFragment, R.id.bill_payment_container, "eb");
        }

        if (id.equals("faq")) {
            title.setText("FAQ");
            addFragmentToActivity(getSupportFragmentManager(), new FaqFragment(), R.id.bill_payment_container, "faq");
        }

        if (id.equals("invite")) {
            title.setText("Invite Friends");
            InviteFriends inviteFriends = new InviteFriends();
            Bundle bundle = new Bundle();
            bundle.putString("number", getIntent().getStringExtra("number"));
            inviteFriends.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), inviteFriends, R.id.bill_payment_container, "invite");
        }

        if (id.equals("code")) {

            title.setText("My Code");
            MyCodeFragment myCodeFragment = new MyCodeFragment();
            if (getIntent().getStringExtra("title") != null) {
                title.setText(getIntent().getStringExtra("title"));
            }
            Bundle bundle = new Bundle();
            bundle.putString("flag", "code");
            myCodeFragment.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), myCodeFragment, R.id.bill_payment_container, "code");

        }

        if (id.equals("notif")) {
            title.setText("Notification");
            addFragmentToActivity(getSupportFragmentManager(), new NotificationViewer(), R.id.bill_payment_container, "notif");
        }

        if (id.equals("deal")) {
            title.setText("Nearby Deals");
            addFragmentToActivity(getSupportFragmentManager(), new DealFragment(), R.id.bill_payment_container, "deal");
        }

        if (id.equals("settings")) {
            title.setText("Settings");
            MyCodeFragment settings = new MyCodeFragment();
            Bundle settingBundle = new Bundle();
            settingBundle.putString("flag", "settings");
            settings.setArguments(settingBundle);
            addFragmentToActivity(getSupportFragmentManager(), settings, R.id.bill_payment_container, "settings");
        }

        if (id.equals("contact")) {
            title.setText("Contact Us");
            addFragmentToActivity(getSupportFragmentManager(), new ContactUsFragment(), R.id.bill_payment_container, "settings");
        }

        if (id.equalsIgnoreCase("kycDetails")) {
            KYCDetailsModel kycDetailsModel = new KYCDetailsModel();
            System.out.println("KYC:: " + GeneralPref.getKYC());
            if (GeneralPref.getKYC().equalsIgnoreCase("I") || GeneralPref.getKYC().equalsIgnoreCase("A")) {

                title.setText("Kyc Details");
                addFragmentToActivity(getSupportFragmentManager(), new KYC_check_fragment(), R.id.bill_payment_container, "KYC");

            } else {
                title.setText("Kyc Details");
                addFragmentToActivity(getSupportFragmentManager(), new KYCFragment(), R.id.bill_payment_container, "KYC");
            }
        }

        if (id.equals("sct")) {
            if (getIntent().getStringExtra("flag").equals("sct")) {
                title.setText("Add Card");
                AddCardFragment addCardFragment = new AddCardFragment();
                Bundle bundle = new Bundle();
                bundle.putString("flag", "sct");
                bundle.putString("card_num", getIntent().getStringExtra("card_num"));
                addCardFragment.setArguments(bundle);
                addFragmentToActivity(getSupportFragmentManager(), addCardFragment, R.id.bill_payment_container, CardListFragment.TAG);
            } else if (getIntent().getStringExtra("flag").equalsIgnoreCase("upi")) {
                title.setText("Add UPI Card");
                android.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                AddCardFragment addCardFragment = new AddCardFragment();
                Bundle bundle = new Bundle();
                bundle.putString("flag", "upi_card");
                bundle.putString("card_num", getIntent().getStringExtra("card_num"));
                addCardFragment.setArguments(bundle);
                addFragmentToActivity(getSupportFragmentManager(), addCardFragment, R.id.bill_payment_container, CardListFragment.TAG);
            } else {

                title.setText("Visa/Master Card");
                DepositEBankingFragment depositEBankingFragment = new DepositEBankingFragment();
                Bundle bundle = new Bundle();
                //             bundle.putString("gateway", getIntent().getStringExtra("gateway"));
                bundle.putString("name", "other");
//              bundle.putString("url", getIntent().getStringExtra("url"));
                depositEBankingFragment.setArguments(bundle);
                addFragmentToActivity(getSupportFragmentManager(), depositEBankingFragment, R.id.bill_payment_container, CardListFragment.TAG);

            }
        }

//        if (id.equals("KYC")) {
//            title.setText("Submit Your KYC Form");
//            addFragmentToActivity(getSupportFragmentManager(), new KYCFragment(), R.id.bill_payment_container, "KYC");
//        }
//        if (id.equals("kyc")) {
//            title.setText("KYC Form");
//            addFragmentToActivity(getSupportFragmentManager(), new KYCFragment(), R.id.bill_payment_container, "KYC");
//        }

        if (id.equals("connect_ips")) {
            title.setText("Connect IPS");
            DepositEBankingFragment depositEBankingFragment = new DepositEBankingFragment();
            Bundle bundle = new Bundle();
            bundle.putString("name", "connect_ips");
            depositEBankingFragment.setArguments(bundle);
            addFragmentToActivity(getSupportFragmentManager(), depositEBankingFragment, R.id.bill_payment_container, "others");
        }
        if (id.equals("add_card")) {
            title.setText("My Cards");
            addFragmentToActivity(getSupportFragmentManager(), new AddManageCardFragment(), R.id.bill_payment_container, AddManageCardFragment.TAG);
        }
        if (id.equals("addCardManageCard")) {
            title.setText("My Cards");
            addFragmentToActivity(getSupportFragmentManager(), new AddManageCardFragment(), R.id.bill_payment_container, AddManageCardFragment.TAG);
        }
        if (id.equalsIgnoreCase("nea")) {
            title.setText("Electricity Bill");
            addFragmentToActivity(getSupportFragmentManager(), new NeaFragment(), R.id.bill_payment_container, "nea bill");
        }
        if (id.equalsIgnoreCase("rechargePin")) {
            title.setText("Recharge Pin");
            addFragmentToActivity(getSupportFragmentManager(), new RechargePin(), R.id.bill_payment_container, "recharge pin");
        }
        if (id.equalsIgnoreCase("withdrawalPin")) {
            title.setText("Withdraw Pin");
            addFragmentToActivity(getSupportFragmentManager(), new WithdrawalFragment(), R.id.bill_payment_container, "withdraw pin");
        }
        if (id.equals("cardTopup")) {
            title.setText("Card Topup");
            addFragmentToActivity(getSupportFragmentManager(), new CardProfileMain(), R.id.bill_payment_container, "cardTopup");
        }
        if (id.equals("about")) {
            title.setText("About Us");
            addFragmentToActivity(getSupportFragmentManager(), new AboutUsFragment(), R.id.bill_payment_container, "aboutUs");
        }
        if (id.equalsIgnoreCase("setCode")) {
            title.setText(("Set Code"));
            String pin_l;
            QPayCustomerDatabase qPayCustomerDatabase = new QPayCustomerDatabase(this);
            pin_l = qPayCustomerDatabase.getPin();
            if (pin_l != null && !qPayCustomerDatabase.getFlagPin().equalsIgnoreCase("0")) {
                Resetpasscode codeFragment = new Resetpasscode();
            /* FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, codeFragment);
            Bundle bundle=new Bundle();
            bundle.putChar("main",'m');
            codeFragment.setArguments(bundle);
            transaction.commit();
            setTitle("Set Passcode");
           */
                Bundle bundle = new Bundle();
                bundle.putString("old_pin", "text");
                codeFragment.setArguments(bundle);
                addFragmentToActivity(getSupportFragmentManager(), codeFragment, R.id.bill_payment_container, "setCode");
            } else if (pin_l != null && qPayCustomerDatabase.getPin().equalsIgnoreCase("0")) {
                this.finishAffinity();

            } else {
                SetCodeFragment setCodeFragment = new SetCodeFragment();
                addFragmentToActivity(getSupportFragmentManager(), setCodeFragment, R.id.bill_payment_container, "setCode");
            }
        }
    }

    private void addFragmentToActivity(FragmentManager fragmentManager, Fragment fragment, int fragmentId, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(fragmentId, fragment);
        transaction.commit();
    }

    public void setTitle(String text) {
        title.setText(text);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void loadFragment(final Fragment frag, final String tag) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.bill_payment_container);
                if (fragment == null) {
                    ft.add(R.id.bill_payment_container, frag, tag);
                } else {
                    ft.replace(R.id.bill_payment_container, frag, tag);
                }
                ft.addToBackStack(tag);
                ft.commit();
            }
        }, 0);
    }

   /* @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        onBackPressed();
        *//*Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);*//*

    }*/
}
