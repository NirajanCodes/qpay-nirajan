package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import java.io.Serializable;

/**
 * Created by CSharp05-user on 16/07/2017.
 */

public class DueDetailsInfo implements Serializable{

  String BillAmount,BillDate,DueBillOf,NoOfDays,PayableAmount,Status;

    public DueDetailsInfo() {
    }

    public String getBillAmount() {
        return BillAmount;
    }

    public void setBillAmount(String billAmount) {
        BillAmount = billAmount;
    }

    public String getBillDate() {
        return BillDate;
    }

    public void setBillDate(String billDate) {
        BillDate = billDate;
    }

    public String getDueBillOf() {
        return DueBillOf;
    }

    public void setDueBillOf(String dueBillOf) {
        DueBillOf = dueBillOf;
    }

    public String getNoOfDays() {
        return NoOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        NoOfDays = noOfDays;
    }

    public String getPayableAmount() {
        return PayableAmount;
    }

    public void setPayableAmount(String payableAmount) {
        PayableAmount = payableAmount;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
