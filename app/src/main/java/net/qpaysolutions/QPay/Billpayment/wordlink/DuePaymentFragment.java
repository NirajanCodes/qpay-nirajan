package net.qpaysolutions.QPay.Billpayment.wordlink;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillpayList;
import net.qpaysolutions.QPay.CASHBACK.CheckRewardAPI;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 6/18/17.
 */

public class DuePaymentFragment extends Fragment implements View.OnClickListener{
    String word_link;
    TextView amount,disc_amount;
    Button confirm;
    String DiscountedAmount,opt1;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        word_link = getArguments().getString("wordlink");
        opt1=getArguments().getString("opt1");
        TextView  username=(TextView)view.findViewById(R.id.username);
        username.setText(GeneralPref.getUsernameWordlink());
        try {
            JSONObject jsonObject1=new JSONObject(word_link);
            String Amount = new Decimalformate().decimalFormate(jsonObject1.getString("Amount"));
            DiscountedAmount =new Decimalformate().decimalFormate( jsonObject1.getString("DiscountedAmount"));
//            JSONArray VolumnType = jsonObject1.getJSONArray("VolumnType");
            amount=(TextView)view.findViewById(R.id.amount);
            disc_amount=(TextView)view.findViewById(R.id.discounted_amount);
            amount.setText("NPR"+Amount);
            disc_amount.setText("NPR"+DiscountedAmount);
            confirm=(Button)view.findViewById(R.id.confirm);
            confirm.setOnClickListener(this);
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.due_wordlink,container,false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
               /* Intent intent = new Intent(getActivity(), BillpayList.class);
                intent.putExtra("money", DiscountedAmount);
                intent.putExtra("byt_code", "303");
                intent.putExtra("cell_number", opt1);
                intent.putExtra("user_id","");
                startActivity(intent);*/

                final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
                new CheckRewardAPI(DiscountedAmount, "303", new DialogInterface() {
                    @Override
                    public void showDialog() {
                        qPayProgressDialog.show();
                    }

                    @Override
                    public void hideDialog() {
                        qPayProgressDialog.dismiss();
                        Intent intent = new Intent(getActivity(), BillpayList.class);
                        intent.putExtra("money", DiscountedAmount);
                        intent.putExtra("byt_code", "303");
                        intent.putExtra("cell_number", opt1);
                        intent.putExtra("user_id","");
                        startActivity(intent);

                    }
                }).execute();
                break;
        }
    }
}
