package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSharp05-user on 16/07/2017.
 */

public class DueDetailsActivity extends AppCompatActivity {

    private RecyclerView dueDetailsRecyclerView;
    private DueDetailsAdapter dueDetailsAdapter;
    private List<DueDetailsInfo> dueDetailsInfoList=new ArrayList<>();
    private  String nea_result;
    private Toolbar toolbar_bill_pay;
   private TextView toolbar_text;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.due_details_activity);
        toolbar_bill_pay=(Toolbar)findViewById(R.id.toolbar);
        toolbar_text=(TextView)findViewById(R.id.title);
        setSupportActionBar(toolbar_bill_pay);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_text.setText("NEA Due Details");
        nea_result=getIntent().getStringExtra("nea_result");
        Log.d("dinesh", "onCreate: "+nea_result);
        try {
            JSONObject jsonObject = new JSONObject(nea_result);
            JSONArray data = jsonObject.getJSONArray("data");
            JSONObject jsonObject1 = data.getJSONObject(0);
            JSONArray Details = jsonObject1.getJSONArray("Details");
            for (int i=0;i<Details.length();i++){
                DueDetailsInfo dueDetailsInfo = new DueDetailsInfo();
                JSONObject object = Details.getJSONObject(i);
                dueDetailsInfo.setBillAmount("NPR "+new Decimalformate().decimalFormate(object.getString("BillAmount")));
                dueDetailsInfo.setBillDate(object.getString("BillDate"));
                dueDetailsInfo.setDueBillOf(object.getString("DueBillOf"));
                dueDetailsInfo.setNoOfDays(object.getString("NoOfDays"));
                dueDetailsInfo.setStatus(object.getString("Status"));
                dueDetailsInfo.setPayableAmount("NPR "+new Decimalformate().decimalFormate(object.getString("PayableAmount")));
                dueDetailsInfoList.add(dueDetailsInfo);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        dueDetailsRecyclerView = (RecyclerView) findViewById(R.id.due_details_recycler_view_id);
        setDueDetailsAdapter();
    }
    private void setDueDetailsAdapter() {
        dueDetailsAdapter = new DueDetailsAdapter(DueDetailsActivity.this, dueDetailsInfoList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DueDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        dueDetailsRecyclerView.setLayoutManager(mLayoutManager);
        dueDetailsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        dueDetailsRecyclerView.setAdapter(dueDetailsAdapter);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
