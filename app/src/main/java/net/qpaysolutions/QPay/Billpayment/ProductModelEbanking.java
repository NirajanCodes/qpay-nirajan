package net.qpaysolutions.QPay.Billpayment;

public class ProductModelEbanking {
    String Id,name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    int icon;

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public ProductModelEbanking() {
    }

    public ProductModelEbanking(String id, int icon, String name) {
        Id = id;
        this.name = name;
        this.icon = icon;
    }
}
