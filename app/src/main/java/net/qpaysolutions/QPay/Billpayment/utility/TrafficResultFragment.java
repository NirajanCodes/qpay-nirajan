package net.qpaysolutions.QPay.Billpayment.utility;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Billpayment.BillpayList;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.QRcodegenerator.Contents;
import net.qpaysolutions.QPay.QRcodegenerator.QRCodeEncoder;
import net.qpaysolutions.QPay.Utils.Decimalformate;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 6/27/17.
 */

public class TrafficResultFragment extends Fragment implements View.OnClickListener {
    String traffic_lookup_result, chitNumber, appId, id, DriverName, Amount, ReasonForCitation, IdentificationNo, Date, Precinct, TrafficName, VehicleNo;
    TextView fine_payrs_name, date, chit_expiry_date, amount, fine_reason, chit_id;
    ImageView traffic_qr;
    Button pay_traffic;
    QPayCustomerDatabase qpayMerchantDatabase;
    boolean IsPaid;
    QPayProgressDialog qPayProgressDialog;
    SharedPreferences last_traffic_pay;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.traffic_pay_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fine_payrs_name = (TextView) view.findViewById(R.id.fine_payrs_name);
        date = (TextView) view.findViewById(R.id.date);
        chit_expiry_date = (TextView) view.findViewById(R.id.chit_expiry_date);
        amount = (TextView) view.findViewById(R.id.amount);
        fine_reason = (TextView) view.findViewById(R.id.fine_reason);
        chit_id = (TextView) view.findViewById(R.id.chit_id);
        traffic_qr = (ImageView) view.findViewById(R.id.traffic_qr);
        pay_traffic = (Button) view.findViewById(R.id.pay_traffic);
        pay_traffic.setOnClickListener(this);
        traffic_lookup_result = getArguments().getString("traffic_lookup_result");
        chitNumber = getArguments().getString("chit_id");
        try {
            JSONObject jsonObject = new JSONObject(traffic_lookup_result);
            Amount =new Decimalformate().decimalFormate(jsonObject.getString("Amount"));
            Date = jsonObject.getString("Date");
            DriverName = jsonObject.getString("DriverName");
            IsPaid = jsonObject.getBoolean("IsPaid");
            Precinct = jsonObject.getString("Precinct");
            ReasonForCitation = jsonObject.getString("ReasonForCitation");
            IdentificationNo = jsonObject.getString("IdentificationNo");
            TrafficName = jsonObject.getString("TrafficName");
            VehicleNo = jsonObject.getString("VehicleNo");
            fine_payrs_name.setText(DriverName);
            date.setText(Date);
            chit_expiry_date.setText(Date);
            amount.setText("NPR " + Amount);
            fine_reason.setText(ReasonForCitation);
            chit_id.setText(chitNumber);
            create(traffic_qr, chitNumber);
              setTrafficHistory(Date,DriverName,chitNumber,IdentificationNo,Amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTrafficHistory(String traffic_date,String traffic_name,String traffic_chit_no,String traffic_id_no,String traffic_amount){
        last_traffic_pay=getActivity().getSharedPreferences("last_pay",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = last_traffic_pay.edit();
        editor.putBoolean("traffic_success",false);
        editor.putString("traffic_date",traffic_date);
        editor.putString("traffic_name",traffic_name);
        editor.putString("traffic_chit_no",traffic_chit_no);
        editor.putString("traffic_id_no",traffic_id_no);
        editor.putString("traffic_amount",traffic_amount);
        editor.apply();
    }

    public void create(ImageView create_my_qrcode, String code) {
        String qrInputText = null;
        String encryptedMsg = code;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            //Find screen size
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                create_my_qrcode.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        qpayMerchantDatabase.getReadableDatabase();
        appId = qpayMerchantDatabase.getKeyAppId();
        id = qpayMerchantDatabase.getCustomerID();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_traffic:
                Intent intent = new Intent(getActivity(), BillpayList.class);
                intent.putExtra("money", Amount);
                intent.putExtra("byt_code", "403");
                intent.putExtra("cell_number", chitNumber);
                intent.putExtra("user_id","");
                startActivity(intent);
//                new TrafficPay().execute();
                break;
        }
    }


}
