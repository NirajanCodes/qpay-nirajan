package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.List;

public class NeaAdapter extends RecyclerView.Adapter<NeaAdapter.NeaHolder> implements Filterable {
    private List<String> list;
    private NeaClickListener neaClickListener;
    private  ArrayList<String> filteredList;
    NeaAdapter(List<String> list, NeaClickListener neaClickListener) {
        this.list=list;
        this.neaClickListener = neaClickListener;

        this.filteredList = (ArrayList<String>) list;
    }


    @NonNull
    @Override
    public NeaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NeaHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_district, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NeaHolder holder, int position) {
        holder.districtName.setText(filteredList.get(position));
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charlist = constraint.toString();
                ArrayList<String> filterableList = new ArrayList<>();
                if (charlist.isEmpty()) {
                    filterableList = (ArrayList<String>) list;
                } else {
                    for (String loopList : list) {
                        if (loopList.toUpperCase().contains(charlist.toUpperCase())) {
                                filterableList.add(loopList);
                        }
                    }
                   filteredList= filterableList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values=filteredList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    class NeaHolder extends RecyclerView.ViewHolder {
        private TextView districtName;

        public NeaHolder(View itemView) {
            super(itemView);
            districtName = itemView.findViewById(R.id.districtName);
            districtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    neaClickListener.neaClick(filteredList.get(getAdapterPosition()));
                }
            });

        }
    }

    public interface NeaClickListener {
        void neaClick(String item);
    }
}
