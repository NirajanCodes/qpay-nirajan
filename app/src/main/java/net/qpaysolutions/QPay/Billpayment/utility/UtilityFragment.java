package net.qpaysolutions.QPay.Billpayment.utility;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.CASHBACK.PrefCashBack;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Billpayment.inusurance.NepalLifeInsuranceFragment;
import net.qpaysolutions.QPay.Billpayment.utility.electricity.NeaFragment;
import net.qpaysolutions.QPay.Billpayment.ubskhanepani.UBSWaterFragment;
import net.qpaysolutions.QPay.Flights.flightfragment.FlightMainActivity;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Utils.UtilityDialog;

/**
 * Created by deadlydragger on 5/5/17.
 */

public class UtilityFragment extends Fragment implements View.OnClickListener {
    private QPayCustomerDatabase QPayCustomerDatabase;
    private   String cust_id, app_id;
    boolean allowBonus;
    double lat, lon;
    private  LinearLayout electricity, khanepani, flights, traffic,nepalLife,nepalKhanepani;
    private NeaFragment neaFragment;
    private UBSWaterFragment khanepaniFragment;
    private TrafficFragment trafficFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.utility_bill_frame, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((UtilitybillPay) getActivity()).setToolbar_text("Utility Bill Pay");
        allowBonus = GeneralPref.getAllowBonus();
        lat = ((CustomerApplication) getActivity().getApplication()).getLat();
        lon = ((CustomerApplication) getActivity().getApplication()).getLng();
        electricity = (LinearLayout) view.findViewById(R.id.electricity);
        khanepani = (LinearLayout) view.findViewById(R.id.khanepani);
        flights = (LinearLayout) view.findViewById(R.id.flights);
        traffic = (LinearLayout) view.findViewById(R.id.traffic);
        nepalLife=(LinearLayout)view.findViewById(R.id.nepalLife);
        nepalKhanepani=(LinearLayout)view.findViewById(R.id.nepalKhanepani);
        nepalKhanepani.setOnClickListener(this);
        flights.setOnClickListener(this);
        khanepani.setOnClickListener(this);
        electricity.setOnClickListener(this);
        traffic.setOnClickListener(this);
        nepalLife.setOnClickListener(this);
        TextView discount=(TextView)view.findViewById(R.id.discountRate) ;
        TextView serviceProviderTextView=(TextView)view.findViewById(R.id.serviceProvider);
        LinearLayout cashBackLayoutLinearLayout=(LinearLayout)view.findViewById(R.id.cashBackLayout);
        Utility.setDiscount(discount,serviceProviderTextView,new PrefCashBack(getContext()).getOTHER(),cashBackLayoutLinearLayout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.electricity:
                if (allowBonus) {
                    neaFragment = new NeaFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.utility_fragment, neaFragment)
                            .commit();
                } else {
                    new UtilityDialog().custumdialogfailure(getActivity());
                }

                break;
            case R.id.khanepani:
                if (allowBonus) {
                    GeneralPref.setWaterInfo("402");
                    khanepaniFragment = new UBSWaterFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.utility_fragment, khanepaniFragment)
                            .commit();
                } else {
                    new UtilityDialog().custumdialogfailure(getActivity());
                }

                break;
            case R.id.nepalKhanepani:
                if (allowBonus) {
                    GeneralPref.setWaterInfo("404");
                    khanepaniFragment = new UBSWaterFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.utility_fragment, khanepaniFragment)
                            .commit();
                } else {
                    new UtilityDialog().custumdialogfailure(getActivity());
                }
                break;
            case R.id.flights:
                if (allowBonus) {

                    startActivity(new Intent(getActivity(), FlightMainActivity.class));
                } else {
                    new UtilityDialog().custumdialogfailure(getActivity());
                }

                break;
            case R.id.traffic:
                if (allowBonus) {
                    trafficFragment = new TrafficFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.utility_fragment, trafficFragment)
                            .commit();
                } else {
                    new UtilityDialog().custumdialogfailure(getActivity());
                }
                break;
            case R.id.nepalLife:
                if (allowBonus) {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.utility_fragment, new NepalLifeInsuranceFragment())
                            .commit();
                } else {
                    new UtilityDialog().custumdialogfailure(getActivity());
                }
                break;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        ((UtilitybillPay)getActivity()).setUtilityViible(isVisibleToUser);
    }
}
