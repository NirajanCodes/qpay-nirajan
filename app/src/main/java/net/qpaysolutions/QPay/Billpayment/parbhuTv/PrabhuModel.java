package net.qpaysolutions.QPay.Billpayment.parbhuTv;

/**
 * Created by deadlydragger on 7/1/18.
 */

public class PrabhuModel {
    String BillAmount, MacVcNumber, ProductId, ProductName, SerialNumber, StockId;
      boolean isCheck;

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getBillAmount() {
        return BillAmount;
    }

    public void setBillAmount(String billAmount) {
        BillAmount = billAmount;
    }

    public String getMacVcNumber() {
        return MacVcNumber;
    }

    public void setMacVcNumber(String macVcNumber) {
        MacVcNumber = macVcNumber;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getStockId() {
        return StockId;
    }

    public void setStockId(String stockId) {
        StockId = stockId;
    }
}
