package net.qpaysolutions.QPay.Billpayment;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by QPay on 6/10/2018.
 */

public class LoadFundAdapters extends RecyclerView.Adapter<LoadFundAdapters.LoadFundViewHolder> {

    private Context context;
    private ArrayList<ProductModelEbanking> products;
    private LoadFundOnClickListener listener;

    public LoadFundAdapters(Context context, ArrayList<ProductModelEbanking> products, LoadFundOnClickListener listener) {
        this.context = context;
        this.products = products;
        this.listener=listener;
    }

    @NonNull
    @Override
    public LoadFundViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_load_fund, parent, false);
        return new LoadFundViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LoadFundViewHolder holder, final int position) {

        holder.icon.setImageResource(products.get(position).getIcon());
        holder.title.setText(products.get(position).getName());
        holder.loadFundLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.loadFundlistener(Integer.parseInt(products.get(position).getId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class LoadFundViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon;
        private TextView title;
        private RelativeLayout loadFundLayout;

        public LoadFundViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.loadFundImageView);
            title = (TextView) itemView.findViewById(R.id.loadFundTitle);
            loadFundLayout = (RelativeLayout) itemView.findViewById(R.id.loadFundLayout);
        }
    }

    public interface LoadFundOnClickListener{
        void loadFundlistener(int position);
    }
}
