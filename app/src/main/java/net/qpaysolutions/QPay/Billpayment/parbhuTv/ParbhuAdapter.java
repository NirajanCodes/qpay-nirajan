package net.qpaysolutions.QPay.Billpayment.parbhuTv;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 7/1/18.
 */

public class ParbhuAdapter extends RecyclerView.Adapter<ParbhuAdapter.PrabhuViewHolder> {
    ArrayList<PrabhuModel> arrayList;
    private static CheckBox lastChecked = null;
    private static int lastCheckedPos = 0;
    PrabhuTvDetailsFragment prabhuTvDetailsFragment;

    public ParbhuAdapter(ArrayList<PrabhuModel> arrayList,PrabhuTvDetailsFragment prabhuTvDetailsFragment) {
        this.arrayList = arrayList;
        this.prabhuTvDetailsFragment=prabhuTvDetailsFragment;
    }

    @NonNull
    @Override
    public PrabhuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PrabhuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prabhu_spineer,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull PrabhuViewHolder holder, final int position) {
        holder.productName.setText(arrayList.get(position).getProductName());
        holder.billAmount.setText("NPR "+arrayList.get(position).getBillAmount());
        holder.checkbox.setChecked(arrayList.get(position).isCheck());
        //for default check in first item
        if(position == 0 && arrayList.get(0).isCheck() && holder.checkbox.isChecked())
        {
            lastChecked = holder.checkbox;
            lastCheckedPos = 0;
        }

        holder.checkbox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CheckBox cb = (CheckBox)v;
                int clickedPos =position;
//                prabhuTvDetailsFragment.setCustomerId(arrayList.get(position).get);
                if(cb.isChecked())
                {
                    if(lastChecked != null)
                    {
                        lastChecked.setChecked(false);
                        arrayList.get(lastCheckedPos).setCheck(false);
                        notifyItemChanged(lastCheckedPos);
                    }
                    lastChecked = cb;
                    lastCheckedPos = clickedPos;
                    arrayList.get(clickedPos).setCheck(true);
                }
                else{
                    lastChecked = null;
                    arrayList.get(clickedPos).setCheck(false);
                }

                notifyItemChanged(clickedPos);

            }
        });

        if (arrayList.get(position).isCheck()){
            holder.checkbox.setChecked(true);

        }else {
            holder.checkbox.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class  PrabhuViewHolder extends RecyclerView.ViewHolder{
        TextView productName,billAmount;
        CheckBox checkbox;

        public PrabhuViewHolder(View itemView) {
            super(itemView);
            productName=itemView.findViewById(R.id.productName);
            billAmount=itemView.findViewById(R.id.billAmount);
            checkbox=itemView.findViewById(R.id.checkbox);
        }
    }

}