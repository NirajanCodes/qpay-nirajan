package net.qpaysolutions.QPay.Billpayment.ubskhanepani;

import android.os.AsyncTask;
import android.util.Log;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/18/18.
 */

public class Ubswateroffice extends AsyncTask<String, String, String> {
    UBSWaterFragment ubsWaterFragment;
    boolean firstTime;

    public Ubswateroffice(UBSWaterFragment ubsWaterFragment, boolean firstTime) {
        this.ubsWaterFragment = ubsWaterFragment;
        this.firstTime = firstTime;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("billPayCode", GeneralPref.getWaterInfo());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            if (GeneralPref.getWaterInfo().equals("402")) {
                return NetworkAPI.sendHTTPData(Constants.USB_OFFICe, jsonObject);
            } else {
                return NetworkAPI.sendHTTPData(Constants.NKC_OFFICe, jsonObject);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d(Utility.TAG, "response water " + s);
        if (firstTime) {
            ubsWaterFragment.hideDialog();
        }
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            if (GeneralPref.getWaterInfo().equals("402")){
                JSONObject UbsWaterOfficeResult = jsonObject.getJSONObject("UbsWaterOfficeResult");
                if (UbsWaterOfficeResult.getString("status").equals("00")) {
                    Tags.setPreferenceArrayKhanepani(s, "402");
                    JSONArray data = UbsWaterOfficeResult.getJSONArray("data");
                    ArrayList<UBSModel> ubsModelArrayList = new ArrayList<>();
                    for (int i = 0; i < data.length(); i++) {
                        UBSModel ubsModel = new UBSModel();
                        JSONObject object = data.getJSONObject(i);
                        ubsModel.setOffice(object.getString("Office"));
                        ubsModel.setOfficeCode(object.getString("OfficeCode"));
                        ubsModelArrayList.add(ubsModel);
                    }
                    ubsWaterFragment.getList(ubsModelArrayList);
                }

            } else {
                if (jsonObject.getString("status").equals("00")) {
                    Tags.setPreferenceArrayKhanepani(s, "404");
                    JSONArray data = jsonObject.getJSONArray("data");
                    ArrayList<UBSModel> ubsModelArrayList = new ArrayList<>();
                    for (int i = 0; i < data.length(); i++) {
                        UBSModel ubsModel = new UBSModel();
                        JSONObject object = data.getJSONObject(i);
                        ubsModel.setOffice(object.getString("Office"));
                        ubsModel.setOfficeCode(object.getString("OfficeCode"));
                        ubsModelArrayList.add(ubsModel);
                    }
                    ubsWaterFragment.getList(ubsModelArrayList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (firstTime) {
            ubsWaterFragment.showDialog();
        }
    }
}
