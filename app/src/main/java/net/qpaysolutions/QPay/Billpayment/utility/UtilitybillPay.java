package net.qpaysolutions.QPay.Billpayment.utility;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 6/28/16.
 */
public class UtilitybillPay extends AppCompatActivity {
    Toolbar toolbar;
    TextView toolbar_text;
    boolean isVisible;
    UtilityFragment utilityFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.utility_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        utilityFragment=new UtilityFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.utility_fragment, utilityFragment)
                .commit();


    }
    public void setUtilityViible(boolean viible){
        isVisible=viible;
    }


   @Override
   public void onBackPressed() {
       super.onBackPressed();
       finish();
   }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("dinesh", "onResume: ");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    public void  setToolbar_text(String name){
        toolbar_text.setText(name);
    }
}
