package net.qpaysolutions.QPay.Billpayment.meroTv;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillpayList;
import net.qpaysolutions.QPay.CASHBACK.CheckRewardAPI;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 5/13/18.
 */

public class MeroTvFragment extends Fragment {

    private String amount, Id, PlanId, OpenPlanId;
    private EditText inputAmount;
    private EditText meroTvEditext;
    private RadioButton seteclPackage;
    private String amount_pay = "";
    private String plan_id = "";
    private RadioButton inputAmountRadio;
    private FrameLayout selectPackage;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
        Button meroTvCheckPayment = (Button) view.findViewById(R.id.meroTvCheckPayment);
        meroTvEditext = (EditText) getView().findViewById(R.id.meroTvEditext);
        meroTvEditext.setMaxLines(1);
        final TextInputLayout inputLayoutId = (TextInputLayout) view.findViewById(R.id.inputLayoutId);
        meroTvCheckPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (meroTvEditext.getText().toString() != null && !meroTvEditext.getText().toString().isEmpty()) {
                    new Utility().hideSoftkey(getActivity());
                    new MeroTvCheckpaymentAPI(new DialogInterface() {
                        @Override
                        public void showDialog() {
                            qPayProgressDialog.show();
                        }

                        @Override
                        public void hideDialog() {
                            qPayProgressDialog.dismiss();

                        }
                    }, getContext(), getMeroId(), MeroTvFragment.this).execute();
                } else {
                    inputLayoutId.setError("Enter Smart Id");
                }
            }
        });

        meroTvEditext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                inputLayoutId.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mero_tv_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public String getMeroId() {
        return meroTvEditext.getText().toString();
    }

    public void setValues(JSONObject object) {
        LinearLayout lookupView = (LinearLayout) getView().findViewById(R.id.lookupView);
        LinearLayout detailsView = (LinearLayout) getView().findViewById(R.id.detailsView);
        LinearLayout packageDetailsList = (LinearLayout) getView().findViewById(R.id.packageDetailsList);
        packageDetailsList.setVisibility(View.VISIBLE);
        detailsView.setVisibility(View.VISIBLE);
        lookupView.setVisibility(View.GONE);
        TextView username = (TextView) getView().findViewById(R.id.username);
        TextView smartCardId = (TextView) getView().findViewById(R.id.smartCardId);
        TextView currentBalance = (TextView) getView().findViewById(R.id.currentBalance);
        TextView noOfTv = (TextView) getView().findViewById(R.id.noOfTv);

        try {
            currentBalance.setText("NPR " + new Decimalformate().decimalFormate(object.getString("CurrentBalance")));
            username.setText(object.getString("CustomerName"));
            smartCardId.setText(object.getString("SmartCardId"));
            noOfTv.setText(object.getString("NoOfTv"));
            JSONArray Plan = object.getJSONArray("Plan");
            Id = object.getString("SmartCardId");
            OpenPlanId = object.getString("OpenPlanId");
            GeneralPref.setUsernameWordlink(object.getString("CustomerName"));
            ArrayList<MeroTvPlan> meroTvPlans = new ArrayList<>();
            for (int i = 0; i < Plan.length(); i++) {
                JSONObject jsonObject = Plan.getJSONObject(i);
                MeroTvPlan meroTvPlan = new MeroTvPlan();
                meroTvPlan.setAmount(jsonObject.getString("Amount"));
                meroTvPlan.setPlanId(jsonObject.getString("PlanId"));
                meroTvPlan.setPlanName(jsonObject.getString("PlanName"));
                meroTvPlans.add(meroTvPlan);
            }
            setLayout(meroTvPlans);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setLayout(final ArrayList<MeroTvPlan> meroTvPlanArrayList) {

        seteclPackage = (RadioButton) getView().findViewById(R.id.seteclPackage);
        inputAmountRadio = (RadioButton) getView().findViewById(R.id.inputAmountRadio);
        selectPackage = (FrameLayout) getView().findViewById(R.id.selectPackage);
        inputAmount = (EditText) getView().findViewById(R.id.amountEnter);
        final Spinner spineerPackageType = (Spinner) getView().findViewById(R.id.spineerPackageType);
        final MeroTvAdapter meroTvAdapter = new MeroTvAdapter(getContext(), meroTvPlanArrayList);
        seteclPackage.setChecked(false);
        inputAmountRadio.setChecked(true);
      /*  spineerPackageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                amount = meroTvPlanArrayList.get(i).getAmount();
                PlanId = meroTvPlanArrayList.get(i).getPlanId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                amount = meroTvPlanArrayList.get(0).getAmount();

            }
        });*/
        seteclPackage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    selectPackage.setVisibility(View.VISIBLE);
                    spineerPackageType.setAdapter(meroTvAdapter);
                    spineerPackageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            amount = meroTvPlanArrayList.get(i).getAmount();
                            PlanId = meroTvPlanArrayList.get(i).getPlanId();

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                            amount = meroTvPlanArrayList.get(0).getAmount();
                        }
                    });
                    inputAmount.setVisibility(View.GONE);
                    inputAmountRadio.setChecked(false);

                }
            }
        });
        inputAmountRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    PlanId = "";
                    amount = "";

                    inputAmount.setVisibility(View.VISIBLE);
                    selectPackage.setVisibility(View.GONE);
                    seteclPackage.setChecked(false);
                }
            }
        });

        Button confirm = (Button) getView().findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());

                if (!seteclPackage.isChecked()) {
                    amount_pay = inputAmount.getText().toString();
                    plan_id = OpenPlanId;
                } else {
                    amount_pay = amount;
                    plan_id = PlanId;
                }

                if (amount_pay != null && !amount_pay.isEmpty() && Double.valueOf(amount_pay) > 0) {
                    new CheckRewardAPI(amount_pay, "208", new DialogInterface() {
                        @Override
                        public void showDialog() {
                            qPayProgressDialog.show();
                        }

                        @Override
                        public void hideDialog() {
                            qPayProgressDialog.dismiss();
                            Intent intent = new Intent(getActivity(), BillpayList.class);
                            intent.putExtra("money", amount_pay);
                            intent.putExtra("user_id", plan_id);
                            intent.putExtra("byt_code", "208");
                            intent.putExtra("cell_number", Id);
                            startActivity(intent);
                        }
                    }).execute();
                }
            }
        });
    }
}

