package net.qpaysolutions.QPay.Billpayment.ubskhanepani.QuickPayDto;


public class QuickPayDto {

    String id;
    String customerId;

    public String getId() {
        return id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return id + "," +
                customerId
                ;
    }
}
