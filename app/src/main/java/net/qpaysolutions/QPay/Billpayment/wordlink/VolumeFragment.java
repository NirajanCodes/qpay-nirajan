package net.qpaysolutions.QPay.Billpayment.wordlink;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import net.qpaysolutions.QPay.Billpayment.BillpayList;
import net.qpaysolutions.QPay.CASHBACK.CheckRewardAPI;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 6/18/17.
 */
public class VolumeFragment extends Fragment implements View.OnClickListener {
    RecyclerView volumed_based;
    WordLinkRecycleAdapter wordLinkRecycleAdapter;
    String word_link,opt1;
    ArrayList<VolumeModel> volumeModels = new ArrayList<>();
    TextView amount;
    String DiscountedAmount,OptionId;
    Button confirm;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        word_link = getArguments().getString("wordlink");
        opt1=getArguments().getString("opt1");

        try {
            JSONObject jsonObject1=new JSONObject(word_link);
            JSONArray VolumnType = jsonObject1.getJSONArray("VolumnType");
            for (int i=0;i<VolumnType.length();i++){
                JSONObject jsonObject = VolumnType.getJSONObject(i);
                VolumeModel volumeModel = new VolumeModel();
                volumeModel.setDiscountedRate(new Decimalformate().decimalFormate(jsonObject.getString("DiscountedRate")));
                volumeModel.setRate(new Decimalformate().decimalFormate(jsonObject.getString("Rate")));
                volumeModel.setOptionId(jsonObject.getString("OptionId"));
                volumeModel.setVolumnPackage(jsonObject.getString("VolumnPackage"));
                volumeModel.setIschecked(false);
                volumeModels.add(volumeModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        volumed_based=(RecyclerView)view.findViewById(R.id.volumed_based);
        amount=(TextView)view.findViewById(R.id.amount);
        volumed_based.setHasFixedSize(true);
        volumed_based.setLayoutManager(new LinearLayoutManager(getContext()));
        wordLinkRecycleAdapter=new WordLinkRecycleAdapter(volumeModels,VolumeFragment.this);
        volumed_based.setAdapter(wordLinkRecycleAdapter);
        confirm=(Button)view.findViewById(R.id.confirm);
        confirm.setOnClickListener(this);
        TextView  username=(TextView)view.findViewById(R.id.username);
        username.setText(GeneralPref.getUsernameWordlink());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.volume_main,container,false);
    }
    public void  goToPay(){
       /* Intent intent = new Intent(getActivity(), BillpayList.class);
        intent.putExtra("money", DiscountedAmount);
        intent.putExtra("byt_code", "303");
        intent.putExtra("cell_number", opt1);
        intent.putExtra("user_id",OptionId);
        startActivity(intent);*/



        final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
        new CheckRewardAPI(DiscountedAmount, "303", new DialogInterface() {
            @Override
            public void showDialog() {
                qPayProgressDialog.show();
            }

            @Override
            public void hideDialog() {
                qPayProgressDialog.dismiss();
                Intent intent = new Intent(getActivity(), BillpayList.class);
                intent.putExtra("money", DiscountedAmount);
                intent.putExtra("byt_code", "303");
                intent.putExtra("cell_number", opt1);
                intent.putExtra("user_id",OptionId);
                startActivity(intent);

            }
        }).execute();
    }
    public void setAmount(String price){
        amount.setText("NPR "+price);
        DiscountedAmount=price;

    }
    public void getOptionId(String OptionIdd){
        OptionId=OptionIdd;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
                goToPay();
                break;
        }
    }
}
