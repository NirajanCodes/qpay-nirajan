package net.qpaysolutions.QPay.Billpayment.ubskhanepani;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;


public class UBSSpineerDapter extends BaseAdapter {
    Context context;

    LayoutInflater inflter;
    ArrayList<UBSModel> arrayList= new ArrayList<>();
    UBSWaterFragment depositEbankingFragment;

    public UBSSpineerDapter(Context applicationContext, ArrayList<UBSModel> arrayList, UBSWaterFragment depositEbankingFragment) {
        this.context = applicationContext;
        this.arrayList = arrayList;
        this.depositEbankingFragment=depositEbankingFragment;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_ebanking_amount_spineer, null);
        TextView names = (TextView) view.findViewById(R.id.itemName);
        names.setText(arrayList.get(i).getOffice());
        return view;
    }
}