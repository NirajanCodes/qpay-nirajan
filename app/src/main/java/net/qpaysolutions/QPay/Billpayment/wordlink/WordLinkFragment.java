package net.qpaysolutions.QPay.Billpayment.wordlink;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

/**
 * Created by deadlydragger on 6/18/17.
 */

public class WordLinkFragment extends Fragment implements View.OnClickListener {
    String appId, id;
    SharedPreferences sharedPreferences_location;
    String File = "location";
    private String lat, lng;
    QPayProgressDialog qPayProgressDialog;
    QPayCustomerDatabase qpayMerchantDatabase;
    Button search;
    String TAG = "dinesh";
    TextView name;
    EditText user_name;
    LinearLayout visibility_layout, background;
    ImageView roundImage;
    String switchInternet = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        qpayMerchantDatabase.getReadableDatabase();
        appId = qpayMerchantDatabase.getKeyAppId();
        id = qpayMerchantDatabase.getCustomerID();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences_location = getActivity().getSharedPreferences(File, Context.MODE_PRIVATE);
        lat = sharedPreferences_location.getString("lat", "");
        lng = sharedPreferences_location.getString("lng", "");
        search = (Button) view.findViewById(R.id.search);
        user_name = (EditText) view.findViewById(R.id.user_name);
        visibility_layout = (LinearLayout) view.findViewById(R.id.visibility_layout);
        background = view.findViewById(R.id.background);
        roundImage = view.findViewById(R.id.roundImage);
        name = view.findViewById(R.id.name);
        if (getArguments().getString("WorldLink") != null && Objects.requireNonNull(getArguments().getString("WorldLink")).equalsIgnoreCase("WorldLink")) {
            background.setBackground(getResources().getDrawable(R.drawable.wlink_bill_payment_top_bg));
            name.setText("WorldLink Communication");
            switchInternet = "WorldLink";
            roundImage.setImageDrawable(getResources().getDrawable(R.drawable.worldlink));
        } else if (getArguments().getString("WebSurfer") != null && Objects.requireNonNull(getArguments().getString("WebSurfer")).equalsIgnoreCase("WebSurfer")) {
            background.setBackground(getResources().getDrawable(R.color.background));
            name.setText("WebSurfer");
            user_name.setHint("User Id");
            switchInternet = "WebSurfer";
            roundImage.setImageDrawable(getResources().getDrawable(R.drawable.websurfer));
        } else if (getArguments().getString("Arrownet") != null && Objects.requireNonNull(getArguments().getString("Arrownet")).equalsIgnoreCase("Arrownet")) {
            background.setBackground(getResources().getDrawable(R.color.background));
            name.setText("Arrownet");
            switchInternet = "Arrownet";
            roundImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrownet));
        }
        search.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.word_link_main, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search:
                new Utility().hideSoftkey(getActivity());
                if (!user_name.getText().toString().trim().isEmpty()) {
                    new CheckPayment(user_name.getText().toString().trim(),"","").execute();
                } else {
                    user_name.setError("Enter User Name");
                }
                break;
        }
    }

    public class CheckPayment extends AsyncTask<String, String, String> {
        String opt1, opt2, opt3;

        public CheckPayment(String opt1, String opt2, String opt3) {
            this.opt1 = opt1;
            this.opt2 = opt2;
            this.opt3 = opt3;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String checkpayment = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                if (getArguments().getString("WorldLink") != null && Objects.requireNonNull(getArguments().getString("WorldLink")).equalsIgnoreCase("WorldLink")) {
                    jsonObject.put("billPayCode", 303);
                } else if (getArguments().getString("WebSurfer") != null && Objects.requireNonNull(getArguments().getString("WebSurfer")).equalsIgnoreCase("WebSurfer")) {
                    jsonObject.put("billPayCode", 314);
                } else if (getArguments().getString("Arrownet") != null && Objects.requireNonNull(getArguments().getString("Arrownet")).equalsIgnoreCase("Arrownet")) {
                    jsonObject.put("billPayCode", 315);
                }
                jsonObject.put("lat", lat);
                jsonObject.put("lng", lng);
                jsonObject.put("opt1", opt1);
                jsonObject.put("opt2", opt2);
                jsonObject.put("opt3", opt3);
                Log.d(TAG, "doInBackground: " + jsonObject);
                checkpayment = networkAPI.sendHTTPData(Constants.CHECK_WORDLINK_BILL, jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return checkpayment;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                Utility.isBuild(s);
                GeneralPref.setUsernameWordlink(user_name.getText().toString().trim());
                user_name.setText("");
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
//                    visibility_layout.setVisibility(View.GONE);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    WordLinkPrabhuPay fragobj = new WordLinkPrabhuPay();
                    Bundle bundle = new Bundle();
                    bundle.putString("wordlink", jsonObject.toString());
                    bundle.putString("user_id",opt1);
                    bundle.putString(("switchInternet"), switchInternet);
                    fragobj.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, fragobj)
                            .addToBackStack("worldlinkDetail")
                            .commit();
                  /*  Intent intent = new Intent(getActivity(), BillPayActivity.class);
                    intent.putExtra("id","wordlink");
                    intent.putExtra("wordlink",jsonObject.toString());
                    startActivity(intent);*/
//                    setFragment(new WordLinkPrabhuPay(),jsonObject1.toString(),"");

                    /*String PackageType = jsonObject1.getString("PackageType");
                    switch (PackageType){
                        case "1":
                            setFragment(new UnlimitedFragment(),jsonObject1.toString(),opt1);
                            break;
                        case "2":
                            setFragment(new VolumeFragment(),jsonObject1.toString(),opt1);
                            break;
                        case "3":
                            setFragment(new DuePaymentFragment(),jsonObject1.toString(),opt1);
                            break;
                        case "4":
                            setFragment(new AdvanceUnlimitedFragment(),jsonObject1.toString(),opt1);
                            break;
                        case "5":
                            setFragment(new AdvanceVolumeFragment(),jsonObject1.toString(),opt1);
                            break;
                    }*/
                } else {
                    new Utility().showSnackbar(visibility_layout, "User Not Found");
                }
            } catch (Exception e) {
                e.printStackTrace();
                new Utility().showSnackbar(visibility_layout, "User Not Found");
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getActivity());
            qPayProgressDialog.show();
        }
    }

    public void setFragment(Fragment fragment, String string, String opt1) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("wordlink", string);
        bundle.putString("opt1", opt1);
        fragmentTransaction.replace(R.id.word_link, fragment);
        fragment.setArguments(bundle);
        fragmentTransaction.commitAllowingStateLoss();

    }
}
