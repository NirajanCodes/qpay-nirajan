package net.qpaysolutions.QPay.Billpayment.utility.electricity;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.List;

/**
 * Created by CSharp05-user on 16/07/2017.
 */

public class DueDetailsAdapter extends RecyclerView.Adapter<DueDetailsAdapter.ViewHolder> {
    private Context context;
    private List<DueDetailsInfo> dueDetailsInfoList;

    public DueDetailsAdapter(Context context, List<DueDetailsInfo> dueDetailsInfoList) {
        this.context = context;
        this.dueDetailsInfoList = dueDetailsInfoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.due_details_item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.BillAmount.setText(dueDetailsInfoList.get(position).getBillAmount());
        holder.BillDate.setText(dueDetailsInfoList.get(position).getBillDate());
        holder.DueBillOf.setText(dueDetailsInfoList.get(position).getDueBillOf());
        holder.NoOfDays.setText(dueDetailsInfoList.get(position).getNoOfDays());
        holder.PayableAmount.setText(dueDetailsInfoList.get(position).getPayableAmount());
        holder.Status.setText(dueDetailsInfoList.get(position).getStatus());
        holder.due_title.setText(dueDetailsInfoList.get(position).getDueBillOf());
    }

    @Override
    public int getItemCount() {
        return dueDetailsInfoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView BillAmount, BillDate, DueBillOf, NoOfDays, PayableAmount, Status, due_title;

        public ViewHolder(View itemView) {
            super(itemView);
            BillAmount = (TextView) itemView.findViewById(R.id.bill_amount);
            BillDate = (TextView) itemView.findViewById(R.id.bill_date);
            DueBillOf = (TextView) itemView.findViewById(R.id.due_of);
            NoOfDays = (TextView) itemView.findViewById(R.id.no_of_days);
            PayableAmount = (TextView) itemView.findViewById(R.id.payable_amount);
            Status = (TextView) itemView.findViewById(R.id.due_status);
            due_title = (TextView) itemView.findViewById(R.id.due_title);

        }
    }
}
