package net.qpaysolutions.QPay.Billpayment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import net.qpaysolutions.QPay.CheckInternetConnection.NetworkInformation;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.AcknTask;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by deadlydragger on 6/28/16.
 */

public class BillpayList extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    static final String TAG = "dinesh";
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String customerId;
    private QPayProgressDialog progressDialog;
    private String app_id, last_4, user_id = "", option3, adVance_pay, title_paying, quick_number = "", quick_code = "", money, cell_number, byte_code;
    private ListView cardBillPay;
    private BillpayCardAdapter billpayCardAdapter;
    private Handler handler = new Handler();
    private int counter = 0;
    private ImageView img_url;
    private int count = 0;
    private NetworkChangeReceiver networkChangeReceiver;
    private boolean mIsReceiverRegistered = false;
    private TextView wallet_amount_text, no, title_paying_to, paidAmount, paidTo;
    boolean isDialogOpen = false;
    private LinearLayout ll_phone_number, pay_through_qpay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.bill_payment_card);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            TextView toolbar_text = toolbar.findViewById(R.id.title);
            toolbar_text.setText("Payment Option");

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            pay_through_qpay = findViewById(R.id.bill_through_qpay);
            paidAmount = findViewById(R.id.paidTo_amount);
            paidTo = findViewById(R.id.paidTo);
            wallet_amount_text = findViewById(R.id.wallet_amount_text);
            cardBillPay = findViewById(R.id.bill_pay_card);
            img_url = findViewById(R.id.img_url);
            title_paying_to = findViewById(R.id.title_paying);
            no = findViewById(R.id.no_ticket);
            ll_phone_number = findViewById(R.id.ll_phone_number);
            pay_through_qpay.setOnClickListener(this);
            QPayCustomerDatabase = new QPayCustomerDatabase(this);
            QPayCustomerDatabase.getReadableDatabase();
            customerId = QPayCustomerDatabase.getCustomerID();
            app_id = QPayCustomerDatabase.getKeyAppId();
            quick_number = QPayCustomerDatabase.checkNumber();
            quick_code = QPayCustomerDatabase.checkQuickCode();
            Log.d(Utility.TAG, "quick code : " + quick_code + "quick number : " + quick_number);
            money = getIntent().getStringExtra("money");
            cell_number = getIntent().getStringExtra("cell_number");
            option3 = getIntent().getStringExtra("cash_Id");
            user_id = getIntent().getStringExtra("user_id");
            byte_code = getIntent().getStringExtra("byt_code");

            if (byte_code.equals("401")) {
                adVance_pay = getIntent().getStringExtra("addAmount1");
            }

            paidAmount.setText(" NPR " + money);

            if (cell_number.equals("1") || cell_number.equals("98********")) {
                ll_phone_number.setVisibility(View.GONE);
                paidTo.setText("98********");

            } else {
                ll_phone_number.setVisibility(View.VISIBLE);
                paidTo.setText(cell_number);
            }

            SQLiteDatabase db = QPayCustomerDatabase.getWritableDatabase();
            Cursor cursor = db.rawQuery(" select * from Profile where act_flag ='V' ", null);
            billpayCardAdapter = new BillpayCardAdapter(this, cursor);
            cardBillPay.setAdapter(billpayCardAdapter);
            cardBillPay.setOnItemClickListener(this);

            networkChangeReceiver = new NetworkChangeReceiver();
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mIsReceiverRegistered = true;
            checkInternetConnection();
            walletBalance();
            SetTitelText(byte_code);
            checkQuickPay(byte_code);
            calculateReward(money);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkQuickPay(String code) {
        switch (code) {
            case "201":
            case "203":
            case "205":
            case "204":
            case "206":
                count = QPayCustomerDatabase.checkQuickNumber(cell_number, byte_code);
                break;
        }
    }

    public void calculateReward(String reward) {
        TextView reward_amount = (TextView) findViewById(R.id.reward_amount);
        reward_amount.setText(Html.fromHtml("You will be awarded with cash back reward of NPR " + "<b>" + GeneralPref.getRewardAmount() + "</b>"));
    }

    public void walletBalance() {
        wallet_amount_text.setText("NPR " + new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));

    }

    public void SetTitelText(String code) {
        switch (code) {
            case "402":
                Log.d("dinesh", "name :" + GeneralPref.getUsernameWordlink() + " code: " + code);
                ll_phone_number.setVisibility(View.VISIBLE);
                no.setText("Customer Name");
                paidTo.setText(GeneralPref.getUsernameWordlink());
                title_paying = "UBS Water";
                img_url.setImageResource(R.drawable.khane_pani);
                title_paying_to.setText(title_paying);
                break;
            case "404":
                no.setText("Customer Name");
                paidTo.setText(GeneralPref.getUsernameWordlink());
                title_paying = "Nepal Water Supply Corporation";
                img_url.setImageResource(R.drawable.nepal_khanepani);
                title_paying_to.setText(title_paying);
                break;
            case "201":
                title_paying = "Ncell";
                img_url.setImageResource(R.drawable.ncell);
                title_paying_to.setText(title_paying);
                break;
            case "203":
            case "205":
            case "612":
            case "606":
            case "607":
            case "608":
            case "209":
            case "210":
                title_paying = "NTC";
                title_paying_to.setText(title_paying);
                img_url.setImageResource(R.drawable.nct_logo);
                break;
            case "301":
                ll_phone_number.setVisibility(View.VISIBLE);
                title_paying = "SUBISU";
                img_url.setImageResource(R.drawable.subisu);
                title_paying_to.setText(title_paying);
                paidTo.setText(user_id);
                no.setText("Customer Id");
                break;
            case "305":
            case "304":
                title_paying = "ADSL";
                title_paying_to.setText(title_paying);
                img_url.setImageResource(R.drawable.adsl);
                break;
            case "613":
            case "614":
            case "615":
            case "629":
            case "630":
                ll_phone_number.setVisibility(View.GONE);
                paidTo.setText("Broadlink");
                title_paying = "Broadlink";
                title_paying_to.setText(title_paying);
                img_url.setImageResource(R.drawable.broodlink);
                break;
            case "303":
                no.setText("Username");
                paidTo.setText(GeneralPref.getUsernameWordlink());
                title_paying = "Worldlink";
                img_url.setImageResource(R.drawable.worldlink);
                title_paying_to.setText(title_paying);
                break;
            case "208":
                no.setText("Username");
                paidTo.setText(GeneralPref.getUsernameWordlink());
                title_paying = "Mero TV";
                img_url.setImageResource(R.drawable.mero_tv);
                title_paying_to.setText(title_paying);
                break;
            case "302":
                no.setText("Customer Name");
                paidTo.setText(GeneralPref.getUsernameWordlink());
                title_paying = "Vianet";
                img_url.setImageResource(R.drawable.logo_vainet);
                title_paying_to.setText(title_paying);
                break;
            case "701":
                no.setText("Customer Name");
                paidTo.setText(GeneralPref.getUsernameWordlink());
                title_paying = "Nepal Life Insurance";
                img_url.setImageResource(R.drawable.nepal_life);
                title_paying_to.setText(title_paying);
                break;
            case "401":
                no.setText("Customer Id");
                paidTo.setText(cell_number);
                title_paying = "NEA";
                img_url.setImageResource(R.drawable.nea);
                title_paying_to.setText(title_paying);
                break;
            case "306":
                paidTo.setText(cell_number);
                no.setText("CAS ID/CHIP ID");
                title_paying = "DishHome";
                img_url.setImageResource(R.drawable.dishhome);
                title_paying_to.setText(title_paying);
                break;
            case "618":
            case "619":
            case "620":
            case "621":
            case "622":
            case "623":
            case "624":
            case "625":
            case "626":
            case "633":
            case "632":
            case "637":
                paidTo.setText(cell_number);
                no.setText("Customer Id");
                no.setVisibility(View.GONE);
                title_paying = "DishHome";
                img_url.setImageResource(R.drawable.dishhome);
                title_paying_to.setText(title_paying);
                break;
            case "204":
                title_paying = "PSTN Landline";
                img_url.setImageResource(R.drawable.nct_logo);
                title_paying_to.setText(title_paying);
                break;
            case "206":
            case "609":
            case "610":
            case "611":
                title_paying = "UTL";
                img_url.setImageResource(R.drawable.utl);
                title_paying_to.setText(title_paying);
                break;
            case "616":
            case "617":
                ll_phone_number.setVisibility(View.GONE);
                title_paying = "Net TV";
                title_paying_to.setText(title_paying);
                img_url.setImageResource(R.drawable.net_tv);
                break;
            case "634":
            case "635":
            case "636":
                no.setVisibility(View.GONE);
                title_paying = "Mero TV";
                img_url.setImageResource(R.drawable.mero_tv);
                title_paying_to.setText(title_paying);
                break;
            case "604":
            case "605":
            case "603":
            case "631":
            case "207":
            case "638":
                title_paying = "Smart Cell";
                img_url.setImageResource(R.drawable.smart_cell);
                title_paying_to.setText(title_paying);
                break;
            case "403":
                ll_phone_number.setVisibility(View.VISIBLE);
                paidTo.setText("Traffic Police");
                title_paying = "Traffic fine pay.";
                no.setText("Ticket Number");
                img_url.setImageResource(R.drawable.traffic);
                title_paying_to.setText(title_paying);
                break;
            case "307":
                ll_phone_number.setVisibility(View.VISIBLE);
                no.setText("Customer Id");
                paidTo.setText(user_id);
                cell_number = user_id;
                user_id = "";
                title_paying = "SIM TV";
                img_url.setImageResource(R.drawable.sim_tv);
                break;
            case "310":
                ll_phone_number.setVisibility(View.VISIBLE);
                no.setText("Customer Id");
                paidTo.setText(getIntent().getStringExtra("cell_number"));
                title_paying = "Prabhu TV";
                img_url.setImageResource(R.drawable.logo_prabhu_tv);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bill_through_qpay:
                try {
                    Double balance_new = Double.valueOf(BalancePref.getAvailableAmount());
                    Double value = Double.parseDouble(money);
                    Log.d(Utility.TAG, "balance : " + balance_new);
                    Log.d(Utility.TAG, "value : " + value);
                    if (value > 0) {
                        if (balance_new >= value) {
                            if (isDialogOpen == false) {
                                isDialogOpen = true;
                                confirmPayThroughQpay(money);
                            }

                        } else {
                            Utility.custumdialogfailureInsufficient("Bill Payment Failure!", "Insufficient QPay balance for this transaction. Please try with cards.", BillpayList.this);
                        }
                    } else {
                        new Utility().showSnackbar(ll_phone_number, getString(R.string.no_due_amount));

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    custumdialogfailure("Bill Payment Failure!", "We are unable to process your transaction at this time. Please try again later.");
                }
                break;

        }
    }

    public void confirmPayThroughQpay(String amount) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BillpayList.this);
        LayoutInflater inflater = BillpayList.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Do you want to Pay NPR " + amount + " from your QPay Account?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PayBill(money, LatLngPref.getLat(), LatLngPref.getLng(), byte_code, cell_number).execute();
                dialog.dismiss();
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public void cradBillPay(final String app_id, final String cust_id, final String amount, final String billpaycode, final String last_4, final String opt1, final String card_token, final long id) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BillpayList.this);
        LayoutInflater inflater = BillpayList.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_entrcardpin, null);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));



       /* final Dialog dialog = new Dialog(BillpayList.this, R.style.UpdownDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custum_dialog_entrcardpin);*/
        final EditText aut_editext = (EditText) v.findViewById(R.id.enter_pin);
        final TextView dialogButton = (TextView) v.findViewById(R.id.proceed_pin_enter);
        TextView dialogcancel = (TextView) v.findViewById(R.id.cancel_pin_enter);
       /* if (aut_editext.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }*/
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String text_from_editext = aut_editext.getText().toString();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(aut_editext.getWindowToken(), 0);
                    if (text_from_editext.length() == 4) {
                        new BillpayCard(app_id, cust_id, amount, billpaycode, last_4, opt1, text_from_editext, card_token, id).execute();
                        dialog.dismiss();
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } else {
                        new Utility().showSnackbar(dialogButton, "Please Insert Valid Pin Number");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        last_4 = QPayCustomerDatabase.getLastFourForBill(id);
        String card_token = QPayCustomerDatabase.getCardType(String.valueOf(id));
        Log.d(Utility.TAG, "last four : " + last_4);
        cradBillPay(app_id, customerId, money, byte_code, last_4, cell_number, card_token, id);
    }

    private class PayBill extends AsyncTask<String, String, String> {
        String amount, byte_code, phone;
        String lat, lon;

        private PayBill(String amount, String lat, String lon, String byte_code, String phone) {
            this.amount = amount;
            this.lat = lat;
            this.lon = lon;
            this.byte_code = byte_code;
            this.phone = phone;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            String result = "";
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", customerId);
                jsonObject.put("billpaycode", byte_code);
                jsonObject.put("amount", amount);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("billpaycode", byte_code);
                jsonObject.put("opt1", phone);
                jsonObject.put("opt2", user_id);
                jsonObject.put("opt3", option3);
                if (byte_code.equals("401") || byte_code.equals("404")) {
                    jsonObject.put("addAmount1", adVance_pay);
                }
                Log.d(TAG, jsonObject.toString());
                HttpUrlConnectionJsonBillPay HttpUrlConnectionJsonBillPay = new HttpUrlConnectionJsonBillPay();
                result = HttpUrlConnectionJsonBillPay.sendHTTPData(Constants.BASE_API + "billpaywithqpayaccount", jsonObject);
                Log.d(TAG, "result : " + result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(BillpayList.this);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String jsonObject) {
            super.onPostExecute(jsonObject);
            progressDialog.dismiss();
            try {
                progressDialog.dismiss();
                progressDialog.hide();
                JSONObject jsonObject1 = new JSONObject(jsonObject);
                JSONObject jsonObject2 = jsonObject1.getJSONObject("BillPayWithQPayAccountResult");
                String status = jsonObject2.getString("status");
                String resp_code = jsonObject2.getString("resp_code");
                String pin = jsonObject2.getString("pin");
                String serial_no = jsonObject2.getString("serial_no");
                String stan = jsonObject2.getString("stan");
                String crrn = jsonObject2.getString("crrn");
                String dsplyMsg = jsonObject2.getString("dsplyMsg");
                String availBal = jsonObject2.getString("avail_bal");
                if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("00")) {
                    if (resp_code.equals("00")) {
                        vibration();
                        BalancePref.setAvailableAmount(new Decimalformate().decimalFormate(availBal));
                        new AcknTask().new Ackclass(app_id, customerId, stan, crrn).execute();
                        DateFormat df = new SimpleDateFormat("MMM d, yyyy");
                        String date = df.format(Calendar.getInstance().getTime());
                        date = date.replace("+", "");
                        Log.d(Utility.TAG, "quick number " + quick_number + "phone " + phone);
                        Log.d(Utility.TAG, "bill pay code billlist : " + byte_code);
                        if (count >= 1) {
                        } else {
                            switch (byte_code) {
                                case "201":
                                case "203":
                                case "205":
                                case "204":
                                case "206":
                                    try {
                                        QPayCustomerDatabase.insertQuickinfo(phone, byte_code, date, user_id);
                                        break;
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }

                                default:
                                    break;
                            }
                        }

                        if (pin != null && serial_no != null) {
                            if (byte_code.equals("612") || byte_code.equals("606") || byte_code.equals("607") || byte_code.equals("608")) {
                                QPayCustomerDatabase.insertPininfo("NTC", date, amount, pin, serial_no);
                            } else if (byte_code.equals("604") || byte_code.equals("605")) {
                                QPayCustomerDatabase.insertPininfo("Smart Cell", date, amount, pin, serial_no);
                            } else if (byte_code.equals("609") || byte_code.equals("610") || byte_code.equals("611")) {
                                QPayCustomerDatabase.insertPininfo("UTL", date, amount, pin, serial_no);

                            } else if (byte_code.equals("616") || byte_code.equals("617")) {
                                QPayCustomerDatabase.insertPininfo("Net Tv", date, amount, pin, serial_no);
                            } else if (byte_code.equals("613") || byte_code.equals("614") || byte_code.equals("615")) {
                                QPayCustomerDatabase.insertPininfo("Broodlink", date, amount, pin, serial_no);
                            } else if (byte_code.equals("618") || byte_code.equals("619") || byte_code.equals("620") || byte_code.equals("621") || byte_code.equals("622") || byte_code.equals("623") || byte_code.equals("624") || byte_code.equals("625") || byte_code.equals("626")) {
                                QPayCustomerDatabase.insertPininfo("Dishome", date, amount, pin, serial_no);
                            }
                        }
                        custumdialogSucess("Bill Payment Success!", dsplyMsg);
                    } else if (resp_code.equals("NE")) {
                        new AcknTask().new Ackclass(app_id, customerId, stan, crrn).execute();
                        custumdialogfailure("Bill Payment Failure!", dsplyMsg);

                    } else if (resp_code.equals("98")) {
                        new AcknTask().new Ackclass(app_id, customerId, stan, crrn).execute();
                        Utility.custumdialogWarning("Bill Payment in Progress!", dsplyMsg, BillpayList.this);

                    } else if (resp_code.equals("KE")) {
                        new AcknTask().new Ackclass(app_id, customerId, stan, crrn).execute();
                        custumdialogfailure("Bill Payment Failure!", dsplyMsg);

                    } else {
                        new AcknTask().new Ackclass(app_id, customerId, stan, crrn).execute();
                        custumdialogfailure("Bill Payment Failure!", dsplyMsg);
                    }
                } else if (status.equals("99")) {
                    new AcknTask().new Ackclass(app_id, customerId, stan, crrn).execute();
                    custumdialogfailure("Bill Payment Failure!", dsplyMsg);
                } else {
                    new AcknTask().new Ackclass(app_id, customerId, stan, crrn).execute();
                    custumdialogfailure("Bill Payment Failure!", dsplyMsg);

                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", BillpayList.this);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mIsReceiverRegistered) {
            unregisterReceiver(networkChangeReceiver);
            networkChangeReceiver = null;
            mIsReceiverRegistered = false;
        }
        super.onBackPressed();
        finish();

    }

    public class BillpayCard extends AsyncTask<String, String, String> {
        String amount, app_id, billpaycode, cust_id, last_4, opt1, pin, card_token;
        long id;

        public BillpayCard(String app_id, String cust_id, String amount, String billpaycode, String last_4, String opt1, String pin, String card_token, long id) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.amount = amount;
            this.billpaycode = billpaycode;
            this.last_4 = last_4;
            this.opt1 = opt1;
            this.pin = pin;
            this.card_token = card_token;
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(BillpayList.this);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("dinesh", "Bill Pay Card : " + s);
            progressDialog.hide();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("BillPayWithCardResult");
                String auth_code = jsonObject1.getString("auth_code");
                String crrn = jsonObject1.getString("crrn");
                String msg = jsonObject1.getString("msg");
                String stan = jsonObject1.getString("stan");
                String status = jsonObject1.getString("status");
                if (status.equals("00")) {
                    runtimerBill(app_id, cust_id, billpaycode, crrn, stan);
                } else if (status.equals("CN")) {
                    QPayCustomerDatabase.deletCard(id);
                    progressDialog.dismiss();
                    progressDialog.hide();
                    custumdialogNotFound("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");
                } else {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    custumdialogfailure("Bill Payment Failure!", "We are unable to process your transaction at this time. Please try again later.");
                }

            } catch (Exception e) {
                e.printStackTrace();
                try {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", BillpayList.this);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            HttpUrlConnectionJsonBillPay HttpUrlConnectionJsonBillPay = new HttpUrlConnectionJsonBillPay();
            String result = null;
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("billpaycode", byte_code);
                jsonObject.put("amount", amount);
                jsonObject.put("last_4", last_4);
                jsonObject.put("card_token", card_token);
                jsonObject.put("lat", ((CustomerApplication) BillpayList.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) BillpayList.this.getApplication()).getLng());
                jsonObject.put("dev_token", ((CustomerApplication) BillpayList.this.getApplication()).getDevtokenVariable());
                jsonObject.put("pin", pin);
                jsonObject.put("billpaycode", byte_code);
                jsonObject.put("opt1", opt1);
                jsonObject.put("opt2", user_id);
                jsonObject.put("opt3", option3);
                if (byte_code.equals("401")) {
                    jsonObject.put("addAmount1", adVance_pay);
                }
                Log.d(Utility.TAG, jsonObject.toString());
                result = HttpUrlConnectionJsonBillPay.sendHTTPData(Constants.BILLPAY_CARD, jsonObject);
                Log.d(Utility.TAG, result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    public class BillPayWithCardStatus extends AsyncTask<String, String, String> {
        String app_id, cust_id, billpaycode, crrn, stan;

        public BillPayWithCardStatus(String app_id, String cust_id, String billpaycode, String crrn, String stan) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.billpaycode = billpaycode;
            this.crrn = crrn;
            this.stan = stan;
        }

        @Override
        protected String doInBackground(String... params) {
            String result_bill_status = null;
            NetworkAPI networkAPIBillPay = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("billpaycode", billpaycode);
                jsonObject.put("crrn", crrn);
                jsonObject.put("stan", stan);
                result_bill_status = networkAPIBillPay.sendHTTPData(Constants.BILLPAY_CARD_STATUS, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result_bill_status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("dinesh", "BillPayWithCardStatus : " + s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("BillPayWithCardStatusResult");

                String crrn = jsonObject1.getString("crrn");
                String dsplyMsg = jsonObject1.getString("dsplyMsg");
                String resp_code = jsonObject1.getString("resp_code");
                final String stan = jsonObject1.getString("stan");
                String status = jsonObject1.getString("status");
                String avail_bal = jsonObject1.getString("avail_bal");

                if (status.equals("00")) {
                    if (resp_code.equals("00")) {
                        BalancePref.setAvailableAmount(avail_bal);
                        vibration();
                        progressDialog.dismiss();
                        progressDialog.hide();
                        new AcknTask().new Ackclass(app_id, cust_id, stan, crrn).execute();
                        DateFormat df = new SimpleDateFormat("MMM d, yyyy");
                        String date = df.format(Calendar.getInstance().getTime());
                        date = date.replace("+", "");

                        if (count >= 1) {

                        } else {
                            switch (byte_code) {
                                /*case "201":
                                case "203":
                                case "205":
                                case "204":
                                case "206":
                                case "301":
                                case "302":
                                case "304":
                                case "305":*/
                                case "201":
                                case "203":
                                case "205":
                                case "204":
                                case "206":
                                    QPayCustomerDatabase.insertQuickinfo(cell_number, byte_code, date, user_id);
                                    break;
                                default:
                                    break;
                            }
                        }
                        custumdialogSucess("Bill Payment Success!", dsplyMsg);
                    } else if (resp_code.equals("LO")) {
                        if (counter < 10) {
                            runtimerBill(app_id, cust_id, billpaycode, crrn, stan);
                        } else {
//                            new Ackclass(app_id, cust_id, stan).execute();
                            progressDialog.dismiss();
                            progressDialog.hide();
                            custumdialogfailure("Bill Payment Failure!", "Transaction time out due to internal network problem.");
                        }
                    }/* else if (resp_code.equals("NE")) {
                        progressDialog.dismiss();
                        progressDialog.hide();
                        new ACKNTask().new Ackclass(app_id, cust_id, stan, crrn).execute();
                        dialogFailure("Bill Payment Failure!", "We are unable to process your transaction at this time. Please try again later.");
                    } else if (resp_code.equals("98")) {
                        progressDialog.dismiss();
                        progressDialog.hide();
                        new ACKNTask().new Ackclass(app_id, cust_id, stan, crrn).execute();
                        Utility.warningDialog("Bill Payment is in Progress!", dsplyMsg, BillpayList.this);

                    } */ else if (resp_code.equals("99")) {
                        progressDialog.dismiss();
                        progressDialog.hide();
                        custumdialogfailure("Bill Payment Failure!", dsplyMsg);
                    } else if (resp_code.equals("98")) {
                        progressDialog.dismiss();
                        progressDialog.hide();
                        Utility.custumdialogWarning("Bill Payment in Progress!", dsplyMsg, BillpayList.this);
                    } else {
                        progressDialog.dismiss();
                        progressDialog.hide();
                        new AcknTask().new Ackclass(app_id, cust_id, stan, crrn).execute();
                        custumdialogfailure("Bill Payment Failure!", dsplyMsg);
                    }

                } else {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    custumdialogfailure("Bill Payment Failure!", dsplyMsg);
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", BillpayList.this);


                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }

        }
    }

    public void runtimerBill(final String app_id, final String cust_id, final String billpaycode, final String crrn, final String stan) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                counter++;
                new BillPayWithCardStatus(app_id, cust_id, billpaycode, crrn, stan).execute();
            }
        }, Tags.TIME_SLEEP);
    }

    public void custumdialogNotFound(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BillpayList.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final ImageView imageView = (ImageView) v.findViewById(R.id.dialog_image);
        imageView.setImageResource(R.drawable.ic_card_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
                startActivity(new Intent(BillpayList.this, MainActivity.class));
                finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BillpayList.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
                //startActivity(new Intent(BillpayList.this, MainActivity.class));
                finish();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

    }

    public void custumdialogSucess(String display_title, String number) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(BillpayList.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(display_title);

        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
                // startActivity(new Intent(BillpayList.this, MainActivity.class));
                finish();


            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

    }

    public void vibration() {
        Vibrator v = (Vibrator) BillpayList.this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mIsReceiverRegistered) {
            if (networkChangeReceiver == null)
                networkChangeReceiver = new NetworkChangeReceiver();
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mIsReceiverRegistered = true;
        }
    }

    @Override
    protected void onPause() {
        if (mIsReceiverRegistered) {
            unregisterReceiver(networkChangeReceiver);
            networkChangeReceiver = null;
            mIsReceiverRegistered = false;
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    private void checkInternetConnection() {
        LinearLayout noInternetConnection = (LinearLayout) findViewById(R.id.no_internet_connection_id);
        NetworkInformation networkInformation = NetworkInformation.getInstance(this);
        if (networkInformation.isOnline()) {
            noInternetConnection.setVisibility(View.GONE);
        } else {
            noInternetConnection.setVisibility(View.VISIBLE);
        }
    }

    public void checkInternetConnection(View view) {
        checkInternetConnection();
    }


    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            NetworkInformation networkInformation = NetworkInformation.getInstance(context);
            if (networkInformation.isOnline()) {
                checkInternetConnection();
            }
        }
    }
}
