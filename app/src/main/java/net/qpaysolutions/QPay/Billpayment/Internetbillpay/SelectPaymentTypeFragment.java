package net.qpaysolutions.QPay.Billpayment.Internetbillpay;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.DirectRecharge.InternetEnterAmountDirectPayFragment;
import net.qpaysolutions.QPay.Billpayment.RechargePin.InternetEnterAmountrechargeEpinFragment;
import net.qpaysolutions.QPay.Billpayment.meroTv.MeroTvFragment;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectPaymentTypeFragment extends Fragment {
    ViewPagerAdapter  viewPagerAdapter;
    TabLayout tabs;
    ViewPager viewPager;
    Toolbar toolbar_custum;
    private InternetEnterAmountDirectPayFragment internetEnterAmountDirectPayFragment;
    private InternetEnterAmountrechargeEpinFragment internetEnterAmountrechargeEpinFragment;
    private MeroTvFragment meroTvFragment;
    Boolean isFromDishHome,isFromMeroTv;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        isFromDishHome=b.getBoolean("isFromDishHome");
        isFromMeroTv=b.getBoolean("isFromMeroTv");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.select_payment_type, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabs=view.findViewById(R.id.phone_tab_layout);
        viewPager=view.findViewById(R.id.view_pager);
        tabs.setupWithViewPager(viewPager);
        internetEnterAmountDirectPayFragment = new InternetEnterAmountDirectPayFragment();
        internetEnterAmountrechargeEpinFragment = new InternetEnterAmountrechargeEpinFragment();
        meroTvFragment = new MeroTvFragment();
        setUpViewPager(viewPager);
    }
    List<String> ncellRechargeAmountList;
    public void setUpViewPager(ViewPager viewPager){
        String[] dishome = getResources().getStringArray(R.array.dishome);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(dishome));
       ViewPagerAdapter viewPagerAdapter= new ViewPagerAdapter(getActivity().getSupportFragmentManager(),2);
       DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int month, String dateDesc) {
            }
        }).textConfirm("DONE")
                .textCancel("CANCEL")
                .btnTextSize(16)
                .viewTextSize(25)
                .setValues(ncellRechargeAmountList)
                .build();
        pickerPopWin.dismissPopWin();
       if(isFromDishHome){
           Bundle bundle = new Bundle();
           bundle.putBoolean("isFromDishHome", isFromDishHome);
           internetEnterAmountDirectPayFragment.setArguments(bundle);
           internetEnterAmountrechargeEpinFragment.setArguments(bundle);
           viewPagerAdapter.addFragment(internetEnterAmountDirectPayFragment,"Direct Recharge");
           viewPagerAdapter.addFragment(internetEnterAmountrechargeEpinFragment,"Recharge E-Pin");
           viewPager.setAdapter(viewPagerAdapter);
           viewPagerAdapter.notifyDataSetChanged();
       }
       else if(isFromMeroTv){
           Bundle bundle = new Bundle();
           bundle.putBoolean("isFromMeroTv", isFromMeroTv);
           internetEnterAmountrechargeEpinFragment.setArguments(bundle);
           viewPagerAdapter.addFragment(meroTvFragment,"Direct Recharge");
           viewPagerAdapter.addFragment(internetEnterAmountrechargeEpinFragment,"Recharge E-Pin");
           viewPager.setAdapter(viewPagerAdapter);
           viewPagerAdapter.notifyDataSetChanged();
       }



    }
}
