package net.qpaysolutions.QPay.Billpayment.utility.subisu;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.InternetFragment;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by deadlydragger on 8/29/17.
 */

public class SubishuFragment extends Fragment implements View.OnClickListener {

    private SharedPreferences sharedPreferences;
    private static String MY_FILE_SHARED = "Mybalance";
    private String byte_code, provider_name, text;
    private EditText id, phone, amount_pay;
    private Button confirm;
    private String cell_number, selectedItem, id_name;
    private LinearLayout llBackground;
    private boolean isValidPay = false;
    private RecyclerView recyclerView;
    private RelativeLayout quickLayout;
    private JSONArray receivedArray = null;
    private QPayProgressDialog qPayProgressDialog;
    private boolean flag = true;
    private LinearLayout top;
    private TextView textNew;
    private String previousData;
    private String[] savedData;
    private boolean dublicate = true;
    private boolean billPayList = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subishu_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        id = (EditText) view.findViewById(R.id.id);
        phone = (EditText) view.findViewById(R.id.phone);
        amount_pay = (EditText) view.findViewById(R.id.amount_pay);
        confirm = (Button) view.findViewById(R.id.confirm);
        llBackground = (LinearLayout) view.findViewById(R.id.llBackground);
        recyclerView = view.findViewById(R.id.quick_recyler);
        top = view.findViewById(R.id.top);
        quickLayout = view.findViewById(R.id.quick_view);
        textNew = view.findViewById(R.id.btn_new);
        qPayProgressDialog = new QPayProgressDialog(getContext());
        confirm.setOnClickListener(this);
        previousData = GeneralPref.getSubisuQuickPay();
        if (previousData != null) {
            try {
                JSONObject jsonObject = new JSONObject(previousData);
                receivedArray = jsonObject.getJSONArray("data");
                JSONObject jsondata;

                savedData = new String[receivedArray.length()];
                for (int i = 0; i < receivedArray.length(); i++) {
                    jsondata = receivedArray.getJSONObject(i);
                    savedData[i] = jsondata.getString("customerId");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        bottomPopUp();
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                llBackground.setBackground(Utility.getAssetImage(getContext(), "subisu_bill_payment_bg.png"));
            } else {
                llBackground.setBackgroundDrawable(Utility.getAssetImage(getContext(), "subisu_bill_payment_bg.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        amount_pay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 1) {
                    Double amount = Double.valueOf(s.toString().trim());
                    if (amount >= 400 && amount < 15001) {
                        isValidPay = true;
                        amount_pay.setError(null);
                    } else {
                        isValidPay = false;
                        amount_pay.setError("Enter amount in range 400 to 15000.");
                    }
                }


            }
        });

        SubisuQuickInterface subisuQuickInterface = new SubisuQuickInterface() {
            @Override
            public void subisuQuick(int position) {
                try {
                    JSONObject clickedArray = (receivedArray.getJSONObject(position));
                    selectedItem = clickedArray.getString("amount");
                    cell_number = clickedArray.getString("phone");
                    id_name = clickedArray.getString("customerId");
                } catch (Exception e) {
                    e.printStackTrace();
                }
              /*
                new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                    @Override
                    public void showDialog() {
                        qPayProgressDialog.show();
                    }

                    @Override
                    public void hideDialog() {
                        qPayProgressDialog.dismiss();

                    }

                }).execute();
                */
                intentFunction();
            }

        };

        SubisuQuickAdapter subisuQuickAdapter = new SubisuQuickAdapter(GeneralPref.getSubisuQuickPay(), subisuQuickInterface);
        recyclerView.setAdapter(subisuQuickAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(MY_FILE_SHARED, Context.MODE_PRIVATE);
        byte_code = sharedPreferences.getString("byte_code", "");
        provider_name = sharedPreferences.getString("provider_name", "");
        text = sharedPreferences.getString("text", "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                cell_number = phone.getText().toString();
                selectedItem = amount_pay.getText().toString();
                id_name = id.getText().toString().trim();
                if (savedData != null) {
                    for (int j = 0; j < savedData.length; j++) {
                        if (id_name.equalsIgnoreCase(savedData[j])) {
                            dublicate = false;
                        }
                    }
                }
                if (isValidPay) {
                    if (!cell_number.isEmpty() && cell_number.length() > 8 && cell_number.length() < 11 && selectedItem.length() > 1 && id_name.length() > 1 && id_name.length() < 16) {
                       /*
                        Intent intent = new Intent(getActivity(), BillpayList.class);
                        intent.putExtra("money", new Decimalformate().decimalFormate(selectedItem));
                        intent.putExtra("byt_code", byte_code);
                        intent.putExtra("cell_number", cell_number);
                        intent.putExtra("user_id", id.getText().toString().trim());
                        startActivity(intent);
                        new Utility().hideSoftkey(getActivity());
                        getActivity().finish();
                      */


                        /*new CheckRewardAPI(selectedItem, byte_code, new DialogInterface() {
                            @Override
                            public void showDialog() {
                                qPayProgressDialog.show();
                            }

                            @Override
                            public void hideDialog() {
                                qPayProgressDialog.dismiss();
                                String previousData = GeneralPref.getSubisuQuickPay();


                                try {

                                    JSONObject mainObject = new JSONObject();
                                    JSONObject subObject = new JSONObject();
                                    if (previousData != null && !previousData.isEmpty()) {
                                        JSONObject receivedObject = new JSONObject(previousData);
                                        receivedArray = receivedObject.getJSONArray("data");
                                        subObject.put("customerId", id_name);
                                        subObject.put("phone", cell_number);
                                        subObject.put("amount", selectedItem);
                                        receivedArray.put(subObject);
                                        mainObject.put("data", receivedArray);
                                        if (dublicate) {
                                            GeneralPref.setSubisuQuickPay(mainObject.toString());
                                        }
                                        Log.d(Utility.TAG, " subisu " + GeneralPref.getSubisuQuickPay());

                                    } else {

                                        receivedArray = new JSONArray();
                                        subObject.put("customerId", id_name);
                                        subObject.put("phone", cell_number);
                                        subObject.put("amount", selectedItem);
                                        receivedArray.put(subObject);
                                        mainObject.put("data", receivedArray);
                                        GeneralPref.setSubisuQuickPay(mainObject.toString());
                                        Log.d(Utility.TAG, " subisu " + GeneralPref.getSubisuQuickPay());
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }).execute();*/
                        String previousData = GeneralPref.getSubisuQuickPay();

                        try {

                            JSONObject mainObject = new JSONObject();
                            JSONObject subObject = new JSONObject();
                            if (previousData != null && !previousData.isEmpty()) {
                                JSONObject receivedObject = new JSONObject(previousData);
                                receivedArray = receivedObject.getJSONArray("data");
                                subObject.put("customerId", id_name);
                                subObject.put("phone", cell_number);
                                subObject.put("amount", selectedItem);
                                receivedArray.put(subObject);
                                mainObject.put("data", receivedArray);
                                if (dublicate) {
                                    GeneralPref.setSubisuQuickPay(mainObject.toString());
                                }
                                Log.d(Utility.TAG, " subisu " + GeneralPref.getSubisuQuickPay());

                            } else {

                                receivedArray = new JSONArray();
                                subObject.put("customerId", id_name);
                                subObject.put("phone", cell_number);
                                subObject.put("amount", selectedItem);
                                receivedArray.put(subObject);
                                mainObject.put("data", receivedArray);
                                GeneralPref.setSubisuQuickPay(mainObject.toString());
                                Log.d(Utility.TAG, " subisu " + GeneralPref.getSubisuQuickPay());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        intentFunction();

                    } else {
                        new Utility().showSnackbar(v, "Enter valid input to proceed further");
                        new Utility().hideSoftkey(getActivity());
                    }
                } else {
                    new Utility().showSnackbar(v, "Enter valid input to proceed further");
                    new Utility().hideSoftkey(getActivity());
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.internet_fragment, new InternetFragment());
                    fragmentTransaction.commit();
                    return true;
                }
                return false;
            }
        });
    }

    private void bottomPopUp() {
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(getView().findViewById(R.id.quick_view));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        if (receivedArray == null || receivedArray.length() < 1) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            quickLayout.setVisibility(View.GONE);
        } else {
            quickLayout.setVisibility(View.VISIBLE);
        }
        textNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    flag = false;
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                }
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        }, 1000);
    }

    public void intentFunction() {

        new Utility().hideSoftkey(getActivity());

        if (billPayList) {
            billPayList = false;
            GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate(byte_code)) / 100) * Double.valueOf(new Decimalformate().decimalFormate(selectedItem))));
            Bundle bundle = new Bundle();
            bundle.putString("money", new Decimalformate().decimalFormate(selectedItem));
            bundle.putString("byt_code", byte_code);
            bundle.putString("cell_number", cell_number);
            bundle.putString("user_id", id_name);
            BillPayListSheet billPayListSheet = new BillPayListSheet();
            billPayListSheet.setArguments(bundle);
            billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                billPayList = true;
            }
        }, 2000);


     /*   Intent intent = new Intent(getActivity(), BillpayList.class);
        intent.putExtra("money", new Decimalformate().decimalFormate(selectedItem));
        intent.putExtra("byt_code", byte_code);
        intent.putExtra("cell_number", cell_number);
        intent.putExtra("user_id", id_name);
        startActivity(intent);
        new Utility().hideSoftkey(getActivity());
        getActivity().finish();*/
    }

}
