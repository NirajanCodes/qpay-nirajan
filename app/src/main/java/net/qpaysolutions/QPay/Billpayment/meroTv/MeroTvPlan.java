package net.qpaysolutions.QPay.Billpayment.meroTv;

/**
 * Created by deadlydragger on 5/14/18.
 */

public class MeroTvPlan {
    String Amount,PlanId,PlanName;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getPlanId() {
        return PlanId;
    }

    public void setPlanId(String planId) {
        PlanId = planId;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }
}
