package net.qpaysolutions.QPay.Billpayment.parbhuTv;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 6/27/18.
 */

public class ParbhuTvFragment extends Fragment {
    TextInputLayout inputLayoutPhone, inputLayoutCashId;
    EditText cashId, phone;
    ArrayList<PrabhuModel> arrayList;
    private LinearLayout mProgressBarLayout;
    private AppCompatButton getDetailsTv;
    private ProgressBar progressBar;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inputLayoutCashId = view.findViewById(R.id.inputLayoutCashId);
        inputLayoutPhone = view.findViewById(R.id.inputLayoutPhone);
        cashId = view.findViewById(R.id.cashId);
        cashId.requestFocus();
        cashId.setFocusable(true);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(inputLayoutCashId, InputMethodManager.SHOW_IMPLICIT);
        phone = view.findViewById(R.id.phone);
        mProgressBarLayout = view.findViewById(R.id.progressBarTvLayout);
        progressBar=view.findViewById(R.id.progressBar);
        addTextChange();
        getDetailsTv = view.findViewById(R.id.getDetailsTv);
        getDetailsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Utility().hideSoftkey(getActivity());
                if (isValid()) {
                    new PrabhuCheckPaymentAPI(cashId.getText().toString(), phone.getText().toString(), ParbhuTvFragment.this, mProgressBarLayout).execute();
                }
            }
        });
//        getSingleData(setArrayList());
    }


    public void dialogFailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.parbhu_pay_main, container, false);
    }

    public boolean isValid() {
        if (cashId.getText().toString() != null && !cashId.getText().toString().isEmpty()) {
        } else {
            inputLayoutCashId.setError("Enter Cas Id");
            return false;
        }
      /*  if (phone.getText().toString() != null && !phone.getText().toString().isEmpty()) {

        } else {
            inputLayoutPhone.setError("Enter Phone");
            return false;
        }*/
        return true;
    }

    private void addTextChange() {
        cashId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutCashId.setError(null);
            }
        });
        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutPhone.setError(null);
            }
        });
    }

    public void loadParbhuTvDetails(String s, String string) {
        Log.d("dineshdetail", "check" + s);
        PrabhuTvDetailsFragment prabhuTvDetailsFragment = new PrabhuTvDetailsFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.internet_fragment, prabhuTvDetailsFragment);
        Bundle bundle = new Bundle();
        bundle.putString("prabhutv", s);
        bundle.putString("phone", string);
        bundle.putString("cashId", cashId.getText().toString());
        prabhuTvDetailsFragment.setArguments(bundle);
        fragmentTransaction.commit();

    }

    public void setClickable() {
        progressBar.getIndeterminateDrawable().setColorFilter(0xFFcc0000, android.graphics.PorterDuff.Mode.SRC_ATOP);
        getDetailsTv.setClickable(true);
        cashId.setEnabled(true);
        getDetailsTv.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
    }

    public void setClickableFalse() {
        getDetailsTv.setClickable(false);
        cashId.setEnabled(false);
        getDetailsTv.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorLightPrimary));
    }

}
