package net.qpaysolutions.QPay.Billpayment.Internetbillpay;

/**
 * Created by deadlydragger on 4/13/18.
 */

public interface FragmentVisible {
    boolean checkVisible();
}
