package net.qpaysolutions.QPay.Billpayment.parbhuTv;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.InternetPayActivity;
import net.qpaysolutions.QPay.CustumClasses.DecimalTextMasking;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by deadlydragger on 6/27/18.
 */

public class PrabhuTvDetailsFragment extends Fragment {
    private static final Pattern sPattern
            = Pattern.compile("^([1-9][0-9]{0,2})?(\\.[0-9]?)?$");
    private LinearLayout changePackageLayout, existingService;
    private RadioButton radioExistingm, radioNew;
    private ArrayList<PrabhuModel> arrayList = new ArrayList<>();
    private String CustomerId, customerName, SessionId, BillAmount, ExpiryDate, MacVcNumber, ProductId, ProductName, SerialNumber, ServiceStartDate, StockId, Balance, cashId;
    private TextInputLayout amountLayout;
    private EditText amount;
    private LinearLayout linearLayoutView;
    boolean billPayList = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.prabhu_tv_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.hideKeyword(getActivity());
        String prabhutv = getArguments().getString("prabhutv");
        final String phone = getArguments().getString("phone");
        cashId = getArguments().getString("cashId");
        linearLayoutView = view.findViewById(R.id.linearLayoutView);
        amount = view.findViewById(R.id.amount);
        amount.addTextChangedListener(new DecimalTextMasking(amount));
        ((InternetPayActivity) getActivity()).titleText.setText("Prabhu Pay Details");
        amountLayout = view.findViewById(R.id.amountLayout);
        Log.d("prabhuTv", prabhutv + "");
        try {
            JSONObject jsonObject = new JSONObject(prabhutv);
            JSONArray data = jsonObject.getJSONArray("data");
            JSONObject datajsonObject = data.getJSONObject(0);
//            JSONArray jsonArray = new JSONArray(prabhutv);
//            JSONObject jsonObject = jsonArray.getJSONObject(0);
            CustomerId = datajsonObject.getString("CustomerId");
            SessionId = datajsonObject.getString("SessionId");
            customerName = datajsonObject.getString("CustomerName");
            Balance = datajsonObject.getString("Balance");
            TextView customerNameText = view.findViewById(R.id.customerName);
            customerNameText.setText(customerName);
            TextView balance = view.findViewById(R.id.balance);
            balance.setText(Balance);
            JSONArray AvailableService = datajsonObject.getJSONArray("AvailableService");
            for (int i = 0; i < AvailableService.length(); i++) {
                JSONObject jsonObject1 = AvailableService.getJSONObject(i);
                PrabhuModel prabhuModel = new PrabhuModel();
                prabhuModel.setBillAmount(jsonObject1.getString("BillAmount"));
                prabhuModel.setMacVcNumber(jsonObject1.getString("MacVcN-umber"));
                prabhuModel.setProductId(jsonObject1.getString("ProductId"));
                prabhuModel.setProductName(jsonObject1.getString("ProductName"));
                prabhuModel.setSerialNumber(jsonObject1.getString("SerialNumber"));
                prabhuModel.setStockId(jsonObject1.getString("StockId"));
                prabhuModel.setCheck(false);
                arrayList.add(prabhuModel);
            }
            spineer(arrayList);
            JSONArray ExistingService = datajsonObject.getJSONArray("ExistingService");
            final JSONObject object = ExistingService.getJSONObject(0);
            BillAmount = new Decimalformate().decimalFormate(object.getString("BillAmount"));
            ExpiryDate = object.getString("ExpiryDate");
            MacVcNumber = object.getString("MacVcNumber");
            ProductId = object.getString("ProductId");
            ProductName = object.getString("ProductName");
            SerialNumber = object.getString("SerialNumber");
            ServiceStartDate = object.getString("ServiceStartDate");
            StockId = object.getString("StockId");
        } catch (Exception e) {
            e.printStackTrace();
            linearLayoutView.setVisibility(View.GONE);
        }
        TextView id = view.findViewById(R.id.customerId);
        id.setText(CustomerId);
        changePackageLayout = view.findViewById(R.id.changePackageLayout);
        existingService = view.findViewById(R.id.existingService);
        changePackageLayout.setVisibility(View.GONE);
        existingService.setVisibility(View.VISIBLE);
        radioNew = view.findViewById(R.id.radioNew);
        radioExistingm = view.findViewById(R.id.radioExisting);
        radioExistingm.setChecked(true);
        radioNew.setChecked(false);
        radioNew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radioExistingm.setChecked(false);
                    changePackageLayout.setVisibility(View.VISIBLE);
                    existingService.setVisibility(View.GONE);
                }
            }
        });
        radioExistingm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radioNew.setChecked(false);
                    existingService.setVisibility(View.VISIBLE);
                    changePackageLayout.setVisibility(View.GONE);
                }
            }
        });
        TextView productName = view.findViewById(R.id.productName);
        productName.setText(ProductName);
        TextView serialNumber = view.findViewById(R.id.serialNumber);
        serialNumber.setText(SerialNumber);
        TextView macVcNumber = view.findViewById(R.id.macVcNumber);
        macVcNumber.setText(MacVcNumber);
        TextView serviceStartDate = view.findViewById(R.id.serviceStartDate);
        serviceStartDate.setText(ServiceStartDate);
        TextView expiryDate = view.findViewById(R.id.expiryDate);
        expiryDate.setText(ExpiryDate);
        final TextView billAmount = view.findViewById(R.id.billAmount);
        billAmount.setText(BillAmount);
        GeneralPref.setUsernameWordlink(ProductName);
        view.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cashId.length() == 11) {

                } else if (cashId.length() == 16) {

                }
//                if (radioNew.isChecked()) {
//                    final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
//                    if (billAmount.getText().toString().length() > 0) {
//                        if (amount.getText().toString().length() > 0) {
//                            Double value = Double.parseDouble(amount.getText().toString());
//                            Log.d("value",value+"");
//                            if (value>=0 || amount.getText().toString().isEmpty()) {
//                                amountLayout.setError("Enter amount");
//                            } else {
//                                new CheckRewardAPI(amount.getText().toString(), "310", new DialogInterface() {
//                                    @Override
//                                    public void showDialog() {
//                                        qPayProgressDialog.show();
//                                    }
//
//                                    @Override
//                                    public void hideDialog() {
//                                        qPayProgressDialog.dismiss();
//                                        Intent intent = new Intent(getActivity(), BillpayList.class);
//                                        intent.putExtra("money", amount.getText().toString());
//                                        intent.putExtra("byt_code", "310");
//                                        intent.putExtra("cell_number", CustomerId);
//                                        intent.putExtra("user_id", SessionId);
//                                        intent.putExtra("customer_name",customerName);
//                                        startActivity(intent);
//                                        getActivity().finish();
//
//                                    }
//                                }).execute();
//                            }
//                        } else {
//                            new CheckRewardAPI(BillAmount, "310", new DialogInterface() {
//                                @Override
//                                public void showDialog() {
//                                    qPayProgressDialog.show();
//                                }
//
//                                @Override
//                                public void hideDialog() {
//                                    qPayProgressDialog.dismiss();
//                                    Intent intent = new Intent(getActivity(), BillpayList.class);
//                                    intent.putExtra("money", BillAmount);
//                                    intent.putExtra("byt_code", "310");
//                                    intent.putExtra("cell_number", CustomerId);
//                                    intent.putExtra("user_id", SessionId);
//                                    intent.putExtra("customer_name",customerName);
//                                    startActivity(intent);
//                                    getActivity().finish();
//
//                                }
//                            }).execute();
//                        }
//                    } else {
//                        if (amount.getText().toString().length() > 0) {
//                            Double value = Double.parseDouble(amount.getText().toString());
//                            Log.d("value1",value+"");
//                            if (value>=0  || amount.getText().toString().isEmpty()) {
//                                amountLayout.setError("Enter amount");
//                            } else {
//                                new CheckRewardAPI(amount.getText().toString(), "310", new DialogInterface() {
//                                    @Override
//                                    public void showDialog() {
//                                        qPayProgressDialog.show();
//                                    }
//
//                                    @Override
//                                    public void hideDialog() {
//                                        qPayProgressDialog.dismiss();
//                                        Intent intent = new Intent(getActivity(), BillpayList.class);
//                                        intent.putExtra("money", amount.getText().toString());
//                                        intent.putExtra("byt_code", "310");
//                                        intent.putExtra("cell_number", CustomerId);
//                                        intent.putExtra("user_id", SessionId);
//                                        intent.putExtra("customer_name",customerName);
//                                        startActivity(intent);
//                                        getActivity().finish();
//
//                                    }
//                                }).execute();
//                            }
//                        } else {
//                            amountLayout.setError("Enter amount");
//                            Log.d("dinesh","error here");
//                        }
//                    }
//                } else {
//                    final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
//                    if (amount.getText().toString().length() > 0) {
//                        if (amount.getText().toString().length() > 0) {
//                            Double value = Double.parseDouble(amount.getText().toString());
//                            Log.d("value3",value+"");
//                            if (value<=0  || amount.getText().toString().isEmpty()) {
//                                amountLayout.setError("Enter amount");
//                            } else {
//                                new CheckRewardAPI(amount.getText().toString(), "310", new DialogInterface() {
//                                    @Override
//                                    public void showDialog() {
//                                        qPayProgressDialog.show();
//                                    }
//                                    @Override
//                                    public void hideDialog() {
//                                        qPayProgressDialog.dismiss();
//                                        Intent intent = new Intent(getActivity(), BillpayList.class);
//                                        intent.putExtra("money", amount.getText().toString());
//                                        intent.putExtra("byt_code", "310");
//                                        intent.putExtra("cell_number", CustomerId);
//                                        intent.putExtra("user_id", SessionId);
//                                        intent.putExtra("customer_name",customerName);
//                                        startActivity(intent);
//                                        getActivity().finish();
//
//                                    }
//                                }).execute();
//                            }
//                        } else {
//                            amountLayout.setError("Enter amount");
//                        }
//
//
//                    } else {
//                        new CheckRewardAPI(billAmount.getText().toString(), "310", new DialogInterface() {
//                            @Override
//                            public void showDialog() {
//                                qPayProgressDialog.show();
//                            }
//                            @Override
//                            public void hideDialog() {
//                                qPayProgressDialog.dismiss();
//                                Intent intent = new Intent(getActivity(), BillpayList.class);
//                                intent.putExtra("money", billAmount.getText().toString());
//                                intent.putExtra("byt_code", "310");
//                                intent.putExtra("cell_number", CustomerId);
//                                intent.putExtra("user_id", SessionId);
//                                intent.putExtra("customer_name",customerName);
//                                startActivity(intent);
//                                getActivity().finish();
//                            }
//                        }).execute();
//
//                    }
//
//                }
                if (radioNew.isChecked()) {
                    Log.d("check1", "check1");
                    final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
                    if (billAmount.getText().toString().length() > 0) {
                        Log.d("check2", "check2");
                        if (amount.getText().toString().length() > 0) {
                            Log.d("check3", "check3");
                            Double value = Double.parseDouble(amount.getText().toString());
                            Log.d("value", value + "");
                            if (value <= 0 || amount.getText().toString().isEmpty()) {
                                Log.d("check4", "check4");
                                amountLayout.setError("Enter amount");
                            } else {
                                Log.d("check5", "check5");
                                /*new CheckRewardAPI(amount.getText().toString(), "310", new DialogInterface() {
                                    @Override
                                    public void showDialog() {
                                        qPayProgressDialog.show();
                                    }
                                    @Override
                                    public void hideDialog() {
                                        qPayProgressDialog.dismiss();
                                        Intent intent = new Intent(getActivity(), BillpayList.class);
                                        intent.putExtra("money", amount.getText().toString());
                                        intent.putExtra("byt_code", "310");
                                        intent.putExtra("cell_number", *//*customerName*//*CustomerId);
                                        intent.putExtra("user_id", SessionId);
                                        intent.putExtra("customerId", CustomerId);
                                        intent.putExtra("cash_Id",cashId);
                                        startActivity(intent);
                                        getActivity().finish();

                                    }
                                }).execute();*/

                                if (billPayList) {
                                    billPayList = false;
                                    GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("310")) / 100) * Double.valueOf(BillAmount)));
                                    Bundle bundle = new Bundle();
                                    bundle.putString("money", BillAmount);
                                    bundle.putString("byt_code", "310");
                                    bundle.putString("cell_number", /*customerName*/CustomerId);
                                    bundle.putString("user_id", SessionId);
                                    bundle.putString("customerId", CustomerId);
                                    bundle.putString("cash_Id", cashId);
                                    BillPayListSheet billPayListSheet = new BillPayListSheet();
                                    billPayListSheet.setArguments(bundle);
                                    billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(() -> billPayList = true, 2000);

                            }
                        } else {
                            Log.d("check6", "check6");
                           /* new CheckRewardAPI(BillAmount, "310", new DialogInterface() {
                                @Override
                                public void showDialog() {
                                    qPayProgressDialog.show();
                                }

                                @Override
                                public void hideDialog() {
                                    qPayProgressDialog.dismiss();
                                    Intent intent = new Intent(getActivity(), BillpayList.class);
                                    intent.putExtra("money", BillAmount);
                                    intent.putExtra("byt_code", "310");
                                    intent.putExtra("cell_number", *//*customerName*//*CustomerId);
                                    intent.putExtra("user_id", SessionId);
                                    intent.putExtra("customerId", CustomerId);
                                    intent.putExtra("cash_Id",cashId);
                                    startActivity(intent);
                                    getActivity().finish();

                                }
                            }).execute();*/
                            if (billPayList) {
                                billPayList = false;
                                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("310")) / 100) * Double.valueOf(BillAmount)));
                                Bundle bundle = new Bundle();
                                bundle.putString("money", BillAmount);
                                bundle.putString("byt_code", "310");
                                bundle.putString("cell_number", /*customerName*/CustomerId);
                                bundle.putString("user_id", SessionId);
                                bundle.putString("customerId", CustomerId);
                                bundle.putString("cash_Id", cashId);
                                BillPayListSheet billPayListSheet = new BillPayListSheet();
                                billPayListSheet.setArguments(bundle);
                                billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
                            }
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    billPayList = true;
                                }
                            }, 2000);

                        }
                    } else {
                        Log.d("check7", "check7");
                        if (amount.getText().toString().length() > 0) {
                            Log.d("check8", "check8");
                            Double value = Double.parseDouble(amount.getText().toString());
                            Log.d("value1", value + "");
                            if (value <= 0 || amount.getText().toString().isEmpty()) {
                                Log.d("check9", "check9");
                                amountLayout.setError("Enter amount");
                            } else {
                                Log.d("check10", "check10");
                              /*  new CheckRewardAPI(amount.getText().toString(), "310", new DialogInterface() {
                                    @Override
                                    public void showDialog() {
                                        qPayProgressDialog.show();
                                    }

                                    @Override
                                    public void hideDialog() {
                                        qPayProgressDialog.dismiss();
                                        Intent intent = new Intent(getActivity(), BillpayList.class);
                                        intent.putExtra("money", amount.getText().toString());
                                        intent.putExtra("byt_code", "310");
                                        intent.putExtra("noProductName",true);
                                        intent.putExtra("cell_number", *//*customerName*//*CustomerId);
                                        intent.putExtra("user_id", SessionId);
                                        intent.putExtra("customerId", CustomerId);
                                        intent.putExtra("cash_Id",cashId);
                                        startActivity(intent);
                                        getActivity().finish();

                                    }
                                }).execute();*/
                                if (billPayList) {
                                    billPayList = false;
                                    GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("310")) / 100) * Double.valueOf(amount.getText().toString())));
                                    Bundle bundle = new Bundle();
                                    bundle.putString("money", amount.getText().toString().replace(",",""));
                                    bundle.putString("byt_code", "310");
                                    bundle.putBoolean("noProductName", true);
                                    bundle.putString("cell_number", /*customerName*/CustomerId);
                                    bundle.putString("user_id", SessionId);
                                    bundle.putString("customerId", CustomerId);
                                    bundle.putString("cash_Id", cashId);
                                    BillPayListSheet billPayListSheet = new BillPayListSheet();
                                    billPayListSheet.setArguments(bundle);
                                    billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        billPayList = true;
                                    }
                                }, 2000);
                            }
                        } else {
                            Log.d("check11", "check12");
                            amountLayout.setError("Enter amount");
                            Log.d("dinesh", "error here");
                        }
                    }
                } else {
//                    final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
//                    Log.d("check12","check12");
//                    if (amount.getText().toString().length() > 0) {
//                        Log.d("check13","check13");
//                        if (amount.getText().toString().length() > 0) {
//                            Log.d("check14","check14");
//                            Double value = Double.parseDouble(amount.getText().toString());
//                            Log.d("value3",value+"");
//                            if (value<=0  || amount.getText().toString().isEmpty()) {
//                                Log.d("check15","check15");
//                                amountLayout.setError("Enter amount");
//                            } else {
//                                Log.d("check16","check16");
//                                new CheckRewardAPI(amount.getText().toString(), "310",getActivity(), new DialogInterface() {
//                                    @Override
//                                    public void showDialog() {
//                                        qPayProgressDialog.show();
//                                    }
//                                    @Override
//                                    public void hideDialog() {
//                                        qPayProgressDialog.dismiss();
//                                        Intent intent = new Intent(getActivity(), BillpayList.class);
//                                        intent.putExtra("money", amount.getText().toString());
//                                        intent.putExtra("byt_code", "310");
//                                        intent.putExtra("cell_number", customerName);
//                                        intent.putExtra("user_id", SessionId);
//                                        startActivity(intent);
//                                        getActivity().finish();
//
//                                    }
//                                }).execute();
//                            }
//                        } else {
//                            amountLayout.setError("Enter amount");
//                        }
//
//
//                    } else {
//                        new CheckRewardAPI(billAmount.getText().toString(), "310",getActivity(), new DialogInterface() {
//                            @Override
//                            public void showDialog() {
//                                qPayProgressDialog.show();
//                            }
//                            @Override
//                            public void hideDialog() {
//                                qPayProgressDialog.dismiss();
//                                Intent intent = new Intent(getActivity(), BillpayList.class);
//                                intent.putExtra("money", billAmount.getText().toString());
//                                intent.putExtra("byt_code", "310");
//                                intent.putExtra("cell_number", customerName);
//                                intent.putExtra("user_id", SessionId);
//                                startActivity(intent);
//                                getActivity().finish();
//                            }
//                        }).execute();
//
//                    }

                }
                Log.d("check1", "check1");
                final QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(getContext());
                if (billAmount.getText().toString().length() > 0) {
                    Log.d("check2", "check2");
                    String enteredAmount = null;
                    if (amount.getText().toString() != null) {
                        enteredAmount = amount.getText().toString();
                        enteredAmount = enteredAmount.replace(",", "");
                    }
                    if (enteredAmount != null && enteredAmount.length() > 0 && !enteredAmount.equalsIgnoreCase(".") && !((Double.valueOf(enteredAmount)) < 1)) {

                        Log.d("check3", "check3");
                        Double value = Double.parseDouble(enteredAmount);
                        Log.d("value", value + "");
                        if (value <= 0 || amount.getText().toString().isEmpty()) {
                            Log.d("check4", "check4");
                            amountLayout.setError("Enter amount");

                        } else {
                            Log.d("check5", "check5");
                           /* new CheckRewardAPI(amount.getText().toString(), "310", new DialogInterface() {
                                @Override
                                public void showDialog() {
                                    qPayProgressDialog.show();
                                }

                                @Override
                                public void hideDialog() {
                                    qPayProgressDialog.dismiss();
                                    Intent intent = new Intent(getActivity(), BillpayList.class);
                                    intent.putExtra("money", amount.getText().toString());
                                    intent.putExtra("byt_code", "310");
                                    intent.putExtra("cell_number", *//*customerName*//*CustomerId);
                                    intent.putExtra("user_id", SessionId);
                                    intent.putExtra("customerId", CustomerId);
                                    intent.putExtra("cash_Id",cashId);
                                    startActivity(intent);
                                    getActivity().finish();

                                }
                            }).execute();*/
                            if (billPayList) {
                                boolean payable = false;
                                if (cashId.length() == 11) {
                                    if (Double.valueOf(enteredAmount) >= 200) {
                                        payable = true;
                                    }
                                } else if (cashId.length() == 16) {
                                    if (Double.valueOf(enteredAmount) >= 300) {
                                        payable = true;
                                    }
                                }
                                billPayList = false;
                                if (payable) {
                                    GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("310")) / 100) * Double.valueOf(enteredAmount)));
                                    Bundle bundle = new Bundle();
                                    bundle.putString("money", amount.getText().toString().replace(",",""));
                                    bundle.putString("byt_code", "310");
                                    bundle.putString("cell_number", /*customerName*/CustomerId);
                                    bundle.putString("user_id", SessionId);
                                    bundle.putString("customerId", CustomerId);
                                    bundle.putString("cash_Id", cashId);
                                    BillPayListSheet billPayListSheet = new BillPayListSheet();
                                    billPayListSheet.setArguments(bundle);
                                    billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
                                } else if (cashId.length() == 11) {
                                    Utility.showSnackbar(getView(), "Enter amount more than 200");
                                } else {
                                    Utility.showSnackbar(getView(), "Enter amount more than 300");
                                }
                            }
                            Handler handler = new Handler();
                            handler.postDelayed(() -> billPayList = true, 2000);
                        }
                    } else {
                        Log.d("check6", "check6");
                     /*   new CheckRewardAPI(BillAmount, "310", new DialogInterface() {
                            @Override
                            public void showDialog() {
                                qPayProgressDialog.show();
                            }

                            @Override
                            public void hideDialog() {
                                qPayProgressDialog.dismiss();
                                Intent intent = new Intent(getActivity(), BillpayList.class);
                                intent.putExtra("money", BillAmount);
                                intent.putExtra("byt_code", "310");
                                intent.putExtra("cell_number", *//*customerName*//*CustomerId);
                                intent.putExtra("user_id", SessionId);
                                intent.putExtra("customerId", CustomerId);
                                intent.putExtra("cash_Id",cashId);
                                startActivity(intent);
                                getActivity().finish();


                            }
                        }).execute();*/

                        if (billPayList) {
                            boolean payable = false;
                            if (cashId.length() == 11) {
                                if (Double.valueOf(BillAmount) >= 200) {
                                    payable = true;
                                }
                            } else if (cashId.length() == 16) {
                                if (Double.valueOf(BillAmount) >= 300) {
                                    payable = true;
                                }
                            }
                            billPayList = false;
                            if (payable) {
                                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("310")) / 100) * Double.valueOf(BillAmount)));
                                Bundle bundle = new Bundle();
                                bundle.putString("money", BillAmount);
                                bundle.putString("byt_code", "310");
                                bundle.putString("cell_number", /*customerName*/CustomerId);
                                bundle.putString("user_id", SessionId);
                                bundle.putString("customerId", CustomerId);
                                bundle.putString("cash_Id", cashId);
                                BillPayListSheet billPayListSheet = new BillPayListSheet();
                                billPayListSheet.setArguments(bundle);
                                billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
                            }
                        } else if (cashId.length() == 11) {
                            Utility.showSnackbar(getView(), "Enter amount more than 200");
                        } else {
                            Utility.showSnackbar(getView(), "Enter amount more than 300");
                        }
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                billPayList = true;
                            }
                        }, 2000);
                    }
                } else
                {
                    Log.d("check7", "check7");
                    if (amount.getText().toString().length() > 0) {
                        Log.d("check8", "check8");
                        Double value = Double.parseDouble((amount.getText().toString()).replace(",",""));
                        Log.d("value1", value + "");
                        if (value <= 0 || amount.getText().toString().isEmpty()) {
                            Log.d("check9", "check9");
                            amountLayout.setError("Enter amount");
                        } else {
                            Log.d("check10", "check10");
                       /*     new CheckRewardAPI(amount.getText().toString(), "310", new DialogInterface() {
                                @Override
                                public void showDialog() {
                                    qPayProgressDialog.show();
                                }

                                @Override
                                public void hideDialog() {
                                    qPayProgressDialog.dismiss();
                                    Intent intent = new Intent(getActivity(), BillpayList.class);
                                    intent.putExtra("money", amount.getText().toString());
                                    intent.putExtra("byt_code", "310");
                                    intent.putExtra("noProductName",true);
                                    intent.putExtra("cell_number", *//*customerName*//*CustomerId);
                                    intent.putExtra("user_id", SessionId);
                                    intent.putExtra("customerId", CustomerId);
                                    intent.putExtra("cash_Id",cashId);
                                    startActivity(intent);
                                    getActivity().finish();

                                }
                            }).execute();*/

                            if (billPayList) {
                                billPayList = false;
                                String payAmount=amount.getText().toString().replace(",","");
                                GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("310")) / 100) * Double.valueOf(payAmount)));
                                Bundle bundle = new Bundle();
                                bundle.putString("money", payAmount);
                                bundle.putString("byt_code", "310");
                                bundle.putBoolean("noProductName", true);
                                bundle.putString("cell_number", /*customerName*/CustomerId);
                                bundle.putString("user_id", SessionId);
                                bundle.putString("customerId", CustomerId);
                                bundle.putString("cash_Id", cashId);
                                BillPayListSheet billPayListSheet = new BillPayListSheet();
                                billPayListSheet.setArguments(bundle);
                                billPayListSheet.show(getActivity().getSupportFragmentManager(), getTag());
                            }

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    billPayList = true;
                                }
                            }, 2000);


                        }
                    } else {
                        Log.d("check11", "check12");
                        amountLayout.setError("Enter amount");
                        Log.d("dinesh", "error here");
                    }

                }
            }
//            }
        });

    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    public void setBillAmount(String billAmount) {
        BillAmount = billAmount;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public void spineer(ArrayList<PrabhuModel> arrayList) {
        RecyclerView packagePrabhu = getView().findViewById(R.id.packagePrabhu);
        packagePrabhu.setLayoutManager(new LinearLayoutManager(getContext()));
        packagePrabhu.setHasFixedSize(true);
        ParbhuAdapter parbhuAdapter = new ParbhuAdapter(arrayList, PrabhuTvDetailsFragment.this);
        packagePrabhu.setAdapter(parbhuAdapter);


    }

}
