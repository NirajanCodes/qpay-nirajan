package net.qpaysolutions.QPay.Billpayment.inusurance;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Billpayment.BillpayList;
import net.qpaysolutions.QPay.Billpayment.utility.UtilitybillPay;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 3/27/18.
 */

public class NepalLifeInsuranceFragment extends Fragment implements DialogInterface {
    private TextInputLayout inputLayoutId;
    private EditText policyNumber;
    private QPayProgressDialog qPayProgressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nepallife, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((UtilitybillPay) getActivity()).setToolbar_text("Nepal Life Insurance");
        inputLayoutId = (TextInputLayout) view.findViewById(R.id.inputLayoutId);
        policyNumber = (EditText) view.findViewById(R.id.policyNumber);
        policyNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutId.setVisibility(View.GONE);
                inputLayoutId.setError(null);
            }
        });
        qPayProgressDialog = new QPayProgressDialog(getContext());

        view.findViewById(R.id.searchButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (policyNumber.getText().toString() != null && !policyNumber.getText().toString().isEmpty()){
                    new Utility().hideSoftkey(getActivity());
                    new NepallifeCheckpayment(NepalLifeInsuranceFragment.this).execute();
                }else {
                    inputLayoutId.setVisibility(View.VISIBLE);
                    inputLayoutId.setError("Enter Policy Number");
                }
            }
        });

    }

    public String getPolicyNumber() {
        return policyNumber.getText().toString();
    }

    @Override
    public void showDialog() {
        qPayProgressDialog.show();
    }

    @Override
    public void hideDialog() {
        qPayProgressDialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (qPayProgressDialog != null) {
            qPayProgressDialog.dismiss();
        }
    }

    public void showLayout(final JSONObject jsonObject) {
        LinearLayout paymentDetails = (LinearLayout) getView().findViewById(R.id.paymentDetails);
        paymentDetails.setVisibility(View.VISIBLE);
        try {
            TextView customerName = (TextView) getView().findViewById(R.id.customerName);
            TextView policyNo = (TextView) getView().findViewById(R.id.policyNo);
            TextView paymentMode = (TextView) getView().findViewById(R.id.paymentMode);
            TextView dueDate = (TextView) getView().findViewById(R.id.dueDate);
            TextView status = (TextView) getView().findViewById(R.id.status);
            TextView totalBalance = (TextView) getView().findViewById(R.id.totalBalance);
            TextView billAmount = (TextView) getView().findViewById(R.id.billAmount);
            customerName.setText(jsonObject.getString("Name"));
            policyNo.setText(jsonObject.getString("PolicyNo"));
            paymentMode.setText(jsonObject.getString("PaymentMode"));
            dueDate.setText(jsonObject.getString("DueDate"));
            status.setText(jsonObject.getString("Status"));
            if ( !jsonObject.getString("TotalBalance").isEmpty()){
                totalBalance.setText("NPR " + jsonObject.getString("TotalBalance"));
            }
            if (!jsonObject.getString("BillAmount").isEmpty()){
                billAmount.setText("NPR " + jsonObject.getString("BillAmount"));
            }
            getView().findViewById(R.id.submitButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), BillpayList.class);
                    try {
                        GeneralPref.setUsernameWordlink(jsonObject.getString("Name"));
                        intent.putExtra("money", jsonObject.getString("BillAmount"));
                        intent.putExtra("byt_code", "701");
                        intent.putExtra("cell_number",policyNumber.getText().toString());
                        intent.putExtra("user_id", jsonObject.getString("Name"));
                        intent.putExtra("sc_no","");
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showError( String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText("Failed!");
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
}
