package net.qpaysolutions.QPay.Billpayment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deadlydragger on 8/28/16.
 */
public class QuickPayActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView pay_option_image;
    TextView provider;
    Button confirm;
    String bill_pay_code = "";
    String user_id="";
    double lat, lon;
    String balance;
    String cell_number, selectedItem="";
    List<String> ncellRechargeAmountList;
    String _id, quick_number;
    QPayCustomerDatabase QPayCustomerDatabase;
    Toolbar toolbar;
    TextView amount_pay,phone,toolbar_text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.quick_pay_bill);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar_text=(TextView)toolbar.findViewById(R.id.title);
            toolbar_text.setText("Quick Pay");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(QuickPayActivity.this, MainActivity.class));

                }
            });
            QPayCustomerDatabase = new QPayCustomerDatabase(this);
            QPayCustomerDatabase.getReadableDatabase();
            pay_option_image = (ImageView) findViewById(R.id.pay_option_image);
            provider = (TextView) findViewById(R.id.provider);
            phone = (TextView) findViewById(R.id.phone);
            amount_pay = (TextView) findViewById(R.id.amount_pay);
            confirm = (Button) findViewById(R.id.confirm);
//            _id = getIntent().getStringExtra("_id");
            _id= GeneralPref.getPhone();
            quick_number = QPayCustomerDatabase.getQuickNumber(_id);
            bill_pay_code = QPayCustomerDatabase.getQuickCode(_id);
            user_id= QPayCustomerDatabase.getQuickId(_id);
            confirm.setOnClickListener(this);
            phone.setText(quick_number);
            Log.d(Utility.TAG,"quick numuber : " + quick_number + " code : " + bill_pay_code+"id = " + _id);

            switch (bill_pay_code){
                case "201":
                    pay_option_image.setImageResource(R.drawable.ncell);
                    provider.setText("Ncell Recharge");
                    break;
                case "203":
                    pay_option_image.setImageResource(R.drawable.nct_logo);
                    provider.setText("NTC Prepaid");
                    break;
                case "205":
                    pay_option_image.setImageResource(R.drawable.nct_logo);
                    provider.setText("NTC Postpaid");
                    break;
                case "204":
                    pay_option_image.setImageResource(R.drawable.adsl);
                    provider.setText("ADSL Landline Recharge");
                    break;
                case "206":
                    pay_option_image.setImageResource(R.drawable.utl);
                    provider.setText("UTL Recharge");
                    break;
                case "301":
                    provider.setText("Subisu internet bill pay");
                    pay_option_image.setImageResource(R.drawable.subisu);
                    break;
                case "304":
                    provider.setText("ADSL unlimited bill pay");
                    pay_option_image.setImageResource(R.drawable.adsl);
                    break;
                case "305":
                    provider.setText("ADSL volume bill pay");
                    pay_option_image.setImageResource(R.drawable.adsl);
                    break;
                case "306":
                    LinearLayout phone_layout =(LinearLayout)findViewById(R.id.phone_layout);
                    phone_layout.setVisibility(View.GONE);
                    LinearLayout dishome =(LinearLayout)findViewById(R.id.dishome);
                    dishome.setVisibility(View.VISIBLE);
                    EditText id =(EditText)findViewById(R.id.id);
                    id.setText(user_id);
                    provider.setText("DishHome bill pay");
                    pay_option_image.setImageResource(R.drawable.dishhome);
                    break;
            }
            lat = ((CustomerApplication) this.getApplication()).getLat();
            lon = ((CustomerApplication) this.getApplication()).getLng();
            seteclAmount();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(QuickPayActivity.this, MainActivity.class));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                try {
                    if (selectedItem.length() > 1){
                        Intent intent = new Intent(QuickPayActivity.this, BillpayList.class);
                        intent.putExtra("money", selectedItem);
                        intent.putExtra("user_id",user_id);
                        intent.putExtra("cell_number", quick_number);
                        intent.putExtra("lat", lat);
                        intent.putExtra("lon", lon);
                        intent.putExtra("byt_code", bill_pay_code);
                        intent.putExtra("balance", balance);
                        Log.d(Utility.TAG,"quick post send : " +  selectedItem+user_id+quick_number+lat+lon+bill_pay_code+balance);
                        startActivity(intent);
                    }else {
                        new Utility().showSnackbar(amount_pay,"Enter valid amount.");
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
        }
    }


    public void seteclAmount() {
        switch (bill_pay_code) {
            case "201":
                String[] ncellRechargeValues = getResources().getStringArray(R.array.ncell_recharge_amount);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(ncellRechargeValues));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        hideSoftkey(phone);
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });
                break;
            case "203":
                String[] ntc_prepaid = getResources().getStringArray(R.array.ntc_prepaid);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(ntc_prepaid));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });
                break;
            case "205":
                String[] ntc_postpaid = getResources().getStringArray(R.array.ntc_postpaid);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(ntc_postpaid));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });
                break;
            case "204":
                String[] adsl = getResources().getStringArray(R.array.landline);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(adsl));
                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });
                break;
            case "206":
                String[] utl = getResources().getStringArray(R.array.utl);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(utl));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });
                break;
            case  "301":
                String[] sobisu = getResources().getStringArray(R.array.sobisu);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(sobisu));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });

                break;
            case "304":

                String[] adsl_unlimited = getResources().getStringArray(R.array.adsl_unlimited);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(adsl_unlimited));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });

                break;
            case "305":
                String[] adsl_volume = getResources().getStringArray(R.array.adsl_volume);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(adsl_volume));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });
                break;
            case "306":
                String[] dishome = getResources().getStringArray(R.array.dishome);
                ncellRechargeAmountList = new ArrayList<>(Arrays.asList(dishome));

                amount_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(QuickPayActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                            @Override
                            public void onDatePickCompleted(int month, String dateDesc) {
                                selectedItem = dateDesc;
                                amount_pay.setText(dateDesc);
                            }
                        }).textConfirm("DONE")
                                .textCancel("CANCEL")
                                .btnTextSize(16)
                                .viewTextSize(25)
                                .setValues(ncellRechargeAmountList)
                                .build();
                        pickerPopWin.showPopWin(QuickPayActivity.this);
                    }
                });

                break;

            default:
                break;
        }

    }

}
