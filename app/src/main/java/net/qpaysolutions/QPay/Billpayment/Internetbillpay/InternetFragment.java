package net.qpaysolutions.QPay.Billpayment.Internetbillpay;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import net.qpaysolutions.QPay.Billpayment.parbhuTv.ParbhuTvFragment;
import net.qpaysolutions.QPay.Billpayment.utility.subisu.SubishuFragment;
import net.qpaysolutions.QPay.Billpayment.vianet.ViaNetFragment;
import net.qpaysolutions.QPay.Billpayment.wordlink.WordLinkFragment;
import net.qpaysolutions.QPay.CASHBACK.PrefCashBack;
import net.qpaysolutions.QPay.CustumClasses.FontTextViewRegular;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by deadlydragger on 4/9/17.
 */

public class InternetFragment extends Fragment implements View.OnClickListener, FragmentVisible {
    private LinearLayout subisu, dishome, adsl_unlimited, adsl_volume, net_tv_epin, brodlink, dishome_epin, sim_tv, wordLink, websurfer, arrownet, meroTv, vianet, merotv_topup, prabhu;
    private LinearLayout enter, scroll;
    private EditText phone_number;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String term_id, bytecode, balance;
    double lat, lon;
    ImageView imageView;
    String selectedItem = "";
    EditText id;
    String user_id, app_id;
    List<String> ncellRechargeAmountList;
    TextView amount_pay, paying_name;
    boolean allowBonus;
    SharedPreferences sharedPreferences;
    static String MY_FILE_SHARED = "Mybalance";
    String byte_code, provider_name, text;
    boolean isvisible;
    Boolean tv;
    Bundle bundle;
    Fragment fragment;
    FontTextViewRegular discountRate_worldlink, discountRate_websurfer, discountRate_arrownet, discountRate_subisu, discountRate_dishome, discountRate_adsl_unlimited, discountRate_adsl_volume, discountRate_net_tv_epin, discountRate_brodlink, discountRate_dishome_epin, discountRate_sim_tv, discountRate_wordLink, discountRate_meroTv, discountRate_vianet, discountRate_merotv_topup, cashBackPrabhuPay;
    String dis_subisu, dis_dishome, dis_adsl_unlimited, dis_adsl_volume, dis_net_tv_epin, dis_brodlink, dis_dishome_epin, dis_sim_tv, dis_arrownet, dis_websurfer, dis_wordLink, dis_meroTv, dis_vianet, dis_merotv_topup, dis_prabhu;
    RelativeLayout cashback_subisu, cashback_dishome, cashback_adsl_unlimited, cashback_adsl_volume, cashback_net_tv_epin, cashback_brodlink, cashback_dishome_epin, cashback_sim_tv, cashback_wordLink, cashback_websurfer, cashback_meroTv, cashback_vianet, cashback_merotv_topup, cashback_prabhu, cashback_arrownet;
    QPayProgressDialog qPayProgressDialog;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.internet_fragment, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        qPayProgressDialog = new QPayProgressDialog(getActivity());
        try {
            ((InternetPayActivity) getActivity()).setVisibleFragment("main");
            if (tv) {
                ((InternetPayActivity) getActivity()).titleText.setText("Television Bill Pay");
            } else {
                ((InternetPayActivity) getActivity()).titleText.setText("Internet Bill Pay");
            }

            TextView discount = (TextView) view.findViewById(R.id.discountRate);
            TextView serviceProviderTextView = (TextView) view.findViewById(R.id.serviceProvider);
            LinearLayout cashBackLayoutLinearLayout = (LinearLayout) view.findViewById(R.id.cashBackLayout);
            Utility.setDiscount(discount, serviceProviderTextView, new PrefCashBack(getContext()).getCABLE(), cashBackLayoutLinearLayout);
            sharedPreferences = getActivity().getSharedPreferences(MY_FILE_SHARED, Context.MODE_PRIVATE);

            if (!GeneralPref.getAllowBonus()) {
                new Checkbonusflag().execute();
            }

            allowBonus = GeneralPref.getAllowBonus();
            QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
            QPayCustomerDatabase.getReadableDatabase();
            term_id = QPayCustomerDatabase.getCustomerID();
            app_id = QPayCustomerDatabase.getKeyAppId();
            dishome_epin = (LinearLayout) view.findViewById(R.id.dishome_epin);

            //cashback

            discountRate_subisu = view.findViewById(R.id.discountRate_subisu);
            discountRate_websurfer = view.findViewById(R.id.discountRate_webSurfer);
            discountRate_arrownet = view.findViewById(R.id.discountRate_webSurfer);
            discountRate_dishome = view.findViewById(R.id.discountRate_dishome);
            discountRate_adsl_unlimited = view.findViewById(R.id.discountRate_adsl_unlimited);
            discountRate_adsl_volume = view.findViewById(R.id.discountRate_adsl_volume);
            discountRate_net_tv_epin = view.findViewById(R.id.discountRate_net_tv_epin);
            discountRate_brodlink = view.findViewById(R.id.discountRate_brodlink);
            discountRate_dishome_epin = view.findViewById(R.id.discountRate_dishome_epin);
            discountRate_sim_tv = view.findViewById(R.id.discountRate_simTv);
            discountRate_wordLink = view.findViewById(R.id.discountRate_worldlink);
            discountRate_meroTv = view.findViewById(R.id.discountRate_merotv_topup);
            discountRate_merotv_topup = view.findViewById(R.id.discountRate_merotv_topup);
            discountRate_vianet = view.findViewById(R.id.discountRate_vianet);
            cashBackPrabhuPay = view.findViewById(R.id.cashBackPrabhuPay);

            brodlink = view.findViewById(R.id.brodlink);
            net_tv_epin = view.findViewById(R.id.net_tv_epin);
            scroll = view.findViewById(R.id.scroll_view);
            prabhu = view.findViewById(R.id.prabhu);
            subisu = view.findViewById(R.id.sobisu);
            adsl_unlimited = view.findViewById(R.id.adsl_unlimited);
            adsl_volume = view.findViewById(R.id.adsl_volume);
            dishome = view.findViewById(R.id.dishome);
            enter = view.findViewById(R.id.enter_form);
            phone_number = view.findViewById(R.id.phone);
            amount_pay = view.findViewById(R.id.amount_pay);
            paying_name = view.findViewById(R.id.paying_name);
            imageView = (ImageView) view.findViewById(R.id.pay_option_image);
            sim_tv = view.findViewById(R.id.simTv);
            wordLink = view.findViewById(R.id.wordLink);
            websurfer = view.findViewById(R.id.websurfer);
            arrownet = view.findViewById(R.id.arrownet);
            meroTv = view.findViewById(R.id.merotv_epin);
            vianet = view.findViewById(R.id.vianet);
            merotv_topup = view.findViewById(R.id.merotv_topup);

            cashback_prabhu = view.findViewById(R.id.cashback_prabhu);
            cashback_subisu = view.findViewById(R.id.cashback_subisu);
            cashback_adsl_unlimited = view.findViewById(R.id.cashback_adsl_unlimited);
            cashback_adsl_volume = view.findViewById(R.id.cashback_adsl_volume);
            cashback_dishome = view.findViewById(R.id.cashback_dishome);
            cashback_sim_tv = view.findViewById(R.id.cashback_simTv);
            cashback_wordLink = view.findViewById(R.id.cashback_worldlink);
            cashback_meroTv = view.findViewById(R.id.cashback_merotv_epin);
            cashback_vianet = view.findViewById(R.id.cashback_vianet);
            cashback_merotv_topup = view.findViewById(R.id.cashback_merotv_topup);
            cashback_brodlink = view.findViewById(R.id.cashback_brodlink);
            cashback_net_tv_epin = view.findViewById(R.id.cashback_net_tv_epin);
            cashback_dishome_epin = view.findViewById(R.id.cashback_dishome_epin);

            if (tv) {
                dishome_epin.setVisibility(View.GONE);
                subisu.setVisibility(View.GONE);
                adsl_unlimited.setVisibility(View.GONE);
                adsl_volume.setVisibility(View.GONE);
                brodlink.setVisibility(View.GONE);
                wordLink.setVisibility(View.GONE);
                websurfer.setVisibility(View.GONE);
                vianet.setVisibility(View.GONE);
                meroTv.setVisibility(View.GONE);
                arrownet.setVisibility(View.GONE);
            } else {
                dishome_epin.setVisibility(View.GONE);
                subisu.setVisibility(View.VISIBLE);
                adsl_unlimited.setVisibility(View.VISIBLE);
                adsl_volume.setVisibility(View.VISIBLE);
                brodlink.setVisibility(View.VISIBLE);
                wordLink.setVisibility(View.VISIBLE);
                vianet.setVisibility(View.VISIBLE);
                websurfer.setVisibility(View.VISIBLE);
                arrownet.setVisibility(View.VISIBLE);
                meroTv.setVisibility(View.GONE);
                prabhu.setVisibility(View.GONE);
                dishome.setVisibility(View.GONE);
                net_tv_epin.setVisibility(View.GONE);
                sim_tv.setVisibility(View.GONE);
                meroTv.setVisibility(View.GONE);
                merotv_topup.setVisibility(View.GONE);
            }

            setCashback("306");
            setCashback("301");
            setCashback("302");
            setCashback("208");
            setCashback("307");
            setCashback("305");
            setCashback("304");
            setCashback("613");
            setCashback("616");
            setCashback("303");
            setCashback("310");

            merotv_topup.setOnClickListener(this);
            vianet.setOnClickListener(this);
            meroTv.setOnClickListener(this);
            wordLink.setOnClickListener(this);
            websurfer.setOnClickListener(this);
            arrownet.setOnClickListener(this);
            sim_tv.setOnClickListener(this);
            dishome_epin.setOnClickListener(this);
            brodlink.setOnClickListener(this);
            net_tv_epin.setOnClickListener(this);
            adsl_volume.setOnClickListener(this);
            subisu.setOnClickListener(this);
            prabhu.setOnClickListener(this);
            adsl_unlimited.setOnClickListener(this);
            dishome.setOnClickListener(this);
            scroll.setVisibility(View.VISIBLE);
//            enter.setVisibility(View.GONE);
            id = view.findViewById(R.id.id);
            phone_number.addTextChangedListener(new TextWatcher() {

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (phone_number.getText().toString().matches("^0")) {
                        phone_number.setError("Enter Valid Number");
                    }
                    if (phone_number.getText().toString().length() > 10) {
                        phone_number.setError("Enter Valid Number");
                    }
                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    if (arg0.length() == 10) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(phone_number.getWindowToken(), 0);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

            });

            View vieww = getActivity().getCurrentFocus();
            if (vieww != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isvisible = true;
        tv = ((InternetPayActivity) getActivity()).isTv();
    }

    @Override
    public void onPause() {
        super.onPause();
        isvisible = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isvisible = false;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tv = ((InternetPayActivity) getActivity()).isTv();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sobisu:
                if (allowBonus == true) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("byte_code", "301");
                    editor.putString("text", "Subisu internet bill pay");
                    editor.putString("provider_name", "sobisu");
                    editor.commit();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new SubishuFragment()).addToBackStack("subisu")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Subisu Bill Payment");
                } else {
                    custumdialogfailure();
                }
                break;

            case R.id.brodlink:
                if (allowBonus == true) {
                    SharedPreferences.Editor editor1 = sharedPreferences.edit();
                    editor1.putString("byte_code", "613");
                    editor1.putString("text", "Broadlink bill pay");
                    editor1.putString("provider_name", "brodlink");
                    editor1.commit();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new InternetEnterAmountFragment()).addToBackStack("Broadlink")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Broadlink Bill Payment");
                } else {
                    custumdialogfailure();
                }

                break;
            case R.id.adsl_unlimited:
                if (allowBonus == true) {
                    SharedPreferences.Editor editor2 = sharedPreferences.edit();
                    editor2.putString("byte_code", "304");
                    editor2.putString("text", "ADSL unlimited bill pay");
                    editor2.putString("provider_name", "adsl");
                    editor2.commit();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new InternetEnterAmountFragment()).addToBackStack("ADSL")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("ADSL Bill Payment");
                } else {
                    custumdialogfailure();
                }

                break;
            case R.id.adsl_volume:
                if (allowBonus == true) {
                    SharedPreferences.Editor editor3 = sharedPreferences.edit();
                    editor3.putString("byte_code", "305");
                    editor3.putString("text", "ASDL volume based bill pay");
                    editor3.putString("provider_name", "adsl");
                    editor3.commit();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new InternetEnterAmountFragment()).addToBackStack("ADSL")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("ADSL Bill Payment");

                } else {
                    custumdialogfailure();
                }

                break;
            case R.id.net_tv_epin:
                if (allowBonus == true) {
                    SharedPreferences.Editor editor4 = sharedPreferences.edit();
                    editor4.putString("byte_code", "616");
                    editor4.putString("text", "Net TV E-Pin");
                    editor4.putString("provider_name", "net_tv");
                    editor4.commit();

                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new InternetEnterAmountFragment()).addToBackStack("Net")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Net TV Bill Payment");


                } else {
                    custumdialogfailure();
                }


                break;
            case R.id.dishome_epin:
                if (allowBonus == true) {
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putString("byte_code", "618");
//                    editor.putString("text", "DishHome E-Pin");
//                    editor.putString("provider_name", "dishhome");
//                    editor.commit();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFromDishHome", true);
                    Fragment fragment = new SelectPaymentTypeFragment();
                    fragment.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, fragment).addToBackStack("DishHome")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("DishHome Bill Payment");

                } else {
                    custumdialogfailure();
                }

                break;
            case R.id.merotv_epin:
                if (allowBonus == true) {
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putString("byte_code", "634");
//                    editor.putString("text", "Mero TV E-Pin");
//                    editor.putString("provider_name", "merotv");
//                    editor.commit();
                    Bundle bundle1 = new Bundle();
                    bundle1.putBoolean("isFromMeroTv", true);
                    Fragment fragment1 = new SelectPaymentTypeFragment();
                    fragment1.setArguments(bundle1);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, fragment1).addToBackStack("Mero")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Mero TV Bill Payment");

                } else {
                    custumdialogfailure();
                }
                break;

            case R.id.dishome:
                if (allowBonus == true) {
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putString("byte_code", "306");
//                    editor.putString("text", "DishHome bill pay");
//                    editor.putString("provider_name", "dishhome");
//                    editor.commit();
                    bundle = new Bundle();
                    bundle.putBoolean("isFromDishHome", true);
                    fragment = new SelectPaymentTypeFragment();
                    fragment.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, fragment).addToBackStack("DishHome")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("DishHome Bill Payment");
                } else {
                    custumdialogfailure();
                }
                break;
            case R.id.simTv:
                if (allowBonus == true) {
                    SharedPreferences.Editor editor5 = sharedPreferences.edit();
                    editor5.putString("byte_code", "307");
                    editor5.putString("text", "Sim TV bill pay");
                    editor5.putString("provider_name", "SimTv");
                    editor5.commit();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new InternetEnterAmountFragment()).addToBackStack("Sim")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Sim TV Bill Payment");
                } else {
                    custumdialogfailure();
                }
                break;
            case R.id.merotv_topup:
                if (allowBonus == true) {
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putString("byte_code", "208");
//                    editor.putString("text", "Mero TV bill pay");
//                    editor.putString("provider_name", "MeroTv");
//                    editor.commit();
                    Bundle bundle2 = new Bundle();
                    bundle2.putBoolean("isFromMeroTv", true);
                    Fragment fragment2 = new SelectPaymentTypeFragment();
                    fragment2.setArguments(bundle2);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, fragment2).addToBackStack("Mero")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Mero TV Bill Payment");
                } else {
                    custumdialogfailure();
                }
                break;
            case R.id.wordLink:
                if (allowBonus == true) {
                    WordLinkFragment wordLinkFragment = new WordLinkFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("WorldLink", "WorldLink");
                    wordLinkFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, wordLinkFragment)
                            .addToBackStack("WorldLink")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("WorldLink Bill Payment");
                } else {
                    custumdialogfailure();
                }
                break;
            case R.id.vianet:

                if (allowBonus == true) {

                    SharedPreferences.Editor editor6 = sharedPreferences.edit();
                    editor6.putString("byte_code", "302");
                    editor6.putString("text", "Vianet internet bill pay");
                    editor6.putString("provider_name", "vianet");
                    editor6.commit();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new ViaNetFragment()).addToBackStack("Vianet")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Vianet Bill Payment");

                } else {
                    custumdialogfailure();
                }

                break;
            case R.id.websurfer:
                if (allowBonus == true) {
                    Bundle bundle = new Bundle();
                    bundle.putString("WebSurfer", "WebSurfer");
                    WordLinkFragment wordLinkFragment = new WordLinkFragment();
                    wordLinkFragment.setArguments(bundle);
                    assert getFragmentManager() != null;
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, wordLinkFragment)
                            .addToBackStack("WebSurfer")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("WebSurfer Bill Payment");
                } else {
                    custumdialogfailure();
                }
                break;
            case R.id.arrownet:
                if (allowBonus == true) {
                    Bundle bundle = new Bundle();
                    bundle.putString("Arrownet", "Arrownet");
                    WordLinkFragment wordLinkFragment = new WordLinkFragment();
                    wordLinkFragment.setArguments(bundle);
                    assert getFragmentManager() != null;
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, wordLinkFragment)
                            .addToBackStack("Arrownet")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Arrownet Bill Payment");
                } else {
                    custumdialogfailure();
                }
                break;
            case R.id.prabhu:
                if (allowBonus) {
                  /*  Intent enternt_bill = new Intent(getActivity(), ParbhuTvActivity.class);
                    enternt_bill.putExtra("isFromPrabhuTv", true);
                    startActivity(enternt_bill);*/
                    getFragmentManager().beginTransaction()
                            .replace(R.id.internet_fragment, new ParbhuTvFragment()).addToBackStack("PrabhuTv")
                            .commit();
                    ((InternetPayActivity) getActivity()).titleText.setText("Prabhu Tv");
                } else {
                    custumdialogfailure();
                }
                break;
        }
    }

    public void custumdialogfailure(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_custom_dialog_bill_payment_feature_unavailable, null);
        final TextView dialog_balance = v.findViewById(R.id.content_text);
        final TextView sure = v.findViewById(R.id.sure);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public void showkeyword() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(enter.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }


    public void hideSoftkey(LinearLayout editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
    }


    @Override
    public boolean checkVisible() {
        return false;
    }

    private void setCashback(String id) {
        String data;
        try {
            JSONObject jsonObject = new JSONObject(GeneralPref.getCashback());
            Log.d("check", "" + jsonObject.optString("616"));
            if (id.equals("306")) {
                if (jsonObject.optString("306").length() > 0) {
                    dis_dishome = jsonObject.optString("306");
                    discountRate_dishome.setText(dis_dishome);
                } else {
                    cashback_dishome.setVisibility(View.GONE);
                }

            } else if (id.equals("301")) {
                if (jsonObject.optString("301").length() > 0) {
                    dis_subisu = jsonObject.optString("301");
                    discountRate_subisu.setText(dis_subisu);
                } else {
                    cashback_subisu.setVisibility(View.GONE);
                }

            } else if (id.equals("302")) {
                if (jsonObject.optString("302").length() > 0) {
                    dis_vianet = jsonObject.optString("302");
                    discountRate_vianet.setText(dis_vianet);
                } else {
                    cashback_vianet.setVisibility(View.GONE);
                }

            } else if (id.equals("208")) {
                if (jsonObject.optString("208").length() > 0) {
                    dis_merotv_topup = jsonObject.optString("208");
                    discountRate_merotv_topup.setText(dis_merotv_topup);
                } else {
                    cashback_merotv_topup.setVisibility(View.GONE);
                }

            } else if (id.equals("307")) {
                if (jsonObject.optString("307").length() > 0) {
                    dis_sim_tv = jsonObject.optString("307");
                    discountRate_sim_tv.setText(dis_sim_tv);
                } else {
                    cashback_sim_tv.setVisibility(View.GONE);
                }

            } else if (id.equals("634")) {
                if (jsonObject.optString("634").length() > 0) {
                    dis_meroTv = jsonObject.optString("634");
                    discountRate_meroTv.setText(dis_meroTv);
                } else {
                    cashback_meroTv.setVisibility(View.GONE);
                }

            } else if (id.equals("618")) {
                if (jsonObject.optString("618").length() > 0) {
                    dis_dishome_epin = jsonObject.optString("618");
                    discountRate_dishome_epin.setText(dis_dishome_epin);
                } else {
                    cashback_dishome_epin.setVisibility(View.GONE);
                }

            } else if (id.equals("616")) {
                if (jsonObject.optString("616").length() > 0) {
                    dis_net_tv_epin = jsonObject.optString("616");
                    discountRate_net_tv_epin.setText(dis_net_tv_epin);
                } else {
                    cashback_net_tv_epin.setVisibility(View.GONE);
                }

            } else if (id.equals("305")) {
                if (jsonObject.optString("305").length() > 0) {
                    dis_adsl_volume = jsonObject.optString("305");
                    discountRate_adsl_volume.setText(dis_adsl_volume);
                } else {
                    cashback_adsl_volume.setVisibility(View.GONE);
                }

            } else if (id.equals("304")) {
                if (jsonObject.optString("304").length() > 0) {
                    dis_adsl_unlimited = jsonObject.optString("304");
                    discountRate_adsl_unlimited.setText(dis_adsl_unlimited);
                } else {
                    cashback_adsl_unlimited.setVisibility(View.GONE);
                }

            } else if (id.equals("613")) {
                if (jsonObject.optString("613").length() > 0) {
                    dis_brodlink = jsonObject.optString("613");
                    discountRate_brodlink.setText(dis_brodlink);
                } else {
                    cashback_brodlink.setVisibility(View.GONE);
                }

            } else if (id.equals("303")) {
                if (jsonObject.optString("303").length() > 0) {
                    dis_wordLink = jsonObject.optString("303");
                    discountRate_wordLink.setText(dis_wordLink);
                } else {
                    cashback_wordLink.setVisibility(View.GONE);
                }
            } else if (id.equals("314")) {
                if (jsonObject.optString("314").length() > 0) {
                    dis_websurfer = jsonObject.optString("314");
                    discountRate_websurfer.setText(dis_websurfer);
                } else {
                    cashback_websurfer.setVisibility(View.GONE);
                }
            } else if (id.equals("315")) {
                if (jsonObject.optString("315").length() > 0) {
                    dis_arrownet = jsonObject.optString("315");
                    discountRate_arrownet.setText(dis_arrownet);
                } else {
                    cashback_arrownet.setVisibility(View.GONE);
                }

            } else if (id.equals("310")) {
                if (jsonObject.optString("310").length() > 0) {
                    dis_prabhu = jsonObject.optString("310");
                    cashBackPrabhuPay.setText(dis_prabhu);
                } else {
                    cashBackPrabhuPay.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class Checkbonusflag extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", GeneralPref.getAppId());
                jsonObject.put("id", GeneralPref.getCustId());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                Log.d("dinesh", "doInBackground: " + jsonObject);
                Log.d("dinesh", "doInBackground: " + new NetworkAPI().sendHTTPData(Constants.CHECKBONUS, jsonObject));
                return new NetworkAPI().sendHTTPData(Constants.CHECKBONUS, jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            Log.d("bonus", "" + s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject jsonObject1 = data.getJSONObject(0);
                    boolean AllowBonus = jsonObject1.getBoolean("AllowBonus");
                    GeneralPref.setAllowBonus(AllowBonus);
//                    new CashBackAPI(new DialogInterface() {
//                        @Override
//                        public void showDialog() {
//                            qPayProgressDialog.show();
//                        }
//
//                        @Override
//                        public void hideDialog() {
//                            qPayProgressDialog.dismiss();
//                        }
//                    },InternetPayActivity.this).execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

