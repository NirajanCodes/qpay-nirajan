package net.qpaysolutions.QPay.Billpayment.ubskhanepani.QuickPayAdapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.ubskhanepani.OnQuickPay;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

public class TopUpAdapter extends RecyclerView.Adapter<TopUpAdapter.ViewHolder> {
    private Object[] strings;
    private String[] fetchedData, regionId, custId, splited, byte_code;
    private Object[] data = null;
    OnQuickPay onQuickPay;

    public TopUpAdapter(String[] strings, OnQuickPay onQuickPay) {
        this.strings = strings;
        this.onQuickPay = onQuickPay;
        data = strings;
        if (data != null) {
            fetchedData = new String[data.length];
            regionId = new String[data.length];
            custId = new String[data.length];
            for (int i = 0; i < data.length; i++) {
                try {
                    fetchedData[i] = data[i].toString();
                    splited = fetchedData[i].split(",");
                    regionId[i] = splited[0];
                    custId[i] = splited[1];
                  //  byte_code[i] = splited[2];
                    Log.d(Utility.TAG, " data : " + fetchedData[i]);
                } catch (Exception e) {

                }
            }
            Log.d(Utility.TAG, " data : " + data);
            Log.d(Utility.TAG, " data : " + data.length);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quick_nea, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.customerId.setText(custId[position]+","+regionId[position]);
        holder.pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onQuickPay.onQuickPay(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView heading, customerId, region, pay;

        public ViewHolder(View itemView) {
            super(itemView);
            heading = itemView.findViewById(R.id.heading);
            customerId = itemView.findViewById(R.id.customer_id);
            region = itemView.findViewById(R.id.reagion);
            pay = itemView.findViewById(R.id.pay);

        }
    }
}
