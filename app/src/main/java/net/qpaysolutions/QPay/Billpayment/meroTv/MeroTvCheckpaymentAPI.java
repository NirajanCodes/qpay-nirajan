package net.qpaysolutions.QPay.Billpayment.meroTv;

import android.content.Context;
import android.os.AsyncTask;

import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 5/13/18.
 */

public class MeroTvCheckpaymentAPI extends AsyncTask<String,String,String> {
    DialogInterface dialogInterface;
    Context context;
    String id;
    MeroTvFragment tvFragment;

    public MeroTvCheckpaymentAPI(DialogInterface dialogInterface, Context context,String id, MeroTvFragment tvFragment) {
        this.dialogInterface = dialogInterface;
        this.context = context;
        this.id=id;
        this.tvFragment=tvFragment;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id",GeneralPref.getCustId());
            jsonObject.put("billPayCode","208");
            jsonObject.put("opt1",id);
            jsonObject.put("opt2","");
            jsonObject.put("opt3","");
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            return NetworkAPI.sendHTTPData(Constants.CHECKUBS_PAYMENT,jsonObject);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialogInterface.hideDialog();
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            String message="";
            if(jsonObject.getString("message")!=null) {
                 message= jsonObject.getString("message");
            }else{
                message="Failed Get User Data";
            }
            if (status.equals("00")){
                JSONArray data = jsonObject.getJSONArray("data");
                JSONObject object=data.getJSONObject(0);
                if (object.getString("RespCode").equals("000")){
                    tvFragment.setValues(object);
                }else {
                    new Utility().showSnackbar(tvFragment.getView(),message);
                }
            }else if (status.equals("99")){
                new Utility().showSnackbar(tvFragment.getView(),message);
            }
            else {
                new Utility().showSnackbar(tvFragment.getView(),jsonObject.getString("message"));
            }

        }catch (Exception e){
            e.printStackTrace();
            new Utility().showSnackbar(tvFragment.getView(),"Failed Get Data");
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialogInterface.showDialog();
    }
}
