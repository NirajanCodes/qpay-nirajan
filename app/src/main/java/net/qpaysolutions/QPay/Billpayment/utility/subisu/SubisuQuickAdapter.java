package net.qpaysolutions.QPay.Billpayment.utility.subisu;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class SubisuQuickAdapter extends RecyclerView.Adapter<SubisuQuickAdapter.subisuViewHolder> implements View.OnClickListener {
    private String jsonString;
    private JSONObject jsondata;
    JSONArray jsonArray;
    String[] string;
    SubisuQuickInterface subisuQuickInterface;
    int position;

    public SubisuQuickAdapter(String jsonString, SubisuQuickInterface subisuQuickInterface) {

        this.jsonString = jsonString;
        this.subisuQuickInterface = subisuQuickInterface;
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            jsonArray = jsonObject.getJSONArray("data");
            string = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                jsondata = jsonArray.getJSONObject(i);
                string[i] = jsondata.getString("customerId");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public subisuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new subisuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quick_nea, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull subisuViewHolder holder, int position) {
        holder.customerId.setText(string[position]);
        holder.pay.setOnClickListener(this);
        this.position = position;
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @Override
    public void onClick(View v) {
        subisuQuickInterface.subisuQuick(position);
    }

    public class subisuViewHolder extends RecyclerView.ViewHolder {
        TextView customerId,pay;

        public subisuViewHolder(View itemView) {
            super(itemView);
            customerId = itemView.findViewById(R.id.customer_id);
            pay=itemView.findViewById(R.id.pay);
        }
    }
}
