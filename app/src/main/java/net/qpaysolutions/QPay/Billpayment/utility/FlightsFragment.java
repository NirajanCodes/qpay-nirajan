package net.qpaysolutions.QPay.Billpayment.utility;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Billpayment.utility.domestic.DomesticFlightActivity;

/**
 * Created by deadlydragger on 5/7/17.
 */

public class FlightsFragment extends Fragment implements View.OnClickListener {
    LinearLayout domestic;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.flights,container,false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((UtilitybillPay) getActivity()).setToolbar_text("Flight Ticket");
        domestic=(LinearLayout)view.findViewById(R.id.domestic);
        domestic.setOnClickListener(this);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.domestic:
                startActivity(new Intent(getActivity(), DomesticFlightActivity.class));
                break;
        }
    }
}
