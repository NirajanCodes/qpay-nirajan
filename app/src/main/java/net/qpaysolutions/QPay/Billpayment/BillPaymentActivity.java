package net.qpaysolutions.QPay.Billpayment;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import net.qpaysolutions.QPay.CASHBACK.CashBackAPI;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.InternetPayActivity;
import net.qpaysolutions.QPay.Billpayment.phonepay.PhonePayActivity;
import net.qpaysolutions.QPay.Billpayment.utility.UtilitybillPay;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.NearByTaxi.NearTaxiActivity;
import net.qpaysolutions.QPay.Resturant.ResturantActivity;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

//import com.roughike.bottombar.BottomBar;

/**
 * Created by deadlydragger on 6/26/16.
 */
public class BillPaymentActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout phone_bill, internate_bill, entertaint_bill, utility_bill;
    Toolbar toolbar;
    TextView textView;
    TextView balance_amount, toolbar_text;
    String blc_amount;
    public static final String MY_PREFS_NAME = "Mybalance";
    QPayProgressDialog qPayProgressDialog;
    private  AHBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            setContentView(R.layout.bill_payment_main);
            qPayProgressDialog= new QPayProgressDialog(BillPaymentActivity.this);

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar_text = (TextView) toolbar.findViewById(R.id.title);
            toolbar_text.setText("Bill Payment");

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            balance_amount = (TextView) findViewById(R.id.balance);
            phone_bill = (LinearLayout) findViewById(R.id.phone_bill);
            internate_bill = (LinearLayout) findViewById(R.id.internate_bill);
            entertaint_bill = (LinearLayout) findViewById(R.id.enternt_bill);
            utility_bill = (LinearLayout) findViewById(R.id.utility_bill);
            phone_bill.setOnClickListener(this);
            internate_bill.setOnClickListener(this);
            entertaint_bill.setOnClickListener(this);
            utility_bill.setOnClickListener(this);
            balance_amount.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            if (GeneralPref.getAllowBonus() == false) {
                new Checkbonusflag().execute();
            }else {
                new CashBackAPI(new DialogInterface() {
                    @Override
                    public void showDialog() {
                        qPayProgressDialog.show();
                    }

                    @Override
                    public void hideDialog() {
                        qPayProgressDialog.dismiss();
                    }
                },this).execute();
            }



            new Utility().hideSoftkey(BillPaymentActivity.this);
            buttonBar();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone_bill:
                Intent intent = new Intent(BillPaymentActivity.this, PhonePayActivity.class);
                startActivity(intent);
                break;
            case R.id.internate_bill:
                Intent internate = new Intent(BillPaymentActivity.this, InternetPayActivity.class);
                startActivity(internate);
                break;
            case R.id.enternt_bill:
                Intent enternt_bill = new Intent(BillPaymentActivity.this, EntertaintActivity.class);
                startActivity(enternt_bill);
                break;
            case R.id.utility_bill:
                Intent utility = new Intent(BillPaymentActivity.this, UtilitybillPay.class);
                startActivity(utility);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new Utility().hideSoftkey(BillPaymentActivity.this);
       /* Intent i = new Intent(BillPaymentActivity.this, MainActivity.class);
        i.putExtra("Check", 6);
        startActivity(i);*/
        BillPaymentActivity.this.finish();
    }

    public void buttonBar() {

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        AHBottomNavigationItem item = new AHBottomNavigationItem(R.string.tab_0, R.drawable.ic_action_footer_home_tab, R.color.colorPrimary);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_action_chat_tab, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_nearby_atm_tab, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_action_button_car, R.color.colorPrimary);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_action_restaurant_tab, R.color.colorPrimary);
        bottomNavigation.addItem(item);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item5);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item4);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setForceTint(false);
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setColored(true);
        try {
            if (GeneralPref.getChatCount() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getChatCount()), 1);
            }
            if (GeneralPref.getTaxi() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getTaxi()), 4);
            }
            if (GeneralPref.getAtm() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getAtm()), 3);
            }
            if (GeneralPref.getNoofRestro() > 2) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getNoofRestro()), 2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                Log.d(Utility.TAG, "tab selected" + position);
                if (position == 0) {
                    Intent history = new Intent(BillPaymentActivity.this, MainActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    BillPaymentActivity.this.startActivity(history, bndlanimation);
                } else if (position == 1) {


                    try {
                        Intent taxi = new Intent(BillPaymentActivity.this, ChatActivity.class);
                        Bundle animationtaxi = ActivityOptions.makeCustomAnimation(BillPaymentActivity.this, R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                        startActivity(taxi, animationtaxi);

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } else if (position == 2) {
                    try {
                        Intent taxi = new Intent(BillPaymentActivity.this, ResturantActivity.class);
                        Bundle animationtaxi = ActivityOptions.makeCustomAnimation(BillPaymentActivity.this, R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                        startActivity(taxi, animationtaxi);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } else if (position == 3) {
                    Intent mycards = new Intent(BillPaymentActivity.this, MainActivity.class);
                    mycards.putExtra("Check", 7);
                    Bundle bundleanim = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    BillPaymentActivity.this.startActivity(mycards, bundleanim);

                } else if (position == 4) {
                    Intent history = new Intent(BillPaymentActivity.this, NearTaxiActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(BillPaymentActivity.this, R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    startActivity(history, bndlanimation);


                }
                return true;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    public class Checkbonusflag extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", GeneralPref.getAppId());
                jsonObject.put("id", GeneralPref.getCustId());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                Log.d("dinesh", "doInBackground: " + jsonObject);
                Log.d("dinesh", "doInBackground: " + new NetworkAPI().sendHTTPData(Constants.BASE_API + "checkbonusflag", jsonObject));
                return new NetworkAPI().sendHTTPData(Constants.BASE_API + "checkbonusflag", jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject jsonObject1 = data.getJSONObject(0);
                    boolean AllowBonus = jsonObject1.getBoolean("AllowBonus");
                        GeneralPref.setAllowBonus(AllowBonus);
                    new CashBackAPI(new DialogInterface() {
                        @Override
                        public void showDialog() {
                            qPayProgressDialog.show();
                        }

                        @Override
                        public void hideDialog() {
                            qPayProgressDialog.dismiss();
                        }
                    },BillPaymentActivity.this).execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
