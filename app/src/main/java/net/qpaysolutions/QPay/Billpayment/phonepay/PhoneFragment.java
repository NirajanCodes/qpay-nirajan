package net.qpaysolutions.QPay.Billpayment.phonepay;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.GeneralPref;

public class PhoneFragment extends Fragment implements View.OnClickListener {
    LinearLayout topUp, myPhone;
    private TextView myNumber;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.phone_fragment, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myNumber = view.findViewById(R.id.my_number);
        myNumber.setText("(" + GeneralPref.getPhone() + ")");
        topUp();
//        rechargePin();
        myPhone();
    }

    private void topUp() {

        topUp = (LinearLayout) getView().findViewById(R.id.topUp);
        topUp.setOnClickListener(this);
        topUp.setVisibility(View.VISIBLE);

    }

    //    private void rechargePin() {
    //    LinearLayout rechargePin = (LinearLayout) getView().findViewById(R.id.recharge);
    //    rechargePin.setOnClickListener(this);
    //    }

    private void myPhone() {
        myPhone = (LinearLayout) getView().findViewById(R.id.myPhone);
        myPhone.setVisibility(View.VISIBLE);
        myPhone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.topUp:
               /* myPhone.setVisibility(View.GONE);
                topUp.setVisibility(View.GONE);*/
                //topUp.setBackgroundColor(getResources().getColor(R.color.background_color_all));
//                if (GeneralPref.getAllowBonus() == true) {
                getFragmentManager().beginTransaction()
//                            .replace(R.id.fragment, new PhoneTopUpFragment())
                        .add(R.id.top_of_all, new PhoneTopUpFragment())
                        .addToBackStack("Phone Fragment")
                        .commit();
               /* } else {
                    ((PhonePayActivity) getActivity()).failureDialog();
                }*/

                break;
//            case R.id.recharge:
//                if (GeneralPref.getAllowBonus() == true) {
//                    getFragmentManager().beginTransaction()
//                            .replace(R.id.fragment, new RechargeFragment())
//                            .commit();
//                }else {
//                    ((PhonePayActivity)getActivity()).failureDialog();
//                }
//
//                break;
            case R.id.myPhone:
              /*  myPhone.setVisibility(View.GONE);
                topUp.setVisibility(View.GONE);*/
                // topUp.setBackgroundColor(getResources().getColor(R.color.background_color_all));
//                if (GeneralPref.getAllowBonus() == true) {
                getFragmentManager().beginTransaction()
                        .add(R.id.top_of_all, new MyPhoneFragment())
                        .addToBackStack("Phone Fragment")
                        .commit();
              /*  } else {
                    ((PhonePayActivity) getActivity()).failureDialog();
                }*/
                break;

        }
    }
}
