package net.qpaysolutions.QPay.Billpayment;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Card.CardPOJO;
import net.qpaysolutions.QPay.R;

import java.util.List;

/**
 * Created by QPay on 2/7/2019.
 */

public class BillPayListSheetAdapter extends RecyclerView.Adapter<BillPayListSheetAdapter.BillPayListViewholder> {
    private Context context;
    private List<CardPOJO> cardPOJOList;
    private OnCardClickListener onCardClickListener;
    private GestureDetector mDetector;

    public BillPayListSheetAdapter(Context context, List<CardPOJO> cardPOJOList, OnCardClickListener onCardClickListener) {
        this.context = context;
        this.cardPOJOList = cardPOJOList;
        this.onCardClickListener = onCardClickListener;
    }


    @NonNull
    @Override
    public BillPayListViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BillPayListViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_billpaylist_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BillPayListViewholder holder, final int position) {
        try {
            final CardPOJO cardPOJO = cardPOJOList.get(position);
            if (cardPOJO.getCardName().equalsIgnoreCase("QPay")) {
                Picasso.get()

                        .load(R.drawable.upi_card_sample)
                        .placeholder(R.drawable.upi_card_sample)
                        .error(R.drawable.upi_card_sample)
                        .noFade()
                        .fit()
                        .into(holder.imageView);
                holder.cardNumber.setText(" XXXX - XXXX - XXXX - " + cardPOJO.getLast4DigitCardNumber());
                holder.amount.setText(cardPOJO.getCardBalance() != null ? " Wallet NPR " + cardPOJO.getCardBalance() : " Wallet NPR 0.0");
            } else {
                Picasso.get()

                        .load(cardPOJO.getImgId())
                        .placeholder(R.drawable.ic_sct_card)
                        .error(R.drawable.ic_sct_card)
                        .noFade()
                        .into(holder.imageView);
                holder.amount.setText(cardPOJO.getCardBalance() != null ? "Last Known NPR " + cardPOJO.getCardBalance() : "");
                holder.expiryDate.setText(cardPOJO.getExpiryDate() != null ? "Exp " + cardPOJO.getExpiryDate()
                        .substring(2) + "/" + cardPOJO.getExpiryDate().substring(0, 2) : "");
                holder.cardNumber.setText(" XXXX - XXXX - XXXX - " + cardPOJO.getLast4DigitCardNumber());
            }

            holder.creditCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCardClickListener.onCardClick(cardPOJO, position);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return cardPOJOList.size();
    }

//    @Override
//    public View getView(final int position, final View contentView, ViewGroup parent) {
//        RelativeLayout mainLayout = (contentView.findViewById(R.id.mainLayout));
//        ImageView v = (contentView.findViewById(R.id.getImage));
//        TextView cardNumber = (contentView.findViewById(R.id.cardNumberTextView));
//        TextView amount = (contentView.findViewById(R.id.amountTextView));
//        TextView expiryDate = (contentView.findViewById(R.id.expiryDateTextView));
//
//
//        try {
//            final CardPOJO cardPOJO = cardPOJOList.get(position);
//            LinearLayout verifyBttn = addManageCardFragment.getActivity().findViewById(R.id.verify_card_layout);
//            TextView verifyTextview = addManageCardFragment.getActivity().findViewById(R.id.verify_textview);
//
//
//            addManageCardFragment.setVisibleGone();
//            if (position == cardPOJOList.size() - 1) {
//                addManageCardFragment.setVisibleGone();
//
//            } else {
//                addManageCardFragment.setVisible();
//                String flag_db = cardPOJOList.get(position + 1).getActFlag().trim();
////                if (flag_db.equals("R")) {
////                    verifyTextview.setText("Verify Card");
////                }
////                if (flag_db.equals("V")) {
////                    verifyTextview.setText("Balance Inq");
////                }
//
//
//            }
//
//
//            System.out.println("CardName:: " + cardPOJO.getCardName() + " position:: " + position);
//            if (cardPOJO.getCardName().equalsIgnoreCase("QPay")) {
//                Picasso.with(context)
//                        .load(R.drawable.upi_card_sample)
//                        .placeholder(R.drawable.upi_card_sample)
//                        .error(R.drawable.upi_card_sample)
//                        .noFade()
//                        .fit()
//                        .into(v);
//                cardNumber.setText(" XXXX - XXXX - XXXX - " + cardPOJO.getLast4DigitCardNumber());
//                amount.setText(cardPOJO.getCardBalance() != null ? " Wallet NPR " + cardPOJO.getCardBalance() : " Wallet NPR 0.0");
//            } else {
//                Picasso.with(context)
//                        .load(cardPOJO.getImgId())
//                        .placeholder(R.drawable.ic_sct_card)
//                        .error(R.drawable.ic_sct_card)
//                        .noFade()
//                        .into(v);
//                amount.setText(cardPOJO.getCardBalance() != null ? "Last Known NPR " + cardPOJO.getCardBalance() : "");
//                expiryDate.setText(cardPOJO.getExpiryDate() != null ? "Exp " + cardPOJO.getExpiryDate()
//                        .substring(2) + "/" + cardPOJO.getExpiryDate().substring(0, 2) : "");
//
//                cardNumber.setText(" XXXX - XXXX - XXXX - " + cardPOJO.getLast4DigitCardNumber());
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return contentView;
//    }

    class BillPayListViewholder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView cardNumber;
        private TextView amount;
        private TextView expiryDate;
        private CardView creditCardView;

        public BillPayListViewholder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.getImage);
            cardNumber = itemView.findViewById(R.id.cardNumberTextView);
            amount = itemView.findViewById(R.id.amountTextView);
            expiryDate = itemView.findViewById(R.id.expiryDateTextView);
            creditCardView = itemView.findViewById(R.id.creditCardView);
        }
    }


    public interface OnCardClickListener {
        void onCardClick(CardPOJO cardPOJO, int position);
    }
}
