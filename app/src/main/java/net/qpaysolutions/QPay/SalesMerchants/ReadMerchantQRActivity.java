package net.qpaysolutions.QPay.SalesMerchants;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.Result;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressdialogwhite;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ReadMerchantQRActivity extends AppCompatActivity {

    private QPayProgressdialogwhite pDialog;
    private String merch_name;
    private String status;
    private String decryptCust_id;
    private SurfaceView barcodeView;
    private LinearLayout black_view;
    private RelativeLayout LayoutScan;
    private Toolbar toolbar;

    private static final int CAMERA_REQUEST_CODE = 101;
    private boolean scan = true;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private MediaPlayer mediaPlayer;
    private boolean playMusic = false;
    private CodeScanner mCodeScanner;
    private boolean permissionEnable = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.scan_pay);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
            toolbar_text.setText("Scan Code");
            TextView titleText = (TextView) findViewById(R.id.titleText);
            titleText.setText(getString(R.string.scan_to_pay));
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
            barcodeView = (SurfaceView) findViewById(R.id.barcode_scanner);
            black_view = (LinearLayout) findViewById(R.id.black_view);
            LayoutScan = findViewById(R.id.LayoutScan);
            mediaPlayer = MediaPlayer.create(ReadMerchantQRActivity.this, R.raw.beep);

            barcodeDetector =
                    new BarcodeDetector.Builder(ReadMerchantQRActivity.this)
                            .setBarcodeFormats(Barcode.QR_CODE)
                            .build();

            cameraSource = new CameraSource
                    .Builder(ReadMerchantQRActivity.this, barcodeDetector)
                    .setRequestedPreviewSize(640, 480)
                    .setAutoFocusEnabled(true)

                    .build();

            int permission = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                makeRequestCamera();
            } else {
                permissionEnable = true;
                playMusic = true;

                qrCodeReading();
                CodeScannerView scannerView = findViewById(R.id.scanner_view);
                mCodeScanner = new CodeScanner(this, scannerView);
                mCodeScanner.setDecodeCallback(result -> runOnUiThread(() -> {
                    if (result.getText() != null) {
                        // Use the post method of the TextView
                        black_view.post(() -> {
                            if (playMusic) {
                                playMusic = false;
                                mediaPlayer.start();
                                LayoutScan.setVisibility(View.GONE);
                                black_view.setVisibility(View.VISIBLE);
                                decryptCust_id = result.getText();
                                if (decryptCust_id != null && decryptCust_id.length() == 24 && !decryptCust_id.equals("QPay-Payment Simplified") && !decryptCust_id.equals(GeneralPref.getCustId())) {
                                    if (scan == true) {
                                        scan = false;
                                        GeneralPref.merchantType("QPAY");
                                        new Get_Trxn(decryptCust_id).execute();
                                    }
                                    Log.d(Utility.TAG, "result from qr : " + decryptCust_id);
                                } else if (decryptCust_id.equals(GeneralPref.getCustId())) {
                                    custumdialogfailure("Not Found!", "You seem to have scanned your own QRcode. Please scan proper QRcode.");
                                } else if (decryptCust_id.equals("QPay-Payment Simplified")) {
                                    custumdialogfailure("Invalid QRcode!", "You seem to have scanned QPay logo. Please scan proper QRcode.");
                                } else if (decryptCust_id.length() > 24) {
                                    GeneralPref.merchantType("UPI");
                                    GeneralPref.setUPIMerchant(decryptCust_id);
                                    Intent intent = new Intent(ReadMerchantQRActivity.this, PaymentMerchantProceed.class);
                                    intent.putExtra("upiMerchant", decryptCust_id);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    custumdialogfailure("Invalid QRcode!", "You seem to have scanned non-QPay QRcode. Please scan proper QRcode.");
                                }
                            }
                        });
                    }
                }));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void makeRequestCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ReadMerchantQRActivity.this.requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    1);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length == 0
                        || grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    new Handler().post(() -> qrCodeReading());

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give camera permission.", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    private void qrCodeReading() {

        barcodeView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(ReadMerchantQRActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(barcodeView.getHolder());
                    }

                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(final Detector.Detections<Barcode> detections) {
                try {
                    final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                    if (barcodes.size() != 0) {
                        // Use the post method of the TextView
                        black_view.post(() -> {
                            if (playMusic) {
                                playMusic = false;
                                mediaPlayer.start();
                                LayoutScan.setVisibility(View.GONE);
                                black_view.setVisibility(View.VISIBLE);
                                decryptCust_id = barcodes.valueAt(0).displayValue.toString();
                                if (decryptCust_id != null && decryptCust_id.length() == 24 && !decryptCust_id.equals("QPay-Payment Simplified") && !decryptCust_id.equals(GeneralPref.getCustId())) {
                                    if (scan == true) {
                                        scan = false;
                                        GeneralPref.merchantType("QPAY");
                                        new Get_Trxn(decryptCust_id).execute();
                                    }
                                    Log.d(Utility.TAG, "result from qr : " + decryptCust_id);
                                } else if (decryptCust_id.equals(GeneralPref.getCustId())) {
                                    custumdialogfailure("Not Found!", "You seem to have scanned your own QRcode. Please scan proper QRcode.");
                                } else if (decryptCust_id.equals("QPay-Payment Simplified")) {
                                    custumdialogfailure("Invalid QRcode!", "You seem to have scanned QPay logo. Please scan proper QRcode.");
                                } else if (decryptCust_id.length() > 24) {
                                    GeneralPref.merchantType("UPI");
                                    GeneralPref.setUPIMerchant(decryptCust_id);
                                    Intent intent = new Intent(ReadMerchantQRActivity.this, PaymentMerchantProceed.class);
                                    intent.putExtra("upiMerchant", decryptCust_id);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    custumdialogfailure("Invalid QRcode!", "You seem to have scanned non-QPay QRcode. Please scan proper QRcode.");
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class Get_Trxn extends AsyncTask<String, String, String> {
        String desc_key;

        public Get_Trxn(String desc_key) {
            this.desc_key = desc_key;
        }

        @Override
        protected String doInBackground(String... uri) {
            String result = null;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("appId", GeneralPref.getAppId());
                jsonObject.put("termId", desc_key);
                jsonObject.put("id", GeneralPref.getCustId());
                jsonObject.put("lat", ((CustomerApplication) ReadMerchantQRActivity.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) ReadMerchantQRActivity.this.getApplication()).getLng());
                Log.d(Utility.TAG, "post to merchant : " + jsonObject);
                NetworkAPI networkAPI = new NetworkAPI();
                result = networkAPI.sendHTTPData(Constants.BASE_API + "merchantinfoforsales", jsonObject);
                Log.d("dinesh", "doInBackground: " + result);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressdialogwhite(ReadMerchantQRActivity.this);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            scan = true;

            try {

                pDialog.dismiss();
                pDialog.hide();
                JSONObject jsonObject = null;
                if (s != null) {
                    try {
                        jsonObject = new JSONObject(s);
                        JSONArray array = jsonObject.getJSONArray("data");
                        JSONObject jsonObject1 = array.getJSONObject(0);
                        merch_name = jsonObject1.getString("MerchName");
                        status = jsonObject.getString("status");
                        JSONArray Denom = jsonObject1.getJSONArray("Denom");
                        boolean HasDenom = jsonObject1.getBoolean("HasDenom");
                        String Address = jsonObject1.getString("Address");
                        String imgURL = jsonObject1.getString("ImageUrl");
                        if (status != null && !status.equals("null") && !status.isEmpty() && status.equals("00")) {
                            Intent intent = new Intent(ReadMerchantQRActivity.this, PaymentMerchantProceed.class);
                            intent.putExtra("foren_id_scan", desc_key);
                            intent.putExtra("name", merch_name);
                            intent.putExtra("ImageUrl", imgURL);
                            intent.putExtra("Address", Address);
                            intent.putExtra("HasDenom", HasDenom);

                            if (HasDenom) {
                                intent.putExtra("Denom", Denom.toString());
                            }

                            startActivity(intent);

                        } else {
                            custumdialogfailure("Invalid QRcode!", "You seem to have scanned invalid QRcode. Please scan proper QRcode.");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            custumdialogfailure("Invalid QRcode!", "You seem to have scanned invalid QRcode. Please scan proper QRcode.");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                } else {

                    custumdialogfailure("Invalid QRcode!", "You seem to have scanned invalid QRcode. Please scan proper QRcode.");

                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    custumdialogfailure("Invalid QRcode!", "You seem to have scanned invalid QRcode. Please scan proper QRcode.");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (permissionEnable) {
            mCodeScanner.startPreview();
        }
    }

    @Override
    protected void onPause() {
        if (permissionEnable) {
            mCodeScanner.releaseResources();

        }
        super.onPause();
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ReadMerchantQRActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraSource.release();
        barcodeDetector.release();
    }
}
