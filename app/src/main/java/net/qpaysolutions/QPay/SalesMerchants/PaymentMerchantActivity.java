package net.qpaysolutions.QPay.SalesMerchants;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Billpayment.BillpayCardAdapter;
import net.qpaysolutions.QPay.CheckInternetConnection.NetworkInformation;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by deadlydragger on 6/5/16.
 */
public class PaymentMerchantActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private QPayCustomerDatabase sctDatabaseHelper;
    private QPayProgressDialog pDialog;
    private LinearLayout qpay_line;
    private ListView payment_listl;
    private TextView paidTo, PaidAmount;
    private QPayProgressDialog pDialogg;
    private String app_id;
    private Toolbar toolbar;
    private NetworkChangeReceiver networkChangeReceiver;
    private boolean mIsReceiverRegistered = false;
    private TextView toolbar_text, wallet_amount_text, deposit_balance_text, avail_bal_text, hold_balance_text;
    private LinearLayout ll_available, ll_deposite, ll_hold;
    private String imgUrl, name, termId, custId;
    private double amount;
    private String TAG = "dinesh";
    private int counter = 0;
    private Handler handler = new Handler();
    private boolean isVirtualCard = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.card_profile);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar_text = (TextView) toolbar.findViewById(R.id.title);
            toolbar_text.setText("Payment Option");
            wallet_amount_text = (TextView) findViewById(R.id.wallet_amount_text);
            deposit_balance_text = (TextView) findViewById(R.id.deposit_balance_text);
            avail_bal_text = (TextView) findViewById(R.id.avail_bal_text);
            ll_available = (LinearLayout) findViewById(R.id.ll_available);
            ll_deposite = (LinearLayout) findViewById(R.id.ll_deposit);
            ll_hold = (LinearLayout) findViewById(R.id.ll_hold);
            qpay_line = (LinearLayout) findViewById(R.id.qpay_row);
            paidTo = (TextView) findViewById(R.id.paidTo);
            PaidAmount = (TextView) findViewById(R.id.paidTo_amount);
            hold_balance_text = (TextView) findViewById(R.id.hold_balance_text);
            sctDatabaseHelper = new QPayCustomerDatabase(PaymentMerchantActivity.this);
            app_id = sctDatabaseHelper.getKeyAppId();
            custId = sctDatabaseHelper.getCustomerID();
            qpay_line.setOnClickListener(this);
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                imgUrl = intent.getStringExtra("imgUrl");
                amount = intent.getDoubleExtra("amount", 0.00);
                name = intent.getStringExtra("name");
                termId = extras.getString("termId");
            }

            SQLiteDatabase db = sctDatabaseHelper.getWritableDatabase();
            payment_listl = (ListView) findViewById(R.id.payment_card_List);
            Cursor cursor;
            if (GeneralPref.getMerchantType().equalsIgnoreCase("UPI")) {
                payment_listl.setVisibility(View.VISIBLE);
                cursor = db.rawQuery(" select * from Profile where act_flag ='upi' ", null);
            } else {
                cursor = db.rawQuery(" select * from Profile where act_flag ='V' ", null);
            }

            BillpayCardAdapter cardListAdapter = new BillpayCardAdapter(this, cursor);
            payment_listl.setAdapter(cardListAdapter);
            payment_listl.setOnItemClickListener(this);

            paidTo.setText(name);
            networkChangeReceiver = new NetworkChangeReceiver();
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mIsReceiverRegistered = true;
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            checkInternetConnection();


            walletBalance();
            String amount_to_pay = new Decimalformate().decimalFormate(String.valueOf(amount));
            PaidAmount.setText("NPR " + amount_to_pay);
            calculateReward(amount_to_pay);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void calculateReward(String reward) {
        TextView reward_amount = (TextView) findViewById(R.id.reward_amount);
        double percent_hunderd = 5 * Double.valueOf(reward);
        double percent_amount = percent_hunderd / 100.0f;
        String amount_discount = new Decimalformate().decimalFormate(String.valueOf(percent_amount));
        reward_amount.setText(Html.fromHtml("You will be awarded with cash back reward of NPR " + "<b>" + amount_discount + "</b>"));

    }

    public void walletBalance() {
        wallet_amount_text.setText("NPR " + BalancePref.getAvailableAmount());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.qpay_row:
                try {
                    if (amount <= 10000) {
                        if (Double.valueOf(BalancePref.getAvailableAmount()) >= amount) {
                            confirmPaythroughQpay(amount);
                        } else {
                            custumdialogfailurecancel("Payment Failure!", "Insufficient QPay balance for this transaction. Please try with card.");
                        }

                    } else {
                        custumdialogfailurecancel("Payment Failure!", "You can't pay more than 10,000 at a time.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mIsReceiverRegistered) {
            if (networkChangeReceiver == null)
                networkChangeReceiver = new NetworkChangeReceiver();
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mIsReceiverRegistered = true;
        }
    }

    @Override
    protected void onPause() {
        if (mIsReceiverRegistered) {
            unregisterReceiver(networkChangeReceiver);
            networkChangeReceiver = null;
            mIsReceiverRegistered = false;
        }
        super.onPause();
    }

    public void confirmPaythroughQpay(double amount) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMerchantActivity.this);
        LayoutInflater inflater = PaymentMerchantActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Do you want to Pay NPR " + new Decimalformate().decimalFormate(String.valueOf(amount)) + " from your QPay Account?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isVirtualCard = false;
                new MerchantPay().execute(isVirtualCard);
                dialog.dismiss();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String last_4 = sctDatabaseHelper.getLastFourForBill(id);
        String card_token = sctDatabaseHelper.getCardType(String.valueOf(id));
        verifycardPayment(last_4, card_token, id);
    }

    public void verifycardPayment(final String last_4, final String card_token, final long id) {
        final Dialog dialog = new Dialog(PaymentMerchantActivity.this, R.style.UpdownDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        dialog.setContentView(R.layout.custum_dialog_entrcardpin);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final EditText enter_pin = (EditText) dialog.findViewById(R.id.enter_pin);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.proceed_pin_enter);
        TextView dialogcancel = (TextView) dialog.findViewById(R.id.cancel_pin_enter);
        if (enter_pin.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String pin = enter_pin.getText().toString();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(enter_pin.getWindowToken(), 0);
                    if (pin.length() == 4 && pin != null && !pin.isEmpty()) {
                        new Payment_card_purchesh(last_4, pin, card_token, id).execute();
                        dialog.dismiss();
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } else {
                        enter_pin.setError("Enter valid pin");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        dialog.show();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

    }

    private class Payment_card_purchesh extends AsyncTask<Void, Void, JSONObject> {
        String last_4, pin, card_token;
        long id;

        private Payment_card_purchesh(String last_4, String pin, String card_token, long id) {
            this.last_4 = last_4;
            this.pin = pin;
            this.card_token = card_token;
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new QPayProgressDialog(PaymentMerchantActivity.this);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            String line = null;
            JSONObject Response = null;
            try {
                NetworkAPI networkAPI = new NetworkAPI();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", app_id);
                jsonObject.put("id", custId);
                jsonObject.put("termId", termId);
                jsonObject.put("last4", last_4);
                jsonObject.put("amount", amount);
                jsonObject.put("pin", pin);
                jsonObject.put("cardToken", card_token);
                jsonObject.put("lat", ((CustomerApplication) PaymentMerchantActivity.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) PaymentMerchantActivity.this.getApplication()).getLng());
                String data = jsonObject.toString();
                Log.d(TAG, "doInBackground: " + jsonObject);
                String result = networkAPI.sendHTTPData(Constants.PURCH_PAYMENT_CARD, jsonObject);
                Log.d(TAG, "doInBackground: " + result);
                Response = new JSONObject(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            super.onPostExecute(args);

            try {
                Log.d(Utility.TAG, args.toString());
                JSONArray jsonArray = args.getJSONArray("data");
                String status = args.getString("status");


                if (status.equals("00")) {
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String crrn = jsonObject.getString("Crrn");
                    String stan = jsonObject.getString("Stan");
                    String resp_code = jsonObject.getString("RespCode");
                    if (resp_code.equals("00")) {
                        callAsynchronousTask(custId, crrn, stan, app_id);
                    } else if (resp_code.equals("CN")) {
                        sctDatabaseHelper.deletCard(id);
                        pDialog.dismiss();
                        pDialog.hide();
                        custumdialogfailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");
                    } else {
                        pDialog.dismiss();
                        pDialog.hide();
                        Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", PaymentMerchantActivity.this);
                    }
                } else if (status.equals("CN")) {
                    sctDatabaseHelper.deletCard(id);
                    pDialog.dismiss();
                    pDialog.hide();
                    custumdialogfailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");
                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", PaymentMerchantActivity.this);
                }
            } catch (Exception e) {
                try {
                    pDialog.dismiss();
                    pDialog.hide();
                    e.printStackTrace();
                    Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", PaymentMerchantActivity.this);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }

        public void callAsynchronousTask(final String cust_id, final String crr, final String stan, final String app_id) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    counter++;
                    Log.d(Utility.TAG, "counter :  " + counter);
                    new CheckPurchTransction(cust_id, crr, stan, app_id).execute();

                }
            }, Tags.TIME_SLEEP);

        }

        private class CheckPurchTransction extends AsyncTask<String, String, JSONObject> {

            String cust_id, crr, stan, app_id;

            private CheckPurchTransction(String cust_id, String crr, String stan, String app_id) {
                this.cust_id = cust_id;
                this.crr = crr;
                this.stan = stan;
                this.app_id = app_id;
            }

            @Override
            protected JSONObject doInBackground(String... params) {
                JSONObject jsonObject = new JSONObject();
                NetworkAPI networkAPI = new NetworkAPI();
                String result;
                try {
                    jsonObject.put("appId", app_id);
                    jsonObject.put("id", cust_id);
                    jsonObject.put("crrn", crr);
                    jsonObject.put("stan", stan);
                    jsonObject.put("lat", 0.00);
                    jsonObject.put("lng", 0.00);
                    Log.d(Utility.TAG, "Post merchant card : " + jsonObject.toString());
                    result = networkAPI.sendHTTPData(Constants.PURCHASE_TRSCTION_CONSUMER, jsonObject);
                    Log.d(Utility.TAG, "return data  : " + result);
                    jsonObject = new JSONObject(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return jsonObject;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(JSONObject s) {
                super.onPostExecute(s);
                try {

                    String status = s.getString("status");
                    String displayMsg = s.getString("message");
                    JSONArray jsonArray = s.getJSONArray("data");
                    String resp_code = jsonArray.getString(0);
                    Log.d(Utility.TAG, "post status : " + status + "resp code : " + resp_code);
                    if (!status.isEmpty() && !status.equals("null") && !status.equals("") && status != null && status.equals("00")) {
                        if (resp_code.equals("00")) {
                            pDialog.dismiss();
                            pDialog.hide();
                            new Ackclass(app_id, cust_id, stan, crr).execute();
                            vibration();
//                            custumdialogSucess();
                        } else if (resp_code.equals("LO")) {
                            if (counter < 10) {
                                callAsynchronousTask(cust_id, crr, stan, app_id);
                            } else {
                                pDialog.dismiss();
                                pDialog.hide();
                                custumdialogfailure("Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");
                            }
                        } else {
                            pDialog.dismiss();
                            pDialog.hide();
                            new Ackclass(app_id, cust_id, stan, crr).execute();
                            custumdialogfailure("Payment Failure!", displayMsg);
                        }
                    } else if (!status.isEmpty() && !status.equals("null") && !status.equals("") && status != null && status.equals("99")) {
                        pDialog.dismiss();
                        pDialog.hide();
                        custumdialogfailure("Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        pDialog.dismiss();
                        pDialog.hide();
                        Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", PaymentMerchantActivity.this);

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }


                }
            }
        }
    }

    public class MerchantPay extends AsyncTask<Boolean, String, String> {

        @Override
        protected String doInBackground(Boolean... params) {
            String transfer_fund_result = null;
            String available;
            String status = null;
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject.put("appId", app_id);
                jsonObject.put("id", custId);
                if (GeneralPref.getMerchantType().equalsIgnoreCase("UPI")) {
                    jsonObject.put("termId", GeneralPref.getUPIMERCHANT());
                } else {
                    jsonObject.put("termId", termId);
                }

                jsonObject.put("amount", amount);
                jsonObject.put("lat", ((CustomerApplication) PaymentMerchantActivity.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) PaymentMerchantActivity.this.getApplication()).getLng());
                jsonObject.put("isVirtualCard", params[0]);
                jsonObject.put("guid", GeneralPref.getUpiId());

                Log.d(TAG, "doInBackground: " + jsonObject);
                transfer_fund_result = networkAPI.sendHTTPData(Constants.MERCHANT_PAY_QPAY, jsonObject);
                Log.d(TAG, "api : " + Constants.MERCHANT_PAY_QPAY);
                Log.d(TAG, "doInBackground: " + transfer_fund_result);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return transfer_fund_result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                pDialogg.dismiss();
                pDialogg.hide();
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                String status = jsonObject.getString("status");
                String message = jsonObject.getString("message");

                if (status.equals("00")) {
                    JSONObject object = jsonArray.getJSONObject(0);
                    String avail_bal = object.getString("AvailBalance");
                    BalancePref.setAvailableAmount(new Decimalformate().decimalFormate(avail_bal));
                    new Ackclass(app_id, custId, object.getString("Stan"), object.getString("Crrn")).execute();
                    vibration();
                    custumdialogSucess(object.getString("Discount"));
                } else if (status.equals("CN")) {

                    custumdialogfailure("Payment Failure!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");
                } else if (status.equals("99")) {
                    custumdialogfailure("Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");
                } else if (status.equals("61")) {
                    custumdialogfailure("Payment Failure!", "You have reached the maximum allowed purchase transaction limit for a day per merchant.");
                } else if (status.equals("KE")) {
                    custumdialogfailure("Payment Failure!", message);
                } else {
                    custumdialogfailure("Payment Failure!", message);
                }
            }catch (Exception e){
                e.printStackTrace();
                try {
                    pDialogg.dismiss();
                    pDialogg.hide();
                    Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", PaymentMerchantActivity.this);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogg = new QPayProgressDialog(PaymentMerchantActivity.this);
         /*   pDialogg.setCancelable(false);*/
            pDialogg.show();
        }
    }


    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, custId, stan, crrn;

        public Ackclass(String app_id, String custId, String stan, String crrn) {
            this.app_id = app_id;
            this.custId = custId;
            this.stan = stan;
            this.crrn = crrn;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", custId);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn", crrn);
                Log.d(Utility.TAG, "post : " + jsonObject);
                ack_test = networkAPI.sendHTTPData(Constants.ACKN, jsonObject);
                Log.d(Utility.TAG, "response : " + ack_test);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }

    }


    public void custumdialogSucess(String discount) {
        GeneralPref.setAllowBonus(true);
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMerchantActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custome_dialog_success, null);

        final TextView paidAmount = v.findViewById(R.id.paidAmount);
        final TextView netAmount = v.findViewById(R.id.netAmount);
        final TextView discountAmount = v.findViewById(R.id.discountAmount);
        final TextView merchantName = v.findViewById(R.id.merchantName);
        final TextView date = v.findViewById(R.id.date);
        final TextView originalAmount = v.findViewById(R.id.originalAmount);
        final TextView youPaid = v.findViewById(R.id.youPaid);

        merchantName.setText(name);
        netAmount.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(amount)));
        netAmount.setPaintFlags(netAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        originalAmount.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(amount)));
        if (Double.parseDouble(discount.replace(",", "")) > 0) {
            double paidTo = amount - Double.parseDouble(discount);
            paidAmount.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(paidTo)));
            youPaid.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(paidTo)));
            discountAmount.setText("NPR " + new Decimalformate().decimalFormate(discount));
        } else {
            paidAmount.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(amount)));
            discountAmount.setText("NPR 0.00");
            netAmount.setVisibility(View.GONE);
            youPaid.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(amount)));

        }

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        String formattedDate = df.format(c);
        date.setText(formattedDate);

        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(PaymentMerchantActivity.this, MainActivity.class));
                finish();
            }
        });
        v.findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                rateApp();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (420 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailurecancel(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMerchantActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                new Cancel().execute();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMerchantActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(PaymentMerchantActivity.this, MainActivity.class));
                finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public void vibration() {
        Vibrator v = (Vibrator) PaymentMerchantActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
    }

    private void checkInternetConnection() {
        LinearLayout noInternetConnection = (LinearLayout) findViewById(R.id.no_internet_connection_id);
        NetworkInformation networkInformation = NetworkInformation.getInstance(this);
        if (networkInformation.isOnline()) {
            noInternetConnection.setVisibility(View.GONE);
        } else {
            noInternetConnection.setVisibility(View.VISIBLE);
        }
    }

    public void checkInternetConnection(View view) {
        checkInternetConnection();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            NetworkInformation networkInformation = NetworkInformation.getInstance(context);
            if (networkInformation.isOnline()) {
                checkInternetConnection();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void setPicasso(ImageView picasso) {
        try {
            if (!imgUrl.isEmpty() && imgUrl != null) {
                Picasso.get()
                        .load(imgUrl)
                        .error(R.drawable.header_placeholder)
                        .placeholder(R.drawable.header_placeholder)
                        .into(picasso);
            } else {
                picasso.setImageResource(R.drawable.header_placeholder);
            }
        } catch (Exception e) {

        }
    }

    public void rateApp() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMerchantActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_rate_now, null);
        builder.setView(v);
        final Dialog dialog = builder.create();
        final TextView current_rate = (TextView) v.findViewById(R.id.current_rate);
        final EditText reviw = (EditText) v.findViewById(R.id.review);
        final TextView restro_name = (TextView) v.findViewById(R.id.restro_name);
        final ImageView imgUrl = (ImageView) v.findViewById(R.id.imgUrl);
        final ImageView cancel_rating = (ImageView) v.findViewById(R.id.cancel_rating);
        cancel_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        setPicasso(imgUrl);
        restro_name.setText(name);
        TextView next = (TextView) v.findViewById(R.id.ok);
        current_rate.setText(String.valueOf(0));
        final me.zhanghai.android.materialratingbar.MaterialRatingBar materialRatingBar = (me.zhanghai.android.materialratingbar.MaterialRatingBar) v.findViewById(R.id.library_normal_ratingbar);
        materialRatingBar.setRating(0);

        materialRatingBar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                current_rate.setText(String.valueOf(rating));
            }
        });
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        reviw.requestFocus();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        final WindowManager.LayoutParams wlmp = dialog.getWindow()
                .getAttributes();
        wlmp.width = 600;
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.gravity = Gravity.CENTER;
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                new PostDealReview(materialRatingBar.getRating(), reviw.getText().toString()).execute();
                dialog.dismiss();


            }
        });
        v.findViewById(R.id.not).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        dialog.show();
    }

    public class PostDealReview extends AsyncTask<String, String, String> {
        float rating;
        String review;

        public PostDealReview(float rating, String review) {
            this.rating = rating;
            this.review = review;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String get_review = "";
            try {
                jsonObject.put("appId", app_id);
                jsonObject.put("custId", custId);
                jsonObject.put("termId", termId);
                jsonObject.put("review", review);
                jsonObject.put("rating", rating);
                jsonObject.put("lat", ((CustomerApplication) PaymentMerchantActivity.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) PaymentMerchantActivity.this.getApplication()).getLng());

                Log.d(Utility.TAG, "rate post : " + jsonObject);
                get_review = networkAPI.sendHTTPData(Constants.MERCHANT_REVIEW, jsonObject);
                Log.d(Utility.TAG, "rate post  response : " + get_review);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return get_review;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialogg.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject GetDealReviewResult = jsonObject.getJSONObject("MerchantRatingResult");
                boolean success = GetDealReviewResult.getBoolean("success");
                onBackPressed();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogg = new QPayProgressDialog(PaymentMerchantActivity.this);
            pDialogg.show();
        }
    }
}
