package net.qpaysolutions.QPay.SalesMerchants;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.DecimalTextMasking;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by deadlydragger on 6/29/16.
 */

public class PaymentMerchantProceed extends AppCompatActivity implements View.OnClickListener {

    private String term_id, name, Address;
    private String TAG;
    private TextView name_merchant, toolbar_text, tv_address, amount_select;
    private ImageView merchant_image;
    private Toolbar toolbar;
    private String imgURL;
    private TextView balance_main;
    private EditText amount;
    private Button confirm_money;
    private LinearLayout ll_pay;
    private boolean HasDenom;
    private List<String> stringList = new ArrayList<>();
    private boolean billPayList = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.payment_proceed);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar_text = (TextView) toolbar.findViewById(R.id.title);
            toolbar_text.setText("Proceed Payment");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            name_merchant = (TextView) findViewById(R.id.merchant_name);
            merchant_image = (ImageView) findViewById(R.id.image_merchant);
            amount = (EditText) findViewById(R.id.amount);
            balance_main = (TextView) findViewById(R.id.balance_main);
            confirm_money = (Button) findViewById(R.id.confirm_money);
            tv_address = (TextView) findViewById(R.id.tv_address);
            ll_pay = (LinearLayout) findViewById(R.id.ll_pay);
            amount_select = (TextView) findViewById(R.id.amount_select);

            balance_main.setText("NPR " + new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
            confirm_money.setOnClickListener(this);
            term_id = getIntent().getStringExtra("foren_id_scan");
            name = getIntent().getStringExtra("name");
            imgURL = getIntent().getStringExtra("ImageUrl");
            Address = getIntent().getStringExtra("Address");
            HasDenom = getIntent().getBooleanExtra("HasDenom", false);

            try {
                if (HasDenom) {
                    amount.setVisibility(View.GONE);
                    amount_select.setVisibility(View.VISIBLE);
                    JSONArray jsonArray = new JSONArray(getIntent().getStringExtra("Denom"));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String s = jsonArray.getString(i);
                        stringList.add(s);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            name_merchant.setText(name);
            tv_address.setText(Address);
           // amount.addTextChangedListener(new DecimalTextMasking(amount));
            amount.setTextSize(20);
            amount.setSelection(amount.getText().toString().length());
            setPicassoImage(imgURL);
            amount_select.setOnClickListener(this);
            if (GeneralPref.getMerchantType().equalsIgnoreCase("UPI")) {
                String barcodeValue = getIntent().getStringExtra("upiMerchant");
                try {
                    Log.d(Utility.TAG, "barcode value  :  " + barcodeValue);
                    HashMap<String, String> hashMap = getUPIData(barcodeValue);
                    Log.d(Utility.TAG, "barcode  value " + hashMap);
                    name = hashMap.get("59");
                    name_merchant.setText(hashMap.get("59"));
                    tv_address.setText(hashMap.get("60"));
                    try {
                        if (hashMap.get("54") != null) {
                            amount.setText(hashMap.get("54"));
                            amount.setFocusable(false);
                        } else {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    amount.setFocusable(true);
                                    new Utility().showKeyboard(PaymentMerchantProceed.this);
                                }
                            }, 500);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        amount.setFocusable(true);
                        new Utility().showKeyboard(PaymentMerchantProceed.this);
                    }
                    amount.addTextChangedListener(new DecimalTextMasking(amount));
                    amount.setTextSize(20);
                    amount.setSelection(amount.getText().toString().length());
                } catch (Exception e) {
                    e.printStackTrace();
                    custumdialogfailure("Invalid QRcode!", "You seem to have scanned invalid QRcode. Please scan proper QRcode.");
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static HashMap<String, String> getUPIData(String merchantId) {
        try {
            HashMap<String, String> tagAndValue = new HashMap<>();
            char[] merchantIds = merchantId.toCharArray();
            for (int i = 0; i < merchantIds.length; ) {
                if (i + 4 < merchantIds.length) {
                    String tag = String.valueOf(merchantIds[i]) + String.valueOf(merchantIds[i + 1]);
                    int length = Integer.parseInt(String.valueOf(merchantIds[i + 2]) + String.valueOf(merchantIds[i + 3]));
                    System.out.println("Tag length:: " + tag + " " + length);
                    StringBuilder values = new StringBuilder();
                    for (int j = i + 4; j < i + 4 + length; j++) {
                        values.append(String.valueOf(merchantIds[j]));
                    }
                    System.out.println("values:: " + values.toString());
                    tagAndValue.put(tag, values.toString());
                    i += 4 + length;
                    System.out.println("value of i:: " + i);
                } else {
                    return tagAndValue;
                }
            }
            return tagAndValue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMerchantProceed.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void setPicassoImage(String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.ic_store)
                    .error(R.drawable.ic_store)
                    .noFade()
                    .into(merchant_image);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.confirm_money:
                new Utility().hideSoftkey(PaymentMerchantProceed.this);
                if (HasDenom) {
                    double amount_txn_decimal = new Decimalformate().decimalFormateDouble(amount_select.getText().toString().replace(",", ""));
                    if (amount_txn_decimal >= 1 && amount_txn_decimal <= 20000) {
                        /*Intent intent = new Intent(PaymentMerchantProceed.this, PaymentMerchantActivity.class);
                        intent.putExtra("imgUrl", imgURL);
                        intent.putExtra("amount", amount_txn_decimal);
                        intent.putExtra("name", name);
                        intent.putExtra("termId", term_id);
                        startActivity(intent);
                        finish();*/
                        if (billPayList) {
                            billPayList = false;
                            Bundle bundle = new Bundle();
                            bundle.putString("imgUrl", imgURL);
                            bundle.putDouble("amount", amount_txn_decimal);
                            bundle.putString("name", name);
                            bundle.putString("termId", term_id);
                            PayMerchantListSheet payMerchantListSheet = new PayMerchantListSheet();
                            payMerchantListSheet.setArguments(bundle);
                            payMerchantListSheet.show(PaymentMerchantProceed.this.getSupportFragmentManager(), PaymentMerchantProceed.this.TAG);
                        }
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                billPayList = true;
                            }
                        }, 2000);
                    } else {
                        new Utility().showSnackbar(confirm_money, "Enter amount in range 1 to 20000.");
                    }
                } else {
                    double amount_txn_decimal = new Decimalformate().decimalFormateDouble(amount.getText().toString().replace(",", ""));
                    if (amount_txn_decimal >= 1 && amount_txn_decimal <= 20000) {
                        /*Intent intent = new Intent(PaymentMerchantProceed.this, PaymentMerchantActivity.class);
                        intent.putExtra("imgUrl", imgURL);
                        intent.putExtra("amount", amount_txn_decimal);
                        intent.putExtra("name", name);
                        intent.putExtra("termId", term_id);
                        startActivity(intent);
                        finish();*/
                        if (billPayList) {
                            billPayList = false;
                            Bundle bundle = new Bundle();
                            bundle.putString("imgUrl", imgURL);
                            bundle.putDouble("amount", amount_txn_decimal);
                            bundle.putString("name", name);
                            bundle.putString("termId", term_id);
                            PayMerchantListSheet payMerchantListSheet = new PayMerchantListSheet();
                            payMerchantListSheet.setArguments(bundle);
                            payMerchantListSheet.show(PaymentMerchantProceed.this.getSupportFragmentManager(), PaymentMerchantProceed.this.TAG);
                        }
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                billPayList = true;
                            }
                        }, 2000);
                    } else {
                        new Utility().showSnackbar(confirm_money, "Enter amount in range 1 to 20000.");
                    }
                }
                break;
            case R.id.amount_select:
                final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(PaymentMerchantProceed.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int month, String dateDesc) {
                        amount_select.setText(dateDesc);
                    }
                }).textConfirm("DONE")
                        .textCancel("CANCEL")
                        .btnTextSize(16)
                        .viewTextSize(25)
                        .setValues(stringList)
                        .build();
                pickerPopWin.showPopWin(PaymentMerchantProceed.this);
                break;
        }
    }
}

