package net.qpaysolutions.QPay.RechargePin;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 8/15/16.
 */
public class RechargePin extends Fragment {
    private QPayCustomerDatabase QPayCustomerDatabase;
    private RecyclerView recharge_recycle;
    private static String appId, id, lat, lng;
    private ArrayList<RechargeModel> rechargeModelArrayList = new ArrayList<>();
    private RechargePinAdapter pinAdapter;
    private static  QPayProgressDialog qPayProgressDialog;
    private LinearLayout recharge_layout,progressLayoutRechargePin;
    private MenuItem menuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        appId = QPayCustomerDatabase.getKeyAppId();
        id = QPayCustomerDatabase.getCustomerID();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recharge_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      /*  ((MainActivity) getActivity()).setToolbar_text("Recharge Pin");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(true);*/
        recharge_recycle = (RecyclerView) view.findViewById(R.id.recharge_recycle);
        recharge_recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        recharge_layout = (LinearLayout) view.findViewById(R.id.recharge_layout);
        progressLayoutRechargePin=view.findViewById(R.id.progressLayoutRechargePin);
        recharge_layout.setVisibility(View.GONE);
        recharge_recycle.setHasFixedSize(true);
        pinAdapter = new RechargePinAdapter(rechargeModelArrayList, RechargePin.this);
        recharge_recycle.setAdapter(pinAdapter);
        new RechargePinAsy(appId, id, "0.0", "0.0").execute();
    }

    public class RechargePinAsy extends AsyncTask<String, String, String> {
        String appId, id, lat, lng;
        public RechargePinAsy(String appId, String id, String lat, String lng) {
            this.appId = appId;
            this.id = id;
            this.lat = lat;
            this.lng = lng;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("lat", lat);
                jsonObject.put("lng", lng);
                String pin_return = new NetworkAPI().sendHTTPData(Constants.RECHARGE_PIN, jsonObject);
                Log.d("dinesh", "doInBackground: " + pin_return);
                return pin_return;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray data = jsonObject.getJSONArray("data");
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    for (int i = 0; i < data.length(); i++) {
                        menuItem.setVisible(true);
                        progressLayoutRechargePin.setVisibility(View.GONE);
                        recharge_layout.setVisibility(View.GONE);
                        JSONObject jsonObject1 = data.getJSONObject(i);
                        RechargeModel rechargeModel = new RechargeModel();
                        rechargeModel.setDate(jsonObject1.getString("Date"));
                        rechargeModel.setAmount(jsonObject1.getString("Amount"));
                        rechargeModel.setBillPayCode(jsonObject1.getString("BillPayCode"));
                        rechargeModel.setPinNo(jsonObject1.getString("PinNo"));
                        rechargeModel.setSerialNo(jsonObject1.getString("SerialNo"));
                        rechargeModel.setCrrn(jsonObject1.getString("Crrn"));
                        rechargeModel.setStan(jsonObject1.getString("Stan"));
                        rechargeModelArrayList.add(rechargeModel);
                    }
                   pinAdapter.notifyDataSetChanged();
                } else {
                    recharge_layout.setVisibility(View.VISIBLE);
                    progressLayoutRechargePin.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
                progressLayoutRechargePin.setVisibility(View.GONE);
                recharge_layout.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            qPayProgressDialog = new QPayProgressDialog(getActivity());
//            qPayProgressDialog.show();
            progressLayoutRechargePin.setVisibility(View.VISIBLE);

        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification, menu);
        MenuItem item = menu.findItem(R.id.notification_delet);
        item.setVisible(false);
        menuItem = item;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notification_delet:
                showDeletsweet();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

/*
    public void showDeletsweet() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Do you want to delete all Recharge Pin ?")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();

                    }
                }).show();
    }
*/



    public void showDeletsweet() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title=(TextView)v.findViewById(R.id.text_title);
        final ImageView img_icon =(ImageView)v.findViewById(R.id.img_icon);
        img_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning));
        img_icon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.color_yellow), android.graphics.PorterDuff.Mode.MULTIPLY);
        title.setText("Are You Sure?");
        dialog_balance.setText("Do you want to delete all Recharge Pin ?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DeletAllRechargePin().execute();
                dialog.dismiss();
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public static class DeletRechargePin extends AsyncTask<String, String, String> {
        String crrn, stan;


        public DeletRechargePin(String crrn, String stan) {
            this.crrn = crrn;
            this.stan = stan;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("lat", 0.0);
                jsonObject.put("lng", 0.0);
                jsonObject.put("crrn", crrn);
                jsonObject.put("stan",stan);
                Log.d("dinesh", "doInBackground: " + jsonObject);
                Log.d("dinesh", "doInBackground: " + NetworkAPI.sendHTTPData(Constants.DELETE_PIN, jsonObject));
                return NetworkAPI.sendHTTPData(Constants.DELETE_PIN, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }

    public  class DeletAllRechargePin extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("lat", 0.0);
                jsonObject.put("lng", 0.0);
                jsonObject.put("crrn", "ALL");
                jsonObject.put("stan","0");
                Log.d("dinesh", "doInBackground: " + jsonObject);
                Log.d("dinesh", "doInBackground: " + NetworkAPI.sendHTTPData(Constants.DELETE_PIN, jsonObject));
                return NetworkAPI.sendHTTPData(Constants.DELETE_PIN, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog= new QPayProgressDialog(getActivity());
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            rechargeModelArrayList.clear();
            pinAdapter.notifyDataSetChanged();
            recharge_layout.setVisibility(View.VISIBLE);
            menuItem.setVisible(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
               /* if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;*/
               getActivity().onBackPressed();
               return true;
            }
        });
    }

}