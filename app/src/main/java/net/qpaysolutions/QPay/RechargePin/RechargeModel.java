package net.qpaysolutions.QPay.RechargePin;

/**
 * Created by deadlydragger on 8/29/17.
 */

public class RechargeModel {
    String Date,PinNo,SerialNo,billPayCode,amount,crrn,stan;

    public String getStan() {
        return stan;
    }

    public String getCrrn() {
        return crrn;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public void setCrrn(String crrn) {
        this.crrn = crrn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getPinNo() {
        return PinNo;
    }

    public void setPinNo(String pinNo) {
        PinNo = pinNo;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getBillPayCode() {
        return billPayCode;
    }

    public void setBillPayCode(String billPayCode) {
        this.billPayCode = billPayCode;
    }
}
