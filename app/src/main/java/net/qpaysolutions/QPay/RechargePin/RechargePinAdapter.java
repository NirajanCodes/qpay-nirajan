package net.qpaysolutions.QPay.RechargePin;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 8/15/16.
 */
public class RechargePinAdapter extends RecyclerView.Adapter<RechargePinAdapter.PinHolder> {
    ArrayList<RechargeModel> rechargeModels;
    public RechargePin rechargePin;

    public RechargePinAdapter(ArrayList<RechargeModel> rechargeModels, RechargePin rechargePin) {
        this.rechargeModels = rechargeModels;
        this.rechargePin = rechargePin;
    }

    @Override
    public PinHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.iteam_pin, parent, false);
        return new PinHolder(view);
    }

    @Override
    public void onBindViewHolder(PinHolder holder, final int position) {
        holder.amount.setText(rechargeModels.get(position).getAmount());
        holder.pin_no.setText(rechargeModels.get(position).getPinNo());
        holder.sn_no.setText(rechargeModels.get(position).getSerialNo());
        holder.date.setText(Utility.dateFormatter(rechargeModels.get(position).getDate()));
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RechargePin.DeletRechargePin(rechargeModels.get(position).getCrrn(), rechargeModels.get(position).getStan()).execute();
                rechargeModels.remove(position);
                notifyDataSetChanged();
            }
        });


        switch (rechargeModels.get(position).getBillPayCode()) {
            case "612":
            case "606":
            case "607":
            case "608":
                holder.title.setText("NTC");
                holder.logo.setImageResource(R.drawable.nct_logo);
                break;
            case "604":
            case "605":
                holder.title.setText("Smart Cell");
                holder.logo.setImageResource(R.drawable.smart_cell);
                break;
            case "609":
            case "610":
            case "611":
                holder.title.setText("UTL");
                holder.logo.setImageResource(R.drawable.utl);
                break;
            case "616":
            case "617":
                holder.title.setText("Net Tv");
                holder.logo.setImageResource(R.drawable.net_tv);
                break;
            case "613":
            case "614":
            case "615":
                holder.title.setText("Broodlink");
                holder.logo.setImageResource(R.drawable.broodlink);
                break;
            case "618":
            case "619":
            case "620":
            case "621":
            case "622":
            case "623":
            case "624":
            case "625":
            case "626":
                holder.title.setText("Dishome");
                holder.logo.setImageResource(R.drawable.dishhome);
                break;
            case "634":
                holder.title.setText("Mero Tv");
                holder.logo.setImageResource(R.drawable.mero_tv);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return rechargeModels.size();
    }

    public class PinHolder extends RecyclerView.ViewHolder {
        public TextView title, amount, pin_no, sn_no, trash, date;
        public ImageView logo;
        public CardView card_recharge_item;

        public PinHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            amount = (TextView) view.findViewById(R.id.amount_card);
            pin_no = (TextView) view.findViewById(R.id.pin_no);
            sn_no = (TextView) view.findViewById(R.id.sn_no);
            trash = (TextView) view.findViewById(R.id.trash);
            date = (TextView) view.findViewById(R.id.date);
            logo = (ImageView) view.findViewById(R.id.logo);
            card_recharge_item = (CardView) view.findViewById(R.id.card_recharge_item);
        }
    }
}
