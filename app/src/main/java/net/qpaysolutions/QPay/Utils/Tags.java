package net.qpaysolutions.QPay.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import net.qpaysolutions.QPay.Billpayment.ubskhanepani.UBSModel;
import net.qpaysolutions.QPay.Billpayment.utility.electricity.NeaOfficeResultModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 9/22/16.
 */
public class Tags {
    public static int TIME_SLEEP = 1000;
    public static final String TERMID = "term_id";
    public static final String LAST_4 = "last_4";
    public static final String PIN = "pin";
    public static final String DEVICE_TOKEN = "dev_token";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String STATUS = "status";
    public static final String MESSAGE = "msg";
    public static final String STAN = "stan";
    public static final String CRRN = "crrn";
    public static final String FFIVE = "55";
    public static final String SFIVE = "75";
    public static final String ZERO = "00";
    public static final String TZERO = "20";
    public static final String ZEIGHT = "08";
    public static final String ZLOW = "LO";
    public static final String CARDNO = "card_no";
    public static final String EDATE = "e_date";
    public static final String BANKNAME = "bankName";
    public static final String CARDURL = "cardUrl";
    public static final String RESP_CODE = "resp_code";
    public static final String DSPLY_MSG = "displayMsg";
    public static final String STAN_SCAP = "Stan";
    public static final String ACCOUNT_NAME_S = "accountName";
    public static final String BRANCH_ID_S = "branchId";
    public static final String ACCOUNT_NUMBER_S = "accountNumber";
    public static final String MERCHANT_BANK_ID = "bankAccId";
    public static String ACCOUNT_TYPE = "account_type";
    public static String APPID = "app_id";
    public static String AMOUNT = "amount";
    public static String CARD_TYPE = "act_type";
    public static String CELL_PHONE = "cell_phone";
    public static String IMG_URL = "imgURL";
    public static String NAME = "name";

    public static String RT = "RT";
    public static String LO = "LO";
    public static String PR = "PR";

    public static String RECEIVER_ID = "reciever_id";
    public static String CUST_ID = "CustId";
    public static String SUCESS = "success";
    public static boolean TRUE = true;
    public static boolean FALSE = false;
    public static String DATA = "data";
    public static String TXNAMOUNT = "TxnAmount";
    public static String GEOLNG = "GeoLng";
    public static String GEOLAT = "GeoLat";
    public static String RESCODE = "ResCode";
    public static String TERM_ID_SAGAR = "TermId";


    //    sagar sir
    public static String ID = "id";
    public static String PAGE = "page";
    public static String OFFSET = "offset";


    //    sagar sir new Key
    public static String APPID_S = "appId";
    public static String TERMID_S = "id";
    public static String ID_S = "Id";
    public static String IMGURL_S = "ImageUrl";
    public static String INSTITUTION_S = "InstitutionName";
    public static String PARCITICIPANTID_S = "participantId";
    public static String BANKNAME_S = "BranchName";
    public static String INSTITUTE_ID_S = "institutionId";

    //resturant
    public static String CATRGORY_S = "Category";
    public static String BACKGROUND_IMAGE = "BackgroundImage";
    public static String ICON = "Icon";
    public static String ADDRESS = "Address";
    public static String RATING = "Rating";
    public static String RESTURANT = "Restaurant";
    public static String CAUSINE = "Cuisine";
    public static String NO_OF_RESTRO = "NumberOfRestro";

    //request
    public static final int REQUEST_CAMERA = 11;
    public static final int REQUEST_STORAGE = 12;
    public static final int PICK_USER_REQUEST = 1;
    public static final int PICK_ID_FRONT_REQUEST = 2;
    public static final int PICK_ID_BACK_REQUEST = 3;
    public static final int LOCATION = 111;
    public static final int NFC = 114;
    public static final int CALL = 112;
    public static final int SMS = 113;
    public static final int KYC_EXTERNAL_STORAGE = 114;
    public static final int LOCATION_SHOP = 115;
    public static final int CALL_SHOP = 113;
    public static final int DEAL = 116;
    public static final int RESTRO = 117;

    public static void setSharedPreferencesArray(String neaOfficeResult) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("neaOfficeResult", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("neaOfficeResult", neaOfficeResult);
        editor.apply();
    }

    public static ArrayList<NeaOfficeResultModel> getPreferencesArray() {
        ArrayList<NeaOfficeResultModel> trafficPrecincts = new ArrayList<>();
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("neaOfficeResult", Context.MODE_PRIVATE);
        String value = sharedPreferences.getString("neaOfficeResult", "");
        try {
            JSONObject jsonObject = new JSONObject(value);
            JSONObject NeaOfficeResult = jsonObject.getJSONObject("NeaOfficeResult");
            JSONArray data = NeaOfficeResult.getJSONArray("data");
            boolean success = NeaOfficeResult.getBoolean("success");
            if (success) {
                for (int i = 0; i < data.length(); i++) {
                    JSONObject jsonObject1 = data.getJSONObject(i);
                    NeaOfficeResultModel neaOfficeResultModel = new NeaOfficeResultModel();
                    neaOfficeResultModel.setCode(jsonObject1.getString("Code"));
                    neaOfficeResultModel.setName(jsonObject1.getString("Name"));
                    trafficPrecincts.add(neaOfficeResultModel);
                }
                return trafficPrecincts;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static void setPreferenceArrayKhanepani(String waterResult, String code) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("ubsOffice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (code.equalsIgnoreCase("402")) {
            editor.putString("ubsOffice", waterResult);
        } else {
            editor.putString("nepalWaterSupply", waterResult);

        }
        editor.apply();
    }

    public static ArrayList<UBSModel> getPreferenceArrayKhanepani(String code) throws JSONException {

        ArrayList<UBSModel> ubsData = new ArrayList<>();
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("ubsOffice", Context.MODE_PRIVATE);
        String value = null;
        try {
            if (code.equalsIgnoreCase("402")) {
                value = sharedPreferences.getString("ubsOffice", "");
                JSONObject jsonObject = new JSONObject(value);
                JSONObject UbsWaterOfficeResult = jsonObject.getJSONObject("UbsWaterOfficeResult");
                boolean success = UbsWaterOfficeResult.getBoolean("success");
                if (success) {
                    JSONArray data = UbsWaterOfficeResult.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        UBSModel ubsModel = new UBSModel();
                        JSONObject object = data.getJSONObject(i);
                        ubsModel.setOffice(object.getString("Office"));
                        ubsModel.setOfficeCode(object.getString("OfficeCode"));
                        ubsData.add(ubsModel);
                    }
                    return ubsData;
                }

            } else {
                value = sharedPreferences.getString("nepalWaterSupply", "");
                JSONObject jsonObject = new JSONObject(value);
                JSONArray data = jsonObject.getJSONArray("data");
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jsonObject1 = data.getJSONObject(i);
                        UBSModel ubsModel = new UBSModel();
                        ubsModel.setOfficeCode(jsonObject1.getString("OfficeCode"));
                        ubsModel.setOffice(jsonObject1.getString("Office"));
                        ubsData.add(ubsModel);
                    }
                    return ubsData;
                }


            }
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }


        return null;
    }


}
