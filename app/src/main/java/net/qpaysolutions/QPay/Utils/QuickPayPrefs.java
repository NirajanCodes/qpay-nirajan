package net.qpaysolutions.QPay.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class QuickPayPrefs {

    public static String getTopUpNumber(){
        SharedPreferences sharedPreferences=CustomerApplication.getContext().getSharedPreferences("MyBalance", Context.MODE_PRIVATE);
       return sharedPreferences.getString("TopUpNumber",GeneralPref.getPhone().toString());
    }

    public static void setTopUpNumber(String number){
        SharedPreferences sharedPreferences=CustomerApplication.getContext().getSharedPreferences("MyBalance",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(number,GeneralPref.getPhone().toString());
        editor.apply();
    }


    public static String getTopUpBalance(){
        SharedPreferences sharedPreferences=CustomerApplication.getContext().getSharedPreferences("MyBalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("TopUpNumber",GeneralPref.getPhone().toString());
    }

    public static void setTopUpBalance(String number){
        SharedPreferences sharedPreferences=CustomerApplication.getContext().getSharedPreferences("MyBalance",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(number,GeneralPref.getPhone().toString());
        editor.apply();
    }
}
