package net.qpaysolutions.QPay.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by deadlydragger on 9/4/17.
 */

public class BalancePref {
    public static  String prefBalance;
    public static String wallet,available,deposite,hold;

    public static double getWalletAmount(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        return new Decimalformate().decimalFormateDouble(sharedPreferences.getString("wallet","0.00"));

    }
    public static String getAvailableAmount(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("available","0.00");
    }

    public static void setAvailableAmount(String s){

        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("available",s);
        editor.apply();

    }

    public static double getDepositeProgress(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        return new Decimalformate().decimalFormateDouble(sharedPreferences.getString("deposite","0.00"));

    }
    public static double getHoldAmount(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        return new Decimalformate().decimalFormateDouble(sharedPreferences.getString("hold","0.00"));

    }

    public static void setWalletAmount(String s){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("wallet",s);
        editor.apply();

    }



    public static void setDepositeProgress(String s){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("deposite",s);
        editor.apply();

    }

    public static void setHoldAmount(String s){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("hold",s);
        editor.apply();

    }

    public static void setBonusAmount(String s){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("bonus",s);
        editor.apply();
    }

    public static String getBonusAmount(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("bonus","0");

    }

    public static void setRewardPoint(String s){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("reward",s);
        editor.apply();

    }

    public static String getRewardPoint(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("prefBalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("reward","0");

    }

}
