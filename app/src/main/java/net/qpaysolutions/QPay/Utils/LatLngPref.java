package net.qpaysolutions.QPay.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by deadlydragger on 8/30/17.
 */

public class LatLngPref {
    public static void setLat(String lat){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("location",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("lat",lat);
        editor.apply();

    }
    public static String getLat(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("location",Context.MODE_PRIVATE);
       String lat=sharedPreferences.getString("lat","0.00");
        return lat;
    }

    public static void setLng(String lng){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("location",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("lng",lng);
        editor.apply();

    }
    public static String getLng(){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("location",Context.MODE_PRIVATE);
        String lat=sharedPreferences.getString("lng","0.00");
        return lat;
    }
}