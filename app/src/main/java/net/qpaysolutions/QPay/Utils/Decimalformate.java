package net.qpaysolutions.QPay.Utils;

/**
 * Created by deadlydragger on 11/9/16.
 */
public class Decimalformate {
    public static String decimalFormate(String value) {
        String decimal = "";
        try {
            if (!value.equals("")) {
                decimal = String.format("%.2f", Double.parseDouble(value));
            } else {
                return "0.0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decimal;
    }

    public static double decimalFormateDouble(String value) {
        String decimal;
        try {
            decimal = String.format("%.2f", Double.parseDouble(value));
            return Double.valueOf(decimal);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0.00;
    }
}
