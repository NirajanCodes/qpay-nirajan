package net.qpaysolutions.QPay.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * Created by deadlydragger on 9/13/17.
 */

public class GeneralPref {

    public static void setPaymentCount(int count) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("paymentCount", count).apply();
    }

    public static int getPaymentCount() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("paymentCount", 29);
    }

    public static void setAllowBonus(boolean allowBonus) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("allowBonus", true);
        editor.apply();
    }

    public static boolean getAllowBonus() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return /*sharedPreferences.getBoolean("allowBonus", false)*/true;
    }

    public static void setPrefCount(int i, String kyc_status, int newChatMessageCount, int number_of_restro, int taxiCount, boolean isBank) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("atm_count", i);
        editor.putString("kyc_status", kyc_status);
        editor.putInt("newChatMessageCount", newChatMessageCount);
        editor.putInt("number_of_restro", number_of_restro);
        editor.putInt("taxiCount", taxiCount);
        editor.putBoolean("isBank", isBank);
        editor.apply();
    }

    public static void setAppId(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("appId", s);
        editor.apply();

    }

    public static void setCustId(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("custId", s);
        editor.apply();

    }

    public static String getAppId() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("appId", "");
    }


    public static void setName(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", s);
        editor.apply();

    }

    public static String getName() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("name", "");
    }


    public static void setMail(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("mail", s);
        editor.apply();

    }

    public static String getMail() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("mail", "");
    }

    public static void setPhone(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("phone", s);
        editor.apply();

    }

    public static String getPhone() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("phone", "");
    }


    public static String getCustId() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("custId", "");
    }

    public static boolean getBankDeposite() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("isBank", false);
    }

    public static void setKYC(String status) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("kyc_status", status);
        editor.apply();

    }

    public static int getAtm() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("atm_count", 0);
    }

    public static String getKYC() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("kyc_status", "");
    }

    public static int getChatCount() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("newChatMessageCount", 0);
    }

    public static int getNoofRestro() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("number_of_restro", 0);
    }

    public static int getTaxi() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("taxiCount", 0);
    }


    public static void settime(int time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("time", time);
        editor.apply();
    }

    public static int getTime() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("time", 0);
    }


//    contact

    public static void setAbout(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("About", time);
        editor.apply();
    }

    public static int getAbout() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("About", 0);
    }

    public static void setPrivacyUrl(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("PrivacyUrl", time);
        editor.apply();
    }

    public static int getPrivacyUrl() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("PrivacyUrl", 0);
    }

    public static void setTermOfUseUrl(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("TermOfUseUrl", time);
        editor.apply();
    }

    public static String getTermOfUseUrl() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("TermOfUseUrl", "");
    }

    public static void setphone(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("phone", time);
        editor.apply();
    }

    public static String getphone() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("phone", "");
    }

    public static void setemail(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email", time);
        editor.apply();
    }

    public static String getemail() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("email", "");
    }

    public static void seturl(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("url", time);
        editor.apply();
    }

    public static String geturl() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("url", "");
    }

    public static void setUsernameWordlink(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username", time);
        editor.apply();
    }

    public static String getUsernameWordlink() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("username", "");
    }

    public static void setIsTv(boolean isTv) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isTv", isTv);
        editor.apply();
    }

    public static boolean getIsTv() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("isTv", false);
    }

    public static void shopShow(boolean time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isShop", time);
        editor.apply();
    }

    public static void setCashback(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("cashback", s);
        editor.apply();

    }

    public static String getCashback() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("cashback", "");
    }

    public static boolean isShop() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("isShop", false);
    }

    public static void setEmailUser(String time) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email", time);
        editor.apply();
    }

    public static String getEmailUser() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("email", "");
    }

    public static void setWaterInfo(String type) {

        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("type", type);
        editor.commit();

    }

    public static String getWaterInfo() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("type", null);

    }


    public static String getToken() {
        SharedPreferences sharePreference = CustomerApplication.getContext().getSharedPreferences("token", Context.MODE_PRIVATE);
        return sharePreference.getString("fcm_token", "APA91bFxQYbKx5v");
    }


    public static void setRewardAmount(String s) {

        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int decimalSplit = s.indexOf(".");
        String beforePoint = s.substring(0, decimalSplit);
        String afterDecimal = s.substring(decimalSplit, s.length());
        String parsed = null;
        if (afterDecimal.length() > 3) {
            afterDecimal = afterDecimal.substring(0, 3);
        } else if (afterDecimal.length() < 1) {
            afterDecimal = afterDecimal.concat(".00");
        } else if (afterDecimal.length() == 2) {
            afterDecimal = afterDecimal.concat("0");
        }
        parsed = beforePoint.concat(afterDecimal);

        editor.putString("reward_amount", parsed);
        editor.apply();

    }

    public static String getRewardAmount() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("reward_amount", "");
    }


    public static void merchantType(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("merchantType", s);
        editor.apply();

    }

    public static String getMerchantType() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("merchantType", "");
    }


    public static void setUPIMerchant(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("upiMerchantn", s);
        editor.apply();

    }

    public static String getUPIMERCHANT() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("upiMerchantn", "");
    }

    public static void setRandomNumber(String r) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("randomNumber", r);
        editor.apply();
    }

    public static String getUpiId() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("randomId", "");
    }

    public static void setUpiId(String r) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("randomId", r);
        editor.apply();
    }

    public static int getRestroCount() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("restro_count", 0);
    }

    public static void setRestroCount(int r) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("restro_count", r);
        editor.apply();
    }

    public static String getRandomNumber() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("randomNumber", "");
    }


    public static void setTutorStatus(boolean show) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("tutor", show);
        editor.apply();
    }

    public static boolean isTutorShown() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("tutor", false);
    }

    public static void setQuickWater(Set<String> strings) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet("waterTopUp", strings);
        editor.apply();
    }

    public static Set<String> getQuickWater() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getStringSet("waterTopUp", null);
    }

    public static void setRebate(String rebate) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("rebate_cashback", rebate);
        editor.apply();
    }

    public static String getRebate() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("rebate_cashback", null);
    }

    public static void setSubisuQuickPay(String subisu) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("MyBalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("subisuQuick", subisu);
        editor.apply();
    }

    public static String getSubisuQuickPay() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("MyBalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("subisuQuick", "");
    }

    public static void setHideBalance(boolean show) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("showBalance", show);
        editor.apply();
    }

    public static boolean getHideBalance() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("showBalance", false);
    }


    public static void setBannerArray(String banners) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("banners", banners);
        editor.apply();
    }

    public static String getBannerArray() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
        return sharedPreferences.getString("banners", "");
    }


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "androidhive-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public GeneralPref(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);

    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor = pref.edit();
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return !pref.contains(IS_FIRST_TIME_LAUNCH);
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
