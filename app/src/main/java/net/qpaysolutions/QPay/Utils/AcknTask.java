package net.qpaysolutions.QPay.Utils;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 5/26/17.
 */

public class AcknTask {
    public  class Ackclass extends AsyncTask<String, String, String> {
        String app_id, cust_id, stan, crrn;

        public Ackclass(String app_id, String cust_id, String stan, String crrn) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.stan = stan;
            this.crrn = crrn;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPIBillPay = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn", crrn);
                Log.d("dinesh", "doInBackground: "+jsonObject.toString());
                ack_test = networkAPIBillPay.sendHTTPData(Constants.ACKN, jsonObject);
                Log.d("dinesh", "doInBackground: "+ack_test);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

}
