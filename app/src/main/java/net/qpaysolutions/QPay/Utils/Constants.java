package net.qpaysolutions.QPay.Utils;

import net.qpaysolutions.QPay.BuildConfig;

/**
 * Created by dinesh on 5/19/16.
 */
public interface Constants {
    String BASE_API = BuildConfig.BASE_API;
    String FIRST_GCM_CALL_URL = BASE_API + "getcustid";
    String SECOND_WEB_CALL = BASE_API + "registercustomerinfo";
    String FOURTH_WEB_CALL = BASE_API + "registercard";
    String AUTH_WEB_CALl = BASE_API + "activatecustomer";
    String LAST_AUTH_CALL = BASE_API + "checkcuststatus";
    String GET_BALANCE_QPAY = BASE_API + "getbalance";
    String REGISTER_CARD = BASE_API + "registercard";
    String VERIFY_CARD_TYPE = BASE_API + "selftopoff";
    String CHECK_STATUS_FINAL_VERIFICATION = BASE_API + "checktxnstatusst";
    String LOOK_UP_FRIENDS = BASE_API + "lookupfriend";
    String GET_BALANCE_EACH_CARD = BASE_API + "balanceinquiry";//balanceinquiry
    String GET_BALANCE_EACH_CARD_FINAL = BASE_API + "balanceinqstatus";
    String TRANSFER_FUND_QPAY = BASE_API + "transferfunds";//cust_id/usko_id/amount
    String SCAN_NAME = BASE_API + "getname";
    String DELET_CARD = BASE_API + "deletecard";
    String RANDOM_PIN = BASE_API + "storeresetpin";
    String TOP_OFF = BASE_API + "selftopoff";
    String CHECK_PHONE = BASE_API + "checkphone";
    String REACT_ACTIVATE = BASE_API + "reactivateCustomer";
    String PURCH_PAYMENT_CARD = BASE_API + "posttxncard";
    String PURCHASE_TRSCTION_CONSUMER = BASE_API + "checkposttxncardstatus";
    String REGISTER_DEVICE = Constants.BASE_API + "registerdevice";
    String MERCHANT_PAY_QPAY = Constants.BASE_API + "posttxnqpay";
    String FUND_TRNAS_CARD_FRND = Constants.BASE_API + "fundtransferwithcard";
    String CHECK_PURCHASE_FRN_CONST = Constants.BASE_API + "checkfundtransfercardstatus";
    String HISTORY_RESULT = BuildConfig.SAGAR_BASE + "transaction/successtransaction";
    String POST_IMAGE_QPAY = BuildConfig.SAGAR_BASE + "upload/postfrombody";
    String ACKN = BASE_API + "appackcon";
    String RESEND_CODE = BASE_API + "resendactivationcodecon";
    String CHECK_MY_STATUS_BG = BASE_API + "checkmystatusbg";
    String CHECK_MY_STATUS = BASE_API + "checkmystatus";
    String CHECK_MY_STATUS_NEW = BASE_API + "checkmystatusnew";
    String GET_TAXI_NEAR = BASE_API + "getnearbytaxis";
    String NEAR_BY_DEALS = BASE_API + "getnearbydeals";
    String CHECKBONUS=BASE_API+"checkbonusflag";
//    String NEAR_BY_DEALS = "https://prod.qpaysolutions.net/QPay.svc/" + "getnearbydeals";

    String BILLPAY_CARD = BASE_API + "BillPayWithCard";
    String BILLPAY_CARD_STATUS = BASE_API + "BillPayWithCardStatus";
    String DELET_NEW_APPID = BASE_API + "deletenewcustinfo";
    String INVITE_FRIENDS = BASE_API + "InviteUser";
    String NEAR_BY_DEALS_BY_ID = BASE_API + "getdealwithid";
    String REVERIFICATION = BASE_API + "retryverification";
    String GET_CONTACT_US = BASE_API + "getmobilecontent";
    String NEAR_BY_SHOP = BASE_API + "terminalnearby";
    String WLINK = BASE_API + "CheckPaymentWlink";
    String NEARBY_ATM = BASE_API + "atmnearbycustomer";
    String NOTIFICATION_SETING = BASE_API + "notificationsettings";
    String FAQ_LIST = BASE_API + "faqcategorymobile?app_type=1";
    String FAQ_DETAILS = BASE_API + "faqcontentmobile?app_type=1&faqcategory_id=";
    int OFFSET = 20;
    String SAGAR_BASE = BuildConfig.SAGAR_BASE;
    String RECEPTION = SAGAR_BASE + "merchantreceipt/getmerchantreceipt";
    String GET_DEAL_REVIEW = BASE_API + "GetDealReview";
    String POST_DEAL_REVIEW = BASE_API + "PostDealReview";
    String MERCHANT_REVIEW = BASE_API + "merchantrating";
    //bank
    String DEPOSIT_TO_BANK = BASE_API + "balancetransfertobank";
    String DELET_BANK_ACCOUNT = BASE_API + "DeleteBankAccount";
    String VERIFY_ACCOUNT = BASE_API + "VerifyBankAccount";
    String DEPOSIT_OTP = BASE_API + "ConfirmBankDeposit";
    String BANK_DEPOSITE = BASE_API + "PostBankAcc";
    String GET_PARTICIPANTS = BASE_API + "GetIpsParticipants";
    String GET_PARTICIPANTS_BRANCH = BASE_API + "GetIpsParticipantsBranch";
    String DEPOSIT_HOLD_AMOUNT = BASE_API + "checkipstxnstatus";
    //kyc
    String GET_ZONE = BASE_API + "getstate";
    String GET_DISTRICT = BASE_API + "GetDistrictByState";
    String GET_VDC_MUNICIPALITY = BASE_API + "GetVdcMunicipality";
    String GET_ALL_DISTRICT = BASE_API + "GetAllDistrict";
    String POST_KYC_INFO = BASE_API + "PostKycInfo";
    //    String BASE_API="https://prod.qpaysolutions.net/QPay.svc/";
    //resturant
    String BASE_API_RESTURANT = BuildConfig.BASE_API_RESTURANT;
    String GET_CATEGORY = BASE_API_RESTURANT + "restaurantcategory";
    String GET_RESTURANT_LIST = BASE_API_RESTURANT + "RestaurantInfo";
    String GETMENUCATRGORY = BASE_API_RESTURANT + "GetMenuAndCategory";
    String POST_ORDER = BASE_API_RESTURANT + "postorder";
    String RESTURANT_PAYMENT = BASE_API + "restaurantpayment";
    String CHECK_STATUS_RETURANT = BASE_API + "CheckRestaurantPaymentStatus";
    //chat api
    String BASE_API_CHAT = BuildConfig.BASE_API_CHAT;
    String SEND_MESSAGE_CHAT = BASE_API_CHAT + "Chat/SendMessage";
    String SYANK_CONTACT = BASE_API_CHAT + "Contact/SyncContact";
    String GET_UNREAD_MESSAGE = BASE_API_CHAT + "Chat/GetUreadMessage";
    String DELET_CHAT_MSG = BASE_API_CHAT + "Chat/DeleteMessage";
    String GET_MESSAGE_BY_ID = BASE_API_CHAT + "Chat/GetMessageById";
    String TRAFFIC_PAY_LOOKUP = BASE_API + "trafficchitlookup";

    //nea
    String NEA_OFFICE = BASE_API + "neaoffice";
    String CHECK_WORDLINK_BILL = BASE_API + "checkpayment";
    String RECHARGE_PIN = BASE_API + "rechargepins";
    String NOTIFICATION = BASE_API + "notification";
    String DELETE_NOTIFICATION = BASE_API + "deletenotification";
    String DELETE_PIN = BASE_API + "deleterechargepin";

    //flights
    String NATIONALITY_SECTOR = BASE_API + "domesticflightsecntly";
    String FLIGHT_SEARCH = BASE_API + "domesticflightsearch";
    String FLIGHT_IMAGE = "https://qpaystorage.blob.core.windows.net/airlines/";
    String BOOK_FLIGHT = BASE_API + "domesticflightbook";
    String BOOK_FLIGHT_CONFIRM = BASE_API + "domesticflightconfirm";
    String TICKETS_DOWNLOAD = SAGAR_BASE + "domesticflight/ticketpdf";
    String TICKETS_HISTORY = SAGAR_BASE + "domesticflight/ticket";

    //    statement
    String STATEMENT = SAGAR_BASE + "transaction/consumerstatement";
    String UPLOAD_IMAGE = BASE_API + "uploadimage";

    //    kyc
    String POST_KYC = BASE_API + "PostKycInfoNew";
    String DETAILS_KYC = BASE_API + "KycDetail";

    // payment gatway
    String PAYMENT_GATEWAY = BASE_API + "Pgateway";
    String PAYMENT_GATWAY_TOPUP = BASE_API + "paymentgatewaycusttopup";

    //water
    String USB_OFFICe = BASE_API + "ubswateroffice";
    String CHECKUBS_PAYMENT = BASE_API + "checkpayment";
    String NKC_OFFICe = BASE_API + "nwscwateroffice";
    //
    String CASHBACK_LIST = BASE_API + "BillPayCashBack";
    String CHECK_REWARD = BASE_API + "checkreward";
    //    upi card
    String PANENROLLMENT = BASE_API + "panenrollment";
    //qr code generation
    String QRGENERATION = BASE_API + "cpqrcgeneration";

    //nchl

    String BANKLIST = BASE_API + "nchlnpibank";
    String BRANCHLIST = BASE_API + "nchlnpibankbranch";
    String IDTYPE = BASE_API + "nchlnpiidtype";
    String NCHLPOSTTXN = BASE_API + "nchlnpiposttxn";

    //Bus
    String BASEAPIBUS ="https://node.qpaysolutions.net/QPay.svc/";
    String BUSROUTES = BASEAPIBUS + "getRouteList";
    String BUS_GETTRIPINFO = BASEAPIBUS + "getTripInfo";
    String BUS_BOOKTICKET = BASEAPIBUS + "bookTicket";
    String BUS_REFRESHTRIPLIST = BASEAPIBUS + "refreshTripList";
    String BUS_CANCELQUEUE = BASEAPIBUS + "cancelQueue";
    String BUS_UPDATEPASSENGERINFO = BASEAPIBUS + "updatePassengerInfo";
    String BUS_CONFIRMPAYMENT = BASEAPIBUS + "confirmPayment";
    String BUS_CONFIRMPAYMENTSTATUS = BASEAPIBUS + "confirmPaymentStatus";
    String BUS_QUERYTICKET = BASEAPIBUS + "queryTicket";
    String BUS_TICKET_PDF=BASE_API_CHAT+"BusSewa/GetBusTicketInPdf";
}
