package net.qpaysolutions.QPay.Utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import net.qpaysolutions.QPay.Manifest;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by deadlydragger on 6/26/16.
 */
public class NetworkAPI {


    public static String getFromurl(String url) {
        String result = "";
        try {
            Log.d("dinesh", "getFromurl: " + url);
            OkHttpClient okHttpClient = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            result = response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
        }
        return result;
    }

    public static String sendHTTPData(String urlpath, JSONObject json) throws Exception {
        String TAG = "dinesh";
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(75, TimeUnit.SECONDS)
                .writeTimeout(75, TimeUnit.SECONDS)
                .readTimeout(75, TimeUnit.SECONDS)
                .build();

        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(urlpath)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String sendHTTPDataForm(String urlpath, JSONObject json) throws Exception {
        String TAG = "dinesh";


        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder()
//                .addEncoded("Content-Type","application/x-www-form-urlencoded")
                .addFormDataPart("APPID", json.getString("APPID"))
                .addFormDataPart("APPNAME", json.getString("APPNAME"))
                .addFormDataPart("MERCHANTID", json.getString("MERCHANTID"))
                .addFormDataPart("PARTICULARS", json.getString("PARTICULARS"))
                .addFormDataPart("REFERENCEID", json.getString("REFERENCEID"))
                .addFormDataPart("REMARKS", json.getString("REMARKS"))
                .addFormDataPart("TOKEN", json.getString("TOKEN"))
                .addFormDataPart("TXNAMT", json.getString("TXNAMT"))
                .addFormDataPart("TXNCRNCY", json.getString("TXNCRNCY"))
                .addFormDataPart("TXNDATE", json.getString("TXNDATE"))
                .addFormDataPart("TXNID", json.getString("TXNID"))

/*
                .addFormDataPart("APPID", *//*json.getString("APPID")*//*"qpay-1")
                .addFormDataPart("APPNAME", *//*json.getString("APPNAME")*//*"qpay")
                .addFormDataPart("MERCHANTID", *//* json.getString("MERCHANTID")*//*"15")
                .addFormDataPart("PARTICULARS", *//* json.getString("PARTICULARS")*//*"QPay Wallet Topup")
                .addFormDataPart("REFERENCEID",  *//*json.getString("REFERENCEID")*//*"154879")
                .addFormDataPart("REMARKS",*//* json.getString("REMARKS")*//*"QPay Wallet Topup")
                .addFormDataPart("TOKEN",*//* json.getString("TOKEN")*//*"aNekfo/BFfusGax0Hs+QVzikRfMdA4oyVKVjfANINNxnrpV+D3G6s7M4rCZctRsN+EUIko6xLsHVEu8jnD3WH3SkiKNsQLAR4cmBKGE2vnCY0QMWE/6JHXjAvjQTU7fzs8Q4qGbHo958vH2uUGfTZHd1nLUVZI89+gsl2YmxQtkQUzrzpdYFAAJdm65/C/4HyHzf3l5sFwKJCtIks9UeF1qEH2PK1JCJQZRVg4BEsw3PN9i/V1++MnCp5T+salq9U/j5smwN9vQFDdVCJ8JDCzJs3KqtcFeZi//oFhSBZFt/Z4e54G7w9SBSWWd8IXIYy8hGuQsfUokolWxiBQG3LQ==")
                .addFormDataPart("TXNAMT",*//* json.getString("TXNAMT")*//*"1111")
                .addFormDataPart("TXNCRNCY", *//*json.getString("TXNCRNCY")*//*"NPR")
                .addFormDataPart("TXNDATE",*//*json.getString("TXNDATE")*//*"13-04-2018")
                .addFormDataPart("TXNID",*//* json.getString("TXNID")*//*"154879810304130737")
         */


                .build();
        Request request = new Request.Builder()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .url(urlpath)
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        System.out.println(response.body().toString());

        return response.body().string();
    }


    public static String sendHTTPDataKYC(String urlpath, JSONObject json) throws Exception {
        String TAG = "dinesh";
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .build();

        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(urlpath)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String downloadFileFromeUrl(String urlpath, JSONObject json, Context contex, String file_name) throws Exception {
        String message;
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .build();

        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(urlpath)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println("Response:: " + response.message());

        String root =Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath();
        File myDir = new File(root + "/qpay");
        myDir.mkdirs();
        String fname = file_name + ".pdf";

        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();

        if (!response.isSuccessful()) {
            throw new IOException("Failed to download file: " + response);

        } else {

            message="Download Successful ";
        }
        InputStream inputStream = response.body().byteStream();
        byte[] buffer = new byte[1024];
//        buffer = response.body().bytes();
        int bufferRead;
        FileOutputStream fos = new FileOutputStream(file);
        while ((bufferRead = inputStream.read(buffer)) != -1) {
            fos.write(buffer, 0, bufferRead);

        }

        fos.flush();
        fos.close();
        return message;
    }
    public static String downloadFileFromeUrlBase64(String urlpath, JSONObject json, Context contex, String file_name) throws Exception {
        String message;
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .build();

        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(urlpath)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println("Response:: " + response.message());

        String root =Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath();
        File myDir = new File(root + "/qpay");
        myDir.mkdirs();
        String fname = file_name + ".pdf";

        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();

        if (!response.isSuccessful()) {
            throw new IOException("Failed to download file: " + response);

        } else {

            message="Download Successful ";
        }
        byte[] pdfAsBytes = Base64.decode(response.body().string(), 0);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(pdfAsBytes);
        fos.flush();
        fos.close();
        return message;
    }
    public static String sendHTTPDataPOST(String urlpath, JSONObject json) {
        String TAG = "dinesh";
        Log.d(TAG, urlpath + "   " + json);
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        try {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .build();

            RequestBody body = RequestBody.create(JSON, json.toString());
            Request request = new Request.Builder()
                    .url(urlpath)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}