package net.qpaysolutions.QPay.BankDeposite;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

/**
 * Created by deadlydragger on 2/7/17.
 */

public class BankDepositOTPFragment extends Fragment implements View.OnClickListener {
    EditText get_otp;
    Button send_otp;
    ProgressBar counterProgressBar;
    MyCountDownTimer myCountDownTimer;
    TextView time_remaining_textview;
    QPayCustomerDatabase qpayMerchantDatabase;
    String app_id, term_id, amount, merchBankAccId;
    String account_id, _id, img_url, bank_name, branch, holder_name, acc_no;
    String Crrn, Stan,Otp;
    QPayProgressDialog qPayProgressDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.deposit_with_otp, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((WithDrawablActivity)getActivity()).setTitleToolbar("Bank OTP");
        _id = getArguments().getString("_id");
        Crrn = getArguments().getString("Crrn");
        Stan = getArguments().getString("Stan");
        Otp=getArguments().getString("Otp");
        amount=getArguments().getString("amount");
        get_otp = (EditText) view.findViewById(R.id.get_otp);
        send_otp = (Button) view.findViewById(R.id.send_otp);
        time_remaining_textview = (TextView) view.findViewById(R.id.time_remaining_textview);
        setKeyboardFocus(get_otp);

        String time = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(5 * 60 * 1000),
                TimeUnit.MILLISECONDS.toSeconds(5 * 60 * 1000) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(5 * 60 * 1000))
        );
        time_remaining_textview.setText(time);
        counterProgressBar = (ProgressBar) view.findViewById(R.id.counter);
        counterProgressBar.setProgress(0);
        myCountDownTimer = new MyCountDownTimer(5 * 60 * 1000, 1000);
        myCountDownTimer.start();

        ImageView account_logo = (ImageView) view.findViewById(R.id.account_logo);
        TextView account_bank_name = (TextView) view.findViewById(R.id.account_bank_name);
        TextView acc_holder_name = (TextView) view.findViewById(R.id.acc_holder_name);
        TextView account_number = (TextView) view.findViewById(R.id.account_number);
        TextView acc_branch = (TextView) view.findViewById(R.id.acc_branch);
        img_url = qpayMerchantDatabase.getImage(_id);
        branch = qpayMerchantDatabase.getAccountBranch(_id);
        bank_name = qpayMerchantDatabase.getAccountBank(_id);
        holder_name = qpayMerchantDatabase.getAccountHolder(_id);
        acc_no = qpayMerchantDatabase.getAccountNumber(_id);
        merchBankAccId = qpayMerchantDatabase.getKeyAccountId(_id);
        Picasso.get()
                .load(img_url)
                .placeholder(R.drawable.ic_sct_card)
                .error(R.drawable.ic_sct_card)
                .noFade()
                .into(account_logo);
        account_bank_name.setText(bank_name);
        acc_branch.setText(branch);
        acc_holder_name.setText(holder_name);
        String substring = acc_no.substring(Math.max(acc_no.length() - 4, 0));
        account_number.setText("XXXX-XXXX-XXXX-" + substring);
        send_otp.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_otp:
                String otp = get_otp.getText().toString();
                if (Otp.equals(otp)){
                    new RqdOtp(otp).execute();
                }

                break;
        }
    }
    public void vibration() {
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
    }
    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int progress = (int) (((5 * 60 * 1000 - millisUntilFinished) * 100) / (5 * 60 * 1000));
            counterProgressBar.setProgress(progress);
            String time = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
            );
            time_remaining_textview.setText(time);
        }
        @Override
        public void onFinish() {
            time_remaining_textview.setText("00:00");
            counterProgressBar.setProgress(100);

            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment,new AccountListingFragment())
                    .commit();

        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getActivity());
        qpayMerchantDatabase.getReadableDatabase();
        qpayMerchantDatabase.getWritableDatabase();
        app_id = qpayMerchantDatabase.getKeyAppId();
        term_id = qpayMerchantDatabase.getCustomerID();
    }
    public static void setKeyboardFocus(final EditText primaryTextField) {
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                primaryTextField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                primaryTextField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                primaryTextField.setSelection(primaryTextField.getText().length());
            }
        }, 100);
    }

    public class RqdOtp extends AsyncTask<String, String, String> {
        String otp;

        public RqdOtp(String otp) {
            this.otp = otp;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String otp_response = "";
            try {
                jsonObject.put(Tags.APPID_S, app_id);
                jsonObject.put(Tags.TERMID_S, term_id);
                jsonObject.put("crrn", Crrn);
                jsonObject.put("stan", Stan);
                jsonObject.put("otp", otp);
                jsonObject.put("bankAccId",merchBankAccId);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "post otp : " + jsonObject);
                otp_response = httpUrlConnectionPost.sendHTTPData(Constants.DEPOSIT_OTP, jsonObject);
                Log.d(Utility.TAG, "response otp : " + otp_response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return otp_response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject ConfirmBankDepositResult = jsonObject.getJSONObject("ConfirmBankDepositResult");
                boolean success = ConfirmBankDepositResult.getBoolean("success");
                if (success)
                {
                    new Ackclass(app_id,term_id,Stan,Crrn).execute();
                    vibration();
                    custumdialogSucess("Bank Deposit Initiated!","Bank Deposit for amount NPR " + amount + " has been initiated. We will notify once it is completed. Thank you for using QPay");
                }else {
                    myCountDownTimer.cancel();
                    custumdialogfailure("Deposit Failure!", "Your amount NPR " + amount + " is failed to deposit in " + bank_name + " " + branch + " branch.");
                }
            }catch (Exception e){
                e.printStackTrace();
                myCountDownTimer.cancel();
                custumdialogfailure("Deposit Failure!", "Your amount NPR " + amount + " is failed to deposit in " + bank_name + " " + branch + " branch.");
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }
    }
    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, term_id, stan,crrn;
        public Ackclass(String app_id, String term_id, String stan,String crrn) {
            this.app_id = app_id;
            this.term_id = term_id;
            this.stan = stan;
            this.crrn=crrn;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("term_id", term_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn",crrn);

                ack_test = networkAPI.sendHTTPData(Constants.ACKN, jsonObject);
                Log.d(Utility.TAG,"ack post : " + jsonObject + " response : " + ack_test);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
}
