package net.qpaysolutions.QPay.BankDeposite;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;

import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by deadlydragger on 1/17/17.
 */

public class AccountListingFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemLongClickListener {
    Button add_account, deposite_Account;
    ListView account_list;
    QPayCustomerDatabase qpayMerchantDatabase;
    Cursor cursor, getCursor,account_verification;
    SQLiteDatabase database;
    AccountAdapter accountAdapter;
    String app_id, term_id;
    QPayProgressDialog qPayProgressDialog;
    String account_date,available_balance;
    SharedPreferences sharedPreferences;
    static String MY_FILE_SHARED = "Mybalance";
    TextView information;
    LinearLayout empty_account;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_main, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((WithDrawablActivity)getActivity()).setTitleToolbar("Account List");
        sharedPreferences = getActivity().getSharedPreferences(MY_FILE_SHARED, Context.MODE_PRIVATE);
        available_balance=sharedPreferences.getString("avail_bal",null);
        add_account = (Button) view.findViewById(R.id.add_account);
        deposite_Account = (Button) view.findViewById(R.id.deposite_amount);
        account_list = (ListView) view.findViewById(R.id.account_list);
        information=(TextView)view.findViewById(R.id.information);
        empty_account=(LinearLayout)view.findViewById(R.id.empty_account);
        add_account.setOnClickListener(this);
        deposite_Account.setOnClickListener(this);
        accountAdapter = new AccountAdapter(getContext(), cursor, AccountListingFragment.this);
        accountAdapter.swapCursor(cursor);
        account_list.setAdapter(accountAdapter);
        account_list.setOnItemLongClickListener(this);
        if (getCursor.getCount() > 0) {
            Log.d(Utility.TAG, "counter : " + cursor.getCount());
            deposite_Account.setVisibility(View.VISIBLE);
        } else {
            deposite_Account.setVisibility(View.GONE);
        }
        if (cursor.getCount()>0){
            empty_account.setVisibility(View.GONE);
            information.setText("Bank Account Information");
        }else {
            account_list.setVisibility(View.GONE);
            empty_account.setVisibility(View.VISIBLE);
            information.setVisibility(View.GONE);
        }
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        qpayMerchantDatabase.getReadableDatabase();
        qpayMerchantDatabase.getWritableDatabase();
        database = qpayMerchantDatabase.getWritableDatabase();
        app_id = qpayMerchantDatabase.getKeyAppId();
        term_id = qpayMerchantDatabase.getCustomerID();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        String date = day + "/" + month + "/" + year;
        deletQuery(date);
        cursor = database.rawQuery(" select * from account ", null);
        getCursor = database.rawQuery(" select * from account where account.act_flag_account = 1 ", null);
        account_verification = database.rawQuery(" select * from account where account.act_flag_account = 0 ", null);
    }
    public String deletQuery(String date) {
        String selectQuery = " delete from account where account.account_date = " + date;
        return selectQuery;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_account:
                try {
                    if (account_verification.getCount() > 0){
                        new Utility().showSnackbar(v,"Please verify the pending account first.");
                    }else {
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragment, new AddAccountFragment())
                                .commit();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.deposite_amount:
                try {
                    if (Double.parseDouble(available_balance) >=1){
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragment, new DepositeAccountList())
                                .commit();
                    }else {
                        new Utility().showSnackbar(v,"Insufficient wallet amount for deposit.");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void onClickVerify(String acc_id, String _id) {
        try {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            VerifyAccountFragment verifyAccountFragment = new VerifyAccountFragment();

            fragmentTransaction.replace(R.id.fragment, verifyAccountFragment);
            Bundle bundle = new Bundle();
            bundle.putString("account_id", acc_id);
            bundle.putString("_id", _id);
            verifyAccountFragment.setArguments(bundle);
            fragmentTransaction.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    public void custumDialogDeletAccount(final String id, final String merchBankAccId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_delet_account, null);
        final TextView sure = (TextView) v.findViewById(R.id.sure);
        final TextView content = (TextView) v.findViewById(R.id.content);
        final TextView content_text = (TextView) v.findViewById(R.id.content_text);
        final ImageView clear_dialog = (ImageView) v.findViewById(R.id.clear_dialog);
        content_text.setText("");
        sure.setText("Are You Sure ?");
        content.setText("Are you sure to delete the selected bank account details? This action can not be undone.");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        v.findViewById(R.id.clear_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                new DeleteMerchantBankAccount(app_id, term_id, merchBankAccId, ((CustomerApplication) getActivity().getApplication()).getLat(), ((CustomerApplication) getActivity().getApplication()).getLng(), id).execute();


            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        String merchBankAccId = qpayMerchantDatabase.getKeyAccountId(String.valueOf(id));

        custumDialogDeletAccount(String.valueOf(id), merchBankAccId);
        return false;
    }
    public class DeleteMerchantBankAccount extends AsyncTask<String, String, String> {
        String appId, termId, merchBankAccId, id;
        double lat, lng;
        public DeleteMerchantBankAccount(String appId, String termId, String merchBankAccId, double lat, double lng, String id) {
            this.appId = appId;
            this.termId = termId;
            this.merchBankAccId = merchBankAccId;
            this.lat = lat;
            this.lng = lng;
            this.id = id;
        }
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String delet_card_response = "";
            try {
                jsonObject.put(Tags.APPID_S, appId);
                jsonObject.put(Tags.TERMID_S, termId);
                jsonObject.put(Tags.MERCHANT_BANK_ID, merchBankAccId);
                jsonObject.put(Tags.LAT, lat);
                jsonObject.put(Tags.LNG, lng);
                Log.d(Utility.TAG, "post delet account : " + jsonObject);
                delet_card_response = httpUrlConnectionPost.sendHTTPData(Constants.DELET_BANK_ACCOUNT, jsonObject);
                Log.d(Utility.TAG, "response from delet bank account : " + delet_card_response);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return delet_card_response;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject DeleteMerchantBankAccountResult = jsonObject.getJSONObject("DeleteBankAccountResult");
                boolean success = DeleteMerchantBankAccountResult.getBoolean(Tags.SUCESS);
                if (success) {
                    qpayMerchantDatabase.deletAccount(id);
                    Cursor newCursor = database.rawQuery(" select * from account ", null);
                    accountAdapter.changeCursor(newCursor);
                    accountAdapter.notifyDataSetChanged();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, new AccountListingFragment())
                            .commit();
                } else {
                       new Utility().showSnackbar(empty_account,"Sorry!Unable to delete the Bank Account.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                  startActivity(new Intent(getActivity(),WithDrawablActivity.class));
                    getActivity().finish();

                    return true;
                }
                return false;
            }
        });
    }


}
