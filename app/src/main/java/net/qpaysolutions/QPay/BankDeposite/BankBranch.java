package net.qpaysolutions.QPay.BankDeposite;

/**
 * Created by deadlydragger on 1/20/17.
 */

public class BankBranch {
    String BranchName,Id;

    public String getId() {
        return Id;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setId(String id) {
        Id = id;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }
}
