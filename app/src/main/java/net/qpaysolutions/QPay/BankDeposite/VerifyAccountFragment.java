package net.qpaysolutions.QPay.BankDeposite;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlydragger on 1/18/17.
 */

public class VerifyAccountFragment extends Fragment implements View.OnClickListener {
    TextView amount;
    String account_id, _id, img_url, bank_name, branch, holder_name, acc_no;
    ImageView account_logo;
    TextView account_bank_name, acc_branch, acc_holder_name, account_number;
    QPayCustomerDatabase qpayMerchantDatabase;
    ImageView account_status;
    String app_id, term_id, merchBankAccId;
    List<String> verify_amount;
    QPayProgressDialog qPayProgressDialog;
    String account;
    ArrayList<String> amountArray = new ArrayList<>();
    Button verify_account;
    String verify_amount_selected="";
    String try_counter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getActivity());
        qpayMerchantDatabase.getReadableDatabase();
        qpayMerchantDatabase.getWritableDatabase();
        app_id = qpayMerchantDatabase.getKeyAppId();
        term_id = qpayMerchantDatabase.getCustomerID();
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Verify Account");
       /* ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);
        ((MainActivity) getActivity()).setRightIcon(true);*/
        amount = (TextView) view.findViewById(R.id.amount);
        account_id = getArguments().getString("account_id");
        _id = getArguments().getString("_id");
        account_logo = (ImageView) view.findViewById(R.id.account_logo);
        account_bank_name = (TextView) view.findViewById(R.id.account_bank_name);
        acc_branch = (TextView) view.findViewById(R.id.acc_branch);
        acc_holder_name = (TextView) view.findViewById(R.id.acc_holder_name);
        account_number = (TextView) view.findViewById(R.id.account_number);
        account_status = (ImageView) view.findViewById(R.id.account_status);
        verify_account=(Button)view.findViewById(R.id.verify_account);
        account_status.setImageResource(R.drawable.ic_unverified_bank_account);

        img_url = qpayMerchantDatabase.getImage(_id);
        branch = qpayMerchantDatabase.getAccountBranch(_id);
        bank_name = qpayMerchantDatabase.getAccountBank(_id);
        holder_name = qpayMerchantDatabase.getAccountHolder(_id);
        acc_no = qpayMerchantDatabase.getAccountNumber(_id);
        merchBankAccId = qpayMerchantDatabase.getKeyAccountId(_id);
        account= qpayMerchantDatabase.getAmount(String.valueOf(_id));
        Log.d(Utility.TAG,"amount list : " +account );
        changeToList(account);
        Picasso.get()
                .load(img_url)
                .placeholder(R.drawable.ic_default_bank_account)
                .error(R.drawable.ic_default_bank_account)
                .noFade()
                .into(account_logo);
        account_bank_name.setText(bank_name);
        acc_branch.setText(branch);
        acc_holder_name.setText(holder_name);
        String substring = acc_no.substring(Math.max(acc_no.length() - 4, 0));
        account_number.setText("XXXX-XXXX-XXXX-" + substring);
        amount.setOnClickListener(this);
        verify_account.setOnClickListener(this);


//        new GetDepositAmount(app_id, term_id, merchBankAccId, _id).execute();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.verify_accountn, container, false);
    }
    public void vibration() {
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
    }
    public static void setKeyboardFocus(final EditText primaryTextField) {
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                primaryTextField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                primaryTextField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                primaryTextField.setSelection(primaryTextField.getText().length());
            }
        }, 100);
    }
public void changeToList(String value){
    try {
        JSONArray jsonArray = new JSONArray(value);


        for (int i = 0; i < jsonArray.length(); i++) {
            amountArray.add(jsonArray.getString(i));

        }
    }catch (Exception e){
        e.printStackTrace();
    }

}
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.amount:
                final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getContext(), new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int month, String dateDesc) {
                        Log.d(Utility.TAG, "amount selected : " + dateDesc);
                        verify_amount_selected=dateDesc;
                        amount.setText(dateDesc);
                    }
                }).textConfirm("DONE")
                        .textCancel("CANCEL")
                        .btnTextSize(16)
                        .viewTextSize(25)
                        .setValues(amountArray)
                        .build();
                pickerPopWin.showPopWin(getActivity());
                break;
            case R.id.verify_account:
                try {
                    if (Double.parseDouble(verify_amount_selected) > 0){
                        try_counter=qpayMerchantDatabase.getAccountRetryCounter(_id);
                        new VerifyBankAccount(verify_amount_selected).execute();
                    }else {
                        Toast.makeText(getContext(),"Select Amount to Verify.",Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getContext(),"Select Amount to Verify.",Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    public class VerifyBankAccount extends AsyncTask<String, String, String> {
        String amount;

        public VerifyBankAccount(String amount) {
            this.amount = amount;
        }
        @Override
        protected String doInBackground(String... params) {
            String verify_account = "";
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Tags.APPID_S, app_id);
                jsonObject.put(Tags.TERMID_S, term_id);
                jsonObject.put(Tags.MERCHANT_BANK_ID, merchBankAccId);
                jsonObject.put(Tags.AMOUNT, amount);
                jsonObject.put(Tags.LAT, ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put(Tags.LNG, ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG,"post verification : " + jsonObject);
                verify_account = httpUrlConnectionPost.sendHTTPData(Constants.VERIFY_ACCOUNT, jsonObject);
                Log.d(Utility.TAG,"response verification : " + verify_account);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return verify_account;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {

                JSONObject jsonObject = new JSONObject(s);
                JSONObject VerifyBankAccountResult = jsonObject.getJSONObject("VerifyBankAccountResult");
                boolean success = VerifyBankAccountResult.getBoolean(Tags.SUCESS);
                if (success){
                    JSONArray data = VerifyBankAccountResult.getJSONArray("data");
                    JSONObject jsonObject1 = data.getJSONObject(0);
                    String Crrn = jsonObject1.getString("Crrn");
                    String Stan = jsonObject1.getString("Stan");
                    qpayMerchantDatabase.updateAccountFlag("1",_id);
                    new Ackclass(app_id,term_id,Stan,Crrn).execute();
                    vibration();
                    custumdialogSucess("Bank Verification Success!","Your request for Bank Verification is completed successfully.");
                }else {
                    if (try_counter.equals("0")){
                        qpayMerchantDatabase.updateAccountTry("1",_id);
                        custumdialogfailure("Bank Verification Failure!","The transaction has been declined because of verification amount mismatch. The verification amount provided does not match verification amount sent to Bank Account.");
                    }else if (try_counter.equals("1")){
                        qpayMerchantDatabase.deletAccount(_id);
                        new DeleteMerchantBankAccount(app_id, term_id, merchBankAccId, ((CustomerApplication) getActivity().getApplication()).getLat(), ((CustomerApplication) getActivity().getApplication()).getLng(), _id).execute();

                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
                custumdialogfailure("Bank Verification Failure!","The transaction has been declined because of verification amount mismatch. The verification amount provided does not match verification amount sent to Bank Account.");

            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }
    }
    public class DeleteMerchantBankAccount extends AsyncTask<String, String, String> {
        String appId, termId, merchBankAccId, id;
        double lat, lng;

        public DeleteMerchantBankAccount(String appId, String termId, String merchBankAccId, double lat, double lng, String id) {
            this.appId = appId;
            this.termId = termId;
            this.merchBankAccId = merchBankAccId;
            this.lat = lat;
            this.lng = lng;
            this.id = id;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String delet_card_response = "";
            try {
                jsonObject.put(Tags.APPID_S, appId);
                jsonObject.put(Tags.TERMID_S, termId);
                jsonObject.put(Tags.MERCHANT_BANK_ID, merchBankAccId);
                jsonObject.put(Tags.LAT, lat);
                jsonObject.put(Tags.LNG, lng);
                Log.d(Utility.TAG, "post delet account : " + jsonObject);
                delet_card_response = httpUrlConnectionPost.sendHTTPData(Constants.DELET_BANK_ACCOUNT, jsonObject);
                Log.d(Utility.TAG, "response from delet bank account : " + delet_card_response);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return delet_card_response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {

                JSONObject jsonObject = new JSONObject(s);
                JSONObject DeleteMerchantBankAccountResult = jsonObject.getJSONObject("DeleteBankAccountResult");
                boolean success = DeleteMerchantBankAccountResult.getBoolean(Tags.SUCESS);
                if (success) {
                    custumdialogfailure("Maximum retry reached!","You have reached the limit on the number of attempts allowed and your bank account has been deleted. Please add your account and try again. Thank you.");
                } else {
                    custumdialogfailure("Bank Verification Failure!","The transaction has been declined because of verification amount mismatch. The verification amount provided does not match verification amount sent to Bank Account.");
                }

            } catch (Exception e) {
                e.printStackTrace();
                custumdialogfailure("Bank Verification Failure!","The transaction has been declined because of verification amount mismatch. The verification amount provided does not match verification amount sent to Bank Account.");

            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();

        }
    }
    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView)v.findViewById(R.id.dialog_icon);
        dialog_icon.setImageResource(R.drawable.ic_bank_verification_success);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final  ImageView dialog_image = (ImageView)v.findViewById(R.id.dialog_image);
        dialog_image.setImageResource(R.drawable.ic_bank_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, term_id, stan,crrn;
        public Ackclass(String app_id, String term_id, String stan,String crrn) {
            this.app_id = app_id;
            this.term_id = term_id;
            this.stan = stan;
            this.crrn=crrn;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", term_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn",crrn);

                ack_test = networkAPI.sendHTTPData(Constants.ACKN, jsonObject);
                Log.d(Utility.TAG,"ack post : " + jsonObject + " response : " + ack_test);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    AccountListingFragment najirEnglish = new AccountListingFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();


                    return true;
                }
                return false;
            }
        });
    }
}
