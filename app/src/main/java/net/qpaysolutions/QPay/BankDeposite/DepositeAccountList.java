package net.qpaysolutions.QPay.BankDeposite;

import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 1/24/17.
 */

public class DepositeAccountList extends Fragment implements AdapterView.OnItemClickListener {
    ListView account_list;
    AccountDepositAdapter accountAdapter;
    QPayCustomerDatabase qpayMerchantDatabase;
    Cursor cursor,getCursor;
    SQLiteDatabase database;
    String app_id,term_id;
    QPayProgressDialog qPayProgressDialog ;
    boolean pending;
    String DepositBalance;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        qpayMerchantDatabase.getReadableDatabase();
        qpayMerchantDatabase.getWritableDatabase();
        database = qpayMerchantDatabase.getWritableDatabase();
        app_id=qpayMerchantDatabase.getKeyAppId();
        term_id=qpayMerchantDatabase.getCustomerID();
        cursor = database.rawQuery(" select * from account where account.act_flag_account = 1 ", null);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((WithDrawablActivity)getActivity()).setTitleToolbar("Bank Deposit");
        account_list=(ListView)view.findViewById(R.id.account_list) ;
        accountAdapter = new AccountDepositAdapter(getContext(), cursor, DepositeAccountList.this);
        accountAdapter.swapCursor(cursor);
        account_list.setAdapter(accountAdapter);
        account_list.setOnItemClickListener(this);
        new CheckHoldaingAmount().execute();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.deposite_list_account,container,false);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(Utility.TAG,"position : " + id);
        if (pending==false){
            DepositeAmountFragment depositeAmountFragment = new DepositeAmountFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment,depositeAmountFragment);
            Bundle bundle = new Bundle();
            bundle.putString("id",String.valueOf(id));
            depositeAmountFragment.setArguments(bundle);
            fragmentTransaction.commit();
        }else {
            custumdialogfailure("Deposit is in progress!","Previous deposit with amount NPR "+DepositBalance +"is in progress. Once previous deposit gets completed, you can initiate another deposit.");
        }
    }
    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_image = (ImageView) v.findViewById(R.id.dialog_image);
        dialog_image.setImageResource(R.drawable.ic_bank_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public class  CheckHoldaingAmount extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String pending_response="";
            try {
              jsonObject.put(Tags.APPID_S,app_id);
                jsonObject.put(Tags.TERMID_S,term_id);
                Log.d(Utility.TAG,"pending post : "+ jsonObject);
                pending_response=httpUrlConnectionPost.sendHTTPData(Constants.DEPOSIT_HOLD_AMOUNT,jsonObject);
                Log.d(Utility.TAG,"pending response : "+ pending_response);

            }catch (Exception e){
                e.printStackTrace();
            }
            return pending_response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog= new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject CheckIpsTxnStatusResult = jsonObject.getJSONObject("CheckIpsTxnStatusResult");
                JSONArray data = CheckIpsTxnStatusResult.getJSONArray("data");
                boolean success = CheckIpsTxnStatusResult.getBoolean("success");
                if (success){
                    JSONObject jsonObject1 = data.getJSONObject(0);
                    pending = jsonObject1.getBoolean("IsPendingTxn");
                    if (pending){
                        DepositBalance=jsonObject1.getString("DepositBalance");
                        custumdialogfailure("Deposit is in progress!","Previous deposit with amount NPR "+DepositBalance +" is in progress. Once previous deposit gets completed, you can initiate another deposit.");
                    }
                }else {

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    AccountListingFragment najirEnglish = new AccountListingFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();


                    return true;
                }
                return false;
            }
        });
    }
}
