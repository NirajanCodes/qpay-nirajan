package net.qpaysolutions.QPay.BankDeposite;

/**
 * Created by deadlydragger on 1/20/17.
 */

public class Bank {
    String Id,ImageUrl,InstitutionName;

    public String getId() {
        return Id;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getInstitutionName() {
        return InstitutionName;
    }

    public void setId(String id) {
        Id = id;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public void setInstitutionName(String institutionName) {
        InstitutionName = institutionName;
    }
}
