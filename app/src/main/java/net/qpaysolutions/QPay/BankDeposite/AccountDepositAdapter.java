package net.qpaysolutions.QPay.BankDeposite;

import android.content.Context;
import android.database.Cursor;
import androidx.cursoradapter.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;


/**
 * Created by deadlydragger on 1/24/17.
 */

public class AccountDepositAdapter extends CursorAdapter {

    DepositeAccountList accountListingFragment;

    public AccountDepositAdapter(Context context, Cursor c, DepositeAccountList accountListingFragment) {
        super(context, c);
        this.accountListingFragment = accountListingFragment;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.account_adapter_deposit_list, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView account_logo = (ImageView) view.findViewById(R.id.account_logo);
        TextView account_bank_name = (TextView) view.findViewById(R.id.account_bank_name);
        ImageView account_status = (ImageView) view.findViewById(R.id.account_status);
        TextView acc_holder_name = (TextView) view.findViewById(R.id.acc_holder_name);
        TextView account_number = (TextView) view.findViewById(R.id.account_number);
        TextView acc_branch = (TextView) view.findViewById(R.id.acc_branch);
        final String account_name = cursor.getString(cursor.getColumnIndexOrThrow("account_name"));
        final String account_no = cursor.getString(cursor.getColumnIndexOrThrow("account_no"));
        final String act_flag_account = cursor.getString(cursor.getColumnIndexOrThrow("act_flag_account"));
        final String account_img = cursor.getString(cursor.getColumnIndexOrThrow("account_img"));
        final String account_bank = cursor.getString(cursor.getColumnIndexOrThrow("account_bank"));
        final String account_bank_branch = cursor.getString(cursor.getColumnIndexOrThrow("account_bank_branch"));
        final String account_id = cursor.getString(cursor.getColumnIndexOrThrow("account_id"));
        final String _id = cursor.getString(cursor.getColumnIndex("_id"));
        String substring = account_no.substring(Math.max(account_no.length() - 4, 0));
        Picasso.get()
                .load(account_img)
                .placeholder(R.drawable.ic_default_bank_account)
                .error(R.drawable.ic_default_bank_account)
                .noFade()
                .into(account_logo);
        account_bank_name.setText(account_bank);
        account_status.setImageResource(R.drawable.ic_verified_bank_account);
        acc_holder_name.setText(account_name);
        account_number.setText("XXXX-XXXX-XXXX-" + substring);
        acc_branch.setText(account_bank_branch);
//        verify_account.setVisibility(View.GONE);
        /*verify_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountListingFragment.onClickVerify(account_id,_id);
            }
        });*/

    }
}
