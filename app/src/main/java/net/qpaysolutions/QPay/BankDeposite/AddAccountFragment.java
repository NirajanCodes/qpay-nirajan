package net.qpaysolutions.QPay.BankDeposite;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import static net.qpaysolutions.QPay.Utils.Utility.TAG;

/**
 * Created by deadlydragger on 7/6/16.
 */
public class AddAccountFragment extends Fragment implements View.OnClickListener {

    private EditText bank_account, bank_accno_confirm;
    private String token, term_id;
    private Button deposite;
    LinearLayout deposite_layout;
    private ArrayList<Bank> banknamelist = new ArrayList<>();
    private ArrayList<BankBranch> bankBranchArrayList = new ArrayList<>();
    private ArrayList<String> banknamelistBank = new ArrayList<>();
    private ArrayList<String> banknamelistBankBranch = new ArrayList<>();

    private String selected_bank, select_brach;
    String positionBank, positionBankBranch;
    String app_id;

    QPayCustomerDatabase qpayMerchantDatabase;
    QPayProgressDialog qPayProgressDialog;
    TextView bank_name, bank_branch;
    EditText bank_amount_name;
    String acc_no, acc_bank, acc_branch, acc_holder_name;
    int count;
    Calendar calendar;
    String try_counter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        qpayMerchantDatabase.getReadableDatabase();
        token = ((CustomerApplication) getActivity().getApplication()).getDevtokenVariable();
        term_id = qpayMerchantDatabase.getCustomerID();
        app_id = qpayMerchantDatabase.getKeyAppId();

        new GetBankname(app_id, term_id).execute();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_account, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((WithDrawablActivity)getActivity()).setTitleToolbar("Add Account");
        bank_account = (EditText) view.findViewById(R.id.bank_accno);

        bank_branch = (TextView) view.findViewById(R.id.bank_branch);
        deposite = (Button) view.findViewById(R.id.bank_deposit);
        deposite_layout = (LinearLayout) view.findViewById(R.id.deposite);
        bank_accno_confirm = (EditText) view.findViewById(R.id.bank_accno_confirm);
        bank_name = (TextView) view.findViewById(R.id.bank_name);
        bank_name.setOnClickListener(this);
        bank_amount_name =
                (EditText) view.findViewById(R.id.bank_amount_name);
        deposite.setOnClickListener(this);
        bank_branch.setOnClickListener(this);
    }
    public void showBankList() {
        bank_branch.setText("");
        positionBankBranch="";
        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getContext(), new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int month, String dateDesc) {

                selected_bank = dateDesc;
                bank_name.setText(dateDesc);
                Log.d(TAG, "selected bank : " + selected_bank);
                for (int ii = 0; ii < banknamelist.size(); ii++) {
                    Log.d(TAG, "onDatePickCompleted: seleted: " + selected_bank + " picked name : " + banknamelist.get(ii).getInstitutionName());
                    if (selected_bank.equals(banknamelist.get(ii).getInstitutionName())) {
                        Log.d(TAG, "position : " + banknamelist.get(ii).getId());
                        positionBank = banknamelist.get(ii).getId();
                        new GetBankBranch(app_id, term_id, banknamelist.get(ii).getId()).execute();
                    }
                }

            }
        }).textConfirm("DONE")
                .textCancel("CANCEL")
                .btnTextSize(16)
                .viewTextSize(25)
                .setValues(banknamelistBank)
                .build();
        pickerPopWin.showPopWin(getActivity());

    }

    public void showBankBranchList() {
        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getContext(), new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int month, String dateDesc) {
                bank_branch.setText(dateDesc);
                select_brach = dateDesc;
                Log.d(Utility.TAG, "selected bank branch: " + select_brach);
                for (int ii = 0; ii < bankBranchArrayList.size(); ii++) {
                    if (select_brach.equals(bankBranchArrayList.get(ii).getBranchName())) {
                        Log.d(Utility.TAG, "position branch : " + bankBranchArrayList.get(ii).getId());
                        positionBankBranch = bankBranchArrayList.get(ii).getId();
                    }
                }

            }
        }).textConfirm("DONE")
                .textCancel("CANCEL")
                .btnTextSize(16)
                .viewTextSize(25)
                .setValues(banknamelistBankBranch)
                .build();
        pickerPopWin.showPopWin(getActivity());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bank_deposit:
                try {
                    acc_no = bank_accno_confirm.getText().toString();
                    acc_holder_name = bank_amount_name.getText().toString();
                    acc_bank = selected_bank;
                    acc_branch = select_brach;
                    if (acc_no != null && !acc_no.isEmpty() && !bank_amount_name.getText().toString().isEmpty() && bank_amount_name.getText().toString() != null && bank_account.getText().toString() != null && !bank_account.getText().toString().isEmpty() && !positionBank.isEmpty() && !positionBankBranch.isEmpty())
                    {
                        count = qpayMerchantDatabase.checkAccNo(acc_no,selected_bank);
                    }else {
                        new Utility().showSnackbar(v, "Insert required information");
                    }
                    if (bank_accno_confirm.getText().toString().equals(bank_account.getText().toString())) {
                        if (count > 0) {
                            custumdialogfailure("Account Already Registered!", "Your account is already registered.");
                        } else {
                            if (bank_amount_name.getText().toString() != null&& !bank_amount_name.getText().toString().isEmpty() && positionBank != null && !positionBank.isEmpty() && bank_account.getText().toString() != null && !bank_account.getText().toString().isEmpty() && positionBankBranch != null && !positionBankBranch.isEmpty()){
                                new MerchantBankDeposit(bank_amount_name.getText().toString(), positionBank, bank_account.getText().toString(), positionBankBranch).execute();
                            }else {
                                new Utility().showSnackbar(v, "Insert required information");
                            }
                        }
                    } else {
                        bank_accno_confirm.setError("Account no. mismatch");
                        bank_account.setError("Account no. mismatch");
                    }
                }catch (Exception e){
                    new Utility().showSnackbar(v, "Insert required information");
                    e.printStackTrace();
                }
                break;
            case R.id.bank_name:
                showBankList();
                break;
            case R.id.bank_branch:
                if (positionBank != null && !positionBank.isEmpty()) {
                    showBankBranchList();
                } else {
                    new Utility().showSnackbar(v, "select bank name first");
                }
                break;
        }
    }
    private class MerchantBankDeposit extends AsyncTask<Void, Void, String> {
        String acc_nam, institutionId, accountNumber, branchId;

        private MerchantBankDeposit(String acc_nam, String institutionId, String accountNumber, String branchId) {
            this.acc_nam = acc_nam;
            this.institutionId = institutionId;
            this.accountNumber = accountNumber;
            this.branchId = branchId;
        }

        @Override
        protected void onPreExecute() {
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String deposite_result = null;
            JSONObject jsonObject;
            JSONObject Response = null;
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            try {
                jsonObject = new JSONObject();
                jsonObject.put(Tags.APPID_S, app_id);
                jsonObject.put(Tags.TERMID_S, term_id);
                jsonObject.put(Tags.ACCOUNT_NAME_S, acc_nam);
                jsonObject.put(Tags.ACCOUNT_NUMBER_S, accountNumber);
                jsonObject.put(Tags.INSTITUTE_ID_S, institutionId);
                jsonObject.put(Tags.BRANCH_ID_S, branchId);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                String data = jsonObject.toString();
                deposite_result = httpUrlConnectionPost.sendHTTPData(Constants.BANK_DEPOSITE, jsonObject);
                Log.d(TAG, "result deposit_result  :" + deposite_result);
                Log.d(TAG, "post  deposit :" + jsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
            return deposite_result;
        }

        @Override
        protected void onPostExecute(String args) {
            super.onPostExecute(args);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(args);
                JSONObject jsonObject1 = jsonObject.getJSONObject("PostBankAccResult");
                Boolean success = jsonObject1.getBoolean(Tags.SUCESS);
                if (success) {
                    JSONArray data = jsonObject1.getJSONArray("data");
                    JSONObject jsonObject2 = data.getJSONObject(0);
                    String Id = jsonObject2.getString(Tags.ID_S);
                    JSONArray Amount = jsonObject2.getJSONArray("Amount");
                    String img_url = jsonObject2.getString("ImageUrl");
                    calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_MONTH, 3);
                    int insertday = calendar.get(Calendar.DAY_OF_MONTH);
                    int month = calendar.get(Calendar.MONTH);
                    int year = calendar.get(Calendar.YEAR);
                    String addded = insertday + "/" + month + "/" + year;
                    Log.d(TAG, " rare date : " + addded);
                    qpayMerchantDatabase.insertAccount(acc_holder_name, acc_no, acc_bank, acc_branch, img_url, "0", Id, Amount.toString(), addded, "0");
                    custumdialogSucess("Bank Account added Successfully!", "Your Bank Account has been added successfully. Please verify the account in order to proceed with deposit.");

                } else {
                    custumdialogfailure("Account Registration Failed!", "Your " + acc_bank + " account ending with account no. " + acc_no + " failed to register.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                custumdialogfailure("Account Registration Failed!", "Your " + acc_bank + " account ending with account no. " + acc_no + " failed to register.");
            }
        }
    }

    private class GetBankname extends AsyncTask<Void, Void, String> {
        String app_id, term_id;

        private GetBankname(String app_id, String term_id) {
            this.app_id = app_id;
            this.term_id = term_id;
        }

        @Override
        protected void onPreExecute() {
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String deposite_result = null;
            JSONObject jsonObject;
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            try {
                jsonObject = new JSONObject();
                jsonObject.put(Tags.APPID_S, app_id);
                jsonObject.put(Tags.TERMID_S, term_id);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());

                deposite_result = httpUrlConnectionPost.sendHTTPData(Constants.GET_PARTICIPANTS, jsonObject);
                Log.d(TAG, "device toke :" + token);
                Log.d(TAG, "result get bank merchant  :" + deposite_result);
                Log.d(TAG, "post  get bank merchant :" + jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return deposite_result;
        }

        @Override
        protected void onPostExecute(String args) {
            super.onPostExecute(args);
            qPayProgressDialog.dismiss();
            try {

                JSONObject jsonObject_main = new JSONObject(args);
                JSONObject bankname = jsonObject_main.getJSONObject("GetIpsParticipantsResult");
                JSONArray jsonArray = bankname.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Bank bank = new Bank();
                    JSONObject BankName = jsonArray.getJSONObject(i);
                    if (BankName.getString(Tags.INSTITUTION_S).length() > 30) {
                        bank.setInstitutionName(BankName.getString(Tags.INSTITUTION_S).substring(0, 30) + "..");
                        bank.setId(BankName.getString(Tags.ID_S));
                        banknamelistBank.add(BankName.getString(Tags.INSTITUTION_S).substring(0, 30) + "..");
                        banknamelist.add(bank);
                    } else {
                        bank.setInstitutionName(BankName.getString(Tags.INSTITUTION_S));
                        bank.setId(BankName.getString(Tags.ID_S));
                        banknamelistBank.add(BankName.getString(Tags.INSTITUTION_S));
                        banknamelist.add(bank);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class GetBankBranch extends AsyncTask<Void, Void, String> {
        String app_id, term_id, participantId;

        private GetBankBranch(String app_id, String term_id, String participantId) {
            this.app_id = app_id;
            this.term_id = term_id;
            this.participantId = participantId;
        }

        @Override
        protected void onPreExecute() {
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String deposite_result = null;
            JSONObject jsonObject;
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            try {
                jsonObject = new JSONObject();
                jsonObject.put(Tags.APPID_S, app_id);
                jsonObject.put(Tags.TERMID_S, term_id);
                jsonObject.put(Tags.PARCITICIPANTID_S, participantId);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(TAG, "post  get branch merchant :" + jsonObject.toString());
                deposite_result = httpUrlConnectionPost.sendHTTPData(Constants.GET_PARTICIPANTS_BRANCH, jsonObject);
                Log.d(TAG, "device toke :" + token);
                Log.d(TAG, "result get branch merchant  :" + deposite_result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return deposite_result;
        }

        @Override
        protected void onPostExecute(String args) {
            super.onPostExecute(args);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject_main = new JSONObject(args);
                JSONObject bankname = jsonObject_main.getJSONObject("GetIpsParticipantsBranchResult");
                JSONArray jsonArray = bankname.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject BankName = jsonArray.getJSONObject(i);
                    BankBranch bank = new BankBranch();

                    if (BankName.getString(Tags.BANKNAME_S).length() > 30) {
                        bank.setBranchName(BankName.getString(Tags.BANKNAME_S).substring(0, 30) + "..");
                        bank.setId(BankName.getString(Tags.ID_S));
                        bankBranchArrayList.add(bank);
                        banknamelistBankBranch.add(BankName.getString(Tags.BANKNAME_S).substring(0, 30) + "..");
                    } else {
                        bank.setBranchName(BankName.getString(Tags.BANKNAME_S));
                        bank.setId(BankName.getString(Tags.ID_S));
                        bankBranchArrayList.add(bank);
                        banknamelistBankBranch.add(BankName.getString(Tags.BANKNAME_S));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView) v.findViewById(R.id.dialog_icon);
        dialog_icon.setImageResource(R.drawable.ic_bank_verification_success);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_image = (ImageView) v.findViewById(R.id.dialog_image);
        dialog_image.setImageResource(R.drawable.ic_bank_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    AccountListingFragment najirEnglish = new AccountListingFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();


                    return true;
                }
                return false;
            }
        });
    }

    public void hideSoftkey(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
    }
}
