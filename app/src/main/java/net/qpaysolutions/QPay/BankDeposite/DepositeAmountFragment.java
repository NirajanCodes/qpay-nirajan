package net.qpaysolutions.QPay.BankDeposite;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deadlydragger on 1/18/17.
 */

public class DepositeAmountFragment extends Fragment implements View.OnClickListener {
    TextView merch_amount;
    SharedPreferences sharedPreferences;
    static String MY_FILE_SHARED = "Mybalance";
    String id;
    String account_id, _id, img_url, bank_name, branch, holder_name, acc_no;
    QPayCustomerDatabase qpayMerchantDatabase;
    TextView deposite_amount;
    List<String> depositAmount;
    String app_id, term_id, amount, merchBankAccId;
    Button confirm_deposit,cancel;
    QPayProgressDialog qPayProgressDialog;
    String max_deposit_amount;
    String balance_merch, available_balance;
    QPayProgressDialog progressDialog;
    double max_deposit;
    TextView text_tab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.deposite_amount, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((WithDrawablActivity)getActivity()).setTitleToolbar("Bank Deposit");
        _id = getArguments().getString("id");
        sharedPreferences = getActivity().getSharedPreferences(MY_FILE_SHARED, Context.MODE_PRIVATE);
        balance_merch = sharedPreferences.getString("avail_bal", null);
        max_deposit_amount = sharedPreferences.getString("max_deposit_amount", null);
        max_deposit=Double.parseDouble(max_deposit_amount);
        merch_amount = (TextView) view.findViewById(R.id.merch_amount);
        merch_amount.setText(balance_merch);
        ImageView account_logo = (ImageView) view.findViewById(R.id.account_logo);
        TextView account_bank_name = (TextView) view.findViewById(R.id.account_bank_name);
        TextView acc_holder_name = (TextView) view.findViewById(R.id.acc_holder_name);
        TextView account_number = (TextView) view.findViewById(R.id.account_number);
        TextView acc_branch = (TextView) view.findViewById(R.id.acc_branch);
        deposite_amount = (TextView) view.findViewById(R.id.deposite_amount);
        confirm_deposit = (Button) view.findViewById(R.id.confirm_deposit);
        text_tab=(TextView)view.findViewById(R.id.text_tab);
        cancel=(Button)view.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        img_url = qpayMerchantDatabase.getImage(_id);
        branch = qpayMerchantDatabase.getAccountBranch(_id);
        bank_name = qpayMerchantDatabase.getAccountBank(_id);
        holder_name = qpayMerchantDatabase.getAccountHolder(_id);
        acc_no = qpayMerchantDatabase.getAccountNumber(_id);
        merchBankAccId = qpayMerchantDatabase.getKeyAccountId(_id);
        Picasso.get()
                .load(img_url)
                .placeholder(R.drawable.ic_default_bank_account)
                .error(R.drawable.ic_default_bank_account)
                .noFade()
                .into(account_logo);
        account_bank_name.setText(bank_name);
        acc_branch.setText(branch);
        acc_holder_name.setText(holder_name);
        String substring = acc_no.substring(Math.max(acc_no.length() - 4, 0));
        account_number.setText("XXXX-XXXX-XXXX-" + substring);
        deposite_amount.setOnClickListener(this);
        confirm_deposit.setOnClickListener(this);
        new Getbalance().execute();


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getActivity());
        qpayMerchantDatabase.getReadableDatabase();
        qpayMerchantDatabase.getWritableDatabase();
        app_id = qpayMerchantDatabase.getKeyAppId();
        term_id = qpayMerchantDatabase.getCustomerID();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.deposite_amount:
                try {
                    double balance = Double.parseDouble(available_balance);
                    if (balance >= 500.00) {
                        selectDepositAmount();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case R.id.confirm_deposit:
                amount=deposite_amount.getText().toString();
                if (amount == null || amount.isEmpty()) {
                    new Utility().showSnackbar(v,"Select Valid Amount.");
                } else {
                    new DepositAmountToBank(app_id, term_id, amount, merchBankAccId).execute();
                }
                break;
            case R.id.cancel:
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment,new AccountListingFragment())
                        .commit();
                break;
        }
    }

    class Getbalance extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... uri) {
            String responseResult = null;
            String status = null;
            JSONObject jsonObject1 = null;
            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_balance.put("app_id", app_id);
                jsonObject_balance.put("cust_id", term_id);
                jsonObject_balance.put("lat", ((CustomerApplication)getActivity().getApplication()).getLat());
                jsonObject_balance.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "get balance from bill pay list : " + jsonObject_balance.toString());
                responseResult = networkAPI.sendHTTPData(Constants.GET_BALANCE_QPAY, jsonObject_balance);
                JSONObject jsonObject = new JSONObject(responseResult);
                jsonObject1 = jsonObject.getJSONObject("getbalanceResult");
                Log.d(Utility.TAG, "response result from call :" + responseResult);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return jsonObject1;
        }

        @Override
        protected void onPostExecute(JSONObject s) {
            super.onPostExecute(s);
            try {
                progressDialog.dismiss();
                String status = null;
                status = s.getString("status");
                available_balance = new Decimalformate().decimalFormate(s.getString("avail_bal"));
                double balance = Double.parseDouble(available_balance);
                if (balance < 500.00) {
                    deposite_amount.setText(available_balance);
                    text_tab.setText("Deposit amount");
                }else {
                    setArraySteps();
                }


            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public void setArraySteps() {
        String[] ncellRechargeValues = getResources().getStringArray(R.array.deposit_amount);
        depositAmount = new ArrayList<>(Arrays.asList(ncellRechargeValues));
        double balance = Double.parseDouble(available_balance);
       if (balance >= 500.00 && balance < 1000.00) {
            depositAmount.add(available_balance);
        } else if (balance >= 1000.00 && balance < 2000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add(available_balance);
        } else if (balance >= 2000.00 && balance < 5000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add(available_balance);
        } else if (balance >= 5000.00 && balance < 10000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add("5000.00");
            depositAmount.add(available_balance);
        } else if (balance >= 10000.00 && balance < 15000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add("5000.00");
            depositAmount.add("10000.00");
            depositAmount.add(available_balance);
        } else if (balance >= 15000.00 && balance < 20000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add("5000.00");
            depositAmount.add("10000.00");
            depositAmount.add("15000.00");
            depositAmount.add(available_balance);
        } else if (balance >= 20000.00 && balance < 30000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add("5000.00");
            depositAmount.add("10000.00");
            depositAmount.add("15000.00");
            depositAmount.add("20000.00");
            depositAmount.add(available_balance);
        } else if (balance >= 30000.00 && balance < 40000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add("5000.00");
            depositAmount.add("10000.00");
            depositAmount.add("15000.00");
            depositAmount.add("20000.00");
            depositAmount.add("30000.00");
            depositAmount.add(available_balance);
        } else if (balance >= 40000.00 && balance < 50000.00) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add("5000.00");
            depositAmount.add("10000.00");
            depositAmount.add("15000.00");
            depositAmount.add("20000.00");
            depositAmount.add("30000.00");
            depositAmount.add("40000.00");
            depositAmount.add(available_balance);
        } else if (balance >= max_deposit) {
//            depositAmount.add("500.00");
            depositAmount.add("1000.00");
            depositAmount.add("2000.00");
            depositAmount.add("5000.00");
            depositAmount.add("10000.00");
            depositAmount.add("15000.00");
            depositAmount.add("20000.00");
            depositAmount.add("30000.00");
            depositAmount.add("40000.00");
            depositAmount.add(max_deposit_amount);
        }
    }

    public void selectDepositAmount() {

        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int month, String dateDesc) {
                amount = dateDesc;
                deposite_amount.setText(dateDesc);
            }
        }).textConfirm("DONE")
                .textCancel("CANCEL")
                .btnTextSize(16)
                .viewTextSize(25)
                .setValues(depositAmount)
                .build();
        pickerPopWin.showPopWin(getActivity());

    }

    public class DepositAmountToBank extends AsyncTask<String, String, String> {
        String appId, termId, amount, merchBankAccId;

        public DepositAmountToBank(String appId, String termId, String amount, String merchBankAccId) {
            this.appId = appId;
            this.termId = termId;
            this.amount = amount;
            this.merchBankAccId = merchBankAccId;
        }

        @Override
        protected String doInBackground(String... params) {
            String deposite_response = "";
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Tags.APPID_S, appId);
                jsonObject.put(Tags.TERMID_S, termId);
                jsonObject.put(Tags.AMOUNT, amount);
                jsonObject.put(Tags.MERCHANT_BANK_ID, merchBankAccId);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "post deposit : " + jsonObject);
                deposite_response = httpUrlConnectionPost.sendHTTPData(Constants.DEPOSIT_TO_BANK, jsonObject);
                Log.d(Utility.TAG, "response deposit : " + deposite_response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return deposite_response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject DirectCrCreditorTxnInsertResult = jsonObject.getJSONObject("DirectCrCreditorTxnInsertResult");
                boolean success = DirectCrCreditorTxnInsertResult.getBoolean(Tags.SUCESS);
                if (success) {
                    JSONArray data = DirectCrCreditorTxnInsertResult.getJSONArray("data");
                    JSONObject jsonObject1 = data.getJSONObject(0);
                    String Crrn = jsonObject1.getString("Crrn");
                    boolean OtpStatus = jsonObject1.getBoolean("OtpStatus");
                    String Stan = jsonObject1.getString("Stan");
                    String Otp = jsonObject1.getString("Otp");
                    if (OtpStatus) {
                        BankDepositOTPFragment bankDepositOTPFragment = new BankDepositOTPFragment();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment, bankDepositOTPFragment);
                        Bundle bundle = new Bundle();
                        bundle.putString("Crrn", Crrn);
                        bundle.putString("Stan", Stan);
                        bundle.putString("Otp", Otp);
                        bundle.putString("_id", _id);
                        bundle.putString("amount",amount);
                        bankDepositOTPFragment.setArguments(bundle);
                        fragmentTransaction.commit();
                    } else {
                        new Ackclass(app_id, term_id, Stan, Crrn).execute();
                        vibration();
                        custumdialogSucess("Bank Deposit Success!","Bank Deposit for amount NPR " + amount + " has been initiated. We will notify once it is completed. Thank you for using QPay");
                    }
                } else {
                    custumdialogfailure("Deposit Failure!", "Your amount NPR " + amount + " is failed to deposit in " + bank_name + " " + branch + " branch.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                custumdialogfailure("Deposit Failure!", "Your amount NPR " + amount + " is failed to deposit in " + bank_name + " " + branch + " branch.");

            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }
    }

    public void vibration() {
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
    }

    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new AccountListingFragment())
                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, term_id, stan, crrn;

        public Ackclass(String app_id, String term_id, String stan, String crrn) {
            this.app_id = app_id;
            this.term_id = term_id;
            this.stan = stan;
            this.crrn = crrn;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("term_id", term_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn", crrn);

                ack_test = networkAPI.sendHTTPData(Constants.ACKN, jsonObject);
                Log.d(Utility.TAG, "ack post : " + jsonObject + " response : " + ack_test);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    AccountListingFragment najirEnglish = new AccountListingFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();


                    return true;
                }
                return false;
            }
        });
    }
}
