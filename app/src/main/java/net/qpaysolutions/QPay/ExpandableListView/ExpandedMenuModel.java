package net.qpaysolutions.QPay.ExpandableListView;

/**
 * Created by deadlydragger on 4/18/17.
 */

public  class ExpandedMenuModel {
    String iconName = "";
    int iconImg = -1; // menu icon resource id
    int addIcon=-1;
    boolean set;

    public void setSet(boolean set) {
        this.set = set;
    }

    public boolean isSet() {
        return set;
    }

    public int getAddIcon() {
        return addIcon;
    }

    public void setAddIcon(int addIcon) {
        this.addIcon = addIcon;
    }

    public String getIconName() {
        return iconName;
    }
    public void setIconName(String iconName) {
        this.iconName = iconName;
    }
    public int getIconImg() {
        return iconImg;
    }
    public void setIconImg(int iconImg) {
        this.iconImg = iconImg;
    }
}
