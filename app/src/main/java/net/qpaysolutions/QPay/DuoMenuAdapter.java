package net.qpaysolutions.QPay;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import net.qpaysolutions.QPay.dashboard.MainActivityFragment;
import net.qpaysolutions.QPay.duonavigation.views.DuoOptionView;

import java.util.ArrayList;

/**
 * Created by PSD on 13-04-17.
 */

public class DuoMenuAdapter extends BaseAdapter {
    private ArrayList<String> mOptions = new ArrayList<>();
    private ArrayList<DuoOptionView> mOptionViews = new ArrayList<>();
    private int[] drawable = {R.drawable.ic_action_home, R.drawable.ic_action_invite_friend, R.drawable.ic_action_floating_add_card, R.drawable.ic_action_recharge_pin, R.drawable.ic_action_my_code, R.drawable.ic_action_history, R.drawable.ic_action_settings, R.drawable.ic_action_notification, R.drawable.ic_action_faq, R.drawable.ic_action_contact, R.drawable.ic_view_kyc, R.drawable.ic_action_about_us, R.drawable.round,R.drawable.ic_action_lock};


    public DuoMenuAdapter(ArrayList<String> options) {
        mOptions = options;
    }

    @Override
    public int getCount() {
        return mOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return mOptions.get(position);
    }

    void setViewSelected(int position, boolean selected) {
        // Looping through the options in the menu
        // Selecting the chosen option
        for (int i = 0; i < mOptionViews.size(); i++) {
            if (i == position) {
//                mOptionViews.get(i).setSelected(selected);
            } else {
//                mOptionViews.get(i).setSelected(!selected);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final String option = mOptions.get(position);

        // Using the DuoOptionView to easily recreate the demo
        final DuoOptionView optionView;
        if (convertView == null) {
            optionView = new DuoOptionView(parent.getContext());
        } else {
            optionView = (DuoOptionView) convertView;
        }
        // Using the DuoOptionView's default selectors
        optionView.bind(option, parent.getContext().getResources().getDrawable(drawable[position]), parent.getContext().getResources().getDrawable(drawable[position]));
        if (option.equals("0")) {
        }
        // Adding the views to an array list to handle view selection
        mOptionViews.add(optionView);
        return optionView;
    }
}
