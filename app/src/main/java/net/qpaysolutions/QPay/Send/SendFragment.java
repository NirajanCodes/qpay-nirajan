package net.qpaysolutions.QPay.Send;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Billpayment.BillPayActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by deadlydragger on 10/16/16.
 */
public class SendFragment extends Fragment implements View.OnClickListener {
    private String cust_id_friend, cell_no_friend, name_friend, status_friend, enc_cust_id, img_url, resp_code;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog pDialog;
    private String app_id, custId;
    private String usr_mobile;
    private SQLiteDatabase db;
    private FriendListAdapter todoAdapter;
    private Cursor todoCursor;
    private boolean isOpenLookup = false;
    private static final int PICK_CONTACT = 1;
    public EditText aut_editext;
    private String cNumber;
    private View view;
    private Dialog dialog;


    private Context mContext;

    private static final int REQUEST = 112;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();
        QPayCustomerDatabase handler = new QPayCustomerDatabase(getActivity());
        db = handler.getWritableDatabase();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(final View view_v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view_v, savedInstanceState);
        view = view_v;
        mContext = getActivity();
    }


    @Override
    public void onResume() {
        super.onResume();
        LinearLayout button_lookup = (LinearLayout) view.findViewById(R.id.lookup_buttton);
        LinearLayout scan_code = (LinearLayout) view.findViewById(R.id.scan_code_button);
        ListView lookup_friend = (ListView) view.findViewById(R.id.lookup_friend_list);
        Button confirm_money = (Button) view.findViewById(R.id.confirm_money);
        LinearLayout list_iteam = (LinearLayout) view.findViewById(R.id.list_iteam);
        LinearLayout no_friends = (LinearLayout) view.findViewById(R.id.no_friends);
        confirm_money.setOnClickListener(this);
        app_id = QPayCustomerDatabase.getKeyAppId();
        custId = QPayCustomerDatabase.getCustomerID();
        usr_mobile = QPayCustomerDatabase.getCustomerPhone();
        button_lookup.setOnClickListener(this);
        scan_code.setOnClickListener(this);
        view.findViewById(R.id.send_money_top_layout).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                View rootView = view.findViewById(R.id.send_money_top_layout).getRootView();
                rootView.getWindowVisibleDisplayFrame(r);
                DisplayMetrics dm = rootView.getResources().getDisplayMetrics();

                int heightDiff = rootView.getBottom() - r.bottom;

                // FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.findViewById(R.id.send_money_top_layout).getLayoutParams();
                // params.bottomMargin = heightDiff;
                //  view.findViewById(R.id.send_money_top_layout).setLayoutParams(params);
            }
        });

        todoCursor = db.rawQuery("SELECT *  FROM friend_list", null);
        int count = todoCursor.getCount();
        Log.d(Utility.TAG, "data from friend_list table: " + todoCursor);
        if (count > 0) {
            list_iteam.setVisibility(View.VISIBLE);
            no_friends.setVisibility(View.GONE);
            todoAdapter = new FriendListAdapter(getActivity(), todoCursor, SendFragment.this);
            lookup_friend.setAdapter(todoAdapter);
        } else {
            no_friends.setVisibility(View.VISIBLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.send_main, container, false);
    }

    public void pickContact() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setTypeAndNormalize(ContactsContract.Contacts.CONTENT_TYPE);
            startActivityForResult(intent, PICK_CONTACT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.dismiss();
    }

    public void LookupDialog() {

        final Dialog dialog = new Dialog(getActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custum_dialog_lookup_send);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout dialogButton = (LinearLayout) dialog.findViewById(R.id.ok);
        LinearLayout dialogCancel = (LinearLayout) dialog.findViewById(R.id.cancel);
        this.dialog = dialog;
        aut_editext = dialog.findViewById(R.id.hint);
        aut_editext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                aut_editext.setError(null);
                if (s.length() == 10) {
                    hideSoftkey(aut_editext);
                }
                if (s.length() > 10) {
                    aut_editext.setError("Enter Valid No");
                }
            }
        });
        final ImageView contact_picker = (ImageView) dialog.findViewById(R.id.contact_picker);
        contact_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {android.Manifest.permission.READ_CONTACTS};
                    if (!(((SendMainActivity) getActivity()).hasPermissions(mContext, PERMISSIONS))) {
                        ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, REQUEST);
                    } else {
                        pickContact();
                    }
                } else {
                    pickContact();
                }

            }
        });
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        showkeyword(aut_editext);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String text_from_editext = aut_editext.getText().toString();
                    if (text_from_editext.length() == 10) {
                        if (!text_from_editext.equals(usr_mobile)) {
                            Cursor cursor = db.rawQuery(" SELECT *  FROM friend_list where friend_list.cell_no = " + text_from_editext, null);
                            int count = cursor.getCount();
                            if (count < 1) {
                                new LookUpFriendsFriends(text_from_editext).execute();
                                hideSoftkey(aut_editext);
                                isOpenLookup = false;
                                dialog.dismiss();

                                Log.d(Utility.TAG, text_from_editext + usr_mobile);
                            } else {
                                aut_editext.setError("Already in friend list");
                            }
                        } else {
                            aut_editext.setError("You can't add your own number.");
                        }
                    } else {
                        aut_editext.setError("Enter Valid Number");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOpenLookup = false;
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (360 * density);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setAttributes(lp);
    }

    public void hideSoftkey(EditText str) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(str.getWindowToken(), 0);
    }

    public void showkeyword(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInputFromInputMethod(editText.getWindowToken(), 0);
    }

    public class LookUpFriendsFriends extends AsyncTask<String, String, String> {
        String cell_phone;

        public LookUpFriendsFriends(String cell_phoone) {
            this.cell_phone = cell_phoone;
        }

        @Override
        protected String doInBackground(String... params) {
            String friends_list_from;
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject_fri = new JSONObject();
            try {
                jsonObject_fri.put("app_id", app_id);
                jsonObject_fri.put("cell_phone", cell_phone);
                jsonObject_fri.put("custId", custId);
                jsonObject_fri.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_fri.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "lookup send : " + jsonObject_fri.toString());
                friends_list_from = networkAPI.sendHTTPData(Constants.LOOK_UP_FRIENDS, jsonObject_fri);
                Log.d(Utility.TAG, "lookup result send : " + friends_list_from);
                JSONObject jsonObject = new JSONObject(friends_list_from);
                JSONObject jsonObject1 = jsonObject.getJSONObject("lookupfriendResult");
                Log.d(Utility.TAG, jsonObject1.toString());
                cell_no_friend = jsonObject1.getString("cell_no");
                name_friend = jsonObject1.getString("name");
                status_friend = jsonObject1.getString("status");
                enc_cust_id = jsonObject1.getString("enc_cust_id");
                img_url = jsonObject1.getString("imgURL");
                resp_code = jsonObject1.getString("resp_code");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status_friend;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.hide();
            pDialog.dismiss();
            try {
                if (s.equals("00")) {
                    if (resp_code.equals("00")) {
                        Cursor cursor = db.rawQuery(" SELECT *  FROM friend_list where friend_list.cell_no = " + cell_no_friend, null);
                        int count = cursor.getCount();
                        if (count < 1) {
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            String date = df.format(Calendar.getInstance().getTime());
                            QPayCustomerDatabase.insertFriendList(cust_id_friend, name_friend, cell_no_friend, enc_cust_id, img_url);
                            ChatModelList chatModelList = new ChatModelList(cell_no_friend, name_friend, enc_cust_id, img_url, date, "No Message.", "0");
                            if (QPayCustomerDatabase.isAlreadyInChatListcontact(cell_no_friend) > 0) {

                            } else {
                                QPayCustomerDatabase.insertChatListData(chatModelList);
                            }
                        }

                        EnterAmountFragment enterAmountFragment = new EnterAmountFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fragment, enterAmountFragment);
                        Bundle bundle = new Bundle();
                        bundle.putString("imgURL", img_url);
                        bundle.putString("foren_id", enc_cust_id);
                        bundle.putString("name", name_friend);
                        bundle.putString("phone", cell_no_friend);
                        enterAmountFragment.setArguments(bundle);
                        fragmentTransaction.commit();
                    } else {
                        custumFriendsnotfound(cell_phone);
                    }
                } else {
                    custumFriendsnotfound(cell_phone);
                }
            } catch (Exception e) {
                e.printStackTrace();
                custumFriendsnotfound(cell_phone);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    public void custumFriendsnotfound(final String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_friendsnotfound, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Your friend with phone number " + Html.fromHtml("<b>" + number + "</b>") + " was not found. Do you want to invite your friend?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity(), BillPayActivity.class);
                intent.putExtra("id", "invite");
                intent.putExtra("Check", 1);
                intent.putExtra("number", number);
                startActivity(intent);
                getActivity().finish();
/*
                Bundle bundle = new Bundle();
                bundle.putString("number", number);
                inviteFriends.setArguments(bundle);
                FragmentManager fragmentManager=getChildFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.send_money_top_layout,inviteFriends);
                transaction.commit();
*/
              /*  Intent intent = new Intent(getActivity(), InviteFriends.class);
                intent.putExtra("Check", 1);
                intent.putExtra("number", number);
                startActivity(intent);
                getActivity().finish();*/

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (300 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lookup_buttton:
                try {
                    if (isOpenLookup == false) {
                        isOpenLookup = true;
                        LookupDialog();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.scan_code_button:
                Intent intent = new Intent(getActivity(), FriendScanActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void setPicassoImage(String url, ImageView imageView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.header_placeholder)
                .error(R.drawable.header_placeholder)
                .noFade()
                .into(imageView);
    }

    public void enterAmountofmoney(final String name, final String foren_cust_id, final String img_url, String phone) {
        EnterAmountFragment enterAmountFragment = new EnterAmountFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, enterAmountFragment);
        Bundle bundle = new Bundle();
        bundle.putString("imgURL", img_url);
        bundle.putString("foren_id", foren_cust_id);
        bundle.putString("name", name);
        bundle.putString("phone", phone);
        enterAmountFragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if(dialog!=null){
            dialog.dismiss();
        }
        LookupDialog();
        if (resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                Cursor phones = null;
                if (hasPhone.equalsIgnoreCase("1")) {
                    phones = getActivity().getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                            null, null);
                    phones.moveToFirst();
                    cNumber = phones.getString(phones.getColumnIndex("data1"));
                }
                if (cNumber != null && !cNumber.isEmpty()) {
                    String phone = cNumber.replace("+977", "")
                            .replace("-", "")
                            .replaceAll("[^0-9]", "")
                            .trim();
                    aut_editext.setText(phone.trim());
                    /* if (phone.trim().length()==11){
                                aut_editext.setSelection(phone.length());
                            }*/
                }
            }
        }
    }

    public void deleteFriendList(String id) {
        String friends_delet = " \'" + id + "\' ";
        QPayCustomerDatabase.deleteFriendList(friends_delet);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment, new SendFragment())
                .addToBackStack(null)
                .commit();

    }

    public void custumdialogfailure(final String id) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custom_dialog_delete, null);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        final TextView sure = (TextView) v.findViewById(R.id.sure);
        sure.setText("Are you sure?");
        final TextView content_text = (TextView) v.findViewById(R.id.content_text);
        content_text.setText("Are you sure to delete ?");
        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                deleteFriendList(id);
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        QPayCustomerDatabase.close();
    }
}
