package net.qpaysolutions.QPay.Send;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.CheckInternetConnection.NetworkInformation;
import net.qpaysolutions.QPay.CheckInternetConnection.Networkavailable;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.PaymentnProfileAdapter.PaymentProfileAdapter;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.util.Timer;

/**
 * Created by deadlydragger on 6/5/16.
 */
public class PaymentFundTransferFriend extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private QPayCustomerDatabase sctDatabaseHelper;
    private TextView balance_qpay;
    String cust_id/* = "20234396"*/, app_id;
    private String status;
    private QPayProgressDialog pDialog;
    private String result;
    private LinearLayout qpay_line;
    private String amount, name, own_cust, friend, amount_friend, dev_token, note;
    private ListView payment_listl;
    private String stan_send, txm_send;
    private static final String TAG = "dinesh";
    private String stan_friend;
    JSONObject Response;
    Handler handler = new Handler();
    final Timer timer = new Timer();
    TextView paidTo, paidToAmount;
    private Toolbar toolbar;
    String stan_qpay;
    Networkavailable networkavailable = new Networkavailable();
    int counter = 0;
    String img_url;
    ImageView set_img;
    String resp_code_qpay, resp_code_card,availBal;
    String crrn_qpay, crrn_card,dsplyMsg;
    private NetworkChangeReceiver networkChangeReceiver;
    private boolean mIsReceiverRegistered = false;
    boolean isDilogOpen = false;
    TextView toolbar_text, wallet_amount_text, deposit_balance_text, avail_bal_text, hold_bal_text;
    private LinearLayout ll_available, ll_deposite, ll_hold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.main_payment);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar_text = (TextView) toolbar.findViewById(R.id.title);
            toolbar_text.setText("Payment Option");
            wallet_amount_text = (TextView) findViewById(R.id.wallet_amount_text);
            deposit_balance_text = (TextView) findViewById(R.id.deposit_balance_text);
            avail_bal_text = (TextView) findViewById(R.id.avail_bal_text);
            ll_available = (LinearLayout) findViewById(R.id.ll_available);
            ll_deposite = (LinearLayout) findViewById(R.id.ll_deposit);
            ll_hold = (LinearLayout) findViewById(R.id.ll_hold);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            paidTo = (TextView) findViewById(R.id.paidTo);
            paidToAmount = (TextView) findViewById(R.id.paidTo_amount);
            hold_bal_text = (TextView) findViewById(R.id.hold_balance_text);
            qpay_line = (LinearLayout) findViewById(R.id.qpay_row);
            sctDatabaseHelper = new QPayCustomerDatabase(PaymentFundTransferFriend.this);
            cust_id = sctDatabaseHelper.getCustomerID();
            app_id = sctDatabaseHelper.getKeyAppId();
            set_img = (ImageView) findViewById(R.id.img_url);
            dev_token = ((CustomerApplication) this.getApplication()).getDevtokenVariable();
            Log.d(TAG, "dev_token :" + dev_token);
            qpay_line.setOnClickListener(this);
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                own_cust = extras.getString("own_id");
                friend = intent.getStringExtra("foren_id");
                amount = new Decimalformate().decimalFormate(extras.getString("amount"));
                amount_friend = new Decimalformate().decimalFormate(intent.getStringExtra("amount"));
                stan_friend = intent.getStringExtra("stan");
                name = extras.getString("name");
                stan_send = extras.getString("stan_send");
                txm_send = extras.getString("txn_amount_send");
                img_url = extras.getString("img_url");
                note = extras.getString("note");
            }
            paidToAmount.setText("NPR " + amount_friend);
            paidTo.setText(name);
            Log.d(Utility.TAG, "own id : " + own_cust + " friend: " + friend + " amount : " + amount + "foren_id : " + friend);
            SQLiteDatabase db = sctDatabaseHelper.getWritableDatabase();
            payment_listl = (ListView) findViewById(R.id.payment_card_List);
            Cursor cursor = db.rawQuery(" select * from Profile where act_flag ='V' ", null);
            PaymentProfileAdapter cardListAdapter = new PaymentProfileAdapter(this, cursor);
            payment_listl.setAdapter(cardListAdapter);
            payment_listl.setOnItemClickListener(this);
            setPicassoImage(img_url, set_img);
            Log.d(Utility.TAG, "Image url : " + img_url);
            networkChangeReceiver = new NetworkChangeReceiver();
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mIsReceiverRegistered = true;
            checkInternetConnection();
            walletBalance();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void walletBalance() {
        wallet_amount_text.setText("NPR " + new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
    }

    public void setPicassoImage(String url, ImageView imageView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.header_placeholder)
                .error(R.drawable.header_placeholder)
                .noFade()
                .into(imageView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.qpay_row:
                try {
                    if (GeneralPref.getAllowBonus()) {
                        double amount_send = Double.parseDouble(amount.replace(",", ""));
                        double balance_send = Double.valueOf(BalancePref.getAvailableAmount());
                        if (amount_send <= balance_send) {
                            if (isDilogOpen == false) {
                                isDilogOpen = true;
                                confirmPaythroughQpay(amount_friend);
                            }

                        } else {
                            custumdialogfailureIsuffiednt("Fund Transfer Failure!", "Insufficient QPay balance for this transaction. ");/*Please try with cards.*/
                        }
                    } else {
                        custumdialogfailure();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    custumdialogfailure("Fund Transfer Failure!", "Insufficient QPay balance for this transaction. ");/*Please try with cards.*/
                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mIsReceiverRegistered) {
            if (networkChangeReceiver == null)
                networkChangeReceiver = new NetworkChangeReceiver();
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mIsReceiverRegistered = true;
        }
    }

    public void custumdialogfailure() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentFundTransferFriend.this);
        LayoutInflater inflater = PaymentFundTransferFriend.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_custom_dialog_bill_payment_feature_unavailable, null);
        final TextView sure = (TextView) v.findViewById(R.id.sure);
        final TextView conttent_text = (TextView) v.findViewById(R.id.content_text);
        conttent_text.setText("Please perform a sales transaction or add SCT card in order to use your sign-up bonus .");
        sure.setText("Fund Transfer Unavailable!");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(PaymentFundTransferFriend.this, MainActivity.class));
                finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onPause() {
        if (mIsReceiverRegistered) {
            unregisterReceiver(networkChangeReceiver);
            networkChangeReceiver = null;
            mIsReceiverRegistered = false;
        }
        super.onPause();
    }

    public void confirmPaythroughQpay(String amount) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentFundTransferFriend.this);
        LayoutInflater inflater = PaymentFundTransferFriend.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Do you want to Send NPR " + amount + " from your QPay Account?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isDilogOpen = false;
                dialog.dismiss();
            }
        });
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new TransferFundqpayfriend().execute();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String last_4 = sctDatabaseHelper.getLastFourForBill(id);
        String card_token = sctDatabaseHelper.getCardType(String.valueOf(id));
        verificationPaymentFriend(last_4, card_token, id);
    }


    public class TransferFundqpayfriend extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            NetworkAPI serviceHandler = new NetworkAPI();
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("dev_token", dev_token);
                jsonObject.put("sen_id", cust_id);
                jsonObject.put("recv_id", friend);
                jsonObject.put("amt", amount_friend);
                jsonObject.put("note", note);
                jsonObject.put("lat", ((CustomerApplication) PaymentFundTransferFriend.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) PaymentFundTransferFriend.this.getApplication()).getLng());
                Log.d(Utility.TAG, "transfer balance using qpay to friends : post " + jsonObject.toString());
                String result = serviceHandler.sendHTTPData(Constants.TRANSFER_FUND_QPAY, jsonObject);
                Log.d(Utility.TAG, "reaponse : " + result);
                JSONObject transferfund = new JSONObject(result);
                JSONObject jsonObject1 = transferfund.getJSONObject("TransferFundsResult");
                status = jsonObject1.getString("status");
                stan_qpay = jsonObject1.getString("stan");
                resp_code_qpay = jsonObject1.getString("resp_code");
                crrn_qpay = jsonObject1.getString("crrn");
                dsplyMsg =jsonObject1.getString("dsplyMsg");
                availBal=jsonObject1.getString("avail_bal");
                return  result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                pDialog.dismiss();
                pDialog.hide();
                if (!s.equals("null") && !s.isEmpty() && status.equals("00")) {
                    if (resp_code_qpay.equals("00")) {
                        BalancePref.setAvailableAmount(new Decimalformate().decimalFormate(availBal));
                        Vibrator v = (Vibrator) PaymentFundTransferFriend.this.getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(1000);

                        custumdialogSucess("Fund Transfer Success!", "You have successfully Sent NPR " + amount_friend + " to " + name + ".");
                        new Ackclass(app_id, cust_id, stan_qpay, crrn_qpay).execute();
                    } else if (resp_code_qpay.equals("UN")) {
                        new Ackclass(app_id, cust_id, stan_qpay, crrn_qpay).execute();
                        String friends_delet = " \'" + friend + "\' ";
                        sctDatabaseHelper.deleteFriendList(friends_delet);
                        custumdialogfailure("Fund Transfer Failure!", dsplyMsg);
                    } else if (resp_code_qpay.equals("99")) {
                        custumdialogfailure("Fund Transfer Failure!", "Your transaction could not be processed.");
                    } else if (resp_code_qpay.equals("KE")) {
                        custumdialogfailure("Fund Transfer Failure!", dsplyMsg);
                    }

                    else {
                        new Ackclass(app_id, cust_id, stan_qpay, crrn_qpay).execute();
                        custumdialogfailure("Fund Transfer Failure!", dsplyMsg);
                    }
                } else {
                    custumdialogfailure("Fund Transfer Failure!", "Your transaction could not be processed.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.");

                } catch (Exception e1) {
                    e1.printStackTrace();

                }


            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(PaymentFundTransferFriend.this);
            pDialog.setCancelable(false);
            pDialog.show();

        }
    }

    private class Payment_card_purcheshFrn extends AsyncTask<Void, Void, JSONObject> {
        String last_4, pin, dev_token, card_token;
        long id;

        private Payment_card_purcheshFrn(String last_4, String pin, String dev_token, String card_token, long id) {
            this.last_4 = last_4;
            this.pin = pin;
            this.dev_token = dev_token;
            this.card_token = card_token;
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new QPayProgressDialog(PaymentFundTransferFriend.this);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            String line = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("dev_token", dev_token);
                jsonObject.put("sender_id", cust_id);
                jsonObject.put("reciever_id", friend);
                jsonObject.put("card_token", card_token);
                jsonObject.put("amt", amount);
                jsonObject.put("last_4", last_4);
                jsonObject.put("note", note);
                jsonObject.put("pin", pin);
                jsonObject.put("lat", ((CustomerApplication) PaymentFundTransferFriend.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) PaymentFundTransferFriend.this.getApplication()).getLng());
                Log.d(Utility.TAG, "post fund card transfer : " + jsonObject.toString());
                result = networkAPI.sendHTTPData(Constants.FUND_TRNAS_CARD_FRND, jsonObject);
                Log.d(Utility.TAG, "response fund card transfer : " + result);
                Response = new JSONObject(result);
            } catch (Exception e) {
                Log.d(Utility.TAG, "Error Encountered");
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            super.onPostExecute(args);
            try {
                Log.d(Utility.TAG, args.toString());
                JSONObject purchaseCardTxnResult = args.getJSONObject("FundTransferWithCardResult");
                String status = purchaseCardTxnResult.getString("status");
                final String crrn = purchaseCardTxnResult.getString("crrn");
                final String stan = purchaseCardTxnResult.getString("stan");
                String resp_code = purchaseCardTxnResult.getString("resp_code");
                if (status.equals("00") && !status.isEmpty() && !status.equals("null") && status != null) {
                    if (resp_code.equals("00")) {
                        callAsynchronousTask(cust_id, crrn, stan);
                    } else if (resp_code.equals("UN")) {
                        pDialog.dismiss();
                        custumdialogfailureunavailable("Fund Transfer Failure!", "Your friends is not available in QPay network.", friend);

                    } else if (resp_code.equals("CN")) {
                        sctDatabaseHelper.deletCard(id);
                        pDialog.dismiss();
                        pDialog.hide();
                        custumdialogfailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");

                    } else {
                        pDialog.dismiss();
                        custumdialogfailure("Fund Transfer Failure!", "Your transaction could not be processed.");
                    }

                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    custumdialogfailure("Fund Transfer Failure!", "Your transaction could not be processed.");
                }
            } catch (Exception e) {
                pDialog.dismiss();
                pDialog.hide();
                custumdialogfailure("Fund Transfer Failure!", "Your transaction could not be processed.");

            }
        }
    }

    public void callAsynchronousTask(final String cust_id, final String crr, final String stan) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                counter++;
                Log.d(Utility.TAG, "counter purchase is : " + counter);
                new CheckpurchtransctionFrn(cust_id, crr, stan, app_id).execute();
            }
        }, Tags.TIME_SLEEP);
    }

    private class CheckpurchtransctionFrn extends AsyncTask<String, String, JSONObject> {

        String cust_id, crrn, stan, app_id;

        private CheckpurchtransctionFrn(String cust_id, String crr, String stan, String app_id) {
            this.cust_id = cust_id;
            this.crrn = crr;
            this.stan = stan;
            this.app_id = app_id;
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();

            String result;
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn", crrn);
                Log.d(Utility.TAG, "CheckFundTransferCardTxnStatusResult post : " + jsonObject.toString());
                result = networkAPI.sendHTTPData(Constants.CHECK_PURCHASE_FRN_CONST, jsonObject);
                Log.d(Utility.TAG, "CheckFundTransferCardTxnStatusResult post : " + result);
                jsonObject = new JSONObject(result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(JSONObject s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = s.getJSONObject("CheckFundTransferCardTxnStatusResult");
                String status = jsonObject.getString("status");
                String display_msg = jsonObject.getString("dsplyMsg");
                String available_balance = jsonObject.getString("avail_bal");
                String resp_code = jsonObject.getString("resp_code");
                if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("00")) {
                    if (resp_code.equals("00")) {
                        pDialog.dismiss();
                        pDialog.hide();
                        new Ackclass(app_id, cust_id, stan, crrn).execute();
                        Vibrator v = (Vibrator) PaymentFundTransferFriend.this.getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(1000);
                        custumdialogSucess("Fund Transfer Success!", display_msg);
                    } else if (resp_code.equals("LO")) {
                        if (counter < 10) {
                            callAsynchronousTask(cust_id, crrn, stan);
                        } else {
                            pDialog.dismiss();
                            pDialog.hide();
                            custumdialogfailure("Fund Transfer Failure!", "We are unable to process your transaction at this time. Please try again later.");
                        }
                    } else if (resp_code.equals("99")) {
                        pDialog.dismiss();
                        pDialog.hide();
                        new Ackclass(app_id, cust_id, stan, crrn).execute();
                        custumdialogfailure("Fund Transfer Failure!", "We are unable to process your transaction at this time. Please try again later.");
                    } else {
                        pDialog.dismiss();
                        pDialog.hide();
                        new Ackclass(app_id, cust_id, stan, crrn).execute();
                        custumdialogfailure("Fund Transfer Failure!", display_msg);
                    }
                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    new Ackclass(app_id, cust_id, stan, crrn).execute();
                    custumdialogfailure("Fund Transfer Failure!", "We are unable to process your transaction at this time. Please try again later.");

                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    pDialog.dismiss();
                    pDialog.hide();
                    custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.");

                } catch (Exception e1) {
                    e1.printStackTrace();

                }


            }
        }
    }

    public void verificationPaymentFriend(final String last_4, final String card_token, final long id) {
        final Dialog dialog = new Dialog(PaymentFundTransferFriend.this, R.style.UpdownDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custum_dialog_entrcardpin);
        final EditText enter_pin = (EditText) dialog.findViewById(R.id.enter_pin);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView dialogButton = (TextView) dialog.findViewById(R.id.proceed_pin_enter);
        TextView dialogcancel = (TextView) dialog.findViewById(R.id.cancel_pin_enter);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = "";
                pin = enter_pin.getText().toString();
                if (pin.length() == 4) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(enter_pin.getWindowToken(), 0);
                    new Payment_card_purcheshFrn(last_4, pin, dev_token, card_token, id).execute();
                    dialog.dismiss();
                } else {
                    enter_pin.setError("Enter valid pin");
                }

            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(enter_pin.getWindowToken(), 0);
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mIsReceiverRegistered) {
            unregisterReceiver(networkChangeReceiver);
            networkChangeReceiver = null;
            mIsReceiverRegistered = false;
        }
        super.onBackPressed();
        Intent intent = new Intent(PaymentFundTransferFriend.this, SendMainActivity.class);
        startActivity(intent);
        finish();
    }

    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, cust_id, stan, crrn;

        public Ackclass(String app_id, String cust_id, String stan, String crrn) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.stan = stan;
            this.crrn = crrn;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("crrn", crrn);
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("stan", stan);
                Log.d(Utility.TAG, "ack post : " + jsonObject);
                ack_test = networkAPI.sendHTTPData(Constants.ACKN, jsonObject);
                Log.d(Utility.TAG, "response : " + jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public void custumdialogfailureIsuffiednt(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentFundTransferFriend.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                /*isDilogOpen = false;
                startActivity(new Intent(PaymentFundTransferFriend.this,SendMainActivity.class));
                finish();*/

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentFundTransferFriend.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDilogOpen = false;
              /*  startActivity(new Intent(PaymentFundTransferFriend.this,MainActivity.class));
                finish();*/
                onBackPressed();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogfailureunavailable(String title, String number, final String friend) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentFundTransferFriend.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                String friends_delet = " \'" + friend + "\' ";
                sctDatabaseHelper.deleteFriendList(friends_delet);
                startActivity(new Intent(PaymentFundTransferFriend.this, MainActivity.class));
                finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentFundTransferFriend.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDilogOpen = false;
                startActivity(new Intent(PaymentFundTransferFriend.this, MainActivity.class));
                finish();

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    private void checkInternetConnection() {
        LinearLayout noInternetConnection = (LinearLayout) findViewById(R.id.no_internet_connection_id);
        NetworkInformation networkInformation = NetworkInformation.getInstance(this);
        if (networkInformation.isOnline()) {
            noInternetConnection.setVisibility(View.GONE);
        } else {
            noInternetConnection.setVisibility(View.VISIBLE);
        }
    }

    public void checkInternetConnection(View view) {
        checkInternetConnection();
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            NetworkInformation networkInformation = NetworkInformation.getInstance(context);
            if (networkInformation.isOnline()) {
                checkInternetConnection();
            }
        }
    }

    public void custumdialogWarning(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentFundTransferFriend.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView) v.findViewById(R.id.dialog_icon);
        title.setText(title_display);
        dialog_balance.setText(number);
        dialog_icon.setImageResource(R.drawable.ic_bill_payment_warning);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(PaymentFundTransferFriend.this, MainActivity.class));
                PaymentFundTransferFriend.this.finish();

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
}
