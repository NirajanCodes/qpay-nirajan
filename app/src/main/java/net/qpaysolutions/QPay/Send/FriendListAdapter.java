package net.qpaysolutions.QPay.Send;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;


/**
 * Created by deadlydragger on 6/7/16.
 */
public class FriendListAdapter extends CursorAdapter {

SendFragment sendFragment;
    public FriendListAdapter(Context context, Cursor c,SendFragment sendFragment) {
        super(context, c,0);
        this.sendFragment=sendFragment;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.friend_list_iteam_send, parent, false);
    }

    @Override
    public void bindView(View view,final Context context, Cursor cursor) {
        final TextView name=(TextView)view.findViewById(R.id.frend);
        TextView cell = (TextView)view.findViewById(R.id.frendcell);
        ImageView friends_image = (ImageView) view.findViewById(R.id.friends_image);
        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.send_money_row_click);
        final String friend_name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        final String friend_cell = cursor.getString(cursor.getColumnIndexOrThrow("cell_no"));
        final String foren_id = cursor.getString(cursor.getColumnIndex("enc_cust_id"));
        final  String img_url = cursor.getString(cursor.getColumnIndex("img_url"));
        final  String _id = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
        name.setText(friend_name);
        cell.setText(friend_cell);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               sendFragment.enterAmountofmoney(friend_name , foren_id,img_url,friend_cell);
            }
        });
        linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                sendFragment.custumdialogfailure(foren_id);
                return true;
            }
        });
        sendFragment.setPicassoImage(img_url, friends_image);

    }
}
