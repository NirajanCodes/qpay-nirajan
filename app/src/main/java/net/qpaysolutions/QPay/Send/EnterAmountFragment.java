package net.qpaysolutions.QPay.Send;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.DecimalTextMasking;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deadlydragger on 8/12/16.
 */
public class EnterAmountFragment extends Fragment implements View.OnClickListener {
    private Button mSendMoney;

    private EditText enter_code_id,note;
    private  String foren_id, name, cust_id, app_id, imgURL;
    private  TextView balance;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String blc_amount;
    private  ProgressDialog progressDialog;
    private  ImageView payingimage;
    private  TextView paying_name,paying_phone,remarks;
    private LinearLayout ll_user_profile;
    private   List<String> remarksList;

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        enter_code_id = (EditText) view.findViewById(R.id.enter_code_id);
        enter_code_id.requestFocus();
        enter_code_id.addTextChangedListener(new DecimalTextMasking(enter_code_id));
        balance = (TextView) view.findViewById(R.id.balance);
        mSendMoney = (Button) view.findViewById(R.id.confirm_money);
        paying_name=(TextView)view.findViewById(R.id.paying_name);
        payingimage=(ImageView)view.findViewById(R.id.paying_img);
        paying_phone=(TextView)view.findViewById(R.id.paying_phone);
        ll_user_profile=(LinearLayout)view.findViewById(R.id.ll_user_profile);
        note=(EditText)view.findViewById(R.id.note);
        remarks=(TextView)view.findViewById(R.id.remarks);
        remarks.setOnClickListener(this);
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                ll_user_profile.setBackground(Utility.getAssetImage(getContext(), "send_to_friend_bg.jpg"));
            } else {
                ll_user_profile.setBackgroundDrawable(Utility.getAssetImage(getContext(), "send_to_friend_bg.jpg"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        mSendMoney.setOnClickListener(this);
        foren_id = getArguments().getString("foren_id");
        name = getArguments().getString("name");
        imgURL = getArguments().getString("imgURL");
        paying_phone.setText(getArguments().getString("phone"));
        new Utility().showKeyboard(getActivity());
        balance.setText("NPR "+ new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
//        enter_code_id.requestFocus();
        try {
            setPicassoImage(imgURL,payingimage);
            paying_name.setText(name);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scan_enter_money, container, false);
    }
    public void setPicassoImage(String url, ImageView imageView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.header_placeholder)
                .error(R.drawable.header_placeholder)
                .noFade()
                .into(imageView);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_money:
                try {
                    String balance = enter_code_id.getText().toString().replace(",","");
                    double balance_enter = Double.parseDouble(balance);
                    if ( balance_enter >=20 && balance_enter <=5000) {
                        Intent intent = new Intent(getActivity(), PaymentFundTransferFriend.class);
                        intent.putExtra("own_id", cust_id);
                        intent.putExtra("foren_id", foren_id);
                        intent.putExtra("amount", String.valueOf(balance_enter));
                        intent.putExtra("name", name);
                        intent.putExtra("img_url", imgURL);
                        intent.putExtra("note",note.getText().toString());
                        startActivity(intent);
                        getActivity().finish();
                        InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(enter_code_id.getWindowToken(),0);
                    } else {
                        enter_code_id.setError("Enter amount in range 20 to 5000.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    enter_code_id.setError("Enter amount in range 20 to 5000.");
                }
                break;
            case R.id.remarks:
                setRemarks();
                break;
        }

    }

    public void setRemarks(){
        new Utility().hideSoftkey(getActivity());
        String[] ncellRechargeValues = getResources().getStringArray(R.array.suggation_payment);
        remarksList = new ArrayList<>(Arrays.asList(ncellRechargeValues));

        final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(),new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int month, String dateDesc) {
                note.setText(dateDesc);
            }
        }).textConfirm("Select")
                .textCancel("Cancel")
                .btnTextSize(16)
                .viewTextSize(25)
                .setValues(remarksList)
                .build();
        pickerPopWin.showPopWin(getActivity());
    }
}
