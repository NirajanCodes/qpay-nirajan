package net.qpaysolutions.QPay.Send;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 6/14/16.
 */public class FriendListSettingAdapter extends CursorAdapter {
    FriendListSetting friendListSetting;

    public FriendListSettingAdapter(Context context, Cursor c,FriendListSetting friendListSetting) {
        super(context, c,0);
        this.friendListSetting=friendListSetting;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.friend_list_iteam, parent, false);
    }

    @Override
    public void bindView(View view,final Context context, Cursor cursor) {
        ImageView friends_imageImageView= (ImageView)view.findViewById(R.id.friends_image);
        final TextView name=(TextView)view.findViewById(R.id.frend);
        TextView cell = (TextView)view.findViewById(R.id.frendcell);
        LinearLayout send_money_row_click = (LinearLayout)view.findViewById(R.id.send_money_row_click);
//        TextView nothing =(TextView)view.findViewById(R.id.nothing);
        final String friend_name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        String friend_cell = cursor.getString(cursor.getColumnIndexOrThrow("cell_no"));
        final String foren_id = cursor.getString(cursor.getColumnIndex("cust_id"));
        final  String img_url = cursor.getString(cursor.getColumnIndex("img_url"));
        // Populate fields with extracted properties
        name.setText(friend_name);
        cell.setText(friend_cell);
        friendListSetting.setPicassoImage(img_url,friends_imageImageView);



    }
}
