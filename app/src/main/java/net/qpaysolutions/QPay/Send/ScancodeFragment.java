package net.qpaysolutions.QPay.Send;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by deadlydragger on 8/9/16.
 */
public class ScancodeFragment extends Fragment {

    private String id;
    private String merch_name, imgURL;
    private QPayCustomerDatabase getQPayCustomerDatabase;
    private String return_cust_id;
    private SurfaceView barcodeView;
    String resp, app_id;

    String cust_id_friend = "", result_scanfriend_cell = "";
    private SQLiteDatabase db;
    TextView scan_loading;
    LinearLayout blace_back;
    MediaPlayer mediaPlayer;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scan_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        barcodeView = (SurfaceView) view.findViewById(R.id.barcode_scanner);
        scan_loading = (TextView) view.findViewById(R.id.scan_loading);
        blace_back = (LinearLayout) view.findViewById(R.id.blace_back);
        TextView titleText = (TextView) view.findViewById(R.id.titleText);
        titleText.setText(getString(R.string.scan_to_send));
        qrCodeReading();
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.beep);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getQPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        getQPayCustomerDatabase.getReadableDatabase();
        db = getQPayCustomerDatabase.getWritableDatabase();
        id = GeneralPref.getCustId();
        app_id = GeneralPref.getAppId();
    }


    private void qrCodeReading() {
        BarcodeDetector barcodeDetector;
        final CameraSource cameraSource;
        barcodeDetector = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        cameraSource = new CameraSource
                .Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .setAutoFocusEnabled(true)
                .build();

        barcodeView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(barcodeView.getHolder());
                    }

                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    barcodeView.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            mediaPlayer.start();
//                            cameraSource.release();
                            blace_back.setVisibility(View.VISIBLE);
                            barcodeView.setVisibility(View.GONE);
                            try {

                                String resultString = barcodes.valueAt(0).displayValue.toString();
                                return_cust_id = barcodes.valueAt(0).displayValue.toString();

                                if (return_cust_id != null && /*return_cust_id.length()==24 &&*/ !return_cust_id.equals("QPay-Payment Simplified") && !return_cust_id.equals(id)) {

                                    new Get_transfer_fund(return_cust_id).execute();
                                } else if (return_cust_id.equals("QPay-Payment Simplified")) {
                                    dialogFailure("Invalid QRcode!", "You seem to have scanned QPay logo. Please scan proper QRcode.");
                                } else if (return_cust_id.equals(id)) {
                                    dialogFailure("Invalid QRcode!", "You seem to have scanned your own QRcode. Please scan proper QRcode.");
                                } else {
                                    dialogFailure("Invalid QRcode!", "You seem to have scanned non QPay QRcode. Please scan proper QRcode.");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    class Get_transfer_fund extends AsyncTask<String, String, String> {

        String code;
        QPayProgressDialog progressDialog=new QPayProgressDialog(getContext());

        public Get_transfer_fund(String code) {
            this.code = code;
        }

        @Override
        protected String doInBackground(String... uri) {
            JSONObject jsonObject_scan = new JSONObject();
            NetworkAPI httpResponse = new NetworkAPI();
            String result;
            String status = null;
            try {
                jsonObject_scan.put("reciever_id", code);
                jsonObject_scan.put("app_id", GeneralPref.getAppId());
                jsonObject_scan.put("cust_id", GeneralPref.getCustId());
                jsonObject_scan.put("lat", LatLngPref.getLat());
                jsonObject_scan.put("lng", LatLngPref.getLng());
                Log.d(Utility.TAG, "post id : " + jsonObject_scan);
                result = httpResponse.sendHTTPData(Constants.SCAN_NAME, jsonObject_scan);
                Log.d(Utility.TAG, "response  : " + result);
                JSONObject jsonObject = new JSONObject(result);
                JSONObject jsonObject1 = jsonObject.getJSONObject("getNameResult");
                merch_name = jsonObject1.getString("name");
                imgURL = jsonObject1.getString("imgURL");
                result_scanfriend_cell = jsonObject1.getString("cell_phone");
                status = jsonObject1.getString("status");
                resp = jsonObject1.getString("resp_code");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            progressDialog.show();

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            progressDialog.dismiss();

            try {
                blace_back.setVisibility(View.VISIBLE);

                if(result_scanfriend_cell.equalsIgnoreCase(GeneralPref.getPhone())){
                    dialogFailure("Invalid QRcode!", "You seem to have scanned you own qr code. Please scan proper QRcode.");
                }
                else if(resp.equals("00")) {
                    Cursor cursor = db.rawQuery(" SELECT *  FROM friend_list where friend_list.cell_no = " + result_scanfriend_cell, null);
                    int count = cursor.getCount();
                    if (count < 1) {
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String date = df.format(Calendar.getInstance().getTime());
                        getQPayCustomerDatabase.insertFriendList(cust_id_friend, merch_name, result_scanfriend_cell, code, imgURL);
                        ChatModelList chatModelList = new ChatModelList(result_scanfriend_cell, merch_name, cust_id_friend, imgURL, date, "No Message.", "0");
                        if (getQPayCustomerDatabase.isAlreadyInChatListcontact(result_scanfriend_cell) > 0) {
                        } else {
                            getQPayCustomerDatabase.insertChatListData(chatModelList);
                        }
                    }
                    EnterAmountFragment enterAmountFragment = new EnterAmountFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, enterAmountFragment);
                    Bundle bundle = new Bundle();
                    bundle.putString("imgURL", imgURL);
                    bundle.putString("foren_id", code);
                    bundle.putString("name", merch_name);
                    bundle.putString("phone", result_scanfriend_cell);
                    enterAmountFragment.setArguments(bundle);
                    fragmentTransaction.commit();
                } else {
                    dialogFailure("Invalid QRcode!", "You seem to have scanned invalid QRcode. Please scan proper QRcode.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    dialogFailure("Invalid QRcode!", "You seem to have scanned invalid QRcode. Please scan proper QRcode.");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void setPicassoImage(String url, ImageView imageView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.header_placeholder)
                .error(R.drawable.header_placeholder)
                .noFade()
                .into(imageView);
    }

    public void dialogFailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title_failed = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView) v.findViewById(R.id.dialog_icon);
        dialog_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_transfer_failure));
        title_failed.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
}
