package net.qpaysolutions.QPay.Send;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.FragmentCollection.InviteFriends;
import net.qpaysolutions.QPay.FragmentCollection.MainFragment;
import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by deadlydragger on 6/14/16.
 */
public class FriendListSetting extends Fragment implements View.OnClickListener, AdapterView.OnItemLongClickListener {
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String cust_id_friend, cell_no_friend, name_friend, status_friend, enc_cust_id,url,resp_code;
    private QPayProgressDialog pDialog;
    private ListView lookup_friend;
    private LinearLayout lookoff_button;
    private SQLiteDatabase db;
    private String user_phone, app_id,custId;
    private  FriendListSettingAdapter todoAdapter;
    LinearLayout no_friends,list_friend_show;
    Cursor todoCursor;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getWritableDatabase();
        QPayCustomerDatabase handler = new QPayCustomerDatabase(getActivity());
        db = handler.getWritableDatabase();
        user_phone = handler.getCustomerPhone();
        app_id = QPayCustomerDatabase.getKeyAppId();
        custId= QPayCustomerDatabase.getCustomerID();
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity)getActivity()). setToolbar_text("QPay Friends");
        ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);
        lookup_friend = (ListView) view.findViewById(R.id.lookup_friend_list);
        lookoff_button = (LinearLayout) view.findViewById(R.id.friend_list_lookup);
        no_friends=(LinearLayout)view.findViewById(R.id.no_friends);
        list_friend_show=(LinearLayout)view.findViewById(R.id.list_friend_show);
        lookoff_button.setOnClickListener(this);
         todoCursor = db.rawQuery("SELECT *  FROM friend_list", null);
        int count = todoCursor.getCount();
        if (count > 0){
            list_friend_show.setVisibility(View.VISIBLE);
            no_friends.setVisibility(View.GONE);
            Log.d(Utility.TAG, "data from friend_list table: " + todoCursor);
            todoAdapter = new FriendListSettingAdapter(getActivity(), todoCursor, FriendListSetting.this);
            lookup_friend.setAdapter(todoAdapter);
            lookup_friend.setLongClickable(true);
            lookup_friend.setOnItemLongClickListener(this);
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.friend_list_setting, container, false);
    }
    public void hideSoftkey(EditText str) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(str.getWindowToken(), 0);
    }
    public void setPicassoImage(String url, ImageView imageView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.header_placeholder)
                .error(R.drawable.header_placeholder)
                .noFade()
                .into(imageView);
    }
    public void lookupfriend_dialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.UpdownDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custum_dialog_lookup_send);
        final LinearLayout dialogButton = (LinearLayout) dialog.findViewById(R.id.ok);
        LinearLayout dialogcancel = (LinearLayout) dialog.findViewById(R.id.cancel);
        final EditText aut_editext = (EditText) dialog.findViewById(R.id.hint);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text_from_editext = aut_editext.getText().toString();
                if (text_from_editext.length() == 10 && !text_from_editext.equals(user_phone)) {
                    Cursor cursor = db.rawQuery(" SELECT *  FROM friend_list where friend_list.cell_no = " + text_from_editext, null);
                    int count = cursor.getCount();
                    Log.d(Utility.TAG, "count : " + count);
                    String look_up_friends = Constants.LOOK_UP_FRIENDS + text_from_editext;
                    if (count < 1) {
                        new LookUpFriendsFriends(text_from_editext).execute();
                        hideSoftkey(aut_editext);
                        dialog.dismiss();
                        Log.d(Utility.TAG, "look up friedns : " + look_up_friends);

                    } else {

                        new Utility().showSnackbar(dialogButton,"Already in friend list");
                        dialog.dismiss();
                    }
                } else {
                    aut_editext.setError("Insert valid number");
                }


            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (360 * density);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setAttributes(lp);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.friend_list_lookup:
                lookupfriend_dialog();

                break;
        }
    }
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        showDeletsweet(id);
//        Toast.makeText(getContext(),"click : " + position,Toast.LENGTH_LONG).show();
        return true;
    }
   /* public void showDeletsweet(final long position) {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Do you want to delete this friend?")
                .setConfirmText("Yes")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                       View view = getActivity().getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        qpayDatabases.deletFriends(position);
                        Cursor newCursor = db.rawQuery(" SELECT *  FROM friend_list  ", null);
                        todoAdapter.changeCursor(newCursor);
                        todoAdapter.notifyDataSetChanged();
                        sDialog.dismiss();
                        sDialog.hide();
                        FriendListSetting friendListSetting = new FriendListSetting();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragment,friendListSetting)
                                .commit();
                        Toast.makeText(getContext(),"Successfully deleted",Toast.LENGTH_LONG).show();
                    }
                }).show();
    }*/

    public void showDeletsweet(final long position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title=(TextView)v.findViewById(R.id.text_title);
        final ImageView img_icon =(ImageView)v.findViewById(R.id.img_icon);
        img_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning));
        img_icon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.color_yellow), android.graphics.PorterDuff.Mode.MULTIPLY);
        title.setText("Are You Sure?");
        dialog_balance.setText("Do you want to delete this friend?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                QPayCustomerDatabase.deletFriends(position);
                Cursor newCursor = db.rawQuery(" SELECT *  FROM friend_list  ", null);
                todoAdapter.changeCursor(newCursor);
                todoAdapter.notifyDataSetChanged();

                FriendListSetting friendListSetting = new FriendListSetting();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment,friendListSetting)
                        .commit();
                Toast.makeText(getContext(),"Successfully deleted",Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public class LookUpFriendsFriends extends AsyncTask<String, String, String> {
        private String cell_phone;
        public LookUpFriendsFriends(String cell_phone) {
            this.cell_phone = cell_phone;
        }
        @Override
        protected String doInBackground(String... params) {
            String friends_list_from;
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject_fri = new JSONObject();
            try {

                jsonObject_fri.put("app_id", app_id);
                jsonObject_fri.put("cell_phone",cell_phone);
                jsonObject_fri.put("custId",custId);
                jsonObject_fri.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_fri.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                friends_list_from = networkAPI.sendHTTPData(Constants.LOOK_UP_FRIENDS,jsonObject_fri);
                JSONObject jsonObject = new JSONObject(friends_list_from);
                JSONObject jsonObject1 = jsonObject.getJSONObject("lookupfriendResult");
                cell_no_friend = jsonObject1.getString("cell_no");
                name_friend = jsonObject1.getString("name");
                status_friend = jsonObject1.getString("status");
                enc_cust_id = jsonObject1.getString("enc_cust_id");
                url= jsonObject1.getString("imgURL");
                resp_code=jsonObject1.getString("resp_code");
                Log.d(Utility.TAG,"url friends : "+ url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status_friend;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());

            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                pDialog.hide();
                pDialog.dismiss();
                if (resp_code != null && !resp_code.isEmpty() && resp_code.equals("00")) {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String date = df.format(Calendar.getInstance().getTime());
                    ChatModelList chatModelList = new ChatModelList(cell_no_friend,name_friend,enc_cust_id,url,date,"No Message.","0");
                    if (QPayCustomerDatabase.isAlreadyInChatListcontact(cell_no_friend)>0){

                    }else {
                        QPayCustomerDatabase.insertChatListData(chatModelList);
                    }

                    sendTofriends(name_friend, cell_no_friend,url);
                } else {
                    custumFriendsnotfound(cell_phone);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void custumFriendsnotfound(final String number){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater =getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_friendsnotfound, null);
        final  TextView dialog_balance=(TextView)v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Your friend with phone number "+ Html.fromHtml("<b>"+number+"</b>")  +" not found. Do you want to invite your friends?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                InviteFriends inviteFriends = new InviteFriends();
                FragmentTransaction  fragmentTransaction= getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment,inviteFriends);
                Bundle bundle = new Bundle();
                bundle.putString("number",number);
                inviteFriends.setArguments(bundle);
                fragmentTransaction.commit();


               /* Intent intent = new Intent(SendMainActivity.this,MainActivity.class);
                intent.putExtra("number",number);
                intent.putExtra("Check",1);
                startActivity(intent);*/
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void addToFriendList(String name, String cell, final String url) {
        final Dialog dialog = new Dialog(getActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custum_dialog_wantto_add);
        ImageView friends_image_foundImageView= (ImageView) dialog.findViewById(R.id.friends_image_found);
        setPicassoImage(url,friends_image_foundImageView);
        TextView friend= (TextView) dialog.findViewById(R.id.frend_found) ;
        TextView cell_friends = (TextView) dialog.findViewById(R.id.frendcell_found);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.ok);
        TextView dialogcancel = (TextView) dialog.findViewById(R.id.cancel);
//        final TextView message = (TextView) dialog.findViewById(R.id.message_add);
        dialogButton.setText("Add");
        dialogcancel.setText("Cancel");
        friend.setText(name);
        cell_friends.setText(cell);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QPayCustomerDatabase.insertFriendList(cust_id_friend, name_friend, cell_no_friend, enc_cust_id,url);
                FriendListSetting friendListSetting = new FriendListSetting();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment,friendListSetting)
                        .commit();
                dialog.hide();
            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment,najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id",1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                  /*  getFragmentManager().beginTransaction()
                            .replace(R.id.fragment,najirEnglish)
                            .commit();*/
                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;
            }
        });
    }

    public void sendTofriends(String name, String cell, final String url){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.UpdownDialog);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_friendsfound, null);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView friends_image_found = (ImageView) v.findViewById(R.id.friends_image_found);
        TextView frend_found = (TextView) v.findViewById(R.id.frend_found);
        TextView frendcell_found = (TextView) v.findViewById(R.id.frendcell_found);
        TextView dialogButton = (TextView) v.findViewById(R.id.ok);
        TextView dialogcancel = (TextView) v.findViewById(R.id.cancel);
        setPicassoImage(url, friends_image_found);
        dialogButton.setText("Add");
        dialogcancel.setText("Cancel");
        frend_found.setText(name_friend);
        frendcell_found.setText(cell_no_friend);

        v.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        v.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QPayCustomerDatabase.insertFriendList(cust_id_friend, name_friend, cell_no_friend, enc_cust_id,url);
                FriendListSetting friendListSetting = new FriendListSetting();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment,friendListSetting)
                        .commit();
                dialog.hide();

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setAttributes(lp);
    }

}
