package net.qpaysolutions.QPay.Withdrawal;

/**
 * Created by QPay on 5/6/2019.
 */

public class BankPojo {
    private String bankName;
    private String id;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
