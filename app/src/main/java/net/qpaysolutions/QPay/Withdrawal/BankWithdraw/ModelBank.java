package net.qpaysolutions.QPay.Withdrawal.BankWithdraw;

import java.util.HashMap;

public class ModelBank {

    private HashMap<String, String> bankInfo;
    private HashMap<String, String> branchInfo;
    private String bankId;
    private String branchId;
    private String idType;

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdType() {
        return idType;
    }



    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchId() {
        return branchId;
    }


    public void setBankInfo(HashMap<String, String> bankInfo) {
        this.bankInfo = bankInfo;
    }

    public void setBranchInfo(HashMap<String, String> branchInfo) {
        this.branchInfo = branchInfo;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public HashMap<String, String> getBankInfo() {
        return bankInfo;
    }

    public HashMap<String, String> getBranchInfo() {
        return branchInfo;
    }

    public String getBankId() {
        return bankId;
    }
}
