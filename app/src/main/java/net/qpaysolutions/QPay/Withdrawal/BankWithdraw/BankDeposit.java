package net.qpaysolutions.QPay.Withdrawal.BankWithdraw;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.BankPojo;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;
import net.qpaysolutions.QPay.Withdrawal.adapter.BankDepositeAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class BankDeposit extends Fragment implements View.OnClickListener {

    private Spinner spinnerBank, spinnerBranch, spinnerIdType;
    private HashMap<String, String> bankNameHashMap, branchNameHashMap;
    private ProgressBar progressBarBank, progressBarBranch, progressBaridType, progressBarMain;
    private ModelBank model;
    private EditText etName, etAddress, etEmail, etMobileNumber, phoneNuber, etAccountNumber, etAmount, etIdNumber;
    private Button proceed;
    private EditText bankNames;
    private RecyclerView bankNameRecyclerView;
    private ArrayList<BankPojo> bankPojos;
    private BankDepositeAdapter adapter;
    private Dialog dialog;
    private String bankId;
    private LinearLayout branchLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bank_deposit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = new ModelBank();
        ((WithDrawablActivity) getActivity()).setTitleToolbar("Bank Withdraw");

        progressBarMain = view.findViewById(R.id.progress_bar_main);
        branchLayout = view.findViewById(R.id.branchLayout);

        etName = view.findViewById(R.id.name);
        etAddress = view.findViewById(R.id.address);
        etEmail = view.findViewById(R.id.email_address);
        etMobileNumber = view.findViewById(R.id.mobile_number);
        phoneNuber = view.findViewById(R.id.phone_number);
        etAccountNumber = view.findViewById(R.id.account_number);
        etAmount = view.findViewById(R.id.amount);
        etAmount.setText(etAmount.getText().toString());
        proceed = view.findViewById(R.id.proceed);
        etIdNumber = view.findViewById(R.id.id_number);
//        spinnerBank = view.findViewById(R.id.bank_names);
        spinnerBranch = view.findViewById(R.id.branch_names);
        spinnerIdType = view.findViewById(R.id.id_type);
        progressBaridType = view.findViewById(R.id.progress_bar_id);
        progressBarBank = view.findViewById(R.id.progress_bar);
        progressBarBank.setVisibility(View.VISIBLE);
        progressBaridType.setVisibility(View.VISIBLE);
        progressBarBranch = view.findViewById(R.id.progress_bar_branch);
        bankNames = view.findViewById(R.id.bankNames);
        bankNames.setOnClickListener(this);
        bankNames.setFocusable(false);
        bankTask(null, bankNameJson(), "BankName", "Id", Constants.BANKLIST);
//        setSpinner(spinnerBank, progressBarBank);
        bankTask(spinnerIdType, idJson(), "IdType", "Id", Constants.IDTYPE);
        setSpinner(spinnerIdType, progressBaridType);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.dialog_bank_withdraw);
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                TextView name, address, accontNumber, idType, idNumber, phoneNumber, mobileNumber, email, amount;
                Button done;
                name = dialog.findViewById(R.id.tv_name);
                name.setText(etName.getText().toString());
                address = dialog.findViewById(R.id.tv_address);
                address.setText(etAddress.getText().toString());
                amount = dialog.findViewById(R.id.tv_amount);
                amount.setText(etAmount.getText().toString());
                accontNumber = dialog.findViewById(R.id.tv_account_number);
                accontNumber.setText(etAccountNumber.getText().toString());
                idType = dialog.findViewById(R.id.tv_id_type);
                idNumber = dialog.findViewById(R.id.tv_id_number);
                idNumber.setText(etIdNumber.getText().toString());
                phoneNumber = dialog.findViewById(R.id.tv_phone);
                phoneNumber.setText(phoneNuber.getText().toString());
                mobileNumber = dialog.findViewById(R.id.tv_mobile);
                mobileNumber.setText(etMobileNumber.getText().toString());
                email = dialog.findViewById(R.id.tv_email);
                email.setText(etEmail.getText().toString());
                idType.setText(model.getIdType());
                idNumber.setText(idNumber.getText().toString());
                phoneNumber.setText(phoneNuber.getText().toString());
                done = dialog.findViewById(R.id.done);
                dialog.show();
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bankTask(null, postTxn(), null, null, Constants.NCHLPOSTTXN);
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    public void bankTask(final Spinner spinner, final String json, final String paraBankName, final String paraId, String url) {
        final List<String> listValuesHashMap = new ArrayList<>();
        final BackgroundTask.GetJson getJson = new BackgroundTask.GetJson() {
            @Override
            public void getJson(String items) {
                Log.d(Utility.TAG, " response " + items);
                JSONObject jsonObject = null;
                HashMap<String, String> hashMap = new HashMap<>();
                try {
                    progressBarMain.setVisibility(View.GONE);
                    jsonObject = new JSONObject(items);
                    if (spinner == null) {
                        progressBarBank.setVisibility(View.GONE);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        JSONArray data = jsonObject.getJSONArray("data");
                        bankPojos = new ArrayList<>();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject1 = data.getJSONObject(i);
                            String bankName = jsonObject1.getString(paraBankName);
                            String id = jsonObject1.getString(paraId);
                            hashMap.put(bankName, id);
                            BankPojo bankPojo = new BankPojo();
                            bankPojo.setBankName(bankName);
                            bankPojo.setId(id);
                            bankPojos.add(bankPojo);
                        }
                        bankNames.setFocusable(true);
                        bankNames.setClickable(true);

                    } else {
                        JSONArray data = jsonObject.getJSONArray("data");
                        //ArrayList<HashMap> bankDictionery = new ArrayList<>();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject1 = data.getJSONObject(i);
                            String bankName = jsonObject1.getString(paraBankName);
                            String id = jsonObject1.getString(paraId);
                            hashMap.put(bankName, id);
                        }
                        bankNameHashMap = hashMap;
                        for (String datas : hashMap.keySet()) {
                            listValuesHashMap.add(datas);
                            Log.d(Utility.TAG, " for each " + datas);
                        }
                        if (spinner != null) {
                            ArrayAdapter<String> arrayData = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.item_spinner, listValuesHashMap);
                            spinner.setAdapter(arrayData);
                        }
                        adapter = new BankDepositeAdapter(bankPojos, getActivity(), new BankDepositeAdapter.BankDepositeClickListener() {
                            @Override
                            public void onClick(BankPojo item) {
                                bankNames.setText(item.getBankName());
                                bankId = item.getId();
                                bankTask(spinnerBranch, branchNameJson(item.getId()), "BranchName", "Id", Constants.BRANCHLIST);
                                setSpinner(spinnerBranch, progressBarBranch);
                                branchLayout.setVisibility(View.VISIBLE);
                                dialog.dismiss();
                            }
                        });
                    }
                } catch (Exception e) {
                }
            }
        };
        BackgroundTask backgroundTask = new BackgroundTask(getJson, json, url);
        backgroundTask.execute();
    }

    public void setSpinner(final Spinner spinner, final ProgressBar progressBar) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (spinner == spinnerBank) {
                        spinnerBranch.setAdapter(null);
                        progressBarBranch.setVisibility(View.VISIBLE);
                    }
                    String finalVlaue = spinner.getSelectedItem().toString();
                    String finalId = bankNameHashMap.get(finalVlaue);

                    Log.d(Utility.TAG, "id" + finalId);
                    if (spinner == spinnerBank) {
                        model.setBankId(finalId);
                        bankTask(spinnerBranch, branchNameJson(model.getBankId()), "BranchName", "Id", Constants.BRANCHLIST);
                        setSpinner(spinnerBranch, progressBarBranch);
                    } else if (spinner == spinnerBranch) {
                        model.setBranchId(finalId);
                    } else if (spinner == spinnerIdType) {
                        model.setIdType(finalId);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public String bankNameJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            return jsonObject.toString();
        } catch (Exception e) {

        }
        return null;
    }

    public String branchNameJson(String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("bankId", id);
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            return jsonObject.toString();
        } catch (Exception e) {

        }
        return null;
    }

    public String idJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            return jsonObject.toString();
        } catch (Exception e) {

        }
        return null;
    }

    public String postTxn() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("amount", etAmount.getText().toString());
            jsonObject.put("bankId", "2301");
            jsonObject.put("branchId", "1");
            jsonObject.put("creditorName", etName.getText().toString());
            jsonObject.put("creditorAccount", etAccountNumber.getText().toString());
            jsonObject.put("creditorIdType", "0001");
            jsonObject.put("creditorIdValue", etIdNumber.getText().toString());
            jsonObject.put("creditorAddress", etAddress.getText().toString());
            jsonObject.put("creditorPhone", phoneNuber.getText().toString());
            jsonObject.put("creditorMobile", etMobileNumber.getText().toString());
            jsonObject.put("creditoremail", etEmail.getText().toString());
            Log.d(Utility.TAG, "request : " + jsonObject.toString());
            progressBarMain.setVisibility(View.VISIBLE);
            return jsonObject.toString();
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bankNames:
                dialog = new Dialog(getActivity(), WindowManager.LayoutParams.MATCH_PARENT);
                dialog.setContentView(R.layout.dialog_fullscreen_withdraw);
                bankNameRecyclerView = dialog.findViewById(R.id.bankRecyclerView);
                SearchView searchView = dialog.findViewById(R.id.searchView);
                searchView.setIconifiedByDefault(true);
                searchView.setFocusable(true);
                searchView.setIconified(false);
                EditText search = searchView.findViewById(R.id.search_src_text);
                search.setTextColor(getResources().getColor(R.color.white));
                search.setHintTextColor(getResources().getColor(android.R.color.white));
                ImageView imageView = searchView.findViewById(R.id.search_close_btn);
                imageView.setImageResource(R.drawable.ic_action_cancel);
                ImageView closeBranch = searchView.findViewById(R.id.search_button);
                closeBranch.setImageResource(R.drawable.ic_search_black_24dp);
                try {

                    Field mDrawable = SearchView.class.getDeclaredField("mSearchHintIcon");
                    mDrawable.setAccessible(true);
                    Drawable drawable = (Drawable) mDrawable.get(search);

                    //removes the search hint while searching
                    drawable.setBounds(0, 0, 0, 0);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                RecyclerView.LayoutManager mLayoutManagerTo = new LinearLayoutManager(getActivity());
                bankNameRecyclerView.setLayoutManager(mLayoutManagerTo);
                bankNameRecyclerView.setItemAnimator(new DefaultItemAnimator());
                bankNameRecyclerView.setAdapter(adapter);
                dialog.show();


                ImageView backArrowImageViews = dialog.findViewById(R.id.backArrowImageView);
                backArrowImageViews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        adapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        adapter.getFilter().filter(newText);
                        return true;
                    }
                });
                break;
        }

    }
}
