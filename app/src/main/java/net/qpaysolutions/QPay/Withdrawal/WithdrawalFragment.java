package net.qpaysolutions.QPay.Withdrawal;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.Card.ebanking.EBankActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.BankWithdraw.BankDeposit;

/**
 * Created by deadlydragger on 2/27/17.
 */

public class WithdrawalFragment extends Fragment implements View.OnClickListener {
    ImageView deposit, nearby;
    TextView balance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_withdrawal, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((WithDrawablActivity) getActivity()).setTitleToolbar("Withdrawal");
        deposit = view.findViewById(R.id.deposit);
        balance = (TextView) view.findViewById(R.id.balance_main);
        nearby = view.findViewById(R.id.nearby);
        deposit.setOnClickListener(this);
        nearby.setOnClickListener(this);
        balance.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
        //  buttonBar(view);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.deposit:
              /*  if (GeneralPref.getBankDeposite()) {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, new AccountListingFragment())
                            .commit();
                } else {

                }*/
              /*  Intent intent = new Intent(getActivity(), EBankActivity.class);
                startActivity(intent);*/
                //getFragmentManager().beginTransaction().replace(R.id.fragment, new BankDeposit()).commit();
//                new Utility().showSnackbar(v, "This feature is currently unavailable.");
                new Utility().showSnackbar(v, "This feature is currently unavailable.");
                break;

            case R.id.nearby:
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new MerchantWithdrawal())
                        .commit();
                break;
        }
    }
}
