package net.qpaysolutions.QPay.Withdrawal;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.Objects;

/**
 * Created by deadlydragger on 2/27/17.
 */

public class WithDrawablActivity extends AppCompatActivity {
    Toolbar toolbaruni;
    TextView textView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdrawal_main);
        toolbaruni=findViewById(R.id.toolbar);
        setSupportActionBar(toolbaruni);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment,new WithdrawalFragment())
                .commit();

    }

public void setTitleToolbar(String string){

    TextView toolbar_text=(TextView)toolbaruni.findViewById(R.id.title);
    toolbar_text.setText(string);
}
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent intent= new Intent(WithDrawablActivity.this, MainActivity.class);
        intent.putExtra("Check",6);
        startActivity(intent);*/
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
