package net.qpaysolutions.QPay.Withdrawal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.QRcodegenerator.Contents;
import net.qpaysolutions.QPay.QRcodegenerator.QRCodeEncoder;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 2/27/17.
 */

public class MerchantWithdrawal extends Fragment {
    QPayCustomerDatabase sctDatabaseHelper;
    String cust_id;
    ImageView my_code;
    private String app_id;
    QPayProgressDialog qPayProgressDialog;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sctDatabaseHelper = new QPayCustomerDatabase(getActivity());
        sctDatabaseHelper.getReadableDatabase();
        cust_id = sctDatabaseHelper.getCustomerID();
        app_id = sctDatabaseHelper.getKeyAppId();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        my_code = (ImageView) view.findViewById(R.id.create_my_qrcode);
        progressBar = view.findViewById(R.id.progressBar);
        ((WithDrawablActivity) getActivity()).setTitleToolbar("Merchant Withdraw");
//        create();
        new GETQRCode().execute();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.withdrawal_merchant, container, false);
    }

    public void create(String cust_id) {
//        String qrInputTextGeneral = sctDatabaseHelper.getEncryptedCustomerId();
        String qrInputText = null;
        String encryptedMsg = cust_id;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                if (bitmap != null) {
                    my_code.setImageBitmap(bitmap);
                } else {
                    create(cust_id);
                }


            } catch (WriterException e) {
                e.printStackTrace();
                try {
                    create(cust_id);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    startActivity(new Intent(getActivity(), WithDrawablActivity.class));
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });
    }

    public class GETQRCode extends AsyncTask<String, String, String> {


        public GETQRCode() {

        }

        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;
            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_balance.put("id", cust_id);
                jsonObject_balance.put("appId", app_id);
                jsonObject_balance.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_balance.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d("dinesh", jsonObject_balance.toString());
                return networkAPI.sendHTTPData(Constants.QRGENERATION, jsonObject_balance);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          progressBar.setVisibility(View.VISIBLE);
          my_code.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d(Utility.TAG, "post service : " + s);
//                qPayProgressDialog.dismiss();

                progressBar.setVisibility(View.GONE);
                my_code.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.getBoolean("success")) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject object = data.getJSONObject(0);
                    String emvCpqrcPayload = object.getString("emvCpqrcPayload");
                    create(emvCpqrcPayload);
                }
            } catch (Exception e) {
//                qPayProgressDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                my_code.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }
    }
}
