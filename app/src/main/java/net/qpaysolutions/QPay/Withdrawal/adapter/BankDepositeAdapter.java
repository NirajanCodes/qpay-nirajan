package net.qpaysolutions.QPay.Withdrawal.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import net.qpaysolutions.QPay.Withdrawal.BankPojo;

import java.util.ArrayList;
import java.util.List;

import net.qpaysolutions.QPay.R;

/**
 * Created by QPay on 5/6/2019.
 */

public class BankDepositeAdapter extends RecyclerView.Adapter<BankDepositeAdapter.BankDepositeViewHolder> implements Filterable {
    private List<BankPojo> bankPojos;
    private Context context;
    private ArrayList<BankPojo> filteredList;
    private BankDepositeClickListener bankDepositeClickListener;
    boolean nonFilerted = false;

    public BankDepositeAdapter(ArrayList<BankPojo> bankPojos, Context context, BankDepositeClickListener bankDepositeClickListener) {
        this.bankPojos = bankPojos;
        this.context = context;
        this.bankDepositeClickListener = bankDepositeClickListener;
        this.filteredList = bankPojos;
    }

    @NonNull
    @Override
    public BankDepositeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BankDepositeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_district, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BankDepositeViewHolder holder, int position) {
        try {
            holder.districtName.setText(filteredList.get(position).getBankName());
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        if (nonFilerted) {
            return filteredList.size();
        } else {
            return bankPojos.size();

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charlist = constraint.toString();
                ArrayList<BankPojo> filterableList = new ArrayList<>();
                if (charlist.isEmpty()) {
                    filterableList = (ArrayList<BankPojo>) bankPojos;
                    nonFilerted = false;
                } else {
                    nonFilerted = true;
                    for (BankPojo loopList : bankPojos) {
                        if (loopList.getBankName().toUpperCase().contains(charlist.toUpperCase())) {
                            filterableList.add(loopList);
                        }
                    }
                }
                filteredList = filterableList;
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<BankPojo>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    class BankDepositeViewHolder extends RecyclerView.ViewHolder {
        private TextView districtName;

        public BankDepositeViewHolder(View itemView) {
            super(itemView);
            districtName = itemView.findViewById(R.id.districtName);
            districtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bankDepositeClickListener.onClick(filteredList.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface BankDepositeClickListener {
        void onClick(BankPojo item);
    }
}
