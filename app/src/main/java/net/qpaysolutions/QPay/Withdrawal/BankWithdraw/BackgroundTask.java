package net.qpaysolutions.QPay.Withdrawal.BankWithdraw;

import android.os.AsyncTask;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import org.json.JSONObject;
import java.util.HashMap;

public class BackgroundTask extends AsyncTask<Void, HashMap, String> {

    GetJson getJson;
    String json;
    String url;

    public BackgroundTask(GetJson getJson, String json, String url) {

        this.getJson = getJson;
        this.json = json;
        this.url=url;

    }

    @Override
    protected String doInBackground(Void... voids) {
        JSONObject jsonObject = null;
        try {

            jsonObject = new JSONObject(json);
            String response = NetworkAPI.sendHTTPData(url, jsonObject);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        getJson.getJson(s);
    }

    interface GetJson {
        void getJson(String json);
    }
}