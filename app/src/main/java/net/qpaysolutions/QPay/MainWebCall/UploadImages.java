package net.qpaysolutions.QPay.MainWebCall;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 8/20/17.
 */

public class UploadImages extends AsyncTask<Void, Void, JSONObject> {
    String cust_id,encodedImage;
    Activity activity;
    QPayCustomerDatabase QPayCustomerDatabase;
    ProgressBar pb_header;

    public UploadImages(String cust_id, String encodedImage, Activity activity, QPayCustomerDatabase QPayCustomerDatabase, ProgressBar pb_header) {
        this.cust_id = cust_id;
        this.encodedImage = encodedImage;
        this.activity = activity;
        this.QPayCustomerDatabase = QPayCustomerDatabase;
        this.pb_header=pb_header;
    }

    @Override
    protected void onPreExecute() {
        pb_header.setVisibility(View.VISIBLE);
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        String line = null;
        JSONObject Response = null;
        JSONObject jsonObject = new JSONObject();
        NetworkAPI networkAPI = new NetworkAPI();
        try {
            jsonObject = new JSONObject();
            jsonObject.put("Id", cust_id);
            jsonObject.put("File", encodedImage);
            jsonObject.put("type", "1");
            String data = jsonObject.toString();
            Log.d(Utility.TAG, "psost imneg to url : " + data);
            String result = networkAPI.sendHTTPData(Constants.POST_IMAGE_QPAY, jsonObject);
            Log.d(Utility.TAG, "Response from php = " + result);
            Response = new JSONObject(result);
        } catch (Exception e) {
            Log.d(Utility.TAG, "Error Encountered");
            e.printStackTrace();
        }
        return Response;
    }

    @Override
    protected void onPostExecute(JSONObject args) {
        super.onPostExecute(args);
        pb_header.setVisibility(View.GONE);
        try {
//                progressDialog.dismiss();
            String http_status = args.getString("HttpStatus");
            String status = args.getString("Status");
            Log.d(Utility.TAG, "data return from server :" + args.toString());
            if (http_status != null && !http_status.isEmpty() && http_status.equals("200")) {
                if (status != null && !status.isEmpty() && status.equals("00")) {
                    String file = args.getString("ImageUrl");
                    QPayCustomerDatabase.updateImg(file);
                        /*Glide
                                .with(MainActivity.this)
                                .load(file)
                                .asBitmap()
                                .error(R.drawable.header_placeholder)
                                .into(new SimpleTarget<Bitmap>(300, 300) {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                                        custum_header_image.setImageBitmap(resource);
                                    }
                                });*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
