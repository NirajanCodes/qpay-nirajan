package net.qpaysolutions.QPay.MainWebCall;

import android.os.AsyncTask;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.GeneralPref;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 9/13/17.
 */

public class Checkbonusflag extends AsyncTask<String,String,String>
{
    String appId,id;
    @Override
    protected String doInBackground(String... params) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId",appId);
            jsonObject.put("id",id);
            return new NetworkAPI().sendHTTPData("",jsonObject);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            JSONObject jsonObject = new JSONObject(s);

            boolean success = jsonObject.getBoolean("success");
            if (success){
                JSONArray data = jsonObject.getJSONArray("data");
                JSONObject jsonObject1 = data.getJSONObject(0);
                boolean AllowBonus = jsonObject1.getBoolean("AllowBonus");
                if (AllowBonus){
                    GeneralPref.setAllowBonus(AllowBonus);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
