package net.qpaysolutions.QPay.MainWebCall;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 8/20/17.
 */


public class RegisterDeviceAsynk extends AsyncTask<Void, Void, JSONObject> {
    String token,app_id,cust_id;
    Activity activity;

    public RegisterDeviceAsynk(String token, String app_id, String cust_id, Activity activity) {
        this.token = token;
        this.app_id = app_id;
        this.cust_id = cust_id;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        String line = null;
        JSONObject jsonObject;
        JSONObject Response = null;
        NetworkAPI networkAPI = new NetworkAPI();
        try {
            jsonObject = new JSONObject();
            jsonObject.put("app_id", app_id);
            jsonObject.put("cust_id", cust_id);
            jsonObject.put("dev_token", token);
            jsonObject.put("lat", ((CustomerApplication) activity.getApplication()).getLat());
            jsonObject.put("lng", ((CustomerApplication) activity.getApplication()).getLng());
            jsonObject.put("device_type", "AND");
            Log.d(Utility.TAG, "post update : " + jsonObject);
            String result = networkAPI.sendHTTPData(Constants.REGISTER_DEVICE, jsonObject);
            Log.d("dinesh", "doInBackground: "+result);
            Response = new JSONObject(result);
            return Response;
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject args) {
        super.onPostExecute(args);
        try {
            JSONObject RegisterUpdateDeviceResult = args.getJSONObject("RegisterUpdateDeviceResult");
            String resp = RegisterUpdateDeviceResult.getString("resp");
            if (resp.equals("00")){
                SharedPreferences sharedPreferences = activity.getSharedPreferences("token", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor= sharedPreferences.edit();
                editor.putString("token_updated",token);
                editor.commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
