package net.qpaysolutions.QPay.MainWebCall;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 8/20/17.
 */

public class GetContactUsAsynk extends AsyncTask<String, String, String> {
    String app_id,cust_id;
    Activity activity;

    public GetContactUsAsynk(String app_id, String cust_id, Activity activity) {
        this.app_id = app_id;
        this.cust_id = cust_id;
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... uri) {
        NetworkAPI httpResponse = new NetworkAPI();
        JSONObject jsonObject = new JSONObject();
        String resukt = "";
        try {
            jsonObject.put("appId", app_id);
            jsonObject.put("id", cust_id);
            jsonObject.put("lat", ((CustomerApplication) activity.getApplication()).getLat());
            jsonObject.put("lng", ((CustomerApplication) activity.getApplication()).getLng());
            resukt = httpResponse.sendHTTPData(Constants.GET_CONTACT_US, jsonObject);
            Log.d("dinesh", "doInBackground: "+resukt);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return resukt;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {

            JSONObject jsonObject1 = new JSONObject(s);
            JSONObject jsonObject = jsonObject1.getJSONObject("GetMobileContentResult");
            boolean Success = jsonObject.getBoolean("Success");
            JSONObject Data = jsonObject.getJSONObject("Data");
            if (Success == true) {
                String url = Data.getString("WebUrl");
                String TermOfUseUrl = Data.getString("TermOfUseUrl");
                String PrivacyUrl = Data.getString("PrivacyUrl");
                String About = Data.getString("About");
                JSONArray phone_object = Data.getJSONArray("Phone");
                JSONArray Email_object = Data.getJSONArray("Email");
                for (int i = 0; i < phone_object.length(); i++) {
                    String phone = phone_object.getString(0);
                    GeneralPref.setphone(phone);
                    Log.d(Utility.TAG, "phone : " + phone);
                }
                for (int i = 0; i < Email_object.length(); i++) {
                    String email = Email_object.getString(0);
                    Log.d(Utility.TAG, "mail : " + email);
                   GeneralPref.setemail(email);
                }
                GeneralPref.seturl(url);
               GeneralPref.setTermOfUseUrl(TermOfUseUrl);
                GeneralPref.setPrivacyUrl(PrivacyUrl);
               GeneralPref.setAbout(About);
                Log.d(Utility.TAG, "termurl : " + TermOfUseUrl + " privacy : " + PrivacyUrl);

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }
}
