package net.qpaysolutions.QPay.Chat.chatList;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ChatViewHolderList extends RecyclerView.ViewHolder{
    ImageView url;
    TextView name,date,msg,message_count;
    public ChatViewHolderList(View itemView) {
        super(itemView);
        url=(ImageView)itemView.findViewById(R.id.url);
        name=(TextView)itemView.findViewById(R.id.name);
        msg=(TextView)itemView.findViewById(R.id.msg);
        date=(TextView)itemView.findViewById(R.id.date);
        message_count=(TextView)itemView.findViewById(R.id.message_count);
    }
}
