package net.qpaysolutions.QPay.Chat.library;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import net.qpaysolutions.QPay.Chat.chatmessageview.models.Message;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.User;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Wrapper class {@link ArrayList} for save data
 * Created by nakayama on 2017/01/13.
 */
public class MessageList {

    private ArrayList<SaveMessage> mMessages;

    public MessageList() {
        mMessages = new ArrayList<>();
    }

   /* public MessageList(List<Message> messageLists){
        mMessages = new ArrayList<>(convertMessage(messageLists));
    }*/

    public ArrayList<Message> getMessages() {
        ArrayList<Message> messages = new ArrayList<>();
        for (SaveMessage saveMessage : mMessages) {
            messages.add(convertMessage(saveMessage));
        }
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        for (Message message : messages) {
            mMessages.add(convertMessage(message));
        }
    }

    public void add(Message message) {
        mMessages.add(convertMessage(message));
    }
    public void delete(int position){
        //if(mMessages.size() > position)
        if(position > 0)
            mMessages.remove(position);
        else
            mMessages.remove(position);
        //else
            //mMessages.remove(position);
    }
    public void clear(){
        mMessages.clear();
    }


    public Message get(int index) {
        return convertMessage(mMessages.get(index));
    }
    public String getId(int index) {
        return mMessages.get(index).getId();
    }

    public int size() {
        return mMessages.size();
    }

    private SaveMessage convertMessage(Message message) {
        SaveMessage saveMessage = new SaveMessage(
                message.getUser().getId(),
                message.getUser().getName(),
                message.getMessageText(),
                message.getCreatedAt(),
                message.isRightMessage(), message.isBitmap(), message.getStatus());

        saveMessage.setType(message.getType());

        if (message.getType() == Message.Type.PICTURE
                 && message.isBitmap()) {

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            message.getPicture().compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            saveMessage.setPictureString(Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT));
        }else if(!message.isBitmap()){
            saveMessage.setPictureString(message.getPictureUrl());
        }

        return saveMessage;
    }

    private Message convertMessage(SaveMessage saveMessage) {
        User user = new User(saveMessage.getId(), saveMessage.getUsername(), null);

        Message message = new Message.Builder()
                .setUser(user)
                .setImageUrl(saveMessage.getContent())
                .setMessageText(saveMessage.getContent())
                .setRightMessage(saveMessage.isRightMessage())
                .setCreatedAt(saveMessage.getCreatedAt())
                .setType(saveMessage.getType())
                .setIsBitmap(saveMessage.isBitmap())
                .setStatus(saveMessage.getStatus())
                .build();

        if (saveMessage.getPictureString() != null && saveMessage.isBitmap()) {
            byte[] bytes = Base64.decode(saveMessage.getPictureString().getBytes(), Base64.DEFAULT);
            message.setPicture(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
        }else if(!saveMessage.isBitmap()){
            message.setmPicture(saveMessage.getPictureString());
        }
        return message;
    }

    private class SaveMessage {
        private String mId;
        private String mUsername;
        private String mContent;
        private Calendar mCreatedAt;
        private boolean mRightMessage;
        private String mPictureString;
        private Message.Type mType;
        private boolean isBitmap;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        private int status;

        public boolean isBitmap() {
            return isBitmap;
        }

        public void setBitmap(boolean bitmap) {
            isBitmap = bitmap;
        }



        public SaveMessage(String id, String username, String content, Calendar createdAt, boolean isRightMessage, boolean isBitmap, int status) {
            mId = id;
            mUsername = username;
            mContent = content;
            mCreatedAt = createdAt;
            mRightMessage = isRightMessage;
            this.isBitmap = isBitmap;
            this.status = status;
        }

        public String getId() {
            return mId;
        }

        public String getUsername() {
            return mUsername;
        }

        public String getContent() {
            return mContent;
        }

        public Calendar getCreatedAt() {
            return mCreatedAt;
        }

        public boolean isRightMessage() {
            return mRightMessage;
        }

        public String getPictureString() {
            return mPictureString;
        }

        public void setPictureString(String pictureString) {
            mPictureString = pictureString;
        }

        public Message.Type getType() {
            return mType;
        }

        public void setType(Message.Type type) {
            mType = type;
        }
    }
}
