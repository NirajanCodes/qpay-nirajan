package net.qpaysolutions.QPay.Chat.qpaychat.eventbus;

public class ChatMessageEvent {
    private String message;

    public ChatMessageEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}