package net.qpaysolutions.QPay.Chat.qpaychat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.qpaysolutions.QPay.Chat.qpaychat.adapter.MessageListAdapter;
import net.qpaysolutions.QPay.Chat.qpaychat.database.MessageDao;
import net.qpaysolutions.QPay.Chat.qpaychat.eventbus.ChatMessageEvent;
import net.qpaysolutions.QPay.Chat.qpaychat.eventbus.TypingEvent;
import net.qpaysolutions.QPay.Chat.qpaychat.model.ConversationPOJO;
import net.qpaysolutions.QPay.Chat.qpaychat.model.GetMessagePOJO;
import net.qpaysolutions.QPay.Chat.qpaychat.model.MessageItem;
import net.qpaysolutions.QPay.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class QpayChat extends Fragment {

    private EditText chatBox;
    private ImageView sendMessage;
    private RecyclerView chatRecyclerView;
    private MessageListAdapter messageListAdapter;
    private List<ConversationPOJO> conversationPOJOS;
    private TextView isTyping;
    private MessageDao messageDao;
    private final int SELECTED_INDEX = 0;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        messageDao = new MessageDao(getContext());

      /*  chatRecyclerView = view.findViewById(R.id.reyclerview_message_list);
        chatRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        messageListAdapter = new MessageListAdapter(getActivity(),
                messageDao.getMessagesByCustomerId(Network.PARTNER_CUST_ID));
        chatRecyclerView.setAdapter(messageListAdapter);

        chatBox = view.findViewById(R.id.edittext_chatbox);
        sendMessage = view.findViewById(R.id.button_chatbox_send);
        isTyping = view.findViewById(R.id.tv_isTyping);
        chatBox.setText(Network.CUST_ID);*/
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chatBox.getText().toString().isEmpty()) {
                    chatBox.setError("Empty Message");
                } else {
                    postMessage(chatBox.getText().toString().trim());
                    chatBox.setText("");

                }
            }
        });

//        chatBox.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                postIsTyping();
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(container.getContext()).inflate(R.layout.activity_chat_conversation,container,false);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when a MessageEvent is posted
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ChatMessageEvent event) {
        Toast.makeText(getContext(), "notification", Toast.LENGTH_SHORT).show();
//        Gson gson = new Gson();
//        ReceivedMessagePOJO receivedMessagePOJO = gson.fromJson(event.getMessage(), ReceivedMessagePOJO.class);
//        conversationPOJOS.add(new ConversationPOJO(true, 0,
//                receivedMessagePOJO.getMessage(), false, Network.PARTNER_CUST_ID));
//        messageListAdapter.updateData(conversationPOJOS);
//        chatRecyclerView.scrollToPosition(conversationPOJOS.size() - 1);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TypingEvent event) {
        Toast.makeText(getContext(), "notification", Toast.LENGTH_SHORT).show();
//        Gson gson = new Gson();
//        ReceivedMessagePOJO receivedMessagePOJO = gson.fromJson(event.getMessage(), ReceivedMessagePOJO.class);
//        conversationPOJOS.add(new ConversationPOJO(true, 0,
//                receivedMessagePOJO.getMessage(), false, Network.PARTNER_CUST_ID));
//        messageListAdapter.updateData(conversationPOJOS);
//        chatRecyclerView.scrollToPosition(conversationPOJOS.size() - 1);

    }

    private void postMessage(final String message) {
        Network.postMessage(message, getActivity(), new NetworkResponse() {
            @Override
            public void onSuccess(String data) {
                //if success tick
            }

            @Override
            public void onError(String data) {
                //send fail
            }
        });
    }

    private void postIsTyping() {
        Network.postIsTyping(Network.USER_NAME, getActivity(), new NetworkResponse() {
            @Override
            public void onSuccess(String data) {

            }

            @Override
            public void onError(String data) {

            }
        });
    }

    private void setupChatView() {


        //  chatRecyclerView.scrollToPosition(conversationPOJOS.size() - 1);

        conversationPOJOS = new ArrayList<>();
        Network.getMessage(getActivity(), messageDao.getLatestMessageIdByCustomerId(Network.PARTNER_CUST_ID), new NetworkResponse() {
            @Override
            public void onSuccess(String data) {
                Gson gson = new Gson();

                GetMessagePOJO receivedMessagePOJO = gson.fromJson(data, GetMessagePOJO.class);
                for (int i = 0; i < receivedMessagePOJO.getData().get(0).getMessage().size(); i++) {
                    messageDao.insertMessage(new MessageItem(receivedMessagePOJO.getData().get(0).getMessage().get(i).getMessage(),
                            false, receivedMessagePOJO.getData().get(0).getMessage().get(i).getId()));

                    conversationPOJOS.add(new ConversationPOJO(true, 0,
                            receivedMessagePOJO.getData().get(0).getMessage().get(i).getMessage(), false, Network.PARTNER_CUST_ID));
                }
                messageListAdapter.updateData(conversationPOJOS);
                chatRecyclerView.scrollToPosition(conversationPOJOS.size() - 1);

            }

            @Override
            public void onError(String data) {

            }
        });
    }
}
