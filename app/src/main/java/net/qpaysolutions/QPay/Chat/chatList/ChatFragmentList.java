package net.qpaysolutions.QPay.Chat.chatList;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.qpaysolutions.QPay.Cache.AppCache;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.Message;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.User;
import net.qpaysolutions.QPay.Chat.conversation.ConversationModule;
import net.qpaysolutions.QPay.Chat.library.AppData;
import net.qpaysolutions.QPay.Chat.library.ChatLibraryActivity;
import net.qpaysolutions.QPay.Chat.library.MessageList;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.NearByShop.NearbyShopListActivity;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.SEARCH_SERVICE;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ChatFragmentList extends Fragment {
    private   RecyclerView chat_recycle_view;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private  ChatAdapterList chatAdapterList;
    private  String lastMessage = "none";
    private SharedPreferences sharedPreferences;
    private String FILE = "chat";
    private String phoneNo;
    private String KEY = "id", AppId, CustId;
    private String TAG = "dinesh";
    private AsyncTask myTask = null;
    private RelativeLayout noChatLayout;
    private QPayProgressDialog qPayProgressDialog;
    private SQLiteDatabase sqLiteDatabase;
    private boolean isOpen = false;
    private SharedPreferences.Editor editor;
    private int id;
    private String MessageIdMessag;
    private  String senderIdMessage, msg;
    private String LAST_MESSAGE_KEY = "last_message";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        sqLiteDatabase = QPayCustomerDatabase.getWritableDatabase();
        QPayCustomerDatabase.updateTotalUnreadMsg("0");
        AppCache.setChatModelLists(QPayCustomerDatabase.getAllChatList(0));
        AppId = QPayCustomerDatabase.getKeyAppId();
        CustId = QPayCustomerDatabase.getCustomerID();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(FILE, MODE_PRIVATE);
        Log.d(TAG, "onViewCreated: " + DateFormat.getDateTimeInstance().format(new Date()));
        chat_recycle_view = (RecyclerView) view.findViewById(R.id.chat_recycle_view);
        noChatLayout = (RelativeLayout) view.findViewById(R.id.rv_no_chat);
        chat_recycle_view.setHasFixedSize(true);
        chat_recycle_view.setLayoutManager(new LinearLayoutManager(getContext()));

        if (!AppCache.getChatModelLists().isEmpty()) {
            chat_recycle_view.setVisibility(View.VISIBLE);
            noChatLayout.setVisibility(View.GONE);

        }


        chatAdapterList = new ChatAdapterList(ChatFragmentList.this, AppCache.getChatModelLists());
        chat_recycle_view.setAdapter(chatAdapterList);

        chat_recycle_view.addOnItemTouchListener(new NearbyShopListActivity.RecyclerTouchListener(getContext(), chat_recycle_view, new NearbyShopListActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (!isOpen) {
                    try {
                        editor = sharedPreferences.edit();
                        editor.putString("url", AppCache.getChatModelLists().get(position).getImg_url_chat());
                        editor.putString("name", AppCache.getChatModelLists().get(position).getName_chat());
                        editor.putString("id", AppCache.getChatModelLists().get(position).getCust_id_friend_chat());
                        editor.putString("phone", AppCache.getChatModelLists().get(position).getCell_no_chat());
                        editor.putString("_id", AppCache.getChatModelLists().get(position).getCell_no_chat());
                        editor.putInt("msg_id", AppCache.getChatModelLists().get(position).get_id());
                        editor.putString("cust_id", AppCache.getChatModelLists().get(position).getCust_id_friend_chat());
                        editor.apply();

                        AppCache.setUserNumber(AppCache.getChatModelLists().get(position).getCell_no_chat());
                        //LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);

                        //AppCache.setMessageList(mMessageList);
                        startActivity(new Intent(getActivity(), ChatLibraryActivity.class));
                        //getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //Log.d("position_id", "onClick: " +chatModelLists.get(position).get_id()chatModelLists.get(position).get_id());


                }
            }

            @Override
            public void onLongClick(View view, int position) {
                Log.d(TAG, "onLongClick: " + position);
                if (!isOpen) {
                    isOpen = true;
                    deleteConversation(AppCache.getChatModelLists().get(position).getCust_id_friend_chat(),
                            AppCache.getChatModelLists().get(position).getName_chat(),
                            String.valueOf(AppCache.getChatModelLists().get(position).getCell_no_chat()));
                }
            }
        }));


        // if(Globale.isActivityVisible())
        //qPayProgressDialog.show();
//        new GetUreadMessage().execute();
        new Utility().hideSoftkey(getActivity());

        if (AppCache.getChatModelLists().isEmpty()) {
            chat_recycle_view.setVisibility(View.GONE);
            noChatLayout.setVisibility(View.VISIBLE);
        }


    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            chat_recycle_view.setVisibility(View.VISIBLE);
            noChatLayout.setVisibility(View.GONE);

            final String message = intent.getStringExtra("Message");
            phoneNo = intent.getStringExtra("PhoneNumber");
            final String senderName = intent.getStringExtra("SenderName");
            String senderId = intent.getStringExtra("SenderId");
            String imageUrl = intent.getStringExtra("ImageUrl");
            String date = intent.getStringExtra("date");
            String receiverId = intent.getStringExtra("ReceiverId");

            final String messageID = intent.getStringExtra("MessageId");
            boolean isImage = intent.getBooleanExtra("isImage", false);
            String NoOfUnreadMsg = intent.getStringExtra("NoOfUnreadMsg");
            if (AppCache.getLastMessage() == null || !AppCache.getLastMessage().equalsIgnoreCase(messageID)) {

                AppCache.setLastMessage(messageID);

                if (AppCache.getUserNumber() != null && !phoneNo.equalsIgnoreCase(AppCache.getUserNumber())) {

                    handleMessageBroadcast(message, senderName,
                            senderId, imageUrl, date, messageID, isImage, NoOfUnreadMsg);

                } else if (AppCache.getUserNumber() == null) {

                    handleMessageBroadcast(message, senderName,
                            senderId, imageUrl, date, messageID, isImage, NoOfUnreadMsg);
                }
            }

            //   new GetMessageById(messageID, senderId, AppId, receiverId).execute();


        }
    };

    private void handleMessageBroadcast(final String message, final String senderName,
                                        final String senderId, final String imageUrl, final String date,
                                        final String messageID, boolean isImage,
                                        final String noOfUnreadMsg) {

        if (isImage
                && message != null
                && !message.isEmpty()
                &&
                (lastMessage.equalsIgnoreCase("none") || !lastMessage.equalsIgnoreCase(message))) {

            AppCache.getMessageList().clear();

            if (QPayCustomerDatabase.isAlreadyInChatListcontact(phoneNo) > 0) {
                Log.d(TAG, "onPostExecute: already " + senderName);
                setUserId(phoneNo);
                QPayCustomerDatabase.updateTotalUnreadMsgCustomer(noOfUnreadMsg, position(phoneNo));
                QPayCustomerDatabase.updateLastyMsg(senderId, "You Received an Image.");
            } else {
                ChatModelList chatModelList = new ChatModelList(phoneNo, senderName, senderId, imageUrl, date, "", noOfUnreadMsg);
                QPayCustomerDatabase.insertChatListData(chatModelList);

                QPayCustomerDatabase.updateTotalUnreadMsgCustomer(noOfUnreadMsg, position(phoneNo));
                QPayCustomerDatabase.updateLastyMsg(senderId, "You Received an Image.");
               /* ConversationModule conversationModule = new ConversationModule(senderId, "", date, "0", "0", messageID, message, "1");
                qpayDatabases.insertConversationData(conversationModule);
*/
            }


            User user;
            try {
                user = AppCache.getmUsers().get(1);
            } catch (Exception e) {
                user = new User(messageID, senderName, null);
            }
            Message message1 = new Message.Builder()
                    .setUsernameVisibility(false)
                    .hideIcon(false)
                    .setRightMessage(false)
                    .setUser(user)
                    .setIsBitmap(false)
                    .setImageUrl(message)
                    .setMessageText(Message.Type.PICTURE.name())
                    .setType(Message.Type.PICTURE)
                    .build();

            AppCache.setMessageList(AppData.getMessageList(CustomerApplication.getContext(), phoneNo));
            AppCache.getMessageList().add(message1);

            AppData.putMessageList(CustomerApplication.getContext(),
                    AppCache.getMessageList(), String.valueOf(phoneNo));


            AppCache.getChatModelLists().clear();
            AppCache.getChatModelLists().addAll(QPayCustomerDatabase.getAllChatList(1));

            chatAdapterList.updateData(AppCache.getChatModelLists());
            //chatAdapterList.notifyDataSetChanged();
        } else {

            if (QPayCustomerDatabase.isAlreadyInChatListcontact(phoneNo) > 0) {
                Log.d(TAG, "onPostExecute: already " + senderName);
                setUserId(phoneNo);
                QPayCustomerDatabase.updateTotalUnreadMsgCustomer(noOfUnreadMsg, position(phoneNo));
                QPayCustomerDatabase.updateLastyMsg(senderId, message);
            } else {
                ChatModelList chatModelList = new ChatModelList(phoneNo, senderName, senderId, imageUrl, date, "", noOfUnreadMsg);
                QPayCustomerDatabase.insertChatListData(chatModelList);

                QPayCustomerDatabase.updateTotalUnreadMsgCustomer(noOfUnreadMsg, position(phoneNo));
                QPayCustomerDatabase.updateLastyMsg(senderId, message);
                /*ConversationModule conversationModule = new ConversationModule(senderId, "", date, "0", "0", messageID, message, "1");
                qpayDatabases.insertConversationData(conversationModule);*/


            }


            User user;

            try {
                user = AppCache.getmUsers().get(1);
            } catch (Exception e) {
                user = new User(messageID, senderName, null);
            }
            final Message receivedMessage = new Message.Builder()
                    .setUsernameVisibility(false)
                    .hideIcon(false)
                    .setUser(user)
                    .setRightMessage(false)
                    .setMessageText(message)
                    .build();

            AppCache.setMessageList(AppData.getMessageList(CustomerApplication.getContext(), phoneNo));
            AppCache.getMessageList().add(receivedMessage);

            AppData.putMessageList(CustomerApplication.getContext(),
                    AppCache.getMessageList(), String.valueOf(phoneNo));


            AppCache.getChatModelLists().clear();
            AppCache.getChatModelLists().addAll(QPayCustomerDatabase.getAllChatList(1));

            chatAdapterList.updateData(AppCache.getChatModelLists());
        }


    }


    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_fragment, container, false);
    }


    public void deleteConversation(final String cust_id_friend_chat, String name, final String phoneNo) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custom_dialog_chat_delet, null);
        final TextView delet_chat = (TextView) v.findViewById(R.id.delet_chat);
        final TextView cancel_delet = (TextView) v.findViewById(R.id.cancel_delet);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(name);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        delet_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isOpen = false;

                MessageList messageList = new MessageList();

                AppData.deleteChat(CustomerApplication.getContext(), phoneNo);

                AppData.putMessageList(CustomerApplication.getContext(),
                        messageList, String.valueOf(phoneNo));

                QPayCustomerDatabase.updateTotalUnreadMsgCustomer("0", position(phoneNo));
                // qpayDatabases.updateLastyMsg(senderId, "You Received an Image.");

                /*AppData.putMessageList(getContext(), , phoneNo);*/
                new DeleteMessage(cust_id_friend_chat, "", true).execute();


            }
        });
        cancel_delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isOpen = false;
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    private class GetMessageById extends AsyncTask<String, String, String> {
        String MessageId;
        String senderId, appId, receiverId;

        public GetMessageById(String messageId, String senderId, String appId, String receiverId) {
            MessageId = messageId;
            this.senderId = senderId;
            this.appId = appId;
            this.receiverId = receiverId;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String sendMessage = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("SenderId", senderId);
                jsonObject.put("ReceiverId", receiverId);
                jsonObject.put("AppId", appId);
                jsonObject.put("MessageId", MessageId);
                Log.d(TAG, "GetMessageById: " + jsonObject);
                sendMessage = NetworkAPI.sendHTTPData(Constants.GET_MESSAGE_BY_ID, jsonObject);
                Log.d(TAG, "GetMessageById vitra : " + sendMessage);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return sendMessage;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

    }

    public void setImage(String url, ImageView imageView) {
        if (!url.isEmpty()) {
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.header_placeholder)
                    .error(R.drawable.header_placeholder)
                    .into(imageView);
        }

    }


    private void initUsers(String url, String yourId) {
        if (url != null && !url.isEmpty()) {

            //User id
            String myId = "0";
            //User icon

            //User name
            String myName = "";
            final Bitmap[] myIcon = new Bitmap[1];
            if (url != null && !url.isEmpty()) {
                Picasso.get()
                        .load(url)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                                /* Save the bitmap or do something with it here */

                                //Set it in the ImageView
//                        theView.setImageBitmap(bitmap);
                                myIcon[0] = bitmap;
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                            }

                        });
            }

            Bitmap yourIcon;
            String yourName = "";
            final Bitmap[] finalYourIcon = new Bitmap[1];
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.header_placeholder)
                    .error(R.drawable.header_placeholder)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                            /* Save the bitmap or do something with it here */

                            //Set it in the ImageView
//                        theView.setImageBitmap(bitmap);
                            finalYourIcon[0] = bitmap;
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }


                    });
            final User me = new User(myId, myName, myIcon[0]);
            final User you = new User(yourId, yourName, finalYourIcon[0]);

            AppCache.getmUsers().add(me);
            AppCache.getmUsers().add(you);
        }
    }

    public int position(String phone) {

        id = QPayCustomerDatabase.getPosition(phone);
        Log.d("position", "position: " + id + " phone : " + phone);
        return id;
    }


    private class GetUreadMessage extends AsyncTask<String, String, String> {


        boolean start = false;

        GetUreadMessage(boolean start) {
            this.start = start;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            qPayProgressDialog.dismiss();

        }

        @Override
        protected String doInBackground(String... params) {

            String GetUreadMessage = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("AppId", AppId);
                jsonObject.put("CustId", CustId);
                Log.d(TAG, "GetUreadMessage: " + jsonObject);
                if (Utility.isConnected()) {
                    GetUreadMessage = NetworkAPI.sendHTTPData(Constants.GET_UNREAD_MESSAGE, jsonObject);
                } else
                    Toast.makeText(getContext(), "Network Connection Error", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return GetUreadMessage;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            qPayProgressDialog.hide();

            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                JSONArray data = jsonObject.getJSONArray("data");
                String message = jsonObject.getString("message");
                /*if(message.equalsIgnoreCase("No Record Found")){


                }*/
                // if()
                if (success) {
                    if (data.length() > 0) {
                        for (int i = 0; i < data.length(); i++) {
                            noChatLayout.setVisibility(View.GONE);
                            chat_recycle_view.setVisibility(View.VISIBLE);

                            DateFormat df = new SimpleDateFormat("hh:mm:ss a");//E,MMM d, yyyy
                            String date = df.format(Calendar.getInstance().getTime());
                            JSONObject jsonObject1 = data.getJSONObject(i);
                            String SenderId = jsonObject1.getString("SenderId");
                            String SenderName = jsonObject1.getString("SenderName");
                            String PhoneNo = jsonObject1.getString("PhoneNo");
                            phoneNo = PhoneNo;
                            String ImageUrl = jsonObject1.getString("ImageUrl");
                            String TotalUnreadMsg = jsonObject1.getString("TotalUnreadMsg");
                            JSONArray ChatMessages = jsonObject1.getJSONArray("ChatMessages");

                            if (Integer.parseInt(TotalUnreadMsg) > 0) {
                                initUsers(ImageUrl, String.valueOf(PhoneNo));


                                if (QPayCustomerDatabase.isAlreadyInChatListcontact(PhoneNo) > 0) {
                                    Log.d(TAG, "onPostExecute: already " + SenderName);
                                    setUserId(PhoneNo);
                                    QPayCustomerDatabase.updateTotalUnreadMsgCustomer(TotalUnreadMsg, position(PhoneNo));
                                } else {
                                /*position(PhoneNo);
                                initUsers(ImageUrl,  position(PhoneNo));*/
                                    //mMessageList = new MessageList();
                                    ChatModelList chatModelList = new ChatModelList(PhoneNo, SenderName, SenderId, ImageUrl, date, "", TotalUnreadMsg);
                                    QPayCustomerDatabase.insertChatListData(chatModelList);


                                }
                                editor = sharedPreferences.edit();
                                for (int j = 0; j < ChatMessages.length(); j++) {
                                    JSONObject jsonObject2 = ChatMessages.getJSONObject(j);
                                    String Message = jsonObject2.getString("Message").replace("'", "");
                                    String SenderIdMessage = jsonObject2.getString("SenderId");
                                    String ReceiverIdMessage = jsonObject2.getString("ReceiverId");
                                    String MessageIdMessage = jsonObject2.getString("MessageId");
                                    String Date = jsonObject2.getString("Date");
                                    boolean IsImage = jsonObject2.getBoolean("IsImage");
                                    senderIdMessage = SenderIdMessage;
                                    msg = Message;
                                    if (!sharedPreferences.getString("last_message", "None").equalsIgnoreCase(MessageIdMessage)) {
                                        if (IsImage) {

                                            ConversationModule conversationModule = new ConversationModule(SenderIdMessage, "", Date, "0", "0", MessageIdMessage, Message, "1");
                                            QPayCustomerDatabase.insertConversationData(conversationModule);
                                            //receiveImage(Message);
                                            QPayCustomerDatabase.updateTotalUnreadMsgCustomer(TotalUnreadMsg, position(phoneNo));
                                            QPayCustomerDatabase.updateLastyMsg(SenderId, "You Received an Image.");

                                        } else {
                                            ConversationModule conversationModule = new ConversationModule(SenderIdMessage, Message, Date, "0", "0", MessageIdMessage, "", "0");
                                            QPayCustomerDatabase.insertConversationData(conversationModule);
                                            receiveMessage(Message);
                                            QPayCustomerDatabase.updateTotalUnreadMsgCustomer(TotalUnreadMsg, position(phoneNo));
                                            QPayCustomerDatabase.updateLastyMsg(SenderId, msg);
                                        }

                                        QPayCustomerDatabase.updateLastyDate(SenderIdMessage, Date);
                                        MessageIdMessag = MessageIdMessage;

                                        editor.putString(String.valueOf(position(PhoneNo)) + "last_message", MessageIdMessag);
                                        editor.apply();
                                    }

                                }
                              /*  if (qpayDatabases.isAlreadyInChatListcontact(phoneNo) > 0) {
                                    Log.d(TAG, "onPostExecute: already " + SenderName);
                                    //setUserId(phoneNo);
                                    qpayDatabases.updateTotalUnreadMsgCustomer(TotalUnreadMsg, position(phoneNo));
                                    qpayDatabases.updateLastyMsg(SenderId, msg);
                                }*/
                               /* Log.d(TAG, "onPostExecute: last message " + senderIdMessage + " " + msg);
                                qpayDatabases.updateLastyMsg(senderIdMessage, msg);*/

                                Log.d("msgId", " Msg Id onPostExecute: " + MessageIdMessag + "position " + String.valueOf(PhoneNo));
                                Log.d(TAG, "put to : " + String.valueOf(PhoneNo));
                                // AppData.putMessageList(Globale.getContext(), AppCache.getMessageList(), String.valueOf(PhoneNo));
                                Log.d("position_id", "onPostExecute: " + String.valueOf(PhoneNo));
                            }


                        }

                    }

                    AppCache.getChatModelLists().clear();
                    AppCache.getChatModelLists().addAll(QPayCustomerDatabase.getAllChatList(1));
                    // AppData.putMessageList(getContext(), AppCache.getMessageList(), phoneNo);
                    //chatModelLists.
                          /*  chat_recycle_view.setLayoutManager(new LinearLayoutManager(getContext()));
                            chatAdapterList = new ChatAdapterList(ChatFragmentList.this, chatModelLists);
                            chat_recycle_view.setAdapter(chatAdapterList);*/
                    //chatAdapterList.notifyDataSetChanged();
                    chatAdapterList.updateData(AppCache.getChatModelLists());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                // if(Globale.isActivityVisible()){

                qPayProgressDialog = new QPayProgressDialog(getActivity());
                qPayProgressDialog.show();
                //}

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }


    public void setUserId(String Id) {
        AppCache.setYourID(Id);

        AppCache.setMessageList(AppData.getMessageList(CustomerApplication.getContext(), String.valueOf(AppCache.getYourID())) == null ?
                new MessageList() : AppData.getMessageList(CustomerApplication.getContext(), String.valueOf(AppCache.getYourID())));

    }

    private void receiveMessage(String sendText) {

        try {
            final Message receivedMessage = new Message.Builder()
                    .setUsernameVisibility(false)
                    .hideIcon(false)
                    .setUser(AppCache.getmUsers().get(1))
                    .setRightMessage(false)
                    .setMessageText(sendText)
                    .build();
            AppCache.getMessageList().add(receivedMessage);
            Log.d(TAG, "receiveMessage: " + AppCache.getMessageList().get(AppCache.getMessageList().size() - 1));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private class DeleteMessage extends AsyncTask<String, String, String> {
        String SenderId, MessageId;
        boolean IsDeleteAll;

        public DeleteMessage(String SenderId, String messageId, boolean IsDeleteAll) {
            this.SenderId = SenderId;
            this.MessageId = messageId;
            this.IsDeleteAll = IsDeleteAll;
        }

        @Override
        protected String doInBackground(String... params) {

            String DeleteMessage = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("SenderId", CustId);
                jsonObject.put("ReceiverId", SenderId);
                jsonObject.put("AppId", AppId);
                jsonObject.put("IsDeleteAll", IsDeleteAll);
                jsonObject.put("MessageId", MessageId);
                Log.d(TAG, "DeleteMessage: " + DeleteMessage);
                DeleteMessage = NetworkAPI.sendHTTPData(Constants.DELET_CHAT_MSG, jsonObject);
                Log.d(TAG, "DeleteMessage: " + DeleteMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return DeleteMessage;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (CustomerApplication.isActivityVisible()) {
                // qPayProgressDialog = new QPayProgressDialog(getActivity());
                //qPayProgressDialog.show();
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (CustomerApplication.isActivityVisible())
                //qPayProgressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        QPayCustomerDatabase.deletChatList(SenderId);
                        QPayCustomerDatabase.deletChatConversation(SenderId);
                        AppCache.getChatModelLists().clear();
                        AppCache.getChatModelLists().addAll(QPayCustomerDatabase.getAllChatList(0));
                        chatAdapterList.updateData(AppCache.getChatModelLists());
                        //chatAdapterList.notifyDataSetChanged();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (AppCache.getChatModelLists().isEmpty()) {
                chat_recycle_view.setVisibility(View.GONE);
                noChatLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    public String getDay() {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH) + 1;
        String date = calendar.get(Calendar.YEAR) + "-" + month + "-" + calendar.get(Calendar.DAY_OF_MONTH);
        Log.d(TAG, "getDay: " + date);
        return date;
    }

    @Override
    public void onPause() {
        super.onPause();
        CustomerApplication.activityPaused();
        getActivity().unregisterReceiver(broadcastReceiver);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: chatList");
        QPayCustomerDatabase.updateTotalUnreadMsg("0");
        getContext().registerReceiver(broadcastReceiver,
                new IntentFilter("custom-event-name"));
        AppCache.getChatModelLists().clear();
        AppCache.getChatModelLists().addAll(QPayCustomerDatabase.getAllChatList(0));
//        new GetUreadMessage(false).execute();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
