package net.qpaysolutions.QPay.Chat.qpaychat.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.Chat.qpaychat.ChatConversationListener;
import net.qpaysolutions.QPay.Chat.qpaychat.model.ConversationList;
import net.qpaysolutions.QPay.R;

import java.util.List;


public class ChatConversationAdapter extends RecyclerView.Adapter<ChatConversationAdapter.ViewHolder> {

    List<ConversationList> conversationLists;
    ChatConversationListener chatConversationListener;
    public ChatConversationAdapter(List<ConversationList> conversationLists, ChatConversationListener chatConversationListener) {
        this.conversationLists = conversationLists;
        this.chatConversationListener = chatConversationListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_conversation, viewGroup, false);
        return new ChatConversationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        ConversationList conversationList = conversationLists.get(i);
        holder.userName.setText(conversationList.getUserName());
        holder.lastMessage.setText(conversationList.getLastMessage());
    }

    @Override
    public int getItemCount() {
        return conversationLists != null ? conversationLists.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView userImage;
        private TextView userName, lastMessage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            userImage =  itemView.findViewById(R.id.iv_user_image);
            userName  =  itemView.findViewById(R.id.tv_user_name);
            lastMessage = itemView.findViewById(R.id.tv_last_messasge);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chatConversationListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}
