package net.qpaysolutions.QPay.Chat.qpaychat.database;

public class MessageDB {
    public static final String TABLE_NAME = "Message";

    public static final String COLUMN_ID = "columns_id";
    public static final String MESSAGE_ID = "message_id";
    public static final String MESSAGE = "message";
    public static final String SENDER_ID = "sender_id";
    public static final String IS_IMAGE = "is_image";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + MESSAGE_ID + " INTEGER,"
                    + MESSAGE + " TEXT,"
                    + IS_IMAGE + " INTEGER,"
                    + SENDER_ID + " TEXT"
                    + ")";
}
