package net.qpaysolutions.QPay.Chat.contact;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class Contact {
    String name,number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
