package net.qpaysolutions.QPay.Chat.contact;

import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.User;
import net.qpaysolutions.QPay.Chat.library.ChatLibraryActivity;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.SEARCH_SERVICE;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ContactFragment extends Fragment implements View.OnClickListener {
    static ArrayList<Contact> alContacts = new ArrayList<>();
    RecyclerView recyclerView;
    static ContactRecycleAdapter contactRecycleAdapter;
    String AppId, CustId;
    QPayCustomerDatabase QPayCustomerDatabase;
    String TAG = "dinesh";
    ArrayList<ContactSync> contactSyncs = new ArrayList<>();
    SQLiteDatabase sqLiteDatabase;
    SharedPreferences sharedPreferences;
    String FILE = "chat";
    public String KEY = "id";
    net.qpaysolutions.QPay.FloatingactionButton.FloatingActionButton add_contact;
    private ArrayList<User> mUsers;
    private static FrameLayout contact_progress;

    private static final int REQUEST = 112;
    boolean _areLecturesLoaded;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contact, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        sqLiteDatabase = QPayCustomerDatabase.getWritableDatabase();
        AppId = QPayCustomerDatabase.getKeyAppId();
        CustId = QPayCustomerDatabase.getCustomerID();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(FILE, MODE_PRIVATE);
        recyclerView = (RecyclerView) view.findViewById(R.id.contact_recycle_view);
        contact_progress = (FrameLayout) view.findViewById(R.id.contact_progress);
        contact_progress.setVisibility(View.GONE);
        add_contact = view.findViewById(R.id.add_contact);
        add_contact.setOnClickListener(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void runContact() {
        new GetAllContact().execute();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !_areLecturesLoaded) {
            if (Build.VERSION.SDK_INT >= 23) {
                String[] PERMISSIONS = {android.Manifest.permission.READ_CONTACTS};
                if (!((ChatActivity) getActivity()).hasPermissions(getContext(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST);
                } else {
                    new GetAllContact().execute();
                }
            } else {
                new GetAllContact().execute();
            }
            _areLecturesLoaded = true;
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search_contact, menu);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (contactRecycleAdapter != null) {
                    contactRecycleAdapter.getFilter().filter(newText);

                }
                return false;
            }
        });
    }

    /**
     * Getting all contact load in background
     */
    public class GetAllContact extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            listAllcontact();

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                contact_progress.setVisibility(View.GONE);
                contactRecycleAdapter = new ContactRecycleAdapter(alContacts, ContactFragment.this, contactSyncs);
                recyclerView.setAdapter(contactRecycleAdapter);
                new SynkContact().execute();
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        if (dy > 0) {
                            add_contact.setVisibility(View.GONE);
                        } else {
                            add_contact.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                            add_contact.setVisibility(View.VISIBLE);
                        }

                        super.onScrollStateChanged(recyclerView, newState);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public void listAllcontact() {
        try {
            ContentResolver cr = getContext().getContentResolver(); //Activity/Application android.content.Context
            Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor.moveToFirst()) {

                do {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        while (pCur.moveToNext()) {
                            Contact contact = new Contact();
                            String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String contactName = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            contact.setName(contactName);
                            contact.setNumber(contactNumber.replace("+977", "").replace("-", ""));
                            alContacts.add(contact);
                            break;
                        }
                        pCur.close();
                    }

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_contact:
                startActivity(new Intent(getActivity(), AddContactActivity.class));
                break;
        }
    }

    public class SynkContact extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String synck_contact = "";
            try {
                JSONObject jsonObject = new JSONObject();
                JSONArray Contact = new JSONArray();
                jsonObject.put("AppId", AppId);
                jsonObject.put("CustId", CustId);
                jsonObject.put("Contact", Contact);
                for (int i = 0; i < alContacts.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("Name", alContacts.get(i).getName());
                    JSONArray jsonArray = new JSONArray();
                    jsonObject1.put("PhoneNoS", jsonArray);
                    jsonArray.put(alContacts.get(i).getNumber());
                    Contact.put(jsonObject1);
                }
                Log.d(TAG, "doInBackground: " + jsonObject);
                synck_contact = networkAPI.sendHTTPData(Constants.SYANK_CONTACT, jsonObject);
                Log.d(TAG, "doInBackground: " + synck_contact);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return synck_contact;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ContactSync contactSync = new ContactSync();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        contactSync.setEnc_cust_id(jsonObject1.getString("CustId"));
                        contactSync.setCell_no(jsonObject1.getString("PhoneNo"));
                        contactSync.setImgURL(jsonObject1.getString("ImageUrl"));
                        contactSync.setName(jsonObject1.getString("Name"));
                        contactSyncs.add(contactSync);
                    }
                    contactRecycleAdapter = new ContactRecycleAdapter(alContacts, ContactFragment.this, contactSyncs);
                    recyclerView.setAdapter(contactRecycleAdapter);
                    contactRecycleAdapter.notifyDataSetChanged();

                }
            } catch (Exception e) {
                e.printStackTrace();
                contactRecycleAdapter = new ContactRecycleAdapter(alContacts, ContactFragment.this, contactSyncs);
                recyclerView.setAdapter(contactRecycleAdapter);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public void insertIntoRow(ChatModelList chatModelList) {
        QPayCustomerDatabase.insertChatListData(chatModelList);
    }

    public int isalreadyList(String cust_id, String phone) {
        return QPayCustomerDatabase.isAlreadyInChatList(cust_id, phone);
    }

    public int position(String phone) {
        Log.d(TAG, "position: " + String.valueOf(QPayCustomerDatabase.getPosition(phone)));
        return QPayCustomerDatabase.getPosition(phone);
    }

    public void goToCoversation(String url, String name, String id, String phone) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("url", url);
        editor.putString("name", name);
        editor.putString("id", id);
        editor.putString("phone", phone);
        editor.putString("_id", phone);
        editor.commit();
        startActivity(new Intent(getActivity(), ChatLibraryActivity.class));
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: chatList");
        super.onResume();
        _areLecturesLoaded = false;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
