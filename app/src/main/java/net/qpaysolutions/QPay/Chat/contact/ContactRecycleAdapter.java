package net.qpaysolutions.QPay.Chat.contact;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.CustumClasses.TextDrawable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ContactRecycleAdapter extends RecyclerView.Adapter<ContactViewHolder> {
    ArrayList<Contact> contacts;
    ContactFragment contactFragment;
    ArrayList<Contact> origMerchantMenuInfo;
    ArrayList<ContactSync> contactSyncs;
    String TAG = "dinesh";

    int count=0;
    int i;

    public ContactRecycleAdapter(ArrayList<Contact> contacts, ContactFragment contactFragment, ArrayList<ContactSync> contactSyncs) {
        this.contacts = contacts;
        this.contactFragment = contactFragment;
        this.contactSyncs = contactSyncs;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_item, parent, false);
        ContactViewHolder contactViewHolder = new ContactViewHolder(view);
        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final String date = df.format(Calendar.getInstance().getTime());
        TextDrawable drawable;
        if (position % 6 == 0) {
            drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(contacts.get(position).getName().charAt(0)), Color.parseColor("#62cc65"));
            holder.first_letter.setImageDrawable(drawable);
        } else if (position % 6 == 1) {
            drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(contacts.get(position).getName().charAt(0)), Color.parseColor("#1898ab"));
            holder.first_letter.setImageDrawable(drawable);
        } else if (position % 6 == 2) {
            drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(contacts.get(position).getName().charAt(0)), Color.parseColor("#a5599d"));
            holder.first_letter.setImageDrawable(drawable);
        } else if (position % 6 == 3) {
            drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(contacts.get(position).getName().charAt(0)), Color.parseColor("#fd464b"));
            holder.first_letter.setImageDrawable(drawable);
        } else if (position % 6 == 4) {
            drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(contacts.get(position).getName().charAt(0)), Color.GRAY);
            holder.first_letter.setImageDrawable(drawable);
        } else if (position % 6 == 5) {
            drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(contacts.get(position).getName().charAt(0)), Color.parseColor("#ff5252"));
            holder.first_letter.setImageDrawable(drawable);
        }
        holder.name.setText(contacts.get(position).getName());
        holder.number.setText(contacts.get(position).getNumber());
        holder.first_letter.setVisibility(View.VISIBLE);
        holder.round_image.setVisibility(View.GONE);
        for (i = 0; i < contactSyncs.size(); i++) {
            if (contactSyncs.get(i).getCell_no().equals(contacts.get(position).getNumber())) {
                if (contactSyncs.get(i).getImgURL() != null && !contactSyncs.get(i).getImgURL().isEmpty()) {
                    holder.first_letter.setVisibility(View.GONE);
                    holder.round_image.setVisibility(View.VISIBLE);
                    loadPicassoImage(contactSyncs.get(i).getImgURL(), holder.round_image);
                }

                holder.ic_chat.setVisibility(View.VISIBLE);
                final String cell_no, name, cust_id, url;

                cell_no = contactSyncs.get(i).getCell_no();
                name = contactSyncs.get(i).getName();
                cust_id = contactSyncs.get(i).getEnc_cust_id();
                url = contactSyncs.get(i).getImgURL();
                Log.d(TAG, "onBindViewHolder: " + cell_no + name + cust_id + url);


                holder.click.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (contactFragment.isalreadyList(cust_id, cell_no) > 0) {
                            Log.d(TAG, "onClick: already");
                            contactFragment.goToCoversation(url, name, cust_id, cell_no);
                            Log.d(TAG, "onClick: " + url + name + cust_id + cell_no);

                        } else {
                            ChatModelList chatModelList = new ChatModelList(cell_no, name, cust_id, url, date, "No Message", "0");
                            contactFragment.insertIntoRow(chatModelList);
                            contactFragment.goToCoversation(url, name, cust_id, cell_no);
                            Log.d(TAG, "onClick: " + url + name + cust_id + cell_no);

                        }

                    }
                });
                break;
            } else {
                holder.ic_chat.setVisibility(View.GONE);
                holder.click.setClickable(true);

            }

        }

    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<Contact> results = new ArrayList<>();
                if (origMerchantMenuInfo == null) origMerchantMenuInfo = contacts;
                if (constraint != null) {
                    if (origMerchantMenuInfo != null & origMerchantMenuInfo.size() > 0) {
                        for (final Contact g : origMerchantMenuInfo) {
                            if (g.getName().toLowerCase().contains(constraint.toString()) || g.getNumber().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                contacts = (ArrayList) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void loadPicassoImage(String url, ImageView img) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()
                    .load(R.drawable.header_placeholder)
                    .error(R.drawable.header_placeholder)
                    .into(img);
        }
    }
}
