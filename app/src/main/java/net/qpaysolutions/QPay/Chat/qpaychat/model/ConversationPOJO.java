package net.qpaysolutions.QPay.Chat.qpaychat.model;

public class ConversationPOJO {
     private boolean isAReceivedMessage;
     private int messageId;
     private String message;
     private boolean isImage;
     private String senderName;

    public ConversationPOJO() {
    }

    public ConversationPOJO(boolean isAReceivedMessage, int messageId, String message, boolean isImage, String senderName) {
        this.isAReceivedMessage = isAReceivedMessage;
        this.messageId = messageId;
        this.message = message;
        this.isImage = isImage;
        this.senderName = senderName;
    }

    public boolean isAReceivedMessage() {
        return isAReceivedMessage;
    }

    public void setAReceivedMessage(boolean AReceivedMessage) {
        isAReceivedMessage = AReceivedMessage;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}
