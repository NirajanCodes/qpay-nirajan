package net.qpaysolutions.QPay.Chat.qpaychat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataItem{

	@SerializedName("LatestMessageId")
	private int latestMessageId;

	@SerializedName("message")
	private List<MessageItem> message;

	public void setLatestMessageId(int latestMessageId){
		this.latestMessageId = latestMessageId;
	}

	public int getLatestMessageId(){
		return latestMessageId;
	}

	public void setMessage(List<MessageItem> message){
		this.message = message;
	}

	public List<MessageItem> getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"latestMessageId = '" + latestMessageId + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}