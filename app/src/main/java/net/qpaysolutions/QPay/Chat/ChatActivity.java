package net.qpaysolutions.QPay.Chat;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.Chat.chatList.ChatFragmentList;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Chat.contact.ContactFragment;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.Utils.CustomerApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ChatActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView textView;
    TabLayout tabs;
    ViewPager viewPager;
    int i = 0;
    private static final int REQUEST = 112;
    ViewPagerAdapter viewPagerAdapterr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        textView = (TextView) findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tabs = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabs.setupWithViewPager(viewPager);
//        setupWithViewPager(viewPager);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setupWithViewPager(viewPager);
        setTitleToolbar("Conversation");

    }

    public void setTitleToolbar(String titleToolbar) {
        textView.setText(titleToolbar);
    }

    public void setupWithViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapterr = viewPagerAdapter;
        viewPagerAdapter.addFragment(new ChatFragmentList(), "CHATS");
        viewPagerAdapter.addFragment(new ContactFragment(), "CONTACTS");
        viewPager.setAdapter(viewPagerAdapter);
        viewPagerAdapter.notifyDataSetChanged();
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> fragments = new ArrayList<>();
        List<String> fragmentTitle = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            fragmentTitle.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position);
        }
    }

    public void setViewPagerPosition() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.hideKeyword(ChatActivity.this);
       /* Intent intent = new Intent(ChatActivity.this, MainActivity.class);
        intent.putExtra("Check",6);
        startActivity(intent);*/
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        CustomerApplication.activityResumed();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    public void onPause() {
        super.onPause();
        CustomerApplication.activityPaused();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    int pos = viewPager.getCurrentItem();
                    Fragment activeFragment = viewPagerAdapterr.getItem(pos);

                    if (pos == 1) {
                        ((ContactFragment) activeFragment).runContact();

                    }
                } else {
                    setViewPagerPosition();
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give contact permission.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


}
