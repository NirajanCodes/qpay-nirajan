package net.qpaysolutions.QPay.Chat.qpaychat;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.qpaysolutions.QPay.Chat.qpaychat.adapter.ChatConversationAdapter;
import net.qpaysolutions.QPay.Chat.qpaychat.database.ConversationListDao;
import net.qpaysolutions.QPay.Chat.qpaychat.model.ConversationList;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.List;


public class ChatConversation extends AppCompatActivity implements ChatConversationListener{

    private ConversationListDao conversationListDao;
    private List<ConversationList> conversationLists;
    private ChatConversationAdapter chatConversationAdapter;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_conversation);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        startService(new Intent(this, NotificationService.class));

        conversationListDao = new ConversationListDao(this);

        if (conversationListDao.getAllConversation().size() > 0) {
            conversationLists = conversationListDao.getAllConversation();
        }else{
            conversationLists = new ArrayList<>();
            ConversationList conversationList = new ConversationList();
            conversationList.setAppId("2A00000027822qp");
            conversationList.setEncryptedCustomerId("L6hWEDkzSAttWidM4UrX/Q==");
            conversationList.setCustomerId("20237034");
            conversationList.setCellPhone("9860567999");
            conversationList.setUserName("Utsav Shrestha");
            conversationList.setLastMessage("No Message");

            ConversationList conversationList1 = new ConversationList();
            conversationList1.setAppId("2A2315320237105");
            conversationList1.setEncryptedCustomerId("oHC7JYFgVvKkoD9BPoPJ1g==");
            conversationList1.setCustomerId("20236611");
            conversationList1.setCellPhone("9801819868");
            conversationList1.setUserName("Ruby Sharma");
            conversationList1.setLastMessage("No Message");

            conversationLists.add(conversationList);
            conversationLists.add(conversationList1);
            conversationListDao.insertMessage(conversationList1);
            conversationListDao.insertMessage(conversationList);
        }

        chatConversationAdapter = new ChatConversationAdapter(conversationLists, this);
        recyclerView.setAdapter(chatConversationAdapter);
    }

    @Override
    public void onClick(int position) {
        startActivity(new Intent(ChatConversation.this, QpayChat.class));
    }
}
