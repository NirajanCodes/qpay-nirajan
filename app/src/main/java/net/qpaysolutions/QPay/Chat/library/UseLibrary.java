package net.qpaysolutions.QPay.Chat.library;

import android.graphics.Bitmap;

/**
 * Created by deadlydragger on 4/11/17.
 */
public class UseLibrary {
    private String mId;
    private String mName;
    private Bitmap mIcon;

    public UseLibrary(String id, String name, Bitmap icon) {
        mId = id;
        mName = name;
        mIcon = icon;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Bitmap getIcon() {
        return mIcon;
    }

    public void setIcon(Bitmap icon) {
        mIcon = icon;
    }
}
