package net.qpaysolutions.QPay.Chat.conversation;

/**
 * Created by deadlydragger on 3/21/17.
 */

public class ConversationModule {
    String id,msg,date,flag,send_fail,messageId;
    int _id;
    String Image_Message,IsImage;

    public String getIsImage() {
        return IsImage;
    }

    public void setIsImage(String isImage) {
        IsImage = isImage;
    }

    public String getImage_Message() {
        return Image_Message;
    }

    public void setImage_Message(String image_Message) {
        Image_Message = image_Message;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSend_fail() {
        return send_fail;
    }

    public void setSend_fail(String send_fail) {
        this.send_fail = send_fail;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public ConversationModule() {
    }

    public ConversationModule(String id, String msg, String date, String flag,String send_fail,String messageId,String Image_Message,String IsImage) {
        this.id = id;
        this.msg = msg;
        this.date = date;
        this.flag = flag;
        this.send_fail=send_fail;
        this.messageId=messageId;
        this.Image_Message=Image_Message;
        this.IsImage=IsImage;
    }

    public String getUrl() {
        return id;
    }

    public void setUrl(String id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
