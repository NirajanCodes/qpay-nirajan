package net.qpaysolutions.QPay.Chat.qpaychat;

public interface NetworkResponse {
    void onSuccess(String data);
    void onError(String data);
}