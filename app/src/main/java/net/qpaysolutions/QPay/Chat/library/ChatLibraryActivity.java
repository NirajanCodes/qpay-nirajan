package net.qpaysolutions.QPay.Chat.library;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.qpaysolutions.QPay.Cache.AppCache;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.Message;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.User;
import net.qpaysolutions.QPay.Chat.chatmessageview.utils.MessageStatusFormatter;
import net.qpaysolutions.QPay.Chat.chatmessageview.views.ChatView;
import net.qpaysolutions.QPay.Chat.conversation.ConversationModule;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Firebase.MyFirebaseMessagingService;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Utils.Utilitypermission;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by deadlydragger on 4/4/17.
 */

public class ChatLibraryActivity extends AppCompatActivity implements MyFirebaseMessagingService.NotificationAlert {
    private ChatView mChatView;
    private ArrayList<User> mUsers;
    TextView textView;
    int backPressedCount = 1;
    Bitmap picture;
    private static final int READ_REQUEST_CODE = 100;
    SharedPreferences sharedPreferences;
    String url, name, id, phone;
    String FILE = "chat";
    Intent intent;
    boolean isImageVisible = false;
    QPayCustomerDatabase QPayCustomerDatabase;
    String userChoosenTask;
    private SQLiteDatabase db;
    String getUrl;
    String SenderCustId, AppId;
    String senderID;
    String TAG = "dinesh";
    QPayProgressDialog qPayProgressDialog;
    String yourId;
    String last_msg_id;
    private boolean visible;
    ImageView fullScreen;
    private String customerId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(this);
        QPayCustomerDatabase.getReadableDatabase();
        db = QPayCustomerDatabase.getWritableDatabase();
        SenderCustId = QPayCustomerDatabase.getCustomerID();


        AppId = QPayCustomerDatabase.getKeyAppId();
        getUrl = QPayCustomerDatabase.getImg();
        Log.d(TAG, "onCreate: " + getUrl);
        //mMessageList = new MessageList();
        setContentView(R.layout.library_chat_view);
        sharedPreferences = getSharedPreferences(FILE, Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name", "");
        url = sharedPreferences.getString("url", "");
        id = sharedPreferences.getString("id", null);
        phone = sharedPreferences.getString("phone", "1");
        yourId = sharedPreferences.getString("_id", "1");
        customerId = sharedPreferences.getString("cust_id", "");
        senderID = yourId;

        last_msg_id = sharedPreferences.getString(yourId + "last_msg_id", "0");


        Toolbar  toolbar = (Toolbar) findViewById(R.id.toolbar);
           textView = (TextView) findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setToolbarText(name);
        initUsers();
        mChatView = (ChatView) findViewById(R.id.chat_view);
        fullScreen = (ImageView) findViewById(R.id.iv_full_screen);

        MyFirebaseMessagingService.setListener(this);

        mChatView.setRightBubbleColor(ContextCompat.getColor(this, R.color.background_light_theme));
        mChatView.setLeftBubbleColor(ContextCompat.getColor(this, R.color.white));
        mChatView.setOptionButtonColor(R.color.teal500);
        mChatView.setRightMessageTextColor(Color.WHITE);
        mChatView.setLeftMessageTextColor(Color.BLACK);
        mChatView.setUsernameTextColor(Color.WHITE);
        mChatView.setSendTimeTextColor(ContextCompat.getColor(this, R.color.color_thirtary_text));
        mChatView.setDateSeparatorColor(ContextCompat.getColor(this, R.color.color_secondary_text));
        mChatView.setMessageStatusTextColor(ContextCompat.getColor(this, R.color.color_thirtary_text));
        mChatView.setOptionIcon(R.drawable.ic_image_chat);
        mChatView.setInputTextHint(getResources().getString(R.string.input_hint));
        mChatView.setMessageMarginTop(5);
        mChatView.setMessageMarginBottom(5);
        mChatView.setOptionButtonColor(ContextCompat.getColor(this, R.color.color_thirtary_text));
        mChatView.enlableSendButton();
        mChatView.setSendIcon(R.drawable.ic_send_chat_msg);

        //Click Send Button
        mChatView.setOnClickSendButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mChatView.getInputText().length() > 0) {
                    initUsers();
                    Message message1 = new Message.Builder()
                            .setRightMessage(true)
                            .setUsernameVisibility(false)
                            .setUser(mUsers.get(0))
                            .setStatusIconFormatter(new MessageStatusFormatter(ChatLibraryActivity.this))
                            .setMessageStatusType(Message.MESSAGE_STATUS_ICON_RIGHT_ONLY)
                            .setStatus(MessageStatusFormatter.STATUS_DELIVERING)
                            .setMessageText(mChatView.getInputText().replaceAll("\\s+", " "))
                            .build();
                    new SendMessage(mChatView.getInputText().replaceAll("\\s+", " "), false, "", mChatView.getSize(),
                            mChatView.getSizeMessageList()).execute();

                    try {
                        mChatView.send(message1);
                        AppCache.getMessageList().add(message1);
                        mChatView.setInputText("");

                        //new PutMessages().execute();
                        AppData.putMessageList(CustomerApplication.getContext(), AppCache.getMessageList(), String.valueOf(yourId));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    new Utility().showSnackbar(view, "Enter valid input.");
                }
            }
        });

        //Click option button
        mChatView.setOnClickOptionButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CharSequence[] items = {"Take Photo", "Upload photo"};

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ChatLibraryActivity.this);
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        boolean result = Utilitypermission.checkPermission(ChatLibraryActivity.this);

                        if (items[item].equals("Take Photo")) {
                            userChoosenTask = "Take Photo";
                            if (result) {
                                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, READ_REQUEST_CODE);
                            }

                            // startActivityForResult(takePicture, 0);//zero can be replaced with any action code

                        } else if (items[item].equals("Upload photo")) {
                            userChoosenTask = "Upload photo";
                            if (result) {
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                    intent = new Intent(Intent.ACTION_GET_CONTENT);

                                } else {
                                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                                }
                                intent.setType("image/*");
                                startActivityForResult(intent, READ_REQUEST_CODE);

                            }
                        }
                    }
                });
                builder.show();


            }
        });

        //Load saved messages
//        loadMessages();
        //new LoadAsynckly().execute();


        AppCache.setMessageList(AppData.getMessageList(getApplicationContext(), phone));
    /*    MessageList listList = AppCache.getMessageList();
        for(int i = 0, j = i + 1; i < listList.size() - 1; i++){
            if(listList.get(i) != listList.get(j)){
                AppCache.getMessageList().add(listList.get(i));
            }
        }*/

        loadMessages();

        new GetMessageById(AppCache.getMessageID()).execute();


        mChatView.setOnMessageViewClickListener(new Message.OnMessageViewClickListener() {
            @Override
            public void onLongClick(final int position) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ChatLibraryActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.custom_dialog_chat_delet, null);
                final TextView delet_chat = (TextView) v.findViewById(R.id.delet_chat);
                final TextView cancel_delet = (TextView) v.findViewById(R.id.cancel_delet);
                final TextView title = (TextView) v.findViewById(R.id.title);
                title.setText("MESSAGE");
                builder.setView(v);
                builder.setCancelable(false);
                final Dialog dialog = builder.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                delet_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                        // qpayDatabases.updateLastyMsg(senderID, AppCache.getMessageList().get(position - 1).getMessageText());

                        //  AppCache.getMessageList().delete(position);
                        mChatView.refreshData(position, position);
                        if (position == 0) {
                            if (AppCache.getMessageList().size() > 0) {
                                QPayCustomerDatabase.updateLastyMsg(id, AppCache.getMessageList().get(position).getMessageText());
                                AppCache.getMessageList().delete(position);
                            } else {
                                QPayCustomerDatabase.updateLastyMsg(id, "No Messages");
                            }
                            new DeleteMessage(senderID, AppCache.getMessageList().getId(position), false).execute();
                        } else {
                            QPayCustomerDatabase.updateLastyMsg(id, AppCache.getMessageList().get(position - 1).getMessageText());
                            AppCache.getMessageList().delete(position);
                            new DeleteMessage(senderID, AppCache.getMessageList().getId(position - 1), false).execute();
                        }

                        //AppData.getMessageList(Globale.getContext(), phone).delete(position);
                        AppData.putMessageList(CustomerApplication.getContext(), AppCache.getMessageList(), phone);


                    }
                });
                cancel_delet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                float density = getResources().getDisplayMetrics().density;
                lp.width = (int) (300 * density);
                lp.height = (int) (320 * density);
                lp.gravity = Gravity.CENTER;
                dialog.getWindow().setAttributes(lp);
            }
        });


        mChatView.setOnBubbleLongClickListener(new Message.OnBubbleLongClickListener() {
            @Override
            public void onLongClick(Message message, final int position) {
                //todo add option to delete image

                final AlertDialog.Builder builder = new AlertDialog.Builder(ChatLibraryActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.custom_dialog_chat_delet, null);
                final TextView delet_chat = (TextView) v.findViewById(R.id.delet_chat);
                final TextView cancel_delet = (TextView) v.findViewById(R.id.cancel_delet);
                final TextView title = (TextView) v.findViewById(R.id.title);
                title.setText("PICTURE");
                builder.setView(v);
                builder.setCancelable(false);
                final Dialog dialog = builder.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                delet_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                        QPayCustomerDatabase.updateLastyMsg(senderID, AppCache.getMessageList().get(position - 1).getMessageText());
                        AppCache.getMessageList().delete(position);
                        mChatView.refreshData(position, position);
                        AppData.putMessageList(CustomerApplication.getContext(), AppCache.getMessageList(), phone);


                    }
                });
                cancel_delet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                float density = getResources().getDisplayMetrics().density;
                lp.width = (int) (300 * density);
                lp.height = (int) (320 * density);
                lp.gravity = Gravity.CENTER;
                dialog.getWindow().setAttributes(lp);
            }
        });


        mChatView.setOnBubbleClickListener(new Message.OnBubbleClickListener() {
            @Override
            public void onClick(Message message) {
                if (message.getType() == Message.Type.PICTURE) {
                    if (message.getPicture() == null) {
                        Picasso.get()

                                .load(message.getPictureUrl())
                                .into(new Target() {
                                    @Override
                                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                                        fullScreen.setImageBitmap(bitmap);
                                        fullScreen.setVisibility(View.VISIBLE);
                                        isImageVisible = true;
                                        hideKeyboard();
                                        backPressedCount++;
                                    }

                                    @Override
                                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                                        fullScreen.setImageDrawable(placeHolderDrawable);
                                    }
                                    @Override
                                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                        fullScreen.setImageDrawable(errorDrawable);
                                    }


                                });
                    } else {

                        fullScreen.setImageBitmap(message.getPicture());
                        fullScreen.setVisibility(View.VISIBLE);
                        isImageVisible = true;
                        hideKeyboard();
                        backPressedCount++;
                    }

                    mChatView.setVisibility(View.GONE);
                }
            }
        });
        //new GetMessageById(last_msg_id).execute();
        visible = true;
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Log.d(TAG, "onReceive: " + intent.getStringExtra("MessageId") + "message " + intent.getStringExtra("Message"));

            if (AppCache.getMessageId() == null) {
                // AppCache.setMessageId(intent.getStringExtra("MessageId"));

                handleBroadcast(intent);
            } else if (AppCache.getMessageId() != null && !AppCache.getMessageId().equalsIgnoreCase(intent.getStringExtra("MessageId"))) {
                handleBroadcast(intent);
            }
        }
    };

    private void handleBroadcast(Intent intent) {
        try {
            //Log.d(TAG, "onReceive: id" + id + " sender Id : " + intent.getStringExtra("SenderId"));
            if (id != null && !id.isEmpty() && id.equals(intent.getStringExtra("SenderId"))) {
                AppCache.setMessageID(intent.getStringExtra("MessageId"));
                AppCache.setMessageId(intent.getStringExtra("MessageId"));
                new GetMessageById(intent.getStringExtra("MessageId")).execute();
                String IsImage = intent.getStringExtra("IsImage");


                if (IsImage.equals("1")) {
                    Log.d(TAG, "onReceive: " + intent.getStringExtra("Message"));
                    receiveImage(intent.getStringExtra("Message"));
                    QPayCustomerDatabase.updateLastyMsg(intent.getStringExtra("SenderId"), "You Received an Image.");
                } else {
                    receiveMessage(intent.getStringExtra("Message"));
                    QPayCustomerDatabase.updateLastyMsg(intent.getStringExtra("SenderId"), intent.getStringExtra("Message"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void receiveImage(String url) {
        Picasso.get()

                .load(url)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                        final Message message = new Message.Builder()
                                .setUsernameVisibility(false)
                                .hideIcon(false)
                                .setRightMessage(false)
                                .setUser(mUsers.get(1))
                                .setMessageText(Message.Type.PICTURE.name())
                                .setIsBitmap(true)
                                .setPicture(bitmap)
                                .setType(Message.Type.PICTURE)
                                .build();
                        mChatView.receive(message);
                        //Add message list
                        AppCache.getMessageList().add(message);
                        // new PutMessages().execute();
                        AppData.putMessageList(CustomerApplication.getContext(), AppCache.getMessageList(), String.valueOf(yourId));

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }

                });
    }

    @Override
    public void alert(Intent intent) {
        if (id != null && !id.isEmpty() && id.equals(intent.getStringExtra("SenderId"))) {
            AppCache.setMessageID(intent.getStringExtra("MessageId"));
            new GetMessageById(intent.getStringExtra("MessageId")).execute();
            String IsImage = intent.getStringExtra("IsImage");
            if (("1").equalsIgnoreCase(IsImage)) {
                Log.d(TAG, "onReceive: " + intent.getStringExtra("Message"));
                receiveImage(intent.getStringExtra("Message"));
            } else {
                receiveMessage(intent.getStringExtra("Message"));
            }
        }
    }

    public class SendMessage extends AsyncTask<String, String, String> {
        String message, time, Image;
        boolean IsImage;
        int positionChatList, positionMessageList;

        public SendMessage(String message, boolean IsImage, String image) {
            this.message = message;
            this.IsImage = IsImage;
            this.Image = image;
            this.positionMessageList = -1;
            this.positionChatList = -1;
        }

        public SendMessage(String message, boolean IsImage, String image,
                           int positionChatList, int positionMessageList) {
            this.message = message;
            this.IsImage = IsImage;
            this.Image = image;
            this.positionChatList = positionChatList;
            this.positionMessageList = positionMessageList;
        }

        @Override
        protected String doInBackground(String... params) {

            String sendMessage = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("SenderId", SenderCustId);
                jsonObject.put("ReceiverId", id);
                jsonObject.put("AppId", AppId);
                jsonObject.put("Message", message);
                jsonObject.put("IsImage", IsImage);
                jsonObject.put("Image", Image);
                Log.d(TAG, "doInBackground: " + jsonObject);
                sendMessage = NetworkAPI.sendHTTPData(Constants.SEND_MESSAGE_CHAT, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return sendMessage;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    QPayCustomerDatabase.updateLastyMsg(id, message);
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject jsonObject1 = data.getJSONObject(0);
                    String MessageId = jsonObject1.getString("MessageId");
                    String FileUrl = jsonObject1.getString("FileUrl");
                    QPayCustomerDatabase.updateSendFail(time, "1");
                    Message message1;
                    if (!Image.isEmpty()) {
                        message1 = new Message.Builder()
                                .setUsernameVisibility(false)
                                .setRightMessage(true)
                                .setMessageText(Message.Type.PICTURE.name())
                                .setUser(mUsers.get(0))
                                .setIsBitmap(true)
                                .setStatusIconFormatter(new MessageStatusFormatter(ChatLibraryActivity.this))
                                .setMessageStatusType(Message.MESSAGE_STATUS_ICON_RIGHT_ONLY)
                                .setStatus(MessageStatusFormatter.STATUS_DELIVERED)
                                .hideIcon(false)
                                .setPicture(Utility.changeBitmapToThumbnail(ChatLibraryActivity.this, picture, (int) (100 * getResources().getDisplayMetrics().density)))
                                .setType(Message.Type.PICTURE)
                                .build();
                    } else {
                        message1 = new Message.Builder()
                                .setRightMessage(true)
                                .setUsernameVisibility(false)
                                .setUser(mUsers.get(0))
                                .setStatusIconFormatter(new MessageStatusFormatter(ChatLibraryActivity.this))
                                .setMessageStatusType(Message.MESSAGE_STATUS_ICON_RIGHT_ONLY)
                                .setStatus(MessageStatusFormatter.STATUS_DELIVERED)
                                .setMessageText(message)
                                .build();
                    }


                    try {

                        if (positionChatList == -1) {

                            AppCache.getMessageList().delete(AppCache.getMessageList().size());
                            // mChatView.send(message1);
                            mChatView.updateList(mChatView.getSize() - 1, mChatView.getSizeMessageList() - 1, message1);
                            AppCache.getMessageList().add(message1);
                            AppData.putMessageList(CustomerApplication.getContext(),
                                    AppCache.getMessageList(), String.valueOf(yourId));

                        } else {
                            AppCache.getMessageList().delete(AppCache.getMessageList().size() - 1);
                            mChatView.updateList(positionChatList, positionMessageList, message1);
                            AppCache.getMessageList().add(message1);
                            AppData.putMessageList(CustomerApplication.getContext(),
                                    AppCache.getMessageList(), String.valueOf(yourId));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (IsImage) {
                        ConversationModule conversationModule = new ConversationModule(id, message, time, "1", "0", MessageId, FileUrl, "1");
                        QPayCustomerDatabase.insertConversationData(conversationModule);
                    } else {
                        ConversationModule conversationModule = new ConversationModule(id, message, time, "1", "0", MessageId, "", "0");
                        QPayCustomerDatabase.insertConversationData(conversationModule);
                    }

                } else {
                    QPayCustomerDatabase.updateLastyMsg(id, message);
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject jsonObject1 = data.getJSONObject(0);
                    String MessageId = jsonObject1.getString("MessageId");
                    String FileUrl = jsonObject1.getString("FileUrl");
                    QPayCustomerDatabase.updateSendFail(time, "1");
                    Message message1;
                    if (!Image.isEmpty()) {
                        message1 = new Message.Builder()
                                .setUsernameVisibility(false)
                                .setRightMessage(true)
                                .setMessageText(Message.Type.PICTURE.name())
                                .setUser(mUsers.get(0))
                                .setIsBitmap(true)
                                .setStatusIconFormatter(new MessageStatusFormatter(ChatLibraryActivity.this))
                                .setMessageStatusType(Message.MESSAGE_STATUS_ICON_RIGHT_ONLY)
                                .setStatus(MessageStatusFormatter.STATUS_ERROR)
                                .hideIcon(false)
                                .setPicture(Utility.changeBitmapToThumbnail(ChatLibraryActivity.this, picture, (int) (100 * getResources().getDisplayMetrics().density)))
                                .setType(Message.Type.PICTURE)
                                .build();
                    } else {
                        message1 = new Message.Builder()
                                .setRightMessage(true)
                                .setUsernameVisibility(false)
                                .setUser(mUsers.get(0))
                                .setStatus(MessageStatusFormatter.STATUS_ERROR)
                                .setStatusIconFormatter(new MessageStatusFormatter(ChatLibraryActivity.this))
                                .setMessageStatusType(Message.MESSAGE_STATUS_ICON_RIGHT_ONLY)
                                .setStatus(MessageStatusFormatter.STATUS_ERROR)
                                .setMessageText(message)
                                .build();
                    }

                    try {
                        if (positionChatList == -1) {
                            mChatView.refreshData(mChatView.getSize() - 1, mChatView.getSizeMessageList() - 1);
                            AppCache.getMessageList().delete(AppCache.getMessageList().size());
                            mChatView.send(message1);
                            AppCache.getMessageList().add(message1);
                            AppData.putMessageList(CustomerApplication.getContext(),
                                    AppCache.getMessageList(), String.valueOf(yourId));

                        } else {
                            // mChatView.refreshData(positionChatList, positionMessageList);
                            AppCache.getMessageList().delete(AppCache.getMessageList().size());
                            // mChatView.send(message1);
                            mChatView.updateList(positionChatList, positionMessageList, message1);
                            AppCache.getMessageList().add(message1);
                            AppData.putMessageList(CustomerApplication.getContext(),
                                    AppCache.getMessageList(), String.valueOf(yourId));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (IsImage) {
                        ConversationModule conversationModule = new ConversationModule(id, message, time, "1", "0", MessageId, FileUrl, "1");
                        QPayCustomerDatabase.insertConversationData(conversationModule);
                    } else {
                        ConversationModule conversationModule = new ConversationModule(id, message, time, "1", "0", MessageId, "", "0");
                        QPayCustomerDatabase.insertConversationData(conversationModule);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }
    }

    class GetMessageById extends AsyncTask<String, String, String> {
        String MessageId;

        public GetMessageById(String messageId) {
            MessageId = messageId;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String sendMessage = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("SenderId", SenderCustId);
                jsonObject.put("ReceiverId", id);
                jsonObject.put("AppId", AppId);
                jsonObject.put("MessageId", MessageId);
                Log.d(TAG, "GetMessageById: " + jsonObject);
                sendMessage = NetworkAPI.sendHTTPData(Constants.GET_MESSAGE_BY_ID, jsonObject);
                Log.d(TAG, "GetMessageById vitra : " + sendMessage);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return sendMessage;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    @Override
    public void onBackPressed() {
        Utils.hideKeyword(ChatLibraryActivity.this);
//        AppData.putMessageList(this, mMessageList, String.valueOf(yourId));
        //super.onBackPressed();

        if (isImageVisible) {
            isImageVisible = false;
            fullScreen.setVisibility(View.GONE);
            mChatView.setVisibility(View.VISIBLE);
            backPressedCount--;
        } else {
            //new PutMessages().execute();


            // AppCache.setMessageList(mMessageList);
            AppCache.setUserNumber(null);
            super.onBackPressed();
//            startActivity(new Intent(ChatLibraryActivity.this, ChatActivity.class));
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void receiveMessage(String sendText) {

        //Receive message
        final Message receivedMessage = new Message.Builder()
                .setUsernameVisibility(false)
                .hideIcon(false)
                .setUser(mUsers.get(1))
                .setRightMessage(false)
                .setMessageText(sendText)
                .build();


        mChatView.receive(receivedMessage);
        AppCache.getMessageList().add(receivedMessage);
        // new PutMessages().execute();
        AppData.putMessageList(CustomerApplication.getContext(), AppCache.getMessageList(), String.valueOf(yourId));


    }

    public void setToolbarText(String toolbarText) {
        textView.setText(toolbarText);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != READ_REQUEST_CODE || resultCode != RESULT_OK || data == null) {
            return;
        }

        try {
            if (userChoosenTask.equalsIgnoreCase("Take Photo")) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                String path = storeImageInternalStorage(photo);
                picture = photo;
                picture = Utility.modifyOrientation(picture, path);
            } else {
                Uri uri = data.getData();


                picture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                picture = Utility.getCorrectlyOrientedImage(this, uri, 600);

                //  File myFile = new File(uri.toString());
                //picture = Utility.modifyOrientation(picture, getRealPathFromURI(this, uri));
            }

            Message message = new Message.Builder()
                    .setUsernameVisibility(false)
                    .setRightMessage(true)
                    .setMessageText(Message.Type.PICTURE.name())
                    .setUser(mUsers.get(0))
                    .setIsBitmap(true)
                    .hideIcon(false)
                    .setStatusIconFormatter(new MessageStatusFormatter(ChatLibraryActivity.this))
                    .setMessageStatusType(Message.MESSAGE_STATUS_ICON_RIGHT_ONLY)
                    .setStatus(MessageStatusFormatter.STATUS_DELIVERING)
                    .setPicture(Utility.changeBitmapToThumbnail(ChatLibraryActivity.this, picture, (int) (100 * getResources().getDisplayMetrics().density)))
                    .setType(Message.Type.PICTURE)
                    .build();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Utility.changeBitmapToThumbnail(ChatLibraryActivity.this, picture,
                    (int) (100 * getResources().getDisplayMetrics().density))
                    .compress(Bitmap.CompressFormat.PNG, 10, baos);
            byte[] b = baos.toByteArray();
            String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
            new SendMessage("You sent a photo", true, imageEncoded).execute();
            // new PutMessages().execute();
            mChatView.send(message);
            AppCache.getMessageList().add(message);

            AppData.putMessageList(CustomerApplication.getContext(), AppCache.getMessageList(), String.valueOf(yourId));


        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        }

    }


    private void initUsers() {
        mUsers = new ArrayList<>();
        //User id
        String myId = "0";
        //User icon

        //User name
        String myName = "";
        final Bitmap[] myIcon = new Bitmap[1];
        if (getUrl != null && !getUrl.isEmpty()) {
            Picasso.get()

                    .load(getUrl)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                            //Set it in the ImageView
//                        theView.setImageBitmap(bitmap);
                            myIcon[0] = bitmap;
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                    });
        }

        Bitmap yourIcon;
        String yourName = "";
        final Bitmap[] finalYourIcon = new Bitmap[1];
        if (url != null && !url.isEmpty()) {
            Picasso.get()

                    .load(url)
                    .error(R.drawable.header_placeholder)
                    .placeholder(R.drawable.header_placeholder)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                            //Set it in the ImageView
//                        theView.setImageBitmap(bitmap);
                            finalYourIcon[0] = bitmap;
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }

                    });
        }
        final User me = new User(myId, myName, myIcon[0]);
        final User you = new User(yourId, yourName, finalYourIcon[0]);

        mUsers.add(me);
        mUsers.add(you);
    }

    /**
     * Load saved messages
     */


    private class DeleteMessage extends AsyncTask<String, String, String> {
        String SenderId, MessageId;
        boolean IsDeleteAll;

        public DeleteMessage(String SenderId, String messageId, boolean IsDeleteAll) {
            this.SenderId = SenderId;
            this.MessageId = messageId;
            this.IsDeleteAll = IsDeleteAll;
        }

        @Override
        protected String doInBackground(String... params) {

            String DeleteMessage = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("SenderId", customerId);
                jsonObject.put("ReceiverId", SenderId);
                jsonObject.put("AppId", AppId);
                jsonObject.put("IsDeleteAll", IsDeleteAll);
                jsonObject.put("MessageId", MessageId);
                Log.d(TAG, "DeleteMessage: " + DeleteMessage);
                DeleteMessage = NetworkAPI.sendHTTPData(Constants.DELET_CHAT_MSG, jsonObject);
                Log.d(TAG, "DeleteMessage: " + DeleteMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return DeleteMessage;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (CustomerApplication.isActivityVisible()) {
                // qPayProgressDialog = new QPayProgressDialog(getActivity());
                //qPayProgressDialog.show();
            }

        }
    }

    private void loadMessages() {
        String lastMessage = "none";
        String pictureUrl = "none";
        // MessageList messageList = new MessageList();
//        mMessageList = AppData.getMessageList(this, String.valueOf(yourId));
        if (AppCache.getMessageList() == null) {
            //mMessageList = new MessageList();
        } else {
            //  messageList = new MessageList(AppCache.getMessageList());
            //AppCache.getMessageList().clear();
            for (int i = 0; i < AppCache.getMessageList().size(); i++) {
                Message message = AppCache.getMessageList().get(i);
                if (message.getStatus() == MessageStatusFormatter.STATUS_DELIVERING
                        || message.getStatus() == MessageStatusFormatter.STATUS_ERROR) {
                    message.setStatusIconFormatter(new MessageStatusFormatter(ChatLibraryActivity.this));
                    message.setMessageStatusType(Message.MESSAGE_STATUS_ICON_RIGHT_ONLY);
                    message.setStatus(MessageStatusFormatter.STATUS_ERROR);
                }

                for (User user : mUsers) {
                    if (message.getUser().getId().equalsIgnoreCase(user.getId())) {
                        message.getUser().setIcon(user.getIcon());
                    }
                }
                if (lastMessage.equalsIgnoreCase("none")
                        || !lastMessage.equalsIgnoreCase(message.getMessageText())
                        || lastMessage.equalsIgnoreCase("PICTURE")) {

                    if (message.getPictureUrl() == null) {
                        if (!message.isDateCell()) {
                            if (message.isRightMessage()) {
                                message.hideIcon(true);
                                message.setDateCell(false);
                                message.setUsernameVisibility(false);

                                mChatView.send(message);
                                //AppCache.getMessageList().add(message);
                            } else {
                                message.setUsernameVisibility(false);
                                mChatView.receive(message);
                                //AppCache.getMessageList().add(message);
                            }

                        }
                    } else if (message.isRightMessage()) {
                        message.hideIcon(true);

                        mChatView.send(message);
                        //AppCache.getMessageList().add(message);
                    } else if (!pictureUrl.equalsIgnoreCase(message.getPictureUrl()) || pictureUrl.equalsIgnoreCase("none")) {
                        //  if (!message.isDateCell()) {
                            /*if (message.isRightMessage()) {
                                message.hideIcon(true);

                                mChatView.send(message);
                            } else {*/
                        message.setUsernameVisibility(false);
                        mChatView.receive(message);
                        // AppCache.getMessageList().add(message);
                        // }
                        pictureUrl = message.getPictureUrl();

                        //  }
                    }

                    lastMessage = message.getMessageText();

                }

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        CustomerApplication.activityResumed();
        registerReceiver(broadcastReceiver,
                new IntentFilter("custom-event-name"));
        Log.d(TAG, "onResume: ");
        initUsers();
    }

    @Override
    public void onPause() {
        super.onPause();
        CustomerApplication.activityPaused();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Save message
        Log.d(TAG, "onPause: ");

    }

    public boolean isVisible() {
        Log.d(TAG, "isVisible: " + visible);
        return visible;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    protected void onStop() {
        super.onStop();
        visible = false;
        // LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);

        //new GetMessageById(AppCache.getMessageID()).execute();
    }

    private class PutMessages extends AsyncTask<Void, String, String> {


        @Override
        protected String doInBackground(Void... params) {
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private String storeImageInternalStorage(Bitmap bmp) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir

        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        //byte[] byteArray = stream.toByteArray();

        File file = new File(directory, "menuImage.png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return directory.getAbsolutePath();
    }


}
