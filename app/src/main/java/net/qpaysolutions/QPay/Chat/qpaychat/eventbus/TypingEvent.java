package net.qpaysolutions.QPay.Chat.qpaychat.eventbus;

public class TypingEvent {
    private String message;

    public TypingEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
