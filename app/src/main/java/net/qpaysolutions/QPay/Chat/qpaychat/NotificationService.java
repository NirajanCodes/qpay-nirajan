package net.qpaysolutions.QPay.Chat.qpaychat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import net.qpaysolutions.QPay.Chat.qpaychat.eventbus.ChatMessageEvent;
import net.qpaysolutions.QPay.Chat.qpaychat.eventbus.TypingEvent;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.CustomerApplication;

import org.greenrobot.eventbus.EventBus;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class NotificationService extends Service {
    private io.socket.client.Socket socket;
    private BroadcastReceiver br;
    IntentFilter intentFilter;
    String app_id, term_id;
    private IBinder iBinder = new SalesBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    public class SalesBinder extends Binder {
        NotificationService getService() {
            return NotificationService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //service is create
        intentFilter = new IntentFilter("qrcodeeditor.blackspring.net.qrcodeeditor.NOTIFICATION");
        listenNotification(br);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    private void listenNotification(final BroadcastReceiver br) {
        try {
            socket = IO.socket("http://socket.qpaysolutions.net");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.d("socket_IDDDD", socket.id());
            }

        }).on(Network.CUST_ID, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                EventBus.getDefault().post(new ChatMessageEvent((String) args[0]));

            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {

            }

        }).on(Network.CUST_ID + "typing", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                EventBus.getDefault().post(new TypingEvent("typing..."));

            }
        });
        socket.connect();
    }

    public void emitTyping(String userId) {
        socket.emit(userId + "typing", "typing...");
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, QpayChat.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = CustomerApplication.CHANNEL_1_ID;
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_send)
                        .setContentTitle("Chat")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}