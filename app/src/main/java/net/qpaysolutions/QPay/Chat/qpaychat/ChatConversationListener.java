package net.qpaysolutions.QPay.Chat.qpaychat;

public interface ChatConversationListener {
    void onClick(int position);
}
