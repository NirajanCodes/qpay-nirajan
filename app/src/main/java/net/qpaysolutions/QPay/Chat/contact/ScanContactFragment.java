package net.qpaysolutions.QPay.Chat.contact;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by deadlydragger on 3/24/17.
 */

public class ScanContactFragment extends Fragment {

    private String id;
    private String merchantName, imgURL;
    private String return_cust_id;
    private String resp, app_id;
    private QPayProgressDialog progressDialog;
    private String custIdFriends = "", resultScanFriendsCell = "";
    private  LinearLayout blackView;
    private SurfaceView barcodeView;
    private QPayCustomerDatabase QPayCustomerDatabase;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scan_to_add,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase =  new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();

        id = GeneralPref.getCustId();
        app_id =GeneralPref.getAppId();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AddContactActivity)getActivity()).setTitleToolbar("Scan Contact");
        barcodeView = (SurfaceView) view.findViewById(R.id.barcode_scanner);
        blackView =(LinearLayout)view.findViewById(R.id.blace_back);
        qrCodeReading();
    }



    private void qrCodeReading(){
        BarcodeDetector barcodeDetector;
        final CameraSource cameraSource;
        barcodeDetector = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        cameraSource = new CameraSource
                .Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .setAutoFocusEnabled(true)
                .build();

        barcodeView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(barcodeView.getHolder());
                    }

                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    barcodeView.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {

                            try {
                                String resultString =  barcodes.valueAt(0).displayValue.toString();
                                return_cust_id = resultString;
                                if (QPayCustomerDatabase.isAlreadyInChatListcontactCustId(return_cust_id) > 0){
                                    new Utility().showSnackbar(blackView,"Already in your chat list.");
                                }else {
                                    if (return_cust_id != null && return_cust_id.length()==24 && !return_cust_id.equals("QPay-Payment Simplified") && !return_cust_id.equals(id)){
                                        blackView.setVisibility(View.VISIBLE);
                                        barcodeView.setVisibility(View.GONE);
                                        new GetContact(return_cust_id).execute();
                                    } else if (return_cust_id.equals("QPay-Payment Simplified")){
                                        custumdialogfailure("Invalid QRcode!","You seem to have scanned QPay logo. Please scan proper QRcode.");
                                    }
                                    else if ( return_cust_id.equals(id)){
                                        custumdialogfailure("Invalid QRcode!","You seem to have scanned your own QRcode. Please scan proper QRcode.");
                                    }
                                    else {
                                        custumdialogfailure("Invalid QRcode!","You seem to have scanned non QPay QRcode. Please scan proper QRcode.");
                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    class GetContact extends AsyncTask<String, String, String> {
        String code;

        public GetContact(String code) {
            this.code = code;
        }

        @Override
        protected String doInBackground(String... uri) {
            JSONObject jsonObject_scan = new JSONObject();
            NetworkAPI httpResponse = new NetworkAPI();
            String resukt;
            String status = null;
            try {
                jsonObject_scan.put("reciever_id", code);
                jsonObject_scan.put("app_id", app_id);
                jsonObject_scan.put("cust_id", id);
                jsonObject_scan.put("lat", ((CustomerApplication)getActivity().getApplication()).getLat());
                jsonObject_scan.put("lng", ((CustomerApplication)getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "post id : " + jsonObject_scan);
                resukt = httpResponse.sendHTTPData(Constants.SCAN_NAME, jsonObject_scan);
                Log.d(Utility.TAG, "response id : " + resukt);
                JSONObject jsonObject = new JSONObject(resukt);
                JSONObject jsonObject1 = jsonObject.getJSONObject("getNameResult");
                merchantName = jsonObject1.getString("name");
                imgURL = jsonObject1.getString("imgURL");
                resultScanFriendsCell = jsonObject1.getString("cell_phone");
                status = jsonObject1.getString("status");
                resp = jsonObject1.getString("resp_code");
                Log.d(Utility.TAG, "scan code result: " + status);
                Log.d(Utility.TAG, "merchant name  : " + merchantName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                if (resp.equals("00")) {
                    if (QPayCustomerDatabase.isAlreadyInChatListcontact(resultScanFriendsCell)>0){
                        custumdialogfailure("Already in List!","Already in your chat list.");
                    }else {
                        foundFriends(merchantName, resultScanFriendsCell,imgURL, custIdFriends);
                    }

                } else {
                    custumdialogfailure("Invalid QRcode!","You seem to have scanned invalid QRcode. Please scan proper QRcode.");

                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                try {
                    custumdialogfailure("Invalid QRcode!","You seem to have scanned invalid QRcode. Please scan proper QRcode.");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void setPicassoImage(String url, ImageView imageView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.header_placeholder)
                .error(R.drawable.header_placeholder)
                .noFade()
                .into(imageView);
    }

    public void foundFriends(final String name,final String cell, final String url,final  String cust_id_friend){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.contact_found, null);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView friends_image_found = (ImageView) v.findViewById(R.id.friends_image_found);
        TextView frend_found = (TextView) v.findViewById(R.id.frend_found);
        TextView frendcell_found = (TextView) v.findViewById(R.id.frendcell_found);
        TextView dialogButton = (TextView) v.findViewById(R.id.ok);
        TextView dialogcancel = (TextView) v.findViewById(R.id.cancel);
        TextView isUser = (TextView)v.findViewById(R.id.isUser);

        setPicassoImage(url, friends_image_found);
        dialogButton.setText("Add");
        dialogcancel.setText("Cancel");
        isUser.setText(name+" is QPay User!");
        frend_found.setText(name);
        frendcell_found.setText("Your friend with Phone number "+cell+" is QPay user. Do you want to chat with your friend?");

        v.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.contact_fragment,new AddContactFragment())
                        .commit();

            }
        });

        v.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String date = df.format(Calendar.getInstance().getTime());
                ChatModelList chatModelList = new ChatModelList(cell,name,cust_id_friend,url,date,"No Message.","0");
                QPayCustomerDatabase.insertChatListData(chatModelList);
                startActivity(new Intent(getActivity(), ChatActivity.class));
                getActivity().finish();



            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (420 * density);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setAttributes(lp);
    }
    public void custumdialogfailure(String title,String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title_failed = (TextView)v.findViewById(R.id.title);
        title_failed.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(),ChatActivity.class));
                getActivity().finish();

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
}
