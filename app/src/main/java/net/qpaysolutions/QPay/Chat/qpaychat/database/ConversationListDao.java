package net.qpaysolutions.QPay.Chat.qpaychat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.qpaysolutions.QPay.Chat.qpaychat.model.ConversationList;

import java.util.ArrayList;
import java.util.List;

public class ConversationListDao {
    private DatabaseHelper databaseHelper;
    public ConversationListDao(Context context) {
        this.databaseHelper = new DatabaseHelper(context);
    }

    public long insertMessage(ConversationList messageItem){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ConversationListDB.CUSTOMER_ID, messageItem.getCustomerId());
        values.put(ConversationListDB.ENCRYPTED_CUSTOMER_ID, messageItem.getEncryptedCustomerId());
        values.put(ConversationListDB.APP_ID, messageItem.getAppId());
        values.put(ConversationListDB.CELL_PHONE, messageItem.getCellPhone());
        values.put(ConversationListDB.USER_NAME, messageItem.getUserName());
        values.put(ConversationListDB.LAST_MESSAGE, messageItem.getLastMessage());

        long id = db.insert(ConversationListDB.TABLE_NAME, null, values);

        db.close();

        return id;
    }

    public List<ConversationList> getAllConversation(){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + ConversationListDB.TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);

        List<ConversationList> conversationItems = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                ConversationList conversationList = new ConversationList();
                conversationList.setCustomerId(cursor.getString(cursor.getColumnIndex(ConversationListDB.CUSTOMER_ID)));
                conversationList.setEncryptedCustomerId(cursor.getString(cursor.getColumnIndex(ConversationListDB.ENCRYPTED_CUSTOMER_ID)));
                conversationList.setAppId(cursor.getString(cursor.getColumnIndex(ConversationListDB.APP_ID)));
                conversationList.setCellPhone(cursor.getString(cursor.getColumnIndex(ConversationListDB.CELL_PHONE)));
                conversationList.setUserName(cursor.getString(cursor.getColumnIndex(ConversationListDB.USER_NAME)));
                conversationList.setLastMessage(cursor.getString(cursor.getColumnIndex(ConversationListDB.LAST_MESSAGE)));

                conversationItems.add(conversationList);
            } while (cursor.moveToNext());
        }
        db.close();

        return conversationItems;
    }

    public int getLatestMessageIdByCustomerId(String senderId){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String selectQuery = "SELECT MAX("+ MessageDB.MESSAGE_ID + ") as "+MessageDB.MESSAGE_ID+" FROM " + MessageDB.TABLE_NAME + " WHERE " + MessageDB.SENDER_ID + " = " + senderId;
        Cursor cursor = db.rawQuery(selectQuery, null);

        int messageID = 0;

        if (cursor.moveToFirst()) {
            messageID = cursor.getInt(cursor.getColumnIndex(MessageDB.MESSAGE_ID));

        }


        // close the db connection
        cursor.close();

        return messageID;
    }
}
