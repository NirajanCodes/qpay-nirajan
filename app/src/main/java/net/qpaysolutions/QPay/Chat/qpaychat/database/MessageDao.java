package net.qpaysolutions.QPay.Chat.qpaychat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.qpaysolutions.QPay.Chat.qpaychat.model.ConversationPOJO;
import net.qpaysolutions.QPay.Chat.qpaychat.model.MessageItem;

import java.util.ArrayList;
import java.util.List;

public class MessageDao {
    private DatabaseHelper databaseHelper;
    public MessageDao(Context context) {
        this.databaseHelper = new DatabaseHelper(context);
    }

    public long insertMessage(MessageItem messageItem){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(MessageDB.MESSAGE, messageItem.getMessage());
        values.put(MessageDB.MESSAGE_ID, messageItem.getId());
        
        long id = db.insert(MessageDB.TABLE_NAME, null, values);

        db.close();

        return id;
    }

    public List<ConversationPOJO> getMessagesByCustomerId(String senderId){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + MessageDB.TABLE_NAME + " WHERE " + MessageDB.SENDER_ID + " = " + senderId;
        Cursor cursor = db.rawQuery(selectQuery, null);

        List<ConversationPOJO> messageItems = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                ConversationPOJO messageItem = new ConversationPOJO();
                messageItem.setMessageId(cursor.getInt(cursor.getColumnIndex(MessageDB.MESSAGE_ID)));
                messageItem.setMessage(cursor.getString(cursor.getColumnIndex(MessageDB.MESSAGE)));
                messageItem.setImage(cursor.getInt(cursor.getColumnIndex(MessageDB.IS_IMAGE)) == 1);

                messageItems.add(messageItem);
            } while (cursor.moveToNext());
        }
        db.close();

        return messageItems;
    }

    public int getLatestMessageIdByCustomerId(String senderId){
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String selectQuery = "SELECT MAX("+ MessageDB.MESSAGE_ID + ") as "+MessageDB.MESSAGE_ID+" FROM " + MessageDB.TABLE_NAME + " WHERE " + MessageDB.SENDER_ID + " = " + senderId;
        Cursor cursor = db.rawQuery(selectQuery, null);

        int messageID = 0;

        if (cursor.moveToFirst()) {
            messageID = cursor.getInt(cursor.getColumnIndex(MessageDB.MESSAGE_ID));

        }


        // close the db connection
        cursor.close();

        return messageID;
    }

}
