package net.qpaysolutions.QPay.Chat.chatmessageview.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import net.qpaysolutions.QPay.R;

/**
 * Created by utsavstha on 6/22/17.
 */

public class MessageStatusFormatter implements IMessageStatusIconFormatter, IMessageStatusTextFormatter {
    public static  final int STATUS_DELIVERING = 0;
    public static final int STATUS_DELIVERED = 1;
    public static final int STATUS_ERROR = 2;

    private Drawable mDeliveringIcon;
    private Drawable mDeliveredIcon;
    private Drawable mErrorIcon;
    private String mDeliveringText;
    private String mDeliveredText;
    private String mErrorText;

    public MessageStatusFormatter(Context context){
        mDeliveredIcon = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_sent_chat));
        mDeliveringIcon = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_refresh));

        mErrorIcon = DrawableCompat.wrap(ContextCompat.getDrawable(context, R.drawable.ic_sent_fail_chat));


        mDeliveredText = "Delivered";
        mErrorText = "Please try again";



    }

    @Override
    public Drawable getStatusIcon(int status, boolean isRightMessage) {
        if(!isRightMessage){
            return null;
        }
        switch (status){
            case STATUS_DELIVERING:
                return mDeliveringIcon;

            case STATUS_DELIVERED:
                return mDeliveredIcon;

            case STATUS_ERROR:
                return mErrorIcon;
        }
        return null;
    }


    @Override
    public String getStatusText(int status, boolean isRightMessage) {
        switch (status) {
            case STATUS_DELIVERED:
                return mDeliveredText;

            case STATUS_ERROR:
                return mErrorText;

        }
        return "";
    }
}
