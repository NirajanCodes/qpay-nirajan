package net.qpaysolutions.QPay.Chat.qpaychat;

import android.app.Activity;
import android.util.Log;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class Network {
    static String SOCKET_URL_SEND = "https://qpaysockettest.azurewebsites.net/api/sendmessage";
    static final String USER_NAME = "Utsav";
    //static final String USER_NAME = "QPAY";

    //static final String MERCHANT_ID = "QPAY1";
    static final String MERCHANT_ID = "QPAY2";

    static final String BASE_URL = "https://qpaychattest.qpaysolutions.net/api/Chat/";
    static final String GET_MESSAGES = BASE_URL + "GetChatMessage";
    static final String POST_MESSAGES = BASE_URL + "InsertChatMessage";

//    static final String APP_ID = "123";
//    static final String CUST_ID = "1234";
//    static final String PARTNER_CUST_ID = "12345";

    static final String APP_ID = "2A00000027822qp";
    static final String CUST_ID = "12345";
    static final String PARTNER_CUST_ID = "1234";


    public static void postMessage(String message, Activity activity, final NetworkResponse response){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("appid", Network.APP_ID);
        jsonObject.addProperty("custid", Network.CUST_ID);
        jsonObject.addProperty("ReceiverCustId", Network.PARTNER_CUST_ID);
        jsonObject.addProperty("message", message);
        jsonObject.addProperty("isImage", false);
        apiCallPost(response, jsonObject, activity, Network.POST_MESSAGES);
    }

    public static void getMessage(Activity activity, int messageId, final NetworkResponse response){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("appid", Network.APP_ID);
        jsonObject.addProperty("custid", Network.CUST_ID);
        jsonObject.addProperty("id", messageId);
        apiCallPost(response, jsonObject, activity, Network.GET_MESSAGES);
    }

    public static void postIsTyping(String username, Activity activity, final NetworkResponse response){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("merchantId", "QPAY2");
        jsonObject.addProperty("token", "EgRCe7D9h8iHTyqqWZQcDg==");

        JsonObject messageParam = new JsonObject();
        //0 for posting message, 1 for isTyping
        messageParam.addProperty("type", "1");
        messageParam.addProperty("message", "");
        messageParam.addProperty("isImage", false);
        messageParam.addProperty("senderName", Network.USER_NAME);
        jsonObject.addProperty("message", messageParam.toString());
        apiCallPost(response, jsonObject, activity, Network.SOCKET_URL_SEND);
    }

    private static void apiCallPost(final NetworkResponse response, JsonObject jsonObject, Activity context, String url) {
        Log.d("API", jsonObject.toString());
        Ion.with(context)
                .load(url)
                .setJsonObjectBody(jsonObject)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e != null) {
                            response.onError(result);
                        } else {
                            response.onSuccess(result);
                        }
                    }
                });
    }
}
