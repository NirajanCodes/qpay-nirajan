package net.qpaysolutions.QPay.Chat.contact;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by deadlydragger on 3/23/17.
 */

public class AddContactFragment extends Fragment implements View.OnClickListener {
    QPayCustomerDatabase QPayCustomerDatabase;
    SQLiteDatabase sqLiteDatabase;
    TextView add_to_contact;
    EditText phone_no;
    String cell_no_friend,name_friend,enc_cust_id,img_url;
    String app_id,status_friend,resp_code,user_cell_phone,custId;
    QPayProgressDialog pDialog;
    LinearLayout goto_scan;
    private static final int REQUEST = 112;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AddContactActivity)getActivity()).setTitleToolbar("Add Contact");
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        app_id= QPayCustomerDatabase.getKeyAppId();
        custId= QPayCustomerDatabase.getCustomerID();
        user_cell_phone= QPayCustomerDatabase.getCustomerPhone();
        sqLiteDatabase= QPayCustomerDatabase.getWritableDatabase();
        add_to_contact=(TextView)view.findViewById(R.id.add_to_contact);
        phone_no=(EditText)view.findViewById(R.id.phone_no);
        goto_scan=(LinearLayout)view.findViewById(R.id.goto_scan);
        goto_scan.setOnClickListener(this);
        add_to_contact.setOnClickListener(this);
        phone_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>=10){
                    hideSoftkey(phone_no);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                phone_no.setError(null);
                if (s.length()>=10){
                    hideSoftkey(phone_no);
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_contact,container,false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_to_contact:
                try{
                    hideSoftkey(phone_no);
                    String phone= phone_no.getText().toString();
                    if (phone != null && !phone.isEmpty() && phone.length()==10){
                        if (!user_cell_phone.equals(phone)){
                            if (QPayCustomerDatabase.isAlreadyInChatListcontact(phone)>0){
                                new Utility().showSnackbar(v,"Already in your chat list.");

                            }else {
                                new LookUpFriends(phone).execute();
                            }
                        }else {
                            new Utility().showSnackbar(v,"Enter friend's number to add to chat list.");
                        }
                    }else {
                        phone_no.setError("Valid Number Required");
                    }


                }catch (Exception e){
                    e.printStackTrace();
                    new Utility().showSnackbar(v,"Enter friend's number to add to chat list.");
                }

                break;
            case R.id.goto_scan:
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {Manifest.permission.CAMERA};
                    if (!((AddContactActivity)getActivity()).hasPermissions(getContext(), PERMISSIONS)) {
                        ActivityCompat.requestPermissions(getActivity(),PERMISSIONS, REQUEST );
                    } else {
                        new Utility().hideSoftkey(getActivity());
                        getFragmentManager().beginTransaction()
                                .replace(R.id.contact_fragment,new ScanContactFragment())
                                .commit();
                    }
                } else {
                    new Utility().hideSoftkey(getActivity());
                    getFragmentManager().beginTransaction()
                            .replace(R.id.contact_fragment,new ScanContactFragment())
                            .commit();
                }


                break;
        }
    }
    public void hideSoftkey(EditText str) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(str.getWindowToken(), 0);
    }
    public class LookUpFriends extends AsyncTask<String, String, String> {
        String cell_phone;
        public LookUpFriends(String cell_phoone) {
            this.cell_phone = cell_phoone;
        }
        @Override
        protected String doInBackground(String... params) {
            String friends_list_from;
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject_fri = new JSONObject();
            try {
                jsonObject_fri.put("app_id", app_id);
                jsonObject_fri.put("cell_phone", cell_phone);
                jsonObject_fri.put("custId",custId);
                jsonObject_fri.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_fri.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                friends_list_from = networkAPI.sendHTTPData(Constants.LOOK_UP_FRIENDS, jsonObject_fri);
                Log.d(Utility.TAG, "lookup result send : " + friends_list_from);
                JSONObject jsonObject = new JSONObject(friends_list_from);
                JSONObject jsonObject1 = jsonObject.getJSONObject("lookupfriendResult");
                Log.d(Utility.TAG, jsonObject1.toString());
                cell_no_friend = jsonObject1.getString("cell_no");
                name_friend = jsonObject1.getString("name");
                status_friend = jsonObject1.getString("status");
                enc_cust_id = jsonObject1.getString("enc_cust_id");
                img_url = jsonObject1.getString("imgURL");
                resp_code=jsonObject1.getString("resp_code");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status_friend;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            try {

                if (!s.equals("null") && !s.isEmpty() && s != null &s.equals("00")) {
                    if (resp_code.equals("00")){

                        foundFriends(name_friend,cell_no_friend,img_url);

                    }else {
                     new Utility().showSnackbar(goto_scan,"Not Found!");
                    }

                } else {
                    new Utility().showSnackbar(goto_scan,"Not Found!");

                }

            } catch (Exception e) {
                e.printStackTrace();
                new Utility().showSnackbar(goto_scan,"Not Found!");

            }
        }
    }
    public void foundFriends(String name, String cell, final String url){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.contact_found, null);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView friends_image_found = (ImageView) v.findViewById(R.id.friends_image_found);
        TextView frend_found = (TextView) v.findViewById(R.id.frend_found);
        TextView frendcell_found = (TextView) v.findViewById(R.id.frendcell_found);
        TextView dialogButton = (TextView) v.findViewById(R.id.ok);
        TextView dialogcancel = (TextView) v.findViewById(R.id.cancel);
        TextView isUser = (TextView)v.findViewById(R.id.isUser);

        setPicassoImage(url, friends_image_found);
        dialogButton.setText("Add");
        dialogcancel.setText("Cancel");
        isUser.setText(name+" is QPay User!");
        frend_found.setText(name);
        frendcell_found.setText("Your friend with Phone number "+cell+" is QPay user. Do you want to chat with your friend?");

        v.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        v.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String date = df.format(Calendar.getInstance().getTime());
                ChatModelList chatModelList = new ChatModelList(cell_no_friend,name_friend,enc_cust_id,img_url,date,"No Message.","0");
                QPayCustomerDatabase.insertChatListData(chatModelList);
                startActivity(new Intent(getActivity(), ChatActivity.class));
                getActivity().finish();


            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (420 * density);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setAttributes(lp);
    }
    public void setPicassoImage(String url,ImageView imageView){
        if (!url.isEmpty()){
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.header_placeholder)
                    .error(R.drawable.header_placeholder)
                    .into(imageView);
        }
    }
}
