package net.qpaysolutions.QPay.Chat.chatList;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ChatModelList {
    String cell_no_chat,name_chat,cust_id_friend_chat,img_url_chat,date_chat,msg_chat;
    String TotalUnreadMsg;
    int _id;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTotalUnreadMsg() {
        return TotalUnreadMsg;
    }

    public void setTotalUnreadMsg(String totalUnreadMsg) {
        TotalUnreadMsg = totalUnreadMsg;
    }

    public ChatModelList() {
    }

    public ChatModelList(String cell_no_chat, String name_chat, String cust_id_friend_chat, String img_url_chat, String date_chat, String msg_chat,String TotalUnreadMsg) {
        this.cell_no_chat = cell_no_chat;
        this.name_chat = name_chat;
        this.cust_id_friend_chat = cust_id_friend_chat;
        this.img_url_chat = img_url_chat;
        this.date_chat = date_chat;
        this.msg_chat = msg_chat;
        this.TotalUnreadMsg=TotalUnreadMsg;
    }

    public String getCell_no_chat() {
        return cell_no_chat;
    }

    public void setCell_no_chat(String cell_no_chat) {
        this.cell_no_chat = cell_no_chat;
    }

    public String getName_chat() {
        return name_chat;
    }

    public void setName_chat(String name_chat) {
        this.name_chat = name_chat;
    }

    public String getCust_id_friend_chat() {
        return cust_id_friend_chat;
    }

    public void setCust_id_friend_chat(String cust_id_friend_chat) {
        this.cust_id_friend_chat = cust_id_friend_chat;
    }

    public String getImg_url_chat() {
        return img_url_chat;
    }

    public void setImg_url_chat(String img_url_chat) {
        this.img_url_chat = img_url_chat;
    }

    public String getDate_chat() {
        return date_chat;
    }

    public void setDate_chat(String date_chat) {
        this.date_chat = date_chat;
    }

    public String getMsg_chat() {
        return msg_chat;
    }

    public void setMsg_chat(String msg_chat) {
        this.msg_chat = msg_chat;
    }
}
