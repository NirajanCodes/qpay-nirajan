package net.qpaysolutions.QPay.Chat.chatList;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ChatAdapterList extends RecyclerView.Adapter<ChatViewHolderList> {

    ChatFragmentList chatFragmentList;
    ArrayList<ChatModelList> chatModelLists;

    public ChatAdapterList(ChatFragmentList chatFragmentList, ArrayList<ChatModelList> chatModelLists) {
        this.chatFragmentList = chatFragmentList;
        this.chatModelLists = chatModelLists;
    }

    public void updateData(ArrayList<ChatModelList> chatModelLists){
        this.chatModelLists = chatModelLists;
        notifyDataSetChanged();
    }

    @Override
    public ChatViewHolderList onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, parent, false);
        ChatViewHolderList chatViewHolderList = new ChatViewHolderList(view);
        return chatViewHolderList;
    }

    @Override
    public void onBindViewHolder(ChatViewHolderList holder, int position) {
        chatFragmentList.setImage(chatModelLists.get(position).getImg_url_chat(), holder.url);
        holder.msg.setText(chatModelLists.get(position).getMsg_chat());
        holder.name.setText(chatModelLists.get(position).getName_chat());
        if (chatModelLists.get(position).getTotalUnreadMsg() != null && !chatModelLists.get(position).getTotalUnreadMsg().equals("0")){
            holder.message_count.setVisibility(View.VISIBLE);
            holder.message_count.setText(chatModelLists.get(position).getTotalUnreadMsg());
        } else {
            holder.message_count.setVisibility(View.GONE);
        }
        SimpleDateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date_set = srcDf.parse(chatModelLists.get(position).getDate_chat());
            SimpleDateFormat destDf = new SimpleDateFormat("MMM dd, yyyy");
            SimpleDateFormat dayOnly = new SimpleDateFormat("yyyy-MM-dd");
            String day_Only = dayOnly.format(date_set);
            String day_today = chatFragmentList.getDay();
            if (day_today.equals(day_Only)) {
                SimpleDateFormat today = new SimpleDateFormat("hh:mm");
                holder.date.setText(today.format(date_set));
            } else {
                holder.date.setText(destDf.format(date_set));
            }
//            holder.date.setText(destDf.format(date_set));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount()
    {
        return chatModelLists.size();
    }
}
