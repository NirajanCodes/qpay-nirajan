package net.qpaysolutions.QPay.Chat.qpaychat.model;

import com.google.gson.annotations.SerializedName;

public class ReceivedMessagePOJO{

	@SerializedName("isImage")
	private boolean isImage;

	@SerializedName("Id")
	private int id;

	@SerializedName("Message")
	private String message;

	public void setIsImage(boolean isImage){
		this.isImage = isImage;
	}

	public boolean isIsImage(){
		return isImage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}