package net.qpaysolutions.QPay.Chat.qpaychat.model;

import com.google.gson.annotations.SerializedName;

public class MessageItem{

	public MessageItem(String message, boolean isImage, int id) {
		this.message = message;
		this.isImage = isImage;
		this.id = id;
	}

	@SerializedName("Message")
	private String message;

	@SerializedName("IsImage")
	private boolean isImage;

	@SerializedName("Id")
	private int id;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setIsImage(boolean isImage){
		this.isImage = isImage;
	}

	public boolean isIsImage(){
		return isImage;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"MessageItem{" + 
			"message = '" + message + '\'' + 
			",isImage = '" + isImage + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}