package net.qpaysolutions.QPay.Chat.qpaychat.model;

public class ConversationList {
    private String customerId;
    private String encryptedCustomerId;
    private String appId;
    private String cellPhone;
    private String userName;
    private String lastMessage;

    public ConversationList() {
    }

    public ConversationList(String customerId, String encryptedCustomerId, String appId, String cellPhone, String userName, String lastMessage) {
        this.customerId = customerId;
        this.encryptedCustomerId = encryptedCustomerId;
        this.appId = appId;
        this.cellPhone = cellPhone;
        this.userName = userName;
        this.lastMessage = lastMessage;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getEncryptedCustomerId() {
        return encryptedCustomerId;
    }

    public void setEncryptedCustomerId(String encryptedCustomerId) {
        this.encryptedCustomerId = encryptedCustomerId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }
}
