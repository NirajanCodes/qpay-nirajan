package net.qpaysolutions.QPay.Chat.qpaychat.database;

public class ConversationListDB {

    public static final String TABLE_NAME = "Conversation";

    public static final String COLUMN_ID = "columns_id";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String ENCRYPTED_CUSTOMER_ID = "enc_customer_id";
    public static final String APP_ID = "app_id";
    public static final String CELL_PHONE = "cell_phone";
    public static final String USER_NAME = "username";
    public static final String LAST_MESSAGE = "last_message";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + CUSTOMER_ID + " TEXT,"
                    + ENCRYPTED_CUSTOMER_ID + " TEXT,"
                    + APP_ID + " TEXT,"
                    + CELL_PHONE + " INTEGER,"
                    + LAST_MESSAGE + " TEXT,"
                    + USER_NAME + " TEXT"
                    + ")";

}
