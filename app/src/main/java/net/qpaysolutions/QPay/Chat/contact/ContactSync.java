package net.qpaysolutions.QPay.Chat.contact;

/**
 * Created by deadlydragger on 3/22/17.
 */

public class ContactSync {
    String enc_cust_id,cell_no,imgURL,Name;

    public String getEnc_cust_id() {
        return enc_cust_id;
    }

    public void setEnc_cust_id(String enc_cust_id) {
        this.enc_cust_id = enc_cust_id;
    }

    public String getCell_no() {
        return cell_no;
    }

    public void setCell_no(String cell_no) {
        this.cell_no = cell_no;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
