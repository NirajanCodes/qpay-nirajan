package net.qpaysolutions.QPay.Chat.contact;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.CustumClasses.RoundImage;

/**
 * Created by deadlydragger on 3/20/17.
 */

public class ContactViewHolder extends RecyclerView.ViewHolder{
    ImageView first_letter;
    TextView name,number;
    LinearLayout click,ic_chat;
    RoundImage round_image;
    public ContactViewHolder(View itemView) {
        super(itemView);
        first_letter=(ImageView)itemView.findViewById(R.id.first_letter);
        name=(TextView)itemView.findViewById(R.id.name);
        number=(TextView)itemView.findViewById(R.id.number);
        ic_chat=(LinearLayout)itemView.findViewById(R.id.ic_chat);
        click=(LinearLayout)itemView.findViewById(R.id.layout_click);
        round_image=(RoundImage)itemView.findViewById(R.id.round_image);
    }
}
