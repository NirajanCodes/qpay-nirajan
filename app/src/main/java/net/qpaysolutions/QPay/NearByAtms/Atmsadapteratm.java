package net.qpaysolutions.QPay.NearByAtms;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 9/13/16.
 */
public class Atmsadapteratm extends RecyclerView.Adapter<Atmsadapteratm.ViewHolder> {
    ArrayList<Atms> itemsData;
    private NearbyAtmsFragment context;
    private int lastPosition = -1;
    public Atmsadapteratm(NearbyAtmsFragment context, ArrayList<Atms> itemsData) {
        this.itemsData = itemsData;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view_atm_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
//        context.setAnimation(viewHolder.itemView, position,lastPosition);
        viewHolder.bank_name.setText(itemsData.get(position).getBank_name());
        viewHolder.adddress.setText(itemsData.get(position).getAddress());
        viewHolder.distance.setText(itemsData.get(position).getRad() +" Km away from you");
        if (!itemsData.get(position).getImgUrl().isEmpty()){
            context.setPicassoImage(itemsData.get(position).getImgUrl(),viewHolder.atms_logo);
        }else {
            viewHolder.atms_logo.setImageResource(R.drawable.ic_atm_default_icon);
        }
//        context.setPicassoImage(itemsData.get(position).getImgUrl(),viewHolder.atms_logo);


    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView bank_name, adddress, distance;
        public ImageView atms_logo;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            bank_name = (TextView) itemLayoutView.findViewById(R.id.bank_name);
            distance = (TextView) itemLayoutView.findViewById(R.id.distance);
            adddress = (TextView) itemLayoutView.findViewById(R.id.address);
            atms_logo=(ImageView)itemLayoutView.findViewById(R.id.bank_img);

        }
    }
    @Override
    public int getItemCount() {

        return itemsData.size();
    }

   /* private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context.this, R.anim.scale_shop_list);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }*/
}