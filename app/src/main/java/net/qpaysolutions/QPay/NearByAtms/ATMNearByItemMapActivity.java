package net.qpaysolutions.QPay.NearByAtms;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.List;
import java.util.Locale;

/**
 * Created by deadlydragger on 9/13/16.
 */
public class ATMNearByItemMapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private Toolbar toolbar;
    private String category_name, size;
    private String address, city;
    private GoogleMap googleMap;
    private TextView my_location, shop_near_you, toolbar_text;
    private Atms myList;
    private String withdraw;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_item_map);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        category_name = getIntent().getStringExtra("category_name");
        size = getIntent().getStringExtra("size");
        withdraw = getIntent().getStringExtra("withdraw");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText("Location");
      /*  toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/
        my_location = (TextView) findViewById(R.id.my_location);
        shop_near_you = (TextView) findViewById(R.id.shop_near_you);
        myList = (Atms) getIntent().getSerializableExtra("mylist");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        initilizeMap();
    }

    private void initilizeMap() {
        if (googleMap == null) {
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!Utility.hasExternalStoragePermission(ATMNearByItemMapActivity.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(ATMNearByItemMapActivity.this, PERMISSIONS, Tags.LOCATION);
            } else {
                setMap(googleMap);
            }
        } else {
            setMap(googleMap);
        }
    }

    private void setMap(final GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }

        mFusedLocationClient.getLastLocation()
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                }).addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(final Location location) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (location != null) {
                                    new GetAddressLocation(Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng())).execute();
                                    try {
                                        if (withdraw.equals("atm")) {
                                            createMarker(myList.getLat(), myList.getLng(), myList.getBank_name(), R.drawable.ic_atm_map_marker, googleMap);
                                        } else if (withdraw.equals("bank")) {
                                            createMarker(myList.getLat(), myList.getLng(), myList.getBank_name(), R.drawable.ic_bank_location_marker, googleMap);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng()))).zoom(12).build();
                                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                                    if (googleMap == null) {
                                        Toast.makeText(getApplicationContext(),
                                                "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                }  else if (Build.VERSION.SDK_INT >= 23) {
                                    String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                                    if (!Utility.hasExternalStoragePermission(ATMNearByItemMapActivity.this, PERMISSIONS)) {
                                        ActivityCompat.requestPermissions(ATMNearByItemMapActivity.this, PERMISSIONS, Tags.LOCATION);
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    }
                                }
                            }
                        }, 500);
                    }
                }
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setMap(googleMap);
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give camera permission.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public class GetAddressLocation extends AsyncTask<String, String, String> {
        double latitude, longitude;

        public GetAddressLocation(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        protected String doInBackground(String... params) {

            return getAdress(latitude, longitude);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            my_location.setText(s);
        }
    }

    protected void createMarker(double latitude, double longitude, String title, int image, GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(image))
//              .snippet(phone)
                .title(title));
    }

    public String getAdress(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            return address + "," + city;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        /*Intent intent = new Intent(WithdrawalIteamDetails.this, MainActivity.class);
        intent.putExtra("Check",6);
        startActivity(intent);*/
    }
}