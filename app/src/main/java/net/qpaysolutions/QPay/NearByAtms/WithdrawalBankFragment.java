package net.qpaysolutions.QPay.NearByAtms;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 9/13/16.
 */
public class WithdrawalBankFragment extends Fragment {
  private   QPayProgressDialog progressDialog;

    private  RecyclerView recycler;
    private  BankAdapter myAdapter;
    private ArrayList<Atms> listBank = new ArrayList<>();
    private LinearLayout no_atm;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.withdraw_list,container,false);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler=(RecyclerView)view.findViewById(R.id.recycler);
        no_atm=(LinearLayout)view.findViewById(R.id.no_atm);
        new Getnearbybanks().execute();
    }
    public class Getnearbybanks extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... uri) {
            String result = null;
            try {

                    result = new NetworkAPI().getFromurl(Constants.BASE_API_CHAT+"PaymentTerminal/NearestPayoutTerminal?locLng="+ LatLngPref.getLng()+"&loclat="+LatLngPref.getLat()+"&rad="+100);
                    Log.d(Utility.TAG,"result branch bank : "+result);


            }catch (Exception e){
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new QPayProgressDialog(getActivity());
            progressDialog.setCancelable(true);
            progressDialog.show();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

                try {
                    progressDialog.dismiss();
                  /*  progressDialog.hide();
                    progressDialog.dismiss();*/
                    JSONObject jsonObject = new JSONObject(s);
                    boolean success = jsonObject.getBoolean("success");
                    String message = jsonObject.getString("message");
                    String status = jsonObject.getString("status");
                    if (success == true){
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
//                        taxi_near_you.setText(String.valueOf(jsonArray.length()));
                        Log.d(Utility.TAG,String.valueOf(jsonArray.length()));
                        for (int i = 0 ; i<jsonArray.length();i++){
                            Atms bankBranch = new Atms();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            bankBranch.setAddress(jsonObject1.getString("Address"));
                            bankBranch.setBank_name(jsonObject1.getString("BranchName"));
                            bankBranch.setInstitutionName(jsonObject1.getString("InstitutionName"));
                            bankBranch.setLat(jsonObject1.getDouble("LocLat"));
                            bankBranch.setLng(jsonObject1.getDouble("LocLng"));
                            bankBranch.setRad(new Decimalformate().decimalFormate( jsonObject1.getString("Rad")));
                            bankBranch.setImgUrl(jsonObject1.getString("ImageUrl"));
                            listBank.add(bankBranch);
                        }
                        if (listBank.size() > 0){
                            no_atm.setVisibility(View.GONE);
                            recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                            recycler.setHasFixedSize(true);
                            myAdapter = new BankAdapter(WithdrawalBankFragment.this, listBank);
                            recycler.setAdapter(myAdapter);
                            recycler.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recycler, new ClickListener() {
                                @Override
                                public void onClick(View view, int position) {
                                    Intent intent = new Intent(getActivity(), ATMNearByItemMapActivity.class);
//                                    intent.putExtra("position",position);
                                    intent.putExtra("mylist",listBank.get(position));
                                    intent.putExtra("withdraw","bank");
                                    startActivity(intent);
                                }

                                @Override
                                public void onLongClick(View view, int position) {

                                }
                            }));
                        }else {
                            no_atm.setVisibility(View.VISIBLE);
                        }

                    }else {
                        no_atm.setVisibility(View.VISIBLE);
                    }


                } catch (Exception e) {no_atm.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    e.printStackTrace();
                }

        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (listBank.size()>0){
            inflater.inflate(R.menu.shop_menu, menu);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
                Intent intent = new Intent(getActivity(), NearByBankListDetailsActivity.class);
                intent.putExtra("mylist",listBank);
                intent.putExtra("location","Bank Location");
                intent.putExtra("size","Branch");
                startActivity(intent);
//                getActivity().finish();
                break;
        }
        return true;
    }
    public void setPicassoImage(String url,ImageView custum_header_image) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.ic_default_withdraw_icon)
                .error(R.drawable.ic_default_withdraw_icon)
                .noFade()
                .into(custum_header_image);
    }
    public void setAnimation(View viewToAnimate, int position,int lastPosition) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_shop_list);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private WithdrawalBankFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final WithdrawalBankFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}