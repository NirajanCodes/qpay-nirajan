package net.qpaysolutions.QPay.NearByAtms;

import java.io.Serializable;

/**
 * Created by deadlydragger on 9/13/16.
 */
public class Atms implements Serializable {
    String address;
    String bank_name;
    String imgUrl;
    double lat;
    double lng;
    String rad;
    String InstitutionName;

    public String getInstitutionName() {
        return InstitutionName;
    }

    public void setInstitutionName(String institutionName) {
        InstitutionName = institutionName;
    }

    public String getAddress() {
        return address;
    }

    public String getRad() {
        return rad;
    }

    public double getLng() {
        return lng;
    }

    public String getBank_name() {
        return bank_name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public double getLat() {
        return lat;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setRad(String rad) {
        this.rad = rad;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
