package net.qpaysolutions.QPay.NearByAtms;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 9/13/16.
 */
public class NearbyAtmsFragment extends Fragment {
    QPayProgressDialog qPayProgressDialog;
    String app_id, cust_id;
    QPayCustomerDatabase QPayCustomerDatabase;
    ArrayList<Atms> atmsArrayList = new ArrayList<>();
    RecyclerView recycler;
    Atmsadapteratm myAdapter;
    LinearLayout no_atm;
    private MenuItem menuItem;
    private Context context;
    private FusedLocationProviderClient mFusedLocationClient;
    private LinearLayout progressLayoutAtm;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.atm_list, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        try {
            QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
            QPayCustomerDatabase.getReadableDatabase();
            app_id = QPayCustomerDatabase.getKeyAppId();
            cust_id = QPayCustomerDatabase.getCustomerID();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressLayoutAtm = view.findViewById(R.id.progressLayoutAtm);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
       /* ((MainActivity) getActivity()).setToolbar_text("ATM Location");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(true);*/
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        no_atm = (LinearLayout) view.findViewById(R.id.no_atm);
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!hasExternalStoragePermission(getContext(), PERMISSIONS)) {
                ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, Tags.LOCATION);

            } else {
                getATM();
            }
        } else {
            getATM();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getActivity().getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        progressLayoutAtm.setVisibility(View.GONE);
        /*if (qPayProgressDialog != null) {
            qPayProgressDialog.dismiss();
        }*/
    }

    public void getATM() {
        new ATMNearBy().execute();
    }

    public static boolean hasExternalStoragePermission(Context context, String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public class ATMNearBy extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String atms = null;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                Log.d(Utility.TAG, "atms post : " + jsonObject);
                atms = networkAPI.sendHTTPData(Constants.NEARBY_ATM, jsonObject);
                Log.d(Utility.TAG, "atms response : " + atms);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return atms;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            qPayProgressDialog.dismiss();
            progressLayoutAtm.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("AtmNearbyCustomerResult");
                JSONArray atmNearby = jsonObject1.getJSONArray("atmNearby");
                for (int i = 0; i < atmNearby.length(); i++) {
                    Atms atms = new Atms();
                    JSONObject jsonObject2 = atmNearby.getJSONObject(i);
                    String address = jsonObject2.getString("address");
                    String bank_name = jsonObject2.getString("bank_name");
                    String imgUrl = jsonObject2.getString("imgUrl");
                    double lat = jsonObject2.getDouble("lat");
                    double lng = jsonObject2.getDouble("lng");
                    String rad = jsonObject2.getString("rad");
                    atms.setAddress(address);
                    atms.setBank_name(bank_name);
                    atms.setImgUrl(imgUrl);
                    atms.setLat(lat);
                    atms.setLng(lng);
                    atms.setRad(new Decimalformate().decimalFormate(rad));
                    atmsArrayList.add(atms);
                }
                if (atmsArrayList.size() > 0) {
                    menuItem.setVisible(true);
                    no_atm.setVisibility(View.GONE);
                    recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    recycler.setHasFixedSize(true);
                    myAdapter = new Atmsadapteratm(NearbyAtmsFragment.this, atmsArrayList);
                    recycler.setAdapter(myAdapter);
                    recycler.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recycler, new ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            Intent intent = new Intent(getActivity(), ATMNearByItemMapActivity.class);
//                            intent.putExtra("position", position);
                            intent.putExtra("mylist", atmsArrayList.get(position));
                            intent.putExtra("withdraw", "atm");
                            startActivity(intent);
                        }

                        @Override
                        public void onLongClick(View view, int position) {
                        }
                    }));
                } else {
                    no_atm.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
                no_atm.setVisibility(View.VISIBLE);

            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  qPayProgressDialog = new QPayProgressDialog(getActivity());
            qPayProgressDialog.setCancelable(false);
            qPayProgressDialog.show();*/
            progressLayoutAtm.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.shop_menu, menu);
        MenuItem menuItemm = menu.findItem(R.id.map);
        menuItemm.setVisible(false);
        menuItem = menuItemm;



    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
                if (atmsArrayList.size() > 0) {
                    Intent intent = new Intent(getActivity(), NearByBankListDetailsActivity.class);
                    intent.putExtra("mylist", atmsArrayList);
                    intent.putExtra("location", "ATM Location");
                    intent.putExtra("size", "ATMs");
                    startActivity(intent);
                }
                break;
        }
        return true;
    }

    public void setPicassoImage(String url, ImageView custum_header_image) {
        try {
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.ic_atm_default_icon)
                    .error(R.drawable.ic_atm_default_icon)
                    .noFade()
                    .into(custum_header_image);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private NearbyAtmsFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final NearbyAtmsFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().onBackPressed();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onDestroy() {

        super.onDestroy();

    }
}