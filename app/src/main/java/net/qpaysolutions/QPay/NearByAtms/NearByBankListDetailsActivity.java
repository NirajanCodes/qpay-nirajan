package net.qpaysolutions.QPay.NearByAtms;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by deadlydragger on 9/11/16.
 */
public class NearByBankListDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private  Toolbar toolbar;
    private   String size, location;
    private   String address, city;
    private GoogleMap googleMap;
    private  TextView my_location, shop_near_you, center, toolbar_text;
    private View myView;
    private ArrayList<Atms> myList;
    private BankListAdapter myAdapter;
    private  RecyclerView mRecyclerView;
    private Marker marker;
    private  int position;
    private FusedLocationProviderClient mFusedLocationClient;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_shop_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        size = getIntent().getStringExtra("size");
        position = getIntent().getIntExtra("position", 0);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText("ATM Location");
        center = (TextView) findViewById(R.id.center);
        center.setText(size);
        my_location = (TextView) findViewById(R.id.my_location);
        shop_near_you = (TextView) findViewById(R.id.shop_near_you);
        setTitle(location);
        myList = (ArrayList<Atms>) getIntent().getSerializableExtra("mylist");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        initilizeMap();
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setHasFixedSize(true);
        myAdapter = new BankListAdapter(NearByBankListDetailsActivity.this, myList);
        mRecyclerView.setAdapter(myAdapter);
        shop_near_you.setText(String.valueOf(myList.size()));

        mRecyclerView.addOnItemTouchListener(new NearByBankListDetailsActivity.RecyclerTouchListener(NearByBankListDetailsActivity.this, mRecyclerView, new NearByBankListDetailsActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(myList.get(position).getLat(), myList.get(position).getLng())).zoom(18).build();

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMapReady) {
        googleMap = googleMapReady;

        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!Utility.hasExternalStoragePermission(NearByBankListDetailsActivity.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(NearByBankListDetailsActivity.this, PERMISSIONS, Tags.LOCATION);
            } else {
                setMap(googleMapReady);
            }
        } else {
            setMap(googleMapReady);

        }


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    setMap(googleMap);

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give camera permission.", Toast.LENGTH_LONG).show();
                }

                break;

        }
    }

    public void setMap(final GoogleMap googleMap){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        mFusedLocationClient.getLastLocation()
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                if (location != null) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                            LatLngPref.setLng(String.valueOf(location.getLongitude()));

                                            for (int i = 0; i < myList.size(); i++) {
                                                createMarker(myList.get(i).getLat(), myList.get(i).getLng(), R.drawable.ic_atm_map_marker, googleMap);
                                            }
                                            new GetAddressLocation(Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng())).execute();
                                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng()))).zoom(10).build();
                                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                                            if (googleMap == null) {
                                                Toast.makeText(getApplicationContext(),
                                                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                                                        .show();
                                            }
                                        }
                                    },500);


                                } else {
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }
                            }
                        }
                );
    }

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {
        private GestureDetector gestureDetector;
        private NearByBankListDetailsActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final NearByBankListDetailsActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private void initilizeMap() {
        try {
            if (googleMap == null) {
                MapFragment mapFragment = (MapFragment) getFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void createMarker(double latitude, double longitude, int image, GoogleMap googleMap) {

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(image))
        );
    }


    public class GetAddressLocation extends AsyncTask<String, String, String> {
        double latitude, longitude;

        public GetAddressLocation(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        protected String doInBackground(String... params) {

            return getAdress(latitude, longitude);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            my_location.setText(s);
        }
    }


    public String getAdress(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            return address + "," + city;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.ViewHolder> {
        ArrayList<Atms> itemsData;
        private Context context;

        private int lastPosition = -1;

        public BankListAdapter(Context context, ArrayList<Atms> itemsData) {
            this.itemsData = itemsData;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            View itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_view_withdraw, parent, false);

            ViewHolder viewHolder = new ViewHolder(itemLayoutView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            viewHolder.txtViewTitle.setText(itemsData.get(position).getBank_name());
            viewHolder.distance.setText(itemsData.get(position).getRad());
            viewHolder.number.setText(itemsData.get(position).getAddress());

        }

        // inner class to hold a reference to each item of RecyclerView
        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtViewTitle, distance, number;
            public ImageView makecall;


            public ViewHolder(View itemLayoutView) {
                super(itemLayoutView);
                myView = itemLayoutView;
                txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
                distance = (TextView) itemLayoutView.findViewById(R.id.item_distance);
                number = (TextView) itemLayoutView.findViewById(R.id.item_number);
                makecall = (ImageView) itemLayoutView.findViewById(R.id.item_makecall);
            }
        }

        // Return the size of your itemsData (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return itemsData.size();
        }

        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(context, R.anim.scale);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }
    }
}


