package net.qpaysolutions.QPay.TrafficHistory;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.QRcodegenerator.Contents;
import net.qpaysolutions.QPay.QRcodegenerator.QRCodeEncoder;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 5/26/17.
 */

public class TrafficHistoryFragment extends Fragment {
    RecyclerView recycle_view;
    TrafficAdapter trafficAdapter;
    ArrayList<TrafficModel> trafficModelArrayList;
    QPayCustomerDatabase QPayCustomerDatabase;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.traffic_history,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity)getActivity()). setToolbar_text("Traffic History");
        ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);
        recycle_view=(RecyclerView)view.findViewById(R.id.recycle_view);
        recycle_view.setHasFixedSize(true);
        recycle_view.setLayoutManager(new LinearLayoutManager(getContext()));
        trafficAdapter=new TrafficAdapter(trafficModelArrayList,TrafficHistoryFragment.this);
        recycle_view.setAdapter(trafficAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase =new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        trafficModelArrayList= QPayCustomerDatabase.getAllTrafficLog();
    }

    public void create(ImageView myImage,String stan) {
//        String qrInputTextGeneral = sctDatabaseHelper.getEncryptedCustomerId();
        String qrInputText = null;
        String encryptedMsg = stan;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                if (bitmap != null) {
                    myImage.setImageBitmap(bitmap);
                } else {
                    create(myImage,stan);
                }


            } catch (WriterException e) {
                e.printStackTrace();
                try {
                    create(myImage,stan);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        }
    }

}
