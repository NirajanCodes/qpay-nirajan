package net.qpaysolutions.QPay.TrafficHistory;

/**
 * Created by deadlydragger on 5/26/17.
 */

public class TrafficModel {

    String name,chit_id,identification_id,cause,amount,crrn,stan;

    public TrafficModel() {

    }

    public TrafficModel(String name, String chit_id, String identification_id, String cause, String amount, String crrn, String stan) {
        this.name = name;
        this.chit_id = chit_id;
        this.identification_id = identification_id;
        this.cause = cause;
        this.amount = amount;
        this.crrn = crrn;
        this.stan = stan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChit_id() {
        return chit_id;
    }

    public void setChit_id(String chit_id) {
        this.chit_id = chit_id;
    }

    public String getIdentification_id() {
        return identification_id;
    }

    public void setIdentification_id(String identification_id) {
        this.identification_id = identification_id;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCrrn() {
        return crrn;
    }

    public void setCrrn(String crrn) {
        this.crrn = crrn;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }
}
