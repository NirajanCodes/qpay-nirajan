package net.qpaysolutions.QPay.TrafficHistory;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 5/26/17.
 */

public class TrafficHolder extends RecyclerView.ViewHolder {
    TextView name,chit_id,cause,id_no,amount;
    ImageView image;
    public TrafficHolder(View itemView) {
        super(itemView);
        image=(ImageView)itemView.findViewById(R.id.image_code);
        name=(TextView)itemView.findViewById(R.id.name);
        chit_id=(TextView)itemView.findViewById(R.id.chit_id);
        cause=(TextView)itemView.findViewById(R.id.cause);
        id_no=(TextView)itemView.findViewById(R.id.id_no);
        amount=(TextView)itemView.findViewById(R.id.amount);
    }
}
