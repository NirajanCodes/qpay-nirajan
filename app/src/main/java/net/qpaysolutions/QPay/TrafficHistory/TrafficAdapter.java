package net.qpaysolutions.QPay.TrafficHistory;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 5/26/17.
 */

public class TrafficAdapter extends RecyclerView.Adapter<TrafficHolder> {
    ArrayList<TrafficModel> trafficModels;
    TrafficHistoryFragment trafficFragment;

    public TrafficAdapter(ArrayList<TrafficModel> trafficModels, TrafficHistoryFragment trafficFragment) {
        this.trafficModels = trafficModels;
        this.trafficFragment = trafficFragment;
    }

    @Override
    public TrafficHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.traffic_row,parent,false);
        TrafficHolder trafficHolder = new TrafficHolder(view);
        return trafficHolder;
    }

    @Override
    public void onBindViewHolder(TrafficHolder holder, int position) {
        holder.cause.setText(trafficModels.get(position).getCause());
        holder.name.setText(trafficModels.get(position).getName());
        holder.chit_id.setText(trafficModels.get(position).getChit_id());
        holder.id_no.setText(trafficModels.get(position).getIdentification_id());
        holder.amount.setText(trafficModels.get(position).getAmount());
        trafficFragment.create(holder.image,trafficModels.get(position).getStan()+trafficModels.get(position).getCrrn());


    }

    @Override
    public int getItemCount() {
        return trafficModels.size();
    }
}
