package net.qpaysolutions.QPay.CustumClasses;

/**
 * Created by deadlydragger on 9/12/16.
 */
public class Regex {
    public static final String NCELL_REGEX = "98[0-2]\\d*";
    public static final String NTC_PREPAID_REGEX = "9[8,7][4,6]\\d*";
    public static final String NTC_POSTPAID_REGEX = "9{7,8}5\\d*";
    public static final String SMART_CELL = "96[0-2]\\d*";
    public static final String UTL = "972\\d*";
}