package net.qpaysolutions.QPay.CustumClasses;

/**
 * Created by deadlydragger on 6/29/18.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import net.qpaysolutions.QPay.Utils.CustomerApplication;

public class FontIcon extends androidx.appcompat.widget.AppCompatTextView {

    static Typeface face = Typeface.createFromAsset(CustomerApplication.getContext().getAssets(), "prabhu_pay.ttf");

    public FontIcon(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setRegularFont();
    }

    public FontIcon(Context context, AttributeSet attrs) {
        super(context, attrs);
        setRegularFont();
    }

    public FontIcon(Context context) {
        super(context);
        setRegularFont();
    }

    public FontIcon(Context context, boolean isBold) {
        super(context);
    }

    public void setRegularFont() {
        setTypeface(face);
    }

}
