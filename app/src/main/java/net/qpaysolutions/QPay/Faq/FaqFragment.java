package net.qpaysolutions.QPay.Faq;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 11/23/16.
 */

public class FaqFragment extends Fragment {
    RecyclerView recyclerView;
    ArrayList<Faqmodel> faqmodels = new ArrayList<>();
    String app_id, term_id;
    QPayProgressDialog qPayProgressDialog;

    FaqAdapter faqAdapter;
    TextView title;
    SharedPreferences sharedPreferences_location;
    String File = "location";
    private String lat, lng;
    QPayCustomerDatabase qpayMerchantDatabase;
    String appId, id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.faq_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      /*  ((MainActivity)getActivity()).setToolbar_text("FAQ");
        ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);
*/
        sharedPreferences_location = getActivity().getSharedPreferences(File, Context.MODE_PRIVATE);
        lat = sharedPreferences_location.getString("lat", "");
        lng = sharedPreferences_location.getString("lng", "");
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        new GetFaqList().execute();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase = new QPayCustomerDatabase(getContext());
        appId = qpayMerchantDatabase.getKeyAppId();
        id = qpayMerchantDatabase.getCustomerID();

    }

    public class GetFaqList extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            NetworkAPI httpResponse = new NetworkAPI();
            String resukt = null;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("lat", lat);
                jsonObject.put("lng", lng);
                jsonObject.put("appType", 1);
                Log.d("dinesh", "doInBackground: " + jsonObject.toString());
                resukt = httpResponse.sendHTTPData(Constants.FAQ_LIST, jsonObject);

                Log.d("dinesh", "doInBackground: " + resukt);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resukt;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject FaqCategoryMobileResult = jsonObject.getJSONObject("FaqCategoryMobileResult");
                String status = FaqCategoryMobileResult.getString("status");
                if (status.equals("00")) {
                    JSONArray FaqCategories = FaqCategoryMobileResult.getJSONArray("FaqCategories");
                    for (int i = 0; i < FaqCategories.length(); i++) {
                        Faqmodel faqmodel = new Faqmodel();
                        JSONObject jsonObject1 = FaqCategories.getJSONObject(i);
                        faqmodel.setCategory_name(jsonObject1.getString("category_name"));
                        faqmodel.setFaqcategory_id(jsonObject1.getString("faqcategory_id"));
                        faqmodel.setOrder(jsonObject1.getString("order"));
                        faqmodels.add(faqmodel);
                    }
                    faqAdapter = new FaqAdapter(getActivity(), faqmodels);
                    recyclerView.setAdapter(faqAdapter);
                    recyclerView.addOnItemTouchListener(new FaqFragment.RecyclerTouchListener(getActivity(), recyclerView, new FaqFragment.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            Intent intent = new Intent(getActivity(), FaqDetails.class);
                            intent.putExtra("id", faqmodels.get(position).getFaqcategory_id());
                            intent.putExtra("title", faqmodels.get(position).getCategory_name());
                            startActivity(intent);
                           /* FaqDetails faqDetails = new FaqDetails();
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fragment,faqDetails);
                            Bundle bundle = new Bundle();
                            bundle.putString("id",faqmodels.get(position).getFaqcategory_id());
                            faqDetails.setArguments(bundle);
                            fragmentTransaction.commit();*/

                        }

                        @Override
                        public void onLongClick(View view, int position) {
                        }
                    }));
                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private FaqFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final FaqFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                   getActivity().onBackPressed();
                    /* MainFragmentNotCall najirEnglish = new MainFragmentNotCall();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();*/
                  /*  getFragmentManager().beginTransaction()
                            .replace(R.id.fragment,najirEnglish)
                            .commit();*/
//                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;
            }
        });
    }
}
