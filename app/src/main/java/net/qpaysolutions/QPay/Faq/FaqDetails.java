package net.qpaysolutions.QPay.Faq;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 11/23/16.
 */

public class FaqDetails extends AppCompatActivity {
    TextView faq_details;
    String id;
    QPayProgressDialog qPayProgressDialog;
    RecyclerView recyclerView;
    ArrayList<Faqmodel> faqmodels = new ArrayList<>();
    FaqAdapterDetails faqDetailsAdapter;
    Toolbar toolbar;
    TextView title;
    String title_name;
    String appId, custId;
    private QPayCustomerDatabase QPayCustomerDatabase;

    /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.faq_details,container,false);
    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_details);
        QPayCustomerDatabase =new QPayCustomerDatabase(this);
        QPayCustomerDatabase.getReadableDatabase();
        appId= QPayCustomerDatabase.getKeyAppId();
        custId= QPayCustomerDatabase.getCustomerID();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText("FAQ Details");
        id = getIntent().getStringExtra("id");
        title = (TextView) findViewById(R.id.title);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        new GetFaqList().execute();
        title.setText("About " + getIntent().getStringExtra("title"));
    }

    public class GetFaqList extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {

            NetworkAPI httpResponse = new NetworkAPI();
            String resukt = null;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("appId", appId);
                jsonObject.put("id", custId);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("appType", 1);
                jsonObject.put("categoryId", id);
                Log.d("dinesh", "doInBackground: " + jsonObject.toString());
                resukt = httpResponse.sendHTTPData(Constants.FAQ_DETAILS, jsonObject);


                Log.d("dinesh", "doInBackground: " + resukt);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resukt;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {

                JSONObject jsonObject = new JSONObject(s);
                JSONObject FaqCategoryMobileResult = jsonObject.getJSONObject("FaqContentMobileResult");
                String status = FaqCategoryMobileResult.getString("status");
                if (status.equals("00")) {
                    JSONArray FaqCategories = FaqCategoryMobileResult.getJSONArray("FaqContents");
                    for (int i = 0; i < FaqCategories.length(); i++) {
                        JSONObject jsonObject1 = FaqCategories.getJSONObject(i);
                        if (jsonObject1.getString("format_type").equals("1")) {
                            Faqmodel faqmodel = new Faqmodel();
                            faqmodel.setAnswer(jsonObject1.getString("answer"));
                            faqmodel.setCredits(jsonObject1.getString("credits"));
                            faqmodel.setImage_url(jsonObject1.getString("image_url"));
                            faqmodel.setQuestion(jsonObject1.getString("question"));
                            faqmodel.setThumbnail(jsonObject1.getString("thumbnail"));
                            faqmodels.add(faqmodel);
                        }

                    }
                    faqDetailsAdapter = new FaqAdapterDetails(FaqDetails.this, faqmodels);
                    recyclerView.setAdapter(faqDetailsAdapter);

                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(FaqDetails.this);
            qPayProgressDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
