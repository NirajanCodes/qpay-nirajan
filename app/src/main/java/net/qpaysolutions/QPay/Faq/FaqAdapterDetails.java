package net.qpaysolutions.QPay.Faq;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 11/23/16.
 */

public class FaqAdapterDetails extends RecyclerView.Adapter<FaqAdapterDetails.Viewholder> {
    Context context;
    ArrayList<Faqmodel> faqmodels;

    public FaqAdapterDetails(Context context, ArrayList<Faqmodel> faqmodels) {
        this.context = context;
        this.faqmodels = faqmodels;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_line_details, parent, false);
        Viewholder viewholder = new Viewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        holder.text_question.setText(faqmodels.get(position).getQuestion());
        holder.answer.setText(faqmodels.get(position).getAnswer());

    }

    @Override
    public int getItemCount() {
        return faqmodels.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView text_question,answer;

        public Viewholder(View itemView) {
            super(itemView);
            text_question = (TextView) itemView.findViewById(R.id.question);
            answer=(TextView)itemView.findViewById(R.id.answer);
        }
    }
}
