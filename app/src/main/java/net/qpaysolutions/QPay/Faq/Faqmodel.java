package net.qpaysolutions.QPay.Faq;

/**
 * Created by deadlydragger on 11/23/16.
 */

public class Faqmodel {
    String category_name, faqcategory_id, order,answer,credits,format_type,image_url,question,thumbnail;


    public String getAnswer() {
        return answer;
    }

    public String getCredits() {
        return credits;
    }

    public String getFormat_type() {
        return format_type;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getQuestion() {
        return question;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    public void setFormat_type(String format_type) {
        this.format_type = format_type;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCategory_name() {
        return category_name;
    }

    public String getFaqcategory_id() {
        return faqcategory_id;
    }

    public String getOrder() {
        return order;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public void setFaqcategory_id(String faqcategory_id) {
        this.faqcategory_id = faqcategory_id;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}

