package net.qpaysolutions.QPay.CASHBACK;

import android.content.Context;
import android.os.AsyncTask;

import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 5/13/18.
 */

public class CashBackAPI extends AsyncTask<String,String,String>{
    DialogInterface dialogInterface;
    Context context;

    public CashBackAPI(DialogInterface dialogInterface,Context context) {
        this.dialogInterface = dialogInterface;
        this.context=context;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject object= new JSONObject();
            object.put("appId", GeneralPref.getAppId());
            object.put("id",GeneralPref.getCustId());
            object.put("lat", LatLngPref.getLat());
            object.put("lng",LatLngPref.getLng());
            Utility.isBuild(object.toString());
            return NetworkAPI.sendHTTPData(Constants.CASHBACK_LIST,object);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialogInterface.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialogInterface.hideDialog();
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("00"))
            {
                JSONArray data = jsonObject.getJSONArray("data");
                JSONObject object=data.getJSONObject(0);
                new PrefCashBack(context).setCable(object.getJSONArray("CableInternet").toString());
                new PrefCashBack(context).setENT(object.getJSONArray("Entertainment").toString());
                new PrefCashBack(context).setOTHER(object.getJSONArray("Other").toString());
                new PrefCashBack(context).setPHONE(object.getJSONArray("Phone").toString());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
