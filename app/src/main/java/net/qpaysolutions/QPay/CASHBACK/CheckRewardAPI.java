package net.qpaysolutions.QPay.CASHBACK;

import android.os.AsyncTask;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 5/16/18.
 */

public class CheckRewardAPI extends AsyncTask<String, String, String> {
    String amount, billpayCode;
    DialogInterface dialogInterface;

    public CheckRewardAPI(String amount, String billpayCode, DialogInterface dialogInterface) {
        this.amount = amount;
        this.billpayCode = billpayCode;
        this.dialogInterface = dialogInterface;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("billPayCode", billpayCode);
            jsonObject.put("amount", amount);
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            Utility.isBuild(jsonObject.toString());
            GeneralPref.setRewardAmount("0.00");
            return NetworkAPI.sendHTTPData(Constants.CHECK_REWARD,jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialogInterface.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialogInterface.hideDialog();
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            if (jsonObject.getString("status").equals("00")){
                JSONArray data = jsonObject.getJSONArray("data");
                JSONObject object= data.getJSONObject(0);
                String Amount =object.getString("Amount");
                GeneralPref.setRewardAmount(Amount);
            }
        }catch (Exception e){
            e.printStackTrace();
            GeneralPref.setRewardAmount("0.00");
        }
    }

}
