package net.qpaysolutions.QPay.CASHBACK;

/**
 * Created by deadlydragger on 5/15/18.
 */

public class CashbackModel {

    String BillPayCode,RebateAmt,RebatePer;

    public String getBillPayCode() {
        return BillPayCode;
    }

    public void setBillPayCode(String billPayCode) {
        BillPayCode = billPayCode;
    }

    public String getRebateAmt() {
        return RebateAmt;
    }

    public void setRebateAmt(String rebateAmt) {
        RebateAmt = rebateAmt;
    }

    public String getRebatePer() {
        return RebatePer;
    }

    public void setRebatePer(String rebatePer) {
        RebatePer = rebatePer;
    }
}
