package net.qpaysolutions.QPay.CASHBACK;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by deadlydragger on 5/13/18.
 */

public class PrefCashBack {
    String Discount="discount";
    String CABLE="CableInternet";
    String ENT="Entertainment";
    String PHONE="Phone";
    String OTHER="Other";
    Context context;
    SharedPreferences sharedPreferences;

   public PrefCashBack(Context context){
        this.context=context;
        sharedPreferences=context.getSharedPreferences(Discount,Context.MODE_PRIVATE);

    }
    public void setCable(String s){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString(CABLE,s);
        editor.apply();
    }
    public void setENT(String s){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString(ENT,s);
        editor.apply();
    }
    public void setPHONE(String s){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PHONE,s);
        editor.apply();

    }
    public void setOTHER(String s){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString(OTHER,s);
        editor.apply();
    }

    public String getCABLE(){
        return sharedPreferences.getString(CABLE,"");
    }

    public String getENT() {
        return sharedPreferences.getString(ENT,"");
    }

    public String getOTHER() {
        return sharedPreferences.getString(OTHER,"");
    }

    public String getPHONE() {
        return sharedPreferences.getString(PHONE,"");
    }
}
