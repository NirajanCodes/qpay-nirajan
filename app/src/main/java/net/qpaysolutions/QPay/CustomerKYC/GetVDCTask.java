package net.qpaysolutions.QPay.CustomerKYC;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 2/23/18.
 */
class GetVDCTask extends AsyncTask<String, String, String> {

    private String districtId;
    private KYCFragment kycFragment;

    public GetVDCTask(String districtId, KYCFragment kycFragment) {
        this.districtId = districtId;
        this.kycFragment = kycFragment;
    }

    @Override
    protected void onPreExecute() {
       kycFragment.showDialog();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = "";
        try {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("custId", GeneralPref.getCustId());
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("districtId", districtId);
            jsonObject.put("lat", LatLngPref.getLng());
            jsonObject.put("lng", LatLngPref.getLng());
            Log.d("dinesh", jsonObject.toString());
            response = httpUrlConnectionPost.sendHTTPData(Constants.GET_VDC_MUNICIPALITY, jsonObject);
            return response;
        } catch (JSONException je) {

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        kycFragment.hideDialog();
        try {
            ArrayList<KYCModel> arrayList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(s).getJSONObject("GetVdcMunicipalityResult");
            if (jsonObject.getBoolean("success")) {
                KYCModel kycModel1= new KYCModel();
                kycModel1.setName("Select Gaupalika/Municipality");
                kycModel1.setId("0");
                arrayList.add(kycModel1);
                JSONArray jDataArr = jsonObject.getJSONArray("data");
                for (int i = 0; i < jDataArr.length(); i++) {
                    KYCModel kycModel= new KYCModel();
                    JSONObject jData = jDataArr.getJSONObject(i);
                    kycModel.setId(jData.getString("Id"));
                    kycModel.setName(jData.getString("VdcMunicipality").replace("Municipality", ""));
                    arrayList.add(kycModel);
                }
                kycFragment.getVDC(arrayList);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}