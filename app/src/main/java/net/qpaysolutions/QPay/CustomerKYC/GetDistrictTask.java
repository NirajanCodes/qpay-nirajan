package net.qpaysolutions.QPay.CustomerKYC;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 2/22/18.
 */
class GetDistrictTask extends AsyncTask<String, String, String> {

    private String zoneId;


    private KYCFragment context;

    public GetDistrictTask( KYCFragment context,String zoneId) {
        this.context = context;
        this.zoneId=zoneId;
    }

    @Override
    protected void onPreExecute() {
       context.showDialog();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = "";
        try {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("stateId", zoneId);
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            Utility.isBuild(jsonObject.toString());
            response = httpUrlConnectionPost.sendHTTPData(Constants.GET_DISTRICT, jsonObject);
            return response;
        } catch (JSONException je) {

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        context.hideDialog();
            try {
                Utility.isBuild(s);
                JSONObject jsonObject = new JSONObject(s)/*.getJSONObject("GetDistrictResult")*/;
                if (jsonObject.getBoolean("success")) {
                    ArrayList<KYCModel> arrayList = new ArrayList<>();
                    KYCModel kycModel1= new KYCModel();
                    kycModel1.setName("Select District");
                    kycModel1.setId("0");
                    arrayList.add(kycModel1);
                    try {
                        JSONArray jDataArr = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jDataArr.length(); i++) {
                            KYCModel kycModel = new KYCModel();
                            JSONObject jData = jDataArr.getJSONObject(i);
                            kycModel.setId(jData.getString("Id"));
                            kycModel.setName(jData.getString("District"));
                            arrayList.add(kycModel);
                        }
                        context.getDistrict(arrayList);

                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }
            } catch (Exception je) {

            }

    }
}