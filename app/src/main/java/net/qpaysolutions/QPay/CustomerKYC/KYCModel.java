package net.qpaysolutions.QPay.CustomerKYC;

/**
 * Created by deadlydragger on 2/22/18.
 */

public class KYCModel {
    String name,id;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
