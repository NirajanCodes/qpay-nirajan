package net.qpaysolutions.QPay.CustomerKYC;

/**
 * Created by deadlydragger on 2/23/18.
 */

public interface Imageurl {
    void getUrlFront(String s);
    void getUrlBack(String s);
    void getUrlPhoto(String s);
    void setNullFront();
    void setNullBack();
    void setNullPhoto();

}
