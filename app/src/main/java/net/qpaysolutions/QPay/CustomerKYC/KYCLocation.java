package net.qpaysolutions.QPay.CustomerKYC;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 2/22/18.
 */

public interface KYCLocation {
    void getDistrict(ArrayList<KYCModel> kycLocations);
    void getZone(ArrayList<KYCModel> kycLocations);
    void getVDC(ArrayList<KYCModel> kycLocations);
    void getDistrictAll(ArrayList<KYCModel> kycModels);
    void getDetails(KYCDetailsModel kycDetailsModels);
}
