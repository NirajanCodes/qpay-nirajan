package net.qpaysolutions.QPay.CustomerKYC;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.CustomerKYC.KYCDetailsModel;
import net.qpaysolutions.QPay.CustomerKYC.KYCFragment;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 3/9/18.
 */

public class KycDetailAPI extends AsyncTask<String,String,String>{
    KYCFragment kycFragment;

    public KycDetailAPI(KYCFragment kycFragment) {
        this.kycFragment = kycFragment;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject= new JSONObject();
            jsonObject.put("custId", GeneralPref.getCustId());
            jsonObject.put("appId",GeneralPref.getAppId());
            jsonObject.put("lat",LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            return NetworkAPI.sendHTTPData(Constants.DETAILS_KYC,jsonObject);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        kycFragment.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        kycFragment.hideDialog();
        try {
            Log.d("dinesh", "onPostExecute: " + s);
            JSONObject jsonObject = new JSONObject(s);
            JSONArray data = jsonObject.getJSONArray("data");
            JSONObject object = data.getJSONObject(0);
            String CustomerName = object.getString("CustomerName");
            String DateOfBirth = object.getString("DateOfBirth");
            String DateOfBirthType = object.getString("DateOfBirthType");
            String District=object.getString("District");
            String DistrictId = object.getString("DistrictId");
            String FatherName = object.getString("FatherName");
            String Gender = object.getString("Gender");
            String GrandFatherName = object.getString("GrandFatherName");
            String IdBackUrl = object.getString("IdBackUrl");
            String IdFrontUrl = object.getString("IdFrontUrl");
            String IdIssuedDate = object.getString("IdIssuedDate");
            String IdIssuedDateType = object.getString("IdIssuedDateType");
            String IdIssuedFrom = object.getString("IdIssuedFrom");
            String Identification = object.getString("Identification");
            String KycStatus=object.getString("KycStatus");
            String MotherName = object.getString("MotherName");
            String Occupation = object.getString("Occupation");
            String PhotoUrl = object.getString("PhotoUrl");
            String State=object.getString("State");
            String Tole = object.getString("WardNo");
            String TypeOfId = object.getString("TypeOfId");
            String VdcMunicipality=object.getString("VdcMunicipality");
            String VdcMunicipalityId = object.getString("VdcMunicipalityId");

            String EmailId = object.getString("EmailId");
            String Address = object.getString("Address");

            GeneralPref.setKYC(KycStatus);
            //        private EditText fullName, dob, fatherName, motherName, grandFatherName, tole, idNo, issueDate;

            KYCDetailsModel kycDetailsModel = new KYCDetailsModel();
            kycDetailsModel.setCustomerName(CustomerName);
            kycDetailsModel.setDateOfBirthType(DateOfBirthType);
            kycDetailsModel.setFatherName(FatherName);
            kycDetailsModel.setMotherName(MotherName);
            kycDetailsModel.setGrandFatherName(GrandFatherName);
            kycDetailsModel.setTole(Tole);
            kycDetailsModel.setIdentification(Identification);
            kycDetailsModel.setIdIssuedDate(IdIssuedDate);
            kycDetailsModel.setIdIssuedDateType(IdIssuedDateType);
            kycDetailsModel.setDateOfBirth(DateOfBirth);
            kycDetailsModel.setGender(Gender);
            kycDetailsModel.setPhotoUrl(PhotoUrl);
            kycDetailsModel.setIdFrontUrl(IdFrontUrl);
            kycDetailsModel.setIdBackUrl(IdBackUrl);

            kycDetailsModel.setEmailId(EmailId);
            kycDetailsModel.setAddress(Address);

            kycDetailsModel.setKycStatus(KycStatus);
            kycDetailsModel.setDistrict(District);
            kycDetailsModel.setVdcMunicipality(VdcMunicipality);
            kycDetailsModel.setState(State);
            GeneralPref.setName(object.getString("CustomerName"));
            kycFragment.getDetails(kycDetailsModel);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
