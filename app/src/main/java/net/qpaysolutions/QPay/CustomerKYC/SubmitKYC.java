package net.qpaysolutions.QPay.CustomerKYC;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 2/25/18.
 */

public class SubmitKYC extends AsyncTask<String,String,String> {
    KYCFragment kycFragment;
    KYCDetailsModel kycDetailsModel;

    public SubmitKYC(KYCFragment kycFragment,KYCDetailsModel kycDetailsModel) {
        this.kycFragment = kycFragment;
        this.kycDetailsModel=kycDetailsModel;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("custId", GeneralPref.getCustId());
            jsonObject.put("appId",GeneralPref.getAppId());
            jsonObject.put("customerName",kycDetailsModel.getCustomerName());
            jsonObject.put("typeOfId",kycDetailsModel.getTypeOfId());
            jsonObject.put("dateOfBirth",kycDetailsModel.getDateOfBirth());
            jsonObject.put("gender",kycDetailsModel.getGender());
            jsonObject.put("occupation",kycDetailsModel.getOccupation());
            jsonObject.put("fatherName",kycDetailsModel.getFatherName());
            jsonObject.put("motherName",kycDetailsModel.getMotherName());
            jsonObject.put("grandFatherName",kycDetailsModel.getGrandFatherName());
            jsonObject.put("districtId",kycDetailsModel.getDistrictId());
            jsonObject.put("vdcMunicipalityId",kycDetailsModel.getVdcMunicipalityId());
            jsonObject.put("wardNo",kycDetailsModel.getTole());
            jsonObject.put("identification",kycDetailsModel.getIdentification());
            jsonObject.put("idIssuedFrom",kycDetailsModel.getIdIssuedFrom());
            jsonObject.put("idIssuedDate",kycDetailsModel.getIdIssuedDate());
            jsonObject.put("imgIdFront",kycDetailsModel.getIdFrontUrl());
            jsonObject.put("imgIdBack",kycDetailsModel.getIdBackUrl());
            jsonObject.put("imgPhoto",kycDetailsModel.getPhotoUrl());
            jsonObject.put("dobType",kycDetailsModel.getDateOfBirthType());
            jsonObject.put("idIssuedDateType",kycDetailsModel.getIdIssuedDateType());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            jsonObject.put("tole","");
            jsonObject.put("emailId",kycDetailsModel.getEmailId());
            jsonObject.put("stateId",kycDetailsModel.getStateId());
//            jsonObject.put("WardNo",kycDetailsModel.getWardNo());
            jsonObject.put("address",kycDetailsModel.getAddress());

            Log.d("dinesh",jsonObject.toString());
            return NetworkAPI.sendHTTPDataKYC(Constants.POST_KYC,jsonObject);


        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        kycFragment.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        kycFragment.hideDialog();
        try {
            Log.d("dinesh", "onPostExecute: "+s);
            JSONObject jsonObject = new JSONObject(s);
            JSONObject PostKycInfoResult= jsonObject.getJSONObject("PostKycInfoNewResult");
            boolean success = PostKycInfoResult.getBoolean("success");
            if (success){
                String message = PostKycInfoResult.getString("message");
                if (PostKycInfoResult.getString("status").equals("00")){
                    GeneralPref.setKYC("I");
                    SharedPreferences sharePreference = CustomerApplication.getContext().getSharedPreferences("Mybalance", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharePreference.edit();
                    editor.putString("kyc_status", "I");
                    editor.apply();
                    kycFragment.custumdialogSucess("KYC Successfully Submitted!", message);
                }
                else {
                    kycFragment.custumdialogfailure("KYC Submission Failed!", message);
                }
            }
            else {
                kycFragment.custumdialogfailure("KYC Submission Failed!", "Failed to submit your KYC information for QPay. Please try again.");
            }
        }catch (Exception e){
            e.printStackTrace();
            kycFragment.custumdialogfailure("KYC Submission Failed!", "You lost your internet connection while processing your transaction. Please try again later");

        }
    }
}
