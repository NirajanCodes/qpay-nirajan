package net.qpaysolutions.QPay.CustomerKYC;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Adapter;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by deadlydragger on 2/22/18.
 */
public class KYCFragment extends Fragment implements KYCLocation, View.OnClickListener, DialogInterface, Imageurl {

    private QPayProgressDialog qPayProgressDialog;
    private KYCLocation kycLocation;
    private String zoneId, vdcId, districtId, idTypeDob, idTypeId, urlFront, urlBack, urlPhoto, gender, occupation, issueFromId, typeOfId;
    public ImageView idFront, idBack, idPhoto;
    private float mDensity;
    private EditText firstName, lastName, fullName, dob, fatherName, motherName, grandFatherName, tole, idNo, issueDate, emailId, address;
    private TextInputLayout dobLayout, fatherNameLayout, montherNameLayout, grandfatherNameLayout, toleLayout, idNoLayout, issueDateLayout, imageFontLayout, imageBackLayout, imagePhotoLayout, zoneLayout, districtLayout, gaupalikaLayout, issuefromLayout, emailIdLayout, addressLayout, inputLayoutfirstName, inputLayoutlastName;
    private Button submitKYC;
    private String current = "";
    private String ddmmyyyy = "DDMMYYYY";
    private Calendar cal;
    private Spinner dateType, dateTypeId, spinnerGender;
    String EmailId, Address;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mDensity = getActivity().getResources().getDisplayMetrics().density;
        qPayProgressDialog = new QPayProgressDialog(getContext());
        submitKYC = (Button) view.findViewById(R.id.submitKYC);
        submitKYC.setOnClickListener(this);
        cal = Calendar.getInstance();
        setIdType();
        setGender();
        setOccupation();
        setDateTypeDob();
        pickFrontId();
        new GetAllZoneTask(KYCFragment.this).execute();
        new GetAllDistrictTask(KYCFragment.this).execute();
        if (GeneralPref.getKYC().equals("R")) {
            new KycDetailAPI(KYCFragment.this).execute();
        }
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
            if (hasCameraPermission(getContext(), PERMISSIONS)) {
                ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, Tags.KYC_EXTERNAL_STORAGE);
            }
        }
        setEditex(view);
        setLayout(view);
        cleanEditext();

//        if (!GeneralPref.getKYC().equals("NA") || !GeneralPref.getKYC().equals("R")) {
//            setData();
//        }
    }

    public boolean hasCameraPermission(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void setLayout(View view) {
//        fullNameLayout = (TextInputLayout) view.findViewById(R.id.inputLayoutName);
        dobLayout = (TextInputLayout) view.findViewById(R.id.dobLayout);
        fatherNameLayout = (TextInputLayout) view.findViewById(R.id.fatherLayout);
        montherNameLayout = (TextInputLayout) view.findViewById(R.id.motherLayout);
        grandfatherNameLayout = (TextInputLayout) view.findViewById(R.id.grandFatherLayout);
        toleLayout = (TextInputLayout) view.findViewById(R.id.toleLayout);
        idNoLayout = (TextInputLayout) view.findViewById(R.id.idNOLayout);
        issueDateLayout = (TextInputLayout) view.findViewById(R.id.idIssueDateLayout);
        imageFontLayout = (TextInputLayout) view.findViewById(R.id.imageFontLayout);
        imageBackLayout = (TextInputLayout) view.findViewById(R.id.imageBackLayout);
        imagePhotoLayout = (TextInputLayout) view.findViewById(R.id.imageIdLayout);
        zoneLayout = (TextInputLayout) view.findViewById(R.id.zoneLayout);
        districtLayout = (TextInputLayout) view.findViewById(R.id.districtLayout);
        gaupalikaLayout = (TextInputLayout) view.findViewById(R.id.gaupalikaLayout);
        issuefromLayout = (TextInputLayout) view.findViewById(R.id.issuefromLayout);
        emailIdLayout = (TextInputLayout) view.findViewById(R.id.emailIdLayout);
        addressLayout = (TextInputLayout) view.findViewById(R.id.addressLayout);
        inputLayoutfirstName = (TextInputLayout) view.findViewById(R.id.inputLayoutfirstName);
        inputLayoutlastName = (TextInputLayout) view.findViewById(R.id.inputLayoutlastName);


    }

    private void setEditex(View view) {
//        fullName = (EditText) view.findViewById(R.id.fullName);
        firstName = (EditText) view.findViewById(R.id.firstName);
        lastName = (EditText) view.findViewById(R.id.lastName);
        dob = (EditText) view.findViewById(R.id.dob);
        fatherName = (EditText) view.findViewById(R.id.fatherName);
        motherName = (EditText) view.findViewById(R.id.motherName);
        grandFatherName = (EditText) view.findViewById(R.id.grandFatherName);
        tole = (EditText) view.findViewById(R.id.tole);
        idNo = ((EditText) view.findViewById(R.id.identification));
        issueDate = (EditText) view.findViewById(R.id.idIssueDate);
        emailId = (EditText) view.findViewById(R.id.emailId);
        address = (EditText) view.findViewById(R.id.address);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.kyc_main, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setIdType() {
        final ArrayList<String> id = new ArrayList<>();
        id.add("Citizenship");
        id.add("Passport");
        id.add("Licence");
//        id.add("Other");
        Spinner spinner = (Spinner) getView().findViewById(R.id.idType);
        Adapter dapterFlight = new Adapter(getContext(), id);
        spinner.setAdapter(dapterFlight);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeOfId = id.get(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setGender() {
        final ArrayList<String> id = new ArrayList<>();
        id.add("Male");
        id.add("Female");
        id.add("Others");
        spinnerGender = (Spinner) getView().findViewById(R.id.gender);
        Adapter dapterFlight = new Adapter(getContext(), id);
        spinnerGender.setAdapter(dapterFlight);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender = id.get(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setOccupation() {
        final ArrayList<String> id = new ArrayList<>();

        id.add("Service");
        id.add("Private Sector");
        id.add("Public Sector");
        id.add("Government Sector");
        id.add("Professional");
        id.add("Self Employed");
        id.add("Retired");
        id.add("Housewife");
        id.add("Student");
        id.add("Business");
        id.add("Others");


        Spinner spinner = (Spinner) getView().findViewById(R.id.occupation);
        Adapter adapter = new Adapter(getContext(), id);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                occupation = id.get(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setZone(final ArrayList<KYCModel> arrayList) {
        Spinner spinner = (Spinner) getView().findViewById(R.id.zone);
        KYCSpineerAdapter dapterFlight = new KYCSpineerAdapter(getContext(), arrayList);
        spinner.setAdapter(dapterFlight);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                zoneLayout.setError(null);
                zoneId = arrayList.get(i).getId();
                if (!zoneId.equals("0")) {
                    new GetDistrictTask(KYCFragment.this, zoneId).execute();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setDistrict(final ArrayList<KYCModel> district) {
        Spinner spinner = (Spinner) getView().findViewById(R.id.district);
        KYCSpineerAdapter dapterFlight = new KYCSpineerAdapter(getContext(), district);
        spinner.setAdapter(dapterFlight);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                districtLayout.setError(null);
                districtId = district.get(i).getId();
                if (!districtId.equals("0")) {
                    new GetVDCTask(districtId, KYCFragment.this).execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setVDC(final ArrayList<KYCModel> vdc) {
        Spinner spinner = (Spinner) getView().findViewById(R.id.vdcMunicipalityId);
        KYCSpineerAdapter dapterFlight = new KYCSpineerAdapter(getContext(), vdc);
        spinner.setAdapter(dapterFlight);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gaupalikaLayout.setError(null);
                vdcId = vdc.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setIssueFrom(final ArrayList<KYCModel> issueFrom) {
        Spinner spinner = (Spinner) getView().findViewById(R.id.issueFrom);
        KYCSpineerAdapter kycSpineerAdapter = new KYCSpineerAdapter(getContext(), issueFrom);
        spinner.setAdapter(kycSpineerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                issueFromId = issueFrom.get(i).getId();
                issuefromLayout.setError(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setDateTypeDob() {
        final ArrayList<KYCModel> kycModels = new ArrayList<>();
        KYCModel kycModel = new KYCModel();
        kycModel.setId("1");
        kycModel.setName("BS");
        kycModels.add(kycModel);
        KYCModel kycModel1 = new KYCModel();
        kycModel1.setId("1");
        kycModel1.setName("AD");
        kycModels.add(kycModel1);
        KYCSpineerAdapter kycSpineerAdapter = new KYCSpineerAdapter(getContext(), kycModels);
        dateType = getView().findViewById(R.id.dateTypeDob);
        dateTypeId = getView().findViewById(R.id.dateTypeId);
        dateTypeId.setAdapter(kycSpineerAdapter);
        dateType.setAdapter(kycSpineerAdapter);
        dateType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idTypeDob = kycModels.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dateTypeId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idTypeId = kycModels.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void getDistrict(ArrayList<KYCModel> kycLocations) {
        setDistrict(kycLocations);

    }

    @Override
    public void getZone(ArrayList<KYCModel> kycLocations) {
        setZone(kycLocations);

    }

    @Override
    public void getVDC(ArrayList<KYCModel> kycLocations) {
        setVDC(kycLocations);

    }

    @Override
    public void getDistrictAll(ArrayList<KYCModel> kycModels) {
        setIssueFrom(kycModels);
    }

    @Override
    public void getDetails(KYCDetailsModel kycDetailsModels) {
        try {
            String name = GeneralPref.getName();
            String[] splited = name.split("\\s+");
            firstName.setText(splited[0]);
            lastName.setText(splited[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dob.setText(kycDetailsModels.getDateOfBirth());
        fatherName.setText(kycDetailsModels.getFatherName());
        motherName.setText(kycDetailsModels.getMotherName());
        grandFatherName.setText(kycDetailsModels.getGrandFatherName());
        tole.setText(kycDetailsModels.getTole());
        idNo.setText(kycDetailsModels.getIdentification());
        issueDate.setText(kycDetailsModels.getIdIssuedDate());

        emailId.setText(kycDetailsModels.getEmailId());
        address.setText(kycDetailsModels.getAddress());
        if (kycDetailsModels.getDateOfBirthType().equals("AD")) {
            dateType.setSelection(1);
        }
        if (kycDetailsModels.getIdIssuedDateType().equals("AD")) {
            dateType.setSelection(1);
        }

        if (kycDetailsModels.getGender().equals("Female")) {
            spinnerGender.setSelection(1);
        }
        if (kycDetailsModels.getGender().equals("Others")) {
            spinnerGender.setSelection(2);
        }
        Picasso.get()
                .load(kycDetailsModels.getIdFrontUrl()).placeholder(R.drawable.user_photo_placeholder).error(R.drawable.user_photo_placeholder).into(idFront);
        Picasso.get()
                .load(kycDetailsModels.getIdBackUrl()).placeholder(R.drawable.user_photo_placeholder).error(R.drawable.user_photo_placeholder).into(idBack);
        Picasso.get()
                .load(kycDetailsModels.getPhotoUrl()).placeholder(R.drawable.user_photo_placeholder).error(R.drawable.user_photo_placeholder).into(idPhoto);
        urlFront = kycDetailsModels.getIdFrontUrl();
        urlBack = kycDetailsModels.getIdBackUrl();
        urlPhoto = kycDetailsModels.getPhotoUrl();

    }


    private void pickFrontId() {
        idFront = getView().findViewById(R.id.idFront);
        idBack = getView().findViewById(R.id.idBack);
        idPhoto = getView().findViewById(R.id.imageUser);
        idPhoto.setOnClickListener(this);
        idBack.setOnClickListener(this);
        idFront.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.idFront:
//                urlFront = null;
                imageFontLayout.setError(null);
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), KYCConstant.PICK_IMAGE_FRONT_ID);
                break;

            case R.id.imageUser:
//                urlPhoto = null;
                imagePhotoLayout.setError(null);
                Intent intent1 = new Intent();
                intent1.setType("image/*");
                intent1.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent1, "Select Picture"), KYCConstant.PICK_IMAGE_ID);
                break;

            case R.id.idBack:
//                urlBack = null;
                imageBackLayout.setError(null);
                Intent intent2 = new Intent();
                intent2.setType("image/*");
                intent2.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent2, "Select Picture"), KYCConstant.PICK_IMAGE_BACK_ID);
                break;

            case R.id.submitKYC:
                try {
                    if (checkError()) {
                        if (isValidUrl()) {
                            if (checkAddress()) {
                                KYCDetailsModel kycDetailsModel = new KYCDetailsModel();
                                kycDetailsModel.setCustomerName(firstName.getText().toString() + " " + lastName.getText().toString());
                                kycDetailsModel.setDateOfBirth(dob.getText().toString());
                                kycDetailsModel.setDateOfBirthType(idTypeDob);
                                kycDetailsModel.setDistrictId(districtId);
                                kycDetailsModel.setFatherName(fatherName.getText().toString());
                                kycDetailsModel.setGender(gender);
                                kycDetailsModel.setGrandFatherName(grandFatherName.getText().toString());
                                kycDetailsModel.setIdBackUrl(urlBack);
                                kycDetailsModel.setIdFrontUrl(urlFront);
                                kycDetailsModel.setPhotoUrl(urlPhoto);
                                kycDetailsModel.setIdIssuedDate(issueDate.getText().toString());
                                kycDetailsModel.setIdIssuedDateType(idTypeId);
                                kycDetailsModel.setIdIssuedFrom(issueFromId);
                                kycDetailsModel.setIdentification(idNo.getText().toString());
                                kycDetailsModel.setMotherName(motherName.getText().toString());
                                kycDetailsModel.setOccupation(occupation);
                                kycDetailsModel.setTole(tole.getText().toString());
                                kycDetailsModel.setTypeOfId(typeOfId);
                                kycDetailsModel.setVdcMunicipalityId(vdcId);
                                kycDetailsModel.setEmailId(emailId.getText().toString());
                                kycDetailsModel.setAddress(address.getText().toString());
//                                kycDetailsModel.setAddress(Address);
                                kycDetailsModel.setStateId(zoneId);
                                new SubmitKYC(KYCFragment.this, kycDetailsModel).execute();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == KYCConstant.PICK_IMAGE_FRONT_ID) {

            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                if (bitmap != null) {

                    idFront.setImageBitmap(Utility.changeBitmapToThumbnail(getActivity(), bitmap, (int) (64 * mDensity)));

                    new SubmitImage(Utility.bitmapToBase64(bitmap), KYCFragment.this, "front").execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == KYCConstant.PICK_IMAGE_BACK_ID) {


            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                if (bitmap != null) {

                    idBack.setImageBitmap(Utility.changeBitmapToThumbnail(getActivity(), bitmap, (int) (64 * mDensity)));
                    new SubmitImage(Utility.bitmapToBase64(bitmap), KYCFragment.this, "back").execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == KYCConstant.PICK_IMAGE_ID) {


            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                if (bitmap != null) {

                    idPhoto.setImageBitmap(Utility.changeBitmapToThumbnail(getActivity(), bitmap, (int) (64 * mDensity)));
                    new SubmitImage(Utility.bitmapToBase64(bitmap), KYCFragment.this, "photo").execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showDialog() {
        if (!qPayProgressDialog.isShowing()) {
            qPayProgressDialog.show();
        }

    }

    @Override
    public void hideDialog() {
        if (qPayProgressDialog != null) {
            qPayProgressDialog.dismiss();
            qPayProgressDialog.hide();
        }
    }

    @Override
    public void getUrlFront(String s) {
        urlFront = s;

    }

    @Override
    public void getUrlBack(String s) {
        urlBack = s;

    }

    @Override
    public void getUrlPhoto(String s) {
        urlPhoto = s;

    }

    @Override
    public void setNullFront() {
        idFront.setImageDrawable(getResources().getDrawable(R.drawable.user_photo_placeholder));
    }

    @Override
    public void setNullBack() {
        idBack.setImageDrawable(getResources().getDrawable(R.drawable.user_photo_placeholder));

    }

    @Override
    public void setNullPhoto() {
        idPhoto.setImageDrawable(getResources().getDrawable(R.drawable.user_photo_placeholder));
    }

    private boolean checkError() {

//        if (fullName.getText().toString() != null && !fullName.getText().toString().isEmpty()) {
//
//        } else {
//            fullNameLayout.setError("Enter Full Name");
//            fullName.requestFocus();
//            return false;
//        }
        if (firstName.getText().toString() != null && !firstName.getText().toString().isEmpty()) {

        } else {
            inputLayoutfirstName.setError("Enter First Name");
            firstName.requestFocus();
            return false;
        }
        if (lastName.getText().toString() != null && !lastName.getText().toString().isEmpty()) {

        } else {
            inputLayoutlastName.setError("Enter Last Name");
            lastName.requestFocus();
            return false;
        }
        if (!dob.getText().toString().equals("DD/MM/YYYY") && dob.getText().toString() != null && !dob.getText().toString().isEmpty() && !dob.getText().toString().contains("D") && !dob.getText().toString().contains("M") && !dob.getText().toString().contains("Y")) {
        } else {
            dob.requestFocus();
            dobLayout.setError("Enter Date of Birth");
            return false;
        }
        if (fatherName.getText().toString() != null && !fatherName.getText().toString().isEmpty()) {

        } else {
            fatherName.requestFocus();
            fatherNameLayout.setError("Enter Father Name");
            return false;
        }

        if (motherName.getText().toString() != null && !motherName.getText().toString().isEmpty()) {

        } else {
            motherName.requestFocus();
            montherNameLayout.setError("Enter Mother Name");
            return false;
        }

        if (grandFatherName.getText().toString() != null && !grandFatherName.getText().toString().isEmpty()) {

        } else {
            grandFatherName.requestFocus();
            grandfatherNameLayout.setError("Enter GrandFather Name");
            return false;
        }

        if (tole.getText().toString() != null && !tole.getText().toString().isEmpty()) {

        } else {
            tole.requestFocus();
            toleLayout.setError("Enter Ward No");
            return false;
        }

        if (idNo.getText().toString() != null && !idNo.getText().toString().isEmpty()) {

        } else {
            idNo.requestFocus();
            idNoLayout.setError("Enter Valid Id No");
            return false;
        }

        if (!issueDate.getText().toString().equals("DD/MM/YYYY") && issueDate.getText().toString() != null && !issueDate.getText().toString().isEmpty() && !issueDate.getText().toString().contains("D") && !issueDate.getText().toString().contains("M") && !issueDate.getText().toString().contains("Y")) {

        } else {
            issueDate.requestFocus();
            issueDateLayout.setError("Enter Issue Date");
            return false;
        }

        if (address.getText().toString() != null && !address.getText().toString().isEmpty()) {

        } else {
            address.requestFocus();
            addressLayout.setError("Enter Address");
            return false;
        }


        if (emailId.getText().toString() != null && !emailId.getText().toString().isEmpty()) {

            if (isValidEmail(emailId.getText().toString())) {

            } else {
                emailId.requestFocus();
                emailIdLayout.setError("Enter Valid Email");
                return false;
            }

        }/* else {
            emailId.requestFocus();
            emailIdLayout.setError("Enter Email");
            return false;
        }*/
        return true;
    }


    private void cleanEditext() {

        firstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutlastName.setError(null);
            }
        });
        lastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutlastName.setError(null);
            }
        });

        dob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals(current)) {
                    return;
                }

                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8) {
                    clean = clean + ddmmyyyy.substring(clean.length());
                } else {
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day = Integer.parseInt(clean.substring(0, 2));
                    int mon = Integer.parseInt(clean.substring(2, 4));
                    int year = Integer.parseInt(clean.substring(4, 8));

                    mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                    cal.set(Calendar.MONTH, mon - 1);
                    year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                    cal.set(Calendar.YEAR, year);


                    day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                    clean = String.format("%02d%02d%02d", day, mon, year);
                }

                clean = String.format("%s/%s/%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                dob.setText(current);
                dob.setSelection(sel < current.length() ? sel : current.length());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                dobLayout.setError(null);

            }
        });

        fatherName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                fatherNameLayout.setError(null);
            }
        });
        motherName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                montherNameLayout.setError(null);
            }
        });
        grandFatherName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                grandfatherNameLayout.setError(null);
            }
        });
        tole.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                toleLayout.setError(null);
            }
        });
        idNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                idNoLayout.setError(null);
            }
        });
        issueDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int j, int i1, int i2) {
                if (s.toString().equals(current)) {
                    return;
                }

                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8) {
                    clean = clean + ddmmyyyy.substring(clean.length());
                } else {
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day = Integer.parseInt(clean.substring(0, 2));
                    int mon = Integer.parseInt(clean.substring(2, 4));
                    int year = Integer.parseInt(clean.substring(4, 8));

                    mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                    cal.set(Calendar.MONTH, mon - 1);
                    year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                    day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                    clean = String.format("%02d%02d%02d", day, mon, year);
                }

                clean = String.format("%s/%s/%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                issueDate.setText(current);
                issueDate.setSelection(sel < current.length() ? sel : current.length());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                issueDateLayout.setError(null);
            }
        });

        emailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                emailIdLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                addressLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private boolean isValidUrl() {
        if (urlFront != null) {
        } else {
            imageFontLayout.setError("Upload Front  Image");
            return false;
        }

        if (urlBack != null) {
        } else {
            imageBackLayout.setError("Upload Back Image");
            return false;
        }

        if (urlPhoto != null) {
        } else {
            imagePhotoLayout.setError("Upload PP Size Image");
            return false;
        }
        return true;

    }

    private boolean checkAddress() {
        if (!zoneId.equals("0")) {

        } else {
            zoneLayout.setError("Select State");
            return false;
        }

        if (!districtId.equals("0")) {

        } else {
            districtLayout.setError("Select District");
            return false;
        }

        if (!vdcId.equals("0")) {

        } else {
            gaupalikaLayout.setError("Select Gaupalika/Municipality");
            return false;
        }
        if (!issueFromId.equals("0")) {

        } else {
            issuefromLayout.setError("Select District");
            return false;
        }
        return true;
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getActivity().getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), MainActivity.class));

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
