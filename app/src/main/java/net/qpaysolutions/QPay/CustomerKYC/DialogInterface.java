package net.qpaysolutions.QPay.CustomerKYC;

/**
 * Created by deadlydragger on 2/23/18.
 */

public interface DialogInterface {
    void showDialog();
    void hideDialog();
}
