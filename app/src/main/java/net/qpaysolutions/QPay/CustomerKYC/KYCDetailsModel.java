package net.qpaysolutions.QPay.CustomerKYC;

/**
 * Created by deadlydragger on 2/25/18.
 */

public class KYCDetailsModel {
    String CustomerName, DateOfBirth, DateOfBirthType, DistrictId, FatherName, Gender, GrandFatherName, IdBackUrl, IdFrontUrl, IdIssuedDate, IdIssuedDateType, IdIssuedFrom, Identification, MotherName, Occupation, PhotoUrl, Tole, TypeOfId, VdcMunicipalityId;

    String EmailId, StateId, WardNo, Address,KycStatus,District,VdcMunicipality,State;

    public void setKycStatus(String kycStatus) {
        KycStatus = kycStatus;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public void setVdcMunicipality(String vdcMunicipality) {
        VdcMunicipality = vdcMunicipality;
    }

    public void setState(String state) {
        State = state;
    }

    public String getKycStatus() {

        return KycStatus;
    }

    public String getDistrict() {
        return District;
    }

    public String getVdcMunicipality() {
        return VdcMunicipality;
    }

    public String getState() {
        return State;
    }

    public String getAddress() {
        return Address;
    }

    public String getEmailId() {
        return EmailId;
    }

    public String getStateId() {
        return StateId;
    }

    public String getWardNo() {
        return WardNo;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }

    public void setWardNo(String wardNo) {
        WardNo = wardNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getDateOfBirthType() {
        return DateOfBirthType;
    }

    public void setDateOfBirthType(String dateOfBirthType) {
        DateOfBirthType = dateOfBirthType;
    }

    public String getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(String districtId) {
        DistrictId = districtId;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getGrandFatherName() {
        return GrandFatherName;
    }

    public void setGrandFatherName(String grandFatherName) {
        GrandFatherName = grandFatherName;
    }

    public String getIdBackUrl() {
        return IdBackUrl;
    }

    public void setIdBackUrl(String idBackUrl) {
        IdBackUrl = idBackUrl;
    }

    public String getIdFrontUrl() {
        return IdFrontUrl;
    }

    public void setIdFrontUrl(String idFrontUrl) {
        IdFrontUrl = idFrontUrl;
    }

    public String getIdIssuedDate() {
        return IdIssuedDate;
    }

    public void setIdIssuedDate(String idIssuedDate) {
        IdIssuedDate = idIssuedDate;
    }

    public String getIdIssuedDateType() {
        return IdIssuedDateType;
    }

    public void setIdIssuedDateType(String idIssuedDateType) {
        IdIssuedDateType = idIssuedDateType;
    }

    public String getIdIssuedFrom() {
        return IdIssuedFrom;
    }

    public void setIdIssuedFrom(String idIssuedFrom) {
        IdIssuedFrom = idIssuedFrom;
    }

    public String getIdentification() {
        return Identification;
    }

    public void setIdentification(String identification) {
        Identification = identification;
    }

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public String getTole() {
        return Tole;
    }

    public void setTole(String tole) {
        Tole = tole;
    }

    public String getTypeOfId() {
        return TypeOfId;
    }

    public void setTypeOfId(String typeOfId) {
        TypeOfId = typeOfId;
    }

    public String getVdcMunicipalityId() {
        return VdcMunicipalityId;
    }

    public void setVdcMunicipalityId(String vdcMunicipalityId) {
        VdcMunicipalityId = vdcMunicipalityId;
    }

}
