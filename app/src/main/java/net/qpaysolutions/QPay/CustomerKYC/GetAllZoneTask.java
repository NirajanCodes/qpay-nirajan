package net.qpaysolutions.QPay.CustomerKYC;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 2/22/18.
 */

// <editor-fold desc="CLASS: GetAllZoneTask">
class GetAllZoneTask extends AsyncTask<String, String, String> {



    private KYCFragment context;

    public GetAllZoneTask(KYCFragment context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
       context.showDialog();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = "";
        try {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("custId", GeneralPref.getCustId());
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            Log.d("dinesh", "doInBackground: " + jsonObject);
            response = httpUrlConnectionPost.sendHTTPData(Constants.GET_ZONE, jsonObject);
        } catch (Exception je) {
            je.printStackTrace();

        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        context.hideDialog();
        try {
            Utility.isBuild(s);

            ArrayList<KYCModel> arrayList=new ArrayList<>();
            JSONObject jsonObject = new JSONObject(s)/*.getJSONObject("GetZoneResult")*/;
            if (jsonObject.getBoolean("success")) {
                KYCModel kycModel1= new KYCModel();
                kycModel1.setName("Select State");
                kycModel1.setId("0");
                arrayList.add(kycModel1);

                JSONArray jDataArr = jsonObject.getJSONArray("data");
                for (int i = 0; i < jDataArr.length(); i++) {
                    KYCModel kycModel = new KYCModel();
                    JSONObject jData = jDataArr.getJSONObject(i);
                    kycModel.setId(jData.getString("Id"));
                    kycModel.setName(jData.getString("State"));
                    arrayList.add(kycModel);


                }

                context.getZone(arrayList);
            }
        } catch (Exception je) {
            je.printStackTrace();
        }

    }
}