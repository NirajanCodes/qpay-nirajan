package net.qpaysolutions.QPay.CustomerKYC;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONArray;
import org.json.JSONObject;

public class KYC_check_fragment extends Fragment {
    private TextView fullName, dob, fatherName, motherName, grandFatherName, tole, idNo, issueDate, emailId, address, dateType, Gender, occupation,idType,idIssuedDateType,district,state,municipality,kycStauts;
    private QPayProgressDialog qPayProgressDialog;
    private View view;
    private  KYCDetailsModel kycDetailsModel;
    private String  urlFront, urlBack, urlPhoto;
    public ImageView idFront, idBack, idPhoto;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
          kycDetailsModel=new KYCDetailsModel();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        qPayProgressDialog=new QPayProgressDialog(getActivity());
        this.view = view;
        new KycDetailAPI(KYC_check_fragment.this).execute();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.kyc_check_fragment, container, false);
    }

    private void setEditex(View view) {

        idFront = (ImageView) getView().findViewById(R.id.idFront);
        idBack = (ImageView) getView().findViewById(R.id.idBack);
        idPhoto = (ImageView) getView().findViewById(R.id.imageUser);

        fullName = view.findViewById(R.id.fullName);
        dob = view.findViewById(R.id.dob);
        fatherName = view.findViewById(R.id.fatherName);
        motherName = view.findViewById(R.id.motherName);
        grandFatherName = view.findViewById(R.id.grandFatherName);
        tole = view.findViewById(R.id.tole);
        idNo = view.findViewById(R.id.identification);
        issueDate = view.findViewById(R.id.idIssueDate);
        emailId = view.findViewById(R.id.emailId);
        address = view.findViewById(R.id.address);
        dateType=view.findViewById(R.id.dateTypeDob);
        idType=view.findViewById(R.id.idType);
        Gender=view.findViewById(R.id.gender);
        idIssuedDateType=view.findViewById(R.id.idIssuedDateType);
        occupation=view.findViewById(R.id.occupation);
        district=view.findViewById(R.id.district);
        state=view.findViewById(R.id.state);
        municipality=view.findViewById(R.id.vdcMunicipalityId);
        kycStauts=view.findViewById(R.id.kycStatus);

    }


    private void setText() {
        Picasso.get()
                .load(kycDetailsModel.getIdFrontUrl()).placeholder(R.drawable.user_photo_placeholder).error(R.drawable.user_photo_placeholder).into(idFront);
        Picasso.get()
                .load(kycDetailsModel.getIdBackUrl()).placeholder(R.drawable.user_photo_placeholder).error(R.drawable.user_photo_placeholder).into(idBack);
        Picasso.get()
                .load(kycDetailsModel.getPhotoUrl()).placeholder(R.drawable.user_photo_placeholder).error(R.drawable.user_photo_placeholder).into(idPhoto);
        urlFront = kycDetailsModel.getIdFrontUrl();
        urlBack = kycDetailsModel.getIdBackUrl();
        urlPhoto = kycDetailsModel.getPhotoUrl();


        fullName.setText(kycDetailsModel.getCustomerName());
        dob.setText(kycDetailsModel.getDateOfBirth());
        fatherName.setText(kycDetailsModel.getFatherName());
        motherName.setText(kycDetailsModel.getMotherName());
        grandFatherName.setText(kycDetailsModel.getGrandFatherName());
        tole.setText(kycDetailsModel.getTole());
        idNo.setText(kycDetailsModel.getIdentification());
        issueDate.setText(kycDetailsModel.getIdIssuedDate());
        emailId.setText(kycDetailsModel.getEmailId());
        address.setText(kycDetailsModel.getAddress());
        occupation.setText(kycDetailsModel.getOccupation());
        dateType.setText(kycDetailsModel.getDateOfBirthType());
        idType.setText(kycDetailsModel.getTypeOfId());
        Gender.setText(kycDetailsModel.getGender());
        idIssuedDateType.setText(kycDetailsModel.getIdIssuedDateType());
        district.setText(kycDetailsModel.getDistrict());
        state.setText(kycDetailsModel.getState());
        municipality.setText(kycDetailsModel.getVdcMunicipality());
        String kyc= GeneralPref.getKYC();
        if(kyc.equalsIgnoreCase("A")) {
            kycStauts.setTextColor(getResources().getColor(R.color.green500));
            kycStauts.setText("ACCEPTED");
        }else {
            kycStauts.setTextColor(getResources().getColor(R.color.btn_color));
            kycStauts.setText("Processing");
        }

    }

    public class KycDetailAPI extends AsyncTask<String, String, String> {
        KYC_check_fragment kycFragment;

        public KycDetailAPI(KYC_check_fragment kycFragment) {
            this.kycFragment = kycFragment;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("custId", GeneralPref.getCustId());
                jsonObject.put("appId", GeneralPref.getAppId());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                Log.d("nirajan",jsonObject.toString());
                return NetworkAPI.sendHTTPData(Constants.DETAILS_KYC, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                Log.d("dinesh", "onPostExecute: " + s);
                JSONObject jsonObject = new JSONObject(s);
                JSONArray data = jsonObject.getJSONArray("data");
                JSONObject object = data.getJSONObject(0);
                String CustomerName = object.getString("CustomerName");
                String DateOfBirth = object.getString("DateOfBirth");
                String DateOfBirthType = object.getString("DateOfBirthType");
                String District = object.getString("District");
                String DistrictId = object.getString("DistrictId");
                String FatherName = object.getString("FatherName");
                String Gender = object.getString("Gender");
                String GrandFatherName = object.getString("GrandFatherName");
                String IdBackUrl = object.getString("IdBackUrl");
                String IdFrontUrl = object.getString("IdFrontUrl");
                String IdIssuedDate = object.getString("IdIssuedDate");
                String IdIssuedDateType = object.getString("IdIssuedDateType");
                String IdIssuedFrom = object.getString("IdIssuedFrom");
                String Identification = object.getString("Identification");
                String KycStatus = object.getString("KycStatus");
                String MotherName = object.getString("MotherName");
                String Occupation = object.getString("Occupation");
                String PhotoUrl = object.getString("PhotoUrl");
                String State = object.getString("State");
                String Tole = object.getString("WardNo");
                String TypeOfId = object.getString("TypeOfId");
                String VdcMunicipality = object.getString("VdcMunicipality");
                String VdcMunicipalityId = object.getString("VdcMunicipalityId");

                String EmailId = object.getString("EmailId");
                String Address = object.getString("Address");


                GeneralPref.setKYC(KycStatus);

                //        private EditText fullName, dob, fatherName, motherName, grandFatherName, tole, idNo, issueDate;

                kycDetailsModel.setCustomerName(CustomerName);
                kycDetailsModel.setDateOfBirthType(DateOfBirthType);
                kycDetailsModel.setFatherName(FatherName);
                kycDetailsModel.setMotherName(MotherName);
                kycDetailsModel.setGrandFatherName(GrandFatherName);
                kycDetailsModel.setTole(Tole);
                kycDetailsModel.setIdentification(Identification);
                kycDetailsModel.setIdIssuedDate(IdIssuedDate);
                kycDetailsModel.setIdIssuedDateType(IdIssuedDateType);
                kycDetailsModel.setDateOfBirth(DateOfBirth);
                kycDetailsModel.setGender(Gender);
                kycDetailsModel.setPhotoUrl(PhotoUrl);
                kycDetailsModel.setIdFrontUrl(IdFrontUrl);
                kycDetailsModel.setIdBackUrl(IdBackUrl);
                kycDetailsModel.setOccupation(Occupation);
                kycDetailsModel.setEmailId(EmailId);
                kycDetailsModel.setAddress(Address);
                kycDetailsModel.setTypeOfId(TypeOfId);
                kycDetailsModel.setKycStatus(KycStatus);
                kycDetailsModel.setDistrict(District);
                kycDetailsModel.setVdcMunicipality(VdcMunicipality);
                kycDetailsModel.setState(State);



//                kycFragment.getDetails(kycDetailsModel);
                setEditex(view);
                setText();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
