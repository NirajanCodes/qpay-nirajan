package net.qpaysolutions.QPay.CustomerKYC;

import android.os.AsyncTask;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 2/23/18.
 */

public class SubmitImage extends AsyncTask<String,String,String> {

    String base64,type;
    KYCFragment kycFragment;

    public SubmitImage(String base64, KYCFragment kycFragment,String type) {
        this.base64 = base64;
        this.kycFragment = kycFragment;
        this.type=type;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("custId", GeneralPref.getCustId());
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            jsonObject.put("img",base64);
            jsonObject.put("code","kycdoc");
            Utility.isBuild(jsonObject.toString());
            return NetworkAPI.sendHTTPData(Constants.UPLOAD_IMAGE,jsonObject);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        kycFragment.hideDialog();
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            boolean success = jsonObject.getBoolean("success");
            if (success){

                JSONArray data = jsonObject.getJSONArray("data");
                JSONObject object= data.getJSONObject(0);
                String ImageUrl = object.getString("ImageUrl");
                switch (type){
                    case "front":
                        kycFragment.getUrlFront(ImageUrl);
                        break;
                    case "back":
                        kycFragment.getUrlBack(ImageUrl);
                        break;
                    case "photo":
                        kycFragment.getUrlPhoto(ImageUrl);
                        break;
                }
            }
            else {
                switch (type){
                    case "front":
                        kycFragment.setNullFront();
                        break;
                    case "back":
                        kycFragment.setNullBack();
                        break;
                    case "photo":
                        kycFragment.setNullPhoto();
                        break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            switch (type){
                case "front":
                    kycFragment.setNullFront();
                    break;
                case "back":
                    kycFragment.setNullBack();
                    break;
                case "photo":
                    kycFragment.setNullPhoto();
                    break;
            }
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        kycFragment.showDialog();
    }
}
