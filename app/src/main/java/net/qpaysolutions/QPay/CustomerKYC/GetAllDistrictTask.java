package net.qpaysolutions.QPay.CustomerKYC;

import android.os.AsyncTask;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/7/18.
 */

class GetAllDistrictTask extends AsyncTask<String, String, String> {

    KYCFragment kycFragment;

    public GetAllDistrictTask(KYCFragment kycFragment) {
        this.kycFragment = kycFragment;
    }

    @Override
    protected void onPreExecute() {
      kycFragment.showDialog();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = "";
        try {
            NetworkAPI httpUrlConnectionPost = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("custId", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());

            response = httpUrlConnectionPost.sendHTTPData(Constants.GET_ALL_DISTRICT, jsonObject);
        } catch (JSONException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {

        kycFragment.hideDialog();
        try {
            JSONObject jsonObject = new JSONObject(s).getJSONObject("GetAllDistrictResult");
            if (jsonObject.getBoolean("success")) {
                ArrayList<KYCModel> arrayList = new ArrayList<>();
                KYCModel kycModel1= new KYCModel();
                kycModel1.setName("Select District");
                kycModel1.setId("0");
                arrayList.add(kycModel1);
                try {
                    JSONArray jDataArr = jsonObject.getJSONArray("data");
                    for (int i = 0; i < jDataArr.length(); i++) {
                        KYCModel kycModel = new KYCModel();
                        JSONObject jData = jDataArr.getJSONObject(i);
                        kycModel.setId(jData.getString("Id"));
                        kycModel.setName(jData.getString("District"));
                        arrayList.add(kycModel);
                    }
                    kycFragment.getDistrictAll(arrayList);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } catch (Exception je) {

        }
    }
}