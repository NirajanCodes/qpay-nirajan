package net.qpaysolutions.QPay.Sqlitedatabases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.Chat.conversation.ConversationModule;
import net.qpaysolutions.QPay.Resturant.itemMenu.MenuListModel;
import net.qpaysolutions.QPay.TrafficHistory.TrafficModel;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;

/**
 * Created by dinesh on 5/15/16.
 */

public class QPayCustomerDatabase extends SQLiteOpenHelper {
    String TAG = "dinesh";
    private static final String LOG = "sctDatabaseHelper";
    private static final String DATABASE_NAME = "qpay";
    private static final int DATABASE_VERSION = 3;
    private static final String TABLE_CUSTIMER_DATA = "customer_data";
    private static final String TABLE_FRIEND_LIST = "friend_list";
    private static final String TABLE_PROFILE = "profile";
    private static final String TABLE_REGISTRATION_FLAG = "registration_flag";
    private static final String TABLE_NOTIFICATION = "notification";
    private static final String TABLE_PIN = "pin";
    private static final String TABLE_QUICK_PAY = "quick_pay";
    private static final String TABLE_ACCOUNT = "account";
    private static final String TABLE_CART = "cart";
    private static final String TABLE_CHAT_LIST = "chat_list";
    private static final String TABLE_CONVERSATION = "conversation";
    private static final String TABLE_TRAFFIC = "traffic";
    private static final String TABLE_QUICK_PAY_NEA = "nea_quick_payment";

    //element of table custumer data
    private static final String KEY_ID = "id";
    private static final String KEY_APP_ID = "app_id";
    private static final String KEY_APP_LOCK = "app_lock";
    private static final String KEY_CURR_CODE = "curr_code";
    private static final String KEY_CURR_TYPE = "curr_type";
    private static final String KEY_CUST_ID_CUSTUMER = "cust_id";
    private static final String KEY_FIRST_NAME = "first_name";
    private static final String KEY_LAST_NAME = "last_name";
    private static final String KEY_L_PIN = "l_pin";
    private static final String KEY_PIN_FLAG = "pin_flag";
    private static final String KEY_PIN_RETRIES_COUNT = "pin_retries_count";
    private static final String KEY_RESET_PIN = "reset_pin";
    private static final String KEY_DEVICE_16 = "enc_cust_id";
    private static final String KEY_PHONE_CUSTUMER = "cust_phone";
    private static final String KEY_IMAGE_ID = "img";
    private static final String KEY_NOTIFICATION = "is_push";
    private static final String KEY_SMS = "is_sms";

    //element of table friend list

    private static final String KEY_CELL_NO = "cell_no";
    private static final String KEY_CUST_ID_FRIEND = "cust_id";
    private static final String KEY_NAME = "name";
    private static final String KEY_ID_FRIENDLIST = "_id";
    private static final String ENC_ID_FRIEND = "enc_cust_id";
    private static final String IMG_URL_FRIENDS = "img_url";
    //conversation
    private static final String KEY_MAPPING = "conversation_mapping";
    private static final String KEY_CONVERSATION = "conversation_msg";
    private static final String KEY_FLAG = "conversation_flag";
    private static final String KEY_DATE = "conversation_date";
    private static final String KEY_SEND_FAIL = "send_fail";
    private static final String KEY_MESSAGE_ID = "messageId";
    private static final String KEY_IMG_MESSAGE = "Image_Message";
    private static final String KEY_ISIMAGE = "IsImage";
    //chat list
    private static final String KEY_CHAT_CELL = "cell_no_chat";
    private static final String KEY_CHAT_NAME = "name_chat";
    private static final String KEY_CHAT_CUST_ID = "cust_id_friend_chat";
    private static final String KEY_CHAT_URL = "img_url_chat";
    private static final String KEY_CHAT_DATE = "date_chat";
    private static final String KEY_CHAT_MSG = "msg_chat";
    private static final String TOTAL_UNREAD = "TotalUnreadMsg";
    //element of table profile
    private static final String KEY_ACT_FLAG = "act_flag";
    private static final String KEY_CARD_TYPE = "card_type";
    private static final String KEY_EXP_DATE = "exp_date";
    private static final String KEY_LAST_FOUR = "last_4";
    private static final String KEY_PROFILE_NAME = "profile_name";
    private static final String KEY_ID_PROFILELIST = "_id";
    private static final String KEY_IMAGE_ATM = "img_id";
    private static final String KEY_CARD_FULL_NO = "card_full";
    private static final String KEY_CARD_NATURE = "card_nature";
    private static final String KEY_CARD_BALANCE = "card_balance";
    private static final String KEY_DATE_LAST_KNOWN = "known_date";
    //element of table registration flag
    private static final String KEY_REG_FLAG = "reg_flag";
    //    Notificatio table
    private static final String KEY_NOTIFICATION_TEXT = "notification";
    private static final String KEY_NOTIFICATION_DATE = "date";
    private static final String KEY_NOTIFICATION_TITLE = "title";
    //register
    private static final String KEY_PIN_TITLE = "title";
    private static final String KEY_PIN_DATE = "date";
    private static final String KEY_PIN_AMOUNT = "amount";
    private static final String KEY_PIN_No = "pn";
    private static final String KEY_PIN_SN = "sn";
    //quick pay
    private static final String KEY_QUICK_NUMBER = "quick_number";
    private static final String KEY_QUICK_BYECODE = "quick_code";
    private static final String KEY_QUICK_DATE = "quick_date";
    private static final String KEY_CUSTUMER_QUICK = "quick_custmer";
    private static final String KEY_CUSTUMER_TITLE = "quick_title";

    //quick pay new
    private static final String KEY_QUICK_PHONE_NUMBER = "quick_phone_umber";
    private static final String KEY_QUICK_AMOUNT = "quick_amount";

    //quick pay nea
    private static final String KEY_REGION = "nea_region";
    private static final String KEY_SC_NO = "nea_amount";
    private static final String KEY_CUSTOMER_ID = "nea_customer_id";

    //account table
    private static final String KEY_ACT_FLAG_ACCOUNT = "act_flag_account";
    private static final String KEY_ACCOUNT_NO = "account_no";
    private static final String KEY_ACCOUNT_NAME = "account_name";
    private static final String KEY_ACCOUNT_IMAGE = "account_img";
    private static final String KEY_BANK_NAME_ACCOUNT = "account_bank";
    private static final String KEY_BANK_BRANCH_ACCOUNT = "account_bank_branch";
    private static final String KEY_ACCOUNT_ID = "account_id";
    private static final String KEY_VERIFY_AMOUNT = "amount";
    private static final String KEY_ACCOUNT_DATE = "account_date";
    private static final String KEY_TRY_COUNTER = "try_counter";
    //    kyc
    public static final String ZONE_TABLE_NAME = "zone";
    private static final String ZONE_PKey = "_id";
    private static final String ZONE_ID = "zone_id";
    private static final String ZONE_NAME = "zone_name";
    private static final String DISTRICT_TABLE_NAME = "district";
    private static final String DISTRICT_PKey = "_id";
    private static final String DISTRICT_ID = "district_id";
    private static final String DISTRICT_NAME = "district_name";

    private static final String VDC_TABLE_NAME = "vdc";
    private static final String VDC_PKey = "_id";
    private static final String VDC_ID = "vdc_id";
    private static final String VDC_NAME = "vdc_name";

    private static final String CATEGORY_ID = "CategoryId";
    private static final String DESCRIPTION = "Description";
    private static final String IMAGE_URL = "ImageUrl";
    private static final String ITEM_ID = "ItemId";
    private static final String ITEM_NAME = "ItemName";
    private static final String RATE = "Rate";
    private static final String COUNT = "Count";
    private static final String SUBTOTAL = "Subtotal";
    //traffic pay sleep
    private static final String NAME_FINEED = "name";
    private static final String CHEAT_ID = "chit_id";
    private static final String IDENTIFICATION_NUMBER = "identification_id";
    private static final String CAUSE = "cause";
    private static final String FINE_AMOUNT = "amount";
    private static final String CRRN_TAFFIC = "crrn";
    private static final String STAN_TRAFFIC = "stan";

    //resturant

    public static final String ALL_DISTRICT_TABLE_NAME = "all_district";
    private static final String ALL_DISTRICT_PKey = "_id";
    private static final String ALL_DISTRICT_ID = "all_district_id";
    private static final String ALL_DISTRICT_NAME = "all_district_name";


    private static final String CREATE_TABLE_ZONE = "CREATE TABLE " + ZONE_TABLE_NAME + " ( " +
            ZONE_PKey + " integer primary key autoincrement, " +
            ZONE_ID + " integer, " +
            ZONE_NAME + " varchar" +
            " )";

    private static final String CREATE_TABLE_DISTRICT = "CREATE TABLE " + DISTRICT_TABLE_NAME + " ( " +
            DISTRICT_PKey + " integer primary key autoincrement, " +
            DISTRICT_ID + " integer, " +
            DISTRICT_NAME + " varchar" +
            " )";
    private static final String CREATE_TABLE_VDC = "CREATE TABLE " + VDC_TABLE_NAME + " ( " +
            VDC_PKey + " integer primary key autoincrement, " +
            VDC_ID + " integer, " +
            VDC_NAME + " varchar" +
            " )";

    private static final String CREATE_TABLE_ALL_DISTRICT = "CREATE TABLE " + ALL_DISTRICT_TABLE_NAME + " ( " +
            ALL_DISTRICT_PKey + " integer primary key autoincrement, " +
            ALL_DISTRICT_ID + " integer, " +
            ALL_DISTRICT_NAME + " varchar" +
            " )";


    //crete table custumer data
    private static final String CREATE_TABLE_CUSTUMER_DATA = "create table " + TABLE_CUSTIMER_DATA + "(" + KEY_ID + " integer primary key, " + KEY_APP_ID + " text , " + KEY_CUST_ID_CUSTUMER + " text , " + KEY_APP_LOCK + " text, " + KEY_CURR_CODE + " text, " + KEY_CURR_TYPE + " text, " + KEY_FIRST_NAME + " text, " + KEY_LAST_NAME + " text ," + KEY_L_PIN + " text ," + KEY_PIN_FLAG + " text, " + KEY_PIN_RETRIES_COUNT + " text, " + KEY_RESET_PIN + " text , " + KEY_PHONE_CUSTUMER + " text ," + KEY_DEVICE_16 + " text , " + KEY_NOTIFICATION + " text , " + KEY_SMS + " text , " + KEY_IMAGE_ID + " text  " + ")";
    //create table friend list
    private static final String CREATE_TABLE_FRIEND_LIST = "create table " + TABLE_FRIEND_LIST + "(" + KEY_ID_FRIENDLIST + " integer primary key autoincrement, " + KEY_CELL_NO + " text, " + KEY_CUST_ID_FRIEND + " text, " + ENC_ID_FRIEND + " , " + KEY_NAME + " text , " + IMG_URL_FRIENDS + " text " + ")";
    // create table profile
    private static final String CREATE_TABLE_PROFILE = " create table " + TABLE_PROFILE + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_ACT_FLAG + " text, " + KEY_CARD_TYPE + " text ," + KEY_EXP_DATE + " text, " + KEY_LAST_FOUR + " text, " + KEY_PROFILE_NAME + " text , " + KEY_CARD_FULL_NO + " text , " + KEY_CARD_NATURE + " text , " + KEY_CARD_BALANCE + " text , " + KEY_DATE_LAST_KNOWN + " text ," + KEY_IMAGE_ATM + " text " + ")";
    // create table restaurant
    private static final String CREATE_TABLE_CART = " create table " + TABLE_CART + "(" + KEY_ID_PROFILELIST + " integer primary key, " + CATEGORY_ID + " text, " + DESCRIPTION + " text ," + IMAGE_URL + " text, " + ITEM_ID + " text, " + ITEM_NAME + " text , " + RATE + " real , " + " text ," + COUNT + " integer " + ")";
    //chat
// create table chat
    private static final String CREATE_TABLE_CHAT_LIST = " create table " + TABLE_CHAT_LIST + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_CHAT_CELL + " text, " + KEY_CHAT_NAME + " text ," + KEY_CHAT_CUST_ID + " text, " + KEY_CHAT_URL + " text, " + KEY_CHAT_DATE + " text , " + TOTAL_UNREAD + " text ," + KEY_CHAT_MSG + " text " + ")";
    //create table for conversation
    private static final String CREATE_TABLE_CONVERSATION = " create table " + TABLE_CONVERSATION + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_MAPPING + " text, " + KEY_CONVERSATION + " text ," + KEY_DATE + " text ," + KEY_SEND_FAIL + " text , " + KEY_MESSAGE_ID + " text ," + KEY_IMG_MESSAGE + " text ," + KEY_ISIMAGE + " text ," + KEY_FLAG + " text " + ")";
    // create table of registration flag
    private static final String CREATE_TABLE_FLAG = " create table " + TABLE_REGISTRATION_FLAG + "(" + KEY_REG_FLAG + " text " + ")";
    private static final String CREATE_TABLE_NOTIFICATION = " create table  " + TABLE_NOTIFICATION + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_NOTIFICATION_TEXT + " text , " + KEY_NOTIFICATION_TITLE + " text , " + KEY_NOTIFICATION_DATE + " text " + ")";
    private static final String CREATE_TABLE_PIN = " create table  " + TABLE_PIN + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_PIN_TITLE + " text , " + KEY_PIN_DATE + " text , " + KEY_PIN_AMOUNT + " text , " + KEY_PIN_No + " text , " + KEY_PIN_SN + " text " + ")";
    private static final String CREATE_TABLE_QUICK = " create table  " + TABLE_QUICK_PAY + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_QUICK_NUMBER + " text , " + KEY_QUICK_DATE + " text , " + KEY_CUSTUMER_QUICK + " text , " + KEY_CUSTUMER_TITLE + " text , " + KEY_QUICK_BYECODE + " text " + ")";
    private static final String CREATE_TABLE_ACCOUNT = " create table  " + TABLE_ACCOUNT + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_ACT_FLAG_ACCOUNT + " text , " + KEY_ACCOUNT_NO + " text , " + KEY_ACCOUNT_NAME + " text , " + KEY_ACCOUNT_IMAGE + " text , " + KEY_BANK_NAME_ACCOUNT + " text , " + KEY_BANK_BRANCH_ACCOUNT + " text , " + KEY_VERIFY_AMOUNT + " text , " + KEY_ACCOUNT_DATE + " text , " + KEY_TRY_COUNTER + " text , " + KEY_ACCOUNT_ID + " text " + ")";
    //traffic
    private static final String CREATE_TABLE_TRAFFIC = " create table " + TABLE_TRAFFIC + "(" + KEY_ID_PROFILELIST + " integer primary key, " + NAME_FINEED + " text, " + CHEAT_ID + " text ," + IDENTIFICATION_NUMBER + " text, " + CAUSE + " text, " + FINE_AMOUNT + " text , " + CRRN_TAFFIC + " text , " + STAN_TRAFFIC + " text " + ")";

    //Quick Pay Tables
    private static final String CREATE_TABLE_QUICK_NEA = " create table " + TABLE_QUICK_PAY_NEA + "(" + KEY_ID_PROFILELIST + " integer primary key, " + KEY_CUSTOMER_ID + " text, " + KEY_REGION + " text, " + KEY_SC_NO + " text" + ")";


    public QPayCustomerDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    /*"/mnt/sdcard/qpay.db"*/

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CUSTUMER_DATA);
        db.execSQL(CREATE_TABLE_FLAG);
        db.execSQL(CREATE_TABLE_FRIEND_LIST);
        db.execSQL(CREATE_TABLE_PROFILE);
        db.execSQL(CREATE_TABLE_NOTIFICATION);
        db.execSQL(CREATE_TABLE_PIN);
        db.execSQL(CREATE_TABLE_QUICK);
        db.execSQL(CREATE_TABLE_ACCOUNT);
        db.execSQL(CREATE_TABLE_CART);
        db.execSQL(CREATE_TABLE_CHAT_LIST);
        db.execSQL(CREATE_TABLE_CONVERSATION);
        db.execSQL(CREATE_TABLE_TRAFFIC);
        db.execSQL(CREATE_TABLE_ZONE);
        db.execSQL(CREATE_TABLE_DISTRICT);
        db.execSQL(CREATE_TABLE_VDC);
        db.execSQL(CREATE_TABLE_ALL_DISTRICT);
        db.execSQL(CREATE_TABLE_QUICK_NEA);
        Log.d(TAG, "onCreate cart : " + CREATE_TABLE_CART);
        Log.d(TAG, CREATE_TABLE_CUSTUMER_DATA);
        Log.d(TAG, CREATE_TABLE_FLAG);
        Log.d(TAG, CREATE_TABLE_FRIEND_LIST);
        Log.d(TAG, CREATE_TABLE_PROFILE);
        Log.d(TAG, CREATE_TABLE_QUICK);
        Log.d(TAG, "onCreate: district tabale " + CREATE_TABLE_DISTRICT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

/*        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTIMER_DATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FRIEND_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REGISTRATION_FLAG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PIN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUICK_PAY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONVERSATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAFFIC);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUICK_PAY_NEA);

        db.execSQL("DROP TABLE IF EXISTS " + ZONE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + DISTRICT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + VDC_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ALL_DISTRICT_TABLE_NAME);*/

        if (oldVersion != newVersion) {
            db.beginTransaction();
            try {
                if (oldVersion == 0) {
                    onCreate(db);
                } else {
                    if (oldVersion > newVersion) {
                        onDowngrade(db, oldVersion, newVersion);
                    } else {
                        try {
                            db.execSQL(CREATE_TABLE_QUICK_NEA);
                            //onCreate(db);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
//                onCreate(db);
//                upgradeVersion3(db);
                db.setVersion(newVersion);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        }

    }

    public long insertAccount(String name, String no, String bank_name, String bank_branch, String imgurl, String account_flag, String account_id, String amount, String date, String try_counter) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ACCOUNT_NAME, name);
        contentValues.put(KEY_ACCOUNT_NO, no);
        contentValues.put(KEY_BANK_NAME_ACCOUNT, bank_name);
        contentValues.put(KEY_BANK_BRANCH_ACCOUNT, bank_branch);
        contentValues.put(KEY_ACCOUNT_IMAGE, imgurl);
        contentValues.put(KEY_ACT_FLAG_ACCOUNT, account_flag);
        contentValues.put(KEY_ACCOUNT_ID, account_id);
        contentValues.put(KEY_VERIFY_AMOUNT, amount);
        contentValues.put(KEY_ACCOUNT_DATE, date);
        contentValues.put(KEY_TRY_COUNTER, try_counter);
        return sqLiteDatabase.insert(TABLE_ACCOUNT, null, contentValues);
    }

    public long insertCartItem(String CategoryId, String Description, String ImageUrl, String ItemId, String ItemName, double Rate, int Count) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CATEGORY_ID, CategoryId);
        contentValues.put(DESCRIPTION, Description);
        contentValues.put(IMAGE_URL, ImageUrl);
        contentValues.put(ITEM_ID, ItemId);
        contentValues.put(ITEM_NAME, ItemName);
        contentValues.put(RATE, Rate);
        contentValues.put(COUNT, Count);

        return sqLiteDatabase.insert(TABLE_CART, null, contentValues);
    }

    public long insertQuickinfo(String no, String code, String date, String quick_cust) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_QUICK_NUMBER, no);
        contentValues.put(KEY_QUICK_BYECODE, code);
        contentValues.put(KEY_QUICK_DATE, date);
        contentValues.put(KEY_CUSTUMER_QUICK, quick_cust);
        return sqLiteDatabase.insert(TABLE_QUICK_PAY, null, contentValues);

    }

/*    public long insertQuickInfoNew(String no, String amount) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_QUICK_NUMBER, no);
        contentValues.put(KEY_QUICK_AMOUNT, amount);
        return sqLiteDatabase.insert(TABLE_QUICK_PAY_NEW, null, contentValues);
    }*/

    public long insertQuickInfoNea(String customerId, String region, String scNo) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CUSTOMER_ID, customerId);
        contentValues.put(KEY_REGION, region);
        contentValues.put(KEY_SC_NO, scNo);

        return sqLiteDatabase.insert(TABLE_QUICK_PAY_NEA, null, contentValues);

    }

    public long insertPininfo(String title, String date, String amount, String pn, String sn) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PIN_TITLE, title);
        contentValues.put(KEY_PIN_DATE, date);
        contentValues.put(KEY_PIN_AMOUNT, amount);
        contentValues.put(KEY_PIN_No, pn);
        contentValues.put(KEY_PIN_SN, sn);
        return sqLiteDatabase.insert(TABLE_PIN, null, contentValues);
    }

    public long insertProfile(String cardtype, String expdate, String last_4, String flag, String Dummy, String img_id, String card_full, String card_nature) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CARD_TYPE, cardtype);
        contentValues.put(KEY_EXP_DATE, expdate);
        contentValues.put(KEY_LAST_FOUR, last_4);
        contentValues.put(KEY_ACT_FLAG, flag);
        contentValues.put(KEY_PROFILE_NAME, Dummy);
        contentValues.put(KEY_IMAGE_ATM, img_id);
        contentValues.put(KEY_CARD_FULL_NO, card_full);
        contentValues.put(KEY_CARD_NATURE, card_nature);
        return sqLiteDatabase.insert(TABLE_PROFILE, null, contentValues);

    }

    public void insertNotification(String msg, String date, String title) {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NOTIFICATION_TEXT, msg);
        contentValues.put(KEY_NOTIFICATION_DATE, date);
        contentValues.put(KEY_NOTIFICATION_TITLE, title);
        sqLiteDatabase.insert(TABLE_NOTIFICATION, null, contentValues);
    }

    public void insertImg(String str) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_IMAGE_ID, str);
        sqLiteDatabase.insert(TABLE_CUSTIMER_DATA, null, values);
    }

    public void updateAccountFlag(String str, String id) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(KEY_ACT_FLAG_ACCOUNT, str);
        db.execSQL("update account set act_flag_account = " + str + " where _id = " + id);

    }

    public void updateAccountTry(String str, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(KEY_ACT_FLAG_ACCOUNT, str);
        db.execSQL("update account set try_counter = " + str + " where _id = " + id);

    }

    public void deletAccount(String _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from account where _id = '" + _id + "'");
    }


    public String getAccountBank(String id) {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select account_bank from  account where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("account_bank"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getAmount(String id) {
        String amount = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select amount from  account where _id = " + id + "";
        Log.d(Utility.TAG, "get last four for top up : " + selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                amount = cursor.getString(cursor.getColumnIndex("amount"));
            } while (cursor.moveToNext());
        }
        return amount;
    }

    public String getAccountBranch(String id) {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select account_bank_branch from  account where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("account_bank_branch"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getImage(String id) {
        String image = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select account_img from  account where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                image = cursor.getString(cursor.getColumnIndex("account_img"));
            } while (cursor.moveToNext());
        }
        return image;
    }

    public String getAccountHolder(String id) {
        String accountGHolder = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select account_name from  account where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                accountGHolder = cursor.getString(cursor.getColumnIndex("account_name"));
            } while (cursor.moveToNext());
        }
        return accountGHolder;
    }

    public String getAccountNumber(String id) {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select account_no from  account where _id = " + id + "";
        Log.d(Utility.TAG, "get last four for top up : " + selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("account_no"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getAccountRetryCounter(String id) {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select try_counter from  account where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("try_counter"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getKeyAccountId(String id) {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select account_id from  account where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("account_id"));

            } while (cursor.moveToNext());
        }
        return s;
    }


    public int checkAccNo(String acc_no, String bank_name) {
        boolean flag;
        String selectQuery = " select 1 from account where account_no = " + acc_no + " and account_bank =  '" + bank_name + "'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();

        return reg_count;
    }

    public void updateTitle(String quicktitle, String id) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CUSTUMER_TITLE, quicktitle);
        sqLiteDatabase.execSQL(" update quick_pay set quick_title = '" + quicktitle + "' where _id = " + id);
    }

    public void updateCardBalance(String quicktitle, String id, String last_known_date) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CARD_BALANCE, quicktitle);
        contentValues.put(KEY_DATE_LAST_KNOWN, last_known_date);
        sqLiteDatabase.execSQL(" update profile set card_balance = '" + quicktitle + "' , known_date = '" + last_known_date + "' where _id = " + id);
    }

    public String checkNumber() {
        String cardType = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "select quick_number from quick_pay ";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                cardType = cursor.getString(cursor.getColumnIndexOrThrow("quick_number"));
            } while (cursor.moveToNext());
        }
        return cardType;

    }

    public String checkQuickCode() {
        String cardType = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "select quick_code from quick_pay ";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                cardType = cursor.getString(cursor.getColumnIndexOrThrow("quick_code"));
            } while (cursor.moveToNext());
        }
        return cardType;
    }

    public String getQuickId(String _id) {
        String cardType = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = " select quick_custmer from quick_pay where  _id = " + _id;

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                cardType = cursor.getString(cursor.getColumnIndexOrThrow("quick_custmer"));
            } while (cursor.moveToNext());
        }
        return cardType;
    }

    public String getQuickCode(String _id) {
        String cardType = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "select quick_code from quick_pay where _id = " + _id;

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                cardType = cursor.getString(cursor.getColumnIndexOrThrow("quick_code"));
            } while (cursor.moveToNext());
        }
        return cardType;
    }


///////////////////////////////////////////////////////////////Quick Pay new get////////////////////////////////////////////////////////


    public ArrayList<String[]> getQuickNeaInfo() {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        int i = 0;
        // String selectQuery = " select " + KEY_REGION + " , " + KEY_SC_NO + " , " + KEY_CUSTOMER_ID + " from" + TABLE_QUICK_PAY_NEA + " where " + KEY_ID + " = " + id;
        String selectQuery = " select * from " + TABLE_QUICK_PAY_NEA;
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        String[] customerId = new String[cursor.getCount()];
        String[] scNo = new String[cursor.getCount()];
        String[] region = new String[cursor.getCount()];
        if (cursor != null && cursor.getCount() > 0) {

            cursor.moveToFirst();
            do {

                customerId[i] = cursor.getString(cursor.getColumnIndexOrThrow(KEY_CUSTOMER_ID)).toString();
                Log.d(Utility.TAG, "customer Id : " + customerId[0]);
                scNo[i] = cursor.getString(cursor.getColumnIndexOrThrow(KEY_SC_NO)).toString();
                region[i] = cursor.getString(cursor.getColumnIndexOrThrow(KEY_REGION)).toString();
                i++;

            } while (cursor.moveToNext());

        }

        ArrayList<String[]> arrayList = new ArrayList();
        arrayList.add(customerId);
        arrayList.add(region);
        arrayList.add(scNo);

        return arrayList;

    }


    public void updateImg(String type) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PHONE_CUSTUMER, type);
        sqLiteDatabase.execSQL(" update customer_data set img = '" + type + "'");
    }

    public void updateServiceNotification(String notification) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NOTIFICATION, notification);
        sqLiteDatabase.execSQL(" update customer_data set is_push = " + notification);
    }

    public void updateServiceSMS(String sms) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_SMS, sms);
        sqLiteDatabase.execSQL(" update customer_data set is_sms = " + sms);
    }

    public String getNotificationId() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select is_push from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("is_push"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getSmsId() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select is_sms from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("is_sms"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public void insertAppId(String type) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_APP_ID, type);
        sqLiteDatabase.execSQL(" update customer_data set app_id = " + type);
    }

    public String getKeyAppId() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select app_id from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("app_id"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getKeyCardFullNo(String _id) {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select card_full from  profile where _id = " + _id;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("card_full"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getKeyCardType(String _id) {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select card_nature from  profile where _id = " + _id;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("card_nature"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public void updateCardnature(String type, String position) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CARD_NATURE, type);
        sqLiteDatabase.execSQL(" update profile set card_nature = " + type + " where _id = " + position);
    }

    public void insertImageAtm(String str) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_IMAGE_ATM, str);
        sqLiteDatabase.insert(TABLE_PROFILE, null, values);
    }

    public String getImg() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "select img from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("img"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public long insertCutid(String str, String app_id, String notification, String sms) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CUST_ID_CUSTUMER, str);
        values.put(KEY_APP_ID, app_id);
        values.put(KEY_NOTIFICATION, notification);
        values.put(KEY_SMS, sms);
        return sqLiteDatabase.insert(TABLE_CUSTIMER_DATA, null, values);
    }

    public long insertDevtoken(String str) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DEVICE_16, str);
        return sqLiteDatabase.insert(TABLE_CUSTIMER_DATA, null, values);
    }

    public void updateDevicetoken(String type) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DEVICE_16, type);
        sqLiteDatabase.execSQL(" update customer_data set enc_cust_id = " + type);
    }

    public void updateCust_id(String type) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CUST_ID_CUSTUMER, type);
        sqLiteDatabase.execSQL(" update customer_data set cust_id = " + type);
    }

    public void updateCustIDAppId(String type, String app_id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CUST_ID_CUSTUMER, type);
        values.put(KEY_APP_ID, app_id);
        sqLiteDatabase.execSQL(" update customer_data set cust_id = " + type + " , app_id = '" + app_id + "'");
    }

    public void updateCust_phone(String type) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PHONE_CUSTUMER, type);
        sqLiteDatabase.execSQL(" update customer_data set cust_phone = " + type);
    }

    public long insertCtypeCcode(String type, String code) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CURR_TYPE, type);
        values.put(KEY_CURR_CODE, code);
        return sqLiteDatabase.insert(TABLE_CUSTIMER_DATA, null, values);
    }

    public void updateCtypecode(String type, String code) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CURR_TYPE, type);
        values.put(KEY_CURR_CODE, code);
        sqLiteDatabase.execSQL(" update customer_data set curr_type = " + type + ", curr_code = " + code);
    }

    public long insertFLname(String first, String last) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, first);
        values.put(KEY_LAST_NAME, last);
        return sqLiteDatabase.insert(TABLE_CUSTIMER_DATA, null, values);
    }

    public void updateFLname(String first, String last) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, first);
        values.put(KEY_LAST_NAME, last);
        sqLiteDatabase.execSQL(" update customer_data set first_name = " + first + ", last_name = " + last);
    }

    public long insertFriendList(String cust_id, String name, String cell, String enc_cust_id, String img_url) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CELL_NO, cell);
        values.put(KEY_CUST_ID_CUSTUMER, cust_id);
        values.put(KEY_NAME, name);
        values.put(ENC_ID_FRIEND, enc_cust_id);
        values.put(IMG_URL_FRIENDS, img_url);
        long friendlistinsert = sqLiteDatabase.insert(TABLE_FRIEND_LIST, null, values);
        return friendlistinsert;
    }

    public void deletFriends(long _id) {
        String id = String.valueOf(_id);
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from friend_list where friend_list._id = " + id);
    }

    public long createRegFlag(String str) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_REG_FLAG, str);
        long regflaginsert = sqLiteDatabase.insert(TABLE_REGISTRATION_FLAG, null, values);
        return regflaginsert;
    }

    public int checkHasCustData() {
        boolean data;
        String selectQuery = "select * from " + TABLE_CUSTIMER_DATA;
        Log.d("custumer", selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int custmr_count = cursor.getCount();
        cursor.close();
        db.close();
        return custmr_count;
    }

    public int checkRegData() {
        boolean flag;
        String selectQuery = " select * from " + TABLE_REGISTRATION_FLAG;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();
        return reg_count;

    }

    public String getRegFlag() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select reg_flag from  registration_flag";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("reg_flag"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getimageAtm() {
        String img = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "select img_id from profile ";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                img = cursor.getString(cursor.getColumnIndex("img_id"));
            } while (cursor.moveToNext());

        }
        return img;
    }

    public String getBanknem() {
        String img = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "select profile_name from profile ";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                img = cursor.getString(cursor.getColumnIndex("profile_name"));
            } while (cursor.moveToNext());

        }
        return img;
    }

    public String getCu_idforen() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select name from  friend_list";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("cust_id"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getQuickNumber(String _id) {
        String cardType = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "select quick_number from quick_pay where _id = " + _id;
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                cardType = cursor.getString(cursor.getColumnIndexOrThrow("quick_number"));
            } while (cursor.moveToNext());
        }
        return cardType;
    }

    public String getUserNameFirst() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select last_name from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("last_name"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getUserNameLast() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select first_name from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("first_name"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getname() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select cust_id from  friend_list";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                s = cursor.getString(cursor.getColumnIndex("cust_id"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getCustomerID() {
        String customerId = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select cust_id from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                customerId = cursor.getString(cursor.getColumnIndex("cust_id"));
            } while (cursor.moveToNext());
        }
        return customerId;
    }

    public String getCustomerPhone() {
        String customerPhone = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select cust_phone from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
            cursor.moveToPosition(0);
            do {
                customerPhone = cursor.getString(cursor.getColumnIndex("cust_phone"));
            } while (cursor.moveToNext());
        }
        return customerPhone;
    }

    public String getEncryptedCustomerId() {
        String encyptedCustomerId = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select enc_cust_id from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                encyptedCustomerId = cursor.getString(cursor.getColumnIndex("enc_cust_id"));
            } while (cursor.moveToNext());
        }
        return encyptedCustomerId;
    }

    public String getLastFourCardInfo() {
        String lastFourDigit = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select last_4 from  profile ";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                lastFourDigit = cursor.getString(cursor.getColumnIndex("last_4"));
            } while (cursor.moveToNext());
        }
        return lastFourDigit;
    }


    public String lastFour(String id) {
        String lastFour = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select last_4 from  profile where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                lastFour = cursor.getString(cursor.getColumnIndex("last_4"));
            } while (cursor.moveToNext());
        }
        return lastFour;
    }

    public String getImageId(String id) {
        String imgId = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select img_id from  profile where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                imgId = cursor.getString(cursor.getColumnIndex("img_id"));
            } while (cursor.moveToNext());
        }
        return imgId;
    }

    public String getBankName(String id) {
        String bankName = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select profile_name from  profile where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                bankName = cursor.getString(cursor.getColumnIndex("profile_name"));
            } while (cursor.moveToNext());
        }
        return bankName;
    }

    public String getCardExp(String id) {
        String cardExp = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select exp_date from  profile where _id = " + id + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                cardExp = cursor.getString(cursor.getColumnIndex("exp_date"));
            } while (cursor.moveToNext());
        }
        return cardExp;
    }

    public String getLastFourForBill(long id) {
        String lastFour = null;
        String s = String.valueOf(id);
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select last_4 from  profile where _id = " + s + "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            do {
                lastFour = cursor.getString(cursor.getColumnIndex("last_4"));
            } while (cursor.moveToNext());
        }
        return lastFour;
    }

    public int checkLast4() {
        String selectQuery = " select last_4 from profile";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();
        return reg_count;
    }

    public int checkLast4Reg(String last_4, String exp) {
        boolean flag;
        String selectQuery = " select 1 from profile where last_4 = '" + last_4 + "' and exp_date = '" + exp + "'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();
        return reg_count;
    }

    public int checkQuickNumber(String phone, String quick_code) {
        if (!phone.isEmpty() && phone != null) {
            boolean flag;
            String selectQuery = " select 1 from quick_pay where quick_number = " + phone + " and quick_code = " + quick_code;
            SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            int reg_count = cursor.getCount();
            cursor.close();
            sqLiteDatabase.close();
            return reg_count;
        }
        return 0;

    }

    public String getExp() {
        String exp = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select exp_date from  profile";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {

            cursor.moveToPosition(0);
            do {
                exp = cursor.getString(cursor.getColumnIndex("exp_date"));
            } while (cursor.moveToNext());
        }
        return exp;
    }

    public String getFlag() {
        String flag = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select reg_flag from  registration_flag";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                flag = cursor.getString(cursor.getColumnIndex("reg_flag"));
            } while (cursor.moveToNext());
        }
        return flag;
    }

    public String getFlagPin() {
        String flagPin = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select pin_flag from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                flagPin = cursor.getString(cursor.getColumnIndex("pin_flag"));
            } while (cursor.moveToNext());
        }
        return flagPin;
    }

    public String getPin() {
        String pin = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select l_pin from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                pin = cursor.getString(cursor.getColumnIndex("l_pin"));
            } while (cursor.moveToNext());
        }
        return pin;
    }

    public String getFirstName() {
        String firstName = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select first_name from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                firstName = cursor.getString(cursor.getColumnIndex("first_name"));
            } while (cursor.moveToNext());
        }
        return firstName;
    }

    public String lastName() {
        String lastName = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select last_name from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                lastName = cursor.getString(cursor.getColumnIndex("last_name"));
            } while (cursor.moveToNext());
        }
        return lastName;
    }


    public void updateFlag(String str) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("reg_flag", str);
        db.execSQL("update registration_flag set reg_flag = " + str);

    }

    public void updateCartCount(int str, String itemId) {
        Log.d(TAG, "onCreate cart : " + CREATE_TABLE_CART);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("Count", str);
        db.execSQL("update cart set Count = " + str + " where  ItemId = '" + itemId + "'");
        Log.d(TAG, "updateCartCount: " + "update cart set Count = " + str + " where  ItemId = '" + itemId + "'");

    }

    public String getCartCount(String id) {

        String cardCount = null;
        SQLiteDatabase db = this.getReadableDatabase();

        String testQuery = "select ItemId, Count from cart;";
        Cursor testCursor = db.rawQuery(testQuery, null);
        if (testCursor.getCount() > 0) {
            testCursor.moveToFirst();
            do {
              /*  Log.d("sangharsha", "ItemId: " + testCursor.getString(testCursor.getColumnIndex("ItemId")));
                Log.d("sangharsha", "Count: " + testCursor.getString(testCursor.getColumnIndex("Count")));*/
            } while (testCursor.moveToNext());

        }

        String selectQuery = "select Count from  cart where ItemId = '" + id + "'";
        Log.d(new Utility().TAG, "get Cart Count: " + selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            cardCount = cursor.getString(cursor.getColumnIndex("Count"));

            return cardCount;
        }
        return String.valueOf(0);

    }

    public void updateFlagPin(String str) {
//        String strFilter = "reg_flag=" + KEY_ACT_FLAG;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("pin_flag", str);
//        return db.update(TABLE_REGISTRATION_FLAG, value, "reg_flag" + " = ?", new String[]{str});
        db.execSQL("update customer_data set pin_flag = " + str);


    }

    public void updatePin_l(String str) {
//        String strFilter = "reg_flag=" + KEY_ACT_FLAG;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("l_pin", str);
        db.execSQL("update customer_data set l_pin = " + str);


    }

    public void updateRandom_pin(String str) {
//        String strFilter = "reg_flag=" + KEY_ACT_FLAG;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("reset_pin", "'" + str + "'");
//        return db.update(TABLE_REGISTRATION_FLAG, value, "reg_flag" + " = ?", new String[]{str});
        db.execSQL("update customer_data set reset_pin = " + str);

    }

    public void updatePin_retrive(String str) {
//        String strFilter = "reg_flag=" + KEY_ACT_FLAG;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("pin_retries_count", str);
//        return db.update(TABLE_REGISTRATION_FLAG, value, "reg_flag" + " = ?", new String[]{str});
        db.execSQL("update customer_data set pin_retries_count = " + str);

    }

    public String getPin_retrive() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select pin_retries_count from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                s = cursor.getString(cursor.getColumnIndex("pin_retries_count"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public String getRandom() {
        String s = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "select reset_pin from  customer_data";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                s = cursor.getString(cursor.getColumnIndex("reset_pin"));
            } while (cursor.moveToNext());
        }
        return s;
    }

    public void updateProfileFlag(String str, String _id) {
//        String strFilter = "reg_flag=" + KEY_ACT_FLAG;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("act_flag", str);
//        return db.update(TABLE_REGISTRATION_FLAG, value, "reg_flag" + " = ?", new String[]{str});
        db.execSQL("update profile set act_flag = " + str + " where _id = " + _id);

    }

    public void updateTypechecking(String str, String _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("card_type", str);
        sqLiteDatabase.execSQL("update profile set card_type = " + str + " where _id = " + _id);
    }

    public String getCardType(String _id) {
        String cardType = null;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = "select card_type from profile where _id = " + _id;

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                cardType = cursor.getString(cursor.getColumnIndexOrThrow("card_type"));
            } while (cursor.moveToNext());
        }
        return cardType;
    }


    public void deletPinIteam(String _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        sqLiteDatabase.execSQL("delete from pin where _id = '" + _id + "'");
    }

    public void deletquickIteam(String _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from quick_pay where _id = '" + _id + "'");
    }

    public void deletCard(long _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String string = String.valueOf(_id);
        sqLiteDatabase.execSQL("delete from profile where _id = '" + string + "'");
        Log.d(Utility.TAG, "delet card : " + "delete from profile where _id = '" + string + "'");
    }

    public void deletCart(String _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from cart where itemId = '" + _id + "'");
        Log.d(TAG, "deletCart: " + "delete from cart where itemId = '" + _id + "'");
    }

    public double sumSubTotal() {
        double s;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = " select sum (Rate) from cart ";
        Cursor cursor = db.rawQuery(selectQuery, null);
        s = cursor.getDouble(cursor.getColumnIndex("Rate"));
        Log.d("dinesh", "sumSubTotal: " + s);
        return s;
    }

    public void deletCartAll() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from cart");

    }

    public void deletEmptrresponse(String _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from profile where _id = '" + _id + "'");
    }


    public void deletNotificationIteam(String _id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        sqLiteDatabase.execSQL("delete from notification where _id = '" + _id + "'");
    }

    public void deleteFriendList(String enc_cust_id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from friend_list where enc_cust_id = " + enc_cust_id + "");
    }


    public void deletPin() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from pin ");
    }

    public void deletQuickpay() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from quick_pay ");
    }


    public double getSubTotal() {
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "select sum(Rate * Count) as sub_total from Cart";
        Cursor cursor = database.rawQuery(sql, null);
        cursor.moveToFirst();
        double subTotal = cursor.getDouble(cursor.getColumnIndex("sub_total"));
        return subTotal;
    }

    // Getting all pressure list
    public ArrayList<MenuListModel> getAllReminders() {
        ArrayList<MenuListModel> reminderList = new ArrayList<>();

        // Select all Query
        String selectQuery = "SELECT * FROM " + TABLE_CART;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MenuListModel reminder = new MenuListModel();
                reminder.setItemName(cursor.getString(cursor.getColumnIndexOrThrow(ITEM_NAME)));
                reminder.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));


                reminder.setImageUrl(cursor.getString(cursor.getColumnIndexOrThrow(IMAGE_URL)));
                reminder.setCategoryId(cursor.getString(cursor.getColumnIndexOrThrow(CATEGORY_ID)));

                reminder.setItemId(cursor.getString(cursor.getColumnIndexOrThrow(ITEM_ID)));
                reminder.setRate(cursor.getDouble(cursor.getColumnIndexOrThrow(RATE)));
                reminder.setCount(cursor.getInt(cursor.getColumnIndexOrThrow(COUNT)));

                // Adding Pressure to list
                reminderList.add(reminder);
            } while (cursor.moveToNext());
        }
        return reminderList;
    }

    //insert chat list
    public long insertChatListData(ChatModelList chatModelList) {
        ContentValues values = new ContentValues();
        values.put(KEY_CHAT_CELL, chatModelList.getCell_no_chat());
        values.put(KEY_CHAT_NAME, chatModelList.getName_chat());
        values.put(KEY_CHAT_CUST_ID, chatModelList.getCust_id_friend_chat());
        values.put(KEY_CHAT_URL, chatModelList.getImg_url_chat());
        values.put(KEY_CHAT_DATE, chatModelList.getDate_chat());
        values.put(KEY_CHAT_MSG, chatModelList.getMsg_chat());
        values.put(TOTAL_UNREAD, chatModelList.getTotalUnreadMsg());
        SQLiteDatabase db = this.getReadableDatabase();
        long insertId = db.insert(TABLE_CHAT_LIST, null,
                values);
        return insertId;
    }


    //insert conversation list
    public long insertConversationData(ConversationModule chatModelList) {
        ContentValues values = new ContentValues();
        values.put(KEY_MAPPING, chatModelList.getUrl());
        values.put(KEY_CONVERSATION, chatModelList.getMsg());
        values.put(KEY_FLAG, chatModelList.getFlag());
        values.put(KEY_DATE, chatModelList.getDate());
        values.put(KEY_SEND_FAIL, chatModelList.getSend_fail());
        values.put(KEY_MESSAGE_ID, chatModelList.getMessageId());
        values.put(KEY_IMG_MESSAGE, chatModelList.getImage_Message());
        values.put(KEY_ISIMAGE, chatModelList.getIsImage());
        SQLiteDatabase db = this.getReadableDatabase();
        long insertId = db.insert(TABLE_CONVERSATION, null,
                values);
        return insertId;
    }

    // Getting all pressure list
    public ArrayList<ChatModelList> getAllChatList(int syst) {
        ArrayList<ChatModelList> reminderList = new ArrayList<>();

        // Select all Query
        String selectQuery = "SELECT * FROM " + TABLE_CHAT_LIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ChatModelList reminder = new ChatModelList();
                reminder.setCell_no_chat(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CHAT_CELL)));
                reminder.setName_chat(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CHAT_NAME)));


                reminder.setCust_id_friend_chat(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CHAT_CUST_ID)));
                reminder.setImg_url_chat(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CHAT_URL)));

                reminder.setDate_chat(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CHAT_DATE)));
                reminder.setMsg_chat(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CHAT_MSG)));
                reminder.setTotalUnreadMsg(cursor.getString(cursor.getColumnIndexOrThrow(TOTAL_UNREAD)));

                reminder.set_id(cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID_PROFILELIST)));


                // Adding Pressure to list
                reminderList.add(reminder);
            } while (cursor.moveToNext());
        }

        return reminderList;
    }


    public void updateTotalUnreadMsg(String value) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.execSQL("update chat_list set TotalUnreadMsg = '" + value + "'");

    }

    public void updateTotalUnreadMsgCustomer(String value, int id) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.execSQL("update chat_list set TotalUnreadMsg = '" + value + "' where _id = '" + id + "'");

    }

    public void updateLastyMsg(String id, String value) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.execSQL("update chat_list set msg_chat = ' " + value + " ' where cust_id_friend_chat = '" + id + "'");
        Log.d(TAG, "updateLastyMsg: " + " update chat_list set msg_chat = '" + value + "' where _id = '" + id + "'");
    }

    public void updateLastyDate(String id, String value) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.execSQL("update chat_list set date_chat = '" + value + "' where cust_id_friend_chat = '" + id + "'");
        Log.d(TAG, "updateLastyMsg: " + " update chat_list set date_chat = '" + value + "' where _id = '" + id + "'");
    }


    // Getting all pressure list
    public ArrayList<ConversationModule> getAllConversation(String id) {
        ArrayList<ConversationModule> reminderList = new ArrayList<>();

        // Select all Query
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATION + " where " + KEY_MAPPING + " = '" + id + "' ORDER BY _id DESC ";
        Log.d(TAG, "getAllConversation: " + selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ConversationModule reminder = new ConversationModule();
                reminder.set_id(cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID_PROFILELIST)));
                reminder.setUrl(cursor.getString(cursor.getColumnIndexOrThrow(KEY_MAPPING)));
                reminder.setMsg(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CONVERSATION)));


                reminder.setFlag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_FLAG)));
                reminder.setDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_DATE)));

                reminder.setMessageId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_MESSAGE_ID)));
                reminder.setSend_fail(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SEND_FAIL)));

                reminder.setImage_Message(cursor.getString(cursor.getColumnIndexOrThrow(KEY_IMG_MESSAGE)));
                reminder.setIsImage(cursor.getString(cursor.getColumnIndexOrThrow(KEY_ISIMAGE)));
                // Adding Pressure to list
                reminderList.add(reminder);
            } while (cursor.moveToNext());
        }
        return reminderList;
    }

    public void updateSendFail(String id, String value) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.execSQL(" update conversation set send_fail = '" + value + "' where conversation_date = '" + id + "'");
    }

    public void updateMsgId(String id, String value) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.execSQL(" update conversation set messageId = '" + value + "' where _id = '" + id + "'");
    }


    public int isAlreadyInChatList(String enc_cust_id, String phone_no) {
        String selectQuery = " select 1 from chat_list where cust_id_friend_chat = '" + enc_cust_id + "' and cell_no_chat = '" + phone_no + "'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();
        return reg_count;
    }

    public int isAlreadyInChatListcontact(String phone_no) {
        String selectQuery = " select 1 from chat_list where cell_no_chat = '" + phone_no + "'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();
        return reg_count;
    }

    public int isAlreadyInChatListcontactCustId(String cust_id) {
        String selectQuery = " select 1 from chat_list where cust_id_friend_chat = '" + cust_id + "'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();
        return reg_count;
    }

    public void deletChatList(String cust_id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(" delete from chat_list where cust_id_friend_chat = '" + cust_id + "'");
        Log.d(TAG, "deletChatList: " + " delete from chat_list where cust_id_friend_chat = '" + cust_id + "'");

    }

    public void deletChatConversation(String cust_id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(" delete from conversation where conversation_mapping = '" + cust_id + "'");
        Log.d(TAG, "deletChatList: " + " delete from conversation where conversation_mapping = '" + cust_id + "'");
    }

    public void deletChatConversationItem(String messageId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(" delete from conversation where messageId = '" + messageId + "'");
        Log.d(TAG, "deletChatList: " + " delete from conversation where messageId = '" + messageId + "'");
    }

    public int isAlreadyInChatListMsgId(String messageId) {
        String selectQuery = " select 1 from conversation where messageId = '" + messageId + "'";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        int reg_count = cursor.getCount();
        cursor.close();
        sqLiteDatabase.close();
        return reg_count;
    }

    public int getPosition(String _id) {
        int cardType = 0;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectQuery = " select _id from chat_list where cell_no_chat = " + _id;

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                cardType = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
            } while (cursor.moveToNext());
        }
        return cardType;
    }

    public ArrayList<TrafficModel> getAllTrafficLog() {
        ArrayList<TrafficModel> trafficModels = new ArrayList<>();
        String selectQuery = " select * from " + TABLE_TRAFFIC;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TrafficModel trafficModel = new TrafficModel();
                trafficModel.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME_FINEED)));
                trafficModel.setChit_id(cursor.getString(cursor.getColumnIndexOrThrow(CHEAT_ID)));
                trafficModel.setIdentification_id(cursor.getString(cursor.getColumnIndexOrThrow(IDENTIFICATION_NUMBER)));
                trafficModel.setCause(cursor.getString(cursor.getColumnIndexOrThrow(CAUSE)));
                trafficModel.setAmount(cursor.getString(cursor.getColumnIndexOrThrow(FINE_AMOUNT)));
                trafficModel.setCrrn(cursor.getString(cursor.getColumnIndexOrThrow(CRRN_TAFFIC)));
                trafficModel.setStan(cursor.getString(cursor.getColumnIndexOrThrow(STAN_TRAFFIC)));
                trafficModels.add(trafficModel);
            } while (cursor.moveToNext());
        }
        return trafficModels;
    }


}
