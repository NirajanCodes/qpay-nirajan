package net.qpaysolutions.QPay.History.historyFragment;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.History.historyModel.Child;
import net.qpaysolutions.QPay.History.historyAdapter.ExpandListAdapter;
import net.qpaysolutions.QPay.History.historyModel.Group;
import net.qpaysolutions.QPay.History.historyInterface.OnLoadMoreListener;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.History.historyInterface.HistoryInterface;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 9/5/16.
 */
public class TotalReceivedFragment extends Fragment {
    HistoryInterface historyInterface;
    LinearLayout empty_history;
    private QPayCustomerDatabase sctDatabaseHelper;
    String cust_id, App_Id;
    private ExpandListAdapter ExpAdapter;
    private ExpandableListView ExpandList;
    private int lastExpandedPosition = -1;

    private ArrayList<Group> list;

    private int page = 1;
    private String trnType = "TR";
    private boolean isAllDataLoaded = false;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.activity_main_history, container, false);
        ExpandList = (ExpandableListView) v.findViewById(R.id.exp_list);
        empty_history = (LinearLayout) v.findViewById(R.id.empty_history);
        sctDatabaseHelper = new QPayCustomerDatabase(getActivity());
        sctDatabaseHelper.getReadableDatabase();
        cust_id = sctDatabaseHelper.getCustomerID();
        App_Id = sctDatabaseHelper.getKeyAppId();
        try {
            list = historyInterface.getTotalReceive();
            if (list.size() > 0) {
                empty_history.setVisibility(View.GONE);
                ExpAdapter = new ExpandListAdapter(
                        getActivity(), trnType, list, ExpandList, onLoadMoreListenerListener);
                ExpandList.setAdapter(ExpAdapter);
                ExpandList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (lastExpandedPosition != -1
                                && groupPosition != lastExpandedPosition) {
                            ExpandList.collapseGroup(lastExpandedPosition);
                        }
                        lastExpandedPosition = groupPosition;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setInterface(HistoryInterface anInterface) {
        this.historyInterface = anInterface;
    }

    public class HistoryDetails extends AsyncTask<String, String, String> {
        String SentAmount, ReceivedAmount;
        private String App_Id;


        public HistoryDetails() {
            page += 1;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            App_Id = sctDatabaseHelper.getKeyAppId();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject_success = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result = null;
            Log.d("sangharsha", "result from history details : " + result);
            try {
                jsonObject_success.put("AppId", App_Id);
                jsonObject_success.put("id", cust_id);
                jsonObject_success.put("Page", page);
                jsonObject_success.put("trnType", "TR");

                result = networkAPI.sendHTTPData(Constants.HISTORY_RESULT, jsonObject_success);
                Log.d("dinesh", "result from history details : " + result);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String status = jsonObject.getString("status");
                String message = jsonObject.getString("message");
                Boolean success = jsonObject.getBoolean("success");
                Log.d("sangharsha", "onPostExecute: " + list.toString());
                list.remove(list.size() - 1);
                if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("200") && success == true) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    JSONObject CustomersTransactionTotal = data.getJSONObject("CustomersTransactionTotal");
                    SentAmount = CustomersTransactionTotal.getString("SentAmount");
                    ReceivedAmount = CustomersTransactionTotal.getString("ReceivedAmount");
                    Log.d(Utility.TAG, "history received : " + result);
                    JSONArray jsonArray = data.getJSONArray("History");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ArrayList<Child> ch_list = new ArrayList<>();
                        ArrayList<Child> ch_list_tot = new ArrayList<>();
                        Group group = new Group();
                        Child child = new Child();
                        JSONObject all_history = jsonArray.getJSONObject(i);
                        String Txn_Time = all_history.getString("Date");
                        String Txn_Amount = Decimalformate.decimalFormate(all_history.getString("TranAmount"));
                        String STAN = all_history.getString("STAN");
                        String CRRN = all_history.getString("CRRN");
                        String Merchant_Name = all_history.getString("PartyName");
                        String Txn_Type = all_history.getString("TranType");
                        String PaymentMethod = all_history.getString("PaymentMethod");
                        String Address = all_history.getString("Address");
                        group.setRebate(Decimalformate.decimalFormate(all_history.getString("Rebate")));
                        group.setMerch_name(Merchant_Name);
                        group.setTxn_amt(Txn_Amount);
                        group.setTxn_date(Txn_Time);
                        child.setAddress(Address);
                        child.setTxn_Type(Txn_Type);
                        child.setCity(STAN);
                        child.setPhone(CRRN);
                        child.setTxn_amt(Txn_Amount);
                        child.setPaymentMethod(PaymentMethod);
                        child.setMerch_name(Merchant_Name);
                        group.setItems(ch_list);
                        ch_list.add(child);
//                            list.add(group);
                        list.add(group);
                    }
                    if (list.size() / Constants.OFFSET == 0) {
                        isAllDataLoaded = true;
                    }
                } else if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("200") && success == false) {
                    isAllDataLoaded = true;
                }
                ExpAdapter.notifyDataSetChanged();
                ExpAdapter.setLoaded();
                ExpAdapter.setIsAllLoaded();
                Log.d("sangharsha", "onPostExecute: 1" + list.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    OnLoadMoreListener onLoadMoreListenerListener = new OnLoadMoreListenerImpl() {
        @Override
        public void onLoadMore() {
            if (list.size() < Constants.OFFSET) {
                isAllDataLoaded = true;
            }
            if (ExpAdapter != null && !isAllDataLoaded) {
                list.add(null);
                ExpAdapter.notifyDataSetChanged();
                new HistoryDetails().execute();
            }
        }
    };

    abstract class OnLoadMoreListenerImpl implements OnLoadMoreListener {

    }
}
