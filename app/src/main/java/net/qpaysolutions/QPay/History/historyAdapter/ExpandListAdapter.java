package net.qpaysolutions.QPay.History.historyAdapter;

/**
 * Created by root on 5/24/16.
 */

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.CustumClasses.TextDrawable;
import net.qpaysolutions.QPay.History.historyInterface.OnLoadMoreListener;
import net.qpaysolutions.QPay.History.historyModel.Child;
import net.qpaysolutions.QPay.History.historyModel.Group;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;


public class ExpandListAdapter extends BaseExpandableListAdapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context context;
    private ArrayList<Group> groups;

    private HistoryScrollListener historyScrollListener;
    private int visibleThreshold = 20;
    private int currentPage = 0;
    private int previousTotal = 20;
    private boolean loading = false;
    private boolean isallDataLoaded = false;
    private OnLoadMoreListener onLoadMoreListener;
    private int preLast;
    private String trnType;

    public ExpandListAdapter(Context context, String trnType, ArrayList<Group> groups, ExpandableListView expandableListView, OnLoadMoreListener onLoadMoreListener) {
        this.context = context;
        this.groups = groups;
        this.onLoadMoreListener = onLoadMoreListener;
        historyScrollListener = new HistoryScrollListener();
        this.trnType = trnType;
        expandableListView.setOnScrollListener(historyScrollListener);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<Child> chList = groups.get(groupPosition).getItems();
        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        int type = getGroupType(groupPosition);
        if (type == VIEW_ITEM) {
            Child child = (Child) getChild(groupPosition, childPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) context
                        .getSystemService(context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.child_iteam_final, null);
            }
            TextView address = (TextView) convertView.findViewById(R.id.address);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView paymet_method = (TextView) convertView.findViewById(R.id.gatwy);
            TextView amount = (TextView) convertView.findViewById(R.id.amount);
            TextView transc_id = (TextView) convertView.findViewById(R.id.trnsc_id);
            address.setText(child.getAddress().toString());
            name.setText(/*paymentHistory.titleize(*/child.getMerch_name());

            paymet_method.setText(child.getPaymentMethod());
            amount.setText(child.getTxn_amt());
            transc_id.setText(child.getPhone());

            return convertView;
        }
        return null;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Child> chList = groups.get(groupPosition).getItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final Group group = (Group) getGroup(groupPosition);
        int type = getGroupType(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            if (type == VIEW_ITEM) {
                convertView = inf.inflate(R.layout.group_iteam_final, parent, false);
            } else {
                convertView = inf.inflate(R.layout.progress_item, parent, false);
            }
        }
        if (type == VIEW_ITEM) {
            ImageView tv = (ImageView) convertView.findViewById(R.id.trns);
            TextView date = (TextView) convertView.findViewById(R.id.dateTime);
            TextView trn_amt = (TextView) convertView.findViewById(R.id.txnmnt);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView indicator = (TextView) convertView.findViewById(R.id.indicator);
            TextView SurchargeRebate = (TextView) convertView.findViewById(R.id.SurchargeRebate);
            TextView Rebate = (TextView) convertView.findViewById(R.id.Rebate);

            try {
                if (Double.parseDouble(group.getSurchargeRebate()) > 0) {

                    SurchargeRebate.setVisibility(View.VISIBLE);
                    SurchargeRebate.setText("Surcharge Rebate : NPR " + group.getSurchargeRebate());
                } else {
                    SurchargeRebate.setVisibility(View.GONE);
                }

                if (Double.parseDouble(group.getRebate()) > 0) {
                    Rebate.setVisibility(View.VISIBLE);
                    Rebate.setText("Cash Back : NPR " + group.getRebate());
                } else {
                    Rebate.setVisibility(View.GONE);
                }

                if (group.getTxn_Type().equals("PU") || group.getTxn_Type().equals("BP") || group.getTxn_Type().equals("TS") || group.getTxn_Type().equals("FTS")) {
                    trn_amt.setTextColor(Color.parseColor("#fd464b"));
                    indicator.setTextColor(Color.parseColor("#fd464b"));
                    indicator.setText("-");
                } else {
                    trn_amt.setTextColor(Color.parseColor("#43A047"));
                    indicator.setTextColor(Color.parseColor("#43A047"));
                    indicator.setText("+");
                }

            TextDrawable drawable;
            if (groupPosition % 6 == 0) {
                drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(group.getMerch_name().charAt(0)), Color.parseColor("#62cc65"));
                tv.setImageDrawable(drawable);
            } else if (groupPosition % 6 == 1) {
                drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(group.getMerch_name().charAt(0)), Color.parseColor("#1898ab"));
                tv.setImageDrawable(drawable);
            } else if (groupPosition % 6 == 2) {
                drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(group.getMerch_name().charAt(0)), Color.parseColor("#a5599d"));
                tv.setImageDrawable(drawable);
            } else if (groupPosition % 6 == 3) {
                drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(group.getMerch_name().charAt(0)), Color.parseColor("#fd464b"));
                tv.setImageDrawable(drawable);
            } else if (groupPosition % 6 == 4) {
                drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(group.getMerch_name().charAt(0)), Color.GRAY);
                tv.setImageDrawable(drawable);
            } else if (groupPosition % 6 == 5) {
                drawable = TextDrawable.builder()
                        .buildRound(String.valueOf(group.getMerch_name().charAt(0)), Color.parseColor("#ff5252"));
                tv.setImageDrawable(drawable);
            }
            name.setText(group.getMerch_name());
            trn_amt.setText(group.getTxn_amt());

            date.setText(group.getTxn_date());
            Log.d(Utility.TAG,"date :  "+date.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (group.getTxn_Type().equals("PU")) {
                    trn_amt.setText(group.getTxn_amt());
                    trn_amt.setTextColor(Color.parseColor("#fd464b"));
                    indicator.setTextColor(Color.parseColor("#fd464b"));
                    indicator.setText("-");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar1);
            progressBar.getIndeterminateDrawable().setColorFilter(0xFF000000, android.graphics.PorterDuff.Mode.MULTIPLY);
            progressBar.setIndeterminate(true);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public int getGroupTypeCount() {
        return 2;
    }

    @Override
    public int getGroupType(int groupPosition) {
        if (groups.get(groupPosition) != null) {
            return VIEW_ITEM;
        } else {
            return VIEW_PROG;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="set Loaded false">
    public void setLoaded() {
        loading = false;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="set All Data Loaded">
    public void setIsAllLoaded() {
        isallDataLoaded = true;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getHistroyScrolllistener">
    public HistoryScrollListener getScrollListener() {
        return historyScrollListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Inner Class: HistoryScrollListener">
    public class HistoryScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            Log.d("sangharsha", "onScrollStateChanged: ");
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            final int lastItem = firstVisibleItem + visibleItemCount;
            if (lastItem == totalItemCount) {
                int lastVisibleItem = view.getLastVisiblePosition();
                Log.d("sangharsha", "onScroll: ");
                if (preLast != lastItem) {
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleItemCount - 1) && !isallDataLoaded) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                    }
                    preLast = lastItem;
                    loading = true;
                }
            }
        }
    }
    // </editor-fold>
}