package net.qpaysolutions.QPay.History.historyModel;

/**
 * Created by root on 5/24/16.
 */
public class Child {

    private String address;
    private String city;
    private String Txn_Type;
    private String PaymentMethod;
    private String phone;
    String merch_name;
    String txn_amt;

    public String getMerch_name(){
        return merch_name;
    }
    public  void  setMerch_name(String merch_name){
        this.merch_name=merch_name;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTxn_Type() {
        return Txn_Type;
    }

    public void setTxn_Type(String Txn_Type) {
        this.Txn_Type = Txn_Type;
    }

    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String PaymentMethod) {
        this.PaymentMethod = PaymentMethod;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getTxn_amt(){
        return txn_amt;
    }
    public void setTxn_amt(String txn_amt){
        this.txn_amt=txn_amt;
    }

}