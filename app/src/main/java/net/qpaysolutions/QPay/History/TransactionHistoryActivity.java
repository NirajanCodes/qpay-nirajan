package net.qpaysolutions.QPay.History;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.History.historyAPI.GetHistoryAPI;
import net.qpaysolutions.QPay.History.historyFragment.BillPaymentFragmentHistory;
import net.qpaysolutions.QPay.History.historyFragment.FundTransferHistoryFragment;
import net.qpaysolutions.QPay.History.historyFragment.PurchaseHistoryFragment;
import net.qpaysolutions.QPay.History.historyFragment.TotalReceivedFragment;
import net.qpaysolutions.QPay.History.historyFragment.TotalSentFragment;
import net.qpaysolutions.QPay.History.historyInterface.HistoryInterface;
import net.qpaysolutions.QPay.History.historyModel.Group;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryActivity extends AppCompatActivity implements View.OnClickListener, HistoryInterface, DialogInterface {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private QPayProgressDialog progressDialog;
    private LinearLayout progressBarLayout;
    private TextView sent, received;
    private String TAG;
    private LinearLayout receive, send;

    ArrayList<Group> fundTransferList = new ArrayList<>();
    ArrayList<Group> paymentList = new ArrayList<>();
    ArrayList<Group> purchasesList = new ArrayList<>();
    ArrayList<Group> totalSentList = new ArrayList<>();
    ArrayList<Group> totalReceiveList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_main_activity_sliding);
        progressBarLayout = findViewById(R.id.progressLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView title = (TextView) findViewById(R.id.title);
        title.setText("Payment History");
        sent = (TextView) findViewById(R.id.total_out);
        received = (TextView) findViewById(R.id.total_in);
        send = (LinearLayout) findViewById(R.id.sent_deatails);
        receive = (LinearLayout) findViewById(R.id.received_details);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        sent.setOnClickListener(this);
        receive.setOnClickListener(this);
        viewPager.setOffscreenPageLimit(3);
//        progressDialog = new QPayProgressDialog(this);
        new GetHistoryAPI(this).execute();
    }


    public void setupViewPager() {

        Bundle bundle = new Bundle();
        BillPaymentFragmentHistory billPaymentFragmentHistory = new BillPaymentFragmentHistory();
        billPaymentFragmentHistory.setInterface(this);
        PurchaseHistoryFragment purchaseHistoryFragment = new PurchaseHistoryFragment();
        purchaseHistoryFragment.setInterface(this);
        FundTransferHistoryFragment fundTransferHistoryFragment = new FundTransferHistoryFragment();
        fundTransferHistoryFragment.setInterface(this);
        TotalReceivedFragment totalReceivedFragment = new TotalReceivedFragment();
        totalReceivedFragment.setInterface(this);
        TotalSentFragment totalSentFragment = new TotalSentFragment();
        totalSentFragment.setInterface(this);
        purchaseHistoryFragment.setArguments(bundle);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(purchaseHistoryFragment, "purchase");
        adapter.addFragment(billPaymentFragmentHistory, "Bill payment");
        adapter.addFragment(fundTransferHistoryFragment, "Transfer");
        adapter.addFragment(totalReceivedFragment, "Total Received");
        adapter.addFragment(totalSentFragment, "Total Sent");
        viewPager.setAdapter(adapter);

    }

    public void setAmount(String SentAmount, String ReceivedAmount) {
        if (SentAmount == null || ReceivedAmount == null || SentAmount.equals("null") || ReceivedAmount.equals("null")) {
            sent.setText("NPR 0.00");
            received.setText("NPR 0.00");
        } else {
            sent.setText("NPR " + SentAmount);
            received.setText("NPR " + ReceivedAmount);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sent_deatails:
                Toast.makeText(getApplicationContext(), "not clicked", Toast.LENGTH_LONG).show();
                break;
            case R.id.received_details:
                break;
        }
    }

    @Override
    public ArrayList<Group> getBillPayment() {
        return paymentList;
    }

    @Override
    public ArrayList<Group> getPurchase() {
        return purchasesList;
    }

    @Override
    public ArrayList<Group> getFundTransfer() {
        return fundTransferList;

    }

    @Override
    public ArrayList<Group> getTotalReceive() {
        return totalReceiveList;
    }

    @Override
    public ArrayList<Group> getTotalSent() {
        return totalSentList;
    }

    @Override
    public void setBillPayment(ArrayList<Group> payment) {

        paymentList = payment;

    }

    @Override
    public void setPurchase(ArrayList<Group> purchase) {
        purchasesList = purchase;

    }

    @Override
    public void setFundTransfer(ArrayList<Group> fundTransfer) {
        fundTransferList = fundTransfer;
    }

    @Override
    public void setTotalReceive(ArrayList<Group> totalReceive) {
        totalReceiveList = totalReceive;
    }

    @Override
    public void setTotalSent(ArrayList<Group> totalSent) {
        totalSentList = totalSent;
    }

    @Override
    public void showDialog() {
        progressBarLayout.setVisibility(View.VISIBLE);
//        progressDialog.show();
    }

    @Override
    public void hideDialog() {
        progressBarLayout.setVisibility(View.GONE);
//        progressDialog.dismiss();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_statement:
                new StatementDialog().statementDialog(TransactionHistoryActivity.this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }
}