package net.qpaysolutions.QPay.History.historyInterface;

import net.qpaysolutions.QPay.History.historyModel.Group;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 8/31/16.
 */
public interface HistoryInterface {
    ArrayList<Group> getBillPayment();

    ArrayList<Group> getPurchase();

    ArrayList<Group> getFundTransfer();

    ArrayList<Group> getTotalReceive();

    ArrayList<Group> getTotalSent();

    void setBillPayment(ArrayList<Group> payment);

    void setPurchase(ArrayList<Group> purchase);

    void setFundTransfer(ArrayList<Group> fundTransfer);

    void setTotalReceive(ArrayList<Group> totalReceive);

    void setTotalSent(ArrayList<Group> totalSent);

}
