package net.qpaysolutions.QPay.History.historyFragment;

/**
 * Created by deadlydragger on 6/24/16.
 */

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.History.historyModel.Child;
import net.qpaysolutions.QPay.History.historyAdapter.ExpandListAdapter;
import net.qpaysolutions.QPay.History.historyModel.Group;
import net.qpaysolutions.QPay.History.historyInterface.OnLoadMoreListener;
import net.qpaysolutions.QPay.History.TransactionHistoryActivity;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.History.historyInterface.HistoryInterface;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FundTransferHistoryFragment extends Fragment {
    private ExpandListAdapter ExpAdapter;
    private ExpandableListView ExpandList;
    ArrayList<Group> list;
    String url;
    ProgressDialog pDialog;
    private QPayCustomerDatabase sctDatabaseHelper;
    private Cursor cursor;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    String cust_id, App_Id;
    String TAG;
    private ProgressDialog progressDialog;
    ArrayList<Group> payment = new ArrayList<Group>();
    ArrayList<Group> purchases = new ArrayList<Group>();

    HistoryInterface historyInterface;
    LinearLayout empty_history;
    TransactionHistoryActivity transactionHistoryActivity;

    private int page = 1;
    private String trnType = "FTS";
    private boolean isAllDataLoaded = false;

    public FundTransferHistoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.activity_main_history, container, false);
        ExpandList = (ExpandableListView) v.findViewById(R.id.exp_list);
        empty_history = (LinearLayout) v.findViewById(R.id.empty_history);
        sctDatabaseHelper = new QPayCustomerDatabase(getActivity());
        sctDatabaseHelper.getReadableDatabase();
        cust_id = sctDatabaseHelper.getCustomerID();
        App_Id = sctDatabaseHelper.getKeyAppId();
        try {
            list = historyInterface.getFundTransfer();
            if (list.size() > 0) {
                empty_history.setVisibility(View.GONE);
                ExpAdapter = new ExpandListAdapter(
                        getActivity(), trnType, list, ExpandList, onLoadMoreListenerListener);
                ExpandList.setAdapter(ExpAdapter);
                ExpandList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                    private int lastExpandedPosition = -1;
                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (lastExpandedPosition != -1
                                && groupPosition != lastExpandedPosition) {
                            ExpandList.collapseGroup(lastExpandedPosition);

                        }
                        lastExpandedPosition = groupPosition;

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }
    public void setInterface(HistoryInterface anInterface) {
        this.historyInterface = anInterface;
    }


    public class HistoryDetails extends AsyncTask<String, String, String> {
        String SentAmount, ReceivedAmount;
        private String App_Id;


        public HistoryDetails(){
            page += 1;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            App_Id=sctDatabaseHelper.getKeyAppId();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject_success = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result=null;
            Log.d("sangharsha", "result from history details : " + result);
            try {
                jsonObject_success.put("AppId",App_Id);
                jsonObject_success.put("id",cust_id);
                jsonObject_success.put("Page",page);
                jsonObject_success.put("trnType",trnType);

                result = networkAPI.sendHTTPData(Constants.HISTORY_RESULT,jsonObject_success);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String status = jsonObject.getString("status");
                String message = jsonObject.getString("message");
                Boolean success = jsonObject.getBoolean("success");
                Log.d("sangharsha", "onPostExecute: " + list.toString());
                list.remove(list.size() - 1);
                if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("200") && success == true) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    JSONObject CustomersTransactionTotal = data.getJSONObject("CustomersTransactionTotal");
                    SentAmount = CustomersTransactionTotal.getString("SentAmount");
                    ReceivedAmount = CustomersTransactionTotal.getString("ReceivedAmount");
                    Log.d(Utility.TAG,"history received : "+ result);
                    JSONArray jsonArray = data.getJSONArray("History");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ArrayList<Child> ch_list = new ArrayList<>();
                        ArrayList<Child> ch_list_tot = new ArrayList<>();
                        Group group = new Group();
                        Child child = new Child();
                        JSONObject all_history = jsonArray.getJSONObject(i);
                        String Txn_Time = all_history.getString("Date");
                        String Txn_Amount =new Decimalformate().decimalFormate( all_history.getString("TranAmount"));
                        String STAN = all_history.getString("STAN");
                        String CRRN = all_history.getString("CRRN");
                        String Merchant_Name = all_history.getString("PartyName");
                        String Txn_Type = all_history.getString("TranType");
                        String PaymentMethod = all_history.getString("PaymentMethod");
                        String Address = all_history.getString("Address");
                        group.setRebate(new Decimalformate().decimalFormate(all_history.getString("Rebate")));

                        group.setMerch_name(Merchant_Name);
                        group.setTxn_amt(Txn_Amount);
                        group.setTxn_date(Txn_Time);
                        child.setAddress(Address);
                        child.setTxn_Type(Txn_Type);
                        child.setCity(STAN);
                        child.setPhone(CRRN);
                        child.setTxn_amt(Txn_Amount);
                        child.setPaymentMethod(PaymentMethod);
                        child.setMerch_name(Merchant_Name);
                        group.setItems(ch_list);
                        ch_list.add(child);
//                            list.add(group);
                        list.add(group);
                    }
                    if(list.size() / Constants.OFFSET == 0){
                        isAllDataLoaded = true;
                    }
                }else if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("200") && success == false){
                    isAllDataLoaded = true;
                }
                ExpAdapter.notifyDataSetChanged();
                ExpAdapter.setLoaded();
                ExpAdapter.setIsAllLoaded();
                Log.d("sangharsha", "onPostExecute: 1" + list.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    OnLoadMoreListener onLoadMoreListenerListener = new OnLoadMoreListenerImpl() {
        @Override
        public void onLoadMore() {
            if(list.size() < Constants.OFFSET){
                isAllDataLoaded = true;
            }
            if(ExpAdapter != null && !isAllDataLoaded) {
                list.add(null);
                ExpAdapter.notifyDataSetChanged();
                new HistoryDetails().execute();
            }
        }
    };

    abstract class OnLoadMoreListenerImpl implements OnLoadMoreListener {

    }

}