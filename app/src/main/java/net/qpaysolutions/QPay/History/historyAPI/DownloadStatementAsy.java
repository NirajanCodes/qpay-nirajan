package net.qpaysolutions.QPay.History.historyAPI;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import androidx.core.content.FileProvider;

import android.util.Log;
import android.widget.Toast;

import net.qpaysolutions.QPay.History.TransactionHistoryActivity;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by deadlydragger on 1/30/18.
 */

public class DownloadStatementAsy extends AsyncTask<Void, String, String> {
    private String StartDate, EndDate;
    private TransactionHistoryActivity context;

    public DownloadStatementAsy(String startDate, String endDate, TransactionHistoryActivity context) {
        this.StartDate = startDate;
        this.EndDate = endDate;
        this.context = context;
    }

    /**
     * Before starting background thread Show Progress Bar Dialog
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        context.showDialog();

    }

    /**
     * Downloading file in background thread
     */
    @Override
    protected String doInBackground(Void... params) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Id", GeneralPref.getCustId());
            jsonObject.put("AppId", GeneralPref.getAppId());
            jsonObject.put("StartDate", StartDate);
            jsonObject.put("EndDate", EndDate);
            Log.d("dinesh", jsonObject.toString());
           NetworkAPI.downloadFileFromeUrl(Constants.STATEMENT, jsonObject, context, "qpay_statement_" + StartDate + "_" + EndDate);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    /**
     * Updating progress bar
     */
    protected void onProgressUpdate(String... progress) {

    }

    /**
     * After completing background task Dismiss the progress dialog
     **/
    @Override
    protected void onPostExecute(String s) {
        context.hideDialog();
        try {
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/qpay/" + "qpay_statement_" + StartDate + "_" + EndDate + ".pdf");
            Uri uri;
            if (Build.VERSION.SDK_INT < 24) {
                uri = Uri.fromFile(file);
            } else {
                uri = Uri.parse(file.getPath());
            }
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".my.package.name.provider", file));
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            context.startActivity(Intent.createChooser(intent, "Open PDF"));
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}

