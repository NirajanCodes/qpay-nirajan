package net.qpaysolutions.QPay.History.historyModel;

/**
 * Created by root on 5/24/16.
 */

import java.util.ArrayList;

public class Group {
    String txn_date;
    String txn_amt;
    String merch_name;
    String Txn_Type;
    String SurchargeRebate, Rebate;

    public String getRebate() {
        return Rebate;
    }

    public void setRebate(String rebate) {
        Rebate = rebate;
    }

    public String getSurchargeRebate() {
        return SurchargeRebate;
    }

    public void setSurchargeRebate(String surchargeRebate) {
        SurchargeRebate = surchargeRebate;
    }

    private ArrayList<Child> Items;

    public String getTxn_date() {
        return txn_date;
    }

    public void setTxn_date(String txn_date) {
        this.txn_date = txn_date;
    }

    public String getTxn_amt() {
        return txn_amt;
    }

    public void setTxn_amt(String txn_amt) {
        this.txn_amt = txn_amt;
    }

    public String getMerch_name() {
        return merch_name;
    }

    public void setMerch_name(String merch_name) {
        this.merch_name = merch_name;
    }

    public ArrayList<Child> getItems() {
        return Items;
    }

    public void setItems(ArrayList<Child> Items) {
        this.Items = Items;
    }

    public String getTxn_Type() {
        return Txn_Type;
    }

    public void setTxn_Type(String Txn_Type) {
        this.Txn_Type = Txn_Type;
    }
}
