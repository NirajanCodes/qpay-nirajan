package net.qpaysolutions.QPay.History.historyAPI;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.History.TransactionHistoryActivity;
import net.qpaysolutions.QPay.History.historyModel.Child;
import net.qpaysolutions.QPay.History.historyModel.Group;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/2/18.
 */

public class GetHistoryAPI extends AsyncTask<String, String, String> {
    private String SentAmount, ReceivedAmount;
    private TransactionHistoryActivity transactionHistoryActivity;

    public GetHistoryAPI(TransactionHistoryActivity transactionHistoryActivity) {
        this.transactionHistoryActivity = transactionHistoryActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        transactionHistoryActivity.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        transactionHistoryActivity.hideDialog();
        try {
            transactionHistoryActivity.setAmount(SentAmount, ReceivedAmount);
            transactionHistoryActivity.setupViewPager();
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    protected String doInBackground(String... params) {
        JSONObject jsonObject_success = new JSONObject();
        String result = null;
        try {
            ArrayList<Group> list = new ArrayList<>();
            ArrayList<Group> payment = new ArrayList<>();
            ArrayList<Group> purchases = new ArrayList<>();
            ArrayList<Group> totalSentList = new ArrayList<>();
            ArrayList<Group> totalReceiveList = new ArrayList<>();

            jsonObject_success.put("AppId", GeneralPref.getAppId());
            jsonObject_success.put("id", GeneralPref.getCustId());
            jsonObject_success.put("Page", "1");
            Log.d(Utility.TAG, "history post : " + jsonObject_success);
            result = NetworkAPI.sendHTTPData(Constants.HISTORY_RESULT, jsonObject_success);
            Log.d(Utility.TAG, "history post : " + result);
            JSONObject jsonObject = new JSONObject(result);
            String status = jsonObject.getString("status");
            String message = jsonObject.getString("message");
            Boolean success = jsonObject.getBoolean("success");
            if (!status.isEmpty() && !status.equals("null") && status != null && status.equals("200") && success == true) {
                JSONObject data = jsonObject.getJSONObject("data");
                JSONObject CustomersTransactionTotal = data.getJSONObject("CustomersTransactionTotal");
                ReceivedAmount = BalancePref.getAvailableAmount();
                SentAmount = new Decimalformate().decimalFormate(CustomersTransactionTotal.getString("Rebate"));
                Log.d(Utility.TAG, "history received : " + result);
                JSONArray jsonArray = data.getJSONArray("History");
                for (int i = 0; i < jsonArray.length(); i++) {
                    ArrayList<Child> ch_list = new ArrayList<>();
                    ArrayList<Child> ch_list_tot = new ArrayList<>();
                    Group group = new Group();
                    Child child = new Child();
                    JSONObject all_history = jsonArray.getJSONObject(i);
                    String Txn_Time = Utility.dateFormatter(all_history.getString("Date"));
                    String Txn_Amount = new Decimalformate().decimalFormate(all_history.getString("TranAmount"));
                    String STAN = all_history.getString("STAN");
                    String CRRN = all_history.getString("CRRN");
                    String Merchant_Name = all_history.getString("PartyName");
                    String Txn_Type = all_history.getString("TranType");
                    String PaymentMethod = all_history.getString("PaymentMethod");
                    String Address = all_history.getString("Address");
                    String SurchargeRebate = new Decimalformate().decimalFormate(all_history.getString("SurchargeRebate"));
                    String Rebate = new Decimalformate().decimalFormate(all_history.getString("Rebate"));
                    if (Txn_Type.equals("PU")) {
                        group.setMerch_name(Merchant_Name);
                        group.setTxn_amt(Txn_Amount);
                        group.setTxn_date(Txn_Time);
                        group.setTxn_Type(Txn_Type);
                        group.setSurchargeRebate(SurchargeRebate);
                        group.setRebate(Rebate);
                        child.setAddress(Address);
                        child.setTxn_Type(Txn_Type);
                        child.setCity(STAN);
                        child.setPhone(CRRN);
                        child.setTxn_amt(Txn_Amount);
                        child.setPaymentMethod(PaymentMethod);
                        child.setMerch_name(Merchant_Name);
                        group.setItems(ch_list);
                        ch_list.add(child);
                        purchases.add(group);


                    } else if (Txn_Type.equals("BP")) {
                        group.setMerch_name(Merchant_Name);
                        group.setTxn_amt(Txn_Amount);
                        group.setTxn_date(Txn_Time);
                        group.setSurchargeRebate(SurchargeRebate);
                        group.setRebate(Rebate);
                        child.setAddress(Address);
                        child.setTxn_Type(Txn_Type);
                        group.setTxn_Type(Txn_Type);
                        child.setCity(STAN);
                        child.setPhone(CRRN);
                        child.setTxn_amt(Txn_Amount);
                        child.setPaymentMethod(PaymentMethod);
                        child.setMerch_name(Merchant_Name);
                        group.setItems(ch_list);
                        ch_list.add(child);
//                            list.add(group);
                        payment.add(group);
                    } else if (Txn_Type.equals("FTR") || Txn_Type.equals("FTS")) {
                        group.setMerch_name(Merchant_Name);
                        group.setTxn_amt(Txn_Amount);
                        group.setTxn_date(Txn_Time);
                        group.setTxn_Type(Txn_Type);
                        group.setSurchargeRebate(SurchargeRebate);
                        group.setRebate(Rebate);
                        child.setAddress(Address);
                        child.setTxn_Type(Txn_Type);
                        child.setCity(STAN);
                        child.setPhone(CRRN);
                        child.setTxn_amt(Txn_Amount);
                        child.setPaymentMethod(PaymentMethod);
                        child.setMerch_name(Merchant_Name);
                        group.setItems(ch_list);
                        ch_list.add(child);
                        list.add(group);
                    }
                    if (Txn_Type.equals("TR")) {
                        group.setMerch_name(Merchant_Name);
                        group.setTxn_amt(Txn_Amount);
                        group.setTxn_date(Txn_Time);
                        group.setTxn_Type(Txn_Type);
                        group.setSurchargeRebate(SurchargeRebate);
                        group.setRebate(Rebate);
                        child.setAddress(Address);
                        child.setTxn_Type(Txn_Type);
                        child.setCity(STAN);
                        child.setPhone(CRRN);
                        child.setTxn_amt(Txn_Amount);
                        child.setPaymentMethod(PaymentMethod);
                        child.setMerch_name(Merchant_Name);
                        group.setItems(ch_list_tot);
                        ch_list_tot.add(child);
                        totalReceiveList.add(group);
                    } else if (Txn_Type.equals("TS")) {
                        group.setMerch_name(Merchant_Name);
                        group.setTxn_amt(Txn_Amount);
                        group.setTxn_date(Txn_Time);
                        group.setTxn_Type(Txn_Type);
                        group.setSurchargeRebate(SurchargeRebate);
                        group.setRebate(Rebate);
                        child.setAddress(Address);
                        child.setTxn_Type(Txn_Type);
                        child.setCity(STAN);
                        child.setPhone(CRRN);
                        child.setTxn_amt(Txn_Amount);
                        child.setPaymentMethod(PaymentMethod);
                        child.setMerch_name(Merchant_Name);
                        group.setItems(ch_list_tot);
                        ch_list_tot.add(child);
                        totalSentList.add(group);
                    }
                }
                transactionHistoryActivity.setFundTransfer(list);
                transactionHistoryActivity.setTotalReceive(totalReceiveList);
                transactionHistoryActivity.setPurchase(purchases);
                transactionHistoryActivity.setBillPayment(payment);
                transactionHistoryActivity.setTotalSent(totalSentList);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}