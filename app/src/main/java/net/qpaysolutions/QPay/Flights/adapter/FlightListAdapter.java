package net.qpaysolutions.QPay.Flights.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Flights.Interface.OnFlightListListener;
import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;


/**
 * Created by CSharp05-user on 14/06/2017.
 */

public class FlightListAdapter extends RecyclerView.Adapter<FlightViewHolder> {

    private Context context;
    private OnFlightListListener onFlightListListener;
    private ArrayList<FlightList> flightList;
    private int type_flight;

    public FlightListAdapter(Context context, OnFlightListListener onFlightListListener, ArrayList<FlightList> flightList, int type_flight) {
        this.context = context;
        this.onFlightListListener = onFlightListListener;
        this.flightList = flightList;
        this.type_flight = type_flight;
    }

    @Override
    public FlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.departure_fragment_view, parent, false);
        return new FlightViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FlightViewHolder holder, final int position) {
        final FlightList list = flightList.get(position);
        bindData(list, holder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFlightListListener.onFlightClicked(list);
            }
        });

        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFlightListListener.onFlightDetails(list);
            }
        });
    }

    public void bindData(FlightList flightList, FlightViewHolder viewHolder) {
        viewHolder.air_line_name.setText(flightList.getAirlineName());
        viewHolder.toFrom.setText(flightList.getDeparture());
        viewHolder.amount.setText("NPR " + flightList.getTotalAmount());
        viewHolder.isRefund.setText(flightList.getRefundable());
        viewHolder.date.setText(flightList.getFlightDate());
        viewHolder.time.setText(Utility.timeFormatter(flightList.getDepartureTime()));
        viewHolder.type_class.setText(flightList.getFlightClass());
        switch (type_flight) {
            case 1:
                viewHolder.type_flight.setText("DEPARTURE");
                break;
            case 2:
                viewHolder.type_flight.setText("ARRIVAL");
                break;
        }

    }

    @Override
    public int getItemCount() {
        return flightList.size();
    }

}
