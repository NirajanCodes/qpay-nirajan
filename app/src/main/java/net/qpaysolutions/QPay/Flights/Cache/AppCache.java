package net.qpaysolutions.QPay.Flights.Cache;

/**
 * Created by CSharp05-user on 12/06/2017.
 */

public class AppCache {

    private static String data;

    public static String getData() {
        return data;
    }

    public static void setData(String data) {
        AppCache.data = data;
    }
}
