package net.qpaysolutions.QPay.Flights.Interface;

import net.qpaysolutions.QPay.Flights.Utility.FlightList;

/**
 * Created by CSharp05-user on 14/06/2017.
 */

public interface OnFlightListListener {
    void onFlightClicked(FlightList position);
    void  onFlightDetails(FlightList flightList);
}
