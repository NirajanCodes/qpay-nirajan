package net.qpaysolutions.QPay.Flights.Utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CSharp05-user on 05/06/2017.
 */

public class Utils {

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd/MM/yyy";
      /*  String outputPattern = "dd MMM yyyy hh:mm a";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String string = null;
        try {
            date = inputFormat.parse(time); //this will return currentTime of given TimeZone
            string = outputFormat.format(date); //this will return local currentTime
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        return time;
    }

    /**
     * Convert String to Calendar Object
     */
    public static Calendar parseStringToCalendar(String dateTime) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MM yyy HH:mm", Locale.ENGLISH);
        try {
            calendar.setTime(simpleDateFormat.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static void toolbarConfiguration(Toolbar toolbar, final AppCompatActivity activity) {
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
                activity.finish();
            }
        });
    }

    public static void customSnackBar(View view, String message) {
        final Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setAction("CLOSE", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        }).setActionTextColor(Color.WHITE);
        snackbar.show();
    }

    public enum State {
        DEPARTURE_STATE,
        ARRIVAL_STATE
    }

    public enum FlightTrip {
        ONE_WAY_TRIP,
        ROUND_WAY_TRIP
    }
    //    public static final String BASE_API_MERCHANT = "https://node.qpaysolutions.net/QPay.svc/";
    public String dateConverter(String mydate){
        SimpleDateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date_set = srcDf.parse(mydate);
            SimpleDateFormat destDf = new SimpleDateFormat("MMM dd, yyyy ");
            mydate = destDf.format(date_set);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mydate;
    }

    public String timeConverter(String mydate){

        SimpleDateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date_set = srcDf.parse(mydate);
            SimpleDateFormat destDf = new SimpleDateFormat(" hh:mm a");
            mydate = destDf.format(date_set);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mydate;
    }
    public void hideSoftkey(View view,Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }
    public static void hideKeyword(Activity context){
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
   /* public static String getDukptCustomerId(String cust_id){
        String encryptedCustomerId = null;
        try {
            encryptedCustomerId = Dukpt.encryptCustomerId(cust_id);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String customerTLV = Dukpt.createTLVCustomerId(encryptedCustomerId);
        return  customerTLV;
    }*/
}
