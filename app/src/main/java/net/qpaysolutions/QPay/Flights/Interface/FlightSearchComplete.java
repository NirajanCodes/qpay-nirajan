package net.qpaysolutions.QPay.Flights.Interface;

/**
 * Created by deadlydragger on 10/13/17.
 */

public interface FlightSearchComplete {
    void onSearchComplete();
    void onSearchFail();
}
