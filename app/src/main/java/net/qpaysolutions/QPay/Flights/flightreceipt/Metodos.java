package net.qpaysolutions.QPay.Flights.flightreceipt;

/**
 * Created by deadlydragger on 12/17/17.
 */

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by hackro on 24/11/15.
 */
public class Metodos {


    public Boolean writeDeparture(String fname, JSONObject fcontent, String type) {
        try {
            String fpath = "/sdcard/" + fname + ".pdf";
            File file = new File(fpath);

            if (!file.exists()) {
                file.createNewFile();
            }
            JSONArray PassengerInfo = fcontent.getJSONArray("PassengerInfo");
            JSONObject departDetails=PassengerInfo.getJSONObject(0);
            Document document = new Document();
            PdfWriter.getInstance(document,
                    new FileOutputStream(file.getAbsoluteFile()));

            document.open();

           /* InputStream ims = Globale.getContext().getAssets().open("qpay_logo.png");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 50, stream);
            Image image = Image.getInstance(stream.toByteArray());


            float scaler = 100;
            image.scalePercent(scaler);

            document.add(image);*/
           document.addTitle("Departure Flight Information ");
            document.add(new Paragraph("Depart : "+departDetails.getString("Sector")));
            document.add(new Paragraph("Flight Number : "+departDetails.getString("FlightNumber")));
            document.add(new Paragraph("Airlines : "+departDetails.getString("AirlineName")));
            document.add(new Paragraph("Departure  Ticket Number : "+departDetails.getString("TicketNumber")));
            if (type.equals("2")){
                JSONObject arrivaltDetails=PassengerInfo.getJSONObject(1);
                document.addTitle("Arrival (Return) Flight Information ");
                document.add(new Paragraph("Depart : "+arrivaltDetails.getString("Sector")));
                document.add(new Paragraph("Flight Number : "+arrivaltDetails.getString("FlightNumber")));
                document.add(new Paragraph("Airlines : "+arrivaltDetails.getString("AirlineName")));
                document.add(new Paragraph("Arrival  Ticket Number : "+arrivaltDetails.getString("TicketNumber")));
            }
            document.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }catch (JSONException e){
            e.printStackTrace();
            return false;
        }
    }


}