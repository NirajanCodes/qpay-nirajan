package net.qpaysolutions.QPay.Flights.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;


import net.qpaysolutions.QPay.Flights.SectorModel;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by QPay on 7/2/2018.
 */

public class DistrictAdapter extends RecyclerView.Adapter<DistrictAdapter.DistrictCustomView> implements Filterable {

    private ArrayList<SectorModel> arrayList;
    private Context context;
    private onDistrictClickListener listener;
    private ArrayList<SectorModel> filteredList;

    public DistrictAdapter(ArrayList<SectorModel> arrayList, Context context, onDistrictClickListener listener) {
        this.arrayList = arrayList;
        this.context = context;
        this.listener = listener;
        this.filteredList = arrayList;
    }

    @NonNull
    @Override
    public DistrictCustomView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DistrictCustomView(LayoutInflater.from(context).inflate(R.layout.item_district, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DistrictCustomView holder, final int position) {
        final SectorModel sectorModel = filteredList.get(position);
        holder.districtName.setText(sectorModel.getName());
//        holder.districtName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onDistrictItemClickListener(filteredL));
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                ArrayList<SectorModel> filterableList = new ArrayList<>();

                if (charString.isEmpty()) {
                    filteredList = arrayList;
                } else {
                    for (SectorModel row : arrayList) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ||row.getId().toLowerCase().contains(charString.toLowerCase())) {
                            filterableList.add(row);
                        }
                    }
                    filteredList = filterableList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<SectorModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    class DistrictCustomView extends RecyclerView.ViewHolder {

        private TextView districtName;
        private LinearLayout districtLayout;

        public DistrictCustomView(View itemView) {
            super(itemView);
            districtLayout = itemView.findViewById(R.id.districtLayout);
            districtName = itemView.findViewById(R.id.districtName);
            districtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDistrictItemClickListener(filteredList.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface onDistrictClickListener {
        void onDistrictItemClickListener(SectorModel sectorModel);
    }
}
