package net.qpaysolutions.QPay.Flights.tabFragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Flights.ConfirmBookingActivity;
import net.qpaysolutions.QPay.Flights.FlightDetailsActivity;
import net.qpaysolutions.QPay.Flights.Interface.OnFlightListListener;
import net.qpaysolutions.QPay.Flights.SelectFlightActivity;
import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Flights.adapter.FlightListAdapter;

import java.util.ArrayList;

/**
 * Created by CSharp05-user on 31/05/2017.
 */

public class ArrivalFragment extends Fragment implements OnFlightListListener {
    private FlightListAdapter flightListAdapter;
    private RecyclerView arrivalRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.arrival_fragment, container, false);
        ((SelectFlightActivity)getActivity()).chat_text.setText("Select Arrival Flight");
        arrivalRecyclerView = (RecyclerView) view.findViewById(R.id.arrival_recyclerView);
        setAdapter();
        return view;
    }
    public void setAdapter() {
            ArrayList<FlightList> flightLists = FlightsUtils.getFlightListIn();
            flightListAdapter = new FlightListAdapter(getContext(),this,flightLists,2);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            arrivalRecyclerView.setLayoutManager(mLayoutManager);
            arrivalRecyclerView.setItemAnimator(new DefaultItemAnimator());
            arrivalRecyclerView.setAdapter(flightListAdapter);
            try {
                ((SelectFlightActivity)getActivity()).setFlightInfo(FlightsUtils.getArrivalPlaceTime());
            }catch (Exception e){
                e.printStackTrace();
            }
    }
    @Override
    public void onFlightClicked(FlightList flightList) {
        FlightsUtils.setArrival(flightList);

        Intent intent = new Intent(getContext(), ConfirmBookingActivity.class);
        startActivity(intent);

    }

    @Override
    public void onFlightDetails(FlightList flightList) {
        FlightsUtils.setArrival(flightList);
        Intent intent = new Intent(getContext(), FlightDetailsActivity.class);
        intent.putExtra("flag","1");
        startActivity(intent);
    }

}
