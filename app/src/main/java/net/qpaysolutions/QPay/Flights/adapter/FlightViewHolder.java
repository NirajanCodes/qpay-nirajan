package net.qpaysolutions.QPay.Flights.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 10/15/17.
 */

public class FlightViewHolder extends RecyclerView.ViewHolder {
    public TextView air_line_name, toFrom, details, amount, isRefund, type_flight, date, type_class, time;

    public FlightViewHolder(View itemView) {
        super(itemView);
        air_line_name = (TextView) itemView.findViewById(R.id.air_line_name);
        toFrom = (TextView) itemView.findViewById(R.id.toFrom);
        details = (TextView) itemView.findViewById(R.id.details);
        amount = (TextView) itemView.findViewById(R.id.amount);
        isRefund = (TextView) itemView.findViewById(R.id.isRefund);
        type_flight = (TextView) itemView.findViewById(R.id.type_flight);
        date = (TextView) itemView.findViewById(R.id.date);
        type_class = (TextView) itemView.findViewById(R.id.type_class);
        time = itemView.findViewById(R.id.time);

    }
}
