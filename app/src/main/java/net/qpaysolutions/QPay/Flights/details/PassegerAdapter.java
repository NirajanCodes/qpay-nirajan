package net.qpaysolutions.QPay.Flights.details;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 10/26/17.
 */

public class PassegerAdapter extends RecyclerView.Adapter<PassegerAdapter.Passennger> {
    ArrayList<PassengerDetailsModel> passengerDetailsModels;

    public PassegerAdapter(ArrayList<PassengerDetailsModel> passengerDetailsModels) {
        this.passengerDetailsModels = passengerDetailsModels;
    }

    @Override
    public Passennger onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.passenger_info_item, parent, false);
        return new Passennger(view);
    }

    @Override
    public void onBindViewHolder(Passennger holder, int position) {
        if (passengerDetailsModels.get(position).getName_middle().equalsIgnoreCase("")) {

            holder.iteam_passenger_name.setText(passengerDetailsModels.get(position).getGender() + " " + passengerDetailsModels.get(position).getName_first() + " " + passengerDetailsModels.get(position).getName_second());

        } else {
            holder.iteam_passenger_name.setText(passengerDetailsModels.get(position).getGender() + " " + passengerDetailsModels.get(position).getName_first() + " " + passengerDetailsModels.get(position).getName_middle() + " " + passengerDetailsModels.get(position).getName_second());

        }
        switch (passengerDetailsModels.get(position).getType()) {
            case 1:
                holder.item_passenger_type.setText("Child");
                break;
            case 2:
                holder.item_passenger_type.setText("Adults");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return passengerDetailsModels.size();
    }

    public class Passennger extends RecyclerView.ViewHolder {
        public TextView iteam_passenger_name, item_passenger_type;

        public Passennger(View itemView) {
            super(itemView);
            iteam_passenger_name = (TextView) itemView.findViewById(R.id.iteam_passenger_name);
            item_passenger_type = (TextView) itemView.findViewById(R.id.item_passenger_type);
        }
    }

}
