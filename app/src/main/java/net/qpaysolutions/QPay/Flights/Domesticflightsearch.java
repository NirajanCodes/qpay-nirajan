package net.qpaysolutions.QPay.Flights;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Flights.Interface.FlightSearchComplete;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.GeneralPref;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 10/12/17.
 */

public class Domesticflightsearch extends AsyncTask<String, String, String> {

    String adult, child, departDate, flightMode, nationality, sectorFrom, sectorTo, returnDate;
    FlightSearchComplete flightSearchComplete;

    public Domesticflightsearch(String adult, String child, String departDate, String flightMode, String nationality, String sectorFrom, String sectorTo, FlightSearchComplete flightSearchComplete, String returnDate) {
        this.adult = adult;
        this.child = child;
        this.departDate = departDate;
        this.flightMode = flightMode;
        this.nationality = nationality;
        this.sectorFrom = sectorFrom;
        this.sectorTo = sectorTo;
        this.flightSearchComplete = flightSearchComplete;
        this.returnDate = returnDate;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("adult", adult);
            jsonObject.put("child", child);
            jsonObject.put("departDate", departDate);
            jsonObject.put("flightMode", flightMode);
            jsonObject.put("nationality", nationality);
            jsonObject.put("sectorFrom", sectorFrom);
            jsonObject.put("sectorTo", sectorTo);
            jsonObject.put("returnDate", returnDate);
            Log.d("dinesh", "doInBackground: " + jsonObject);
            return new NetworkAPI().sendHTTPData(Constants.FLIGHT_SEARCH, jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            Log.d("dinesh", "doInBackground: " + s);
            JSONObject jsonObject = new JSONObject(s);
            boolean success = jsonObject.getBoolean("success");
            if (success) {
                FlightsUtils.setDepartureTimePlace("Departure : " + sectorFrom + "->" + sectorTo + " " + departDate);
                FlightsUtils.setArrivalTimePlace("Departure : " + sectorTo + "->" + sectorFrom + " " + returnDate);

                JSONArray data = jsonObject.getJSONArray("data");
                FlightsUtils.setFlightSearchListOut(data.getJSONObject(0).getJSONArray("Outbound").toString());
                FlightsUtils.setFlightSearchListIn(data.getJSONObject(0).getJSONArray("Inbound").toString());
                int total_pass = Integer.valueOf(adult) + Integer.valueOf(child);
                FlightsUtils.setPassenger(String.valueOf(total_pass));
                FlightsUtils.setAdults(Integer.valueOf(adult));
                FlightsUtils.setChilds(Integer.valueOf(child));
                FlightsUtils.setTripType(flightMode);
                FlightsUtils.from(sectorFrom);
                FlightsUtils.to(sectorTo);
                FlightsUtils.date(departDate);
                flightSearchComplete.onSearchComplete();
            } else {
                flightSearchComplete.onSearchFail();
            }

        } catch (Exception e) {
            e.printStackTrace();
            flightSearchComplete.onSearchFail();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
}
