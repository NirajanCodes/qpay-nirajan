package net.qpaysolutions.QPay.Flights.details;

import java.io.Serializable;

/**
 * Created by deadlydragger on 10/26/17.
 */

public class PassengerDetailsModel implements Serializable {
    String name_first, gender, name_second, name_middle;
    int type;
    boolean isValid;

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getName_second() {
        return name_second;
    }

    public void setName_second(String name_second) {
        this.name_second = name_second;
    }

    public String getName_first() {
        return name_first;
    }

    public void setName_first(String name_first) {
        this.name_first = name_first;
    }

    public void setName_middle(String name_middle) {
        this.name_middle = name_middle;
    }

    public String getName_middle() {
        if (name_middle != null && !name_middle.equalsIgnoreCase("null") && !name_middle.equalsIgnoreCase(" ")) {
            return name_middle;
        }else{
            return "";
        }
    }

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name_first;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name_first = name;
    }

    public int getType() {
        return type;
    }


    public void setType(int type) {
        this.type = type;
    }
}
