package net.qpaysolutions.QPay.Flights;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Utility;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 10/29/17.
 *
 * */

public class ConfirmBookingActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private TextView chat_text;
    private QPayProgressDialog qPayProgressDialog;
    String id_in, id_out;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_flight);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        chat_text = (TextView) toolbar.findViewById(R.id.title);
        chat_text.setText("Flight Confirmation");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Button confirm_booking = (Button) findViewById(R.id.confirm_booking);
        confirm_booking.setOnClickListener(this);
        departureDetails();
        arrivalDetails();

    }

    private void departureDetails() {

        FlightList departureList = FlightsUtils.getDeparture();
        id_out = departureList.getFlightId();
        TextView payment_depart_to = (TextView) findViewById(R.id.payment_depart_to);
        TextView payment_flight_id = (TextView) findViewById(R.id.payment_flight_id);
        TextView payment_date_departure = (TextView) findViewById(R.id.payment_date_departure);
        TextView payment_flight_class = (TextView) findViewById(R.id.payment_flight_class);
        TextView payment_departure_date = (TextView) findViewById(R.id.payment_departure_date);
        TextView payment_departure_land = (TextView) findViewById(R.id.payment_departure_land);
        TextView payment_dep_from = (TextView) findViewById(R.id.payment_dep_from);
        TextView payment_dep_to = (TextView) findViewById(R.id.payment_dep_to);
        TextView payment_airline_name = (TextView) findViewById(R.id.payment_airline_name);
        TextView payment_fund_type = (TextView) findViewById(R.id.payment_fund_type);
        ImageView payment_img_url = (ImageView) findViewById(R.id.payment_img_url);

        payment_depart_to.setText(FlightsUtils.getFROM() + " -> " + FlightsUtils.getTO());
        payment_flight_id.setText("Flight No : " + departureList.getFlightNumber());
        payment_date_departure.setText(FlightsUtils.getDate());
        payment_flight_class.setText("Class " + departureList.getFlightClass());
        payment_departure_date.setText(departureList.getDepartureTime());
        payment_departure_land.setText(departureList.getArrivalTime());
        payment_dep_from.setText(departureList.getDeparture());
        payment_dep_to.setText(departureList.getArrival());
        payment_airline_name.setText(departureList.getAirlineName());
        payment_fund_type.setText(departureList.getRefundable());
        Picasso.get().load(Constants.FLIGHT_IMAGE + departureList.getAirlineCode() + ".png").placeholder(R.drawable.flights).error(R.drawable.flights).into(payment_img_url);
    }

    private void arrivalDetails() {
        FlightList arrivalList = FlightsUtils.getArrival();
        if (FlightsUtils.getTripType().equals("2")) {
            id_in = arrivalList.getFlightId();
            LinearLayout layoutArrival = (LinearLayout) findViewById(R.id.layoutArrival);
            layoutArrival.setVisibility(View.VISIBLE);

            TextView payment_arrival_from = (TextView) findViewById(R.id.payment_arrival_from);
            TextView payment_flight_id_arrival = (TextView) findViewById(R.id.payment_flight_id_arrival);
            TextView payment_date_arrival = (TextView) findViewById(R.id.payment_date_arrival);
            TextView payment_flight_class_arrival = (TextView) findViewById(R.id.payment_flight_class_arrival);
            TextView payment_arrival_date = (TextView) findViewById(R.id.payment_arrival_date);
            TextView payment_arrival_lang = (TextView) findViewById(R.id.payment_arrival_lang);
            TextView payment_arrival = (TextView) findViewById(R.id.payment_arrival);
            TextView payment_arrival_to = (TextView) findViewById(R.id.payment_arrival_to);
            TextView payment_arrival_airlines = (TextView) findViewById(R.id.payment_arrival_airlines);
            TextView payment_arrival_refund = (TextView) findViewById(R.id.payment_arrival_refund);
            ImageView payment_img_url_arrival = (ImageView) findViewById(R.id.payment_img_url_arrival);
            payment_arrival_from.setText(FlightsUtils.getTO() + " -> " + FlightsUtils.getFROM());
            payment_flight_id_arrival.setText("Flight No : " + arrivalList.getFlightNumber());
            payment_date_arrival.setText(FlightsUtils.getDate());
            payment_flight_class_arrival.setText("Class " + arrivalList.getFlightClass());
            payment_arrival_date.setText(arrivalList.getDepartureTime());
            payment_arrival_lang.setText(arrivalList.getArrivalTime());
            payment_arrival.setText(arrivalList.getDeparture());
            payment_arrival_to.setText(arrivalList.getArrival());
            payment_arrival_airlines.setText(arrivalList.getAirlineName());
            payment_arrival_refund.setText(arrivalList.getRefundable());
            Picasso.get().load(Constants.FLIGHT_IMAGE + arrivalList.getAirlineCode() + ".png").placeholder(R.drawable.flights).error(R.drawable.flights).into(payment_img_url_arrival);


        } else {
            LinearLayout layoutArrival = (LinearLayout) findViewById(R.id.layoutArrival);
            layoutArrival.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_booking:

                new Domesticflightbook(id_in, id_out).execute();

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public class Domesticflightbook extends AsyncTask<String, String, String> {

        private String flightIdIn, flightIdOut;

        public Domesticflightbook(String flightIdIn, String flightIdOut) {
            this.flightIdIn = flightIdIn;
            this.flightIdOut = flightIdOut;
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", GeneralPref.getAppId());
                jsonObject.put("id", GeneralPref.getCustId());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("flightIdIn", flightIdIn);
                jsonObject.put("flightIdOut", flightIdOut);
                Log.d("dinesh", "doInBackground: " + jsonObject);
                return new NetworkAPI().sendHTTPData(Constants.BOOK_FLIGHT, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(ConfirmBookingActivity.this);
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try{
                Log.d("dinesh", "doInBackground: " + s);
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    Intent intent = new Intent(ConfirmBookingActivity.this, FlightDetailsActivity.class);
                    intent.putExtra("flag","1");
                    startActivity(intent);
                    finish();
                } else {
                    new Utility().showSnackbar(chat_text, "Failed to book this flight.");
                    onBackPressed();
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
                new Utility().showSnackbar(chat_text, "Failed to book this flight.");
                onBackPressed();
                finish();
            }
        }
    }
}
