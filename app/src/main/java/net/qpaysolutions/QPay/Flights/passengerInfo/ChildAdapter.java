package net.qpaysolutions.QPay.Flights.passengerInfo;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Flights.BookFlightActivity;
import net.qpaysolutions.QPay.Flights.details.PassengerDetailsModel;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 10/16/17.
 */

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ChildHolder> {
    int size;


    BookFlightActivity detailsInfoActivity;
    private ArrayList<PassengerDetailsModel> arrayList = new ArrayList<>();


    public ChildAdapter(int size, BookFlightActivity detailsInfoActivity) {
        this.size = size;
        this.detailsInfoActivity = detailsInfoActivity;
    }

    @Override
    public ChildHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChildHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_passenger, parent, false));
    }

    @Override
    public void onBindViewHolder(ChildHolder holder, int position) {
        final PassengerDetailsModel detailsModel = new PassengerDetailsModel();

        holder.gender_child.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                detailsModel.setGender(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    detailsModel.setName_first(s.toString());
                }
            }
        });
        holder.middle_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    detailsModel.setName_middle(s.toString());
                }
            }
        });
        holder.last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    detailsModel.setName_second(s.toString());
                }
            }
        });
        detailsModel.setType(1);
        detailsInfoActivity.setPassenger(detailsModel);
        arrayList.add(detailsModel);
        detailsInfoActivity.child(arrayList);
    }

    @Override
    public int getItemCount() {
        return size;
    }

    public class ChildHolder extends RecyclerView.ViewHolder {
        public EditText first_name, last_name,middle_name;
        public Spinner gender_child;

        public ChildHolder(View itemView) {
            super(itemView);
            gender_child = (Spinner) itemView.findViewById(R.id.gender_child);
            first_name = (EditText) itemView.findViewById(R.id.first_name);
            middle_name = (EditText) itemView.findViewById(R.id.middle_name);
            last_name = (EditText) itemView.findViewById(R.id.last_name);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(detailsInfoActivity,
                    R.array.gender, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            gender_child.setAdapter(adapter);

        }
    }

}
