package net.qpaysolutions.QPay.Flights;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.Flights.tabFragment.DepartureFragment;

public class SelectFlightActivity extends AppCompatActivity {

    public Toolbar toolbar;
    public TextView chat_text, flightType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_flight);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        chat_text = (TextView) findViewById(R.id.title);
        flightType = (TextView) findViewById(R.id.flightType);
        Utils.toolbarConfiguration(toolbar, this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_flight, new DepartureFragment()).addToBackStack("departure fragment")
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void setFlightInfo(String type) {
        flightType.setText(type);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}