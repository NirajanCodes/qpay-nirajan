package net.qpaysolutions.QPay.Flights.Interface;

/**
 * Created by CSharp05-user on 12/06/2017.
 */

public interface OnDataPassedListener {
    void onDataPassed(String data);
}
