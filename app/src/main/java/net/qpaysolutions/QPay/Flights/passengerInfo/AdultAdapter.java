package net.qpaysolutions.QPay.Flights.passengerInfo;

import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Flights.BookFlightActivity;
import net.qpaysolutions.QPay.Flights.details.PassengerDetailsModel;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 10/16/17.
 */

public class AdultAdapter extends RecyclerView.Adapter<AdultAdapter.AdultHolder> {
    private int noOfAdults;
    private BookFlightActivity detailsInfoActivity;
    ArrayList<PassengerDetailsModel> passengerDetailsModels;


    public AdultAdapter(int noOfAdults, BookFlightActivity detailsInfoActivity) {
        this.noOfAdults = noOfAdults;
        this.detailsInfoActivity = detailsInfoActivity;
    }

    @Override
    public AdultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.passenger_info, parent, false);
        return new AdultHolder(view);
    }

    @Override
    public void onBindViewHolder(AdultHolder holder, int position) {
        final PassengerDetailsModel detailsModel = new PassengerDetailsModel();
        passengerDetailsModels = new ArrayList<>();
        holder.gender_adult.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                detailsModel.setGender(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.first_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    detailsModel.setName_first(s.toString());

                } else {
                    detailsModel.setName("");
                }
            }
        });
        holder.middle_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    detailsModel.setName_middle(s.toString());

                } else {
                    detailsModel.setName_middle(" ");
                }
            }
        });
        holder.last_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s.toString().trim();

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    detailsModel.setName_second(s.toString());
                } else {
                    detailsModel.setName_second("");
                }
            }
        });
        detailsModel.setType(2);
        detailsInfoActivity.setPassenger(detailsModel);
        passengerDetailsModels.add(detailsModel);
        detailsInfoActivity.adulst(passengerDetailsModels);
    }

    @Override
    public int getItemCount(){
        return noOfAdults;
    }

    public class AdultHolder extends RecyclerView.ViewHolder {
        public EditText first_name, last_name,middle_name;
        public Spinner gender_adult;

        public AdultHolder(View itemView){
            super(itemView);
            gender_adult = (Spinner) itemView.findViewById(R.id.gender_adult);
            first_name = (EditText) itemView.findViewById(R.id.first_name);
            middle_name= itemView.findViewById(R.id.middle_name);
            last_name = (EditText) itemView.findViewById(R.id.last_name);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(detailsInfoActivity,
                    R.array.gender, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            gender_adult.setAdapter(adapter);
        }
    }
}
