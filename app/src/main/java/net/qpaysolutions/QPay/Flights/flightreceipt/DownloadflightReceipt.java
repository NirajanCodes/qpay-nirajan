package net.qpaysolutions.QPay.Flights.flightreceipt;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 12/13/17.
 */

public class DownloadflightReceipt extends AppCompatActivity implements View.OnClickListener {
    private String id_in, id_out;
    private String stan,crrn;
    private Context context;
    private QPayProgressDialog qPayProgressDialog;
    private   Button downloadReceiptButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_bill);
        context=this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView chat_text = (TextView) toolbar.findViewById(R.id.title);
        chat_text.setText("Flight Receipt");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        departureDetails();
        arrivalDetails();
         downloadReceiptButton = (Button) findViewById(R.id.downloadReceipt);
        downloadReceiptButton.setOnClickListener(this);
        stan=getIntent().getStringExtra("stan");
        crrn= getIntent().getStringExtra("crrn");
        qPayProgressDialog= new QPayProgressDialog(this);
        downloadReceiptButton.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
            if (!Utility.hasExternalStoragePermission(DownloadflightReceipt.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(DownloadflightReceipt.this, PERMISSIONS, Tags.REQUEST_STORAGE);
            } else {

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Tags.REQUEST_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                break;

        }
    }

    private void departureDetails() {
        TextView payment_depart_to = (TextView) findViewById(R.id.payment_depart_to);
        TextView payment_flight_id = (TextView) findViewById(R.id.payment_flight_id);
        TextView payment_date_departure = (TextView) findViewById(R.id.payment_date_departure);
        TextView payment_flight_class = (TextView) findViewById(R.id.payment_flight_class);
        TextView payment_departure_date = (TextView) findViewById(R.id.payment_departure_date);
        TextView payment_departure_land = (TextView) findViewById(R.id.payment_departure_land);
        TextView payment_dep_from = (TextView) findViewById(R.id.payment_dep_from);
        TextView payment_dep_to = (TextView) findViewById(R.id.payment_dep_to);
        TextView payment_airline_name = (TextView) findViewById(R.id.payment_airline_name);
        TextView payment_fund_type = (TextView) findViewById(R.id.payment_fund_type);
        ImageView payment_img_url = (ImageView) findViewById(R.id.payment_img_url);
        TextView ticketNumberDeparture = (TextView) findViewById(R.id.ticketNumberDeparture);
        TextView reportingTimeDeparture = (TextView) findViewById(R.id.reportingTimeDeparture);
        try {
            JSONArray jsonArray = new JSONArray(FlightsUtils.getFlightBill());
            JSONObject object = jsonArray.getJSONObject(0);
            payment_depart_to.setText(object.getString("Sector"));
            payment_flight_id.setText("Flight No : " + object.getString("FlightNumber"));
            payment_date_departure.setText(object.getString("FlightDate"));
            payment_flight_class.setText("Class " + object.getString("ClassCode"));
            payment_departure_date.setText(object.getString("FlightTime"));
            payment_departure_land.setText(object.getString("ArrivalTime"));
            payment_dep_from.setText(object.getString("Departure"));
            payment_dep_to.setText(object.getString("Arrival"));
            payment_airline_name.setText(object.getString("AirlineName"));
            payment_fund_type.setText(object.getString("Refundable"));
            ticketNumberDeparture.setText(object.getString("TicketNumber"));
            reportingTimeDeparture.setText(object.getString("ReportingTime"));
            Picasso.get().load(Constants.FLIGHT_IMAGE + object.getString("AirlineCode") + ".png").placeholder(R.drawable.flights).error(R.drawable.flights).into(payment_img_url);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void arrivalDetails() {
        FlightList arrivalList = FlightsUtils.getArrival();
        if (FlightsUtils.getTripType().equals("2")) {
            id_in = arrivalList.getFlightId();
            LinearLayout layoutArrival = (LinearLayout) findViewById(R.id.layoutArrival);
            layoutArrival.setVisibility(View.VISIBLE);

            TextView payment_arrival_from = (TextView) findViewById(R.id.payment_arrival_from);
            TextView payment_flight_id_arrival = (TextView) findViewById(R.id.payment_flight_id_arrival);
            TextView payment_date_arrival = (TextView) findViewById(R.id.payment_date_arrival);
            TextView payment_flight_class_arrival = (TextView) findViewById(R.id.payment_flight_class_arrival);
            TextView payment_arrival_date = (TextView) findViewById(R.id.payment_arrival_date);
            TextView payment_arrival_lang = (TextView) findViewById(R.id.payment_arrival_lang);
            TextView payment_arrival = (TextView) findViewById(R.id.payment_arrival);
            TextView payment_arrival_to = (TextView) findViewById(R.id.payment_arrival_to);
            TextView payment_arrival_airlines = (TextView) findViewById(R.id.payment_arrival_airlines);
            TextView payment_arrival_refund = (TextView) findViewById(R.id.payment_arrival_refund);
            ImageView payment_img_url_arrival = (ImageView) findViewById(R.id.payment_img_url_arrival);
            TextView ticketNumberArrival = (TextView) findViewById(R.id.ticketNumberArrival);
            TextView reportingTimeArrival = (TextView) findViewById(R.id.reportingTimeArrival);
            try {
                JSONArray jsonArray = new JSONArray(FlightsUtils.getFlightBill());
                JSONObject object = jsonArray.getJSONObject(1);
                payment_arrival_from.setText(object.getString("Sector"));
                payment_flight_id_arrival.setText("Flight No : " + object.getString("FlightNumber"));
                payment_date_arrival.setText(object.getString("FlightDate"));
                payment_flight_class_arrival.setText("Class " + object.getString("ClassCode"));
                payment_arrival_date.setText(object.getString("FlightTime"));
                payment_arrival_lang.setText(object.getString("ArrivalTime"));
                payment_arrival.setText(object.getString("Departure"));
                payment_arrival_to.setText(object.getString("Arrival"));
                payment_arrival_airlines.setText(object.getString("AirlineName"));
                payment_arrival_refund.setText(object.getString("Refundable"));
                Picasso.get().load(Constants.FLIGHT_IMAGE + object.getString("AirlineCode") + ".png").placeholder(R.drawable.flights).error(R.drawable.flights).into(payment_img_url_arrival);
                ticketNumberArrival.setText(object.getString("TicketNumber"));
                reportingTimeArrival.setText(object.getString("ReportingTime"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            LinearLayout layoutArrival = (LinearLayout) findViewById(R.id.layoutArrival);
            layoutArrival.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.downloadReceipt:
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
                    if (!Utility.hasExternalStoragePermission(DownloadflightReceipt.this, PERMISSIONS)) {
                        ActivityCompat.requestPermissions(DownloadflightReceipt.this, PERMISSIONS, Tags.REQUEST_STORAGE);
                    } else {
                        new   GeneratePDF(DownloadflightReceipt.this,stan,crrn).execute();
                    }
                }else {
                    new   GeneratePDF(DownloadflightReceipt.this,stan,crrn).execute();
                }


                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent= new Intent(DownloadflightReceipt.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
