package net.qpaysolutions.QPay.Flights.tabFragment;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Flights.ConfirmBookingActivity;
import net.qpaysolutions.QPay.Flights.FlightDetailsActivity;
import net.qpaysolutions.QPay.Flights.Interface.OnFlightListListener;
import net.qpaysolutions.QPay.Flights.SelectFlightActivity;
import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Flights.adapter.FlightListAdapter;

import java.util.ArrayList;

/**
 * Created by CSharp05-user on 31/05/2017.
 */

public class DepartureFragment extends Fragment implements OnFlightListListener {

    private FlightListAdapter flightListAdapter;
    private RecyclerView departureRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.departure_fragment, container, false);
        ((SelectFlightActivity)getActivity()).chat_text.setText("Select Departure Flight");
        departureRecyclerView = (RecyclerView) view.findViewById(R.id.departure_recyclerView);
        setAdapter();
        return view;
    }

    public void setAdapter() {
        ArrayList<FlightList> flightLists = FlightsUtils.getFlightListOut();
        flightListAdapter = new FlightListAdapter(getContext(), this,flightLists,1);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        departureRecyclerView.setLayoutManager(mLayoutManager);
        departureRecyclerView.setItemAnimator(new DefaultItemAnimator());
        departureRecyclerView.setAdapter(flightListAdapter);
        try {
            ((SelectFlightActivity)getActivity()).setFlightInfo(FlightsUtils.getDeparturePlaceTime());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onFlightClicked(FlightList flightList) {
        FlightsUtils.setDeparture(flightList);
        if (FlightsUtils.getTripType().equals("2")){
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_flight,new ArrivalFragment())
                    .commit();
        }else {
            Intent intent = new Intent(getContext(), ConfirmBookingActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onFlightDetails(FlightList flightList) {
        FlightsUtils.setDeparture(flightList);
        if (FlightsUtils.getTripType().equals("2")){
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_flight,new ArrivalFragment())
                    .commit();
        }else {
            Intent intent = new Intent(getContext(), FlightDetailsActivity.class);
            intent.putExtra("flag","1");
            startActivity(intent);
        }
    }

}
