package net.qpaysolutions.QPay.Flights.details;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.CASHBACK.CheckRewardAPI;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.FlightDetailsActivity;
import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Flights.payment.FlightPaymentActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 10/17/17.
 */

public class DetailsInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<PassengerDetailsModel> detailsModelArrayList;
    private String total_payment_final, total_reward_amount;
    private boolean billPayList=true;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_flight);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView chat_text = (TextView) toolbar.findViewById(R.id.title);
        chat_text.setText("Flight Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        detailsModelArrayList =
                (ArrayList<PassengerDetailsModel>) getIntent().getSerializableExtra("passenger");
        departureDetails();
        arrivalDetails();
        setPassangerDetails();
        contactInfo();
        paymentInfo();
        Button details = (Button) findViewById(R.id.details);
        details.setOnClickListener(this);
        Button pay = (Button) findViewById(R.id.pay);
        pay.setOnClickListener(this);
    }

    private void departureDetails() {
        FlightList departureList = FlightsUtils.getDeparture();
        TextView payment_depart_to = (TextView) findViewById(R.id.payment_depart_to);
        TextView payment_flight_id = (TextView) findViewById(R.id.payment_flight_id);
        TextView payment_date_departure = (TextView) findViewById(R.id.payment_date_departure);
        TextView payment_flight_class = (TextView) findViewById(R.id.payment_flight_class);
        TextView payment_departure_date = (TextView) findViewById(R.id.payment_departure_date);
        TextView payment_departure_land = (TextView) findViewById(R.id.payment_departure_land);
        TextView payment_dep_from = (TextView) findViewById(R.id.payment_dep_from);
        TextView payment_dep_to = (TextView) findViewById(R.id.payment_dep_to);
        TextView payment_airline_name = (TextView) findViewById(R.id.payment_airline_name);
        TextView payment_fund_type = (TextView) findViewById(R.id.payment_fund_type);
        ImageView payment_img_url = (ImageView) findViewById(R.id.payment_img_url);
        payment_depart_to.setText(FlightsUtils.getFROM() + " -> " + FlightsUtils.getTO());
        payment_flight_id.setText("Flight No : " + departureList.getFlightNumber());
        payment_date_departure.setText(FlightsUtils.getDate());
        payment_flight_class.setText("Class " + departureList.getFlightClass());
        payment_departure_date.setText(departureList.getDepartureTime());
        payment_departure_land.setText(departureList.getArrivalTime());
        payment_dep_from.setText(departureList.getDeparture());
        payment_dep_to.setText(departureList.getArrival());
        payment_airline_name.setText(departureList.getAirlineName());
        payment_fund_type.setText(departureList.getRefundable());
        Picasso.get().load(Constants.FLIGHT_IMAGE + departureList.getAirlineCode() + ".png").placeholder(R.drawable.flights).error(R.drawable.flights).into(payment_img_url);

    }

    private void arrivalDetails() {
        FlightList arrivalList = FlightsUtils.getArrival();
        if (FlightsUtils.getTripType().equals("2")) {
            LinearLayout layoutArrival = (LinearLayout) findViewById(R.id.layoutArrival);
            layoutArrival.setVisibility(View.VISIBLE);
            TextView payment_arrival_from = (TextView) findViewById(R.id.payment_arrival_from);
            TextView payment_flight_id_arrival = (TextView) findViewById(R.id.payment_flight_id_arrival);
            TextView payment_date_arrival = (TextView) findViewById(R.id.payment_date_arrival);
            TextView payment_flight_class_arrival = (TextView) findViewById(R.id.payment_flight_class_arrival);
            TextView payment_arrival_date = (TextView) findViewById(R.id.payment_arrival_date);
            TextView payment_arrival_lang = (TextView) findViewById(R.id.payment_arrival_lang);
            TextView payment_arrival = (TextView) findViewById(R.id.payment_arrival);
            TextView payment_arrival_to = (TextView) findViewById(R.id.payment_arrival_to);
            TextView payment_arrival_airlines = (TextView) findViewById(R.id.payment_arrival_airlines);
            TextView payment_arrival_refund = (TextView) findViewById(R.id.payment_arrival_refund);
            ImageView payment_img_url_arrival = (ImageView) findViewById(R.id.payment_img_url_arrival);
            payment_arrival_from.setText(FlightsUtils.getTO() + " -> " + FlightsUtils.getFROM());
            payment_flight_id_arrival.setText("Flight No : " + arrivalList.getFlightNumber());
            payment_date_arrival.setText(FlightsUtils.getDate());
            payment_flight_class_arrival.setText("Class " + arrivalList.getFlightClass());
            payment_arrival_date.setText(arrivalList.getDepartureTime());
            payment_arrival_lang.setText(arrivalList.getArrivalTime());
            payment_arrival.setText(arrivalList.getDeparture());
            payment_arrival_to.setText(arrivalList.getArrival());
            payment_arrival_airlines.setText(arrivalList.getAirlineName());
            payment_arrival_refund.setText(arrivalList.getRefundable());
            Picasso.get().load(Constants.FLIGHT_IMAGE + arrivalList.getAirlineCode() + ".png").placeholder(R.drawable.flights).error(R.drawable.flights).into(payment_img_url_arrival);
        } else {
            LinearLayout layoutArrival = (LinearLayout) findViewById(R.id.layoutArrival);
            layoutArrival.setVisibility(View.GONE);
        }

    }

    private void setPassangerDetails() {
        RecyclerView passenger_recycle = (RecyclerView) findViewById(R.id.passenger_recycle);
        passenger_recycle.setHasFixedSize(true);
        passenger_recycle.setLayoutManager(new LinearLayoutManager(this));
        PassegerAdapter passegerAdapter = new PassegerAdapter(detailsModelArrayList);
        passenger_recycle.setAdapter(passegerAdapter);
    }

    private void contactInfo() {
        TextView contact_mobile = (TextView) findViewById(R.id.contact_mobile);
        TextView contact_name = (TextView) findViewById(R.id.contact_name);
        TextView contact_email = (TextView) findViewById(R.id.contact_email);
        QPayCustomerDatabase QPayCustomerDatabase = new QPayCustomerDatabase(this);
        QPayCustomerDatabase.getReadableDatabase();
        contact_name.setText(QPayCustomerDatabase.getFirstName() + " " + QPayCustomerDatabase.lastName());
        contact_mobile.setText(QPayCustomerDatabase.getCustomerPhone());
        contact_email.setText(GeneralPref.getEmailUser());
    }

    private void paymentInfo() {
        TextView payment_total = (TextView) findViewById(R.id.payment_total);
        TextView payment_tax = (TextView) findViewById(R.id.payment_tax);
        TextView payment_commission = (TextView) findViewById(R.id.payment_commission);
        FlightList departureList = FlightsUtils.getDeparture();
        if (FlightsUtils.getTripType().equals("1")) {
            payment_total.setText("NPR " + new Decimalformate().decimalFormate(departureList.getTotalAmount()));
            payment_tax.setText("NPR " + new Decimalformate().decimalFormate(departureList.getTax()));
            payment_commission.setText("NPR " + new Decimalformate().decimalFormate(departureList.getTotalCommission()));
            total_payment_final = departureList.getTotalAmount();
            total_reward_amount = departureList.getTotalCommission();
        } else {
            FlightList apparture = FlightsUtils.getArrival();
            double total_payment = Double.parseDouble(departureList.getTotalAmount()) + Double.parseDouble(apparture.getTotalAmount());
            double total_tax = Double.parseDouble(departureList.getTax()) + Double.parseDouble(apparture.getTax());
            double total_commission = Double.parseDouble(departureList.getTotalCommission()) + Double.parseDouble(apparture.getTotalCommission());
            payment_total.setText("NPR " + total_payment);
            payment_tax.setText("NPR " + total_tax);
            payment_commission.setText("NPR " + total_commission);
            total_payment_final = String.valueOf(total_payment);
            total_reward_amount = apparture.getTotalCommission();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        https:
//www.facebook.com/groups/847068115342002/?multi_permalinks=1473774782671329&notif_id=1509338061802274&notif_t=commerce_interesting_product#
        switch (v.getId()) {
            case R.id.details:
             /*final   QPayProgressDialog qPayProgressDialog = new QPayProgressDialog(DetailsInfoActivity.this);
                new CheckRewardAPI(total_reward_amount, "101", new DialogInterface() {
                    @Override
                    public void showDialog() {
                        qPayProgressDialog.show();
                    }

                    @Override
                    public void hideDialog() {
                        qPayProgressDialog.dismiss();
                        startActivity(new Intent(DetailsInfoActivity.this, FlightDetailsActivity.class));
                        finish();
                    }
                }).execute();
*/
//                FlightDetailsActivity
                Intent intent = new Intent(DetailsInfoActivity.this, FlightDetailsActivity.class);
                intent.putExtra("flag", "2");
                startActivity(intent);

                break;
            case R.id.pay:
                final QPayProgressDialog qPayProgressDialog1 = new QPayProgressDialog(DetailsInfoActivity.this);
                new CheckRewardAPI(total_reward_amount, "101", new DialogInterface() {
                    @Override
                    public void showDialog() {
                        qPayProgressDialog1.show();
                    }

                    @Override
                    public void hideDialog() {
                        qPayProgressDialog1.dismiss();
                        Intent intent = new Intent(DetailsInfoActivity.this, FlightPaymentActivity.class);
                        intent.putExtra("total_grand", total_payment_final);
                        startActivity(intent);
                    }
                }).execute();

              /*  if (billPayList) {
                    billPayList = false;
                    GeneralPref.setRewardAmount(String.valueOf((Double.valueOf(Utility.parseRebate("101")) / 100) * Double.valueOf(total_payment_final)));
                    Bundle bundle = new Bundle();
                    bundle.putString("money", total_payment_final);
                    bundle.putString("cell_number", "1");
                    bundle.putString("byt_code", "101");
                    BillPayListSheet billPayListSheet = new BillPayListSheet();
                    billPayListSheet.setArguments(bundle);
                    billPayListSheet.show(getSupportFragmentManager(), "flight");
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        billPayList = true;
                    }
                }, 2000);*/
                break;
        }
    }
}
