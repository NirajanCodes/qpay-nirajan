package net.qpaysolutions.QPay.Flights;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.Flights.details.DetailsInfoActivity;
import net.qpaysolutions.QPay.Flights.details.PassengerDetailsModel;
import net.qpaysolutions.QPay.Flights.passengerInfo.AdultAdapter;
import net.qpaysolutions.QPay.Flights.passengerInfo.ChildAdapter;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 10/16/17.
 */

public class BookFlightActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private TextView chat_text;
    private ArrayList<PassengerDetailsModel> detailsModelArrayList = new ArrayList<>();
    private FlightList flightLists, flightListArrival;
    private ArrayList<PassengerDetailsModel> adults = new ArrayList<>();
    private ArrayList<PassengerDetailsModel> childs = new ArrayList<>();
    private TextView user_first_name, user_last_name, user_email, user_phone;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String selectedGender;
    private String firstName, lastname, email, phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flight_book_contact);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        QPayCustomerDatabase = new QPayCustomerDatabase(this);
        QPayCustomerDatabase.getReadableDatabase();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        chat_text = (TextView) toolbar.findViewById(R.id.title);
        chat_text.setText("Passenger Information");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recycleAdult(FlightsUtils.getAdults());
        recycleChild(Integer.valueOf(FlightsUtils.getChilds()));
        userInfo();
        Button button = (Button) findViewById(R.id.booked);
        button.setOnClickListener(this);
        setGenderContact();
        flightLists =
                FlightsUtils.getDeparture();

        flightListArrival =
                FlightsUtils.getArrival();
    }

    private void setGenderContact() {
        Spinner user_gender_contact = (Spinner) findViewById(R.id.user_gender_contact);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(BookFlightActivity.this,
                R.array.gender, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        user_gender_contact.setAdapter(adapter);
        user_gender_contact.setOnItemSelectedListener(this);
    }

    private void userInfo() {
        user_first_name = (TextView) findViewById(R.id.user_first_name);
        user_first_name.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        user_last_name = (TextView) findViewById(R.id.user_last_name);
        user_last_name.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        user_email = (TextView) findViewById(R.id.user_email);
        user_phone = (TextView) findViewById(R.id.user_phone);


        user_first_name.setText(QPayCustomerDatabase.getFirstName());
        user_last_name.setText(QPayCustomerDatabase.lastName());
        user_phone.setText(QPayCustomerDatabase.getCustomerPhone());
        user_email.setText(GeneralPref.getEmailUser());
    }

    private void recycleAdult(int noOfAdults) {
        TextView adultinfo = (TextView) findViewById(R.id.adult_info);
        if (noOfAdults > 1) {
            adultinfo.setText(noOfAdults + " Adult's Information");
            adultinfo.setVisibility(View.VISIBLE);
        } else {
            adultinfo.setText(noOfAdults + " Adult Information");
            adultinfo.setVisibility(View.GONE);
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.adult_recycle);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        AdultAdapter adultAdapter = new AdultAdapter(noOfAdults, BookFlightActivity.this);
        recyclerView.setAdapter(adultAdapter);
    }

    private void recycleChild(int noOfChilds) {
        TextView adultinfo = (TextView) findViewById(R.id.child_info);
        if (noOfChilds > 1) {
            adultinfo.setText(noOfChilds + " Child Information");
            adultinfo.setVisibility(View.VISIBLE);
        } else {
            adultinfo.setText(noOfChilds + " Child Information");
            adultinfo.setVisibility(View.GONE);
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.child_recycle);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ChildAdapter childAdapter = new ChildAdapter(noOfChilds, BookFlightActivity.this);
        recyclerView.setAdapter(childAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.booked:
                if (isValidAdult(adults) == true) {
                    if (isValidChild(childs)) {
                        firstName = user_first_name.getText().toString();
                        email = user_email.getText().toString();
                        lastname = user_last_name.getText().toString();
                        phone = user_phone.getText().toString();
                        new DomesticFlightConfirm().execute();
                    } else {
                        Utils.customSnackBar(v, "Please Enter child information");
                    }


                } else {
                    Utils.customSnackBar(v, "Please Enter information");
                }
                break;
        }
    }


    public void setPassenger(PassengerDetailsModel passenger) {
        detailsModelArrayList.add(passenger);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedGender = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public class DomesticFlightConfirm extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", GeneralPref.getAppId());
                jsonObject.put("id", GeneralPref.getCustId());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("adult", FlightsUtils.getAdults());
                jsonObject.put("adultPassanger",getAdultsDetails(adults));
                jsonObject.put("child", FlightsUtils.getChilds());
                jsonObject.put("childPassanger", getChildsDetails(childs));
                jsonObject.put("contactEmail", email);
                jsonObject.put("contactPerson", selectedGender + " " + firstName + " " + lastname);
                jsonObject.put("contactPhone", phone);
                jsonObject.put("flightIdOut", flightLists.getFlightId());
                if (FlightsUtils.getTripType().equals("2")) {
                    jsonObject.put("flightIdIn", flightListArrival.getFlightId());
                }
                Log.d("dinesh", "onPostExecute: " + jsonObject);
                FlightsUtils.setFlightPay(jsonObject.toString());
//             return HttpUrlConnectionJson.sendHTTPData(ApplicationConstant.BOOK_FLIGHT_CONFIRM,jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (FlightsUtils.getTripType().equals("1")) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(BookFlightActivity.this, DetailsInfoActivity.class);
                bundle.putSerializable("passenger", detailsModelArrayList);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();

            } else {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(BookFlightActivity.this, DetailsInfoActivity.class);
                bundle.putSerializable("passenger", detailsModelArrayList);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        }
    }

    public JSONArray getAdultsDetails(ArrayList<PassengerDetailsModel> passengerDetailsModels) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < passengerDetailsModels.size(); i++) {
            try {
                JSONObject object = new JSONObject();
                object.put("Title", passengerDetailsModels.get(i).getGender());
                object.put("FirstName", passengerDetailsModels.get(i).getName_first());
                object.put("MiddleName", passengerDetailsModels.get(i).getName_middle());
                object.put("LastName", passengerDetailsModels.get(i).getName_second());
                jsonArray.put(object);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private boolean isValidAdult(ArrayList<PassengerDetailsModel> passengerDetailsModels) {
        boolean isValid = false;

        for (int i = 0; i < passengerDetailsModels.size(); i++) {

            if (passengerDetailsModels.get(i).getName_first() != null && !passengerDetailsModels.get(i).getName_first().isEmpty() && passengerDetailsModels.get(i).getName_second() != null && !passengerDetailsModels.get(i).getName_second().isEmpty()) {
                isValid = true;
            } else {
                return false;
            }


        }


        return isValid;
    }


    private boolean isValidChild(ArrayList<PassengerDetailsModel> passengerDetailsModels) {
        boolean isValid = false;
        if (passengerDetailsModels.size() == 0) {
            return true;
        }

        for (int i = 0; i < passengerDetailsModels.size(); i++) {

            if (passengerDetailsModels.get(i).getName_first() != null && !passengerDetailsModels.get(i).getName_first().isEmpty() && passengerDetailsModels.get(i).getName_second() != null && !passengerDetailsModels.get(i).getName_second().isEmpty()) {
                isValid = true;
            } else {
                return false;
            }


        }


        return isValid;
    }

    public void adulst(ArrayList<PassengerDetailsModel> passengerDetailsModels) {
        adults = passengerDetailsModels;
    }

    public JSONArray getChildsDetails(ArrayList<PassengerDetailsModel> PassengerDetailsModel) {
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < PassengerDetailsModel.size(); i++) {
            try {
                JSONObject object = new JSONObject();
                object.put("Title",PassengerDetailsModel.get(i).getGender());
                object.put("FirstName",PassengerDetailsModel.get(i).getName_first());
                object.put("MiddleName",PassengerDetailsModel.get(i).getName_middle());
                object.put("LastName", PassengerDetailsModel.get(i).getName_second());
                jsonArray.put(object);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    public void child(ArrayList<PassengerDetailsModel> passengerDetailsModels) {

        childs = passengerDetailsModels;


    }

}
