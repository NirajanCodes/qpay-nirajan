package net.qpaysolutions.QPay.Flights.Utility;

/**
 * Created by deadlydragger on 10/12/17.
 */

public class FlightsSector {
    String Nationality,NtlyCode,SecCode,Sector;

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getNtlyCode() {
        return NtlyCode;
    }

    public void setNtlyCode(String ntlyCode) {
        NtlyCode = ntlyCode;
    }

    public String getSecCode() {
        return SecCode;
    }

    public void setSecCode(String secCode) {
        SecCode = secCode;
    }

    public String getSector() {
        return Sector;
    }

    public void setSector(String sector) {
        Sector = sector;
    }
}
