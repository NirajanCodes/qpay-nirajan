package net.qpaysolutions.QPay.Flights.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.CustomerApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 10/12/17.
 */

public class FlightsUtils {
    public static String FLIGHTS = "flights";
    public static String NATILITY = "Nationality";
    public static String SECTOR = "Sector";
    public static String SEARCH = "SearchOut";
    public static String SEARCHIN = "Searchin";
    public static String PASSENGER = "passenger";
    public static String ADULTS = "adults";
    public static String CHILDs = "childs";
    public static String TRIP_TYPE = "trantype";

    public static String DEPARTURE = "departure";
    public static String ARRIVAL = "arrival";
    public static String FROM = "from";
    public static String TO = "to";
    public static String DATE = "date";
    public static String PAY = "flight_pay";
    public static String BILL = "flight_bill";

    public static String getTripType() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TRIP_TYPE, "");
    }

    public static void setTripType(String searchList) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TRIP_TYPE, searchList);
        editor.apply();
    }

    public static void setPassenger(String searchList) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PASSENGER, searchList);
        editor.apply();
    }

    public static String getPassenger() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PASSENGER, "");
    }

    public static void setAdults(int searchList) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(ADULTS, searchList);
        editor.apply();
    }

    public static int getAdults() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(ADULTS, 0);
    }

    public static void setChilds(int searchList) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(CHILDs, searchList);
        editor.apply();
    }

    public static int getChilds() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(CHILDs, 0);
    }

    public static void setFlightSearchListOut(String searchList) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SEARCH, searchList);
        editor.apply();
    }


    public static void from(String sh) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FROM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FROM, sh);
        editor.apply();
    }

    public static String getFROM() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FROM, Context.MODE_PRIVATE);
        return sharedPreferences.getString(FROM, "");
    }

    public static void to(String sh) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(TO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TO, sh);
        editor.apply();
    }

    public static String getTO() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(TO, Context.MODE_PRIVATE);

        return sharedPreferences.getString(TO, "");
    }


    public static void date(String sh) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(TO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DATE, sh);
        editor.apply();
    }

    public static String getDate() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(TO, Context.MODE_PRIVATE);

        return sharedPreferences.getString(DATE, "");
    }


    public static ArrayList<FlightList> getFlightListOut() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        String searchList = sharedPreferences.getString(SEARCH, "");
        ArrayList<FlightList> flightListArrayList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(searchList);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                FlightList flightList = new FlightList();
                flightList.setAdultCommission(jsonObject.getString("AdultCommission"));
                flightList.setAdultFare(jsonObject.getString("AdultFare"));
                flightList.setAircraftType(jsonObject.getString("AircraftType"));
                flightList.setAirlineCode(jsonObject.getString("AirlineCode"));
                flightList.setAirlineName(jsonObject.getString("AirlineName"));
                flightList.setArrival(jsonObject.getString("Arrival"));
                flightList.setArrivalTime(jsonObject.getString("ArrivalTime"));
                flightList.setChildCommission(jsonObject.getString("ChildCommission"));
                flightList.setChildFare(jsonObject.getString("ChildFare"));
                flightList.setCurrency(jsonObject.getString("Currency"));
                flightList.setDeparture(jsonObject.getString("Departure"));
                flightList.setDepartureTime(jsonObject.getString("DepartureTime"));
                flightList.setFlightClass(jsonObject.getString("FlightClass"));
                flightList.setFlightDate(jsonObject.getString("FlightDate"));
                flightList.setFlightId(jsonObject.getString("FlightId"));
                flightList.setFlightNumber(jsonObject.getString("FlightNumber"));
                flightList.setFreeBaggage(jsonObject.getString("FreeBaggage"));
                flightList.setFuelSurcharge(new Decimalformate().decimalFormate(jsonObject.getString("FuelSurcharge")));
                flightList.setNationality(jsonObject.getString("Nationality"));
                flightList.setRefundable(jsonObject.getString("Refundable"));
                flightList.setTax(new Decimalformate().decimalFormate(jsonObject.getString("Tax")));
                flightList.setTitle(jsonObject.getString("Title"));
                flightList.setTotalAmount(new Decimalformate().decimalFormate(jsonObject.getString("TotalAmount")));
                flightList.setTotalCommission(new Decimalformate().decimalFormate(jsonObject.getString("TotalCommission")));
                flightListArrayList.add(flightList);
            }
            return flightListArrayList;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void setFlightSearchListIn(String searchList) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SEARCHIN, searchList);
        editor.apply();
    }


    public static ArrayList<FlightList> getFlightListIn() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        String searchList = sharedPreferences.getString(SEARCHIN, "");
        ArrayList<FlightList> flightListArrayList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(searchList);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                FlightList flightList = new FlightList();
                flightList.setAdultCommission(jsonObject.getString("AdultCommission"));
                flightList.setAdultFare(jsonObject.getString("AdultFare"));
                flightList.setAircraftType(jsonObject.getString("AircraftType"));
                flightList.setAirlineCode(jsonObject.getString("AirlineCode"));
                flightList.setAirlineName(jsonObject.getString("AirlineName"));
                flightList.setArrival(jsonObject.getString("Arrival"));
                flightList.setArrivalTime(jsonObject.getString("ArrivalTime"));
                flightList.setChildCommission(jsonObject.getString("ChildCommission"));
                flightList.setChildFare(jsonObject.getString("ChildFare"));
                flightList.setCurrency(jsonObject.getString("Currency"));
                flightList.setDeparture(jsonObject.getString("Departure"));
                flightList.setDepartureTime(jsonObject.getString("DepartureTime"));
                flightList.setFlightClass(jsonObject.getString("FlightClass"));
                flightList.setFlightDate(jsonObject.getString("FlightDate"));
                flightList.setFlightId(jsonObject.getString("FlightId"));
                flightList.setFlightNumber(jsonObject.getString("FlightNumber"));
                flightList.setFreeBaggage(jsonObject.getString("FreeBaggage"));
                flightList.setFuelSurcharge(new Decimalformate().decimalFormate(jsonObject.getString("FuelSurcharge")));
                flightList.setNationality(jsonObject.getString("Nationality"));
                flightList.setRefundable(jsonObject.getString("Refundable"));
                flightList.setTax(new Decimalformate().decimalFormate(jsonObject.getString("Tax")));
                flightList.setTitle(jsonObject.getString("Title"));
                flightList.setTotalAmount(new Decimalformate().decimalFormate(jsonObject.getString("TotalAmount")));
                flightList.setTotalCommission(new Decimalformate().decimalFormate(jsonObject.getString("TotalCommission")));
                flightListArrayList.add(flightList);
            }
            return flightListArrayList;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void setNationality(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NATILITY, s);
        editor.apply();
    }

    public static ArrayList<FlightsSector> getNationality() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        String nationality = sharedPreferences.getString(NATILITY, "");
        ArrayList<FlightsSector> nationalitySector = new ArrayList<>();
        try {
            JSONArray Nationality = new JSONArray(nationality);
            for (int i = 0; i < Nationality.length(); i++) {
                JSONObject jsonObject = Nationality.getJSONObject(i);
                FlightsSector flightsSector = new FlightsSector();
                flightsSector.setNationality(jsonObject.getString("Nationality"));
                flightsSector.setNtlyCode(jsonObject.getString("NtlyCode"));
                nationalitySector.add(flightsSector);
            }
            return nationalitySector;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void setSector(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SECTOR, s);
        editor.apply();
    }

    public static ArrayList<FlightsSector> getSector() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(FLIGHTS, Context.MODE_PRIVATE);
        String sector = sharedPreferences.getString(SECTOR, "");
        ArrayList<FlightsSector> nationalitySector = new ArrayList<>();
        try {
            JSONArray Nationality = new JSONArray(sector);
            for (int i = 0; i < Nationality.length(); i++) {
                JSONObject jsonObject = Nationality.getJSONObject(i);
                FlightsSector flightsSector = new FlightsSector();
                flightsSector.setSecCode(jsonObject.getString("SecCode"));
                flightsSector.setSector(jsonObject.getString("Sector"));
                nationalitySector.add(flightsSector);
            }
            return nationalitySector;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setDeparture(FlightList departure) {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(DEPARTURE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String deap = gson.toJson(departure);
        editor.putString(DEPARTURE, deap);
        editor.apply();
    }

    public static void setArrival(FlightList arrival) {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(ARRIVAL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String deap = gson.toJson(arrival);
        editor.putString(ARRIVAL, deap);
        editor.apply();
    }

    public static FlightList getDeparture() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(DEPARTURE, Context.MODE_PRIVATE);
        return new Gson().fromJson(sharedPreferences.getString(DEPARTURE, ""), FlightList.class);
    }
    public static String getDeparturePlaceTime() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(DEPARTURE, Context.MODE_PRIVATE);
        return sharedPreferences.getString("departure_time_date", "");
    }

    public static void  setDepartureTimePlace(String s){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(DEPARTURE, Context.MODE_PRIVATE);
           SharedPreferences.Editor editor = sharedPreferences.edit();
           editor.putString("departure_time_date",s);
           editor.apply();
    }
    public static void  setArrivalTimePlace(String s){
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(DEPARTURE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("arrival_time_date",s);
        editor.apply();
    }
    public static String getArrivalPlaceTime() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(DEPARTURE, Context.MODE_PRIVATE);
        return sharedPreferences.getString("arrival_time_date", "");
    }

    public static FlightList getArrival() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(ARRIVAL, Context.MODE_PRIVATE);
        return new Gson().fromJson(sharedPreferences.getString(ARRIVAL, ""), FlightList.class);
    }

    public static void setFlightPay(String pay) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(PAY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PAY, pay);
        editor.apply();
    }

    public static String getFlightPay() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(PAY, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PAY, "");
    }

    public static void setFlightBill(String pay) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(PAY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(BILL, pay);
        editor.apply();
    }

    public static String getFlightBill() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(PAY, Context.MODE_PRIVATE);
        return sharedPreferences.getString(BILL, "");
    }


}
