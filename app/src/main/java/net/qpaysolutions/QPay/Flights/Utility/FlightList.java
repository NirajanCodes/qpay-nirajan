package net.qpaysolutions.QPay.Flights.Utility;

import java.io.Serializable;

/**
 * Created by deadlydragger on 10/13/17.
 */

public class FlightList implements Serializable{
    String AdultCommission, AdultFare, AircraftType, AirlineCode, AirlineName, Arrival, ArrivalTime, ChildCommission, ChildFare, Currency, Departure, DepartureTime, FlightClass, FlightDate, FlightId, FlightNumber, FreeBaggage, FuelSurcharge, Nationality, Refundable, Tax, Title,TotalAmount,TotalCommission;

    public FlightList() {
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public String getTotalCommission() {
        return TotalCommission;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public void setTotalCommission(String totalCommission) {
        TotalCommission = totalCommission;
    }

    public String getAdultCommission() {
        return AdultCommission;
    }

    public void setAdultCommission(String adultCommission) {
        AdultCommission = adultCommission;
    }

    public String getAdultFare() {
        return AdultFare;
    }

    public void setAdultFare(String adultFare) {
        AdultFare = adultFare;
    }

    public String getAircraftType() {
        return AircraftType;
    }

    public void setAircraftType(String aircraftType) {
        AircraftType = aircraftType;
    }

    public String getAirlineCode() {
        return AirlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        AirlineCode = airlineCode;
    }

    public String getAirlineName() {
        return AirlineName;
    }

    public void setAirlineName(String airlineName) {
        AirlineName = airlineName;
    }

    public String getArrival() {
        return Arrival;
    }

    public void setArrival(String arrival) {
        Arrival = arrival;
    }

    public String getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        ArrivalTime = arrivalTime;
    }

    public String getChildCommission() {
        return ChildCommission;
    }

    public void setChildCommission(String childCommission) {
        ChildCommission = childCommission;
    }

    public String getChildFare() {
        return ChildFare;
    }

    public void setChildFare(String childFare) {
        ChildFare = childFare;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getDeparture() {
        return Departure;
    }

    public void setDeparture(String departure) {
        Departure = departure;
    }

    public String getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(String departureTime) {
        DepartureTime = departureTime;
    }

    public String getFlightClass() {
        return FlightClass;
    }

    public void setFlightClass(String flightClass) {
        FlightClass = flightClass;
    }

    public String getFlightDate() {
        return FlightDate;
    }

    public void setFlightDate(String flightDate) {
        FlightDate = flightDate;
    }

    public String getFlightId() {
        return FlightId;
    }

    public void setFlightId(String flightId) {
        FlightId = flightId;
    }

    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }

    public String getFreeBaggage() {
        return FreeBaggage;
    }

    public void setFreeBaggage(String freeBaggage) {
        FreeBaggage = freeBaggage;
    }

    public String getFuelSurcharge() {
        return FuelSurcharge;
    }

    public void setFuelSurcharge(String fuelSurcharge) {
        FuelSurcharge = fuelSurcharge;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getRefundable() {
        return Refundable;
    }

    public void setRefundable(String refundable) {
        Refundable = refundable;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
