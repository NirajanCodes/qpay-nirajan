package net.qpaysolutions.QPay.Flights;

/**
 * Created by deadlydragger on 3/7/18.
 */

public class SectorModel {
    String id,name;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }
}
