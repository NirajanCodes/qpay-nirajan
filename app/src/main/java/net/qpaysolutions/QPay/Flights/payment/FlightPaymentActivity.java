package net.qpaysolutions.QPay.Flights.payment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Flights.flightreceipt.DownloadflightReceipt;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 10/29/17.
 */

public class FlightPaymentActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private Toolbar toolbar;
    private TextView chat_text;
    private QPayCustomerDatabase QPayCustomerDatabase;

    private String appId, custId;
    private String available_balance = "", total_grand;
    boolean isDialogOpen = false;
    private TextView twoWayTitleTextView;
    private TextView twoWayTextView;
    private TextView depatureDateTextView;
    private TextView arrivalTextView;
    private TextView totalFareTextView;
    private LinearLayout arrivalLayout;
    private TextView cashBackTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utility.setStatusColor(this);
        setContentView(R.layout.flight_payment);
        QPayCustomerDatabase = new QPayCustomerDatabase(this);
        QPayCustomerDatabase.getReadableDatabase();
        appId = QPayCustomerDatabase.getKeyAppId();
        custId = QPayCustomerDatabase.getCustomerID();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        chat_text = (TextView) toolbar.findViewById(R.id.title);
        chat_text.setText("Flight Payment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView reward_amount = (TextView) findViewById(R.id.reward_amount);
        twoWayTitleTextView = findViewById(R.id.twoWayValueTextView);
        twoWayTextView = findViewById(R.id.twoWayTextView);
        depatureDateTextView = findViewById(R.id.depatureDateTextView);
        arrivalTextView = findViewById(R.id.arrivalTextView);
        totalFareTextView = findViewById(R.id.totalFareTextView);
        arrivalLayout = findViewById(R.id.arrivalLayout);
        cashBackTextView = findViewById(R.id.cashBackTextView);
        FlightList departureList = FlightsUtils.getDeparture();
        FlightList arrivalList = FlightsUtils.getArrival();

        if (FlightsUtils.getTripType().equals("2")) {
            twoWayTitleTextView.setText(FlightsUtils.getFROM() + " - " + FlightsUtils.getTO() + ", " + FlightsUtils.getTO() + " - " + FlightsUtils.getFROM());
            twoWayTextView.setText("Two Way");
            arrivalLayout.setVisibility(View.VISIBLE);
            depatureDateTextView.setText(FlightsUtils.getDate() + ", " + departureList.getDepartureTime());
         //   arrivalTextView.setText(FlightsUtils.getDepatureDate() + ", " + arrivalList.getDepartureTime());
            double total_payment = Double.parseDouble(departureList.getTotalAmount()) + Double.parseDouble(arrivalList.getTotalAmount());
            totalFareTextView.setText(String.valueOf(total_payment));
        } else {
            totalFareTextView.setText("NPR " + new Decimalformate().decimalFormate(departureList.getTotalAmount()));
            twoWayTitleTextView.setText(FlightsUtils.getFROM() + " - " + FlightsUtils.getTO());
            twoWayTextView.setText("One Way");
            depatureDateTextView.setText(FlightsUtils.getDate() + ", " + departureList.getDepartureTime());
            arrivalLayout.setVisibility(View.GONE);
        }
        cashBackTextView.setText("NPR " + GeneralPref.getRewardAmount());
//        pointsTextView.setText(GeneralPref.getPrabhuPayPoints());
//        System.out.println("Rewards Points:: "+GeneralPref.getPrabhuPayPoints());
        reward_amount.setText("You will be awarded with cash back reward of NPR" + GeneralPref.getRewardAmount() + ".");

        TextView qpay_balance = (TextView) findViewById(R.id.qpay_balance);
        String balance = "AVAILABLE BALANCE :" + "<b>" + "NPR " + new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()) + "</b> ";
        qpay_balance.setText(Html.fromHtml(balance));
        LinearLayout pay_through_qpay = (LinearLayout) findViewById(R.id.pay_through_qpay);
        pay_through_qpay.setOnClickListener(this);
        available_balance = BalancePref.getAvailableAmount();
        total_grand = getIntent().getStringExtra("total_grand");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//changes
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_through_qpay:
                if (isDialogOpen == false) {
                    try {
                        if (GeneralPref.getAllowBonus()) {
                            isDialogOpen = true;
                            double available = Double.parseDouble(available_balance);
                            double amount = Double.parseDouble(total_grand);
                            confirmPaythroughQpay();

                          /*  if (available > amount) {
                                confirmPaythroughQpay();
                            } else {
                                isDialogOpen = false;
                                Utility.custumdialogfailureInsufficient("Insufficient Balance!", "Insufficient QPay balance for this transaction. Please try with cards.", FlightPaymentActivity.this);
                            }*/
                        } else {
                            dialogFailure();
                        }
                    } catch (Exception e) {
                        dialogFailure("Restaurant Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");
                    }
                }
                break;
        }
    }

    public void dialogFailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(FlightPaymentActivity.this);
        LayoutInflater inflater = FlightPaymentActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
                finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public void dialogFailure() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(FlightPaymentActivity.this);
        LayoutInflater inflater = FlightPaymentActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_custom_dialog_bill_payment_feature_unavailable, null);
        final TextView sure = (TextView) v.findViewById(R.id.sure);
        final TextView conttent_text = (TextView) v.findViewById(R.id.content_text);
        conttent_text.setText("Please perform a sales transaction or add SCT card in order to use your sign-up bonus .");
        sure.setText("Fund Transfer Unavailable!");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                startActivity(new Intent(FlightPaymentActivity.this, MainActivity.class));
                finish();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public void dialogSuccess(String display_title, String number, final JSONObject jsonObject) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(FlightPaymentActivity.this);
        LayoutInflater inflater = FlightPaymentActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(display_title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {
                    JSONObject jsonObject1 =jsonObject;
                    String Crrn = jsonObject1.getString("Crrn");
                    String Stan = jsonObject1.getString("Stan");
                    Intent intent = new Intent(FlightPaymentActivity.this, DownloadflightReceipt.class);
                    intent.putExtra("stan", Stan);
                    intent.putExtra("crrn", Crrn);
                    startActivity(intent);
                    finish();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void confirmPaythroughQpay() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(FlightPaymentActivity.this);
        LayoutInflater inflater = FlightPaymentActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Do you want to Pay from your QPay Account?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DomesticFlightConfirm().execute();
                dialog.dismiss();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

    }


    public class DomesticFlightConfirm extends AsyncTask<String, String, String> {
        QPayProgressDialog qPayProgressDialog;

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject(FlightsUtils.getFlightPay());
                Log.d("dinesh", "doInBackground: " + jsonObject);
                return NetworkAPI.sendHTTPData(Constants.BOOK_FLIGHT_CONFIRM, jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(FlightPaymentActivity.this);
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                Log.d("dinesh", "doInBackground: " + s);
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success){
                    if (jsonObject.getString("status").equals("00")){
                        dialogSuccess("Success!",jsonObject.getString("message"),jsonObject.getJSONArray("data").getJSONObject(0));
                    } else {
                        dialogFailure("Payment Failure!", jsonObject.getString("message"));
                    }
                } else {
                    dialogFailure("Payment Failure!", jsonObject.getString("message"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                dialogFailure("Payment Failure!", "Flight Payment failure due to internal network problem.");
            }
        }
    }
}
