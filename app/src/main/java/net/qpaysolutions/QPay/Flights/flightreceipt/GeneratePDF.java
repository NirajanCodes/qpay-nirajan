package net.qpaysolutions.QPay.Flights.flightreceipt;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import androidx.core.content.FileProvider;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by deadlydragger on 6/15/18.
 */


public class  GeneratePDF extends AsyncTask<String,String,String> {
    String stan,crrn;
    Context context;
    QPayProgressDialog qPayProgressDialog;

    public GeneratePDF(Context context,String stan, String crrn) {
        this.context=context;
        this.stan = stan;
        this.crrn = crrn;

    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id",GeneralPref.getCustId());
            jsonObject.put("stan",stan);
            jsonObject.put("crrn",crrn);
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng",LatLngPref.getLng());
            NetworkAPI.downloadFileFromeUrl(Constants.TICKETS_DOWNLOAD,jsonObject,context,"qpay_flights_tickets"+"_"+crrn);


        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        qPayProgressDialog= new QPayProgressDialog(context);
        qPayProgressDialog.show();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        qPayProgressDialog.dismiss();
        try{
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/qpay/"+"qpay_flights_tickets"+"_"+crrn+".pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() +".my.package.name.provider" ,file), "application/pdf");
            context.startActivity(Intent.createChooser(intent, "Open PDF"));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
