package net.qpaysolutions.QPay.Flights;


import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.Flights.Utility.FlightList;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;

public class FlightDetailsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView chat_text;
    private FlightList flightLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        chat_text = (TextView) toolbar.findViewById(R.id.title);
        chat_text.setText("Flight Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        flightLists =FlightsUtils.getDeparture();
        setDetailsDeparture(flightLists, FlightsUtils.getPassenger(), FlightsUtils.getAdults(), FlightsUtils.getChilds());

        if (FlightsUtils.getTripType().equals("2")){
            LinearLayout arrivalLayout =(LinearLayout)findViewById(R.id.arrivalLayout);
            arrivalLayout.setVisibility(View.VISIBLE);
            setDetailsArrival( FlightsUtils.getArrival(), FlightsUtils.getPassenger(), FlightsUtils.getAdults(), FlightsUtils.getChilds());
        }
    }

    private void setDetailsDeparture(FlightList details, String total_passenger, int noOfAdults, int noOfChilds) {
        TextView flight_number = (TextView) findViewById(R.id.flight_number);
        TextView baggage_weight = (TextView) findViewById(R.id.baggage_weight);
        TextView no_of_passengers = (TextView) findViewById(R.id.no_of_passengers);
        TextView no_of_adults = (TextView) findViewById(R.id.no_of_adults);
        TextView total_cost_of_adults = (TextView) findViewById(R.id.total_cost_of_adults);
        TextView no_of_childs = (TextView) findViewById(R.id.no_of_childs);
        TextView total_cost_of_child = (TextView) findViewById(R.id.total_cost_of_child);
        TextView fuel_charge = (TextView) findViewById(R.id.fuel_charge);
        TextView tax = (TextView) findViewById(R.id.tax);
        TextView commission = (TextView) findViewById(R.id.commission);
        TextView total_fee = (TextView) findViewById(R.id.total_fee);
        flight_number.setText("Departure Flight Number : " + details.getFlightNumber());
        baggage_weight.setText(details.getFreeBaggage());
        no_of_passengers.setText(total_passenger);
        no_of_adults.setText(noOfAdults + " Adult(s) @ NPR" + details.getAdultFare());
        double total_adult = noOfAdults * Double.parseDouble(details.getAdultFare());
        total_cost_of_adults.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(total_adult)));
        if (noOfChilds>0){
            no_of_childs.setText(noOfChilds + " Child(s) @ NPR" + details.getChildFare());
            double total_child = noOfAdults * Double.parseDouble(details.getChildFare());
            total_cost_of_child.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(total_child)));
        }
        fuel_charge.setText("NPR " + details.getFuelSurcharge());
        tax.setText("NPR " + details.getTax());
        commission.setText("NPR " + details.getTotalCommission());
        total_fee.setText("NPR " + details.getTotalAmount());
        Button proceed = (Button) findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if (getIntent().getStringExtra("flag").equals("2")){
                       onBackPressed();
                        finish();
                    }else {
                        Intent intent = new Intent(FlightDetailsActivity.this, BookFlightActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
//              onBackPressed();
            }
        });
    }

    private void setDetailsArrival(FlightList details, String total_passenger, int noOfAdults, int noOfChilds) {
        TextView flight_number = (TextView) findViewById(R.id.arrival_flight_number);
        TextView baggage_weight = (TextView) findViewById(R.id.arrival_baggage_weight);
        TextView no_of_passengers = (TextView) findViewById(R.id.arrival_no_of_passengers);
        TextView no_of_adults = (TextView) findViewById(R.id.arrival_no_of_adults);
        TextView total_cost_of_adults = (TextView) findViewById(R.id.arrival_total_cost_of_adults);
        TextView no_of_childs = (TextView) findViewById(R.id.arrival_no_of_childs);
        TextView total_cost_of_child = (TextView) findViewById(R.id.arrival_total_cost_of_child);
        TextView fuel_charge = (TextView) findViewById(R.id.arrival_fuel_charge);
        TextView tax = (TextView) findViewById(R.id.arrival_tax);
        TextView commission = (TextView) findViewById(R.id.arrival_commission);
        TextView total_fee = (TextView) findViewById(R.id.arrival_total_fee);
        flight_number.setText("Arrival Flight Number : " + details.getFlightNumber());
        baggage_weight.setText(details.getFreeBaggage());
        no_of_passengers.setText(total_passenger);
        no_of_adults.setText(noOfAdults + " Adult(s) @ NPR" + details.getAdultFare());
        double total_adult = noOfAdults * Double.parseDouble(details.getAdultFare());
        total_cost_of_adults.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(total_adult)));

        if (noOfChilds>0){
            no_of_childs.setText(noOfChilds + " Child(s) @ NPR" + details.getChildFare());

            double total_child = noOfAdults * Double.parseDouble(details.getChildFare());
            total_cost_of_child.setText("NPR " + new Decimalformate().decimalFormate(String.valueOf(total_child)));
        }

        fuel_charge.setText("NPR " + details.getFuelSurcharge());
        tax.setText("NPR " + details.getTax());
        commission.setText("NPR " + details.getTotalCommission());
        total_fee.setText("NPR " + details.getTotalAmount());
        Button proceed = (Button) findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if (getIntent().getStringExtra("flag").equals("2")){
                        onBackPressed();
                        finish();
                    }else {
                        Intent intent = new Intent(FlightDetailsActivity.this, BookFlightActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
