package net.qpaysolutions.QPay;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.RequiresApi;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Billpayment.BillPayActivity;
import net.qpaysolutions.QPay.Billpayment.BillPaymentActivity;
import net.qpaysolutions.QPay.Card.AddCardActivity;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.CheckInternetConnection.NetworkInformation;
import net.qpaysolutions.QPay.Deal.DealFragment;
import net.qpaysolutions.QPay.FragmentCollection.InviteFriends;
import net.qpaysolutions.QPay.FragmentCollection.MainFragmentNotCall;
import net.qpaysolutions.QPay.History.TransactionHistoryActivity;
import net.qpaysolutions.QPay.Interface.HomeLogo;
import net.qpaysolutions.QPay.MainWebCall.RegisterDeviceAsynk;
import net.qpaysolutions.QPay.MainWebCall.UploadImages;
import net.qpaysolutions.QPay.NFC.AccountStorage;
import net.qpaysolutions.QPay.NearByAtms.NearbyAtmsFragment;
import net.qpaysolutions.QPay.NearByShop.NearbyShopFragment;
import net.qpaysolutions.QPay.Notification.NotificationViewer;
import net.qpaysolutions.QPay.Register.Unauthorizeuser;
import net.qpaysolutions.QPay.SalesMerchants.ReadMerchantQRActivity;
import net.qpaysolutions.QPay.Send.SendMainActivity;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Utils.Utilitypermission;
import net.qpaysolutions.QPay.dashboard.MainActivityFragment;
import net.qpaysolutions.QPay.duonavigation.views.DuoDrawerLayout;
import net.qpaysolutions.QPay.duonavigation.views.DuoMenuView;
import net.qpaysolutions.QPay.duonavigation.widgets.DuoDrawerToggle;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;


public class MainActivity extends AppCompatActivity implements View.OnClickListener /*,Drawerlockinterface*/, HomeLogo, DuoMenuView.OnMenuClickListener {

    private Toolbar toolbar;
    private TextView toolbar_text;
    private DuoDrawerLayout duodrawerLayout;
    private NavigationView nDrawer;
    private String flag_pin, pin_leangth, pin_random;
    private QPayCustomerDatabase customerDatabase;
    private String cust_id, app_id;
    private ImageView custum_header_image;
    private ProgressBar pb_header;
    private String encodedImage;
    private QPayCustomerDatabase qpayDatabase;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    //  private String first_name, last_name, up_first_name, up_last_name;
    static final String TAG = "dinesh";
    private String custumer_phone;
    private String img_no;
    private SQLiteDatabase db;
    private Utilitypermission utilitypermission = new Utilitypermission();
    private String one;
    private String number = "";
    int openFragment = 0;
    private NetworkChangeReceiver networkChangeReceiver;
    private boolean mIsReceiverRegistered = false;
    private ImageView image_toolbar;
    public static final String MY_PREFS_NAME = "token";
    private SharedPreferences sharePreference;
    private String tooken;
    private ImageView right_icon, refresh, myCode;
    private DuoMenuAdapter mMenuAdapter;
    private int lastExpandedPosition = -1;
    private ViewHolder mViewHolder;
    private FragmentManager fm;
    private String kyc_status = "";
    private ArrayList<String> mTitles = new ArrayList<>();
    private TextView userName;
    private TextView userPhoneNumber;
    private DuoLock duoLock;
    private MainActivityFragment fragment;
    private MainActivityFragment.SetName setName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_main);
            setTheme(R.style.AppTheme);


            //            int count = 0;
//            List<Integer> primes = new ArrayList<>();
//            for (int p = 29; p < 1000; ) {
//                p = calculate(p);
//                primes.add(p);
////                count++;
//            }
//            Log.d(TAG, primes.toString());
//
////            startService(new Intent(this, CardService.class));
//            GeneralPref.setPaymentCount(29);
            //       new CheckStatusNew().execute();
//            Log.d(TAG, Dukpt.hmac("10000332100300E00091", "100.05"));
            if (utilitypermission.checkAndRequestPermissions(this)) {

            }
            toolbar = findViewById(R.id.toolbar_main);

            sharePreference = getSharedPreferences("key_intent", MODE_PRIVATE);
            customerDatabase = new QPayCustomerDatabase(this);
            customerDatabase.getReadableDatabase();
            customerDatabase.getWritableDatabase();
            db = customerDatabase.getWritableDatabase();
            pin_leangth = customerDatabase.getPin();
            qpayDatabase = new QPayCustomerDatabase(this);
            pin_random = customerDatabase.getRandom();
            cust_id = customerDatabase.getCustomerID();
            //storing customer id for nfc

            GeneralPref.setCustId(cust_id);
            AccountStorage.SetAccount(MainActivity.this, cust_id);
            Log.d("custid_main_activity", "" + AccountStorage.GetAccount(MainActivity.this));
            app_id = customerDatabase.getKeyAppId();
//            pb_header = findViewById(R.id.pb_header);
            userName = findViewById(R.id.userNameDrawer);
            userPhoneNumber = findViewById(R.id.userCellDrawer);
//            custum_header_image = findViewById(R.id.header_add);
            userName.setText(GeneralPref.getName());
            userPhoneNumber.setText(customerDatabase.getCustomerPhone());
            refresh = findViewById(R.id.refresh);
            myCode = findViewById(R.id.my_code);
            try {
                String name = customerDatabase.getFirstName() + " " + customerDatabase.lastName();
                GeneralPref.setName(name);

            } catch (Exception e) {
                e.printStackTrace();
            }
            duodrawerLayout = findViewById(R.id.drawer);

            // img_no = customerDatabase.getImg();
            one = getIntent().getStringExtra("one");
            Intent intent = getIntent();
            //   openFragment = intent.getIntExtra("Check", 0);
            number = intent.getStringExtra("number");
            kyc_status = GeneralPref.getKYC();
            fm = getSupportFragmentManager();
            mTitles = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.ld_activityScreenTitles)));
            mViewHolder = new ViewHolder();
            duoNavigationLock();
            // handleToolbar();
            handleMenu();
            handleDrawer();
            setName = name ->{
                    userName.setText(name);

        };
        fragment = new MainActivityFragment(refresh, myCode, toolbar, duoLock, setName);

        fm.beginTransaction()
                .replace(R.id.container, fragment).commit();
        checkInternetConnection();
        UpdateDeviceToken();
        display();
    } catch(
    Exception e)

    {
        e.printStackTrace();
    }


}

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public boolean disableKYC(boolean set) {
        return set;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }


    private void duoNavigationLock() {
        duoLock = new DuoLock() {
            @Override
            public void lockNavigation(int lockmode) {
                duodrawerLayout.setDrawerLockMode(DuoDrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                mViewHolder.mDuoMenuView.setVisibility(View.GONE);
                duodrawerLayout.closeDrawer();
            }
        };
    }

    public void display() {
        try {
            Log.d(Utility.TAG, "flag from invite : " + openFragment);
            switch (openFragment) {
                case 6:
                    MainFragmentNotCall mainFragmentNotCall = new MainFragmentNotCall();
                    FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction1.replace(R.id.fragment, mainFragmentNotCall).commit();
                    break;
                /*case 0:
                    MainActivityFragment fragment1 = new MainActivityFragment();
                    fm.beginTransaction()
                            .replace(R.id.container, fragment1).commit();
                    //setPicassoImage(GeneralPref.getImageUrl());*/
                case 5:
                    // fragment = new MainActivityFragment(duoLock, refresh, myCode, toolbar);
                    fragment = new MainActivityFragment(refresh, myCode, toolbar, duoLock, setName);
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, fragment);
                    Bundle bundlemain = new Bundle();
                    bundlemain.putInt("id", openFragment);
                    // bundlemain.putSerializable("duoLock", duoLock);
                    fragment.setArguments(bundlemain);
                    fragmentTransaction.commit();
                    break;
                case 1:
                    InviteFriends fragment = new InviteFriends();
                    FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                    fragmentManager.replace(R.id.fragment, fragment);
                    Bundle bundle = new Bundle();
                    bundle.putString("number", number);
                    fragment.setArguments(bundle);
                    fragmentManager.commit();
                    setTitle("Invite Friends");
//                    openFragment=0;
                    break;
                case 7:
                    NearbyAtmsFragment nearbyAtmsFragment = new NearbyAtmsFragment();
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.animation_one_activity, R.anim.animation_two_activity)
                            .replace(R.id.fragment, nearbyAtmsFragment)
                            .commit();
                    break;
               /* case 2:
                    DealFragment cardProfileMain = new DealFragment();
                    FragmentTransaction fragmentManagercardProfileMain = getSupportFragmentManager().beginTransaction();
                    fragmentManagercardProfileMain
                            .replace(R.id.fragment, cardProfileMain).commit();
                    break;*/
                case 3:

                    NotificationViewer notificationViewer = new NotificationViewer();
                    FragmentTransaction fragmentManagernotificationViewer = getSupportFragmentManager().beginTransaction();
                    fragmentManagernotificationViewer
                            .replace(R.id.fragment, notificationViewer).commit();
                    break;
                case 8:
                    startActivity(new Intent(MainActivity.this, ChatActivity.class));
                    finish();
                    break;

            }

           /* View headerLayout = nDrawer.inflateHeaderView(R.layout.duo_view_header);
            pb_header = headerLayout.findViewById(R.id.pb_header);
            LinearLayout background_header = headerLayout.findViewById(R.id.background_header);
            try {
                if (Build.VERSION.SDK_INT >= 16) {
                    background_header.setBackground(getAssetImage(this, "profile_bg_customer.png"));
                } else {
                    background_header.setBackgroundDrawable(getAssetImage(this, "profile_bg_customer.png"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            setPicassoImage(img_no);
            Log.d(Utility.TAG, Constants.LAST_AUTH_CALL + cust_id);
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            networkChangeReceiver = new NetworkChangeReceiver();
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mIsReceiverRegistered = true;
            checkInternetConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setToolbar_text(String title) {
        toolbar_text.setText(title);
    }

    public void setRightIcon(boolean bool) {
        if (bool) {
            right_icon.setVisibility(View.GONE);
        } else {
            right_icon.setVisibility(View.VISIBLE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.pay_frame:
                startActivity(new Intent(MainActivity.this, ReadMerchantQRActivity.class));
                break;
            case R.id.send:
                Intent i = new Intent(MainActivity.this, SendMainActivity.class);
                i.putExtra("friend_list", "0");
                startActivity(i);
                break;
            case R.id.bill_payment:
                Intent iii = new Intent(MainActivity.this, BillPaymentActivity.class);
                MainActivity.this.startActivity(iii);
                finish();
                break;

         /*   case R.id.header_add:
                selectImage();
                break;*/


            /*case R.id.lock_now:
                try {
                    flag_pin = customerDatabase.getFlagPin();
                    if (flag_pin != null && flag_pin.equals("1")) {
                        System.runFinalizersOnExit(true);
                        this.finishAffinity();
                    } else {
                        try {
                            SetCodeFragment setPasscodeFragment = new SetCodeFragment();
                            FragmentTransaction fragmentTransactionmainFragmentsetPasscodeFragment = getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.animation_one_activity, R.anim.animation_two_activity);
                            fragmentTransactionmainFragmentsetPasscodeFragment.replace(R.id.fragment, setPasscodeFragment);
                            fragmentTransactionmainFragmentsetPasscodeFragment.commit();
                            setTitle("Set Passcode");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                drawerLayout.closeDrawers();
                break; */
        }
    }

    private void handleToolbar() {
        setSupportActionBar(mViewHolder.mToolbar);
    }

    private void handleDrawer() {
        final DuoDrawerToggle duoDrawerToggle = new DuoDrawerToggle(MainActivity.this,
                mViewHolder.mDuoDrawerLayout,
                mViewHolder.mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );

        mViewHolder.mDuoDrawerLayout.setDrawerListener(duoDrawerToggle);
        duoDrawerToggle.syncState();

      /*  ValueAnimator anim = ValueAnimator.ofFloat(0,1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                duoDrawerToggle.onDrawerSlide(mViewHolder.mDuoDrawerLayout, slideOffset);
            }
        });


        anim.setInterpolator(new DecelerateInterpolator());
        // You can change this duration to more closely match that of the default animation.
        anim.setDuration(100);
        anim.start();
*/
    }

    private void handleMenu() {

        mMenuAdapter = new DuoMenuAdapter(mTitles);
        mViewHolder.mDuoMenuView.setOnMenuClickListener((DuoMenuView.OnMenuClickListener) this);
        mViewHolder.mDuoMenuView.setAdapter(mMenuAdapter);

    }

    public void UpdateDeviceToken() {

        sharePreference = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        tooken = sharePreference.getString("fcm_token", "");
        String token_updated = sharePreference.getString("token_updated", "");
        if (!tooken.equals(token_updated)) {
            new RegisterDeviceAsynk(tooken, app_id, cust_id, MainActivity.this).execute();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            Log.w(Utility.TAG, "onResume");
            if (!mIsReceiverRegistered) {
                if (networkChangeReceiver == null)
                    networkChangeReceiver = new NetworkChangeReceiver();
                registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                mIsReceiverRegistered = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onPause() {

        super.onPause();
        try {
            if (mIsReceiverRegistered) {
                unregisterReceiver(networkChangeReceiver);
                networkChangeReceiver = null;
                mIsReceiverRegistered = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void showHomeLogo(boolean tru) {
        if (tru) {
            image_toolbar.setVisibility(View.GONE);
        } else {
            image_toolbar.setVisibility(View.VISIBLE);
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "CusUpload photo"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {Manifest.permission.CAMERA};
                    if (!hasCameraPermission(MainActivity.this, PERMISSIONS)) {
                        ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, Tags.REQUEST_CAMERA);
                    } else {
                        cameraIntent();
                    }
                } else {
                    cameraIntent();
                }
            } else if (items[item].equals("Upload photo")) {
                userChoosenTask = "Upload photo";
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE};
                    if (!hasExternalStoragePermission(MainActivity.this, PERMISSIONS)) {
                        ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, Tags.REQUEST_STORAGE);
                    } else {
                        galleryIntent();
                    }
                } else {
                    galleryIntent();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), Tags.REQUEST_STORAGE);
    }

    private void cameraIntent() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Tags.REQUEST_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Tags.REQUEST_STORAGE)
                onSelectFromGalleryResult(data);
            else if (requestCode == Tags.REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        custum_header_image.setImageBitmap(thumbnail);
        new UploadImages(cust_id, encodedImage, MainActivity.this, customerDatabase, pb_header).execute();
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Log.d(Utility.TAG, "url : " + data.getData());
//                Utility.rotate(bm,270);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
                encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                custum_header_image.setImageBitmap(bm);
                new UploadImages(cust_id, encodedImage, MainActivity.this, customerDatabase, pb_header).execute();
                Log.d(Utility.TAG, "from upload image  bitmap : " + encodedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setPicassoImage(String url) {
        Log.d(TAG, "setPicassoImage: " + url);
        if (url != null && !url.isEmpty()) {
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.header_placeholder)
                    .error(R.drawable.header_placeholder)
                    .noFade()
                    .into(custum_header_image);
        }
    }


    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    boolean doubleBackToExitPressedOnce = false;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {

        if (mIsReceiverRegistered) {
            unregisterReceiver(networkChangeReceiver);
            networkChangeReceiver = null;
            mIsReceiverRegistered = false;
        }
        if (doubleBackToExitPressedOnce) {
            System.runFinalizersOnExit(true);
            this.finishAffinity();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    private void checkInternetConnection() {
        LinearLayout noInternetConnection = findViewById(R.id.no_internet_connection_id);
        NetworkInformation networkInformation = NetworkInformation.getInstance(this);
        if (networkInformation.isOnline()) {
            noInternetConnection.setVisibility(View.GONE);
        } else {
            noInternetConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        return super.dispatchTouchEvent(ev);
    }

    public void checkInternetConnection(View view) {
        checkInternetConnection();
    }

    @Override
    public void onFooterClicked() {

    }

    @Override
    public void onHeaderClicked() {

    }

    @Override
    public void onOptionClicked(int position, Object objectClicked) {
        // Set the toolbar title
        setTitle(mTitles.get(position));

        // Set the right options selected
        mMenuAdapter.setViewSelected(position, true);

        // Navigate to the right fragment
        switch (position) {

            case 0:

                break;

            case 1:
                Intent inviteFrnsIntent = new Intent(this, BillPayActivity.class);
                inviteFrnsIntent.putExtra("id", "invite");
                inviteFrnsIntent.putExtra("number", number);
                startActivity(inviteFrnsIntent);
                break;

            case 2:

                Intent intent = new Intent(this, AddCardActivity.class);
//                        intent.putExtra("id", "cardTopup");
//                        intent.putExtra("id", "add_card");
                startActivity(intent);
                break;

            case 3:
                Intent rechargePin = new Intent(this, BillPayActivity.class);
                rechargePin.putExtra("id", "rechargePin");
                startActivity(rechargePin);
                break;

            case 4:
                Intent myCodeIntent = new Intent(this, BillPayActivity.class);
                myCodeIntent.putExtra("id", "code");
                myCodeIntent.putExtra("flag", "code");
                startActivity(myCodeIntent);

                break;
            case 5:
                startActivity(new Intent(MainActivity.this, TransactionHistoryActivity.class));
                break;
            case 6:
                Intent settingsIntent = new Intent(this, BillPayActivity.class);
                settingsIntent.putExtra("id", "settings");
                startActivity(settingsIntent);
                break;
            case 7:

                Intent notificationIntent = new Intent(this, BillPayActivity.class);
                notificationIntent.putExtra("id", "notif");
                startActivity(notificationIntent);
                break;

            case 8:
                Intent faqIntent = new Intent(this, BillPayActivity.class);
                faqIntent.putExtra("id", "faq");
                startActivity(faqIntent);
                break;

            case 9:
                Intent contactIntent = new Intent(this, BillPayActivity.class);
                contactIntent.putExtra("id", "contact");
                startActivity(contactIntent);
                break;

            case 10:
                Intent kycDetail = new Intent(this, BillPayActivity.class);
                kycDetail.putExtra("id", "kycDetails");
                startActivity(kycDetail);
                break;

            case 11:
                Intent about = new Intent(this, BillPayActivity.class);
                about.putExtra("id", "about");
                startActivity(about);
                break;
           /* case 12:
                Intent quickPay = new Intent(this, BillPayActivity.class);
                quickPay.putExtra("id", "quick");
                startActivity(quickPay);
                break;*/

            case 13:
                try {
                    flag_pin = qpayDatabase.getFlagPin();
                    if (flag_pin != null && flag_pin.equals("1")) {
                        System.runFinalizersOnExit(true);
                        this.finishAffinity();
                    } else {
                        Intent notificationIntent1 = new Intent(this, BillPayActivity.class);
                        notificationIntent1.putExtra("id", "setCode");
                        startActivity(notificationIntent1);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

        mViewHolder.mDuoDrawerLayout.closeDrawer();
    }


public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        NetworkInformation networkInformation = NetworkInformation.getInstance(context);
        if (networkInformation.isOnline()) {
            checkInternetConnection();
        }
    }
}


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean hasCameraPermission(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean hasExternalStoragePermission(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean hasSMSPermission(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Tags.REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give camera permission.", Toast.LENGTH_LONG).show();
                }
                break;
            case Tags.REQUEST_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give gallery permission.", Toast.LENGTH_LONG).show();
                }
                break;
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    NearbyAtmsFragment atmsFragment = (NearbyAtmsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
                    atmsFragment.getATM();
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give gallery permission.", Toast.LENGTH_LONG).show();
                }
                break;
            case Tags.SMS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    InviteFriends inviteSMS = (InviteFriends) getSupportFragmentManager().findFragmentById(R.id.fragment);
                    inviteSMS.sendSMS();
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give gallery permission.", Toast.LENGTH_LONG).show();
                }
                break;
            case Tags.LOCATION_SHOP:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    NearbyShopFragment inviteSMS = (NearbyShopFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
                    inviteSMS.shopLocation();

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give gallery permission.", Toast.LENGTH_LONG).show();
                }
                break;
            case Tags.DEAL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    DealFragment inviteSMS = (DealFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
                    inviteSMS.setLocation();

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give gallery permission.", Toast.LENGTH_LONG).show();
                }
                break;
            case 112: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    InviteFriends fragment = (InviteFriends) getSupportFragmentManager().findFragmentById(R.id.fragment);
                    fragment.pickContact();
                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give contact permission.", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }


    public void loadUA() {
        try {
            duodrawerLayout.setDrawerLockMode(DuoDrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            Unauthorizeuser blockedfragment = new Unauthorizeuser();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, blockedfragment)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

private class ViewHolder {
    private DuoDrawerLayout mDuoDrawerLayout;
    private DuoMenuView mDuoMenuView;
    private Toolbar mToolbar;

    ViewHolder() {
        mDuoDrawerLayout = findViewById(R.id.drawer);
        mDuoMenuView = (DuoMenuView) mDuoDrawerLayout.getMenuView();
        mToolbar = findViewById(R.id.toolbar_main);
    }
}

public interface DuoLock extends Serializable {
    void lockNavigation(int lockmode);
}

public class CheckStatusNew extends AsyncTask<String, String, String> {
    int prime = 0;

    @Override
    protected String doInBackground(String... params) {
        String status_check;
        try {
            JSONObject jsonObject = new JSONObject();

//                prime = calculate(GeneralPref.getPaymentCount());
//                int prime = 997;

//                String hmacKey = Dukpt.incrementKSN(ksn, prime);

//                String hash = Dukpt.hmac(GeneralPref.getPaymentCount(), amount);
//                String hash = Dukpt.hmac("12345", "qpaylimited");

                /*String encryptedCustomerId = Dukpt.encryptCustomerId(cust_id);
//              String customerTLV = Dukpt.createTLVCustomerId(encryptedCustomerId);
                String amount = "1000";

                String customerTLVwithAmount = Dukpt.createTLVCustomerIdWithAmount(encryptedCustomerId, amount);
*/
            jsonObject.put("app_id", app_id);
            jsonObject.put("cust_id", cust_id);

//                jsonObject.put("cust_id", customerTLV);
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            //  jsonObject.put("Amount", amount);
            Log.d(TAG, "check ip : " + jsonObject);
            status_check = NetworkAPI.sendHTTPData(Constants.CHECK_MY_STATUS_NEW, jsonObject);
            Log.d(TAG, "check ip : " + jsonObject);

            return status_check;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //  progressBar.setVisibility(View.VISIBLE);
//            rotateImage(360, 100, refresh, 500, false);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        //progressBar.setVisibility(View.GONE);
        Log.d(TAG, "onPostExecute: " + s);

        try {
            JSONObject jsonObject = new JSONObject(s);
            Log.d(TAG, "jsonObject : " + jsonObject);
            JSONObject checkmystatusResult = jsonObject.getJSONObject("checkmystatusResult");
            String status = checkmystatusResult.getString("status");
            if (status.equals("00")) {
                String res_code = checkmystatusResult.getString("resp_code");
                if (res_code.equals("00")) {

                    // paisaTextView.setText(BalancePref.getRewardPoint());
//                        bonusTextView.setText("NPR "+BalancePref.getBonusAmount());
                    // bonusTextView.setText(BalancePref.getBonusAmount());

                } else if (res_code.equals("NB")) {
                    toolbar.setVisibility(View.GONE);
                    duoLock.lockNavigation(2);
                } else if (res_code.equals("UA")) {
                    toolbar.setVisibility(View.GONE);
                    duoLock.lockNavigation(2);


                } else if (res_code.equals("S")) {


                }

            } else if (status.equalsIgnoreCase("S")) {
                duoLock.lockNavigation(2);
                toolbar.setVisibility(View.GONE);


            } else if (status.equals("UA")) {

                duoLock.lockNavigation(2);
                toolbar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}


}

