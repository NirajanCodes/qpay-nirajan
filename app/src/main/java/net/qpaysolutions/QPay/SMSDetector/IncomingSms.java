package net.qpaysolutions.QPay.SMSDetector;

/**
 * Created by deadlydragger on 6/14/16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import net.qpaysolutions.QPay.Register.EnterVerificationCodeFragment;

public class IncomingSms extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    String number = message.replaceAll("\\D+", "");
                    try {
                        if (senderNum.equals("QPay")) {
                            EnterVerificationCodeFragment Sms = new EnterVerificationCodeFragment();
                            Sms.recivedSms(number);
                        }
                    } catch (Exception e) {
                    }

                }
            }

        } catch (Exception e) {

        }
    }

}


