package net.qpaysolutions.QPay.MyCode;

/**
 * Created by deadlydragger on 8/19/16.
 */

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.PassCodeView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 8/19/16.
 */
public class DisablePasscode extends Fragment {
    private PassCodeView PassCodeView;
    TextView promptView;
    QPayCustomerDatabase QPayCustomerDatabase;
    String pin_l;
    LinearLayout passcode_background;
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase =new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        pin_l= QPayCustomerDatabase.getPin();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.set_passcode_main,container,false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PassCodeView = (net.qpaysolutions.QPay.CustumClasses.PassCodeView) view.findViewById(R.id.set_passcode);
        promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        PassCodeView.setTypeFace(typeFace);
        PassCodeView.setKeyTextColor(R.color.black_shade);
        promptView.setTypeface(typeFace);
        bindEvents();
        promptView.setText("Disable Passcode");

      /*  passcode_background = (LinearLayout)view.findViewById(R.id.passcode_background);
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                passcode_background.setBackground(getAssetImage(getContext(), "passcode_background.png"));
            } else {
                passcode_background.setBackgroundDrawable(getAssetImage(getContext(), "passcode_background.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
    private void bindEvents() {
        PassCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                try {
                    if (text.length() == 4) {
                        if (text.equals(pin_l)){
                            QPayCustomerDatabase.updateFlagPin(" '0' ");
                            Toast.makeText(getContext(),"Passcode disabled ",Toast.LENGTH_LONG).show();
                            MyCodeFragment myCodeFragment = new MyCodeFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.bill_payment_container, myCodeFragment)
                                    .commit();
                        }else {
                            PassCodeView.setError(true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
