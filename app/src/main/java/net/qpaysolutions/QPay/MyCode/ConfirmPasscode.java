package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.PassCodeView;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

/**
 * Created by deadlydragger on 8/19/16.
 */
public class ConfirmPasscode extends Fragment {
    private PassCodeView PassCodeView;
    TextView promptView;
    String confirm_code;
    private QPayCustomerDatabase QPayCustomerDatabase;
    String app_id,cust_id;
    private String dinesh;
    FrameLayout pDialog;
    LinearLayout passcode_background;
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pDialog=(FrameLayout)view.findViewById(R.id.progressBar) ;
        PassCodeView=(PassCodeView)view.findViewById(R.id.set_passcode);
        promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        PassCodeView.setTypeFace(typeFace);
        PassCodeView.setKeyTextColor(R.color.black_shade);
        promptView.setTypeface(typeFace);
        promptView.setText("Confirm Passcode");
        confirm_code=getArguments().getString("code");
        Log.d(Utility.TAG,"first code : " + confirm_code);
        bindEvents();
        passcode_background = (LinearLayout)view.findViewById(R.id.passcode_background);

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.set_passcode_main,container,false);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        app_id= QPayCustomerDatabase.getKeyAppId();
        cust_id= QPayCustomerDatabase.getCustomerID();
    }
    private void bindEvents() {
        PassCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                try {

                    Log.d(Utility.TAG,"last enter : " + text);
                    dinesh=gen();
                    if (text.length()==4){
                        if (confirm_code.equals(text)){
                            new SendRandompin().execute();
                        }else {
                            PassCodeView.setError(true);
                        }


                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }
    public class SendRandompin extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String resul_send_random;
            JSONObject jsonObject_pin = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String pin_response_random=null;
            try {
                jsonObject_pin.put("app_id",app_id);
                jsonObject_pin.put("cust_id",cust_id);
                jsonObject_pin.put("reset_pin",dinesh);
                jsonObject_pin.put("lat", 0.0);
                jsonObject_pin.put("lng", 0.0);
                Log.d(Utility.TAG,"post pin : " + jsonObject_pin.toString());
                resul_send_random = networkAPI.sendHTTPData(Constants.RANDOM_PIN,jsonObject_pin);
                Log.d(Utility.TAG,"response pin : " + resul_send_random);
                JSONObject jsonObject = new JSONObject(resul_send_random);
                JSONObject jsonObject1 = jsonObject.getJSONObject("storeresetpinResult");
                pin_response_random = jsonObject1.getString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return pin_response_random;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.setVisibility(View.GONE);
            try {
                if ( s.equals("00")) {

                    QPayCustomerDatabase.updatePin_l("'"+confirm_code+"'");
                    QPayCustomerDatabase.updateFlagPin(" '1' ");
                    Log.d(Utility.TAG, "random key " + dinesh);
                    QPayCustomerDatabase.updateRandom_pin(dinesh);
                    try {
                        MyCodeFragment myCodeFragment = new MyCodeFragment();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .add(R.id.bill_payment_container, myCodeFragment)
                                .commit();
                        Toast.makeText(getActivity(), "Successfully updated your password ", Toast.LENGTH_SHORT).show();
                    }catch (Exception e){

                        e.printStackTrace();

                    }


                } else {
                    Toast.makeText(getActivity(), "Failed to set Password ", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                e.printStackTrace();

                try {

                    Toast.makeText(getActivity(), "Failed to set Password ", Toast.LENGTH_SHORT).show();
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }
        }
    }
    public String gen() {
        Random r = new Random(System.currentTimeMillis());
        return String.valueOf(1000 + r.nextInt(2000));
    }
}
