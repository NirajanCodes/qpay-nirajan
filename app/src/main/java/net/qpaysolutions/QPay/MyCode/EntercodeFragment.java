package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.PassCodeView;
import net.qpaysolutions.QPay.Register.BlockFragment;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 8/10/16.
 */
public class EntercodeFragment extends Fragment{
    private PassCodeView passCodeView;
    private   int count = 0;
    String   pin_l;
    QPayCustomerDatabase QPayCustomerDatabase;
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
           pin_l= QPayCustomerDatabase.getPin();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_passcode,container,false);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        passCodeView = (PassCodeView) view.findViewById(R.id.pass_code_view);
        TextView promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        passCodeView.setTypeFace(typeFace);
        passCodeView.setKeyTextColor(R.color.black_shade);
        promptView.setTypeface(typeFace);
        bindEvents();
    }
    private void bindEvents() {
        passCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                try {
                    if (text.length() == 4) {

                        Log.d("dinesh",pin_l);
                        Log.d("dinesh",text.toString());
                        if (text.equals(pin_l)) {

                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.putExtra("Check",5);
                            startActivity(intent);
                            getActivity().finish();
                        }
                        else if (count >= 2){
                            try {
                                QPayCustomerDatabase.updatePin_retrive("1");
                                BlockFragment blockedfragment = new BlockFragment();
                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment, blockedfragment)
                                        .commit();
                                getActivity().setTitle("Blocked User");
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }else {
                            count++;
                            passCodeView.setError(true);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }
}

