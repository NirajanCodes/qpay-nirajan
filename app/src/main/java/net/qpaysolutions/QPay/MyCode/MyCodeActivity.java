package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.QRcodegenerator.Contents;
import net.qpaysolutions.QPay.QRcodegenerator.QRCodeEncoder;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyCodeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView title;
    private QPayCustomerDatabase sctDatabaseHelper;
    private String name_databases, last_name, full_name, app_id, flag_pin, pin_lenght, cust_id;
    private QPayProgressDialog qPayProgressDialog;
    private ImageView myImage;
    private String result;
    private String status;
    private TextView balance;

    private TextView name_setting, balancce_setting;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycode);

        sctDatabaseHelper = new QPayCustomerDatabase(this);
        sctDatabaseHelper.getReadableDatabase();
        cust_id = sctDatabaseHelper.getCustomerID();
        flag_pin = sctDatabaseHelper.getFlagPin();
        name_databases = sctDatabaseHelper.getUserNameFirst();
        last_name = sctDatabaseHelper.getUserNameLast();
        app_id = sctDatabaseHelper.getKeyAppId();
        myImage = findViewById(R.id.create_my_qrcode);
        new GETQRCode().execute();
    }

    public class GETQRCode extends AsyncTask<String, String, String> {


        public GETQRCode() {

        }

        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;
            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_balance.put("id", cust_id);
                jsonObject_balance.put("appId", app_id);
                jsonObject_balance.put("lat", LatLngPref.getLat());
                jsonObject_balance.put("lng", LatLngPref.getLng());
                Log.d("dinesh", jsonObject_balance.toString());
                return networkAPI.sendHTTPData(Constants.QRGENERATION, jsonObject_balance);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(MyCodeActivity.this);
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d(Utility.TAG, "post service : " + s);
                qPayProgressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.getBoolean("success")) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject object = data.getJSONObject(0);
                    String emvCpqrcPayload = object.getString("emvCpqrcPayload");
                    create(emvCpqrcPayload);
                }
            } catch (Exception e) {
                qPayProgressDialog.dismiss();
                e.printStackTrace();
            }
        }
    }


    public class GetQpaybalance extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;

            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_balance.put("cust_id", cust_id);
                jsonObject_balance.put("app_id", app_id);
                jsonObject_balance.put("lat", LatLngPref.getLat());
                jsonObject_balance.put("lng", LatLngPref.getLng());
                result = networkAPI.sendHTTPData(Constants.GET_BALANCE_QPAY, jsonObject_balance);
                JSONObject jsonObject = new JSONObject(result);
                JSONObject jsonObject1 = jsonObject.getJSONObject("getbalanceResult");
                balance_avail = jsonObject1.getString("avail_bal");
                status = jsonObject1.getString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return balance_avail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d(Utility.TAG, "available qpay balance is: " + s);
                if (status.equals("00")) {
                    balancce_setting.setText("NPR " + s);
                } else {
                    balance.setText("Unavailable");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void create(String cust_id) {
//        String qrInputTextGeneral = sctDatabaseHelper.getEncryptedCustomerId();
        String qrInputText = null;
        String encryptedMsg = cust_id;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            WindowManager manager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                if (bitmap != null) {
                    myImage.setImageBitmap(bitmap);
                } else {
                    this.create(cust_id);
                }


            } catch (WriterException e) {
                e.printStackTrace();
                try {
                    this.create(cust_id);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        }
    }
}
