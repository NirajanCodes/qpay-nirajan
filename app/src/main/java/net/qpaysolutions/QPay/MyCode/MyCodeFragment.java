package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.QRcodegenerator.Contents;
import net.qpaysolutions.QPay.QRcodegenerator.QRCodeEncoder;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 8/2/16.
 */
public class MyCodeFragment extends Fragment implements View.OnClickListener {

    private ImageView local_acess;
    private TextView balance;
    private QPayCustomerDatabase sctDatabaseHelper;
    private String result;
    private String status;
    private String cust_id;
    private Switch enable_local, notification_enable, sms_enable;
    private String flag_pin, pin_lenght;
    private String TAG = Utility.TAG;
    private TextView condition;
    private TextView name_setting, balancce_setting;
    private String name_databases, last_name, full_name;
    private LinearLayout reset_pass, lockCode, myCode;
    private ScrollView info;
    private ImageView myImage;
    private String app_id;
    QPayProgressDialog qPayProgressDialog;
    String noti, sms;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sctDatabaseHelper = new QPayCustomerDatabase(getActivity());
        sctDatabaseHelper.getReadableDatabase();
        cust_id = sctDatabaseHelper.getCustomerID();
        flag_pin = sctDatabaseHelper.getFlagPin();
        name_databases = sctDatabaseHelper.getUserNameFirst();
        last_name = sctDatabaseHelper.getUserNameLast();
        app_id = sctDatabaseHelper.getKeyAppId();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
           /* ((MainActivity) getActivity()).setToolbar_text("My Code");
            ((MainActivity) getActivity()).showHomeLogo(true);
            ((MainActivity) getActivity()).setRightIcon(false);*/
            local_acess = (ImageView) view.findViewById(R.id.local_acess);
            enable_local = (Switch) view.findViewById(R.id.enable_local_acess);
            condition = (TextView) view.findViewById(R.id.pass_condition);
            balancce_setting = (TextView) view.findViewById(R.id.balance_setting);
            name_setting = (TextView) view.findViewById(R.id.name_setting);
            reset_pass = (LinearLayout) view.findViewById(R.id.reset_pass);
            myImage = (ImageView) view.findViewById(R.id.create_my_qrcode);
            notification_enable = (Switch) view.findViewById(R.id.notification_enable);
            sms_enable = (Switch) view.findViewById(R.id.sms_enable);
            full_name = last_name + " " + name_databases;
            name_setting.setText(full_name);
            lockCode = view.findViewById(R.id.lockCode);
            info = view.findViewById(R.id.info);
            myCode = view.findViewById(R.id.my_code);
            Log.d(TAG, "flag pin from setting is : " + flag_pin);
            pin_lenght = sctDatabaseHelper.getPin();
            Log.d(TAG, "pin lenght : " + pin_lenght);
            Log.d(TAG, "cust_id from setting main");
            noti = sctDatabaseHelper.getNotificationId();
            sms = sctDatabaseHelper.getSmsId();
            progressBar = view.findViewById(R.id.progressBar);
            Log.d(Utility.TAG, "sms: " + sms + "  noti : " + noti);
            if (noti.equals("1")) {
                notification_enable.setChecked(true);
            } else {
                notification_enable.setChecked(false);
            }
            if (sms.equals("1")) {
                sms_enable.setChecked(true);
            } else {
                sms_enable.setChecked(false);
            }
            //create(cust_id);
//            new GetQpaybalance().execute();
            balancce_setting.setText("NPR " + BalancePref.getAvailableAmount());
            Log.d(TAG, "get balance from setting : " + Constants.GET_BALANCE_QPAY + cust_id);
            enable_local.setChecked(false);
            if (flag_pin != null && !flag_pin.isEmpty() && flag_pin.equals("1")) {
                enable_local.setChecked(true);
            }
            enable_local.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (pin_lenght == null) {
//                        setLocalPassword();
                            SetCodeFragment setPasscodeFragment = new SetCodeFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.bill_payment_container, setPasscodeFragment)
                                    .commit();
                        } else {
//                        enablePAss();
                            EnablePasscode setPasscodeFragment = new EnablePasscode();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.bill_payment_container, setPasscodeFragment)
                                    .commit();
                        }
                    } else {
//                    disablePAss();
                        DisablePasscode setPasscodeFragment = new DisablePasscode();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.bill_payment_container, setPasscodeFragment)
                                .commit();
                    }
                }
            });
            reset_pass.setOnClickListener(this);
            notification_enable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        sctDatabaseHelper.updateServiceNotification("1");
//                         new GetEnableService(sms,"1").execute();
                    } else {
                        sctDatabaseHelper.updateServiceNotification("0");
//                         new GetEnableService(sms,"0").execute();
                    }
                }
            });
            sms_enable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        sctDatabaseHelper.updateServiceSMS("1");
//                        new GetEnableService("1",noti).execute();
                    } else {
                        sctDatabaseHelper.updateServiceSMS("0");
//                        new GetEnableService("0",noti).execute();
                    }
                }
            });
            if (getArguments().getString("flag").equalsIgnoreCase("code")) {
                new GETQRCode().execute();
                lockCode.setVisibility(View.GONE);
                info.setVisibility(View.VISIBLE);
                myCode.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setting_main, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reset_pass:
                try {
                    if (pin_lenght.length() > 2) {
                        Resetpasscode setPasscodeFragment = new Resetpasscode();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.bill_payment_container, setPasscodeFragment)
                                .commit();
                    } else {
                        enable_local.setChecked(true);
                        SetCodeFragment setPasscodeFragment = new SetCodeFragment();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.bill_payment_container, setPasscodeFragment)
                                .commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        enable_local.setChecked(true);
                        SetCodeFragment setPasscodeFragment = new SetCodeFragment();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.bill_payment_container, setPasscodeFragment)
                                .commit();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                break;
        }
    }

    public class GetEnableService extends AsyncTask<String, String, String> {

        String is_sms, is_push;

        public GetEnableService(String is_sms, String is_push) {
            this.is_push = is_push;
            this.is_sms = is_sms;
        }

        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;
            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_balance.put("cust_id", cust_id);
                jsonObject_balance.put("app_id", app_id);
                jsonObject_balance.put("is_sms", is_sms);
                jsonObject_balance.put("is_push", is_push);
                jsonObject_balance.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_balance.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "post service : " + jsonObject_balance);
                result = networkAPI.sendHTTPData(Constants.NOTIFICATION_SETING, jsonObject_balance);
                JSONObject jsonObject = new JSONObject(result);
                JSONObject jsonObject1 = jsonObject.getJSONObject("NotificationSettingsResult");
                status = jsonObject1.getString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return balance_avail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getActivity());
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                qPayProgressDialog.dismiss();

                if (status.equals("00")) {

                } else {
                    balance.setText("Unavailable");
                }
            } catch (Exception e) {
                qPayProgressDialog.dismiss();
                e.printStackTrace();
            }
        }
    }

    public class GETQRCode extends AsyncTask<String, String, String> {


        public GETQRCode() {

        }

        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;
            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_balance.put("id", cust_id);
                jsonObject_balance.put("appId", app_id);
                jsonObject_balance.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_balance.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d("dinesh", jsonObject_balance.toString());
                return networkAPI.sendHTTPData(Constants.QRGENERATION, jsonObject_balance);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            qPayProgressDialog = new QPayProgressDialog(getActivity());
//            qPayProgressDialog.show();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d(Utility.TAG, "post service : " + s);
//                qPayProgressDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                myImage.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.getBoolean("success")) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject object = data.getJSONObject(0);
                    String emvCpqrcPayload = object.getString("emvCpqrcPayload");
                    create(emvCpqrcPayload);
                }
            } catch (Exception e) {
//                qPayProgressDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }
    }


    public class GetQpaybalance extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;

            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_balance.put("cust_id", cust_id);
                jsonObject_balance.put("app_id", app_id);
                jsonObject_balance.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_balance.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                result = networkAPI.sendHTTPData(Constants.GET_BALANCE_QPAY, jsonObject_balance);
                JSONObject jsonObject = new JSONObject(result);
                JSONObject jsonObject1 = jsonObject.getJSONObject("getbalanceResult");
                balance_avail = jsonObject1.getString("avail_bal");
                status = jsonObject1.getString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return balance_avail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d(TAG, "available qpay balance is: " + s);
                if (status.equals("00")) {
                    balancce_setting.setText("NPR " + s);
                } else {
                    balance.setText("Unavailable");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*public void create() {
//        String qrInputTextGeneral = sctDatabaseHelper.getEncryptedCustomerId();
        String qrInputText = null;
        String encryptedMsg = cust_id;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                if (bitmap != null) {
                    myImage.setImageBitmap(bitmap);
                } else {
                    create();
                }

            } catch (WriterException e) {
                e.printStackTrace();
                try {
                    create();

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        }
    }
*/
    public void create(String cust_id) {
//        String qrInputTextGeneral = sctDatabaseHelper.getEncryptedCustomerId();
        String qrInputText = null;
        String encryptedMsg = cust_id;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                if (bitmap != null) {
                    myImage.setImageBitmap(bitmap);
                } else {
                    this.create(cust_id);
                }


            } catch (WriterException e) {
                e.printStackTrace();
                try {
                    this.create(cust_id);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().onBackPressed();
                   /* MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                  *//*  getFragmentManager().beginTransaction()
                            .replace(R.id.fragment,najirEnglish)
                            .commit();*//*
                    getActivity().setTitle("QPay");*/
                    return true;
                }
                return false;
            }
        });
    }
}
