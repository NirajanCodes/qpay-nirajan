package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.CustumClasses.PassCodeView;
import net.qpaysolutions.QPay.Utils.Utility;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 8/19/16.
 */
public class SetCodeFragment extends Fragment {
    private PassCodeView PassCodeView;
    private TextView promptView;
    private String enter_code;

    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*((MainActivity)getActivity()). setToolbar_text("Set Passcode");   ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);*/
        PassCodeView = (PassCodeView) view.findViewById(R.id.set_passcode);
        promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        PassCodeView.setTypeFace(typeFace);
        PassCodeView.setKeyTextColor(R.color.white);
        promptView.setTypeface(typeFace);
        bindEvents();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.set_passcode_main, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void bindEvents() {
        PassCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                try {
                    enter_code = text;
                    Log.d(Utility.TAG, "first enter : " + enter_code);
                    if (text.length() == 4) {
                        PassCodeView.setError(true);
                        try {
                            ConfirmPasscode confirmPasscode = new ConfirmPasscode();
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.bill_payment_container, confirmPasscode);
                            Bundle bundle = new Bundle();
                            bundle.putString("code", text);
                            confirmPasscode.setArguments(bundle);
                            fragmentTransaction.commit();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d("dinesh","onPause:");
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d("dinesh","onResume:");
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    MyCodeFragment najirEnglish = new MyCodeFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.bill_payment_container,najirEnglish)
                            .commit();
                    getActivity().setTitle("My Code");
                    return true;
                }
                return false;
            }
        });
    }
}
