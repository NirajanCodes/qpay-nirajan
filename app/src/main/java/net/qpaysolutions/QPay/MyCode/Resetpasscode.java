package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 8/19/16.
 */

public class Resetpasscode extends Fragment {
    private net.qpaysolutions.QPay.CustumClasses.PassCodeView PassCodeView;
    TextView promptView;
    LinearLayout passcode_background;
    QPayCustomerDatabase QPayCustomerDatabase;
    String pin_l;
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PassCodeView = (net.qpaysolutions.QPay.CustumClasses.PassCodeView) view.findViewById(R.id.set_passcode);
        promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        PassCodeView.setTypeFace(typeFace);
        PassCodeView.setKeyTextColor(R.color.black_shade);
        promptView.setTypeface(typeFace);
        bindEvents();
        promptView.setText("Enter Old Passcode");
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.set_passcode_main,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase =new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        pin_l= QPayCustomerDatabase.getPin();
    }

    private void bindEvents() {
        PassCodeView.setOnTextChangeListener(new net.qpaysolutions.QPay.CustumClasses.PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                try {
                    if (text.length() == 4) {
                        if (text.equals(pin_l)){
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            Resetpasscodefirst settingFragment = new Resetpasscodefirst();
                            fragmentTransaction .replace(R.id.bill_payment_container,settingFragment);
                            Bundle bundle = new Bundle();
                            bundle.putString("old_pin",text);
                            settingFragment.setArguments(bundle);
                            fragmentTransaction.commit();
                        }else {
                            PassCodeView.setError(true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                   /* MyCodeFragment najirEnglish = new MyCodeFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.bill_payment_container,najirEnglish)
                            .commit();
                    getActivity().setTitle("My Code");
                    return true;
                }
                return false;
            }*/
                    getActivity().onBackPressed();
                    return true;
                }
                return true;
            }
        });
    }
}
