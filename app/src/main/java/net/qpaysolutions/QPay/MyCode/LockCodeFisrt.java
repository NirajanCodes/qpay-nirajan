package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by deadlydragger on 8/21/16.
 */
public class LockCodeFisrt extends Fragment {
    private net.qpaysolutions.QPay.CustumClasses.PassCodeView PassCodeView;
    TextView promptView;
    private QPayCustomerDatabase QPayCustomerDatabase;
    String pin;
    Toolbar toolbar;
    TextView toolbar_text;
    LinearLayout passcode_background;

    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        pin = QPayCustomerDatabase.getPin();
//        qpayDatabases.updateFlagPin(" '2' ");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.set_passcode_main_locknowfirst, container, false);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar_text = (TextView) toolbar.findViewById(R.id.toolbar_text);
        toolbar_text.setText("Lock Now");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
        PassCodeView = (net.qpaysolutions.QPay.CustumClasses.PassCodeView) view.findViewById(R.id.set_passcode);
        promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        PassCodeView.setTypeFace(typeFace);
        PassCodeView.setKeyTextColor(R.color.black_shade);
        promptView.setTypeface(typeFace);
        bindEvents();
        passcode_background = (LinearLayout) view.findViewById(R.id.passcode_background);
    }

    private void bindEvents() {
        PassCodeView.setOnTextChangeListener(new net.qpaysolutions.QPay.CustumClasses.PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                try {
                    if (text.length() == 4) {
                        if (text.equals(pin)) {
                            QPayCustomerDatabase.updateFlagPin(" '1' ");
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            PassCodeView.setError(true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
