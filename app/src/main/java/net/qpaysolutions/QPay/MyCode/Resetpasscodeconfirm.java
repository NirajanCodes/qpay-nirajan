package net.qpaysolutions.QPay.MyCode;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

/**
 * Created by deadlydragger on 8/19/16.
 */
public class Resetpasscodeconfirm extends Fragment {
    private net.qpaysolutions.QPay.CustumClasses.PassCodeView PassCodeView;
    TextView promptView;
    String old_pin,new_pin;
    QPayCustomerDatabase QPayCustomerDatabase;
    String pin;
    FrameLayout pDialog;
    String app_id,cust_id,pin_response_random;
    LinearLayout passcode_background;
    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PassCodeView = (net.qpaysolutions.QPay.CustumClasses.PassCodeView) view.findViewById(R.id.set_passcode);
        promptView = (TextView) view.findViewById(R.id.promptview);
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        PassCodeView.setTypeFace(typeFace);
        PassCodeView.setKeyTextColor(R.color.black_shade);
        pDialog=(FrameLayout)view.findViewById(R.id.progressBar);
        promptView.setTypeface(typeFace);
        bindEvents();
        promptView.setText("Confirm new Passcode");
        old_pin=getArguments().getString("old_pin");
        new_pin=getArguments().getString("new_pin");

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.set_passcode_main,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        pin= QPayCustomerDatabase.getPin();
        app_id= QPayCustomerDatabase.getKeyAppId();
        cust_id= QPayCustomerDatabase.getCustomerID();
    }

    private void bindEvents() {
        PassCodeView.setOnTextChangeListener(new net.qpaysolutions.QPay.CustumClasses.PassCodeView.TextChangeListener() {
            @Override
            public void onTextChanged(String text) {
                try {
                    if (text.length() == 4) {
                        if (text.equals(new_pin) && old_pin.equals(pin)){
                            String dinesh= gen();
                            Log.d("dinesh",new_pin);
                            QPayCustomerDatabase.updatePin_l("'"+new_pin +"'");
                            new SendRandomPin(dinesh,new_pin).execute();
                            Toast.makeText(getContext(),"Passcode Updated",Toast.LENGTH_LONG).show();
                            MyCodeFragment myCodeFragment = new MyCodeFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.bill_payment_container, myCodeFragment)
                                    .commit();
                        }else {
                            Toast.makeText(getContext(),"Passcode mismatch",Toast.LENGTH_LONG).show();
                            Resetpasscode settingFragment = new Resetpasscode();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.bill_payment_container,settingFragment)
                                    .commit();
                            PassCodeView.setError(true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public String gen() {
        Random r = new Random(System.currentTimeMillis());
        return String.valueOf(1000 + r.nextInt(2000));
    }

    public class SendRandomPin extends AsyncTask<String, String, String> {
        String dinesh,pin_l;

        public SendRandomPin(String dinesh, String pin_l){
            this.dinesh=dinesh;
            this.pin_l=pin_l;
        }
        @Override
        protected String doInBackground(String... params) {
            String resultRandomPin;
            JSONObject jsonObject_pin = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {

                jsonObject_pin.put("app_id",app_id);
                jsonObject_pin.put("cust_id",cust_id);
                jsonObject_pin.put("reset_pin",dinesh);
                jsonObject_pin.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_pin.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG,"post pin : " + jsonObject_pin.toString());
                resultRandomPin = networkAPI.sendHTTPData(Constants.RANDOM_PIN,jsonObject_pin);
                Log.d(Utility.TAG,"response pin : " + resultRandomPin);
                JSONObject jsonObject = new JSONObject(resultRandomPin);
                JSONObject jsonObject1 = jsonObject.getJSONObject("storeresetpinResult");
                pin_response_random = jsonObject1.getString("status");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return pin_response_random;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setVisibility(View.VISIBLE);

        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.setVisibility(View.GONE);
            try {
                if (s != null && !s.isEmpty() && s.equals("00")) {
//                    qpayDatabases.updatePin_l(pin_l);
                    QPayCustomerDatabase.updateFlagPin(" '1' ");
                    Log.d(Utility.TAG, "random key " + dinesh);
                    QPayCustomerDatabase.updateRandom_pin(dinesh);
                    Toast.makeText(getContext(), "Successfully update your password ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Failed to set Password ", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){

                e.printStackTrace();
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    MyCodeFragment najirEnglish = new MyCodeFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.bill_payment_container,najirEnglish)
                            .commit();
                    getActivity().setTitle("My Code");
                    return true;
                }
                return false;
            }
        });
    }

}