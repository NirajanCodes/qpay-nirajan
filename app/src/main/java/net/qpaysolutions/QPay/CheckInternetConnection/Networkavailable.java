package net.qpaysolutions.QPay.CheckInternetConnection;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by deadlydragger on 6/29/16.
 */
public class Networkavailable {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(conMan.getActiveNetworkInfo() != null && conMan.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;
    }
}
