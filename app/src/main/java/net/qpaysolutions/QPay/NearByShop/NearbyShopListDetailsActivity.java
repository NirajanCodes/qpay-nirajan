package net.qpaysolutions.QPay.NearByShop;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by deadlydragger on 9/11/16.
 */
public class NearbyShopListDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private  Toolbar toolbar;
    private  String category_name, size;
    private  String address, city;
    private GoogleMap googleMapItme;
    private   TextView my_location, shop_near_you;
    private View myView;
    private  ArrayList<Shop> myList;
    private  ShopAdapter myAdapter;
    RecyclerView mRecyclerView;

    int position;
    GoogleMap googleMap;
    private String phoneCall;
    private FusedLocationProviderClient mFusedLocationClient;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_shop_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        category_name = getIntent().getStringExtra("category_name");
        size = getIntent().getStringExtra("size");
        position = getIntent().getIntExtra("position", 0);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbar_text = (TextView) toolbar.findViewById(R.id.title);
        toolbar_text.setText("Shop Location");
        my_location = (TextView) findViewById(R.id.my_location);
        shop_near_you = (TextView) findViewById(R.id.shop_near_you);

        myList = (ArrayList<Shop>) getIntent().getSerializableExtra("mylist");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        initilizeMap();
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setHasFixedSize(true);
        myAdapter = new ShopAdapter(NearbyShopListDetailsActivity.this, myList);
        mRecyclerView.setAdapter(myAdapter);
        shop_near_you.setText(String.valueOf(myList.size()));

    }

    private void initilizeMap() {
        try {
            if (googleMap == null) {
                MapFragment mapFragment = (MapFragment) getFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void createMarker(double latitude, double longitude, int image,GoogleMap googleMap) {

       googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(image))
        );
    }

    public String getAdress(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            Log.d(Utility.TAG, address + city + state + country + postalCode + knownName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address + "," + city;
    }

    @Override
    public void onMapReady(GoogleMap googleMapReady) {
        googleMap=googleMapReady;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        googleMapItme=googleMap;
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!Utility.hasExternalStoragePermission(NearbyShopListDetailsActivity.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(NearbyShopListDetailsActivity.this, PERMISSIONS, Tags.LOCATION);
            } else {
                setMap(googleMapReady);
            }
        } else {
            setMap(googleMapReady);

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    setMap(googleMap);

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give camera permission.", Toast.LENGTH_LONG).show();
                }
                break;
            case Tags.CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callToShop();

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Go to setting and give camera permission.", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    public void setMap(final GoogleMap googleMapItme){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            googleMapItme.setMyLocationEnabled(true);
        }
        mFusedLocationClient.getLastLocation()
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    // Logic to handle location object
                                    LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                    LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                    new GetAddress().execute();
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(10).build();
                                    googleMapItme.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                    googleMapItme.getUiSettings().setMyLocationButtonEnabled(true);
                                    if (googleMapItme == null) {
                                        Toast.makeText(getApplicationContext(),
                                                "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                } else {
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }
                            }
                        }


                );


        for (int i = 0; i < myList.size(); i++) {
            createMarker(myList.get(i).getLat(), myList.get(i).getLng(), R.drawable.ic_shop_marker,googleMapItme);

        }



    }
    private void callToShop() {

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneCall));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            startActivity(callIntent);
        }

    }
    public void requestCallPermission(String phone){
        phoneCall=phone;
        String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};

        ActivityCompat.requestPermissions(NearbyShopListDetailsActivity.this, PERMISSIONS, Tags.CALL);
    }


    public class GetAddress extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            String location_address = getAdress(Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng()));
            return location_address;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                my_location.setText(s);

            } catch (NullPointerException e) {

            }

        }
    }


    public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ViewHolder> {
        ArrayList<Shop> itemsData;
        private Context context;

        private int lastPosition = -1;

        public ShopAdapter(Context context, ArrayList<Shop> itemsData) {
            this.itemsData = itemsData;
            this.context = context;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            // create a new view
            View itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_view_shop, parent, false);
            // create ViewHolder

            ViewHolder viewHolder = new ViewHolder(itemLayoutView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            setAnimation(viewHolder.itemView, position);
            viewHolder.txtViewTitle.setText(itemsData.get(position).getName());
            viewHolder.number.setText(itemsData.get(position).getCell_phone());
            viewHolder.adddress.setText(itemsData.get(position).getAddress());
            viewHolder.merchant_pp.setText(itemsData.get(position).getRad());
            viewHolder.item_makecall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + itemsData.get(position).getCell_phone()));
                    if (ActivityCompat.checkSelfPermission(NearbyShopListDetailsActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        startActivity(callIntent);
                    }else {
                        requestCallPermission(itemsData.get(position).getCell_phone());
                    }

                }
            });
            myView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(myList.get(position).getLat(), myList.get(position).getLng())).zoom(18).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            });
        }

        // inner class to hold a reference to each item of RecyclerView
        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txtViewTitle, adddress, number;
            public TextView merchant_pp;
            public ImageView item_makecall;

            public ViewHolder(View itemLayoutView) {
                super(itemLayoutView);
                myView = itemLayoutView;
                txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
                number = (TextView) itemLayoutView.findViewById(R.id.item_number);
                adddress = (TextView) itemLayoutView.findViewById(R.id.item_address);
                merchant_pp = (TextView) itemLayoutView.findViewById(R.id.merchant_pp);
                item_makecall=(ImageView)itemLayoutView.findViewById(R.id.item_makecall);
            }
        }

        // Return the size of your itemsData (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return itemsData.size();
        }

        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(context, R.anim.scale);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
