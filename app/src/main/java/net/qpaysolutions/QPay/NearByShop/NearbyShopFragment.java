package net.qpaysolutions.QPay.NearByShop;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.FragmentCollection.MainFragment;
import net.qpaysolutions.QPay.FragmentCollection.MainFragmentNotCall;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.NearByTaxi.NearTaxiActivity;
import net.qpaysolutions.QPay.Resturant.ResturantActivity;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 9/11/16.
 */
public class NearbyShopFragment extends Fragment implements View.OnClickListener {
    LinearLayout food, fashion, ent, misc;
    TextView food_array, ent_array, misc_array, fashion_array;
    TextView balance;
    String cust_id, app_id;
    double lat, lng;
    QPayCustomerDatabase QPayCustomerDatabase;
    QPayProgressDialog qPayProgressDialog;
    ArrayList<Shop> getdealDataListFashion = new ArrayList<>();
    ArrayList<Shop> getdealDataListFood = new ArrayList<>();
    ArrayList<Shop> getdealDataListEnt = new ArrayList<>();
    ArrayList<Shop> getdealDataListmisc = new ArrayList<>();
    AHBottomNavigation bottomNavigation;
    public static final String MY_PREFS_NAME = "Mybalance";
    SharedPreferences sharePreference;
    private FusedLocationProviderClient mFusedLocationClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nearby_shop, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).setToolbar_text("Nearby Shop");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(false);
        food = (LinearLayout) view.findViewById(R.id.food);
        fashion = (LinearLayout) view.findViewById(R.id.fashion);
        ent = (LinearLayout) view.findViewById(R.id.ent);
        misc = (LinearLayout) view.findViewById(R.id.misc);
        balance = (TextView) view.findViewById(R.id.balance_shop);
        food_array = (TextView) view.findViewById(R.id.food_array);
        ent_array = (TextView) view.findViewById(R.id.entraint_array);
        misc_array = (TextView) view.findViewById(R.id.others_array);
        fashion_array = (TextView) view.findViewById(R.id.fashion_array);
        sharePreference = getActivity().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);

        balance.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
        lat = ((CustomerApplication) getActivity().getApplication()).getLat();
        lng = ((CustomerApplication) getActivity().getApplication()).getLng();
        buttonBar(view);

        food.setOnClickListener(this);
        fashion.setOnClickListener(this);
        ent.setOnClickListener(this);
        misc.setOnClickListener(this);

               if (Build.VERSION.SDK_INT >= 23) {
                   String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                   if (!Utility.hasExternalStoragePermission(getContext(), PERMISSIONS)) {
                       ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, Tags.LOCATION_SHOP);
                   } else {
                       shopLocation();
                   }
               } else {
                   shopLocation();

               }



    }

    public void shopLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null){
                                LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                new NearByShop().execute();
                            }
                        }
                    });
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.food:
                if (getdealDataListFood.size() > 0) {

                    Intent intent = new Intent(getActivity(), NearbyShopListActivity.class);
                    intent.putExtra("mylist", getdealDataListFood);
                    intent.putExtra("category_name", "Food");
                    intent.putExtra("size", String.valueOf(getdealDataListFood.size()));
                    startActivity(intent);
                } else {

                    new Utility().showSnackbar(v, "There are no nearby shops found around you.");

                }
                break;
            case R.id.fashion:
                if (getdealDataListFashion.size() > 0) {

                    Intent intent = new Intent(getActivity(), NearbyShopListActivity.class);
                    intent.putExtra("mylist", getdealDataListFashion);
                    intent.putExtra("category_name", "Fashion");
                    intent.putExtra("size", String.valueOf(getdealDataListFashion.size()));
                    startActivity(intent);
                } else {
                    new Utility().showSnackbar(v, "There are no nearby shops found around you.");


                }
                break;
            case R.id.ent:
                if (getdealDataListEnt.size() > 0) {
                    Intent intent = new Intent(getActivity(), NearbyShopListActivity.class);
                    intent.putExtra("mylist", getdealDataListEnt);
                    intent.putExtra("category_name", "Entertainment");
                    intent.putExtra("size", String.valueOf(getdealDataListEnt.size()));
                    startActivity(intent);
                } else {
                    new Utility().showSnackbar(v, "There are no nearby shops found around you.");
                }
                break;
            case R.id.misc:
                if (getdealDataListmisc.size() > 0) {
                    Intent intent = new Intent(getActivity(), NearbyShopListActivity.class);
                    intent.putExtra("mylist", getdealDataListmisc);
                    intent.putExtra("category_name", "Miscellaneous");
                    intent.putExtra("size", String.valueOf(getdealDataListmisc.size()));
                    startActivity(intent);
                } else {
                    new Utility().showSnackbar(v, "There are no nearby shops found around you.");
                }
                break;
        }
    }

    public void dialogFailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final TextView proceed_txt = (TextView) v.findViewById(R.id.proceed_txt);
        proceed_txt.setText("OK");
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, new MainFragmentNotCall())
                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
    public class NearByShop extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String nearbyShop = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                nearbyShop = networkAPI.sendHTTPData(Constants.NEAR_BY_SHOP, jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return nearbyShop;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getActivity());
            qPayProgressDialog.setCancelable(false);
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Utility.isBuild(s);
                qPayProgressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("TerminalNearbyResult");
                String status = jsonObject1.getString("status");
                String resp_code = jsonObject1.getString("resp_code");
                if (resp_code.equals("00")) {
                    JSONArray jsonArray = jsonObject1.getJSONArray("terminalsNearby");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        String category = jsonObject2.getString("category");
                        String cell_phone = jsonObject2.getString("cell_phone");
                        double lat = jsonObject2.getDouble("lat");
                        double lng = jsonObject2.getDouble("lng");
                        String name = jsonObject2.getString("name");
                        String rad = new Decimalformate().decimalFormate(jsonObject2.getString("rad"));
                        String url = jsonObject2.getString("imgUrl");
                        String address = jsonObject2.getString("address");
                        if (category.equals("Food")) {
                            Shop shop = new Shop();
                            shop.setCategory(category);
                            shop.setCell_phone(cell_phone);
                            shop.setLat(lat);
                            shop.setLng(lng);
                            shop.setName(name);
                            shop.setRad(rad);
                            shop.setAddress(address);
                            shop.setUrl(url);
                            getdealDataListFood.add(shop);
                        } else if (category.equals("Fashion")) {
                            Shop shop = new Shop();
                            shop.setCategory(category);
                            shop.setCell_phone(cell_phone);
                            shop.setLat(lat);
                            shop.setLng(lng);
                            shop.setName(name);
                            shop.setRad(rad);
                            shop.setAddress(address);
                            shop.setUrl(url);
                            getdealDataListFashion.add(shop);
                        } else if (category.equals("Entertainment")) {
                            Shop shop = new Shop();
                            shop.setCategory(category);
                            shop.setCell_phone(cell_phone);
                            shop.setLat(lat);
                            shop.setLng(lng);
                            shop.setName(name);
                            shop.setRad(rad);
                            shop.setAddress(address);
                            shop.setUrl(url);
                            getdealDataListEnt.add(shop);
                        } else if (category.equals("Miscellaneous")) {
                            Shop shop = new Shop();
                            shop.setCategory(category);
                            shop.setCell_phone(cell_phone);
                            shop.setLat(lat);
                            shop.setLng(lng);
                            shop.setName(name);
                            shop.setRad(rad);
                            shop.setAddress(address);
                            shop.setUrl(url);
                            getdealDataListmisc.add(shop);
                        }

                    }
                    food_array.setText(String.valueOf(getdealDataListFood.size()));
                    fashion_array.setText(String.valueOf(getdealDataListFashion.size()));
                    misc_array.setText(String.valueOf(getdealDataListmisc.size()));
                    ent_array.setText(String.valueOf(getdealDataListEnt.size()));
                } else {
                    dialogFailure("No Shop Found!", "There are no nearby shops found around you.");
                }

            } catch (Exception e) {
                e.printStackTrace();
                qPayProgressDialog.dismiss();
                dialogFailure("No Shop Found!", "There are no nearby shops found around you.");
            }
        }
    }

    public void buttonBar(View view) {
        bottomNavigation = (AHBottomNavigation) view.findViewById(R.id.bottom_navigation);
        AHBottomNavigationItem item = new AHBottomNavigationItem(R.string.tab_0, R.drawable.ic_action_footer_home_tab, R.color.colorPrimary);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_action_chat_tab, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_action_button_car, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_nearby_atm_tab, R.color.colorPrimary);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_action_restaurant_tab, R.color.colorPrimary);
        bottomNavigation.addItem(item);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item5);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item4);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setForceTint(false);
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setColored(true);
        try {
            if (GeneralPref.getChatCount() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getChatCount()), 1);
            }
            if (GeneralPref.getTaxi() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getTaxi()), 4);
            }
            if (GeneralPref.getAtm() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getAtm()), 3);
            }
            if (GeneralPref.getNoofRestro() > 2) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getNoofRestro() ), 2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                Log.d("dinesh", "tab selected" + position);
                if (position == 0) {
                    Intent history = new Intent(getActivity(), MainActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    getActivity().startActivity(history, bndlanimation);
                } else if (position == 1) {
                    try {
                        Intent taxi = new Intent(getActivity(), ChatActivity.class);
                        Bundle animationtaxi = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                        getActivity().startActivity(taxi, animationtaxi);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } else if (position == 2) {
                    Intent history = new Intent(getActivity(), ResturantActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    NearbyShopFragment.this.startActivity(history, bndlanimation);

                } else if (position == 3) {
                    Intent mycards = new Intent(getActivity(), MainActivity.class);
                    mycards.putExtra("Check", 7);
                    Bundle bundleanim = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    getActivity().startActivity(mycards, bundleanim);
                } else if (position == 4) {
                    Intent history = new Intent(getActivity(), NearTaxiActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    startActivity(history, bndlanimation);
                }
                return true;
            }
        });
    }
    @Override
    public void onResume() {
//        bottomNavigation.setCurrentItem(0);
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;
            }
        });
    }

}
