package net.qpaysolutions.QPay.NearByShop;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by deadlydragger on 7/24/17.
 */

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ViewHolder> {
    ArrayList<Shop> itemsData;
    private NearbyShopListActivity context;
    private int lastPosition = -1;

    public ShopListAdapter(NearbyShopListActivity context, ArrayList<Shop> itemsData) {
        this.itemsData = itemsData;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view_shop_list, parent, false);
        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
//            setAnimation(viewHolder.itemView, position);
        viewHolder.txtViewTitle.setText(itemsData.get(position).getName().toUpperCase());
        viewHolder.adddress.setText(itemsData.get(position).getAddress());
        viewHolder.distance.setText(new DecimalFormat("##.##").format(Double.valueOf(itemsData.get(position).getRad())));
        viewHolder.phone.setText(itemsData.get(position).getCell_phone());
        context.setPicassoImage(itemsData.get(position).getUrl(), viewHolder.merchant_pp);
        viewHolder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopupMenu(viewHolder.overflow, position);
            }
        });


    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewTitle, adddress, distance, phone;
        public ImageView merchant_pp, overflow;
        LinearLayout make_call;
        RelativeLayout itemClick;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            distance = (TextView) itemLayoutView.findViewById(R.id.item_distance);
            adddress = (TextView) itemLayoutView.findViewById(R.id.item_address);
            merchant_pp = (ImageView) itemLayoutView.findViewById(R.id.merchant_pp);
            phone = (TextView) itemLayoutView.findViewById(R.id.item_phone);
            make_call = (LinearLayout) itemLayoutView.findViewById(R.id.make_call);
            overflow = (ImageView) itemLayoutView.findViewById(R.id.overflow);
            itemClick=(RelativeLayout)itemLayoutView.findViewById(R.id.itemClick);
        }
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view, int position) {
        // inflate menu
        PopupMenu popup = new PopupMenu(context, view);

        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_submenu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));
        context.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        int position;

        public MyMenuItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + itemsData.get(position).getCell_phone()));
                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        context.startActivity(callIntent);
                    }else {
                        context.requestCallPermission(itemsData.get(position).getCell_phone());
                    }

                    return true;
                case R.id.action_play_next:
                    Intent intent = new Intent(context, NearbyShopItemMapActivity.class);
                    intent.putExtra("mylist", itemsData.get(position));
//                    intent.putExtra("position", position);
                    context.startActivity(intent);
                    return true;
                default:
            }
            return false;
        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {

        return itemsData.size();
    }


}
