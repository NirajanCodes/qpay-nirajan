package net.qpaysolutions.QPay.Card.ebanking;

/**
 * Created by deadlydragger on 3/18/18.
 */

public class EbankModel {
    String Id, LogoUrl, Name, Type, amount, description;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        LogoUrl = logoUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EbankModel() {
    }

    public EbankModel(String logoUrl, String name, String description) {
        LogoUrl = logoUrl;
        Name = name;
        this.description = description;
    }
}
