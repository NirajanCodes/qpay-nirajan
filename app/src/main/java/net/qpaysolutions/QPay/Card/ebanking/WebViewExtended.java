package net.qpaysolutions.QPay.Card.ebanking;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.webkit.WebView;

/**
 * Created by deadlydragger on 3/21/18.
 */

public class WebViewExtended extends WebView {

        public WebViewExtended(Context context) {
            super(context);
            initView(context);
        }

        public WebViewExtended(Context context, AttributeSet attrs) {
            super(context, attrs);
            initView(context);

        }

        private void initView(Context context){
            // i am not sure with these inflater lines
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // you should not use a new instance of MyWebView here
            // MyWebView view = (MyWebView) inflater.inflate(R.layout.custom_webview, this);
            this.getSettings().setJavaScriptEnabled(true) ;
            this.getSettings().setUseWideViewPort(true);
            this.getSettings().setLoadWithOverviewMode(true);
            this.getSettings().setDomStorageEnabled(true);

        }
    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        return new BaseInputConnection(this, false);
    }

}