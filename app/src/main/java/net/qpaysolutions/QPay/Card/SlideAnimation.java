package net.qpaysolutions.QPay.Card;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import net.qpaysolutions.QPay.Utils.Utility;


public abstract class SlideAnimation implements View.OnTouchListener {
    private View v;
    private final GestureDetector gestureDetector = new GestureDetector(new GestureListener());


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        this.v = v;
        return gestureDetector.onTouchEvent(event);
    }


    private final class GestureListener implements GestureDetector.OnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {

            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            Log.d(Utility.TAG, "motion event show press  : " + e);
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Log.d(Utility.TAG,"motion event single tap : "+ e);
            final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 600);
            v.setLayoutParams(lp);
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            Float e3 = Math.abs(e2.getX()) - Math.abs(e1.getX());
            Log.d(Utility.TAG, "motion event e3 : " + e2.getX());
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {

                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                Log.d(Utility.TAG, "diff x : " + e2.getX());
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        onSwipeRight();
                    } else {
                        onSwipeLeft();
                    }
                }

            } catch (Exception e) {

            }


            //    return super.onFling(e1, e2, velocityX, velocityY);
            return true;

        }
    }

    public void onSwipeRight() {
        //Toast.makeText(CustomerApplication.getContext(), "swipe Right", Toast.LENGTH_SHORT).show();
//
    }

    public void onSwipeLeft() {

        //Toast.makeText(CustomerApplication.getContext(), "swipe Left", Toast.LENGTH_SHORT).show();
    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
    }
}
