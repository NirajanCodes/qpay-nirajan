package net.qpaysolutions.QPay.Card;

import android.app.Dialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 5/25/16.
 */

public class AddCardFragment extends Fragment implements View.OnClickListener {
    private EditText cardno, date, thirdbox, cvs;
    private StringBuilder sb = new StringBuilder();
    private net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase qPayCustomerDatabase;
    private QPayProgressDialog pDialog;
    private String cust_id;
    private String web_call_verify;
    String first_4, last_4, card_numbr, date_month, date_year;
    String type = "S";
    private static final char space = '-';
    private Button regiuster_card;
    int database_four;

    private String app_id;
    final ArrayList<String> listOfPattern = new ArrayList<String>();
    TextView amount_pay;
    List<String> ncellRechargeAmountList;
    String selectedItem = "";
    LinearLayout hidekey_work_register;

    private int count_card_match = 0;
    public static String TAG = AddCardFragment.class.getCanonicalName();

    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename)));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_card_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            Toolbar toolbar = ((AddCardActivity) getActivity()).findViewById(R.id.toolbar);
            TextView title = ((AddCardActivity) getActivity()).findViewById(R.id.title);
            if (getArguments().getString("flag").equalsIgnoreCase("upi_card")) {
                title.setText("Add UPI Card");

            } else {
                title.setText("Add Card");

            }
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hideKeyword(getActivity());
                    getActivity().getSupportFragmentManager().popBackStack(CardListFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            });

            cardno = (EditText) view.findViewById(R.id.card_number);
            cardno.setText(getArguments().getString("card_num"));
            date = (EditText) view.findViewById(R.id.date);
            regiuster_card = (Button) view.findViewById(R.id.reghister_card_process);
            amount_pay = (TextView) view.findViewById(R.id.amount_pay);
            cvs = view.findViewById(R.id.cvs);
            hidekey_work_register = (LinearLayout) view.findViewById(R.id.hidekey_work_register);
            amount_pay.setOnClickListener(this);
            date.addTextChangedListener(mDateEntryWatcher);
            cardno.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (s.length() > 0 && (s.length() % 5) == 0) {
                        final char c = s.charAt(s.length() - 1);
                        if (space == c) {
                            s.delete(s.length() - 1, s.length());
                        }
                    }
                    // Insert char where needed.
                    if (s.length() > 0 && (s.length() % 5) == 0) {
                        char c = s.charAt(s.length() - 1);
                        // Only if its a digit where there should be a space we insert a space
                        if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 4) {
//                            s.insert(s.length() - 1, String.valueOf(space));

                            String s1 = s.toString().substring(0, s.length() - 1) + String.valueOf(space) + c;
                            cardno.setText(s1);
                        }
                    }
                    cardno.setSelection(cardno.getText().length());
                    if (s.length() == 23) {
                        date.requestFocus();
                    }
                }
            });
            regiuster_card.setOnClickListener(this);
            Log.d("dinesh", "add issue " + getArguments().getString("flag"));
            if (getArguments().getString("flag").equals("upi_card")) {
                LinearLayout accountType = view.findViewById(R.id.accountType);
                accountType.setVisibility(View.GONE);
                LinearLayout cvslayout = view.findViewById(R.id.cvslayout);
                cvslayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            getActivity().setTitle("Register Card");
            qPayCustomerDatabase = new QPayCustomerDatabase(getContext());
            qPayCustomerDatabase.getReadableDatabase();
            qPayCustomerDatabase.getWritableDatabase();
            cust_id = qPayCustomerDatabase.getCustomerID();
            database_four = qPayCustomerDatabase.checkLast4();
            app_id = qPayCustomerDatabase.getKeyAppId();
/*
            String ptVisa = "^4[0-9]{6,}$";
            listOfPattern.add(ptVisa);
            String ptMasterCard = "^5[1-5][0-9]{5,}$";
            listOfPattern.add(ptMasterCard);
            String ptAmeExp = "^3[47][0-9]{5,}$";
            listOfPattern.add(ptAmeExp);
            String ptDinClb = "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$";
            listOfPattern.add(ptDinClb);
            String ptDiscover = "^6(?:011|5[0-9]{2})[0-9]{3,}$";
            listOfPattern.add(ptDiscover);
            String ptSCT = "^[6-9][0-9]{12}(?:[0-9]{3})?$";
            listOfPattern.add(ptSCT);
            String ptJcb = "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$";
            listOfPattern.add(ptJcb);
*/
            String ptVisa = "^4[0-9]{6,}$";
            listOfPattern.add(ptVisa);
            String ptMasterCard = "^5[1-5][0-9]{5,}$";
            listOfPattern.add(ptMasterCard);
            String ptAmeExp = "^3[47][0-9]{5,}$";
            listOfPattern.add(ptAmeExp);
            String ptDinClb = "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$";
            listOfPattern.add(ptDinClb);
            String ptDiscover = "^6(?:011|5[0-9]{2})[0-9]{3,}$";
            listOfPattern.add(ptDiscover);
            String ptSCT = "^[6-9][0-9]{15}(?:[0-9]{3})?$";
            listOfPattern.add(ptSCT);
            String ptJcb = "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$";
            listOfPattern.add(ptJcb);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.amount_pay:
                showAmount(v);
                break;
            case R.id.reghister_card_process:
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(hidekey_work_register.getWindowToken(), 0);
                    date.clearFocus();
                    cardno.clearFocus();
                    String card_number = "";
                    card_number = cardno.getText().toString();
                    if (!card_number.isEmpty() || cardno.getText().toString() == null) {
                        last_4 = cardno.getText().toString().substring(Math.max(cardno.getText().toString().length() - 4, 0));
                        first_4 = cardno.getText().toString().substring(0, Math.min(cardno.getText().toString().length(), 4));
                        card_numbr = cardno.getText().toString().replaceAll("-", "");
                        date_month = date.getText().toString().substring(0, Math.min(date.getText().toString().length(), 2));
                        date_year = date.getText().toString().substring(Math.max(date.getText().toString().length() - 2, 0));
                        String expdate = date_year.concat(date_month);
                        DateFormat dateFormatter = new SimpleDateFormat("MM/yyyy");
                        dateFormatter.setLenient(false);
                        Date today = new Date();
                        String s = dateFormatter.format(today);
                        Date date2 = dateFormatter.parse(s);
                        Date date1 = dateFormatter.parse(date.getText().toString());
                        long difference = date1.getTime() - date2.getTime();
                        long differenceDates = difference / (24 * 60 * 60 * 1000);
                        int count = qPayCustomerDatabase.checkLast4Reg(last_4, expdate);

                        if (type.length() == 1 || cvs.getText().toString() != null) {
                            if (count > 0) {
                                custumdialogfailure("Already Registered!", "Card ending with last four digit " + last_4 + " already exists in your profile.");
//                                || card_numbr.substring(0,1).equals("7") || card_numbr.substring(0,1).equals("8") || card_numbr.substring(0,1).equals("9")||card_numbr.substring(0,1).equals("1")||card_numbr.substring(0,1).equals("2")||card_numbr.substring(0,1).equals("3")||card_numbr.substring(0,1).equals("5")
                            } else if (card_numbr.substring(0, 6).equals("999881")) {
                                new Utility().showSnackbar(v, "Card starting with first six digit " + card_numbr.substring(0, 6) + " is not allowed to register.");
                            } else {
                                if (!card_numbr.isEmpty() && card_numbr.length() >= 16 && !date.getText().toString().isEmpty() && date.getText().toString().length() == 7) {

                                   /* if (differenceDates > 0) {
                                        for (String p : listOfPattern) {
                                            if (card_numbr.matches(p)) {
                                                new Cardregistration(expdate, cust_id, type, card_numbr).execute();
                                                break;
                                            }
                                        }
                                    } else {
                                        cardno.getText().clear();
                                        date.getText().clear();
                                        new Utility().showSnackbar(v,"Enter Valid Card and Expiry Date.");

                                    }*/
                                    if (differenceDates >= 0) {
                                        for (String p : listOfPattern) {
                                            if (card_numbr.matches(p)) {
                                                if (getArguments().getString("flag").equalsIgnoreCase("upi_card")) {
                                                    new Cardregistration(date.getText().toString(), cust_id, type, card_numbr, cvs.getText().toString()).execute();
                                                } else {
                                                    new Cardregistration(expdate, cust_id, type, card_numbr, "").execute();
                                                }
                                                break;
                                            }
                                        }
                                    } else {
                                        cardno.getText().clear();
                                        date.getText().clear();
                                        new Utility().showSnackbar(v, "Enter Valid Card and Expiry Date.");
                                    }
                                } else {

                                    if (card_numbr.isEmpty() || card_numbr.length() < 16 || card_numbr.length() > 16) {
                                        cardno.setError("Please Insert Valid Input");
                                    }
                                    if (date.getText().toString().isEmpty()) {
                                        date.setError("Please Insert Valid Input");
                                    }
                                }
                            }
                        } else {

                            new Utility().showSnackbar(hidekey_work_register, "Select Card type.");
                        }

                    } else {
                        cardno.setError("Enter card number.");
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
                break;
        }
    }

    public class Cardregistration extends AsyncTask<String, String, JSONObject> {
        String e_date, id, act_type, card_no, cvn2;

        public Cardregistration(String e_date, String id, String act_type, String card_no, String cvn2) {

            this.e_date = e_date;
            this.id = id;
            this.act_type = act_type;
            this.card_no = card_no;
            this.cvn2 = cvn2;
        }

        @Override
        protected JSONObject doInBackground(String... uri) {
            JSONObject Response = null;
            String line = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                if (getArguments().getString("flag").equalsIgnoreCase("upi_card")) {
/*
                    jsonObject.put("appId", app_id);
                    jsonObject.put("id", id);
                    jsonObject.put("cardNo", card_no);
                    jsonObject.put("expDate", e_date);
                    jsonObject.put("cvn2", cvn2);*/
                    jsonObject.put("appId", app_id);
                    jsonObject.put("id", id);
                    jsonObject.put("cardNo", card_no);
                    jsonObject.put("expDate", e_date);
                    jsonObject.put("cvn2", cvn2);

                } else {

                    jsonObject.put("app_id", app_id);
                    jsonObject.put("cust_id", id);
                    jsonObject.put("lat", LatLngPref.getLat());
                    jsonObject.put("lng", LatLngPref.getLng());
                    jsonObject.put("card_no", card_no);
                    jsonObject.put("e_date", e_date);
                    jsonObject.put("act_type", type);
                    jsonObject.put("dev_token", GeneralPref.getToken());


                }


                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(TAG, "Post from php = " + jsonObject);
                String result = "";
                if (getArguments().getString("flag").equalsIgnoreCase("upi_card")) {
                    result = networkAPI.sendHTTPData(Constants.PANENROLLMENT, jsonObject);
                } else {
                    result = networkAPI.sendHTTPData(Constants.FOURTH_WEB_CALL, jsonObject);
                }
                Response = new JSONObject(result);
            } catch (Exception e) {
                Log.d(TAG, "Error Encountered");
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new QPayProgressDialog(getContext());
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(JSONObject s) {
            super.onPostExecute(s);
            String message = null;
            try {
                Log.d("dinesh", s.toString());
                pDialog.dismiss();
                if (getArguments().getString("flag").equalsIgnoreCase("upi_card")) {
                    JSONObject jsonObject = new JSONObject(s.toString());
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        JSONObject object = data.getJSONObject(0);
//                        String Token = object.getString("id");
                        String id = object.getString("id");
                        GeneralPref.setUpiId(id);
                        String imageUrl = object.getString("ImageUrl");
//                        qPayCustomerDatabase.insertProfile(Token/*"VISA"*/, date_year.concat(date_month), last_4, " upi ", "UPI Card", "www.google.com", card_numbr, "");
                        qPayCustomerDatabase.insertProfile(id/*"VISA"*/, date_year.concat(date_month), last_4, " upi ", "UPI Card", imageUrl, card_numbr, "");
                    } else {
                        custumdialogfailure("Card Registration Failure!", jsonObject.getString("message"));
                    }
                } else {
                    last_4 = card_no.substring(card_no.length() - 4);
                    JSONObject result = s.getJSONObject("registercardResult");
                    String status = result.getString("status");
                    String bankName = result.getString("bankName");
                    String cardUrl = null;
                    if (result.getString("cardUrl") == null || result.getString("cardUrl").isEmpty() || result.getString("cardUrl").equalsIgnoreCase("")) {
                        cardUrl = "www.google.com";
                    } else {
                        cardUrl = result.getString("cardUrl");
                    }
                    String card_token = result.getString("card_token");
                    String getLast_4 = qPayCustomerDatabase.getLastFourCardInfo();
                    message = result.getString("message");
                    if (!status.equals("null") && status != null && !status.isEmpty() && status.equals("00")) {
                        if (database_four == 0) {
                            if (first_4.startsWith("4")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"VISA"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));
                            } else if (first_4.startsWith("5")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"MC"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));
                            } else if (first_4.startsWith("6")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"SCT"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));
                            } else if (first_4.startsWith("9")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"QPAy"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));
                            }
                            custumdialogSucess("Card Register Success!", "Card Registered Successfully. In order to use this card verification is required.");
                            web_call_verify = Constants.VERIFY_CARD_TYPE + cust_id + "/" + last_4 + "/" + "500_00" + "/";
                            Log.d(TAG, "web call for verifying card :" + web_call_verify);
                        } else if (database_four > 0 /*&& !getLastFourCardInfo.equals(last_4)*/) {
                            if (first_4.startsWith("4")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"VISA"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));

                            } else if (first_4.startsWith("5")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"MC"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));

                            } else if (first_4.startsWith("6")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"SCT"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));
                            } else if (first_4.startsWith("9")) {
                                qPayCustomerDatabase.insertProfile(card_token/*"QPAy"*/, date_year.concat(date_month), last_4, " R ", bankName, cardUrl, card_numbr, act_type.substring(0, 1));
                            } else {
                                Toast.makeText(getContext(), "failed to insert", Toast.LENGTH_LONG).show();
                            }
                            web_call_verify = Constants.VERIFY_CARD_TYPE + cust_id + "/" + last_4 + "/" + "500_00" + "/";
                            Log.d(TAG, "web call for verifying card :" + web_call_verify);
                            custumdialogSucess("Card Register Success!", "Card Registered Successfully. In order to use this card verification is required.");
                        } else {
                            custumdialogfailure("Card Registration Failure!", "Something went wrong please recheck and register your card.");
                        }
                    } else if (status.equals("04")) {
                        custumdialogfailure("Card Registration Failure!", message);
                    } else {
                        custumdialogfailure("Card Registration Failure!", message);
                    }
                    Log.d(Utility.TAG, s.toString());
                }
            } catch (Exception e) {
                pDialog.dismiss();
                pDialog.hide();
                e.printStackTrace();
                custumdialogfailure("Card Registration Failure!", message);
            }
        }
    }

    public void custumdialogfailure(String title, String number) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_image = (ImageView) v.findViewById(R.id.dialog_image);
        dialog_image.setImageResource(R.drawable.ic_card_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
               /* startActivity(new Intent(getActivity(),MainActivity.class));
                getActivity().finish();*/
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogSucess(String title_diaplay, String number) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView) v.findViewById(R.id.dialog_icon);
        dialog_icon.setImageResource(R.drawable.ic_card_verification_success);
        title.setText(title_diaplay);

        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {


//
//                    if (BillPayActivity.fragmentList.size() > 0) {
//                        android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                        for (Fragment activeFragment : BillPayActivity.fragmentList) {
//                            transaction.remove(activeFragment);
//                        }
//                        BillPayActivity.fragmentList.clear();
//                        transaction.commit();
//                    }
//                    FrameLayout frameLayout=((BillPayActivity)getActivity()).findViewById(R.id.bill_payment_container);
//                    frameLayout.removeAllViewsInLayout();
//                    AddManageCardFragment cardProfileMain = new AddManageCardFragment();
//                    getFragmentManager().beginTransaction()
//                            .replace(R.id.bill_payment_container, cardProfileMain)
//                            .addToBackStack(AddManageCardFragment.TAG)
//                            .commit();
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                        }
//                    });

                    try {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getFragmentManager().popBackStack(CardListFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                            }
                        }, 1000);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getFragmentManager().popBackStack(AddManageCardFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                            }
                        }, 1000);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.card_container, new AddManageCardFragment())
                                .addToBackStack(AddManageCardFragment.TAG).commit();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*super.*/
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
//        Fragment cardProfileMain = new AddManageCardFragment();
//        getFragmentManager().beginTransaction().setCustomAnimations(R.anim.animation_one_activity, R.anim.animation_two_activity)
//                .replace(R.id.bill_payment_container, cardProfileMain)
//                .addToBackStack(AddManageCardFragment.TAG)
//                .commit();
        getFragmentManager().popBackStack(AddManageCardFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private TextWatcher mDateEntryWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            boolean isValid = true;
            if (working.length() == 2 && before == 0) {
                if (Integer.parseInt(working) < 1 || Integer.parseInt(working) > 12) {
                    isValid = false;
                } else {
                    working += "/";
                    date.setText(working);
                    date.setSelection(working.length());
                }
            } else if (working.length() == 7 && before == 0) {
                String enteredYear = working.substring(3);
                int currentYear = Calendar.getInstance().get(Calendar.YEAR);
                if (Integer.parseInt(enteredYear) < currentYear) {
                    isValid = false;
                }

                date.clearFocus();
            } else if (working.length() != 7) {
                isValid = false;
            }

            if (!isValid) {
                date.requestFocus();
                date.setError("Enter a valid date: MM/YYYY");
            } else {
                date.requestFocus();
                date.setError(null);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

            if (s.length() == 7) {


                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(date.getWindowToken(), 0);

            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    };


    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                    CardProfileMain najirEnglish = new CardProfileMain();
//                    getFragmentManager().beginTransaction()
//                            .replace(R.id.fragment,najirEnglish)
//                            .commit();
//                    getActivity().setTitle("QPay");
                    Utils.hideKeyword(getActivity());
                    getActivity().getSupportFragmentManager().popBackStack(AddManageCardFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                }
                return false;
            }
        });
    }

    public void showAmount(View view) {
        String[] utl = getResources().getStringArray(R.array.acc_type);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(utl));

        amount_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(hidekey_work_register.getWindowToken(), 0);

                final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int month, String dateDesc) {
                        selectedItem = dateDesc;
                        if (selectedItem.equals("Saving")) {
                            type = "S";
                            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(hidekey_work_register.getWindowToken(), 0);
                        } else if (selectedItem.equals("Checking")) {
                            type = "C";
                            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(hidekey_work_register.getWindowToken(), 0);
                        }
                        amount_pay.setText(dateDesc);
                    }
                }).textConfirm("DONE")
                        .textCancel("CANCEL")
                        .btnTextSize(16)
                        .viewTextSize(16)
                        .setValues(ncellRechargeAmountList)
                        .build();
                pickerPopWin.showPopWin(getActivity());
            }
        });
    }


}
