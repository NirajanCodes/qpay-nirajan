package net.qpaysolutions.QPay.Card.ebanking;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deadlydragger on 4/17/18.
 */

public class PostFormData extends AsyncTask<String, String, String> {
    String url;
    JSONObject jsonObject;
    EBankActivity eBankActivity;
    public PostFormData(String url, JSONObject jsonObject,EBankActivity eBankActivity){
        this.url=url;
        this.jsonObject=jsonObject;
        this.eBankActivity=eBankActivity;

    }
    @Override
    protected String doInBackground(String... strings) {
        try {
            Log.d("dinesh","url :"+url+" json :"+jsonObject);
            return loadHtmlContent(jsonObject,url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
//            String html = new String(s.string(), "UTF-8");
            Utility.isBuild(s);
            eBankActivity.loadHtml(s);
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    private String loadHtmlContent(JSONObject jsonObject,String url){

        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<body onload='form.submit()'>");
        sb.append(String.format("<form name='form' action='%s' method='post'>", url,"post"));
        try {

            if(jsonObject.has("APPID") && jsonObject.getString("APPID")!=null && !jsonObject.getString("APPID").isEmpty()) {
                sb.append(String.format("<input type='hidden' name='APPID' value='%s'>", jsonObject.getString("APPID")));
                sb.append(String.format("<input type='hidden' name='APPNAME' value='%s'>", jsonObject.getString("APPNAME")));
                sb.append(String.format("<input type='hidden' name='MERCHANTID' value='%s'>", jsonObject.getString("MERCHANTID")));
                sb.append(String.format("<input type='hidden' name='PARTICULARS' value='%s'>", jsonObject.getString("PARTICULARS")));
                sb.append(String.format("<input type='hidden' name='REFERENCEID' value='%s'>", jsonObject.getString("REFERENCEID")));
                sb.append(String.format("<input type='hidden' name='REMARKS' value='%s'>", jsonObject.getString("REMARKS")));
                sb.append(String.format("<input type='hidden' name='TOKEN' value='%s'>", jsonObject.getString("TOKEN")));
                sb.append(String.format("<input type='hidden' name='TXNAMT' value='%s'>", jsonObject.getString("TXNAMT")));
                sb.append(String.format("<input type='hidden' name='TXNCRNCY' value='%s'>", jsonObject.getString("TXNCRNCY")));
                sb.append(String.format("<input type='hidden' name='TXNDATE' value='%s'>", jsonObject.getString("TXNDATE")));
                sb.append(String.format("<input type='hidden' name='TXNID' value='%s'>", jsonObject.getString("TXNID")));
            }else if(jsonObject.has("Amount") && jsonObject.getString("Amount")!=null && !jsonObject.getString("Amount").isEmpty()){
                sb.append(String.format("<input type='hidden' name='Amount' value='%s'>", jsonObject.getString("Amount")));
                sb.append(String.format("<input type='hidden' name='Nonsecure' value='%s'>", jsonObject.getString("Nonsecure")));
                sb.append(String.format("<input type='hidden' name='currencyCode' value='%s'>", jsonObject.getString("currencyCode")));
                sb.append(String.format("<input type='hidden' name='hashValue' value='%s'>", jsonObject.getString("hashValue")));
                sb.append(String.format("<input type='hidden' name='invoiceNo' value='%s'>", jsonObject.getString("invoiceNo")));
                sb.append(String.format("<input type='hidden' name='paymentGatewayID' value='%s'>", jsonObject.getString("paymentGatewayID")));
                sb.append(String.format("<input type='hidden' name='productDesc' value='%s'>", jsonObject.getString("productDesc")));
                sb.append(String.format("<input type='hidden' name='userDefined1' value='%s'>", jsonObject.getString("userDefined1")));
                sb.append(String.format("<input type='hidden' name='userDefined2' value='%s'>", jsonObject.getString("userDefined2")));
                sb.append(String.format("<input type='hidden' name='userDefined3' value='%s'>", jsonObject.getString("userDefined3")));
                sb.append(String.format("<input type='hidden' name='userDefined4' value='%s'>", jsonObject.getString("userDefined4")));
                sb.append(String.format("<input type='hidden' name='userDefined5' value='%s'>", jsonObject.getString("userDefined5")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sb.append(String.format("</form>"));
        sb.append("</body>");
        sb.append("</html>");
        return sb.toString();
    }

}
