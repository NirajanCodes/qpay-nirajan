package net.qpaysolutions.QPay.Card.ebanking;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

/**
 * Created by deadlydragger on 3/18/18.
 */

public class PaymentGateWayCustTopup extends AsyncTask<String,String,String> {
    DepositEBankingFragment depositEBankingFragment;
    Context context;

    public PaymentGateWayCustTopup(DepositEBankingFragment depositEBankingFragment,Context context) {
        this.depositEBankingFragment = depositEBankingFragment;
        this.context=context;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            Utility.isBuild(depositEBankingFragment.postJson().toString());
            Log.d("dinesh","request: "+depositEBankingFragment.postJson().toString());

            return NetworkAPI.sendHTTPData(Constants.PAYMENT_GATWAY_TOPUP, depositEBankingFragment.postJson());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        depositEBankingFragment.showDialog();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        depositEBankingFragment.hideDialog();
        try {
            Utility.isBuild(s);
            Log.d("dinesh","response: "+s);
            JSONObject jsonObject = new JSONObject(s);
            boolean success = jsonObject.getBoolean("success");
            String status = jsonObject.getString("status");
            if (success){
                if (status.equals("00")){
                    if (jsonObject.getJSONArray("data").getJSONObject(0).getBoolean("IsPost")){
                        depositEBankingFragment.openWebViewPostForm(jsonObject.getJSONArray("data").getJSONObject(0).getString("ProcessUrl"),jsonObject.getJSONArray("data").getJSONObject(0).getJSONObject("RequestData"));
                    }else {
                        depositEBankingFragment.openWebView(jsonObject.getJSONArray("data").getJSONObject(0).getString("ProcessUrl"));
                    }
                }else {
                    Toast.makeText(depositEBankingFragment.getContext(),"Failed to load fund",Toast.LENGTH_LONG).show();
                }
            }else {
                Toast.makeText(depositEBankingFragment.getContext(),"Failed to load fund",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context,"Failed to load fund",Toast.LENGTH_LONG).show();
        }
    }
}
