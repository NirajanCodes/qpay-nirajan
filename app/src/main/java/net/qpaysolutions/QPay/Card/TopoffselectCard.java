package net.qpaysolutions.QPay.Card;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
/**
 * Created by deadlydragger on 8/10/16.
 */
public class TopoffselectCard extends Fragment implements AdapterView.OnItemClickListener {
    private QPayCustomerDatabase QPayCustomerDatabase;
    private   int checkrow;
    private  ListView listViewDialog;
    private   Cursor cursor;
    private  String last_4;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();
        SQLiteDatabase db = QPayCustomerDatabase.getWritableDatabase();
         cursor = db.rawQuery(" select * from profile where act_flag  = 'V'  ", null);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.alrt_check_dialog,container,false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listViewDialog = (ListView) view.findViewById(R.id.listView1);
        CardListTopoff cardListAdapter = new CardListTopoff(getContext(), cursor,TopoffselectCard.this);
        listViewDialog.setAdapter(cardListAdapter);
        listViewDialog.setItemsCanFocus(true);
        listViewDialog.setOnItemClickListener(this);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
       TopUpFragment topUpFragment = new TopUpFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("id", String.valueOf(id));
        topUpFragment.setArguments(bundle);
        fragmentTransaction  .replace(R.id.fragment,topUpFragment);
        fragmentTransaction .commit();
        getActivity().setTitle("Top-Up");
    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    CardProfileMain najirEnglish = new CardProfileMain();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment,najirEnglish)
                            .commit();
                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;
            }
        });
    }
}
