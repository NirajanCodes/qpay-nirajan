package net.qpaysolutions.QPay.Card;

/**
 * Created by QPay on 1/20/2019.
 */

public class CardPOJO {

    private String cardNumber;
    private String expiryDate;
    private String cardName;
    private String cardType;
    private String actFlag;
    private String last4DigitCardNumber;
    private String profileName;
    private String imgId;
    private String cardNature;
    private String cardBalance;
    private String cardKnownDate;
    private String cardId;



    public String getCardId() {
        return cardId;
    }

    public CardPOJO setCardId(String cardId) {
        this.cardId = cardId;
        return this;
    }

    public String getCardType() {
        return cardType;
    }

    public CardPOJO setCardType(String cardType) {
        this.cardType = cardType;
        return this;
    }

    public String getActFlag() {
        return actFlag;
    }

    public CardPOJO setActFlag(String actFlag) {
        this.actFlag = actFlag;
        return this;
    }

    public String getLast4DigitCardNumber() {
        return last4DigitCardNumber;
    }

    public CardPOJO setLast4DigitCardNumber(String last4DigitCardNumber) {
        this.last4DigitCardNumber = last4DigitCardNumber;
        return this;
    }

    public String getProfileName() {
        return profileName;
    }

    public CardPOJO setProfileName(String profileName) {
        this.profileName = profileName;
        return this;
    }

    public String getImgId() {
        return imgId;
    }

    public CardPOJO setImgId(String imgId) {
        this.imgId = imgId;
        return this;
    }

    public String getCardNature() {
        return cardNature;
    }

    public CardPOJO setCardNature(String cardNature) {
        this.cardNature = cardNature;
        return this;
    }

    public String getCardBalance() {
        return cardBalance;
    }

    public CardPOJO setCardBalance(String cardBalance) {
        this.cardBalance = cardBalance;
        return this;
    }

    public String getCardKnownDate() {
        return cardKnownDate;
    }

    public CardPOJO setCardKnownDate(String cardKnownDate) {
        this.cardKnownDate = cardKnownDate;
        return this;
    }

    public String getCardName() {
        return cardName;
    }

    public CardPOJO setCardName(String cardName) {
        this.cardName = cardName;
        return this;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public CardPOJO setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public CardPOJO setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
        return this;
    }
}
