package net.qpaysolutions.QPay.Card.animations;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import net.qpaysolutions.QPay.Card.CardPOJO;

import java.util.List;

/**
 * Created by QPay on 1/20/2019.
 */

public class CardListener implements CardStack.CardEventListener {
    private Context context;
    private List<CardPOJO> mCardStack;

    public CardListener(Context context,  List<CardPOJO> mCardStack) {
        this.context = context;
        this.mCardStack = mCardStack;
    }

    @Override
    public boolean swipeEnd(int section, float distance) {
        //if "return true" the dismiss animation will be triggered
        //if false, the card will move back to stack
        //distance is finger swipe distance in dp

        //the direction indicate swipe direction
        //there are four directions
        //  0  |  1
        // ----------
        //  2  |  3
        Log.d("rae", "swipeEnd:" + section + "-" + distance);

        return (distance > 300) ? true : false;
    }

    @Override
    public boolean swipeStart(int section, float distance) {
        Toast.makeText(context, "swipeStart", Toast.LENGTH_SHORT).show();
        Log.d("rae", "swipeStart:" + section + "-" + distance);

        return false;
    }

    @Override
    public boolean swipeContinue(int section, float distanceX, float distanceY) {
        Toast.makeText(context, "swipeContinue", Toast.LENGTH_SHORT).show();
        Log.d("rae", "swipeContinue:" + section + "-" + distanceX + "-" + distanceY);
        return false;
    }

    @Override
    public void discarded(final int mIndex, final int direction) {
        Toast.makeText(context, "discarded", Toast.LENGTH_SHORT).show();
        Log.d("rae", "discarded:" + mIndex + "-" + direction);
//        AlertDialog.Builder builder;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
//        } else {
//            builder = new AlertDialog.Builder(context);
//        }
//        builder.setTitle("Delete Card")
//                .setMessage("Are you sure you want to delete this card?")
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        System.out.println("Position:: "+mIndex);
//                        dialog.dismiss();
//                    }
//                })
//                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
    }

    @Override
    public void topCardTapped() {
        Log.d("rae", "topCardTapped");
        Toast.makeText(context, "topCardTapped", Toast.LENGTH_SHORT).show();
    }
}
