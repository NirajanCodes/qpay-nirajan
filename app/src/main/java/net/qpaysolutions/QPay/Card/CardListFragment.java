package net.qpaysolutions.QPay.Card;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.Billpayment.BillPayActivity;
import net.qpaysolutions.QPay.Card.ebanking.DepositEBankingFragment;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;

/**
 * Created by QPay on 6/14/2018.
 */

public class CardListFragment extends Fragment implements View.OnClickListener {

    private TextInputEditText detectCard;
    private ImageView imageView;
    private Button next;
    private String[] name;
    private Intent intent;
    private View v = null;
    private String card_name = null;
    private int length, validLength;
    private LinearLayout parentView = null;
    private ArrayList<ImageView> arrayList = new ArrayList<>(4);
    private ImageView master, sct, union, visa, middleImage;
    public static String TAG = CardListFragment.class.getCanonicalName();
    private String chooseFragment = null;
    private String con = null;
    private String middle = null;
    private String last = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            Toolbar toolbar = ((AddCardActivity) getActivity()).findViewById(R.id.toolbar);
            TextView title = ((AddCardActivity) getActivity()).findViewById(R.id.title);
            title.setText("Enter Card Number");
            toolbar.setNavigationOnClickListener(v -> {
                hideKeyboard(getActivity());
                getFragmentManager().popBackStack(AddManageCardFragment.TAG, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
            });
        }
        catch(Exception e) {

        }
        detectCard = view.findViewById(R.id.detect_card);
        imageView = view.findViewById(R.id.imageView);
        middleImage = view.findViewById(R.id.iv_middle_image);
        next = view.findViewById(R.id.next);
        next.setOnClickListener(this);
        parentView = getView().findViewById(R.id.animationView);
        createView();
        customLayout();
        stringList();

        intent = new Intent(getActivity(), BillPayActivity.class);
        detectCard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                length = s.length();
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean flag = false;

                if ((s.length() < 1) && s.length() < length) {
                    middleImage.setVisibility(View.GONE);
                    customLayout();
                }

                intent.putExtra("id", "sct");
                if ((s.length() == 1 || s.length() == 7 || s.length() == 9 || s.length() == 12 || s.length() == 19) && s.length() > length) {
                    card_name = "dummy";
                    for (int i = 0; i < name.length; i++) {
                        try {
                            String first = name[i].substring(0, 4);
                            if (name[i].length() == 6) {
                                middle = name[i].substring(4, 6);
                                con = first + "-" + middle;
                            } else if (name[i].length() == 8) {
                                middle = name[i].substring(4, 8);
                                con = first + "-" + middle;
                            } else if (name[i].length() == 10) {
                                middle = name[i].substring(4, 8);
                                last = name[i].substring(8, 10);
                                con = first + "-" + middle + "-" + last;
                            }
                            if (name[i].length() == 6) {
                                if (con.contains(s.subSequence(0, 6))) {

                                    card_name = "sct";
                                    if (String.valueOf(s.charAt(0)).contains("4")) {
                                        card_name = "visa";
                                    }
                                    if (String.valueOf(s.charAt(0)).contains("5")) {
                                        card_name = "master";
                                    }
                                    chooseFragment = "sct";
                                    intent.putExtra("flag", "sct");
                                    flag = true;
                                }
                            }

                            if (name[i].length() == 8) {
                                if (con.contains(s.subSequence(0, 8))) {

                                    card_name = "sct";
                                    if (String.valueOf(s.charAt(0)).contains("4")) {
                                        card_name = "visa";
                                    }
                                    if (String.valueOf(s.charAt(0)).contains("5")) {
                                        card_name = "master";
                                    }
                                    chooseFragment = "sct";
                                    intent.putExtra("flag", "sct");
                                    flag = true;
                                }
                            }

                            if (name[i].length() == 10) {
                                if (con.contains(s.subSequence(0,12))) {

                                    card_name = "sct";
                                    if (String.valueOf(s.charAt(0)).contains("4")) {
                                        card_name = "visa";
                                    }
                                    if (String.valueOf(s.charAt(0)).contains("5")) {
                                        card_name = "master";
                                    }
                                    chooseFragment = "sct";
                                    intent.putExtra("flag", "sct");
                                    flag = true;
                                }
                            }if(name[i].equalsIgnoreCase(s.toString())){
                                chooseFragment="sct";
                                card_name="sct";
                                intent.putExtra("flag", "upi");

                            }
                            else if (!flag && s != null) {
//                            intent.putExtra("flag", "sct");
                                chooseFragment=null;
                                switch (String.valueOf(s.charAt(0))) {
                                    case "2":
                                        //changd temporarily
                                        //card_name = "upi";
                                        //
                                        card_name = "upi";
                                        chooseFragment = "other";
                                        intent.putExtra("flag", "sct");
                                        break;
                                    case "4":
                                        card_name = "visa";
                                        chooseFragment = "other";
                                        intent.putExtra("flag", "others");
                                        break;
                                    case "5":
                                        card_name = "master";
                                        chooseFragment = "other";
                                        intent.putExtra("flag", "others");
                                        break;
                                    case "6":
                                        //changd temporarily
                                        //card_name = "upi";
                                        //
                                        card_name = "upi";
                                        //card_name = "upi";
                                        chooseFragment = "other";
                                        intent.putExtra("flag", "upi");
                                        break;
                                }
                            }

                            if (card_name.equalsIgnoreCase("sct")) {
                                middleImage.setImageResource(R.drawable.ic_sct_card);
                                scaleAnimation(1, 0.0f, 0.25f);
                            } else if (card_name.equalsIgnoreCase("visa")) {
                                middleImage.setImageResource(R.drawable.ic_visa_card);
                                scaleAnimation(2, 0.25f, 0.25f);
                            } else if (card_name.equalsIgnoreCase("master")) {
                                middleImage.setImageResource(R.drawable.ic_master_card);
                                scaleAnimation(3, 0.5f, 0.25f);
                            } else if (card_name.equalsIgnoreCase("upi")) {
                                middleImage.setImageResource(R.drawable.ic_upi_card);
                                scaleAnimation(0, -0.25f, 0.25f);
                            }
                            if (length < s.length()) {
                                for (int j = 0; j < arrayList.size(); j++) {
                                    if (arrayList.get(j) == v) {
                                        arrayList.get(j).setAlpha(1f);
                                    } else {
                                        arrayList.get(j).setAlpha(0.1f);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.d(Utility.TAG, "error  : " + e);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    validLength = s.length();
                    if (s.length() % 5 == 0 && length < s.length()) {
                        String text = detectCard.getText().append("-").toString();
                        String start = text.substring(0, s.length() - 1);
                        String end = text.substring(s.length() - 1);
                        detectCard.setText(start + "-" + end);
                        detectCard.setSelection(s.length() + 1);
                    }
                    if (s.length() > 22) {
                        next.requestFocus();
                        hideKeyboard(getActivity());
                    }
                } catch (Exception e) {
                    Log.d(Utility.TAG, "error : " + e);
                }
            }
        });
    }

    public void scaleAnimation(int arrayIndex, float pivotValueX, float pivotValueY) {

        middleImage.setVisibility(View.VISIBLE);
        middleImage.setScaleX(2f);
        middleImage.setScaleY(2f);
       /*

        anim = new ScaleAnimation(1f, 2f, 1f, 2f, Animation.RELATIVE_TO_PARENT, pivotValueX, Animation.RELATIVE_TO_PARENT, pivotValueY);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setZAdjustment(Animation.ZORDER_TOP);
        v.startAnimation(anim);

        */
        v = arrayList.get(arrayIndex);
        parentView.removeAllViews();
        parentView.setWeightSum(3f);
        int j = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) != v) {
                parentView.addView(arrayList.get(i), j);
                j++;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (chooseFragment != null && validLength > 18) {
            String cardNumber = detectCard.getText().toString();
//            intent.putExtra("card_num", detectCard.getText().toString());
//            startActivity(intent);
            hideKeyboard(getActivity());
            if (chooseFragment.equalsIgnoreCase("sct")) {
                AddCardFragment addCardFragment = new AddCardFragment();
                Bundle bundle = new Bundle();
                bundle.putString("flag", "sct");
                bundle.putString("card_num", cardNumber);
                addCardFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.card_container, addCardFragment).addToBackStack(TAG).commit();
            } else if (chooseFragment.equalsIgnoreCase("upi")) {
                AddCardFragment addCardFragment = new AddCardFragment();
                Bundle bundle = new Bundle();
                bundle.putString("flag", "upi_card");
                bundle.putString("card_num", cardNumber);
                addCardFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.card_container, addCardFragment).addToBackStack(TAG).commit();
            } else if (chooseFragment.equalsIgnoreCase("other")) {
                DepositEBankingFragment depositEBankingFragment = new DepositEBankingFragment();
                Bundle bundle = new Bundle();
                bundle.putString("name", "other");
                depositEBankingFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.card_container, depositEBankingFragment).addToBackStack(TAG).commit();
            }
        } else {
            detectCard.setError("Invalid Card Number");
        }
    }

    public static void hideKeyboard(Activity activity) {

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void createView() {

        final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(0, 0, 10, 0);
        lp.weight = 1;
        lp.gravity = Gravity.CENTER;

        union = new ImageView(getActivity());
        union.setImageResource(R.drawable.ic_upi_card);
        union.setLayoutParams(lp);

        sct = new ImageView(getActivity());
        sct.setImageResource(R.drawable.ic_sct_card);
        sct.setLayoutParams(lp);

        visa = new ImageView(getActivity());
        visa.setImageResource(R.drawable.ic_visa_card);
        visa.setLayoutParams(lp);

        master = new ImageView(getActivity());
        master.setImageResource(R.drawable.ic_master_card);
        master.setLayoutParams(lp);

    }

    public void customLayout() {
        arrayList.clear();
        arrayList.add(0, union);
        arrayList.add(1, sct);
        arrayList.add(2, visa);
        arrayList.add(3, master);
        parentView.removeAllViews();
        parentView.setWeightSum(4f);
        parentView.addView(arrayList.get(0), 0);
        parentView.addView(arrayList.get(1), 1);
        parentView.addView(arrayList.get(2), 2);
        parentView.addView(arrayList.get(3), 3);
    }

    public void stringList() {

        name = new String[]{
/*
                "475396",
                "475183",
                "636697",
                "637053",
                "636572",
                "62347717",
                "606136",
                "524682",
                "524683",
                "636584",
                "627765",
                "347708",
                "637729",
                "62347707",
                "604810",
                "637727",
                "62347706",
                "627707",
                "62347715",
                "539933",
                "554391",
                "606175",
                "62347709",
                "606288",
                "637067",
                "629903",
                "636533",
                "62347714",
                "605095",
                "62347716",
                "629866",
                "605106",
                "605914",
                "62347711",
                "636519",
                "636686",
                "606311",
                "636372",
                "627889",
                "418240",
                "405019",
                "636991",
                "606025",
                "605098",
                "636397",
                "636345",
                "637029",
                "62347712",
                "466046",
                "605001",
                "636263",
                "62347718",
                "605158",
                "62347713",
                "629976",
                "63767630",
                "63767610",
                "466044",
                "605139",
                "606080",
                "637752",
                "636243",
                "620000",
                "60851607",
                "62347721",
                "636697",
                "637053",
                "636572",
                "606136",
                "636584",
                "637729",
                "627765",
                "606175",
                "606288",
                "636230",
                "637067",
                "629903",
                "636533",
                "605095",
                "629866",
                "605106",
                "636686",
                "605914",
                "636519",
                "606311",
                "636372",
                "636991",
                "606025",
                "636438",
                "629887",
                "636283",
                "639404",
                "605098",
                "636397",
                "606103",
                "636345",
                "637029",
                "636263",
                "605158",
                "629976",
                "605139",
                "605101",
                "606080",
                "637752",
                "629841",
                "636243",
                "620000",
                "629961",
                "606098",
                "604810",
                "627707",
                "627889",
                "60851607",
                "475396",
                "475183",
                "418240",
                "524682",
                "524683",
                "554391",
                "539933",
                "405019",
                "637727",
                "62347708",
                "62347706",
                "62347707",
                "62347709",
                "62347711",
                "62347712",
                "62347713",
                "62347714",
                "62347715",
                "62347716",
                "62347717",
                "62347718",
                "34779801",
                "34779802",
                "34779803",
                "34779804",
                "34779805",
                "34779806",
                "34779807",
                "34777402",
                "34779809",
                "34777001",
                "34779810",
                "34779811",
                "34779812",
                "34779813",
                "34779814",
                "34777004",
                "34777005",
                "6234777402",
                "6234777002",
                "6234779802",
                "62347723",
                "466044",
                "466046",
                "654321",
                "639616",
                "629953",
                "605001",
                "60851606",
                "63767630",
                "639315",
                "457987",
                "422627",
                "458701",
                "439371",
                "406750",
                "410149",
                "6234777001",
                "6234779807",
                "6234779804",
                "6234777003",
                "6234779808",
                "6234777404",*/

                "636697",
                "637053",
                "636572",
                "606136",
                "636584",
                "637729",
                "627765",
                "606175",
                "606288",
                "636230",
                "637067",
                "629903",
                "636533",
                "605095",
                "629866",
                "605106",
                "636686",
                "605914",
                "636519",
                "606311",
                "636372",
                "636991",
                "606025",
                "636438",
                "629887",
                "636283",
                "639404",
                "605098",
                "636397",
                "606103",
                "636345",
                "637029",
                "636263",
                "605158",
                "629976",
                "605139",
                "605101",
                "606080",
                "637752",
                "629841",
                "636243",
                "620000",
                "629961",
                "606098",
                "604810",
                "627707",
                "627889",
                "60851607",
                "475396",
                "475183",
                "418240",
                "524682",
                "524683",
                "554391",
                "539933",
                "405019",
                "637727",
                "62347708",
                "62347706",
                "62347707",
                "62347709",
                "62347711",
                "62347712",
                "62347713",
                "62347714",
                "62347715",
                "62347716",
                "62347717",
                "62347718",
                "34779801",
                "34779803",
                "34779805",
                "34779806",
                "34779810",
                "34779811",
                "34779812",
                "34779813",
                "34779814",
                "34777004",
                "34777005",
                "6234777402",
                "6234777002",
                "6234779802",
                "62347723",
                "466044",
                "466046",
                "654321",
                "639616",
                "629953",
                "605001",
                "60851606",
                "63767630",
                "639315",
                "457987",
                "422627",
                "458701",
                "406759",
                "439371",
                "406750",
                "410149",
                "6234777001",
                "6234779807",
                "6234779804",
                "6234777003",
                "6234779808",
                "6234777404",
                "34779809",
                "6234777405",
                "6234779801",
                "6234777004",
                "6234777005",
                "6234779809",
                "6234779810",
                "6234779811",
                "6234779812",
                "6234779813",
                "6234779814",
                "6234777104",
                "6234779816",
                "6234777405",
                "6234779817",
                "6234779821",
                "6234777101",
                "6234779818",
                "6234777103",
                "6234779819",
                "6234779803",
                "62347725",
                "6234779820",
                "62347722",
                "6234777105",
                "6234777005",
                "6234779809",
                "6234779810",
                "6234779811",
                "6234779812",
                "6234779813",
                "6234779814",
                "6234777104",
                "6234779816",
                "6234777405",
                "6234779817",
                "6234779821",
                "6234777101",
                "6234779818",
                "6234777103",
                "6234779819",
                "6234779803",
                "62347725",
                "6234779820",
                "62347722",
                "6234777105"
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    hideKeyboard(getActivity());
                    getFragmentManager().popBackStack(AddManageCardFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                }
                return false;
            }
        });
    }
}

