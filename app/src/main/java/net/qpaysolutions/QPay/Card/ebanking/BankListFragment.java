package net.qpaysolutions.QPay.Card.ebanking;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.qpaysolutions.QPay.Billpayment.BillPayActivity;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by QPay on 6/12/2018.
 */

public class BankListFragment extends Fragment implements EbankInterface {
    private RecyclerView recyclerView;
    private BankListAdapter mAdapter;
    private ArrayList<EbankModel> bankArrayList;
    String key;
    private LinearLayout progressLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bank_list, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.bankRecyclerView);
        progressLayout=view.findViewById(R.id.progressLayoutBankList);
        Log.d("dinesh", "isEb..." + getArguments().getBoolean("isEb"));
        if (getArguments().getBoolean("isEb")) {
            new Pgateway(BankListFragment.this,progressLayout).execute("bank");
            key = "ebanking";
        } else {
            new Pgateway(BankListFragment.this,progressLayout).execute("mbank");
            key = "mbanking";
        }


    }

    @Override
    public void getelist(ArrayList<EbankModel> arrayList) {
        bankArrayList = arrayList;
        mAdapter = new BankListAdapter(getActivity(), bankArrayList, new BankListAdapter.OnBankItemClick() {
            @Override
            public void onItemClickBank(int position) {
                Intent intent = new Intent(getActivity(), BillPayActivity.class);
                intent.putExtra("id", key);
                intent.putExtra("name", bankArrayList.get(position).getName());
                intent.putExtra("gateway", bankArrayList.get(position).getId());
                intent.putExtra("url", bankArrayList.get(position).getLogoUrl());
                startActivity(intent);
            }
        });
        GridLayoutManager MyLayoutManager2 = new GridLayoutManager(getActivity(), 2);
        MyLayoutManager2.setOrientation(GridLayoutManager.VERTICAL);

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setLayoutManager(MyLayoutManager2);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
