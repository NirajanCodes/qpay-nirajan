package net.qpaysolutions.QPay.Card;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.Card.CardLists.MyAdapter;
import net.qpaysolutions.QPay.Card.adapter.CardsDataAdapter;
import net.qpaysolutions.QPay.Card.animations.CardStack;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.FloatingactionButton.FloatingActionButton;
import net.qpaysolutions.QPay.FloatingactionButton.FloatingActionsMenu;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by QPay on 7/12/2018.
 */

public class AddManageCardFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    private ListView listView;
    private net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase QPayCustomerDatabase;
    private String cust_id, last_4, exp_date, app_id;
    private String status;
    private String status_inquiry, resp_code_inquiry, avail_bal_inquiry, pin, message;
    private QPayProgressDialog pDialog;
    private Handler handler = new Handler();
    private String check_balance_inq_result;
    private String delet_status;
    private TextView qpay_card_cbalance;
    private String device_token;
    private String resp_code, status_verification_last;
    private AddCardListAdapter cardListAdapter;
    private SQLiteDatabase db;
    private TextView cardText, walletAmount;
    private int counter = 0;
    private FloatingActionButton fab1, fab2;
    private Boolean isFabOpen = false;
    private FloatingActionsMenu fab;
    private String card_no_full;
    private QPayProgressDialog qPayProgressDialog;
    boolean check_20 = true;
    boolean IsDialogOpen = false;
    boolean isDeletDialogOpen = false;
    private FloatingActionButton addDebiCard;
    private LinearLayout emptyCardlayout, wallet_layout;
    MyAdapter mAdapter;

    ViewPager mPager;

    private CardStack mCardStack;
    private CardsDataAdapter mCardAdapter;
    List<CardPOJO> cardPOJOList = new ArrayList<>();
    private RelativeLayout action_layout;
    private LinearLayout verifyBttn, topUpBttn, deleteBttn;
    private TextView verifyTextview;
    private ImageView verifyBalanceInqImageview;
    public static String ulrImage = null;
    public static String TAG = AddManageCardFragment.class.getCanonicalName();
    String balance = "0.00";
    private int pos = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();
        db = QPayCustomerDatabase.getWritableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
        last_4 = QPayCustomerDatabase.getLastFourCardInfo();
        exp_date = QPayCustomerDatabase.getExp();
        device_token = ((CustomerApplication) getActivity().getApplication()).getDevtokenVariable();
        Log.d(Utility.TAG, "device token from card main : " + device_token);
        Log.d(Utility.TAG, "cust_id from card profile maina: " + cust_id);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
//        TextView textView = ((BillPayActivity) getActivity()).findViewById(R.id.title);
//        textView.setText("My Cards");


//        qpay_card_cbalance = (TextView) view.findViewById(R.id.qpay_balance);
        try {
            Toolbar toolbar = ((AddCardActivity) getActivity()).findViewById(R.id.toolbar);
            TextView title = ((AddCardActivity) getActivity()).findViewById(R.id.title);
            title.setText("My Cards");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        } catch (Exception e) {

        }
        emptyCardlayout = view.findViewById(R.id.emptyCardlayout);
        mCardStack = view.findViewById(R.id.card_container);

//        listView = (ListView) view.findViewById(R.id.setting_list);
        Cursor cursor = db.rawQuery(" select * from profile ", null);
        setCardList(cursor);
//        cardListAdapter = new AddCardListAdapter(getContext(), cursor, AddManageCardFragment.this);
//        cardListAdapter.swapCursor(cursor);
//        listView.setAdapter(cardListAdapter);
//        listView.setLongClickable(true);
//        // recentList(view);
//        listView.setClickable(true);
//        listView.setOnItemClickListener(this);
//        listView.setOnItemLongClickListener(this);
        wallet_layout = view.findViewById(R.id.wallet_layout);
        action_layout = view.findViewById(R.id.card_action_layout);
        verifyBttn = view.findViewById(R.id.verify_card_layout);
        topUpBttn = view.findViewById(R.id.topup_layout);
        deleteBttn = view.findViewById(R.id.delete_layout);
        verifyTextview = view.findViewById(R.id.verify_textview);
        verifyBalanceInqImageview = view.findViewById(R.id.verify_imageview);
        //changes
//        cardText = view.findViewById(R.id.card_text);
        //    cardText.setText("**** **** **** " + GeneralPref.getRandomNumber());
        walletAmount = view.findViewById(R.id.wallet_amount);
        walletAmount.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
        if (((cursor != null) && (cursor.getCount() > 0))) {
            emptyCardlayout.setVisibility(View.GONE);
        } else {
            emptyCardlayout.setVisibility(View.VISIBLE);
        }

//        qpay_card_cbalance.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
//        new AddManageCardFragment.GetQpaybalance().execute(Constants.GET_BALANCE_QPAY + cust_id);
        floatingBottom(view);
        view = getActivity().getCurrentFocus();

        if (view != null) {

            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }

        mCardStack.setContentResource(R.layout.card_content);
        mCardStack.setStackMargin(40);
        mCardAdapter = new CardsDataAdapter(getActivity(), AddManageCardFragment.this, getCardList(), new CardsDataAdapter.OnCardClickListener() {
            @Override
            public void onCardVerifyListener(CardPOJO cardPOJO, int position) {
                pos = position;
                if (cardPOJO.getActFlag().trim().equals("R")) {
                    pos = position;
                    verifyCard(cardPOJO.getCardId());

                }
                if (cardPOJO.getActFlag().trim().equals("V")) {
                    getCardBalance(cardPOJO.getCardId());

                }

            }

            @Override
            public void onCardTopupListener(CardPOJO cardPOJO, int position) {
                ulrImage = cardPOJO.getImgId();
//                Intent intent = new Intent(getActivity(), BillPayActivity.class);
//                intent.putExtra("id", "topoff");
//                intent.putExtra("cardId", cardPOJO.getCardId());
//                startActivity(intent);

//                TopUpFragment topUpFragment = new TopUpFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString("id", cardPOJO.getCardId());
//                topUpFragment.setArguments(bundle);
//                FrameLayout frameLayout = getActivity().findViewById(R.id.card_container);
//                frameLayout.removeAllViewsInLayout();
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.card_container, topUpFragment)
//                        .addToBackStack(AddManageCardFragment.TAG).commit();

                Intent intent = new Intent(getActivity(), TopupCardActivity.class);
                intent.putExtra("id", cardPOJO.getCardId());
                startActivity(intent);
            }

            @Override
            public void onCardDeleteListener(CardPOJO cardPOJO, int position) {
                String last_4_delet = QPayCustomerDatabase.lastFour(cardPOJO.getCardId());
                String exp = QPayCustomerDatabase.getCardExp(cardPOJO.getCardId());
                String card_token = QPayCustomerDatabase.getCardType(cardPOJO.getCardId());
                if (isDeletDialogOpen == false) {
                    isDeletDialogOpen = true;
                    showDeletsweet(Long.parseLong(cardPOJO.getCardId()), last_4_delet, exp, card_token);
                    Log.d(Utility.TAG, "position : " + cardPOJO.getCardId());
                }
            }


        });

        mCardAdapter.addAll(getCardList());

        mCardStack.setAdapter(mCardAdapter);
        mCardStack.setVisibleCardNum(getCardList().size());

//        mCardStack.setListener(new CardListener(getActivity(), getCardList()));
        if (getCardList().size() == 1) {
            mCardStack.setCanSwipe(false);
        }


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_card, container, false);

    }

    public void floatingBottom(View view) {
        addDebiCard = view.findViewById(R.id.addDebiCard);
        ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
        drawable.getPaint().setColor(getResources().getColor(R.color.white));
        addDebiCard.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addDebiCard:
                try {

                    CardListFragment register = new CardListFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.card_container, register)
                            .addToBackStack(TAG)
                            .commit();
                    Utils.hideKeyword(getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

//            case R.id.top_off_neaw:
//                try {
//                    SQLiteDatabase db = QPayCustomerDatabase.getWritableDatabase();
//                    Cursor cursor = db.rawQuery(" select * from profile where act_flag  = 'V'  ", null);
//                    Cursor cursor_reg = db.rawQuery(" select * from profile  ", null);
//                    cursor.moveToFirst();
//                    int checkrow = cursor.getCount();
//                    Log.d(Utility.TAG, "check row : " + checkrow + "receiver : " + cursor_reg.getCount());
//                    if (cursor_reg.getCount() > 0) {
//                        if (checkrow > 0) {
//                            TopoffselectCard topUpFragment = new TopoffselectCard();
//                            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.animation_one_activity, R.anim.animation_two_activity)
//                                    .replace(R.id.bill_payment_container, topUpFragment)
//                                    .commit();
//                            getActivity().setTitle("Select Card ");
//                        } else {
//                            Toast.makeText(getContext(), "Please verify your card in order to use this feature.", Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        Toast.makeText(getContext(), "Please register and verify your card in order to use this feature.", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        String last_4_delet = QPayCustomerDatabase.lastFour(String.valueOf(id));
        String exp = QPayCustomerDatabase.getCardExp(String.valueOf(id));
        String card_token = QPayCustomerDatabase.getCardType(String.valueOf(id));
        if (isDeletDialogOpen == false) {
            isDeletDialogOpen = true;
            showDeletsweet(id, last_4_delet, exp, card_token);
            Log.d(Utility.TAG, "position : " + id);
        }
        return true;
    }

    public void showDeletsweet(final long position, final String last_4_delet, final String exp, final String card_token) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.text_title);
        final ImageView img_icon = (ImageView) v.findViewById(R.id.img_icon);
        img_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning));
        img_icon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.color_yellow), android.graphics.PorterDuff.Mode.MULTIPLY);
        title.setText("Are You Sure?");
        dialog_balance.setText("Do you want to delete Card Profile ending with " + last_4_delet + " ?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isDeletDialogOpen = false;
                dialog.dismiss();
            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String web_call_delet = Constants.DELET_CARD + cust_id + "/" + last_4 + "/" + exp;
                Log.d(Utility.TAG, " web service call for deleting card : " + web_call_delet);
                new AddManageCardFragment.deletCard(last_4_delet, exp, position, card_token).execute();
                isDeletDialogOpen = false;
                view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                dialog.dismiss();
                isDeletDialogOpen = false;
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String last_4_delet = QPayCustomerDatabase.lastFour(String.valueOf(id));
        String exp = QPayCustomerDatabase.getCardExp(String.valueOf(id));
        String card_token = QPayCustomerDatabase.getCardType(String.valueOf(id));
        if (isDeletDialogOpen == false) {
            isDeletDialogOpen = true;
            showDeletsweet(id, last_4_delet, exp, card_token);
            Log.d(Utility.TAG, "position : " + id);
        }
    }


    private class CardBalance extends AsyncTask<Void, Void, JSONObject> {
        String last_4, pin, _id, card_token;

        private CardBalance(String last_4, String pin, String _id, String card_token) {
            this.last_4 = last_4;
            this.pin = pin;
            this._id = _id;
            this.card_token = card_token;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new QPayProgressDialog(getContext());
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            String line = null;
            JSONObject Response = null;
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("dev_token", device_token);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("last_4", last_4);
                jsonObject.put("pin", pin);
                jsonObject.put("card_token", card_token);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                String result = NetworkAPI.sendHTTPData(Constants.GET_BALANCE_EACH_CARD, jsonObject);
                Log.d(Utility.TAG, "Response from php = " + result);
                Response = new JSONObject(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            super.onPostExecute(args);
            try {
                Log.d(Utility.TAG, args.toString());
                JSONObject purchaseCardTxnResult = args.getJSONObject("BalanceInquiryResult");
                String status = purchaseCardTxnResult.getString("status");
                String auth_code = purchaseCardTxnResult.getString("auth_code");
                final String crrn = purchaseCardTxnResult.getString("crrn");
                final String stan = purchaseCardTxnResult.getString("stan");
                String msg = purchaseCardTxnResult.getString("msg");
                if (status.equals("00")) {
                    if (auth_code.equals("00") && !auth_code.isEmpty() && !auth_code.equals("null") && status != null) {
                        check_balance_inq_result = Constants.GET_BALANCE_EACH_CARD_FINAL + cust_id + "/" + stan;
                        runinBalance(cust_id, stan, app_id, _id, crrn);
                    } else {
                        pDialog.dismiss();
                        dialogFailure("Balance Inquiry Failure!", "We are unable to complete your request.");
                    }
                } else if (status.equals("CN")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    Log.d(Utility.TAG, "position : " + _id);
                    QPayCustomerDatabase.deletCard(Long.valueOf(_id));
                    Cursor newCursor = db.rawQuery(" select * from profile ", null);
                    setCardList(newCursor);
//                    mCardAdapter.clear();
//                    mCardAdapter.addAll(getCardList());
//                    mCardAdapter.notifyDataSetChanged();
//                    cardListAdapter.changeCursor(newCursor);
//                    cardListAdapter.notifyDataSetChanged();
                    dialogFailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");
                } else {
                    pDialog.dismiss();
                    dialogFailure("Balance Inquiry Failure!", "We are unable to complete your request.");
                }

            } catch (Exception e) {
                e.printStackTrace();
                try {
                    pDialog.dismiss();
                    dialogFailure("Balance Inquiry Failure!", "We are unable to complete your request.");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void runinBalance(final String cust_id, final String stan, final String app_id, final String _id, final String crrn) {
        handler.postDelayed(new Runnable() {
            public void run() {
                counter++;
                new AddManageCardFragment.CheckBalanceInqStatusResult(cust_id, stan, app_id, _id, crrn).execute();
            }
        }, Tags.TIME_SLEEP);
    }

    public class CheckBalanceInqStatusResult extends AsyncTask<String, String, String> {
        String cust_id, app_id, stan, _id, crrn;

        public CheckBalanceInqStatusResult(String cust_id, String stan, String app_id, String _id, String crrn) {
            this.cust_id = cust_id;
            this.stan = stan;
            this.app_id = app_id;
            this._id = _id;
            this.crrn = crrn;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject_blc = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result;
            try {
                jsonObject_blc.put("app_id", app_id);
                jsonObject_blc.put("cust_id", cust_id);
                jsonObject_blc.put("stan", stan);
                jsonObject_blc.put("t_flag", "1");
                Log.d(Utility.TAG, "balance inq status post : " + jsonObject_blc.toString());
                result = networkAPI.sendHTTPData(Constants.GET_BALANCE_EACH_CARD_FINAL, jsonObject_blc);
                JSONObject jsonObject = new JSONObject(result);
                JSONObject jsonObject1 = jsonObject.getJSONObject("CheckBalanceInqStatusResult");
                status_inquiry = jsonObject1.getString("status");
                avail_bal_inquiry = jsonObject1.getString("avail_bal");
                resp_code_inquiry = jsonObject1.getString("resp_code");
                message = jsonObject1.getString("displayMsg");
                Log.d(Utility.TAG, "status_inquiry: " + status_inquiry + "aval balance :" + avail_bal_inquiry + "resp code :" + resp_code_inquiry + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (status_inquiry.equals("00")) {

                    if (resp_code_inquiry != null && !resp_code_inquiry.isEmpty() && !resp_code_inquiry.equals("null") && resp_code_inquiry.equals("00")) {
                        pDialog.dismiss();
                        displayBalance(avail_bal_inquiry, _id);
                        new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();


                    } else if (resp_code_inquiry != null && !resp_code_inquiry.isEmpty() && !resp_code_inquiry.equals("null") && resp_code_inquiry.equals("LO")) {
                        if (counter < 10) {
                            runinBalance(cust_id, stan, app_id, _id, crrn);
                        } else {
                            pDialog.dismiss();
                            pDialog.hide();
                            dialogFailure("Balance Inquiry Failure!", "Could not proceed your request,please try again later.");

                        }


                    }/* else if (resp_code_inquiry != null && !resp_code_inquiry.isEmpty() && !resp_code_inquiry.equals("null") && resp_code_inquiry.equals("08")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Balance Inquiry Failure!",message);
                } else if (resp_code_inquiry.equals("87")) {
                    new Ackclass(app_id,cust_id,stan,crrn).execute();
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Balance Inquiry Failure!",message);
                } else if (resp_code_inquiry != null && !resp_code_inquiry.isEmpty() && !resp_code_inquiry.equals("null") && resp_code_inquiry.equals("99")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Balance Inquiry Failure!","Could not proceed your request,please try again later.");
//                    showNegativeToast( message);
                }*/ else {
                        new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();
                        pDialog.dismiss();
                        pDialog.hide();
                        dialogFailure("Balance Inquiry Failure!", message);
                    }
                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Balance Inquiry Failure!", "Could not proceed your request,please try again later.");

                }

            } catch (Exception e) {
                pDialog.dismiss();
                pDialog.hide();
                e.printStackTrace();
                try {
                    dialogFailure("Balance Inquiry Failure!", "Could not proceed your request,please try again later.");

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void getCardBalance(final String _id) {
        final Dialog dialog = new Dialog(getContext(), R.style.UpdownDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custum_dialog_entrcardpin);
        final EditText aut_editext = (EditText) dialog.findViewById(R.id.enter_pin);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.proceed_pin_enter);
        TextView dialogCancel = (TextView) dialog.findViewById(R.id.cancel_pin_enter);

        aut_editext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }
            }
        });
        if (aut_editext.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text_from_editext = aut_editext.getText().toString();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(aut_editext.getWindowToken(), 0);
                if (text_from_editext.length() == 4) {
                    IsDialogOpen = false;
                    String last_4_balance = QPayCustomerDatabase.lastFour(_id);
                    String card_token = QPayCustomerDatabase.getCardType(_id);
                    new AddManageCardFragment.CardBalance(last_4_balance, text_from_editext, _id, card_token).execute();
                    dialog.dismiss();
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                } else {
                    aut_editext.setError("Please Insert Valid Pin Number");
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsDialogOpen = false;
                dialog.dismiss();
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        if (IsDialogOpen == false) {
            IsDialogOpen = true;
            dialog.show();
        }

    }

    public class deletCard extends AsyncTask<String, String, String> {

        String last_4, exp_date, card_token;
        long position;

        public deletCard(String last_4, String exp_date, long position, String card_token) {
            this.last_4 = last_4;
            this.exp_date = exp_date;
            this.position = position;
            this.card_token = card_token;
        }

        @Override
        protected String doInBackground(String... params) {
            String card_delet;
            JSONObject jsonObject_del = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_del.put("app_id", app_id);
                jsonObject_del.put("cust_id", cust_id);
                jsonObject_del.put("last_4", last_4);
                jsonObject_del.put("exp_date", exp_date);
                jsonObject_del.put("card_token", card_token);
                jsonObject_del.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_del.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d(Utility.TAG, "delet card post : " + jsonObject_del);
                card_delet = networkAPI.sendHTTPData(Constants.DELET_CARD, jsonObject_del);
                Log.d(Utility.TAG, "delet card response : " + card_delet);
                JSONObject jsonObject = new JSONObject(card_delet);
                JSONObject jsonObject1 = jsonObject.getJSONObject("deletecardResult");
                delet_status = jsonObject1.getString("status");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return delet_status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.setCancelable(false);
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (s.equals("00")) {
                    qPayProgressDialog.dismiss();
                    qPayProgressDialog.hide();
                    QPayCustomerDatabase.deletCard(position);
                    Cursor newCursor = db.rawQuery(" select * from profile ", null);
//                    cardListAdapter.changeCursor(newCursor);
//                    cardListAdapter.notifyDataSetChanged();
                    setCardList(newCursor);
                    mCardAdapter.clear();
                    mCardAdapter.addAll(getCardList());
                    mCardAdapter.notifyDataSetChanged();
                    AddManageCardFragment AddManageCardFragment = new AddManageCardFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.card_container, AddManageCardFragment)
                            .addToBackStack(TAG)
                            .commit();
                } else {
                    qPayProgressDialog.dismiss();
                    qPayProgressDialog.hide();
                }
            } catch (Exception e) {
                e.printStackTrace();
                qPayProgressDialog.dismiss();
                qPayProgressDialog.hide();
            }
        }
    }


    public class GetQpaybalance extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;
            JSONObject balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                balance.put("cust_id", cust_id);
                balance.put("app_id", app_id);
                balance.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                balance.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                String result = networkAPI.sendHTTPData(Constants.GET_BALANCE_QPAY, balance);
                JSONObject jsonObject = new JSONObject(result);
                JSONObject jsonObject1 = jsonObject.getJSONObject("getbalanceResult");
                balance_avail = jsonObject1.getString("avail_bal");
                status = jsonObject1.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return balance_avail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  pDialog = new QPayProgressDialog(getActivity());
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
               /* pDialog.dismiss();
                pDialog.hide();*/
                Log.d(Utility.TAG, "available qpay balance is: " + s);
                if (status.equals("00")) {
                    qpay_card_cbalance.setText(s);
//                    new AlertDialog.Builder(getActivity()).setMessage("your available qpay balance is Rs." + s);
                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void message(String msg) {
        new AlertDialog.Builder(getContext()).setMessage(msg)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                }).show();
    }


    public void displayBalance(final String balance, final String _id) {
        final Dialog dialog = new Dialog(getContext(), R.style.UpdownDialog);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_balance_inquiry);
        final TextView balance_card = (TextView) dialog.findViewById(R.id.balance_card);
        final TextView ok = (TextView) dialog.findViewById(R.id.ok);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        balance_card.setText("NPR " + balance);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    DateFormat df = new SimpleDateFormat("dd/MM/yy");
                    DateFormat df = new SimpleDateFormat("E,MMM d, yyyy ");
                    String date = df.format(Calendar.getInstance().getTime());
                    date = date.replace("+", "");
                    QPayCustomerDatabase.updateCardBalance(balance, _id, date);
                    Log.d(Utility.TAG, "balance and card id : " + balance + _id);
                    dialog.dismiss();
//                    getFragmentManager().beginTransaction()
//                            .replace(R.id.card_container, new AddManageCardFragment())
//                            .addToBackStack(TAG)
//                            .commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void verifyCard(final String _id) {
        final Dialog dialog = new Dialog(getContext(), R.style.UpdownDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custum_dialog_entrcardpin);
        final EditText aut_editext = (EditText) dialog.findViewById(R.id.enter_pin);
        aut_editext.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView dialogButton = dialog.findViewById(R.id.proceed_pin_enter);
        TextView dialogcancel = dialog.findViewById(R.id.cancel_pin_enter);
        TextView verify_content = dialog.findViewById(R.id.verify_content);
        verify_content.setVisibility(View.VISIBLE);
        verify_content.setText(getString(R.string.amount_fivehundred));
        dialogButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(aut_editext.getWindowToken(), 0);
                pin = aut_editext.getText().toString();
                if (pin != null && pin.length() == 4) {
                    IsDialogOpen = false;
                    String last_4_verification = QPayCustomerDatabase.lastFour(_id);
                    String card_token = QPayCustomerDatabase.getCardType(_id);
                    new AddManageCardFragment.VerifyCardAsynkTasky("10.00", cust_id, pin, last_4_verification, _id, card_token).execute();
                    dialog.dismiss();
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                } else {
                    aut_editext.setError("Please enter valid pin");
                }
            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                IsDialogOpen = false;
                dialog.dismiss();
            }
        });
        if (IsDialogOpen == false) {
            IsDialogOpen = true;
            dialog.show();

        }


    }


    public class ReverificationCard extends AsyncTask<String, String, JSONObject> {
        String amount, id, pin, last_4, _id, account_type, card_token;

        public ReverificationCard(String amount, String id, String pin, String last_4, String _id, String account_type, String card_token) {
            this.amount = amount;
            this.id = id;
            this.pin = pin;
            this.last_4 = last_4;
            this._id = _id;
            this.account_type = account_type;
            this.card_token = card_token;

        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject Response = null;
            String line = null;
            NetworkAPI networkAPI = new NetworkAPI();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("account_type", account_type);
                jsonObject.put("app_id", app_id);
                jsonObject.put("amount", amount);
                jsonObject.put("cust_id", id);
                jsonObject.put("last_4", last_4);
                jsonObject.put("pin", pin);
                jsonObject.put("card_token", card_token);
                jsonObject.put("dev_token", ((CustomerApplication) getActivity().getApplication()).getDevtokenVariable());
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                String data = jsonObject.toString();
                Log.d(Utility.TAG, "data for RetryVerificationResult card :" + data);
                String yourURL = Constants.TOP_OFF;
                URL url = new URL(yourURL);
                String result = networkAPI.sendHTTPData(Constants.REVERIFICATION, jsonObject);
                Log.d(Utility.TAG, "RetryVerificationResult from php = " + result);
                Response = new JSONObject(result);
            } catch (Exception e) {
                Log.d(Utility.TAG, "Error Encountered");
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(JSONObject s) {
            super.onPostExecute(s);
            try {
                Log.d(Utility.TAG, "self top off result " + s.toString());
                JSONObject result = s.getJSONObject("RetryVerificationResult");
                String status = result.getString("status");
                String msg = result.getString("msg");
                String stan = result.getString("stan");
                String crrn = result.getString("crrn");
                if (!status.equals("null") && !status.isEmpty() && status != null && status.equals("00")) {
                    runInFiftnVerify(app_id, cust_id, stan, "1", _id, crrn, last_4, card_token);
                } else if (status.equals("55")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogRetryFailure("Card Verification Failure!", msg);

                } else if (status.equals("75")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogRetryFailure("Card Verification Failure!", msg);
                } else if (status.equals("99")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogRetryFailure("Card Verification Failure!", "Your card ending with " + last_4 + " could not be verified. Please try again later.");
                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogRetryFailure("Card Verification Failure!", "Your card ending with " + last_4 + " could not be verified. Please try again later.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                pDialog.hide();
                dialogRetryFailure("Card Verification Failure!", "Your card ending with " + last_4 + " could not be verified. Please try again later.");
            }
        }
    }

    public class VerifyCardAsynkTasky extends AsyncTask<String, String, JSONObject> {
        String amount, id, pin, last_4, _id, card_token;

        public VerifyCardAsynkTasky(String amount, String id, String pin, String last_4, String _id, String card_token) {
            this.amount = amount;
            this.id = id;
            this.pin = pin;
            this.last_4 = last_4;
            this._id = _id;
            this.card_token = card_token;
            pDialog = new QPayProgressDialog(getContext());
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject Response = null;
            String line = null;
            NetworkAPI networkAPI = new NetworkAPI();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("amount", amount);
                jsonObject.put("cust_id", id);
                jsonObject.put("last_4", last_4);
                jsonObject.put("pin", pin);
                jsonObject.put("t_flag", "1");
                jsonObject.put("card_token", card_token);
                jsonObject.put("dev_token", ((CustomerApplication) getActivity().getApplication()).getDevtokenVariable());
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                String data = jsonObject.toString();
                Log.d(Utility.TAG, "data for registration card :" + data);
                String yourURL = Constants.TOP_OFF;
                URL url = new URL(yourURL);
                String result = networkAPI.sendHTTPData(Constants.TOP_OFF, jsonObject);
                Log.d(Utility.TAG, "Response from php = " + result);
                Response = new JSONObject(result);
//                connection.disconnect();
            } catch (Exception e) {
                Log.d(Utility.TAG, "Error Encountered");
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(JSONObject s) {
            super.onPostExecute(s);
            try {
                Log.d(Utility.TAG, "self top off result " + s.toString());
                JSONObject result = s.getJSONObject("selftopoffResult");
                String msg = result.getString("msg");
                String stan = result.getString("stan");
                String crrn = result.getString("crrn");
                String status = result.getString("resp_code");
                if (!status.equals("null") && !status.isEmpty() && status != null && status.equals("00")) {
                    runInFiftnVerify(app_id, cust_id, stan, "1", _id, crrn, last_4, card_token);
                } else if (status.equals("55")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Card Verification Failure!", "Your transaction could not be processed.");
                } else if (status.equals("VR")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    QPayCustomerDatabase.updateProfileFlag(" 'V' ", _id);
                    new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();
                    Cursor newCursor = db.rawQuery(" select * from profile ", null);
                    setCardList(newCursor);
//                    mCardAdapter.clear();
//                    mCardAdapter.addAll(getCardList());
//                    mCardAdapter.notifyDataSetChanged();
//                    cardListAdapter.changeCursor(newCursor);
//                    cardListAdapter.notifyDataSetChanged();
                    custumdialogSucess("Card Verification Success!", "Your card ending in " + last_4 + " has been successfully verified");
                } else if (status.equals("CN")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    Log.d(Utility.TAG, "position : " + _id);
                    QPayCustomerDatabase.deletCard(Long.valueOf(_id));
                    Log.d(Utility.TAG, "position : " + id);
                    Cursor newCursor = db.rawQuery(" select * from profile ", null);
//                    cardListAdapter.changeCursor(newCursor);
//                    cardListAdapter.notifyDataSetChanged();
                    setCardList(newCursor);
                    dialogFailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");
                } else if (status.equals("99")) {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Card Verification Failure!", "Your transaction could not be processed.");
                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Card Verification Failure!", "Your transaction could not be processed.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                pDialog.hide();
                dialogFailure("Card Verification Failure!", "Your transaction could not be processed.");
            }
        }
    }

    public void runInFiftnVerify(final String app_id, final String cust_id, final String stan, final String t_flag, final String _id, final String crrn, final String last_4, final String card_token) {
        handler.postDelayed(new Runnable() {
            public void run() {
                counter++;
                new AddManageCardFragment.CheckTxnStatusSTResultVeruify(app_id, cust_id, stan, t_flag, _id, crrn, last_4, card_token).execute(/*final_verification_card*/);
            }
        }, Tags.TIME_SLEEP);
    }

    public class CheckTxnStatusSTResultVeruify extends AsyncTask<String, String, String> {
        String app_id, cust_id, stan, t_flag, _id, crrn, last_4, card_token;

        public CheckTxnStatusSTResultVeruify(String app_id, String cust_id, String stan, String t_flag, String _id, String crrn, String last_4, String card_token) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.stan = stan;
            this.t_flag = t_flag;
            this._id = _id;
            this.crrn = crrn;

            this.last_4 = last_4;
            this.card_token = card_token;
        }

        @Override
        protected String doInBackground(final String... params) {
            String message = null;
            String result_final_verification;
            JSONObject jsonObject_final = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                jsonObject_final.put("app_id", app_id);
                jsonObject_final.put("cust_id", cust_id);
                jsonObject_final.put("stan", stan);
                jsonObject_final.put("t_flag", t_flag);
                jsonObject_final.put("crrn", crrn);
                Log.d(Utility.TAG, "final card verification:  " + jsonObject_final.toString());
                result_final_verification = networkAPI.sendHTTPData(Constants.CHECK_STATUS_FINAL_VERIFICATION, jsonObject_final);
                Log.d(Utility.TAG, "card verify " + result_final_verification);
                JSONObject jsonObject = new JSONObject(result_final_verification);
                JSONObject jsonObject1 = jsonObject.getJSONObject("CheckTxnStatusSTResult");
                status_verification_last = jsonObject1.getString("status");
                resp_code = jsonObject1.getString("resp_code");
                balance = jsonObject1.getString("avail_bal");
                message = jsonObject1.getString("displayMsg");
                Log.d(Utility.TAG, "card verify " + resp_code);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return message;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String message) {
            super.onPostExecute(message);
            System.out.println("CheckTxnStatusSTResultVeruify:: " + message);
            try {
                if (status_verification_last.equals("00")) {
                    if (resp_code.equals("00")) {
                        pDialog.dismiss();
                        if (!balance.equalsIgnoreCase("0.00") || !balance.equalsIgnoreCase("0")) {
                            BalancePref.setAvailableAmount(balance);
                        }
//                        pDialog.hide();

                        QPayCustomerDatabase.updateProfileFlag(" 'V' ", _id);
                        new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();
                        Cursor newCursor = db.rawQuery(" select * from profile ", null);
                        setCardList(newCursor);
//                        mCardAdapter.clear();
//                        mCardAdapter.addAll(getCardList());
//                        mCardAdapter.notifyDataSetChanged();
//                        cardListAdapter.changeCursor(newCursor);
//                        cardListAdapter.notifyDataSetChanged();
                        custumdialogSucess("Card Verification Success!", message);
                    } else if (resp_code.equals("20") || resp_code.equals("76")) {
                        if (check_20 == true) {
                            check_20 = false;
                            String card_nature = QPayCustomerDatabase.getKeyCardType(_id);
                            String card_no = QPayCustomerDatabase.getKeyCardFullNo(_id);
                            if (card_nature.equals("S")) {
                                QPayCustomerDatabase.updateCardnature("'C'", _id);
                                new AddManageCardFragment.ReverificationCard("10.00", cust_id, pin, last_4, _id, "C", card_token).execute();
                            } else if (card_nature.equals("C")) {
                                QPayCustomerDatabase.updateCardnature("'S'", _id);
                                new AddManageCardFragment.ReverificationCard("10.00", cust_id, pin, last_4, _id, "S", card_token).execute();
                            }
                        } else {
                            pDialog.dismiss();
                            pDialog.hide();
                            dialogRetryFailure("Card Verification Failure!", message);
                        }
                    } else if (resp_code.equals("08") && !resp_code.isEmpty() && !resp_code.equals("null") && resp_code != null) {
                        new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();
                        pDialog.dismiss();
                        pDialog.hide();
                        dialogFailure("Card Verification Failure!", message);
                    } else if (resp_code.equals("55") && !resp_code.isEmpty() && !resp_code.equals("null") && resp_code != null) {
                        new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();
                        pDialog.dismiss();
                        pDialog.hide();
                        Log.d(Utility.TAG, "message : " + message);
                        dialogFailure("Card Verification Failure!", message);
                    } else if (resp_code.equals("75") && !resp_code.isEmpty() && !resp_code.equals("null") && resp_code != null) {
                        new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();
                        pDialog.dismiss();
                        pDialog.hide();
                        Log.d(Utility.TAG, "message : " + message);
                        dialogFailure("Card Verification Failure!", message);
                    } else if (resp_code.equals("LO") && !resp_code.isEmpty() && !resp_code.equals("null") && resp_code != null) {
                        if (counter < 10) {
                            runInFiftnVerify(app_id, cust_id, stan, t_flag, _id, crrn, last_4, card_token);
                        } else {
                            pDialog.dismiss();
                            pDialog.hide();
                            dialogFailure("Card Verification Failure!", "Your card ending with " + last_4 + " could not be verified due to connection time out.");
                            Log.d(Utility.TAG, "operation fail in card verification");
                            Log.d(Utility.TAG, "card verification Loop Finish ");
                        }
                    } else {
                        new AddManageCardFragment.Ackclass(app_id, cust_id, stan, crrn).execute();
                        pDialog.dismiss();
                        pDialog.hide();
                        dialogFailure("Card Verification Failure!", message);
                    }
                } else {
                    dialogFailure("Card Verification Failure!", "Your card ending with " + last_4 + " could not be verified due to connection time out.");
                    Log.d(Utility.TAG, "operation fail in card verification");

                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                    pDialog.dismiss();
                    pDialog.hide();
                    dialogFailure("Card Verification Failure!", "Your card ending with " + last_4 + " could not be verified due to connection time out.");

                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        }
    }


    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView) v.findViewById(R.id.dialog_icon);
        dialog_icon.setImageResource(R.drawable.ic_card_verification_success);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {
                    dialog.dismiss();
                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }


                    getActivity().getSupportFragmentManager().popBackStack(AddCardActivity.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    AddManageCardFragment AddManageCardFragment = new AddManageCardFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.card_container, AddManageCardFragment)
                            .addToBackStack(TAG)
                            .commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogRetryFailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_image = (ImageView) v.findViewById(R.id.dialog_image);
        dialog_image.setImageResource(R.drawable.ic_card_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.card_container, new AddManageCardFragment())
//                        .addToBackStack(TAG)
//                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogFailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_image = (ImageView) v.findViewById(R.id.dialog_image);
        dialog_image.setImageResource(R.drawable.ic_card_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.card_container, new AddManageCardFragment())
//                        .addToBackStack(TAG)
//                        .commit();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, cust_id, stan, crrn;

        public Ackclass(String app_id, String cust_id, String stan, String crrn) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.stan = stan;
            this.crrn = crrn;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn", crrn);
                Log.d(Utility.TAG, "ack post : " + jsonObject);
                ack_test = networkAPI.sendHTTPData(Constants.ACKN, jsonObject);
                Log.d(Utility.TAG, "response : " + jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

    private List<CardPOJO> getCardList() {
        return cardPOJOList != null ? cardPOJOList : null;
    }

    private void setCardList(Cursor cursor) {
        System.out.println("Cursor length:: " + cursor.getCount());
        cardPOJOList.clear();
        try {

            CardPOJO cardPOJO = new CardPOJO();
            cardPOJO.setCardNumber("**** **** **** " + GeneralPref.getRandomNumber()).setCardName("QPay")
                    .setLast4DigitCardNumber(GeneralPref.getRandomNumber()).setCardBalance((new Decimalformate().decimalFormate(BalancePref.getAvailableAmount())));
            cardPOJOList.add(cardPOJO);
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

                String prepaid = cursor.getString(cursor.getColumnIndex("card_type"));
                String flag = cursor.getString(cursor.getColumnIndex("act_flag"));
                String last_4 = cursor.getString(cursor.getColumnIndex("last_4"));
                String exp = cursor.getString(cursor.getColumnIndex("exp_date"));
                String profile_name = cursor.getString(cursor.getColumnIndex("profile_name"));
                String img_id = cursor.getString(cursor.getColumnIndex("img_id"));
                String _id = cursor.getString(cursor.getColumnIndex("_id"));
                String card_no = cursor.getString(cursor.getColumnIndexOrThrow("card_full"));
                String card_nature = cursor.getString(cursor.getColumnIndexOrThrow("card_nature"));
                String card_balance = cursor.getString(cursor.getColumnIndexOrThrow("card_balance"));
                String known_date = cursor.getString(cursor.getColumnIndexOrThrow("known_date"));
                CardPOJO cardPOJO1 = new CardPOJO();
                cardPOJO1.setCardName(profile_name).setCardNumber(card_no).setActFlag(flag).setCardBalance(card_balance)
                        .setCardId(_id).setCardKnownDate(known_date).setCardNature(card_nature).setCardType(prepaid)
                        .setLast4DigitCardNumber(last_4).setExpiryDate(exp).setImgId(img_id);
                cardPOJOList.add(cardPOJO1);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setVisible() {
        action_layout.setVisibility(View.VISIBLE);
    }

    public void setVisibleGone() {
        action_layout.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Utils.hideKeyword(getActivity());
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });
    }


}


