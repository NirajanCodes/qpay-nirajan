package net.qpaysolutions.QPay.Card.CardLists;

/**
 * Created by QPay on 1/17/2019.
 */

public interface ICardDiscard {

    void onDelete();
}
