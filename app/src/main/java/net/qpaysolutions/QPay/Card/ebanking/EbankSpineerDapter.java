package net.qpaysolutions.QPay.Card.ebanking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;


public class EbankSpineerDapter extends BaseAdapter {
    Context context;

    LayoutInflater inflter;
    ArrayList<EbankModel> arrayList= new ArrayList<>();
    DepositEBankingFragment depositEBankingFragment;

    public EbankSpineerDapter(Context applicationContext, ArrayList<EbankModel> arrayList,DepositEBankingFragment depositEBankingFragment) {
        this.context = applicationContext;
        this.arrayList = arrayList;
        this.depositEBankingFragment = depositEBankingFragment;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_ebanking_spineer, null);
        ImageView icon = (ImageView) view.findViewById(R.id.itemIcon);
        TextView names = (TextView) view.findViewById(R.id.itemName);
        depositEBankingFragment.setPicasso(icon,arrayList.get(i).getLogoUrl());

        names.setText(arrayList.get(i).getName());
        return view;
    }
}