package net.qpaysolutions.QPay.Card.ebanking;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Card.AddCardActivity;
import net.qpaysolutions.QPay.Card.CardListFragment;
import net.qpaysolutions.QPay.CustomerKYC.DialogInterface;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/18/18.
 */

public class DepositEBankingFragment extends Fragment implements DialogInterface, EbankInterface {
    private QPayProgressDialog qPayProgressDialog;
    private String bankId, amount;
    private TextInputLayout nameLayout, phoneLayout, emailLayout, remarksLayout, amountSelectLayout;
    private EditText name, phone, email, remarks, amountSelect;
    private String bankName;
    private String bankUrl;
    private TextView bNameTextView;
    private ImageView bankLogo;
    private QPayCustomerDatabase qpayDatabase;
    private String first_name, last_name, up_first_name, up_last_name, custumer_phone, customer_email;
    private ArrayList<EbankModel> arrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ebanking_deposit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            Toolbar toolbar = ((AddCardActivity) getActivity()).findViewById(R.id.toolbar);
            TextView title = ((AddCardActivity) getActivity()).findViewById(R.id.title);
            title.setText("Visa/Master Card/Union Pay ");
            toolbar.setNavigationOnClickListener(v -> {
                Utils.hideKeyword(getActivity());
                getActivity().onBackPressed();
            });
        } catch (Exception e) {

        }
        qpayDatabase = new QPayCustomerDatabase(getActivity());
        qpayDatabase.getReadableDatabase();
        qpayDatabase.getWritableDatabase();
        first_name = qpayDatabase.getFirstName();
        last_name = qpayDatabase.lastName();
        remarksLayout = view.findViewById(R.id.inputLayoutRemark);
        custumer_phone = qpayDatabase.getCustomerPhone();

        try {
            up_first_name = first_name.substring(0, 1).toUpperCase() + first_name.substring(1);
            up_last_name = last_name.substring(0, 1).toUpperCase() + last_name.substring(1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        qPayProgressDialog = new QPayProgressDialog(getActivity());
        Utils.hideKeyword(getActivity());
        view.findViewById(R.id.confirmButton).setOnClickListener(view1 -> {
            try {
                if (email.getText().toString() == null || email.getText().toString().isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() == false) {
                    emailLayout.setError("Enter Valid Email");
                } else if (remarks.getText().toString() == null || remarks.getText().toString().isEmpty()) {
                    remarksLayout.setError("Enter Remarks");
                } else {
                    if (amountSelect.getText().toString() != null && !amountSelect.getText().toString().isEmpty() && Double.parseDouble(amountSelect.getText().toString()) >= 100 && Double.parseDouble(amountSelect.getText().toString()) <= 5000) {
                        new PaymentGateWayCustTopup(DepositEBankingFragment.this,getContext()).execute();
                    } else {
                        amountSelectLayout.setError("Enter amount in range 100-5000");
                    }
                }
            } catch (
                    NullPointerException e) {
                e.printStackTrace();
                amountSelectLayout.setError("Enter amount in range 100-5000");
            }
        });
        name = view.findViewById(R.id.fullName);
        phone = view.findViewById(R.id.phone);
        email = view.findViewById(R.id.email);
        remarks = view.findViewById(R.id.remark);
        bNameTextView = view.findViewById(R.id.bankName);
        bankLogo = view.findViewById(R.id.bankLogo);
        emailLayout = view.findViewById(R.id.inputLayoutEmail);
        amountSelect = view.findViewById(R.id.amountSelect);
        amountSelectLayout = view.findViewById(R.id.amountSelectLayout);
        amountSelect.setInputType(InputType.TYPE_CLASS_NUMBER);
        amountSelect.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                amountSelectLayout.setError(null);
            }
        });

        if (getArguments().getString("name").equals("connect_ips")) {
            new ConnectIPS(ebankModels -> {
                bankName = arrayList.get(0).getName();
                bankId = arrayList.get(0).getId();
                bankUrl = arrayList.get(0).getLogoUrl();
                bNameTextView.setText(bankName);
                Log.d(Utility.TAG, "bank name: " + bankName + " bank url : " + bankUrl);
                Picasso.get().load(bankUrl).placeholder(R.drawable.bank_default_logo).into(bankLogo);
            }).execute("IPS");

        } else if (getArguments().getString("name").equals("other")) {
            new ConnectIPS(new GetConnectIPSResponse() {
                @Override
                public void onResponse(ArrayList<EbankModel> ebankModels) {
                    bankName = arrayList.get(0).getName();
                    bankId = arrayList.get(0).getId();
                    bankUrl = arrayList.get(0).getLogoUrl();
                    bNameTextView.setText(bankName);
                    Log.d(Utility.TAG, "bank name: " + bankName + " bank url : " + bankUrl);

                    Picasso.get().load(bankUrl).placeholder(R.drawable.bank_default_logo).into(bankLogo);
                }
            }).execute("card");

        } else {
            bankName = getArguments().getString("name");
            bankId = getArguments().getString("gateway");
            bankUrl = getArguments().getString("url");
        }

        bNameTextView.setText(bankName);

        Picasso.get().load(bankUrl).placeholder(R.drawable.ic_default_bank_account).into(bankLogo);
//        setAdapterAmount();


        name.setText(up_first_name + " " + up_last_name);
        phone.setText(custumer_phone);
        name.setEnabled(false);
        phone.setEnabled(false);
        //emailLayout.setVisibility(View.GONE);

        /*if (GeneralPref.getemail() != null) {
            email.setText(GeneralPref.getemail());
            email.setEnabled(false);
        } else {
            emailLayout.setVisibility(View.GONE);
        }*/

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                nameLayout.setError(null);
            }
        });
        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                phoneLayout.setError(null);
            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                emailLayout.setError(null);
            }
        });
        remarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                remarksLayout.setError(null);
            }
        });
    }

    private boolean isValid() {
//        if (name.getText().toString() == null || name.getText().toString().isEmpty() || name.getText().toString().length() < 5) {
//            nameLayout.setError("Enter Full Name");
//            return false;
//        }
//        if (phone.getText().toString() == null || phone.getText().toString().isEmpty() || phone.getText().toString().length() != 10) {
//            phoneLayout.setError("Enter Valid Number");
//            return false;
//        }
        if (email.getText().toString() == null || email.getText().toString().isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() == false) {
            emailLayout.setError("Enter Valid Email");
            return false;

        }
//        if (remarks.getText().toString() == null || remarks.getText().toString().isEmpty()) {
//            remarksLayout.setError("Enter Remarks");
//            return false;
//        }
        return true;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showDialog() {
        qPayProgressDialog.show();
    }

    @Override
    public void hideDialog() {
        qPayProgressDialog.dismiss();
        qPayProgressDialog.hide();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (qPayProgressDialog != null) {
            qPayProgressDialog.dismiss();
        }
    }

    @Override
    public void getelist(ArrayList<EbankModel> arrayList) {
        setAdapter(arrayList);

    }

    private void setAdapter(final ArrayList<EbankModel> arrayList) {
        Spinner bankSelect = (Spinner) getView().findViewById(R.id.bankSelect);
        EbankSpineerAdapter customAdapter = new EbankSpineerAdapter(getActivity(), arrayList, DepositEBankingFragment.this);
        bankSelect.setAdapter(customAdapter);
        bankSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bankId = arrayList.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    public void setPicasso(ImageView picasso, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get().load(url).error(R.drawable.user_photo_placeholder).placeholder(R.drawable.user_photo_placeholder).into(picasso);
        }
    }

    public JSONObject postJson() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("gatewayId", bankId);
            jsonObject.put("amount", amountSelect.getText().toString());
            jsonObject.put("remarks", remarks.getText().toString());
            jsonObject.put("custName", name.getText().toString());
            jsonObject.put("custEmail", email.getText().toString());
            jsonObject.put("custMobile", phone.getText().toString());
            return jsonObject;
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void openWebView(String s) {
       /* Intent intent = new Intent(getActivity(), EBankActivity.class);
        intent.putExtra("url", s);
        intent.putExtra("isPost", "false");
        startActivity(intent);*/
        String url = s;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
        getActivity().finish();
    }

    public void openWebViewPostForm(String s, JSONObject object) {
        Intent intent = new Intent(getActivity(), EBankActivity.class);
        intent.putExtra("url", s);
        intent.putExtra("json", object.toString());
        intent.putExtra("isPost", "true");
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                Utils.hideKeyword(getActivity());
               getActivity().onBackPressed();
                return true;
            }
            return false;
        });
    }


    private class ConnectIPS extends AsyncTask<String, String, String> {
        private QPayProgressDialog qPayProgressDialog;
        private GetConnectIPSResponse connectIPSResponse;

        public ConnectIPS(GetConnectIPSResponse connectIPSResponse) {
            this.connectIPSResponse = connectIPSResponse;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", GeneralPref.getAppId());
                jsonObject.put("id", GeneralPref.getCustId());
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                jsonObject.put("type", strings[0]);
                return NetworkAPI.sendHTTPData(Constants.PAYMENT_GATEWAY, jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                Utility.isBuild(s);
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        EbankModel ebankModel = new EbankModel();
                        ebankModel.setId(object.getString("Id"));
                        ebankModel.setLogoUrl(object.getString("LogoUrl"));
                        ebankModel.setName(object.getString("Name"));
                        ebankModel.setType(object.getString("Type"));
                        arrayList.add(ebankModel);
                    }
                    connectIPSResponse.onResponse(arrayList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getActivity());
            qPayProgressDialog.show();
        }
    }

    public interface GetConnectIPSResponse {
        void onResponse(ArrayList<EbankModel> ebankModels);
    }
}
