package net.qpaysolutions.QPay.Card;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by QPay on 1/27/2019.
 */

public class TopupCardActivity extends AppCompatActivity {
    EditText mCardPin;
    Button mDoneButton;
    String selectedItem = "";
    String id;
    net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase QPayCustomerDatabase;
    String last_4;
    QPayProgressDialog pDialog;
    String app_id, cust_id;
    private String status_topoff_final, stan_code_topoff;
    int count = 0;
    private Handler handler = new Handler();
    private String displayMsg;
    private TextView amount_pay;
    private String url_card, card_name, card_exp;
    TextView card_lastfour, card_exp_date, bank_name;
    ImageView card_load;
    List<String> ncellRechargeAmountList;
    String card_token;
    private ImageView cardImage;
    private Toolbar toolbar;
    public TextView title;

    private String balance = "0.00";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        toolbar = findViewById(R.id.toolbar);
        title = findViewById(R.id.title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        title.setText("Top-Up");
        toolbar.setNavigationOnClickListener(v -> {
            Utils.hideKeyword(TopupCardActivity.this);
            onBackPressed();
        });

        try {

            id = getIntent().getStringExtra("id");
            try {
                QPayCustomerDatabase = new QPayCustomerDatabase(this);
                QPayCustomerDatabase.getReadableDatabase();
                QPayCustomerDatabase.getWritableDatabase();
                app_id = QPayCustomerDatabase.getKeyAppId();
                cust_id = QPayCustomerDatabase.getCustomerID();
            } catch (Exception e) {
                e.printStackTrace();
            }
            last_4 = QPayCustomerDatabase.lastFour(id);
            card_name = QPayCustomerDatabase.getBankName(id);
            url_card = QPayCustomerDatabase.getImageId(id);
            card_exp = QPayCustomerDatabase.getCardExp(id);
            card_token = QPayCustomerDatabase.getCardType(id);
            (mDoneButton = (Button) findViewById(R.id.done_button_id)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(Utility.TAG, "amount selected : " + selectedItem);
                    if (mCardPin.getText().toString().length() < 4) {
                        mCardPin.setError("enter valid pin");
                        return;
                    } else {
                        if (selectedItem.equals("500.00") || selectedItem.equals("1000.00") || selectedItem.equals("1500.00") || selectedItem.equals("2000.00") || selectedItem.equals("2500.00") || selectedItem.equals("3000.00") || selectedItem.equals("4000.00") || selectedItem.equals("5000.00") || selectedItem.equals("6000.00") || selectedItem.equals("7000.00") || selectedItem.equals("8000.00") || selectedItem.equals("10000.00")) {
                            new TopOffAsyk(last_4, mCardPin.getText().toString(), selectedItem, card_token).execute();
                        } else {
                            Toast.makeText(TopupCardActivity.this, "Insert valid amount", Toast.LENGTH_LONG).show();
                        }

                    }
                }
            });
            cardImage = findViewById(R.id.cardImageView);
            Picasso.get()
                    .load(AddManageCardFragment.ulrImage)
                    .placeholder(R.drawable.ic_sct_card)
                    .error(R.drawable.ic_sct_card)
                    .noFade()
                    .into(cardImage);
            mCardPin = (EditText) findViewById(R.id.card_pin_id);
            mCardPin.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.length() == 4) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    }
                }
            });
            amount_pay = (TextView) findViewById(R.id.amount_pay);
            amount_pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAmount(v);
                }
            });
            card_lastfour = (TextView) findViewById(R.id.card_lastfour);
            card_exp_date = (TextView) findViewById(R.id.card_exp);
            card_exp_date.setText(card_exp);
            card_lastfour.setText("XXXX-XXXX-XXXX-" + last_4);
            card_load = (ImageView) findViewById(R.id.card_load);
            bank_name = (TextView) findViewById(R.id.bank_name);
            bank_name.setText(card_name);
            Picasso.get()
                    .load(url_card)
                    .placeholder(R.drawable.ic_sct_card)
                    .error(R.drawable.ic_sct_card)
                    .noFade()
                    .into(card_load);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.hideKeyword(TopupCardActivity.this);
        finish();
    }

    private class TopOffAsyk extends AsyncTask<Void, Void, JSONObject> {
        String last_4, pin, amount, card_token;

        private TopOffAsyk(String last_4, String pin, String amount, String card_token) {
            this.last_4 = last_4;
            this.pin = pin;
            this.amount = amount;
            this.card_token = card_token;
        }

        @Override
        protected void onPreExecute() {
            pDialog = new QPayProgressDialog(TopupCardActivity.this);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            String line = null;
            JSONObject Response = null;
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("last_4", last_4);
                jsonObject.put("amount", amount);
                jsonObject.put("pin", pin);
                jsonObject.put("card_token", card_token);
                jsonObject.put("t_flag", "0");
                jsonObject.put("lat", ((CustomerApplication) TopupCardActivity.this.getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) TopupCardActivity.this.getApplication()).getLng());
                String data = jsonObject.toString();
                Log.d(Utility.TAG, "data for post payment_option card self top off :" + data);
                String yourURL = Constants.TOP_OFF;

                String result = new NetworkAPI().sendHTTPData(yourURL, jsonObject);
                Log.d(Utility.TAG, "Response from php = " + result);
                Response = new JSONObject(result);

            } catch (Exception e) {
                Log.d(Utility.TAG, "Error Encountered");
                e.printStackTrace();
            }
            return Response;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            super.onPostExecute(args);

            try {
                Log.d(Utility.TAG, args.toString());
                JSONObject selftopoffResult = args.getJSONObject("selftopoffResult");
                String status = selftopoffResult.getString("status");
                String crrn = selftopoffResult.getString("crrn");
                stan_code_topoff = selftopoffResult.getString("stan");
                String resp_code = selftopoffResult.getString("resp_code");
                if (status.equals("00") && !status.isEmpty() && !status.equals("null") && status != null) {
                    if (resp_code.equals("00")) {
                        final String top_off_final = Constants.CHECK_STATUS_FINAL_VERIFICATION + cust_id + "/" + stan_code_topoff + "/" + "1";
                        runintimer(app_id, cust_id, stan_code_topoff, crrn);
                    } else if (resp_code.equals("CN")) {
                        QPayCustomerDatabase.deletCard(Long.valueOf(id));
                        pDialog.dismiss();
                        pDialog.hide();
                        custumdialogfailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");
                    } else {
                        pDialog.dismiss();
                        pDialog.hide();
                        custumdialogfailure("Topup Failure!", "Your transaction could not be processed.");
                    }

                } else if (status.equals("CN")) {
                    QPayCustomerDatabase.deletCard(Long.valueOf(id));
                    pDialog.dismiss();
                    pDialog.hide();
                    custumdialogfailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");

                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    custumdialogfailure("Topup Failure!", "Your transaction could not be processed.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                pDialog.hide();
                custumdialogfailure("Topup Failure!", "Your transaction could not be processed.");
            }
        }
    }

    public void runintimer(final String app_id, final String cust_id, final String stan_code_topoff, final String crrn) {
        count++;
        Log.d(Utility.TAG, "counter for top-up : " + count);
        handler.postDelayed(new Runnable() {
            public void run() {
                new CheckTxnStatusSTResult(app_id, cust_id, stan_code_topoff, "0", crrn).execute();

            }
        }, Tags.TIME_SLEEP);
    }

    public class CheckTxnStatusSTResult extends AsyncTask<String, String, String> {
        String app_id, cust_id, stan, flag, crrn;

        public CheckTxnStatusSTResult(String app_id, String cust_id, String stan, String flag, String crrn) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.stan = stan;
            this.flag = flag;
            this.crrn = crrn;
        }

        @Override
        protected String doInBackground(final String... params) {
            String resp_code = null;
            JSONObject jsonObject_verify = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result_top_off_final;
            try {
                jsonObject_verify.put("app_id", app_id);
                jsonObject_verify.put("cust_id", cust_id);
                jsonObject_verify.put("stan", stan);
                jsonObject_verify.put("t_flag", flag);
                jsonObject_verify.put("crrn", crrn);
                Log.d(Utility.TAG, "post final : " + jsonObject_verify.toString());
                result_top_off_final = networkAPI.sendHTTPData(Constants.CHECK_STATUS_FINAL_VERIFICATION, jsonObject_verify);
                Log.d(Utility.TAG, "response final : " + result_top_off_final);
                JSONObject jsonObject = new JSONObject(result_top_off_final);
                JSONObject jsonObject1 = jsonObject.getJSONObject("CheckTxnStatusSTResult");
                status_topoff_final = jsonObject1.getString("status");
                resp_code = jsonObject1.getString("resp_code");
                displayMsg = jsonObject1.getString("displayMsg");
                balance=jsonObject1.getString("avail_bal");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resp_code;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                System.out.println("Dinesh::P<><>:: " + s);
              /*  pDialog.dismiss();
                pDialog.hide();*/
                if (status_topoff_final != null && !status_topoff_final.isEmpty() && status_topoff_final.equals("00")) {
                    if (s.equals("LO")) {
                        if (count < 10) {
                            runintimer(app_id, cust_id, stan_code_topoff, crrn);
                        } else {
                            pDialog.dismiss();
                            pDialog.hide();
                            custumdialogfailure("Topup Failure!", "Your transaction could not be processed.");

                        }

                    } else if (s.equals("00")) {
                        pDialog.dismiss();
                        pDialog.hide();
                        new Ackclass(app_id, cust_id, stan, crrn).execute();
                        custumdialogSucess("Topup Success!", displayMsg);


                    } else if (s.equals("99")) {
                        pDialog.dismiss();
                        pDialog.hide();
                        custumdialogfailure("Topup Failure!", "Your transaction could not be processed.");
                    } else {
                        new Ackclass(app_id, cust_id, stan, crrn).execute();
                        pDialog.dismiss();
                        pDialog.hide();
                        custumdialogfailure("Topup Failure!", displayMsg);
                    }
                } else {
                    pDialog.dismiss();
                    pDialog.hide();
                    custumdialogfailure("Topup Failure!", "Your transaction could not be processed.");
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                    pDialog.dismiss();
                    pDialog.hide();
                    custumdialogfailure("Topup Failure!", "Your transaction could not be processed.");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        }
    }

    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, cust_id, stan, crrn;

        public Ackclass(String app_id, String cust_id, String stan, String crrn) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.stan = stan;
            this.crrn = crrn;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPI = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn", crrn);
                ack_test = networkAPI.sendHTTPData(Constants.ACKN, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }

    private void hideKeyboard() {
        View view = TopupCardActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) TopupCardActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showAmount(View view) {
        String[] utl = getResources().getStringArray(R.array.topup_amount);
        ncellRechargeAmountList = new ArrayList<>(Arrays.asList(utl));

        amount_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Utility().hideSoftkey(TopupCardActivity.this);
                final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(TopupCardActivity.this, new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int month, String dateDesc) {
                        selectedItem = dateDesc;
                        amount_pay.setText(dateDesc);
                    }
                }).textConfirm("DONE")
                        .textCancel("CANCEL")
                        .btnTextSize(16)
                        .viewTextSize(25)
                        .setValues(ncellRechargeAmountList)
                        .build();
                pickerPopWin.showPopWin(TopupCardActivity.this);
            }
        });
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(TopupCardActivity.this);
        LayoutInflater inflater = TopupCardActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_image = (ImageView) v.findViewById(R.id.dialog_image);
        dialog_image.setImageResource(R.drawable.ic_card_verification_failure);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
              /*  try {

                    CardProfileMain cardProfileMain = new CardProfileMain();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, cardProfileMain)
                            .commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void custumdialogSucess(String title_display, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(TopupCardActivity.this);
        LayoutInflater inflater = TopupCardActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        final ImageView dialog_icon = (ImageView) v.findViewById(R.id.dialog_icon);
        dialog_icon.setImageResource(R.drawable.ic_card_verification_success);
        title.setText(title_display);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(!balance.equalsIgnoreCase("0.00") || !balance.equalsIgnoreCase("0")) {
                    BalancePref.setAvailableAmount(balance);
                }
                startActivity(new Intent(TopupCardActivity.this, MainActivity.class));
                finishAffinity();
                /*try {

                    CardProfileMain cardProfileMain = new CardProfileMain();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, cardProfileMain)
                            .commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }
}
