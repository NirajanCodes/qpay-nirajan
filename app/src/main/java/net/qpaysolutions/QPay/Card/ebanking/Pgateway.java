package net.qpaysolutions.QPay.Card.ebanking;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/18/18.
 */

public class Pgateway extends AsyncTask<String, String, String> {
    private BankListFragment depositEBankingFragment;
    private QPayProgressDialog qPayProgressDialog;
    private LinearLayout progressLayout;

    public Pgateway(BankListFragment depositEBankingFragment, LinearLayout progressLayout) {
        this.depositEBankingFragment = depositEBankingFragment;
        this.progressLayout = progressLayout;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("type", strings[0]);
            Log.d("dinesh", "json...." + jsonObject);
            return NetworkAPI.sendHTTPData(Constants.PAYMENT_GATEWAY, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
//        qPayProgressDialog.dismiss();
        Log.d("dinesh", s + "");
        try {
            Utility.isBuild(s);
            JSONObject jsonObject = new JSONObject(s);
            boolean success = jsonObject.getBoolean("success");
            if (success) {
                JSONArray data = jsonObject.getJSONArray("data");
                ArrayList<EbankModel> arrayList = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    JSONObject object = data.getJSONObject(i);
                    EbankModel ebankModel = new EbankModel();
                    ebankModel.setId(object.getString("Id"));
                    ebankModel.setLogoUrl(object.getString("LogoUrl"));
                    ebankModel.setName(object.getString("Name"));
                    ebankModel.setType(object.getString("Type"));
                    arrayList.add(ebankModel);
                }
                depositEBankingFragment.getelist(arrayList);
                progressLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            progressLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        qPayProgressDialog = new QPayProgressDialog(depositEBankingFragment.getActivity());
//        qPayProgressDialog.show();
        progressLayout.setVisibility(View.VISIBLE);
    }
}
