package net.qpaysolutions.QPay.Card;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

/**
 * Created by deadlydragger on 6/7/16.
 */
public class CardListTopoff extends CursorAdapter {
    private TopoffselectCard cardProfileMain;
    int count=0;

    public CardListTopoff(Context context, Cursor c,TopoffselectCard cardProfileMain) {
        super(context, c, 0);
        this.cardProfileMain=cardProfileMain;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.setting_row_list_card_topoff, parent, false);
    }
    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        try {
            ImageView logo_setting = (ImageView) view.findViewById(R.id.img_logo_setting);
            TextView card_num = (TextView) view.findViewById(R.id.card_number);
            TextView exp_date = (TextView) view.findViewById(R.id.exp_date);
//        TextView balance_card = (TextView) view.findViewById(R.id.balance_card);
            TextView bank_name=(TextView)view.findViewById(R.id.bank_name);
            TextView account_balance = (TextView)view.findViewById(R.id.account_balance);
            TextView account_date = (TextView)view.findViewById(R.id.account_date);
            String prepaid = cursor.getString(cursor.getColumnIndex("card_type"));
            final String flag = cursor.getString(cursor.getColumnIndex("act_flag"));
            String last_4 = cursor.getString(cursor.getColumnIndex("last_4"));
            String exp = cursor.getString(cursor.getColumnIndex("exp_date"));
            String profile_name= cursor.getString(cursor.getColumnIndex("profile_name"));
            String img_id = cursor.getString(cursor.getColumnIndex("img_id"));
            final String _id = cursor.getString(cursor.getColumnIndex("_id"));
            final String card_balance = cursor.getString(cursor.getColumnIndexOrThrow("card_balance"));
            final String known_date = cursor.getString(cursor.getColumnIndexOrThrow("known_date"));
            final String flag_db=flag.trim();
            Log.d(Utility.TAG,"flag is : "+flag);
            bank_name.setText(profile_name);
            try {
                count=card_balance.length();

                if (count > 0){
                    account_balance.setText("NPR "+card_balance);
                    account_date.setText(" as of "+known_date);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            exp_date.setText(exp.substring(2)+"/"+exp.substring(0,2));
            Picasso.get()
                    .load(img_id)
                    .placeholder(R.drawable.ic_sct_card)
                    .error(R.drawable.ic_sct_card)
                    .noFade()
                    .into(logo_setting);
            card_num.setText(" XXXX - XXXX - XXXX - " + last_4);
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}


