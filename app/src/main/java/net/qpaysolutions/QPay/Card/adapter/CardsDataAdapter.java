package net.qpaysolutions.QPay.Card.adapter;

import android.content.Context;
import android.view.GestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.Card.AddManageCardFragment;
import net.qpaysolutions.QPay.Card.CardPOJO;
import net.qpaysolutions.QPay.R;

import java.util.List;

public class CardsDataAdapter extends ArrayAdapter<CardPOJO> {
    private Context context;
    private AddManageCardFragment addManageCardFragment;
    private List<CardPOJO> cardPOJOList;
    private OnCardClickListener onCardClickListener;
    private GestureDetector mDetector;

    public CardsDataAdapter(Context context, AddManageCardFragment addManageCardFragment, List<CardPOJO> cardPOJOList, OnCardClickListener onCardClickListener) {
        super(context, R.layout.card_content);
        this.context = context;
        this.addManageCardFragment = addManageCardFragment;
        this.cardPOJOList = cardPOJOList;
        this.onCardClickListener = onCardClickListener;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View contentView, ViewGroup parent) {
        RelativeLayout mainLayout = (contentView.findViewById(R.id.mainLayout));
        ImageView v = (contentView.findViewById(R.id.getImage));
        TextView cardNumber = (contentView.findViewById(R.id.cardNumberTextView));
        TextView amount = (contentView.findViewById(R.id.amountTextView));
        TextView expiryDate = (contentView.findViewById(R.id.expiryDateTextView));


        try {
            final CardPOJO cardPOJO = cardPOJOList.get(position);
            LinearLayout verifyBttn = addManageCardFragment.getActivity().findViewById(R.id.verify_card_layout);
            TextView verifyTextview = addManageCardFragment.getActivity().findViewById(R.id.verify_textview);
            ImageView verifyBalanceInqImageview = addManageCardFragment.getActivity().findViewById(R.id.verify_imageview);
            LinearLayout topupBttn = addManageCardFragment.getActivity().findViewById(R.id.topup_layout);
            LinearLayout deleteBttn = addManageCardFragment.getActivity().findViewById(R.id.delete_layout);


            addManageCardFragment.setVisibleGone();
            if (position == cardPOJOList.size() - 1) {
                addManageCardFragment.setVisibleGone();
                topupBttn.setVisibility(View.GONE);

            } else {
                addManageCardFragment.setVisible();
                topupBttn.setVisibility(View.GONE);
                String flag_db = cardPOJOList.get(position + 1).getActFlag().trim();
                if (flag_db.equals("R")) {
                    verifyTextview.setText("Verify Card");
                }
                if (flag_db.equals("V")) {
                    verifyTextview.setText("Balance Inq");
                    verifyBalanceInqImageview.setImageResource(R.drawable.ic_card_balance_inquiry);
                    topupBttn.setVisibility(View.VISIBLE);
                }
                verifyBttn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onCardClickListener.onCardVerifyListener(cardPOJOList.get(position + 1),position+1);
//                        addManageCardFragment.verifyCard(cardPOJO.getCardId());
                    }
                });

                topupBttn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onCardClickListener.onCardTopupListener(cardPOJOList.get(position + 1),position+1);
                    }
                });
                deleteBttn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onCardClickListener.onCardDeleteListener(cardPOJOList.get(position + 1),position+1);
                    }
                });

            }


            System.out.println("CardName:: " + cardPOJO.getCardName() + " position:: " + position);
            if (cardPOJO.getCardName().equalsIgnoreCase("QPay")) {
                Picasso.get()
                        .load(R.drawable.upi_card_sample)
                        .placeholder(R.drawable.upi_card_sample)
                        .error(R.drawable.upi_card_sample)
                        .noFade()
                        .fit()
                        .into(v);
                cardNumber.setText(" XXXX - XXXX - XXXX - " + cardPOJO.getLast4DigitCardNumber());
                amount.setText(cardPOJO.getCardBalance() != null ? " Wallet NPR " + cardPOJO.getCardBalance() : " Wallet NPR 0.0");
            } else {
                Picasso.get()
                        .load(cardPOJO.getImgId())
                        .placeholder(R.drawable.ic_sct_card)
                        .error(R.drawable.ic_sct_card)
                        .noFade()
                        .into(v);
                amount.setText(cardPOJO.getCardBalance() != null ? "Last Known NPR " + cardPOJO.getCardBalance() : "");
                expiryDate.setText(cardPOJO.getExpiryDate() != null ? "Exp " + cardPOJO.getExpiryDate()
                        .substring(2) + "/" + cardPOJO.getExpiryDate().substring(0, 2) : "");

                cardNumber.setText(" XXXX - XXXX - XXXX - " + cardPOJO.getLast4DigitCardNumber());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentView;
    }


    public interface OnCardClickListener {
        void onCardVerifyListener(CardPOJO cardPOJO, int position);

        void onCardTopupListener(CardPOJO cardPOJO, int position);

        void onCardDeleteListener(CardPOJO cardPOJO, int position);

    }

}


