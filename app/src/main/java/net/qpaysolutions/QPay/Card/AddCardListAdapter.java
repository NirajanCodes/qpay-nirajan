package net.qpaysolutions.QPay.Card;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;


/**
 * Created by QPay on 7/12/2018.
 */

public class AddCardListAdapter extends CursorAdapter {

    private AddManageCardFragment cardProfileMain;

    public AddCardListAdapter(Context context, Cursor c, AddManageCardFragment cardProfileMain) {
        super(context, c, 0);
        this.cardProfileMain = cardProfileMain;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.card_list_row, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        ImageView logo_setting = view.findViewById(R.id.img_logo_setting);
        TextView card_num = view.findViewById(R.id.card_number);
        TextView exp_date = view.findViewById(R.id.exp_date);
        TextView balance_card = view.findViewById(R.id.balance_card);
        TextView bank_name = view.findViewById(R.id.bank_name);
        TextView account_type = view.findViewById(R.id.account_type);
        TextView account_balance = view.findViewById(R.id.account_balance);
        ImageView adapter_card_list = (ImageView) view.findViewById(R.id.card_icon);
        TextView account_bknown_date = view.findViewById(R.id.account_date);
        try {
            String prepaid = cursor.getString(cursor.getColumnIndex("card_type"));
            final String flag = cursor.getString(cursor.getColumnIndex("act_flag"));
            String last_4 = cursor.getString(cursor.getColumnIndex("last_4"));
            String exp = cursor.getString(cursor.getColumnIndex("exp_date"));
            String profile_name = cursor.getString(cursor.getColumnIndex("profile_name"));
            String img_id = cursor.getString(cursor.getColumnIndex("img_id"));
            final String _id = cursor.getString(cursor.getColumnIndex("_id"));
            final String card_no = cursor.getString(cursor.getColumnIndexOrThrow("card_full"));
            final String card_nature = cursor.getString(cursor.getColumnIndexOrThrow("card_nature"));
            final String card_balance = cursor.getString(cursor.getColumnIndexOrThrow("card_balance"));
            final String known_date = cursor.getString(cursor.getColumnIndexOrThrow("known_date"));
            Log.d(Utility.TAG, "card balance : " + card_balance + "known date : " + known_date);
            Log.d(Utility.TAG, "card no full : " + card_no);
            Log.d(Utility.TAG, "card type nnature : " + card_nature);
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (card_nature.equals("S")) {
                account_type.setText("Saving account");
            } else if (card_nature.equals("C")) {
                account_type.setText("Checking account");
            }
            if (profile_name.equals("NA")) {
                bank_name.setText("SCT Card");
            } else {
                bank_name.setText(profile_name);
            }
            final String flag_db = flag.trim();
            Log.d(Utility.TAG, "flag is : " + flag);
            if (flag_db.equals("V")) {
                adapter_card_list.setImageResource(R.drawable.ic_card_verified);
                balance_card.setText("Balance Inq");
                balance_card.setTextColor(Color.parseColor("#1aace1"));
            } else if (flag_db.equals("R")) {
                adapter_card_list.setImageResource(R.drawable.ic_unverified_card);
                balance_card.setText("Verify Card");
                balance_card.setTextColor(Color.parseColor("#fa5b5b"));
            }
            exp_date.setText(exp.substring(2) + "/" + exp.substring(0, 2));
            Picasso.get()
                    .load(img_id)
                    .placeholder(R.drawable.ic_sct_card)
                    .error(R.drawable.ic_sct_card)
                    .noFade()
                    .into(logo_setting);
            card_num.setText(" XXXX - XXXX - XXXX - " + last_4);
            balance_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flag_db.equals("V")) {
                        cardProfileMain.getCardBalance(_id);
                    } else if (flag_db.equals("R")) {
                        cardProfileMain.verifyCard(_id);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
