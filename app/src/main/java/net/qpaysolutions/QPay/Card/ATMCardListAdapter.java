package net.qpaysolutions.QPay.Card;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Card.ebanking.EbankModel;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by QPay on 6/14/2018.
 */

public class ATMCardListAdapter extends RecyclerView.Adapter<ATMCardListAdapter.ATMCardListViewHolder> {
    private ArrayList<EbankModel> arrayList;
    private Context context;
    private AtmClickListener listener;

    public ATMCardListAdapter(ArrayList<EbankModel> arrayList, Context context, AtmClickListener listener) {
        this.arrayList = arrayList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ATMCardListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ATMCardListViewHolder(LayoutInflater.from(context).inflate(R.layout.item_atm_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ATMCardListViewHolder holder, final int position) {
        holder.cardName.setText(arrayList.get(position).getName());
        if (!arrayList.get(position).getDescription().equals("")) {
            holder.cardSupportLabel.setText(arrayList.get(position).getDescription());

        } else {
            holder.cardSupportLabel.setText("Load via " + arrayList.get(position).getName());
        }

        int drawable=Integer.parseInt(arrayList.get(position).getLogoUrl());
        Picasso.get().load(drawable).placeholder(R.drawable.ic_bank_default).into(holder.cardLogo);
        holder.atmLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listener.onAtmClickListener(position);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ATMCardListViewHolder extends RecyclerView.ViewHolder {
        private ImageView cardLogo;
        private TextView cardName;
        private TextView cardSupportLabel;
        private RelativeLayout atmLayout;

        public ATMCardListViewHolder(View itemView) {
            super(itemView);
            cardLogo = itemView.findViewById(R.id.atmImageView);
            cardName = itemView.findViewById(R.id.atmTitle);
            cardSupportLabel = itemView.findViewById(R.id.atmLabel);
            atmLayout = itemView.findViewById(R.id.atmLayout);
        }
    }

    interface AtmClickListener {
        void onAtmClickListener(int position) throws IOException, InterruptedException;
    }
}
