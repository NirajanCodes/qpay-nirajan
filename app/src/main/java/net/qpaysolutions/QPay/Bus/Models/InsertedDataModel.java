package net.qpaysolutions.QPay.Bus.Models;

public class InsertedDataModel {
    String detail;

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }
}
