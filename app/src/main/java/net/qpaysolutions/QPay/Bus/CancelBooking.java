package net.qpaysolutions.QPay.Bus;

import android.util.Log;

import net.qpaysolutions.QPay.Bus.Models.SeatLayout;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CancelBooking {
    public CancelBooking(String ticketSerialNumber, String tripId, RefreshSeatLayout refreshSeatLayout) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("tripId", tripId);
            jsonObject.put("ticketSrlNo", ticketSerialNumber);
            Log.d("nirajan", "cancel : " + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        BusSearchInterface busSearchInterface = data -> {
            String numberOfColmns = null;

            try {
                ArrayList<SeatLayout> arrayListSeat = new ArrayList<>();
                JSONObject seatJson = new JSONObject(data);
                JSONArray datas = seatJson.getJSONArray("data");
                String message = seatJson.getString("message");
                String status = seatJson.getString("status");
                if (status.equalsIgnoreCase("00")) {
                    JSONObject jsonArray = datas.getJSONObject(0);
                    JSONArray seatLayoutArray = jsonArray.getJSONArray("SeatLayout");
                    numberOfColmns = jsonArray.getString("NoOfColumn");

                    for (int i = 0; i < seatLayoutArray.length(); i++) {
                        SeatLayout seatLayout = new SeatLayout();
                        JSONObject seatObject = seatLayoutArray.getJSONObject(i);
                        seatLayout.setBookingStatus(seatObject.getString("BookingStatus"));
                        seatLayout.setDisplayName(seatObject.getString("DisplayName"));
                        arrayListSeat.add(seatLayout);
                    }
                    refreshSeatLayout.refreshSeatLayout(arrayListSeat, numberOfColmns,message);
                }else {
                    refreshSeatLayout.refreshSeatLayout(arrayListSeat, numberOfColmns,message);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        };
        BusNetworking busNetworking = new BusNetworking(Constants.BUS_CANCELQUEUE, jsonObject, true, busSearchInterface);
        busNetworking.execute();
    }

    public interface RefreshSeatLayout {
        void refreshSeatLayout(ArrayList<SeatLayout> seatLayout, String numberOfColumns,String message);
    }
}
