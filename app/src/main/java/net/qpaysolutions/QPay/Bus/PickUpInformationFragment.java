package net.qpaysolutions.QPay.Bus;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.qpaysolutions.QPay.Billpayment.BillPayListSheet;
import net.qpaysolutions.QPay.Bus.Models.BookedTicketModel;
import net.qpaysolutions.QPay.Bus.Models.ObjectModel;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

public class PickUpInformationFragment extends Fragment {
    private ArrayList<ObjectModel> collection;
    private BookedTicketModel bookedTicketModel;
    private QPayProgressDialog qPayProgressDialog;
    private boolean billPayList = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bus_personal_information, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button proceed = view.findViewById(R.id.proceed);
        EditText contactNumber = view.findViewById(R.id.contact);
        EditText email = view.findViewById(R.id.email);
        qPayProgressDialog = new QPayProgressDialog(getContext());
        TextView boardingPoint = view.findViewById(R.id.boarding_locations);
        bookedTicketModel = (BookedTicketModel) getArguments().getSerializable("bookedTicketModel");
        String tripId = getArguments().getString("ticketId");
        String boardingPoints = getArguments().getString("boardingPoint");
        String price = getArguments().getString("price");
        boardingPoint.setText(boardingPoints);
        ArrayList<String> bookedSeats = (ArrayList<String>) getArguments().getSerializable("seats");
        UpdateInfoAdapter.GetInsertedInformation getInsertedInformation = collection -> PickUpInformationFragment.this.collection = collection;
        UpdateInfoAdapter updateInfoAdapter = new UpdateInfoAdapter(bookedSeats, bookedTicketModel, getInsertedInformation);
        RecyclerView recyclerView = view.findViewById(R.id.recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(updateInfoAdapter);
        /// on button clicked ///
        proceed.setOnClickListener(view1 -> {
            String validString = validate(contactNumber, email, bookedSeats);
            if (validString.equalsIgnoreCase("Valid")) {
                JSONArray passengerPriceArray = new JSONArray();
                JSONArray passengerDetail = new JSONArray();
                JSONArray passengerTypeArray = new JSONArray();
                JSONArray passengerPriceDetail = new JSONArray();
                JSONArray passengerFullDetail = new JSONArray();
                ArrayList<String> detailData = new ArrayList<>();
                for (int j = 0; j < bookedSeats.size(); j++) {
                    JSONObject passengerFullDetailObject = new JSONObject();
                    JSONObject passengerPriceObject = new JSONObject();
                    try {
                        passengerPriceObject.put("seat", bookedSeats.get(j));
                        passengerFullDetailObject.put("seat", bookedSeats.get(j));
                        passengerPriceObject.put("name", collection.get(j).getName().getText().toString());
                        assert bookedTicketModel != null;
                        if (bookedTicketModel.isMultiprice()) {
                            if (collection.get(j).getNationallity().getSelectedItem().toString().equalsIgnoreCase("Nepali")) {
                                passengerPriceObject.put("id", bookedTicketModel.getTicketPriceLists().get(0).getId());
                                passengerFullDetailObject.put("id", bookedTicketModel.getTicketPriceLists().get(0).getId());
                            } else if (collection.get(j).getNationallity().getSelectedItem().toString().equalsIgnoreCase("Indian")) {
                                passengerPriceObject.put("id", bookedTicketModel.getTicketPriceLists().get(1).getId());
                                passengerFullDetailObject.put("id", bookedTicketModel.getTicketPriceLists().get(1).getId());
                            } else {
                                passengerPriceObject.put("id", bookedTicketModel.getTicketPriceLists().get(2).getId());
                                passengerFullDetailObject.put("id", bookedTicketModel.getTicketPriceLists().get(2).getId());
                            }
                        }
                        passengerPriceArray.put(passengerPriceObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONObject passengerTypeDetailObject = new JSONObject();
                    detailData.add(collection.get(j).getName().getText().toString());
                    detailData.add(collection.get(j).getAge().getText().toString());
                    if (collection.get(j).getMale().isChecked()) {
                        detailData.add(collection.get(j).getMale().getText().toString());
                    } else {
                        detailData.add(collection.get(j).getFemale().getText().toString());
                    }
                    detailData.add(collection.get(j).getNationallity().getSelectedItem().toString());
                    try {
                        for (int i = 0; i < bookedTicketModel.getPassengerTypeDetails().size(); i++) {
                            JSONObject detail = new JSONObject();
                            try {
                                detail.put("detail", detailData.get(i));
                                detail.put("id", bookedTicketModel.getPassengerTypeDetails().get(i).getId());
                                passengerDetail.put(detail);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        passengerTypeDetailObject.put("seat", bookedSeats.get(j));
                        passengerTypeDetailObject.put("passengerDetail", passengerDetail);
                        passengerFullDetailObject.put("passengerDetail", passengerDetail);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    passengerTypeArray.put(passengerTypeDetailObject);
                    passengerFullDetail.put(passengerFullDetailObject);
                    passengerPriceDetail.put(passengerPriceObject);
                }

                JSONObject main = new JSONObject();
                try {
                    main.put("appId", GeneralPref.getAppId());
                    main.put("id", GeneralPref.getCustId());
                    main.put("lat", LatLngPref.getLat());
                    main.put("lng", LatLngPref.getLng());
                    main.put("tripId", tripId);
                    main.put("contactNumber", contactNumber.getText().toString());
                    main.put("email", email.getText().toString());
                    main.put("name", collection.get(0).getName().getText().toString());
                    main.put("boardingPoint", boardingPoint.getText().toString());
                    main.put("ticketSrlNo", bookedTicketModel.getTicketSrlNo());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (bookedTicketModel.isMultiprice() && bookedTicketModel.isPassengerTypeDetailRequired()) {
                        main.put("passengerFullDetail", passengerFullDetail);
                        Log.d("nirajan", "passengerFullDetail : " + passengerFullDetail);
                    } else if (bookedTicketModel.isMultiprice()) {
                        main.put("passengerPriceDetail", passengerPriceArray);
                        Log.d("nirajan", "passengerPriceDetail : " + passengerPriceDetail);

                    } else if (bookedTicketModel.isPassengerTypeDetailRequired()) {
                        main.put("passengerTypeArray", passengerTypeArray);
                        Log.d("nirajan", "passengerTypeArray : " + passengerTypeArray);
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                UpdatePassengerInfoApi.UpadatePassengerInterface upadatePassengerInterface = new UpdatePassengerInfoApi.UpadatePassengerInterface() {
                    @Override
                    public void updateInterface(String data) {
                        qPayProgressDialog.dismiss();
                        JSONObject jsonObject = null;
                        qPayProgressDialog.dismiss();
                        try {
                            jsonObject = new JSONObject(data);

                            if (jsonObject.getString("status").equalsIgnoreCase("00")) {
                                if (billPayList) {
                                    billPayList = false;
                                    int amountRs = 0;
                                    String amountDollar;
                                    String newPrice;
                                    if (price == null || price.isEmpty()) {
                                        for (int j = 0; j < bookedSeats.size(); j++) {
                                                if (collection.get(j).getNationallity().getSelectedItem().toString().equalsIgnoreCase("Nepali")) {
                                                    amountRs = amountRs + Integer.parseInt(bookedTicketModel.getTicketPriceLists().get(0).getPriceInRs());
                                                    amountDollar = bookedTicketModel.getTicketPriceLists().get(0).getPriceInDollar();
                                                } else if (collection.get(j).getNationallity().getSelectedItem().toString().equalsIgnoreCase("Indian")) {
                                                    amountRs = amountRs + Integer.parseInt(bookedTicketModel.getTicketPriceLists().get(1).getPriceInRs());
                                                    amountDollar = bookedTicketModel.getTicketPriceLists().get(1).getPriceInDollar();
                                                } else {
                                                    amountRs = amountRs + Integer.parseInt(bookedTicketModel.getTicketPriceLists().get(2).getPriceInRs());
                                                    amountDollar = bookedTicketModel.getTicketPriceLists().get(2).getPriceInDollar();
                                            }
                                        }
                                        newPrice = String.valueOf(amountRs);
                                    } else {
                                        newPrice = price;
                                    }
                                    Bundle bundle = new Bundle();
                                    bundle.putString("money", newPrice);
                                    bundle.putString("byt_code", "102");
                                    bundle.putString("cell_number", tripId);
                                    bundle.putString("cash_Id", bookedTicketModel.getTicketSrlNo());
                                    bundle.putBoolean("downloadTicket",true);
                                    BillPayListSheet billPayListSheet = new BillPayListSheet();
                                    billPayListSheet.setArguments(bundle);
                                    billPayListSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), getTag());
                                }
                                getActivity().runOnUiThread(() -> {
                                    Handler handler = new Handler();
                                    handler.postDelayed(() -> billPayList = true, 2000);
                                });
                            } else {
                                String message = jsonObject.getString("message");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
                new UpdatePassengerInfoApi(main, upadatePassengerInterface);
                Log.d("nirajan", "request " + main);
                qPayProgressDialog.show();
            } else {
                Toast.makeText(getContext(), validString, Toast.LENGTH_LONG).show();
            }
        });
    }

    String validate(TextView contactNumber, TextView email, ArrayList bookedSeats) {

        for (int j = 0; j < bookedSeats.size(); j++) {
            String name = collection.get(j).getName().getText().toString();
            String age = collection.get(j).getAge().getText().toString();
            int ageNumber = 0;
            if (bookedTicketModel.isPassengerTypeDetailRequired() && !age.isEmpty()) {
                ageNumber = Integer.parseInt(age);
            }
            if (bookedTicketModel.isPassengerTypeDetailRequired() && age.isEmpty()) {
                return "No filed should be empty";
            }
            if (name.isEmpty()) {
                return "No filed should be empty";
            } else if (name.length() < 4) {
                return "Name";
            } else if (bookedTicketModel.isPassengerTypeDetailRequired() && (ageNumber > 100 || ageNumber < 1)) {
                return "Invalid Age";
            }
        }

        String contact = contactNumber.getText().toString();
        String emailAdd = email.getText().toString();
        if (contact.isEmpty() && emailAdd.isEmpty()) {
            return "No filed should be empty";
        }
        if (contact.length() != 10) {
            return "Invalid contact number";
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailAdd).matches()) {
            return "Invalid email";
        }
        return "Valid";
    }
}
