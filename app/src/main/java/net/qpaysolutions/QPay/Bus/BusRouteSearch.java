package net.qpaysolutions.QPay.Bus;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONObject;

public class BusRouteSearch {

    public BusRouteSearch(BusSearchInterface busSearchInterface) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id",GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            new BusNetworking(Constants.BUSROUTES, jsonObject, true, busSearchInterface).execute();
        } catch (Exception e) {

        }
    }
}
