package net.qpaysolutions.QPay.Bus;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.qpaysolutions.QPay.Bus.Models.BookedTicketModel;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BookTicket {

    public BookTicket(String tripId, ArrayList seatNumber, Context context, OnModelSet onModelSet) {

        BusSearchInterface busSearchInterface = data -> {

            try {
                BookedTicketModel bookedTicketModel = null;
                JSONObject jsonObject = new JSONObject(data);
                JSONArray datas = jsonObject.getJSONArray("data");
                String message = jsonObject.getString("message");
                String status = jsonObject.getString("status");
                boolean success = jsonObject.getBoolean("success");
                //   if (status.equalsIgnoreCase("00") && success) {
                if (status.equalsIgnoreCase("00")) {
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    bookedTicketModel = gson.fromJson(datas.getJSONObject(0).toString(), BookedTicketModel.class);
                    onModelSet.onModelSet(bookedTicketModel, message);
                } else {
                    onModelSet.onModelSet(bookedTicketModel, message);
                }
            } catch (Exception e) {
                e.printStackTrace();
                onModelSet.onModelSet(null, null);
            }
        };
        Log.d("nirajan", "seat : " + seatNumber);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(seatNumber);

        try {
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("tripId", tripId);
            jsonObject.put("seat", jsonArray);
            Log.d("nirajan", "request bus ticket : " + jsonObject.toString());


        } catch (Exception e) {
            e.printStackTrace();
        }

        BusNetworking busNetworking = new BusNetworking(Constants.BUS_BOOKTICKET, jsonObject, true, busSearchInterface);
        busNetworking.execute();
    }

    public interface OnModelSet {
        void onModelSet(BookedTicketModel bookedTicketModel, String message);
    }
}
