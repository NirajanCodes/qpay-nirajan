package net.qpaysolutions.QPay.Bus;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.qpaysolutions.QPay.Bus.Adapters.TicketsAdapter;
import net.qpaysolutions.QPay.Bus.Models.TripInfoModel;
import net.qpaysolutions.QPay.Bus.SeatSelection.SeatSelectionFragment;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

public class SelectTicketFragment extends Fragment {
    ArrayList<TripInfoModel> tripsModelArrayList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_selectbusticket, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tripsModelArrayList = (ArrayList<TripInfoModel>) getArguments().getSerializable("arrayList");
        TicketsAdapter.OnClickRecylerItem onClickRecyclerItem = tripInfoModel -> {
            Bundle bundle = new Bundle();
            Log.d("nirajan", " selected Fragment  : ");
            bundle.putSerializable("tripsInfoModel", tripInfoModel);
            SeatSelectionFragment seatSelectionFragment = new SeatSelectionFragment();
            seatSelectionFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main, seatSelectionFragment,"Seats")
                    .addToBackStack("Seats")
                    .commit();
        };
        TicketsAdapter ticketsAdapter = new TicketsAdapter(tripsModelArrayList, onClickRecyclerItem);
        RecyclerView recyclerView = view.findViewById(R.id.rv_tickets);
        recyclerView.setAdapter(ticketsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
