package net.qpaysolutions.QPay.Bus;

import android.util.Log;

import net.qpaysolutions.QPay.Bus.Models.SeatLayout;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RefreshLayout {
    public RefreshLayout(String tripId, RefreshLayoutInterface refreshLayoutInterface) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id", GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("tripId", tripId);
            Log.d("nirajan", "cancel : " + jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        BusSearchInterface busSearchInterface = data -> {
            try {
                ArrayList<SeatLayout> arrayListSeat = new ArrayList<>();
                String numberOfColmns=null;
                JSONObject seatJson = new JSONObject(data);
                JSONArray datas = seatJson.getJSONArray("data");
                String message = seatJson.getString("message");
                String status = seatJson.getString("status");
                if (status.equalsIgnoreCase("00")) {
                    JSONObject jsonArray = datas.getJSONObject(0);
                    JSONArray seatLayoutArray = jsonArray.getJSONArray("SeatLayout");
                    numberOfColmns = jsonArray.getString("NoOfColumn");
                    for (int i = 0; i < seatLayoutArray.length(); i++) {
                        SeatLayout seatLayout = new SeatLayout();
                        JSONObject seatObject = seatLayoutArray.getJSONObject(i);
                        seatLayout.setBookingStatus(seatObject.getString("BookingStatus"));
                        seatLayout.setDisplayName(seatObject.getString("DisplayName"));
                        arrayListSeat.add(seatLayout);
                    }
                    refreshLayoutInterface.refreshSeatLayout(arrayListSeat, numberOfColmns,message);
                }else {
                    refreshLayoutInterface.refreshSeatLayout(arrayListSeat, numberOfColmns,message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        };
        BusNetworking busNetworking = new BusNetworking(Constants.BUS_REFRESHTRIPLIST, jsonObject, true, busSearchInterface);
        busNetworking.execute();
    }

    public interface RefreshLayoutInterface {
        void refreshSeatLayout(ArrayList seatLayout, String numberOfColumns, String message);
    }

}
