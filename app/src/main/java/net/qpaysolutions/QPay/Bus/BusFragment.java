package net.qpaysolutions.QPay.Bus;

import android.app.Dialog;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import net.qpaysolutions.QPay.AmountPicker.DatePickerPopWin;
import net.qpaysolutions.QPay.Bus.Adapters.BusRouteAdapter;
import net.qpaysolutions.QPay.Bus.Models.TripInfoModel;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Flights.Adapter;
import net.qpaysolutions.QPay.Flights.SectorModel;
import net.qpaysolutions.QPay.Flights.SpineerSectorAdapter;
import net.qpaysolutions.QPay.Flights.Utility.FlightsSector;
import net.qpaysolutions.QPay.Flights.Utility.FlightsUtils;
import net.qpaysolutions.QPay.Flights.Utility.Utils;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class BusFragment extends Fragment implements View.OnClickListener, BusTicketInfo.PassTicketModelArray {


    private LinearLayout fromSpinner, toSpinner, airlineSpinner, classSpinner, arrivalLayout;
    private TextView setClassSpinnerData/*, setFromSpinnerData, setToSpinnerData*/;
    private Button dayButton, nightButton, bothButton, searchFlightButton;
    private LinearLayout departureDatePickerButton, arrivalDatePickerButton;
    private TextView adultDecrementTextView, adultIncrementTextView, childrenDecrementTextView, childrenIncrementTextView, adultNumberTextView, childrenNumberTextView;
    private TextView departureTextView, arrivalTextView, toTextView, fromTextView;
    private ImageView departureImage, arrivalImage, flightTripImageView, reverse;
    private View viewLayout;
    private List<String> classSpinnerList;
    private String selectedItem = "";
    private ColorStateList oneWayColorList;
    private ColorStateList roundWayColorList;
    private Utils.State state;
    private Utils.FlightTrip flightTrip;
    private TextView chat_text, fromLayout, toLayout, shortFormDistrictName, shortFormDistrictNameTo;
    private Calendar calendar;
    private android.app.DatePickerDialog departurePicker, arrivalPicker;
    private int yearDate, monthValue, day;
    private ArrayList<String> nationality = new ArrayList<>();
    private ArrayList<SectorModel> sector = new ArrayList<>();
    private String appId, id;
    private net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase QPayCustomerDatabase;
    private QPayProgressDialog qPayProgressDialog;
    private LinearLayout nationalityLayout;
    private String code_from, code_to, code_nationality, trip;
    private ArrayList<FlightsSector> arrayListNationality;
    private ArrayList<FlightsSector> arrayListSector;
    private String selectFromAddress, selectToAddress, nationalityFlight;
    private BusFragment busFragment;
    private BusRouteAdapter busRouteAdapter;
    private RotateAnimation rotateAnimation;
    private String shift = "Day";
    private ArrayList busArrayList = new ArrayList();

    @Override
    public void onViewCreated(@NonNull View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        qPayProgressDialog = new QPayProgressDialog(getContext());
        Utility.hideSoftkey(getActivity());
        dayButton = (Button) v.findViewById(R.id.btn_day);
        nightButton = (Button) v.findViewById(R.id.btn_night);
        bothButton = (Button) v.findViewById(R.id.btn_both);
        fromSpinner = (LinearLayout) v.findViewById(R.id.activity_main_from_spinner_id);
        toSpinner = (LinearLayout) v.findViewById(R.id.activity_main_to_spinner_id);
        departureTextView = (TextView) v.findViewById(R.id.departure_date);
        arrivalTextView = (TextView) v.findViewById(R.id.arrival_date);
        departureDatePickerButton = (LinearLayout) v.findViewById(R.id.activity_main_departure_date_picker_id);
        arrivalDatePickerButton = (LinearLayout) v.findViewById(R.id.activity_main_arrival_date_picker_id);
        airlineSpinner = (LinearLayout) v.findViewById(R.id.activity_main_airline_spinner_id);
        classSpinner = (LinearLayout) v.findViewById(R.id.activity_main_class_spinner_id);
        setClassSpinnerData = (TextView) v.findViewById(R.id.activity_main_set_class_spinner_data_id);
        adultDecrementTextView = (TextView) v.findViewById(R.id.activity_main_adults_decrement_btn_id);
        adultNumberTextView = (TextView) v.findViewById(R.id.no_of_adults);
        adultIncrementTextView = (TextView) v.findViewById(R.id.activity_main_adults_increment_btn_id);
        childrenDecrementTextView = (TextView) v.findViewById(R.id.activity_main_children_decrement_btn_id);
        childrenNumberTextView = (TextView) v.findViewById(R.id.no_of_childs);
        childrenIncrementTextView = (TextView) v.findViewById(R.id.activity_main_children_increment_btn_id);
        searchFlightButton = (Button) v.findViewById(R.id.search_flights);
        // flightTripImageView = (ImageView)  v.findViewById(R.id.activity_main_flight_trip_id);
        arrivalLayout = (LinearLayout) v.findViewById(R.id.activity_main_arrival_layout_id);
        viewLayout = v.findViewById(R.id.activity_main_arrival_layout_view_id);
        departureImage = (ImageView) v.findViewById(R.id.activity_main_departure_icon);
        arrivalImage = (ImageView) v.findViewById(R.id.activity_main_arrival_icon);
        reverse = v.findViewById(R.id.reverse);
        fromTextView = v.findViewById(R.id.tvFrom);
        toTextView = v.findViewById(R.id.tvTo);
        shortFormDistrictName = v.findViewById(R.id.shortFormDistrictName);
        shortFormDistrictNameTo = v.findViewById(R.id.shortFormDistrictNameTo);
        viewLayout.setVisibility(View.GONE);
        calendar = Calendar.getInstance();
        busRouteSearch();
        departureDatePicker();
        //  new FlightsSectorSearch(GeneralPref.getAppId(), GeneralPref.getCustId(), LatLngPref.getLat(), LatLngPref.getLng()).execute();
        fromLayout = v.findViewById(R.id.fromFlight);
        toLayout = v.findViewById(R.id.toFlight);
        rotateAnimation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(500);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        initializeListener();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bus_main, container, false);
    }


    private void busRouteSearch() {

        BusSearchInterface busRouteSearch = routes -> {
            qPayProgressDialog.dismiss();
            BusUtils.setBusRoutes(routes);
            try {
                JSONObject jsonObject = new JSONObject(routes);
                JSONArray data = jsonObject.getJSONArray("data");
                JSONObject routesData = data.getJSONObject(0);
                JSONArray rotesArray = routesData.getJSONArray("Routes");
                for (int i = 0; i < rotesArray.length(); i++)
                    busArrayList.add(rotesArray.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        qPayProgressDialog.show();
        new BusRouteSearch(busRouteSearch);
    }

    private void initializeListener() {

        adultDecrementTextView.setOnClickListener(this);
        adultIncrementTextView.setOnClickListener(this);
        childrenDecrementTextView.setOnClickListener(this);
        childrenIncrementTextView.setOnClickListener(this);
        departureDatePickerButton.setOnClickListener(this);
        searchFlightButton.setOnClickListener(this);
        classSpinner.setOnClickListener(this);
        shortFormDistrictName.setOnClickListener(this);
        shortFormDistrictNameTo.setOnClickListener(this);
        reverse.setOnClickListener(this);
        dayButton.setOnClickListener(this);
        nightButton.setOnClickListener(this);
        bothButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_one_way:
                oneWayTrip();
                break;
            case R.id.activity_main_adults_decrement_btn_id:
                adultDecrement();
                break;
            case R.id.activity_main_adults_increment_btn_id:
                adultIncrement();
                break;
            case R.id.activity_main_children_decrement_btn_id:
                childrenDecrement();
                break;
            case R.id.activity_main_children_increment_btn_id:
                childrenIncrement();
                break;
            case R.id.activity_main_departure_date_picker_id:
                departurePicker.show();
                break;
            case R.id.activity_main_arrival_date_picker_id:
                //  arrivalDatePicker();
                // arrivalPicker.show();
                break;
            case R.id.search_flights:
                callsearchFlights();
                break;
            case R.id.activity_main_airline_spinner_id:

                break;
            case R.id.activity_main_class_spinner_id:
                setClassSpinnerData();
                break;
            case R.id.activity_main_from_spinner_id:
//                setFromSpinnerData();
                break;
            case R.id.activity_main_to_spinner_id:
//                setToSpinnerData();
                break;

            case R.id.shortFormDistrictName:

                final Dialog dialog = new Dialog(getActivity(), WindowManager.LayoutParams.MATCH_PARENT);
                dialog.setContentView(R.layout.dialog_full_screen);
                RecyclerView recyclerView = dialog.findViewById(R.id.districtRecyclerView);
                SearchView searchView = dialog.findViewById(R.id.searchView);

                searchView.setIconifiedByDefault(true);
                searchView.setFocusable(true);
                searchView.setIconified(false);

                final EditText searchEditTextBranch = dialog.findViewById(R.id.search_src_text);
                searchEditTextBranch.setTextColor(getResources().getColor(R.color.white));
                searchEditTextBranch.setHintTextColor(getResources().getColor(android.R.color.white));


                final ImageView imvCloseBranch = dialog.findViewById(R.id.search_close_btn);
                imvCloseBranch.setImageResource(R.drawable.ic_action_cancel);

                ImageView searchButtonBranch = dialog.findViewById(R.id.search_button);
                searchButtonBranch.setImageResource(R.drawable.ic_search_black_24dp);
                try {
                    Field mDrawable = SearchView.class.getDeclaredField("mSearchHintIcon");
                    mDrawable.setAccessible(true);
                    Drawable drawable = (Drawable) mDrawable.get(searchView);
                    drawable.setBounds(0, 0, 0, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                BusRouteAdapter.OnBusRouteClickListener onBusRouteClickListener = new BusRouteAdapter.OnBusRouteClickListener() {
                    @Override
                    public void onBusRouteClickListener(String route) {
                        shortFormDistrictName.setText(route);
                        dialog.dismiss();
                        Utility.hideSoftkey(getActivity());
                    }
                };

                busRouteAdapter = new BusRouteAdapter(busArrayList, getContext(), onBusRouteClickListener);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(busRouteAdapter);
                dialog.show();
                ImageView backArrowImageView = dialog.findViewById(R.id.backArrowImageView);
                backArrowImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        busRouteAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        busRouteAdapter.getFilter().filter(newText);
                        return true;
                    }
                });
                break;

            case R.id.shortFormDistrictNameTo:

                final Dialog toDialog = new Dialog(getActivity(), WindowManager.LayoutParams.MATCH_PARENT);
                toDialog.setContentView(R.layout.dialog_full_screen);
                RecyclerView recyclerViewTo = toDialog.findViewById(R.id.districtRecyclerView);
                SearchView searchViewTo = toDialog.findViewById(R.id.searchView);
                searchViewTo.setIconifiedByDefault(true);
                searchViewTo.setFocusable(true);
                searchViewTo.setIconified(false);

                final EditText searchEditTextBranchFrom = searchViewTo.findViewById(R.id.search_src_text);
                searchEditTextBranchFrom.setTextColor(getResources().getColor(R.color.white));
                searchEditTextBranchFrom.setHintTextColor(getResources().getColor(android.R.color.white));

                final ImageView imvCloseBranchFrom = searchViewTo.findViewById(R.id
                        .search_close_btn);
                imvCloseBranchFrom.setImageResource(R.drawable.ic_action_cancel);

                ImageView searchButtonBranchfrom = searchViewTo.findViewById(R.id.search_button);
                searchButtonBranchfrom.setImageResource(R.drawable.ic_search_black_24dp);
                try {
                    Field mDrawable = SearchView.class.getDeclaredField("mSearchHintIcon");
                    mDrawable.setAccessible(true);
                    Drawable drawable = (Drawable) mDrawable.get(searchViewTo);
                    drawable.setBounds(0, 0, 0, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                BusRouteAdapter.OnBusRouteClickListener onBusRouteClickListener2 = new BusRouteAdapter.OnBusRouteClickListener() {
                    @Override
                    public void onBusRouteClickListener(String route) {
                        shortFormDistrictNameTo.setText(route);

                        Utility.hideSoftkey(getActivity());
                        toDialog.dismiss();
                    }
                };
                busRouteAdapter = new BusRouteAdapter(busArrayList, getContext(), onBusRouteClickListener2);

                RecyclerView.LayoutManager mLayoutManagerTo = new LinearLayoutManager(getActivity());
                recyclerViewTo.setLayoutManager(mLayoutManagerTo);
                recyclerViewTo.setItemAnimator(new DefaultItemAnimator());
                recyclerViewTo.setAdapter(busRouteAdapter);
                toDialog.show();

                ImageView backArrowImageViews = (ImageView) toDialog.findViewById(R.id.backArrowImageView);
                backArrowImageViews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        toDialog.dismiss();
                    }
                });

                searchViewTo.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        busRouteAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        busRouteAdapter.getFilter().filter(newText);
                        return true;
                    }
                });
                break;

            case R.id.reverse:
                try {
                    if (!TextUtils.isEmpty(shortFormDistrictName.getText().toString()) || !TextUtils.isEmpty(shortFormDistrictNameTo.getText())) {
                        reverse.startAnimation(rotateAnimation);
                        String temporaryName = fromLayout.getText().toString();
                        fromLayout.setText(toLayout.getText().toString());
                        toLayout.setText(temporaryName);
                        String temporaryCode;
                        temporaryCode = code_from;
                        code_from = code_to;
                        code_to = temporaryCode;
                        String tempShort;
                        tempShort = shortFormDistrictName.getText().toString();
//                        shortFormDistrictName.setText(shortFormDistrictNameTo.getText().toString()+" - "+toLayout.getText().toString());
//                        shortFormDistrictNameTo.setText(tempShort+" - "+fromLayout.getText().toString());
                        shortFormDistrictName.setText(shortFormDistrictNameTo.getText().toString());
                        shortFormDistrictNameTo.setText(tempShort);
                    }
                } catch (Exception e) {

                }
                break;

            case R.id.btn_day:
                nightButton.setBackgroundResource(R.drawable.flight_button_background);
                dayButton.setBackgroundResource(R.drawable.bus_button_background_selectes);
                bothButton.setBackgroundResource(R.drawable.flight_button_background);
                shift = "day";
                break;
            case R.id.btn_night:
                dayButton.setBackgroundResource(R.drawable.flight_button_background);
                nightButton.setBackgroundResource(R.drawable.bus_button_background_selectes);
                bothButton.setBackgroundResource(R.drawable.flight_button_background);
                shift = "night";
                break;
            case R.id.btn_both:
                bothButton.setBackgroundResource(R.drawable.bus_button_background_selectes);
                nightButton.setBackgroundResource(R.drawable.flight_button_background);
                dayButton.setBackgroundResource(R.drawable.flight_button_background);
                shift = "both";
                break;
        }
    }

    public void adultDecrement() {
        int number;
        try {
            number = Integer.parseInt(adultNumberTextView.getText().toString());
        } catch (NumberFormatException nfe) {
            number = 0;
        }
        if (number > 1) {
            number = number - 1;
        } else {
            number = 0;
        }
        adultNumberTextView.setText(String.valueOf(number));
    }

    public void adultIncrement() {
        int number;
        try {
            number = Integer.parseInt(adultNumberTextView.getText().toString());
        } catch (NumberFormatException nfe) {
            number = 1;
        }
        number = number + 1;
        if (number > 5) {
            Utils.customSnackBar(viewLayout, "Can not take more than 5 Seats");
        } else {
            adultNumberTextView.setText(String.valueOf(number));
        }
    }

    public void childrenIncrement() {

        int number;

        try {

            number = Integer.parseInt(childrenNumberTextView.getText().toString());

        } catch (NumberFormatException nfe) {
            number = 1;
        }
        number = number + 1;
        if (number > 5) {
            Utils.customSnackBar(viewLayout, "Can not take more than 5 Seats");
        } else {
            childrenNumberTextView.setText(String.valueOf(number));
        }
    }

    public void childrenDecrement() {

        int number;
        try {
            number = Integer.parseInt(childrenNumberTextView.getText().toString());
        } catch (NumberFormatException nfe) {
            number = 0;
        }
        if (number > 1) {
            number = number - 1;
        } else {
            number = 0;
        }
        childrenNumberTextView.setText(String.valueOf(number));

    }

    public void callsearchFlights() {

        if (departureTextView.getText().toString() == null

                || departureTextView.getText().toString().equals("Select Date")
                || departureTextView.getText().toString().isEmpty()
                || shortFormDistrictName.getText().length() < 1
                || shortFormDistrictNameTo.getText().length() < 1
                || adultNumberTextView.getText().toString().equals("0")
                || adultNumberTextView.getText().toString().isEmpty()
                || adultNumberTextView.getText().toString().length() < 1) {

            Utils.customSnackBar(viewLayout, "All fields are required");

        } else {

            qPayProgressDialog.show();
            new BusTicketInfo(shortFormDistrictName.getText().toString(),
                    shortFormDistrictNameTo.getText().toString(),
                    departureTextView.getText().toString(),
                    shift, qPayProgressDialog, this);

        }
    }


    public void oneWayTrip() {
        this.flightTrip = Utils.FlightTrip.ONE_WAY_TRIP;
        // this.oneWayButton.setBackgroundResource(R.drawable.pressed_button_border_bg);
        //this.oneWayButton.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        //flightTripImageView.setVisibility(View.VISIBLE);
//            flightTripImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_one_way_flight));
        viewLayout.setVisibility(View.VISIBLE);
        // arrivalLayout.setVisibility(View.VISIBLE);

//            roundTripButton.setBackgroundResource(R.drawable.unpressed_button_border_bg);
        // roundTripButton.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
        // roundTripButton.setTextColor(ContextCompat.getColor(getContext(), R.color.color_thirtary_text));
        viewLayout.setVisibility(View.GONE);
        arrivalLayout.setVisibility(View.GONE);

        trip = "1";

    }

  /*  public void roundTrip() {
        int oneTripButton = oneWayColorList.getDefaultColor();
        if (!(oneTripButton == R.color.lightBlue)) {
            this.flightTrip = Utils.FlightTrip.ROUND_WAY_TRIP;
            this.roundTripButton.setBackgroundResource(R.drawable.pressed_button_border_bg);
            this.roundTripButton.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
//            flightTripImageView.setVisibility(View.VISIBLE);
            //           flightTripImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_two_way_flight));
            viewLayout.setVisibility(View.VISIBLE);
            arrivalLayout.setVisibility(View.VISIBLE);

//            oneWayButton.setBackgroundResource(R.drawable.unpressed_button_border_bg);
            oneWayButton.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
            oneWayButton.setTextColor(ContextCompat.getColor(getContext(), R.color.color_thirtary_text));

        }
    }
*/

    public void departureDatePicker() {
        Calendar newCalendar = Calendar.getInstance();
        departurePicker = new android.app.DatePickerDialog(getContext(), R.style.DialogTheme, new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int value = month + 1;
                departureTextView.setText(year + "-" + value + "-" + dayOfMonth);
                day = dayOfMonth;
                yearDate = year;
                monthValue = month;
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        departurePicker.getDatePicker().setMinDate(newCalendar.getTime().getTime());
    }

    public void arrivalDatePicker() {
        Log.d("dinesh", "arrivalDatePicker: " + yearDate + " " + day + " " + monthValue);
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.set(Calendar.YEAR, yearDate);
        newCalendar.set(Calendar.DAY_OF_MONTH, day);
        newCalendar.set(Calendar.MONTH, monthValue);
        arrivalPicker = new android.app.DatePickerDialog(getContext(), R.style.DialogTheme, new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                int value = month + 1;
                arrivalTextView.setText(year + "-" + value + "-" + dayOfMonth);

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        arrivalPicker.getDatePicker().setMinDate(newCalendar.getTime().getTime());
    }

    public void setNationality() {

        Spinner spinner = (Spinner) getView().findViewById(R.id.nationalityFlight);
        final ArrayList<String> result = new ArrayList<>(nationality);
//        Collections.reverse(result);
        Adapter dapterFlight = new Adapter(getContext(), result);

        spinner.setAdapter(dapterFlight);
        spinner.setSelection(1);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String setNationality = nationality.get(position);
                nationalityFlight = setNationality;
                for (int i = 0; i < arrayListNationality.size(); i++) {
                    if (arrayListNationality.get(i).getNationality().equals(setNationality)) {
                        code_nationality = arrayListNationality.get(i).getNtlyCode();
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getNationality() {

        arrayListNationality = FlightsUtils.getNationality();

        for (int i = 0; i < arrayListNationality.size(); i++) {
            nationality.add(arrayListNationality.get(i).getNationality());

        }
    }

    public void getSector() {
        arrayListSector = FlightsUtils.getSector();
        for (int i = 0; i < arrayListSector.size(); i++) {
            SectorModel sectorModel = new SectorModel();
            sectorModel.setName(arrayListSector.get(i).getSector());
            sectorModel.setId(arrayListSector.get(i).getSecCode());
            sector.add(sectorModel);
//            sector.add(arrayListSector.get(i).getSector());
        }
    }

    public void setClassSpinnerData() {
        String[] classSpinnerValues = getResources().getStringArray(R.array.class_spinner_data);
        classSpinnerList = new ArrayList<>(Arrays.asList(classSpinnerValues));
        classSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classSpinnerList.size() > 0) {
                    final DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(getContext(), new DatePickerPopWin.OnDatePickedListener() {
                        @Override
                        public void onDatePickCompleted(int month, String dateDesc) {
                            selectedItem = dateDesc;
                            setClassSpinnerData.setText(dateDesc);
                        }
                    }).textConfirm("DONE")
                            .textCancel("CANCEL")
                            .btnTextSize(16)
                            .viewTextSize(20)
                            .setValues(classSpinnerList)
                            .build();
                    pickerPopWin.showPopWin(getActivity());
                } else {
                    getActivity().onBackPressed();
                }

            }
        });
    }

    public void setFromSpinnerData() {


        Spinner spinner = (Spinner) getView().findViewById(R.id.fromFlight);


        SpineerSectorAdapter dapterFlight = new SpineerSectorAdapter(getContext(), sector);
        spinner.setAdapter(dapterFlight);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectFromAddress = sector.get(position).getName();
                code_from = sector.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void setToSpinnerData() {

        Spinner spinner = (Spinner) getView().findViewById(R.id.toFlight);


        SpineerSectorAdapter dapterFlight = new SpineerSectorAdapter(getContext(), sector);

        spinner.setAdapter(dapterFlight);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectToAddress = sector.get(position).getName();
                code_to = sector.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void passTicketModelArray(ArrayList<TripInfoModel> tripsModelArray) {

        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putSerializable("arrayList", tripsModelArray);
        SelectTicketFragment selectTicketFragment = new SelectTicketFragment();
        selectTicketFragment.setArguments(bundle);
        fragmentManager.replace(R.id.main_container, selectTicketFragment, "selectTicket").addToBackStack("selectTicket");
        fragmentManager.commit();

    }

//
//    public class FlightsSectorSearch extends AsyncTask<String, String, String> {
//        String appId, id, lat, lng;
//
//
//        public FlightsSectorSearch(String appId, String id, String lat, String lng) {
//            this.appId = appId;
//            this.id = id;
//            this.lat = lat;
//            this.lng = lng;
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("appId", appId);
//                jsonObject.put("id", id);
//                jsonObject.put("lat", lat);
//                jsonObject.put("lng", lng);
//                Log.d("dinesh", "doInBackground: " + jsonObject);
//                return new NetworkAPI().sendHTTPData(Constants.NATIONALITY_SECTOR, jsonObject);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            qPayProgressDialog.dismiss();
//            try {
//                Log.d("dinesh", "doInBackground: " + s);
//                JSONObject jsonObject = new JSONObject(s);
//                if (jsonObject.getString("status").equals("00")) {
//                    JSONArray data = jsonObject.getJSONArray("data");
//                    JSONObject object = data.getJSONObject(0);
//                    JSONArray Nationality = object.getJSONArray("Nationality");
//                    JSONArray Sector = object.getJSONArray("Sector");
//                    FlightsUtils.setNationality(Nationality.toString());
//                    FlightsUtils.setSector(Sector.toString());
//                    getNationality();
//                    getSector();
//                    setNationality();
//                    //setFromSpinnerData();
//                    //setToSpinnerData();
//
//                } else {
//                    Toast.makeText(getContext(), "Network Issue!", Toast.LENGTH_LONG).show();
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                try {
//                    Toast.makeText(getContext(), "Network Issue!", Toast.LENGTH_LONG).show();
//                } catch (Exception e2) {
//
//                }
//            }
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            qPayProgressDialog = new QPayProgressDialog(getActivity());
//            qPayProgressDialog.show();
//
//        }
//    }
}
