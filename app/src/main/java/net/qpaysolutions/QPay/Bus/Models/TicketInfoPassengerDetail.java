package net.qpaysolutions.QPay.Bus.Models;

import android.annotation.SuppressLint;

import com.google.android.material.internal.ParcelableSparseArray;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressLint("RestrictedApi")
public class TicketInfoPassengerDetail extends ParcelableSparseArray implements Serializable  {

    @SerializedName("Detail")
    String detail;


    @SerializedName("Id")
    String id;

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public String getId() {
        return id;
    }
}
