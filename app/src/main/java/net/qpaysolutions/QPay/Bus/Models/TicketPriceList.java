package net.qpaysolutions.QPay.Bus.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TicketPriceList implements Serializable {

    @SerializedName("Id")
    private String id;

    @SerializedName("PassengerType")
    private String passengerType;

    @SerializedName("PriceInRs")
    private String priceInRs;

    @SerializedName("PriceInDollar")
    private String priceInDollar;

    @SerializedName("TicketSrlNo")
    private String ticketSerialNumber;

    @SerializedName("TimeOut")
    private String timeOut;

    public String getId() {
        return id;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public String getPriceInRs() {
        return priceInRs;
    }

    public String getPriceInDollar() {
        return priceInDollar;
    }

    public String getTicketSerialNumber() {
        return ticketSerialNumber;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public void setPriceInRs(String priceInRs) {
        this.priceInRs = priceInRs;
    }

    public void setPriceInDollar(String priceInDollar) {
        this.priceInDollar = priceInDollar;
    }

    public void setTicketSerialNumber(String ticketSerialNumber) {
        this.ticketSerialNumber = ticketSerialNumber;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }
}
