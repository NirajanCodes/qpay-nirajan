package net.qpaysolutions.QPay.Bus;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import net.qpaysolutions.QPay.Bus.Models.BookedTicketModel;
import net.qpaysolutions.QPay.Bus.Models.InsertedDataModel;
import net.qpaysolutions.QPay.Bus.Models.ObjectModel;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

public class UpdateInfoAdapter extends RecyclerView.Adapter<UpdateInfoAdapter.InfoViewHolder> {
    private ArrayList<String> seats;
    private BookedTicketModel bookedTicketModel;
    private Context mContext;
    private ArrayList<InsertedDataModel> insertedDataModelArrayList;
    GetInsertedInformation getInsertedInformation;
    ArrayList<ObjectModel> collection = new ArrayList();

    UpdateInfoAdapter(ArrayList<String> seats, BookedTicketModel bookedTicketModel, GetInsertedInformation getInsertedInformation) {

        this.seats = seats;
        this.bookedTicketModel = bookedTicketModel;
        insertedDataModelArrayList = new ArrayList<>();
        this.getInsertedInformation = getInsertedInformation;

    }

    @NonNull
    @Override
    public InfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new InfoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_adapter_passengerinfo, parent, false));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull InfoViewHolder holder, int position) {
        ObjectModel objectModel = new ObjectModel();
        objectModel.setName(holder.name);
        objectModel.setAge(holder.age);
        objectModel.setMale(holder.male);
        objectModel.setFemale(holder.female);
        objectModel.setNationallity(holder.nationality);
        collection.add(objectModel);
        getInsertedInformation.insertedInformation(collection);
    }

    @Override
    public int getItemCount() {
        return seats.size();
    }

    public class InfoViewHolder extends RecyclerView.ViewHolder {

        TextView seatNumber;
        EditText name, age;
        RadioButton male,female;
        Spinner nationality;
        LinearLayout lname, lage, lgender, lnationality;

        public InfoViewHolder(@NonNull View itemView) {
            super(itemView);
            seatNumber = itemView.findViewById(R.id.seat_number);
            name = itemView.findViewById(R.id.name);
            age = itemView.findViewById(R.id.age);
            male = itemView.findViewById(R.id.male);
            female = itemView.findViewById(R.id.female);
            nationality = itemView.findViewById(R.id.nationality_spinner);
            lname = itemView.findViewById(R.id.lname);
            lage = itemView.findViewById(R.id.lage);
            lgender = itemView.findViewById(R.id.lgender);
            lgender = itemView.findViewById(R.id.lnationality);
            String[] spinnerArray = {"Nepali", "India", "Others"};
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                    (mContext, android.R.layout.simple_spinner_item,
                            spinnerArray); //selected item will look like a spinner set from XML
            nationality.setAdapter(spinnerArrayAdapter);
            if (!bookedTicketModel.isPassengerTypeDetailRequired()) {
                lage.setVisibility(View.GONE);
                lgender.setVisibility(View.GONE);
            }
        }
    }

    public interface GetInsertedInformation {
        void insertedInformation(ArrayList collection);
    }
}
