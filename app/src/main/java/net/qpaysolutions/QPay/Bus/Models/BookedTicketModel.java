package net.qpaysolutions.QPay.Bus.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BookedTicketModel implements Serializable {

    @SerializedName("BoardingPoints")
    ArrayList<String> boardingPoints;

    @SerializedName("MultiPrice")
    boolean multiprice;

    @SerializedName("Name")
    boolean name;

    @SerializedName("NameRequired")
    boolean nameRequired;

    @SerializedName("PassengerTypeDetail")
    ArrayList<PassengerTypeDetail> passengerTypeDetails;

    @SerializedName("PassengerTypeDetailRequired")
    boolean passengerTypeDetailRequired;

    @SerializedName("TicketPriceList")
    ArrayList<TicketPriceList> ticketPriceLists;

    @SerializedName("TicketSrlNo")
    String ticketSrlNo;

    @SerializedName("TimeOut")
    String timeOut;

    @SerializedName("message")
    String message;

    @SerializedName("status")
    String status;

    @SerializedName("success")
    boolean success;

    public ArrayList<String> getBoardingPoints() {
        return boardingPoints;
    }

    public boolean isMultiprice() {
        return multiprice;
    }

    public boolean isName() {
        return name;
    }

    public boolean isNameRequired() {
        return nameRequired;
    }

    public ArrayList<PassengerTypeDetail> getPassengerTypeDetails() {
        return passengerTypeDetails;
    }

    public boolean isPassengerTypeDetailRequired() {
        return passengerTypeDetailRequired;
    }

    public ArrayList<TicketPriceList> getTicketPriceLists() {
        return ticketPriceLists;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getTicketSrlNo() {
        return ticketSrlNo;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setBoardingPoints(ArrayList<String> boardingPoints) {
        this.boardingPoints = boardingPoints;
    }

    public void setMultiprice(boolean multiprice) {
        this.multiprice = multiprice;
    }

    public void setName(boolean name) {
        this.name = name;
    }

    public void setNameRequired(boolean nameRequired) {
        this.nameRequired = nameRequired;
    }

    public void setPassengerTypeDetails(ArrayList<PassengerTypeDetail> passengerTypeDetails) {
        this.passengerTypeDetails = passengerTypeDetails;
    }

    public void setPassengerTypeDetailRequired(boolean passengerTypeDetailRequired) {
        this.passengerTypeDetailRequired = passengerTypeDetailRequired;
    }

    public void setTicketPriceLists(ArrayList<TicketPriceList> ticketPriceLists) {
        this.ticketPriceLists = ticketPriceLists;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setTicketSrlNo(String ticketSrlNo) {
        this.ticketSrlNo = ticketSrlNo;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }
}
