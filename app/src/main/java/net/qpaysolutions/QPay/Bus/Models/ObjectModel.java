package net.qpaysolutions.QPay.Bus.Models;

import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class ObjectModel {
    private TextView name;
    private TextView age;
    private RadioButton male;
    private RadioButton female;

    public void setMale(RadioButton male) {
        this.male = male;
    }

    public void setFemale(RadioButton female) {
        this.female = female;
    }

    public RadioButton getMale() {
        return male;
    }

    public RadioButton getFemale() {
        return female;
    }

    private Spinner nationallity;

    public void setName(TextView name) {
        this.name = name;
    }

    public void setAge(TextView age) {
        this.age = age;
    }



    public void setNationallity(Spinner nationallity) {
        this.nationallity = nationallity;
    }

    public TextView getName() {
        return name;
    }

    public TextView getAge() {
        return age;
    }


    public Spinner getNationallity() {
        return nationallity;
    }
}
