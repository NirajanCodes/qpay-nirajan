package net.qpaysolutions.QPay.Bus.SeatSelection;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.qpaysolutions.QPay.Bus.BookTicket;
import net.qpaysolutions.QPay.Bus.CancelBooking;
import net.qpaysolutions.QPay.Bus.Models.BookedTicketModel;
import net.qpaysolutions.QPay.Bus.Models.SeatLayout;
import net.qpaysolutions.QPay.Bus.Models.TripInfoModel;
import net.qpaysolutions.QPay.Bus.PickUpInformationFragment;
import net.qpaysolutions.QPay.Bus.RefreshLayout;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.List;

public class SeatSelectionFragment extends Fragment implements BusSeatAdapter.OnSeatSelected, View.OnClickListener {

    private TextView txtSeatSelected;
    private Button book;
    ArrayList<String> bookedSeats = new ArrayList<>();
    ArrayList<SeatLayout> seatLayouts;
    BusSeatAdapter adapter;
    Dialog dialog;
    BookedTicketModel bookedTicketModel;
    TripInfoModel tripInfoModel;
    CancelBooking.RefreshSeatLayout refreshSeatLayout;
    RefreshLayout.RefreshLayoutInterface refreshLayoutInterface;
    int COLUMNS;
    GridLayoutManager manager;
    List<AbstractItem> items;
    BookTicket.OnModelSet onModelSet;
    QPayProgressDialog qPayProgressDialog;
    String boardingPoint;
    View view2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bus_seatselection, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        bookedSeats.clear();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        seatLayouts = new ArrayList<SeatLayout>();
        tripInfoModel = (TripInfoModel) getArguments().getSerializable("tripsInfoModel");
        qPayProgressDialog = new QPayProgressDialog(getContext());
        COLUMNS = tripInfoModel.getNoOfColumn();
        seatLayouts = tripInfoModel.getSeatModel();
        txtSeatSelected = view.findViewById(R.id.txt_seat_selected);
        Button refresh = view.findViewById(R.id.refresh);
        refresh.setOnClickListener(this);
        book = view.findViewById(R.id.btn_book);
        onModelSet = (bookedTicketModel, message) -> {
            qPayProgressDialog.dismiss();
            if (bookedTicketModel != null) {
                getActivity().runOnUiThread(() -> {
                    this.bookedTicketModel = bookedTicketModel;
                    dialog = new Dialog(getContext());
                    dialog.setTitle("Ticket Information");
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_ticket_information);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    Button cancel = dialog.findViewById(R.id.btn_cancel);
                    cancel.setOnClickListener(this);
                    Button proceed = dialog.findViewById(R.id.btn_proceed);
                    proceed.setOnClickListener(this);
                    TextView serialNumber = dialog.findViewById(R.id.ticket_serial_number);
                    serialNumber.setText(bookedTicketModel.getTicketSrlNo());
                    ListView listView = dialog.findViewById(R.id.boarding_locations);
                    ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, bookedTicketModel.getBoardingPoints());

                    listView.setOnItemClickListener((adapterView, view1, i, l) -> {
                        boardingPoint = adapterView.getItemAtPosition(i).toString();
                        Log.d("nirajan", "boarding Point : " + boardingPoint);
                        if (view2 != null) {
                            view2.setBackgroundColor(getResources().getColor(R.color.white));
                        }
                        view2 = view1;
                        view1.setBackgroundColor(getResources().getColor(R.color.background_color_all));
                    });
                    listView.setAdapter(itemsAdapter);
                    // textView.setText(bookedTicketModel.getTicketPriceLists().get(0).getPriceInRs());
                    dialog.show();
                });
            } else {
                getActivity().runOnUiThread(() ->
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show()
                );
            }
        };

        //For cancel queue
        refreshSeatLayout = (seatLayout, numberOfColumns, message) ->
                getActivity().runOnUiThread(() -> {
                    qPayProgressDialog.dismiss();
                    if (seatLayout != null && numberOfColumns != null) {
                        bookedSeats.clear();
                        txtSeatSelected.setText(bookedSeats.toString()
                                .replace("[", "")
                                .replace("]", ""));
                        seatLayouts = seatLayout;
                        COLUMNS = Integer.parseInt(numberOfColumns);
                        items = items(seatLayouts);
                        adapter.notifyDataChanges(seatLayouts, items);

                    } else {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });

        refreshLayoutInterface = (seatLayout, numberOfColumns, message) ->

                getActivity().runOnUiThread(() -> {
                    qPayProgressDialog.dismiss();
                    if (seatLayout != null && numberOfColumns != null) {
                        bookedSeats.clear();
                        txtSeatSelected.setText(bookedSeats.toString()
                                .replace("[", "")
                                .replace("]", ""));
                        seatLayouts = seatLayout;
                        COLUMNS = Integer.parseInt(numberOfColumns);
                        items = items(seatLayouts);
                        adapter.notifyDataChanges(seatLayouts, items);

                    } else {
                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                });

        book.setOnClickListener(this);
        items = items(seatLayouts);
        manager = new GridLayoutManager(getContext(), COLUMNS);
        RecyclerView recyclerView = view.findViewById(R.id.lst_items);
        recyclerView.setLayoutManager(manager);
        adapter = new BusSeatAdapter(getContext(), items, seatLayouts, this);
        recyclerView.setAdapter(adapter);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.refresh:
                qPayProgressDialog.show();
                new RefreshLayout(tripInfoModel.getId(), refreshLayoutInterface);
                break;
            case R.id.btn_cancel:
                qPayProgressDialog.show();
                dialog.dismiss();
                new CancelBooking(bookedTicketModel.getTicketSrlNo(), tripInfoModel.getId(), refreshSeatLayout);
                break;
            case R.id.btn_proceed:
                if (boardingPoint != null && !boardingPoint.isEmpty()) {
                    dialog.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("PassengerDetail", tripInfoModel.getTicketInfoPassengerDetail());
                    bundle.putSerializable("bookedTicketModel", bookedTicketModel);
                    bundle.putString("ticketId", tripInfoModel.getId());
                    bundle.putSerializable("seats", bookedSeats);
                    bundle.putString("boardingPoint", boardingPoint);
                    bundle.putString("price", tripInfoModel.getTicketPrice());

                    PickUpInformationFragment pickUpInformationFragment = new PickUpInformationFragment();
                    pickUpInformationFragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.main, pickUpInformationFragment);
                    fragmentTransaction.addToBackStack("seatSelection");
                    fragmentTransaction.commit();
                } else {
                    Toast.makeText(getContext(), " Please Select Boarding Point ", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_book:
                if (!(bookedSeats.size() < 1)) {
                    qPayProgressDialog.show();
                    new BookTicket(tripInfoModel.getId(), bookedSeats, getContext(), onModelSet);
                } else {
                    Toast.makeText(getContext(), "Please Select a seat ", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    List<AbstractItem> items(ArrayList<SeatLayout> seatLayouts) {
        List<AbstractItem> items = new ArrayList<>();
        for (int i = 0; i < seatLayouts.size(); i++) {
            if (seatLayouts.get(i).getBookingStatus().equalsIgnoreCase("No")) {
                items.add(new CenterItem(String.valueOf(i)));
            } else if (seatLayouts.get(i).getBookingStatus().equalsIgnoreCase("Yes")) {
                items.add(new EdgeItem(String.valueOf(i)));
            } else if (seatLayouts.get(i).getBookingStatus().equalsIgnoreCase("na") && seatLayouts.get(i).getDisplayName().equalsIgnoreCase("na")) {
                items.add(new EmptyItem(String.valueOf(i)));
            }
        }
        return items;
    }

    @Override
    public void onSeatSelected(String seatNumber) {
        if (!bookedSeats.contains(seatNumber)) {
            bookedSeats.add(seatNumber);
            txtSeatSelected.setText(bookedSeats.toString()
                    .replace("[", "")
                    .replace("]", ""));
            Log.d("nirajan", " selected SeatNumber : " + seatNumber);
        } else {
            bookedSeats.remove(seatNumber);
            txtSeatSelected.setText(bookedSeats.toString()
                    .replace("[", "")
                    .replace("]", ""));
            Log.d("nirajan", " selected SeatNumber : " + seatNumber);
            Log.d("nirajan", " booked seats : " + bookedSeats);
        }
    }
}
