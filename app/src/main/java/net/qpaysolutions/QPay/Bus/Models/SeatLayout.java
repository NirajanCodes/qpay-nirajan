package net.qpaysolutions.QPay.Bus.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SeatLayout implements Serializable {

    @SerializedName("BookingStatus")
    private String bookingName;

    @SerializedName("DisplayName")
    private String displayName;

    public void setBookingStatus(String bookingName) {
        this.bookingName = bookingName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getBookingStatus() {
        return bookingName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
