package net.qpaysolutions.QPay.Bus;

import net.qpaysolutions.QPay.Utils.Constants;

import org.json.JSONObject;

public class UpdatePassengerInfoApi {
    public UpdatePassengerInfoApi(JSONObject json, UpadatePassengerInterface upadatePassengerInterface) {

        BusSearchInterface busSearchInterface = new BusSearchInterface() {
            @Override
            public void onSuccessData(String data) {
                upadatePassengerInterface.updateInterface(data);
            }
        };
        new BusNetworking(Constants.BUS_UPDATEPASSENGERINFO, json, true, busSearchInterface).execute();
    }

    public interface UpadatePassengerInterface {
        void updateInterface(String data);
    }
}
