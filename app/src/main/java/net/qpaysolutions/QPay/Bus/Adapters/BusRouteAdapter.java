package net.qpaysolutions.QPay.Bus.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

public class BusRouteAdapter extends RecyclerView.Adapter<BusRouteAdapter.BusCustomView> implements Filterable {

    private Context context;
    private ArrayList<String> arrayList;
    private ArrayList filteredList;
    private OnBusRouteClickListener busRouteClickListener;

    public BusRouteAdapter(ArrayList arrayList, Context context, OnBusRouteClickListener busRouteClickListener) {
        this.context = context;
        this.arrayList = arrayList;
        filteredList = arrayList;
        this.busRouteClickListener = busRouteClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                ArrayList<String> filterableList = new ArrayList<>();
                if (charString.isEmpty()) {
                    filteredList = arrayList;
                } else {
                    for (String row : arrayList) {
                        if (row.toLowerCase().contains(charString.toLowerCase()))
                            filterableList.add(row);
                    }
                    filteredList = filterableList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterableList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public BusCustomView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BusCustomView(LayoutInflater.from(context).inflate(R.layout.item_district, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BusCustomView holder, int position) {
        try {
            holder.districtName.setText(filteredList.get(position).toString());
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    class BusCustomView extends RecyclerView.ViewHolder {
        private TextView districtName;

        public BusCustomView(@NonNull View itemView) {
            super(itemView);
            districtName = itemView.findViewById(R.id.districtName);
            districtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    busRouteClickListener.onBusRouteClickListener(filteredList.get(getAdapterPosition()).toString());
                }
            });
        }
    }

    public interface OnBusRouteClickListener {
        void onBusRouteClickListener(String route);
    }
}
