package net.qpaysolutions.QPay.Bus;

import android.content.Context;
import android.content.SharedPreferences;

import net.qpaysolutions.QPay.Utils.CustomerApplication;

public class BusUtils {

    public static String ROUTES = "routes";

    public static String BUS = "bus";

    public static void setBusRoutes(String s) {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(BUS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ROUTES, s);
        editor.apply();
    }
    public static String getRoutes() {
        SharedPreferences sharedPreferences = CustomerApplication.getContext().getSharedPreferences(BUS, Context.MODE_PRIVATE);
        return sharedPreferences.getString(ROUTES, "");
    }

}
