package net.qpaysolutions.QPay.Bus;

import android.os.AsyncTask;
import android.util.Log;

import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONObject;

public class BusNetworking extends AsyncTask<Void, Void, String> {

    private JSONObject jsonObject;
    private String url;
    private boolean post;
    private BusSearchInterface busSearchInterface;

    public BusNetworking(String url, JSONObject jsonObject, boolean post, BusSearchInterface busSearchInterface) {

        this.jsonObject = jsonObject;
        this.url = url;
        this.post = post;
        this.busSearchInterface = busSearchInterface;

    }

    @Override
    protected String doInBackground(Void... voids) {
        String response = null;
        try {
            if (post) {
                response = NetworkAPI.sendHTTPData(url, jsonObject);
                Log.d("nirajan", "response : " + response);
            } else {
                response = NetworkAPI.getFromurl(url);
                Log.d("nirajan", "response : " + response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        busSearchInterface.onSuccessData(response);
        return response;
    }
}
