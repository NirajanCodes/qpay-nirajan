package net.qpaysolutions.QPay.Bus;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.qpaysolutions.QPay.Bus.Models.TripInfoModel;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BusTicketInfo {


    public BusTicketInfo(String from, String to, String date, String shift, final QPayProgressDialog qPayProgressDialog, final PassTicketModelArray passTicketModelArray) {
       BusSearchInterface busSearchInterface = data -> {
           qPayProgressDialog.dismiss();
           try {

               ArrayList<TripInfoModel> tripInfoModelsArray = new ArrayList<>();
               JSONObject jsonObject = new JSONObject(data);
               JSONArray dataArray = jsonObject.getJSONArray("data");
               JSONObject dataValue = dataArray.getJSONObject(0);
               JSONArray trips = dataValue.getJSONArray("Trips");

               for (int i = 0; i < trips.length(); i++) {

                   JSONObject tripsfirst = trips.getJSONObject(i);
                   Log.d("nirajan", " tripsfirst  : " + tripsfirst.toString());
                   TripInfoModel tripInfoModel;
                   GsonBuilder builder = new GsonBuilder();
                   Gson gson = builder.create();

                   try {

                       tripInfoModel = gson.fromJson(tripsfirst.toString(), TripInfoModel.class);
                       tripInfoModelsArray.add(tripInfoModel);
                       Log.d("nirajan", "tripsmodel  : " + tripInfoModel);
                       Log.d("nirajan", "date value  : " + tripInfoModel.getDate());

                   } catch (Exception e) {

                       e.printStackTrace();
                       Log.d("nirajan", "exception  : " + e.toString());

                   }
               }

               passTicketModelArray.passTicketModelArray(tripInfoModelsArray);
               Log.d("nirajan", "tripsarray  : " + tripInfoModelsArray);

           } catch (JSONException e) {
               e.printStackTrace();
           }
       };

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appId", GeneralPref.getAppId());
            jsonObject.put("id",GeneralPref.getCustId());
            jsonObject.put("lat", LatLngPref.getLat());
            jsonObject.put("lng", LatLngPref.getLng());
            jsonObject.put("from", from);
            jsonObject.put("to", to);
            jsonObject.put("date", date);
            jsonObject.put("shift", shift);
            BusNetworking busNetworking = new BusNetworking(Constants.BUS_GETTRIPINFO, jsonObject, true, busSearchInterface);
            busNetworking.execute();

        } catch (JSONException ignored) {

        }
    }

    public interface PassTicketModelArray {
        void passTicketModelArray(ArrayList<TripInfoModel> tripsModelArray);
    }
}
