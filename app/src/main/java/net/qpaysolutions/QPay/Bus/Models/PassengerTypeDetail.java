package net.qpaysolutions.QPay.Bus.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PassengerTypeDetail implements Serializable {

    @SerializedName("detail")
    String detail;

    @SerializedName("id")
    String id;

    public String getDetail() {
        return detail;
    }

    public String getId() {
        return id;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setId(String id) {
        this.id = id;
    }
}
