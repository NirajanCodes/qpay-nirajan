package net.qpaysolutions.QPay.Bus.SeatSelection;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import net.qpaysolutions.QPay.Bus.Models.SeatLayout;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.List;

public class BusSeatAdapter extends SelectableAdapter<RecyclerView.ViewHolder> {

    private OnSeatSelected mOnSeatSelected;

    static Context mContext;
    static String color;

    private static class EdgeViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgSeatOccupied;
        private TextView seatNumber;

        public EdgeViewHolder(View itemView) {
            super(itemView);

            imgSeatOccupied = itemView.findViewById(R.id.img_seat_occupied);
            seatNumber = itemView.findViewById(R.id.seat_number);
        }
    }

    private static class CenterViewHolder extends RecyclerView.ViewHolder {

        ImageView imgSeat;
        private final ImageView imgSeatSelected;
        private TextView seatNumber;

        public CenterViewHolder(View itemView) {
            super(itemView);
            imgSeat = (ImageView) itemView.findViewById(R.id.img_seat);
            imgSeat.setColorFilter(mContext.getResources().getColor(R.color.blueGray500));
            color = imgSeat.getColorFilter().toString();
            imgSeatSelected = (ImageView) itemView.findViewById(R.id.img_seat_selected);
            seatNumber = itemView.findViewById(R.id.seat_number);
        }
    }

    private static class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

    }

    private LayoutInflater mLayoutInflater;
    private List<AbstractItem> mItems;
    private ArrayList<SeatLayout> seatLayouts;

    public BusSeatAdapter(Context context, List<AbstractItem> items, ArrayList<SeatLayout> seatLayouts, OnSeatSelected onSeatSelected) {
        mOnSeatSelected = onSeatSelected;
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mItems = items;
        this.seatLayouts = seatLayouts;
    }

    @Override
    public int getItemCount() {
        return seatLayouts.size();
    }

    public void notifyDataChanges(ArrayList<SeatLayout> seatLayouts, List<AbstractItem> mItems) {
        this.seatLayouts = seatLayouts;
        this.mItems = mItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == AbstractItem.TYPE_CENTER) {
            View itemView = mLayoutInflater.inflate(R.layout.recylerview_bus_seatselection, parent, false);
            return new CenterViewHolder(itemView);
        } else if (viewType == AbstractItem.TYPE_EDGE) {
            View itemView = mLayoutInflater.inflate(R.layout.recylerview_bus_selected, parent, false);
            return new EdgeViewHolder(itemView);
        } else {
            View itemView = new View(mContext);
            return new EmptyViewHolder(itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;    
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        int type = mItems.get(position).getType();
        Log.d("nirajan", "type " + type);
        if (type == AbstractItem.TYPE_CENTER) {
            final CenterItem item = (CenterItem) mItems.get(position);
            final CenterViewHolder holder = (CenterViewHolder) viewHolder;
            holder.imgSeat.setOnClickListener(v -> {
                Log.d("nirajan", "position " + position);
                mOnSeatSelected.onSeatSelected(holder.seatNumber.getText().toString());
                Log.d("nirajan", "filter  " + holder.imgSeat.getColorFilter().toString());
                if (holder.imgSeat.getColorFilter().toString().equalsIgnoreCase(color)) {
                    holder.imgSeat.setColorFilter(Color.GREEN);
                    holder.seatNumber.setTextColor(Color.GREEN);
                } else {
                    holder.imgSeat.setColorFilter(mContext.getResources().getColor(R.color.blueGray500));
                    holder.seatNumber.setTextColor(mContext.getResources().getColor(R.color.blueGray500));
                }
            });
            holder.seatNumber.setText(seatLayouts.get(position).getDisplayName());
            Log.d("nirajan", "position without touch " + position);
            Log.d("nirajan", "seat " + seatLayouts.get(position).getDisplayName().equalsIgnoreCase(holder.seatNumber.getText().toString()));
        } else if (type == AbstractItem.TYPE_EDGE) {
            final EdgeItem item = (EdgeItem) mItems.get(position);
            final EdgeViewHolder holder = (EdgeViewHolder) viewHolder;
            holder.seatNumber.setText(seatLayouts.get(position).getDisplayName());
        }
    }

    public interface OnSeatSelected {
        void onSeatSelected(String seatNumber);
    }
}