package net.qpaysolutions.QPay.Bus.SeatSelection;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.NetworkAPI;

import org.json.JSONException;
import org.json.JSONObject;

public class DownloadPdf extends AsyncTask<Void, Void, Void> {
    String stan, crrn, appId, id;
    Context context;

    public DownloadPdf(String stan, String crrn, String appId, String id, Context context) {
        this.appId = appId;
        this.id = id;
        this.stan = stan;
        this.crrn = crrn;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", appId);
            jsonObject.put("AppId", id);
            jsonObject.put("Stan", stan);
            jsonObject.put("Crrn", crrn);
            Log.d("nirajan"," stan " +jsonObject);
            try {
                Looper.prepare();
                String message = NetworkAPI.downloadFileFromeUrlBase64(Constants.BUS_TICKET_PDF, jsonObject, context, "bus");
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
