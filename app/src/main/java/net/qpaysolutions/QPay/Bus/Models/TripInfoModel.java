package net.qpaysolutions.QPay.Bus.Models;

import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TripInfoModel implements Serializable {

    @SerializedName("Amenities")
    ArrayList<String> Amenities;

    @SerializedName("BusNo")
    String busNo;

    @SerializedName("BusType")
    String busType;

    @SerializedName("Date")
    String date;

    @SerializedName("DateEn")
    String dateEn;

    @SerializedName("DepartureTime")
    String departureTime;

    @SerializedName("DetailImage")
    List detailImage;

    @SerializedName("FaceImage")
    String faceImage;

    @SerializedName("Id")
    String id;

    @SerializedName("ImgList")
    List<String> imageList ;

    @SerializedName("LockStatus")
    String lockStatus;

    @SerializedName("MultiPrice")
    String multiPrice;

    @SerializedName("NoOfColumn")
    int noOfColumn;

    @SerializedName("Operator")
    String operator;

    @SerializedName("OperatorName")
    String operatorName;

    @SerializedName("PassengerDetail")
    ArrayList<TicketInfoPassengerDetail> ticketInfoPassengerDetail;

    @SerializedName("Rating")
    String rating;

    @SerializedName("SeatLayout")
    ArrayList<SeatLayout> seatModel;

    @SerializedName("Shift")
    String shift;

    @SerializedName("TicketPrice")
    String ticketPrice;

    @SerializedName("TicketPriceList")
    List ticketPriceList = new ArrayList();

    public void setAmenities(ArrayList<String> amenities) {
        Amenities = amenities;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDateEn(String dateEn) {
        this.dateEn = dateEn;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public void setDetailImage(List detailImage) {
        this.detailImage = detailImage;
    }

    public void setFaceImage(String faceImage) {
        this.faceImage = faceImage;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImageList(List<String> imageList)     {
        this.imageList = imageList;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

    public void setMultiPrice(String multiPrice) {
        this.multiPrice = multiPrice;
    }

    public void setNoOfColumn(int noOfColumn) {
        this.noOfColumn = noOfColumn;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }


    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public void setTicketPrice(String ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public void setTicketPriceList(List ticketPriceList) {
        this.ticketPriceList = ticketPriceList;
    }

    public ArrayList<String> getAmenities() {
        return Amenities;
    }

    public String getBusNo() {
        return busNo;
    }

    public String getBusType() {
        return busType;
    }

    public String getDate() {
        return date;
    }

    public String getDateEn() {
        return dateEn;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public List getDetailImage() {
        return detailImage;
    }

    public String getFaceImage() {
        return faceImage;
    }

    public String getId() {
        return id;
    }

    public List getImageList() {
        return imageList;
    }

    public String getLockStatus() {
        return lockStatus;
    }

    public String getMultiPrice() {
        return multiPrice;
    }

    public int getNoOfColumn() {
        return noOfColumn;
    }


    public String getOperator() {
        return operator;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setTicketInfoPassengerDetail(ArrayList<TicketInfoPassengerDetail> ticketInfoPassengerDetail) {
        this.ticketInfoPassengerDetail = ticketInfoPassengerDetail;
    }

    public ArrayList<TicketInfoPassengerDetail> getTicketInfoPassengerDetail() {
        return ticketInfoPassengerDetail;
    }

    public String getRating() {
        return rating;
    }


    public String getShift() {
        return shift;
    }

    public String getTicketPrice() {
        return ticketPrice;
    }

    public List getTicketPriceList() {
        return ticketPriceList;
    }

    public ArrayList<SeatLayout> getSeatModel() {
        return seatModel;
    }

    public void setSeatModel(ArrayList<SeatLayout> seatModel) {
        this.seatModel = seatModel;
    }
}
