package net.qpaysolutions.QPay.Bus.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import net.qpaysolutions.QPay.Bus.Models.TripInfoModel;
import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

public class TicketsAdapter extends RecyclerView.Adapter<TicketsAdapter.TicketsViewHolder> {

    ArrayList<TripInfoModel> tripsModelArrayList;
    private Context context;
    private OnClickRecylerItem onClickRecylerItem;

    public TicketsAdapter(ArrayList<TripInfoModel> tripsModelArrayList, OnClickRecylerItem onClickRecylerItem) {
        this.tripsModelArrayList = tripsModelArrayList;
        this.onClickRecylerItem = onClickRecylerItem;
    }

    @NonNull
    @Override
    public TicketsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new TicketsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recylerview_bustickets, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TicketsViewHolder holder, final int position) {

        holder.busName.setText(tripsModelArrayList.get(position).getOperator());
        //  Picasso.with(context).load(tripsModelArrayList.get(position).getFaceImage()).into(holder.busImage);
        holder.shift.setText(tripsModelArrayList.get(position).getShift());
        if (tripsModelArrayList.get(position).getTicketPrice() == null || tripsModelArrayList.get(position).getTicketPrice().equalsIgnoreCase("null")) {
            holder.price.setText("Price not provided");
        } else {
            holder.price.setText("NPR " + tripsModelArrayList.get(position).getTicketPrice());
        }
        holder.recylerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickRecylerItem.onClickRecyclerItem(tripsModelArrayList.get(position));
            }
        });
        aminities(position, holder);
    }

    @Override
    public int getItemCount() {
        return tripsModelArrayList.size();
    }

    void aminities(int position, TicketsViewHolder holder) {
        ArrayList<String> aminities = tripsModelArrayList.get(position).getAmenities();
        for (int i = 0; i < aminities.size(); i++) {
            if (!aminities.get(i).contains("•")) {
                aminities.set(i, "\u2022 " + aminities.get(i));
            }
        }
        Log.d("nirajan", "aminities " + aminities);
        holder.aminities.setText("");
        holder.aminities.setText(aminities.toString()
                .replaceAll("(?<=[a-zA-Z]),", " ")
                .replaceAll("(?<=[,][\\s])•", "")
                .replaceAll("(?<=[,][\\s])","")
                .replace("[", "")
                .replace("]", ""));
    }

    class TicketsViewHolder extends RecyclerView.ViewHolder {
        ImageView busImage;
        TextView busName;
        TextView suspension;
        TextView shift;
        TextView price;
        LinearLayout recylerItem;
        TextView aminities;

        TicketsViewHolder(@NonNull View itemView) {
            super(itemView);
            busImage = itemView.findViewById(R.id.bus_image);
            busName = itemView.findViewById(R.id.busname);
            suspension = itemView.findViewById(R.id.suspension);
            shift = itemView.findViewById(R.id.shift);
            price = itemView.findViewById(R.id.price);
            recylerItem = itemView.findViewById(R.id.recycler_item);
            aminities = itemView.findViewById(R.id.aminities);
        }
    }


    public interface OnClickRecylerItem {
        void onClickRecyclerItem(TripInfoModel tripInfoModel);
    }
}
