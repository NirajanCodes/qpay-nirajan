package net.qpaysolutions.QPay.Notification;

/**
 * Created by deadlydragger on 9/10/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
