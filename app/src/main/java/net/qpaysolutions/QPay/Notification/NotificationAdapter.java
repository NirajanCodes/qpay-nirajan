package net.qpaysolutions.QPay.Notification;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Utility;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 7/20/16.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {
    ArrayList<NotificationMode> notificationModes;
    NotificationViewer notificationViewer;


    public NotificationAdapter(ArrayList<NotificationMode> notificationModes, NotificationViewer notificationViewer) {
        this.notificationModes = notificationModes;
        this.notificationViewer = notificationViewer;
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_iteam_notification, parent, false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, final int position) {
        final NotificationMode notification = notificationModes.get(position);
        holder.title.setText(notification.getTitle());
        holder.message.setText(notification.getMessage());
        holder.date.setText(Utility.dateFormatter(notification.getDate()));
        if (notification.getTitle().equals("Money Sent.")) {
            holder.set_view_color.setBackgroundColor(Color.parseColor("#f2635f"));
        } else if (notification.getTitle().equals("Money Recieved.")) {
            holder.set_view_color.setBackgroundColor(Color.parseColor("#60ba62"));
        }
        holder.trash.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                notificationModes.remove(position);
                new NotificationViewer.DeletNotificationItem(notification.getId(),false).execute();
                notifyDataSetChanged();
                if (notificationModes.size()>0){
                    notificationViewer.no_of_notification.setText(notificationModes.size()+" Notifications");
                    notificationViewer.menuItem.setVisible(true);
                    notificationViewer.no_notification.setVisibility(View.GONE);
                }else {
                    notificationViewer.no_of_notification.setVisibility(View.GONE);
                    notificationViewer.menuItem.setVisible(false);
                    notificationViewer.no_notification.setVisibility(View.VISIBLE);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationModes.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {
        TextView title, date, message, trash;
        View set_view_color;

        public NotificationHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            message = (TextView) view.findViewById(R.id.notification);
            date = (TextView) view.findViewById(R.id.notification_date);
            set_view_color = (View) view.findViewById(R.id.set_view_color);
            trash = (TextView) view.findViewById(R.id.trash);
        }
    }
}
