package net.qpaysolutions.QPay.Notification;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.EndlessRecyclerOnScrollListener;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 6/17/16.
 */
public class NotificationViewer extends Fragment {
    private QPayCustomerDatabase QPayCustomerDatabase;

    private FrameLayout progressBar;
    public LinearLayout no_notification;
    private ArrayList<NotificationMode> notificaionArray = new ArrayList<>();
    private RecyclerView notification_recycle;
    public static String appId, id;
    private NotificationAdapter notificationAdapter;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout progressNotification;
    public TextView no_of_notification;
    private int count = 1;
    public MenuItem menuItem;
    public QPayProgressDialog qPayProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        try {

            QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
            QPayCustomerDatabase.getReadableDatabase();
            QPayCustomerDatabase.getWritableDatabase();
            appId = QPayCustomerDatabase.getKeyAppId();
            id = QPayCustomerDatabase.getCustomerID();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      /*  ((MainActivity) getActivity()).setToolbar_text("Notification");
        ((MainActivity) getActivity()).showHomeLogo(true);
        ((MainActivity) getActivity()).setRightIcon(true);*/
        no_notification = (LinearLayout) view.findViewById(R.id.no_notification);
        progressBar = (FrameLayout) view.findViewById(R.id.progressBar);
        notification_recycle = (RecyclerView) view.findViewById(R.id.notification_recycle);
        progressNotification = (LinearLayout) view.findViewById(R.id.progressNotification);
        no_of_notification = (TextView) view.findViewById(R.id.no_of_notification);
        no_of_notification.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        notification_recycle.setLayoutManager(linearLayoutManager);
        progressBar.setVisibility(View.GONE);
        progressNotification.setVisibility(View.GONE);
        new NotificationAsy(appId, id, "0.0", "0.0", count, true).execute();
        progressNotification.setVisibility(View.GONE);
        notificationAdapter = new NotificationAdapter(notificaionArray, NotificationViewer.this);
        notification_recycle.setAdapter(notificationAdapter);
        notification_recycle.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                count++;
                if (notificaionArray.size() % 20 == 0) {
                    new NotificationAsy(appId, id, "0.0", "0.0", count, false).execute();

                }
            }
        });

    }

    public class NotificationAsy extends AsyncTask<String, String, String> {
        String appId, id, lat, lng;
        int i;
        boolean aBoolean;

        public NotificationAsy(String appId, String id, String lat, String lng, int i, boolean aBoolean) {
            this.appId = appId;
            this.id = id;
            this.lat = lat;
            this.lng = lng;
            this.i = i;
            this.aBoolean = aBoolean;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("lat", lat);
                jsonObject.put("lng", lng);
                jsonObject.put("page", i);
                jsonObject.put("offset", 20);
                Log.d("dinesh", "doInBackground: " + jsonObject);
                String pin_return = new NetworkAPI().sendHTTPData(Constants.NOTIFICATION, jsonObject);
                Log.d("dinesh", "doInBackground: " + pin_return);
                return pin_return;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (aBoolean) {
                progressBar.setVisibility(View.GONE);
            } else {
                progressNotification.setVisibility(View.GONE);
            }
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray data = jsonObject.getJSONArray("data");
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    if (data.length()>0){
                        menuItem.setVisible(true);
                        no_notification.setVisibility(View.GONE);
                        String NoOfRec = jsonObject.getString("NoOfRec");
                        no_of_notification.setVisibility(View.VISIBLE);
                        no_of_notification.setText(NoOfRec + " Notifications");
                        for (int i = 0; i < data.length(); i++) {
                            no_notification.setVisibility(View.GONE);
                            JSONObject jsonObject1 = data.getJSONObject(i);
                            NotificationMode notificationMode = new NotificationMode();
                            notificationMode.setDate(jsonObject1.getString("Date"));
                            notificationMode.setTitle(jsonObject1.getString("Title"));
                            notificationMode.setMessage(jsonObject1.getString("Message"));
                            notificationMode.setId(jsonObject1.getString("Id"));
                            notificaionArray.add(notificationMode);
                        }
                        notificationAdapter.notifyDataSetChanged();
                    }else {
                        no_notification.setVisibility(View.VISIBLE);
                    }



                } else {
                    if (aBoolean) {
                        no_notification.setVisibility(View.VISIBLE);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                no_notification.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (aBoolean) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressNotification.setVisibility(View.VISIBLE);
            }


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                   getActivity().onBackPressed();
                    /*MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment, najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", 1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                  *//*  getFragmentManager().beginTransaction()
                            .replace(R.id.fragment,najirEnglish)
                            .commit();*//*
                    getActivity().setTitle("QPay");*/
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification, menu);
        MenuItem item = menu.findItem(R.id.notification_delet);
        item.setVisible(false);
        menuItem = item;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notification_delet:

                showDeletsweet();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void showDeletsweet() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title=(TextView)v.findViewById(R.id.text_title);
        final ImageView img_icon =(ImageView)v.findViewById(R.id.img_icon);
        img_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_warning));
        img_icon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.color_yellow), android.graphics.PorterDuff.Mode.MULTIPLY);
        title.setText("Are You Sure?");
        dialog_balance.setText("Do you want to delete all notification ?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {new DeletNotification("ALL").execute();
                dialog.dismiss();
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public class DeletNotification extends AsyncTask<String, String, String> {

        String notificationId;



        public DeletNotification(String notificationId) {
            this.notificationId = notificationId;

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng",  LatLngPref.getLng());
                jsonObject.put("notificationId", notificationId);
                Log.d("dinesh", "doInBackground: " + jsonObject);
                Log.d("dinesh", "doInBackground: " + NetworkAPI.sendHTTPData(Constants.DELETE_NOTIFICATION, jsonObject));
                return NetworkAPI.sendHTTPData(Constants.DELETE_NOTIFICATION, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            menuItem.setVisible(false);
            qPayProgressDialog.dismiss();
            notificaionArray.clear();
            no_of_notification.setVisibility(View.GONE);
            no_notification.setVisibility(View.VISIBLE);
            notificationAdapter.notifyDataSetChanged();

        }
    }

    public static class DeletNotificationItem extends AsyncTask<String, String, String> {

        String notificationId;
        boolean aBoolean;


        public DeletNotificationItem(String notificationId, boolean aBoolean) {
            this.notificationId = notificationId;
            this.aBoolean = aBoolean;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("id", id);
                jsonObject.put("lat", 0.0);
                jsonObject.put("lng", 0.0);
                jsonObject.put("notificationId", notificationId);
                Log.d("dinesh", "doInBackground: " + jsonObject);
                Log.d("dinesh", "doInBackground: " + NetworkAPI.sendHTTPData(Constants.DELETE_NOTIFICATION, jsonObject));
                return NetworkAPI.sendHTTPData(Constants.DELETE_NOTIFICATION, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }
}