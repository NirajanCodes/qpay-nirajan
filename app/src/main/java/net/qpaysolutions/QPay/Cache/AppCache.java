package net.qpaysolutions.QPay.Cache;

import net.qpaysolutions.QPay.Chat.chatList.ChatModelList;
import net.qpaysolutions.QPay.Chat.chatmessageview.models.User;
import net.qpaysolutions.QPay.Chat.library.MessageList;

import java.util.ArrayList;

/**
 * Created by utsavstha on 5/28/17.
 */

public class AppCache {
    private static String yourID;
    private static String ipek;
    private static String ksn;


    public static String getLastMessageId() {
        return lastMessageId;
    }

    public static void setLastMessageId(String lastMessageId) {
        AppCache.lastMessageId = lastMessageId;
    }

    private static String messageID;
    private static MessageList messageList;
    private static String lastMessageId;

    public static String getLastMessage() {
        return lastMessage;
    }

    public static void setLastMessage(String lastMessage) {
        AppCache.lastMessage = lastMessage;
    }

    private static String lastMessage;

    public static ArrayList<ChatModelList> getChatModelLists() {
 /*       if(chatModelLists == null)  return chatModelLists = new ArrayList<>();*/

        return chatModelLists;
    }

    public static void setChatModelLists(ArrayList<ChatModelList> chatModelLists) {
        AppCache.chatModelLists = chatModelLists;
    }

    private static ArrayList<ChatModelList> chatModelLists = new ArrayList<>();

    public static MessageList getImageList() {
        if(imageList == null) return imageList = new MessageList();
        else return imageList;
    }

    public static void setImageList(MessageList imageList) {
        AppCache.imageList = imageList;
    }

    public static MessageList imageList;

    public static String getUserNumber() {
        return userNumber;
    }

    public static void setUserNumber(String userNumber) {
        AppCache.userNumber = userNumber;
    }

    private static String userNumber;

    public static ArrayList<User> getmUsers() {
        if(mUsers == null)
            mUsers  = new ArrayList<>();

        return mUsers;
    }




    public static String getMessageId() {
        return messageId;
    }

    public static void setMessageId(String messageId) {
        AppCache.messageId = messageId;
    }

    public static String messageId;
    public static void setmUsers(ArrayList<User> mUsers) {
        AppCache.mUsers = mUsers;
    }

    private static ArrayList<User> mUsers;

    public static String getYourID() {
        return yourID;
    }

    public static void setYourID(String yourID) {
        AppCache.yourID = yourID;
    }

    public static void setMessageID(String messageID) {
        AppCache.messageID = messageID;
    }

    public static MessageList getMessageList() {
        if(messageList == null) return messageList = new MessageList();

        else return messageList;
    }

    public static String getMessageID() {

        return messageID;
    }

    public static void setMessageList(MessageList messageList) {
        AppCache.messageList = messageList;
    }
}
