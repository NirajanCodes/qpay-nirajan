package net.qpaysolutions.QPay.Receiption;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.FragmentCollection.MainFragment;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 12/21/16.
 */

public class ReceptionFragment extends Fragment {
    RecyclerView rceycle_reception;
    String term_id;
    QPayCustomerDatabase qpayMerchantDatabase;
    ArrayList<Reception>  receptionArrayList = new ArrayList<>();
    QPayProgressDialog qPayProgressDialog;
    ReceptionAdapter receptionAdapter;
    LinearLayout no_reception;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reception_main,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        getActivity(). setTitle("Receipt");
        ((MainActivity)getActivity()).showHomeLogo(true);
        ((MainActivity)getActivity()).setRightIcon(false);
        no_reception=(LinearLayout)view.findViewById(R.id.no_reception);
        rceycle_reception=(RecyclerView)view.findViewById(R.id.rceycle_reception);
        term_id=qpayMerchantDatabase.getCustomerID();
        new ReceptionAsyk(term_id,"1","10").execute();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qpayMerchantDatabase= new QPayCustomerDatabase(getActivity());
        qpayMerchantDatabase.getReadableDatabase();
    }
    public class ReceptionAsyk extends AsyncTask<String,String,String>{
        String id,page,offset;

        public ReceptionAsyk(String id,String page,String offset){
            this.id=id;
            this.page=page;
            this.offset=offset;
        }

        @Override
        protected String doInBackground(String... params) {
            String reception_result="";
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("id",id);
                jsonObject.put("page",page);
                jsonObject.put("offset",offset);
                jsonObject.put("lat", ((CustomerApplication)getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication)getActivity().getApplication()).getLng());
                Log.d(Utility.TAG,"post receipt  : " + jsonObject);
                reception_result = networkAPI.sendHTTPData(Constants.RECEPTION,jsonObject);

                Log.d(Utility.TAG,"receipt : " + reception_result);

            }catch (Exception e){
                e.printStackTrace();
            }
            return reception_result;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success==true){
                    JSONArray data = jsonObject.getJSONArray("data");
                    for (int i= 0; i<data.length();i++){
                        JSONObject object = data.getJSONObject(i);
                        Reception reception = new Reception();
                        reception.setMerchName(object.getString("MerchName"));
                        reception.setAddress(object.getString("Address"));
                        reception.setTid(object.getString("Tid"));
                        reception.setAmount(new Decimalformate().decimalFormate(object.getString("Amount")));
                        reception.setAprCode(object.getString("AprCode"));
                        reception.setBatchNo(object.getString("BatchNo"));
                        reception.setCard(object.getString("Card"));
                        reception.setCardType(object.getString("CardType"));
                        reception.setCrrn(object.getString("Crrn"));
                        reception.setExpDate(object.getString("ExpDate"));
                        reception.setInvoiceNo(object.getString("InvoiceNo"));
                        reception.setMid(object.getString("Mid"));
                        reception.setParticular(object.getString("Particular"));
                        reception.setDate(object.getString("Date"));
                        receptionArrayList.add(reception);
                    }
                    receptionAdapter= new ReceptionAdapter(getContext(),receptionArrayList);
                    rceycle_reception.setLayoutManager( new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));
                    rceycle_reception.setHasFixedSize(true);
                    rceycle_reception.setAdapter(receptionAdapter);
                }else {
                    no_reception.setVisibility(View.VISIBLE);
                }

            }catch (Exception e){
                e.printStackTrace();
                no_reception.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog= new QPayProgressDialog(getActivity());
            qPayProgressDialog.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    MainFragment najirEnglish = new MainFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment,najirEnglish);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id",1);
                    najirEnglish.setArguments(bundle);
                    fragmentTransaction.commit();
                  /*  getFragmentManager().beginTransaction()
                            .replace(R.id.fragment,najirEnglish)
                            .commit();*/
                    getActivity().setTitle("QPay");
                    return true;
                }
                return false;
            }
        });
    }
}
