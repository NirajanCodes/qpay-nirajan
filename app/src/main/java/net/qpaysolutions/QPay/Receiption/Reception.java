package net.qpaysolutions.QPay.Receiption;

/**
 * Created by deadlydragger on 12/26/16.
 */

public class Reception {
    String MerchName,Address,Tid,Amount,AprCode,BatchNo,Card,CardType,Crrn,ExpDate,InvoiceNo,Mid,Particular,Date;

    public String getCrrn() {
        return Crrn;
    }

    public String getAddress() {
        return Address;
    }

    public String getAmount() {
        return Amount;
    }

    public String getAprCode() {
        return AprCode;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public String getCard() {
        return Card;
    }

    public String getCardType() {
        return CardType;
    }

    public String getDate() {
        return Date;
    }

    public String getExpDate() {
        return ExpDate;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public String getMerchName() {
        return MerchName;
    }

    public String getMid() {
        return Mid;
    }

    public String getParticular() {
        return Particular;
    }

    public String getTid() {
        return Tid;
    }

    public void setCrrn(String crrn) {
        Crrn = crrn;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public void setAprCode(String aprCode) {
        AprCode = aprCode;
    }

    public void setBatchNo(String batchNo) {
        BatchNo = batchNo;
    }

    public void setCard(String card) {
        Card = card;
    }

    public void setCardType(String cardType) {
        CardType = cardType;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setExpDate(String expDate) {
        ExpDate = expDate;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public void setMerchName(String merchName) {
        MerchName = merchName;
    }

    public void setMid(String mid) {
        Mid = mid;
    }

    public void setParticular(String particular) {
        Particular = particular;
    }

    public void setTid(String tid) {
        Tid = tid;
    }
}
