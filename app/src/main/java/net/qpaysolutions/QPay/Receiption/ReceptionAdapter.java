package net.qpaysolutions.QPay.Receiption;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by deadlydragger on 12/26/16.
 */

public class ReceptionAdapter extends RecyclerView.Adapter<ReceptionAdapter.ReceptionViewholder> {
    ArrayList<Reception> receptions;
    Context context;

    public ReceptionAdapter(Context context, ArrayList<Reception> receptions) {
        this.context = context;
        this.receptions = receptions;
    }

    @Override
    public ReceptionViewholder onCreateViewHolder(ViewGroup parent, int viewType) {

          View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reception_row,parent,false);
          ReceptionViewholder receptionViewholder = new ReceptionViewholder(view);
          return receptionViewholder;

    }

    @Override
    public void onBindViewHolder(ReceptionViewholder holder, int position) {

        holder.tid.setText(receptions.get(position).getTid());
        holder.mid.setText(receptions.get(position).getMid());
        holder.batch_no.setText(receptions.get(position).getBatchNo());
        holder.invoice_no.setText(receptions.get(position).getInvoiceNo());
        holder.card.setText(receptions.get(position).getCard());
        holder.exp_date.setText(receptions.get(position).getExpDate());
        holder.apr_code.setText(receptions.get(position).getAprCode());
        holder.crrn.setText(receptions.get(position).getCrrn());
        holder.name.setText(receptions.get(position).getMerchName());
        holder.address.setText(receptions.get(position).getAddress());
        holder.amount_total.setText(receptions.get(position).getAmount());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String apiu_date = receptions.get(position).getDate();
        try {
            Date date = simpleDateFormat.parse(apiu_date);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MMM dd, yyyy");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm a");
            holder.date.setText(simpleDateFormat1.format(date));
            holder.time.setText(time.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public int getItemCount() {
        return receptions.size();
    }

    public class ReceptionViewholder extends RecyclerView.ViewHolder {
        TextView date,time,tid,mid,batch_no,invoice_no,card,exp_date,apr_code,crrn,name,address,amount_total;

        public ReceptionViewholder(View itemView) {

            super(itemView);
            date=(TextView)itemView.findViewById(R.id.date);
            time=(TextView)itemView.findViewById(R.id.time);
            tid=(TextView)itemView.findViewById(R.id.tid);
            mid=(TextView)itemView.findViewById(R.id.mid);
            batch_no=(TextView)itemView.findViewById(R.id.batch_no);
            invoice_no=(TextView)itemView.findViewById(R.id.invoice_no);
            card=(TextView)itemView.findViewById(R.id.card);
            exp_date=(TextView)itemView.findViewById(R.id.exp_date);
            apr_code=(TextView)itemView.findViewById(R.id.apr_code);
            crrn=(TextView)itemView.findViewById(R.id.crrn);
            name=(TextView)itemView.findViewById(R.id.name);
            address=(TextView)itemView.findViewById(R.id.address);
            amount_total=(TextView)itemView.findViewById(R.id.amount_total);

        }
    }
}
