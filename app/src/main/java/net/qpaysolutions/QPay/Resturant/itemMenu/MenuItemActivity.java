package net.qpaysolutions.QPay.Resturant.itemMenu;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 3/9/17.
 */

public class MenuItemActivity extends AppCompatActivity {
    Toolbar toolbar;
    String resturant_name, id, Address, imgUrl;
    TextView title;
    MenuItemFragment menuItemFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title = (TextView) toolbar.findViewById(R.id.title);
        resturant_name = getIntent().getStringExtra("name");
        id = getIntent().getStringExtra("id");
        Address = getIntent().getStringExtra("Address");
        imgUrl = getIntent().getStringExtra("imgUrl");
         menuItemFragment = new MenuItemFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_menu, menuItemFragment);
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("Address", Address);
        bundle.putString("name", resturant_name);
        bundle.putString("imgUrl", imgUrl);
        menuItemFragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    public void setTitleToolbar(String address) {
        title.setText(address);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
