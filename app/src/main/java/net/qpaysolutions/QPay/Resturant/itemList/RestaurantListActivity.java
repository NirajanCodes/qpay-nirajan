package net.qpaysolutions.QPay.Resturant.itemList;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 1/22/18.
 */

public class RestaurantListActivity extends AppCompatActivity {
    String id, icon, background, name;
    Toolbar toolbaruni;
    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_main);
        toolbaruni = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaruni);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textView = (TextView) toolbaruni.findViewById(R.id.title);
        id = getIntent().getStringExtra("id");
        icon = getIntent().getStringExtra("icon");
        background = getIntent().getStringExtra("background");
        name = getIntent().getStringExtra("name");
        ResturantItemListFragment resturantItemListFragment = new ResturantItemListFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentt_list, resturantItemListFragment);
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("icon", icon);
        bundle.putString("background", background);
        bundle.putString("name", name);
        resturantItemListFragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    public void setTitleToolbar(String address) {
        textView.setText(address);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void setIcon(ImageView icon, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()

                    .load(url)
                    .error(R.drawable.ic_atm_default_icon)
                    .placeholder(R.drawable.ic_atm_default_icon)
                    .into(icon);
        }
    }

    public void setBackground(String url, final LinearLayout linearLayout) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()
                    .load(url).into(new Target() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    linearLayout.setBackground(new BitmapDrawable(bitmap));
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }


                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
        }
    }
}
