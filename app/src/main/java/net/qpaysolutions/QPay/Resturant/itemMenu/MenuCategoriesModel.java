package net.qpaysolutions.QPay.Resturant.itemMenu;

/**
 * Created by deadlydragger on 3/9/17.
 */

public class MenuCategoriesModel {
    String Category,IconUrl,Id;

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getIconUrl() {
        return IconUrl;
    }

    public void setIconUrl(String iconUrl) {
        IconUrl = iconUrl;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
