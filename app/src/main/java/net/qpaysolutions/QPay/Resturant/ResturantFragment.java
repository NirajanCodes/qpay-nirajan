package net.qpaysolutions.QPay.Resturant;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.NearByShop.NearbyShopListActivity;
import net.qpaysolutions.QPay.NearByTaxi.NearTaxiActivity;
import net.qpaysolutions.QPay.Resturant.itemList.RestaurantListActivity;
import net.qpaysolutions.QPay.Resturant.maincatrgory.RestaurantCategoryDapter;
import net.qpaysolutions.QPay.Resturant.maincatrgory.ResturantCategoryModel;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static androidx.core.content.ContextCompat.checkSelfPermission;

/**
 * Created by deadlydragger on 3/6/17.
 */

public class ResturantFragment extends Fragment {
    private  String appId, cust_id;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private  QPayProgressDialog qPayProgressDialog;
    private  SharedPreferences sharedPreferences;
    private  String FILE = "resturant";
    private   String KEY = "resturant";
    private   String TAG = "dinesh";
    private   String get_resturant;
    private   ArrayList<ResturantCategoryModel> categoryModelArrayList;
    private RestaurantCategoryDapter resturantCatrgoryAdapter;
    private RecyclerView recycle_review;
    private  LinearLayout match_screen_resturant;
    private int height;
    private  AHBottomNavigation bottomNavigation;
    private FusedLocationProviderClient mFusedLocationClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.resturant_main, container, false);
    }

    public void setHeight(int office) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("height", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("height", office);
        editor.apply();

    }

    public int getHeight() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("height", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("height", 0);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ResturantActivity) getActivity()).setToolbarTitle("Restaurant");

        bottomNavigation = (AHBottomNavigation) view.findViewById(R.id.bottom_navigation);
        recycle_review = (RecyclerView) view.findViewById(R.id.recycle_review);
        match_screen_resturant = (LinearLayout) view.findViewById(R.id.match_screen_resturant);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recycle_review.addItemDecoration(new DividerItemDecoration(spacingInPixels));
        recycle_review.setHasFixedSize(true);
        recycle_review.setLayoutManager(new GridLayoutManager(getContext(), 2));
        ViewTreeObserver vto = match_screen_resturant.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int width = match_screen_resturant.getMeasuredWidth();
                height = match_screen_resturant.getMeasuredHeight();
            }
        });
        try {
            get_resturant = sharedPreferences.getString(KEY, null);
            JSONArray jsonArray = new JSONArray(get_resturant);
            categoryModelArrayList = new ArrayList<>();
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    ResturantCategoryModel resturantCategoryModel = new ResturantCategoryModel();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    resturantCategoryModel.setBackgroundImage(jsonObject1.getString(Tags.BACKGROUND_IMAGE));
                    resturantCategoryModel.setCategory(jsonObject1.getString(Tags.CATRGORY_S));
                    resturantCategoryModel.setIcon(jsonObject1.getString(Tags.ICON));
                    resturantCategoryModel.setId(jsonObject1.getString(Tags.ID_S));
                    resturantCategoryModel.setNoOfRestro(jsonObject1.getString(Tags.NO_OF_RESTRO));
                    categoryModelArrayList.add(resturantCategoryModel);
                }
                Log.d(TAG, "onViewCreated: " + height);
                resturantCatrgoryAdapter = new RestaurantCategoryDapter(getContext(), categoryModelArrayList, getHeight());
                recycle_review.setAdapter(resturantCatrgoryAdapter);
                recycle_review.addOnItemTouchListener(new NearbyShopListActivity.RecyclerTouchListener(getContext(), recycle_review, new NearbyShopListActivity.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        if (Integer.valueOf(categoryModelArrayList.get(position).getNoOfRestro()) > 0) {

                            Intent intent = new Intent(getActivity(), RestaurantListActivity.class);
                            intent.putExtra("id", categoryModelArrayList.get(position).getId());
                            intent.putExtra("icon", categoryModelArrayList.get(position).getIcon());
                            intent.putExtra("background", categoryModelArrayList.get(position).getBackgroundImage());
                            intent.putExtra("name", categoryModelArrayList.get(position).getCategory());
                            startActivity(intent);
                        } else {
                            new Utility().showSnackbar(view, "No Restaurant");
                        }

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getRestro();
        buttonBar();


    }
    public static boolean locationPermission(Context context, String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void getRestro() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (!locationPermission(getContext(), PERMISSIONS)) {
               requestPermissions(PERMISSIONS, Tags.LOCATION);
            } else {
                if (checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mFusedLocationClient.getLastLocation()
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }
                            })
                            .addOnSuccessListener(new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(final Location location) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (location != null) {
                                                LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                                LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                                ((ResturantActivity) getActivity()).new LocationDelivery().execute();
                                                new ResturantCategory().execute();
                                            } else {
                                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                                startActivity(intent);
                                            }
                                        }
                                    },100);

                                }
                            });
                }


            }
        } else {
            mFusedLocationClient.getLastLocation()
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    })
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(final Location location) {

                          new Handler().postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                  if (location != null) {
                                      LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                      LatLngPref.setLng(String.valueOf(location.getLongitude()));
                                      ((ResturantActivity) getActivity()).new LocationDelivery().execute();
                                      new ResturantCategory().execute();
                                  } else {
                                      Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                      startActivity(intent);
                                  }
                              }
                          },100);

                        }
                    });

        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        appId = QPayCustomerDatabase.getKeyAppId();
        cust_id = QPayCustomerDatabase.getCustomerID();
        sharedPreferences = getActivity().getSharedPreferences(FILE, Context.MODE_PRIVATE);
    }

    public class ResturantCategory extends AsyncTask<String, String, String> {
        ArrayList<ResturantCategoryModel> categoryModelArrayList = new ArrayList<>();

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            String category_response = "";
            try {
                jsonObject.put(Tags.APPID_S, appId);
                jsonObject.put(Tags.CUST_ID, cust_id);
                Log.d(TAG, "doInBackground: " + jsonObject.toString());
                category_response = networkAPI.sendHTTPData(Constants.GET_CATEGORY, jsonObject);
                Log.d(TAG, "doInBackground: " + category_response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return category_response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(KEY, data.toString());
                    editor.apply();
                    get_resturant = sharedPreferences.getString(KEY, null);
                    JSONArray jsonArray = new JSONArray(get_resturant);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ResturantCategoryModel resturantCategoryModel = new ResturantCategoryModel();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        resturantCategoryModel.setBackgroundImage(jsonObject1.getString(Tags.BACKGROUND_IMAGE));
                        resturantCategoryModel.setCategory(jsonObject1.getString(Tags.CATRGORY_S));
                        resturantCategoryModel.setIcon(jsonObject1.getString(Tags.ICON));
                        resturantCategoryModel.setId(jsonObject1.getString(Tags.ID_S));
                        resturantCategoryModel.setNoOfRestro(jsonObject1.getString(Tags.NO_OF_RESTRO));
                        categoryModelArrayList.add(resturantCategoryModel);
                    }
                    Log.d(TAG, "onPostExecute: " + height);
                    resturantCatrgoryAdapter = new RestaurantCategoryDapter(getContext(), categoryModelArrayList, height);
                    setHeight(height);
                    recycle_review.setAdapter(resturantCatrgoryAdapter);

                } else {
                    get_resturant = sharedPreferences.getString(KEY, null);
                    JSONArray jsonArray = new JSONArray(get_resturant);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ResturantCategoryModel resturantCategoryModel = new ResturantCategoryModel();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        resturantCategoryModel.setBackgroundImage(jsonObject1.getString(Tags.BACKGROUND_IMAGE));
                        resturantCategoryModel.setCategory(jsonObject1.getString(Tags.CATRGORY_S));
                        resturantCategoryModel.setIcon(jsonObject1.getString(Tags.ICON));
                        resturantCategoryModel.setId(jsonObject1.getString(Tags.ID_S));
                        resturantCategoryModel.setNoOfRestro(jsonObject1.getString(Tags.NO_OF_RESTRO));
                        categoryModelArrayList.add(resturantCategoryModel);
                    }
                    resturantCatrgoryAdapter = new RestaurantCategoryDapter(getContext(), categoryModelArrayList, height);
                    recycle_review.setAdapter(resturantCatrgoryAdapter);
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    get_resturant = sharedPreferences.getString(KEY, null);
                    JSONArray jsonArray = new JSONArray(get_resturant);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ResturantCategoryModel resturantCategoryModel = new ResturantCategoryModel();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        resturantCategoryModel.setBackgroundImage(jsonObject1.getString(Tags.BACKGROUND_IMAGE));
                        resturantCategoryModel.setCategory(jsonObject1.getString(Tags.CATRGORY_S));
                        resturantCategoryModel.setIcon(jsonObject1.getString(Tags.ICON));
                        resturantCategoryModel.setId(jsonObject1.getString(Tags.ID_S));
                        resturantCategoryModel.setNoOfRestro(jsonObject1.getString(Tags.NO_OF_RESTRO));
                        categoryModelArrayList.add(resturantCategoryModel);
                    }
                    resturantCatrgoryAdapter = new RestaurantCategoryDapter(getContext(), categoryModelArrayList, height);
                    recycle_review.setAdapter(resturantCatrgoryAdapter);

                } catch (Exception ee) {
                    ee.printStackTrace();
                }

            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getActivity());
            qPayProgressDialog.show();
        }
    }

    public void buttonBar() {

        AHBottomNavigationItem item = new AHBottomNavigationItem(R.string.tab_0, R.drawable.ic_action_footer_home_tab, R.color.colorPrimary);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_action_chat_tab, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_nearby_atm_tab, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_action_button_car, R.color.colorPrimary);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_action_restaurant_tab, R.color.colorPrimary);
        bottomNavigation.addItem(item);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item5);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item4);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#cfe1fa"));
        bottomNavigation.setCurrentItem(2);
        bottomNavigation.setForceTint(false);
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setColored(true);
        try {
            if (GeneralPref.getChatCount() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getChatCount()), 1);
            }
            if (GeneralPref.getTaxi() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getTaxi()), 4);
            }
            if (GeneralPref.getAtm() > 0) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getAtm()), 3);
            }
            if (GeneralPref.getNoofRestro() > 2) {
                bottomNavigation.setNotification(String.valueOf(GeneralPref.getNoofRestro()), 2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                Log.d(Utility.TAG, "tab selected" + position);
                if (position == 0) {

                    getActivity().onBackPressed();
                } else if (position == 1) {

                    Intent mycards = new Intent(getActivity(), ChatActivity.class);
                    mycards.putExtra("Check", 2);
                    Bundle bundleanim = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    getActivity().startActivity(mycards, bundleanim);
                } else if (position == 2) {


                } else if (position == 3) {
                    Intent mycards = new Intent(getActivity(), MainActivity.class);
                    mycards.putExtra("Check", 7);
                    Bundle bundleanim = ActivityOptions.makeCustomAnimation(getContext(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    getActivity().startActivity(mycards, bundleanim);

                } else if (position == 4) {
                    Intent history = new Intent(getActivity(), NearTaxiActivity.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.animation_one_activity, R.anim.animation_two_activity).toBundle();
                    startActivity(history, bndlanimation);
                }
                return true;
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getRestro();

                } else {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getActivity().getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "Go to setting and give camera permission.", Toast.LENGTH_LONG).show();
                }

                break;
        }



    }
}
