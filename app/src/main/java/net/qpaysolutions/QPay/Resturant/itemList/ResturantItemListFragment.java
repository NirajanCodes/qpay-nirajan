package net.qpaysolutions.QPay.Resturant.itemList;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Resturant.DividerItemDecoration;
import net.qpaysolutions.QPay.Resturant.maincatrgory.ResturantCategoryModel;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlydragger on 3/8/17.
 */

public class ResturantItemListFragment extends Fragment {
    RecyclerView recycle_review;
    ResturantListAdapter resturantListAdapter;
    QPayProgressDialog qPayProgressDialog;
    String id, background, icon, name;
    SharedPreferences sharedPreferences;
    String FILE = "list";
    String KEY = "list";
    String get_List;
    String cust_id, app_id;
    QPayCustomerDatabase QPayCustomerDatabase;
    String TAG = "dinesh";
    ArrayList<ResturantListModel> listModelArrayList = new ArrayList<>();
    ImageView icon_img;
    LinearLayout list_background;
    TextView text_category;
    private List<ResturantCategoryModel> merchantMenuList;
    private List<ResturantCategoryModel> origMerchantMenuInfo;
    EditText searchOption;
    private  LinearLayout no_store;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        cust_id = QPayCustomerDatabase.getCustomerID();
        app_id = QPayCustomerDatabase.getKeyAppId();
        QPayCustomerDatabase.deletCartAll();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.resturant_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(FILE, Context.MODE_PRIVATE);
        id = getArguments().getString("id");
        background = getArguments().getString("background");
        icon = getArguments().getString("icon");
        name = getArguments().getString("name");
        ((RestaurantListActivity) getActivity()).setTitleToolbar(name + " List");
        recycle_review = (RecyclerView) view.findViewById(R.id.recycle_review);
        icon_img = (ImageView) view.findViewById(R.id.icon);
        list_background = (LinearLayout) view.findViewById(R.id.list_background);
        text_category = (TextView) view.findViewById(R.id.text_category);
        text_category.setText(name);
        recycle_review.setHasFixedSize(true);
        recycle_review.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recycle_review.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.divider));
        ((RestaurantListActivity) getActivity()).setBackground(background, list_background);
        ((RestaurantListActivity) getActivity()).setIcon(icon_img, icon);
        no_store=(LinearLayout)view.findViewById(R.id.no_store);
        new ResturantList(cust_id, app_id, id).execute();
        searchresturant(view);
    }


    public class ResturantList extends AsyncTask<String, String, String> {
        String cust_id, app_id, id;

        public ResturantList(String cust_id, String app_id, String id) {
            this.cust_id = cust_id;
            this.app_id = app_id;
            this.id = id;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String list_response = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Tags.CUST_ID, cust_id);
                jsonObject.put(Tags.APPID_S, app_id);
                jsonObject.put(Tags.ID, id);
                Log.d(TAG, "doInBackground: " + jsonObject);
                list_response = networkAPI.sendHTTPData(Constants.GET_RESTURANT_LIST, jsonObject);
                Log.d(TAG, "doInBackground: " + list_response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return list_response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean(Tags.SUCESS);
                if (success) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(KEY, jsonArray.toString());
                    editor.apply();
                    get_List = sharedPreferences.getString(KEY, null);
                    JSONArray jsonArray1 = new JSONArray(get_List);
                    if (jsonArray1.length() > 0) {
                        no_store.setVisibility(View.GONE);
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            ArrayList<String> opening = new ArrayList<>();
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            ResturantListModel resturantListModel = new ResturantListModel();
                            resturantListModel.setId(jsonObject1.getString(Tags.ID_S));
                            resturantListModel.setBackgroundImage(jsonObject1.getString(Tags.BACKGROUND_IMAGE));
                            resturantListModel.setAddress(jsonObject1.getString(Tags.ADDRESS));
                            resturantListModel.setRating(jsonObject1.getString(Tags.RATING));
                            resturantListModel.setRestaurant(jsonObject1.getString(Tags.RESTURANT));
                            resturantListModel.setImageUrl(jsonObject1.getString(Tags.IMGURL_S));
                            resturantListModel.setCuisine(jsonObject1.getString(Tags.CAUSINE));
                            JSONArray OpeningHour = jsonObject1.getJSONArray("OpeningHour");
                            opening.add(OpeningHour.getString(0));
                            opening.add(OpeningHour.getString(1));
                            resturantListModel.setResturantListModels(opening);
                            Log.d(Utility.TAG,"address : "+resturantListModel.getAddress());
                            listModelArrayList.add(resturantListModel);
                        }
                        resturantListAdapter = new ResturantListAdapter(listModelArrayList, getContext());
                        recycle_review.setAdapter(resturantListAdapter);
                       /* recycle_review.addOnItemTouchListener(new NearbyShopListActivity.RecyclerTouchListener(getContext(), recycle_review, new NearbyShopListActivity.ClickListener() {
                            @Override
                            public void onClick(View view, int position) {

                              *//*  Intent intent = new Intent(getActivity(), MenuItemActivity.class);
                                intent.putExtra("id", listModelArrayList.get(position).getId());
                              *//**//*  intent.putExtra("icon",categoryModelArrayList.get(position).getIcon());
                                intent.putExtra("background",categoryModelArrayList.get(position).getBackgroundImage());*//**//*
                                intent.putExtra("imgUrl", listModelArrayList.get(position).getImageUrl());
                                intent.putExtra("name", listModelArrayList.get(position).getRestaurant());
                                intent.putExtra("Address", listModelArrayList.get(position).getAddress());
                                startActivity(intent);
                                Log.d(TAG, "onClick: restaurant "+resturantListAdapter.getItemId(position));*//*
                            }

                            @Override
                            public void onLongClick(View view, int position) {

                            }
                        }));*/
                    }else {
                        no_store.setVisibility(View.VISIBLE);
                    }


                } else {
                    no_store.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
                try {
                    no_store.setVisibility(View.VISIBLE);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }
    }

    public void searchresturant(View view) {
        searchOption = (EditText) view.findViewById(R.id.search);
        searchOption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (resturantListAdapter != null) {
                    resturantListAdapter.getFilter().filter(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }



}
