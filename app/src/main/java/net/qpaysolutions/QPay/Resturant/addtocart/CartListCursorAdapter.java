package net.qpaysolutions.QPay.Resturant.addtocart;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;

/**
 * Created by deadlydragger on 3/10/17.
 */

public class CartListCursorAdapter extends CursorAdapter  {
    CartListFragment cartListFragment;
    public CartListCursorAdapter(Context context, Cursor c,CartListFragment cartListFragment) {
        super(context, c);
        this.cartListFragment=cartListFragment;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_list, parent, false);
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final TextView name = (TextView) view.findViewById(R.id.item_name);
        final TextView price = (TextView) view.findViewById(R.id.item_price);
        TextView count = (TextView) view.findViewById(R.id.item_count);
        ImageView clear_item=(ImageView)view.findViewById(R.id.clear_item);

        final String CategoryId = cursor.getString(cursor.getColumnIndexOrThrow("CategoryId"));
        final String Description = cursor.getString(cursor.getColumnIndexOrThrow("Description"));
        final String ImageUrl = cursor.getString(cursor.getColumnIndexOrThrow("ImageUrl"));
        final String id = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
        final String ItemId = cursor.getString(cursor.getColumnIndexOrThrow("ItemId"));
        final String ItemName = cursor.getString(cursor.getColumnIndexOrThrow("ItemName"));
        final String Rate = cursor.getString(cursor.getColumnIndexOrThrow("Rate"));
        final String Count = cursor.getString(cursor.getColumnIndexOrThrow("Count"));
        name.setText(ItemName);
        price.setText("NPR "+new Decimalformate().decimalFormate(Rate));
        count.setText(Count);
        clear_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartListFragment.deletItem(ItemId);
            }
        });
    }


}
