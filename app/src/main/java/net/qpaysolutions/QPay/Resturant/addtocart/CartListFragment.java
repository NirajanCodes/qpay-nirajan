package net.qpaysolutions.QPay.Resturant.addtocart;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Resturant.checkout.CheckOutActivity;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.Utility;

/**
 * Created by deadlydragger on 3/10/17.
 */

public class CartListFragment extends Fragment implements View.OnClickListener {
    ImageView image_restaurant;
    TextView name_restaurant, location_restaurant, clear_all, subtotal,vat_percent,service_percent;
    SharedPreferences sharedPreferences;
    String FILE = "restaurant_menu";
    String KEY = "restaurant_menu";
    String TAG = "dinesh";
    String Id, name, Address, imgUrl;
    ListView cartList;
    Cursor todoCursor;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private SQLiteDatabase db;
    CartListCursorAdapter cartListCursorAdapter;
    String VAT = "Vat";
    String SERVICE_CHARGE = "ServiceCharge";
    float Vat, ServiceCharge;
    TextView vat, service_charge, grand_total;
    double sub_total, charge, vat_total;
    Button proceed,more;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();
        db = QPayCustomerDatabase.getWritableDatabase();
        todoCursor = db.rawQuery("SELECT *  FROM cart ORDER BY _id DESC", null);//order by date desc
        Log.d(TAG, "onCreate: " + QPayCustomerDatabase.getSubTotal());

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPreferences = getActivity().getSharedPreferences(FILE, Context.MODE_PRIVATE);
        Id = sharedPreferences.getString("id", null);
        name = sharedPreferences.getString("name", null);
        Address = sharedPreferences.getString("address", null);
        imgUrl = sharedPreferences.getString("imgUrl", null);
        Vat = sharedPreferences.getFloat(VAT, 0);
        ServiceCharge = sharedPreferences.getFloat(SERVICE_CHARGE, 0);
        image_restaurant = (ImageView) view.findViewById(R.id.image_restaurant);
        name_restaurant = (TextView) view.findViewById(R.id.name_restaurant);
        location_restaurant = (TextView) view.findViewById(R.id.location_restaurant);
        cartList = (ListView) view.findViewById(R.id.cartList);
        clear_all = (TextView) view.findViewById(R.id.clear_all);
        subtotal = (TextView) view.findViewById(R.id.subtotal);
        vat = (TextView) view.findViewById(R.id.vat);
        grand_total = (TextView) view.findViewById(R.id.total);
        service_charge = (TextView) view.findViewById(R.id.serviceCharge);
        vat_percent=(TextView)view.findViewById(R.id.vat_percent);
        service_percent=(TextView)view.findViewById(R.id.servicecharge_percent);
        proceed=(Button)view.findViewById(R.id.proceed);
        more=(Button)view.findViewById(R.id.more);
        proceed.setOnClickListener(this);
        more.setOnClickListener(this);
        vat_percent.setText("Vat "+(int) Vat +"%");
        service_percent.setText("Service Charge "+(int) ServiceCharge+"%");
        clear_all.setOnClickListener(this);
        setIcon(image_restaurant, imgUrl);
        name_restaurant.setText(name);
        location_restaurant.setText(Address);
        cartListCursorAdapter = new CartListCursorAdapter(getContext(), todoCursor, CartListFragment.this);
        cartList.setAdapter(cartListCursorAdapter);
        setSubtotal();
        cartList.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
               /* v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;*/
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        setListViewHeightBasedOnChildren(cartList);

    }
    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_to_cart, container, false);
    }

    public void setIcon(ImageView icon, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()

                    .load(url)
                    .error(R.drawable.ic_atm_default_icon)
                    .placeholder(R.drawable.ic_atm_default_icon)
                    .into(icon);
        }

    }

    public void deletItem(String id) {
        QPayCustomerDatabase.deletCart(id);
        cartListCursorAdapter.getCursor().requery();
        cartListCursorAdapter.notifyDataSetChanged();
        Utility.setDynamicHeight(cartList);
        setSubtotal();
        if (cartListCursorAdapter.getCursor().getCount() > 0){

        }else {
            getActivity().onBackPressed();
        }
    }

    public void setSubtotal() {
        sub_total = QPayCustomerDatabase.getSubTotal();
        subtotal.setText(String.valueOf(new Decimalformate().decimalFormate(String.valueOf(sub_total))));
        totalVat();
        serViceCharge();
        grandTotal();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear_all:
                QPayCustomerDatabase.deletCartAll();
                getActivity().onBackPressed();
                break;
            case R.id.proceed:
                startActivity(new Intent(getActivity(), CheckOutActivity.class));
                break;
            case R.id.more:
                getActivity().onBackPressed();
                break;
        }
    }

    public void totalVat() {
        vat_total = sub_total * (Vat / 100.0);
        vat.setText(new Decimalformate().decimalFormate(String.valueOf(vat_total)));

    }

    public void grandTotal() {
        double total = sub_total + charge + vat_total;
        grand_total.setText(new Decimalformate().decimalFormate(String.valueOf(total)));
    }

    public void serViceCharge() {
        charge = sub_total * (ServiceCharge / 100.0);
        service_charge.setText(String.valueOf(charge));
    }
}
