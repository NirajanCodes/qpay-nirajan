package net.qpaysolutions.QPay.Resturant.itemList;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/8/17.
 */

public class ResturantListModel {
    String Id,Restaurant,ImageUrl,Address,Rating,Cuisine,BackgroundImage;
    ArrayList<String> resturantListModels;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getRestaurant() {
        return Restaurant;
    }

    public void setRestaurant(String restaurant) {
        Restaurant = restaurant;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getCuisine() {
        return Cuisine;
    }

    public void setCuisine(String cuisine) {
        Cuisine = cuisine;
    }

    public String getBackgroundImage() {
        return BackgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        BackgroundImage = backgroundImage;
    }

    public ArrayList<String> getResturantListModels() {
        return resturantListModels;
    }

    public void setResturantListModels(ArrayList<String> resturantListModels) {
        this.resturantListModels = resturantListModels;
    }
}
