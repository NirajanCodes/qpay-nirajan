package net.qpaysolutions.QPay.Resturant.itemMenu;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.Decimalformate;

import java.util.ArrayList;


/**
 * Created by deadlydragger on 3/9/17.
 */

public class MenuListAdapter extends RecyclerView.Adapter<MenuListViewHolder> {
    MenuItemFragment context;
    ArrayList<MenuListModel> listModels;



    public MenuListAdapter(MenuItemFragment context, ArrayList<MenuListModel> listModels) {
        this.context = context;
        this.listModels = listModels;


    }

    @Override
    public MenuListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item_list,parent,false);
        MenuListViewHolder menuListViewHolder = new MenuListViewHolder(view,context,listModels);
        return menuListViewHolder;
    }

    @Override
    public void onBindViewHolder(final MenuListViewHolder holder, final int position) {
        try {
            final int[] count = {0};
            context.setIcon(holder.imgUrl,listModels.get(position).getImageUrl());
            holder.item_name.setText(listModels.get(position).getItemName());
            holder.item_price.setText("NPR "+new Decimalformate().decimalFormate(String.valueOf(listModels.get(position).getRate())));
            holder.item_description.setText(listModels.get(position).getDescription());
            holder.item_cart.setText(context.getCartCount(listModels.get(position).getItemId()));
            holder.add_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count[0]++;
                    holder.item_cart.setText(String.valueOf(count[0]));

                    if (count[0] == 1) {
                        context.inSertCartList(listModels.get(position).getCategoryId(), listModels.get(position).getDescription(), listModels.get(position).getImageUrl(), listModels.get(position).getItemId(), listModels.get(position).getItemName(), listModels.get(position).getRate(), count[0]);
                    } else if (count[0] > 1) {

                        context.upDateCart(count[0],listModels.get(position).getItemId());
                    }
                    context.totalItemCart();
                }
            });
            holder.remove_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    int positionRemove = getAdapterPosition();
                    count[0]--;
                    if (count[0] <0){
                        count[0] =0;
//                    menuItemFragment.setCartLayout(false);
                    }
                    if (count[0] > 0) {
                        context.upDateCart(count[0],listModels.get(position).getItemId());
                        holder.item_cart.setText(String.valueOf(count[0]));
//                    menuItemFragment.setCartLayout(true);
                    } else if (count[0] == 0) {
                        context.removeCartList(listModels.get(position).getItemId());
                        holder.item_cart.setText(String.valueOf(0));
//                    menuItemFragment.setCartLayout(false);
                    }
                    context.totalItemCart();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }

}
