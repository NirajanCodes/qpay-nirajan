package net.qpaysolutions.QPay.Resturant.itemList;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Resturant.itemMenu.MenuItemActivity;

import java.util.ArrayList;
/**
 * Created by deadlydragger on 3/8/17.
 */
public class ResturantListAdapter extends RecyclerView.Adapter<ResturantListViewHolder> {
    ArrayList<ResturantListModel> listModelArrayList;
    Context context;
    ArrayList<ResturantListModel> origMerchantMenuInfo;
    public ResturantListAdapter(ArrayList<ResturantListModel> listModelArrayList, Context context) {
        this.listModelArrayList = listModelArrayList;
        this.context = context;
    }
    @Override
    public ResturantListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.resturant_list_item,parent,false);
        ResturantListViewHolder resturantListViewHolder = new ResturantListViewHolder(view);
        return resturantListViewHolder;
    }
    @Override
    public void onBindViewHolder(ResturantListViewHolder holder, final int position) {
        ((RestaurantListActivity)context).setIcon(holder.list_image,listModelArrayList.get(position).getImageUrl());
        holder.category_opening_hour.setText(listModelArrayList.get(position).getResturantListModels().get(0)+ " "+listModelArrayList.get(position).getResturantListModels().get(1));
        holder.category_food_type.setText(listModelArrayList.get(position).getCuisine());
        holder.category_item_name.setText(listModelArrayList.get(position).getRestaurant());
        holder.category_rating.setText(listModelArrayList.get(position).getRating());
        holder.category_item_location.setText(listModelArrayList.get(position).getAddress());
        holder.layout_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MenuItemActivity.class);
                intent.putExtra("id", listModelArrayList.get(position).getId());
                              /*  intent.putExtra("icon",categoryModelArrayList.get(position).getIcon());
                                intent.putExtra("background",categoryModelArrayList.get(position).getBackgroundImage());*/
                intent.putExtra("imgUrl", listModelArrayList.get(position).getImageUrl());
                intent.putExtra("name", listModelArrayList.get(position).getRestaurant());
                intent.putExtra("Address", listModelArrayList.get(position).getAddress());
                context.startActivity(intent);

            }
        });
    }
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ResturantListModel> results = new ArrayList<>();
                if (origMerchantMenuInfo == null) origMerchantMenuInfo = listModelArrayList;
                if (constraint != null) {
                    if (origMerchantMenuInfo != null & origMerchantMenuInfo.size() > 0) {
                        for (final ResturantListModel g : origMerchantMenuInfo) {
                            if (g.getRestaurant().toLowerCase().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listModelArrayList = (ArrayList) results.values;
               notifyDataSetChanged();
            }
        };
    }
    @Override
    public int getItemCount() {
        return listModelArrayList.size();
    }
}
