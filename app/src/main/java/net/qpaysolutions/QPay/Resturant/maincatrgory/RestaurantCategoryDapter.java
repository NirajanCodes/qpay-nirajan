package net.qpaysolutions.QPay.Resturant.maincatrgory;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Resturant.ResturantActivity;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/8/17.
 */

public class RestaurantCategoryDapter extends RecyclerView.Adapter<ResturantCatrgoryViewHolder> {
    int height;
    ArrayList<ResturantCategoryModel> resturantCategoryModels;
    Context context;

    public RestaurantCategoryDapter(Context context, ArrayList<ResturantCategoryModel> resturantCategoryModels, int height) {
        this.context = context;
        this.resturantCategoryModels = resturantCategoryModels;
        this.height=height;
    }

    @Override
    public ResturantCatrgoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        ResturantCatrgoryViewHolder resturantCatrgoryViewHolder = new ResturantCatrgoryViewHolder(view);
        return resturantCatrgoryViewHolder;
    }

    @Override
    public void onBindViewHolder(ResturantCatrgoryViewHolder holder, int position) {
        holder.background_resturant.getLayoutParams().height = (int) ((height/2)-1*context.getResources().getDisplayMetrics().density);
        holder.background_resturant.requestLayout();
        holder.category_name.setText(resturantCategoryModels.get(position).getCategory());
        if (!resturantCategoryModels.get(position).getNoOfRestro().equals("0")){
            holder.noOrRestro.setText(resturantCategoryModels.get(position).getNoOfRestro());
        }else {
            holder.noOrRestro.setVisibility(View.GONE);
        }

        ((ResturantActivity) context).setIcon(holder.icon_resturant, resturantCategoryModels.get(position).getIcon());
        ((ResturantActivity)context).setBackground(resturantCategoryModels.get(position).getBackgroundImage(),holder.background_resturant);



    }

    @Override
    public int getItemCount() {
        return resturantCategoryModels.size();
    }
}
