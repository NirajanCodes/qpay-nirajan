package net.qpaysolutions.QPay.Resturant.checkout;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 3/13/17.
 */

public class CheckOutActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView title;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title = (TextView) toolbar.findViewById(R.id.title);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_checkout,new CheckOutFragment())
                .commit();
    }
    public void setToolbarTitle(String toolbarTitle){
        title.setText(toolbarTitle);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
