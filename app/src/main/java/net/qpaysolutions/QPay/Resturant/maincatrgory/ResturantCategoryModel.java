package net.qpaysolutions.QPay.Resturant.maincatrgory;

/**
 * Created by deadlydragger on 3/8/17.
 */

public class ResturantCategoryModel {
    String Id,Category,BackgroundImage,Icon,noOfRestro;

    public String getNoOfRestro() {
        return noOfRestro;
    }

    public void setNoOfRestro(String noOfRestro) {
        this.noOfRestro = noOfRestro;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getBackgroundImage() {
        return BackgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        BackgroundImage = backgroundImage;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }
}
