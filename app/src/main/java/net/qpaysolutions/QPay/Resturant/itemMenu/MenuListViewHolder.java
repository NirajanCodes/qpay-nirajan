package net.qpaysolutions.QPay.Resturant.itemMenu;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/9/17.
 */

public class MenuListViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
    ImageView imgUrl;
    TextView item_name, item_price, item_description, item_cart;
    Button add_item, remove_item;
    int count = 0;
    ArrayList<MenuListModel> menuListModels;
    MenuItemFragment menuItemFragment;
    String TAG = "dinesh";

    public MenuListViewHolder(View itemView, MenuItemFragment menuItemFragment, ArrayList<MenuListModel> menuListModels) {
        super(itemView);
        this.menuItemFragment = menuItemFragment;
        this.menuListModels = menuListModels;
        imgUrl = (ImageView) itemView.findViewById(R.id.imgUrl);
        item_name = (TextView) itemView.findViewById(R.id.item_name);
        item_price = (TextView) itemView.findViewById(R.id.item_price);
        item_description = (TextView) itemView.findViewById(R.id.item_description);
        add_item = (Button) itemView.findViewById(R.id.add_item);
        item_cart = (TextView) itemView.findViewById(R.id.item_cart);
        remove_item = (Button) itemView.findViewById(R.id.remove_item);
    /*    add_item.setOnClickListener(this);
        remove_item.setOnClickListener(this);*/
    }

  /*  @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_item:
                count++;
                item_cart.setText(String.valueOf(count));

                int position = getAdapterPosition();

                if (count == 1) {
                    menuItemFragment.inSertCartList(menuListModels.get(position).getCategoryId(), menuListModels.get(position).getDescription(), menuListModels.get(position).getImageUrl(), menuListModels.get(position).getItemId(), menuListModels.get(position).getItemName(), menuListModels.get(position).getRate(), count);
                } else if (count > 1) {

                    menuItemFragment.upDateCart(count,menuListModels.get(position).getItemId());
                }
                Log.d(TAG, "onClick: " + position);
                menuItemFragment.totalItemCart();
                break;
            case R.id.remove_item:
                int positionRemove = getAdapterPosition();
                count--;
                if (count <0){
                    count=0;
//                    menuItemFragment.setCartLayout(false);
                }
                if (count > 0) {
                    menuItemFragment.upDateCart(count,menuListModels.get(positionRemove).getItemId());
                    item_cart.setText(String.valueOf(count));
//                    menuItemFragment.setCartLayout(true);
                } else if (count == 0) {
                    menuItemFragment.removeCartList(menuListModels.get(positionRemove).getItemId());
                    item_cart.setText(String.valueOf(0));
//                    menuItemFragment.setCartLayout(false);
                }
                menuItemFragment.totalItemCart();
                break;
        }
    }*/
}
