package net.qpaysolutions.QPay.Resturant.itemMenu;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import net.qpaysolutions.QPay.R;
import java.util.ArrayList;
/**
 * Created by deadlydragger on 3/9/17.
 */
public class MenuCategoriesAdapter extends RecyclerView.Adapter<MenuCategoriesViewHolder> {
    ArrayList<MenuCategoriesModel> menuCategoriesModels;
    MenuItemFragment context;
    int selected_position = 7;

    public MenuCategoriesAdapter(ArrayList<MenuCategoriesModel> menuCategoriesModels, MenuItemFragment context) {
        this.menuCategoriesModels = menuCategoriesModels;
        this.context = context;
    }
    @Override
    public MenuCategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_menu, parent, false);
        MenuCategoriesViewHolder category_menu = new MenuCategoriesViewHolder(view,context.getContext());
        return category_menu;
    }
    @Override
    public void onBindViewHolder(final  MenuCategoriesViewHolder holder, final int position) {

        holder.name.setText(menuCategoriesModels.get(position).getCategory());
        context.setIcon(holder.icon, menuCategoriesModels.get(position).getIconUrl());

        MenuCategoriesModel item = menuCategoriesModels.get(position);

        if(selected_position == position){
            holder.icon.setColorFilter(ContextCompat.getColor(context.getContext(),R.color.colorPrimary));
            holder.name.setTextColor(ContextCompat.getColor(context.getContext(),R.color.colorPrimary));
        }else{
            holder.icon.setColorFilter(ContextCompat.getColor(context.getContext(),R.color.color_thirtary_text));
            holder.name.setTextColor(ContextCompat.getColor(context.getContext(),R.color.color_thirtary_text));
        }

        holder.menu_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.icon.setColorFilter(ContextCompat.getColor(context.getContext(),R.color.colorPrimary));
                holder.name.setTextColor(ContextCompat.getColor(context.getContext(),R.color.colorPrimary));
                // Updating old as well as new positions
                notifyItemChanged(selected_position);
                selected_position = position;
                notifyItemChanged(selected_position);
                // Do your another stuff for your onClick
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuCategoriesModels.size();
    }
}
