package net.qpaysolutions.QPay.Resturant.resturantPayment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Billpayment.BillpayCardAdapter;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Resturant.ResturantActivity;
import net.qpaysolutions.QPay.Resturant.checkout.CheckOutActivity;
import net.qpaysolutions.QPay.Resturant.itemMenu.MenuListModel;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/15/17.
 */

public class ResturantPaymentFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    SharedPreferences sharedPreferences;
    String FILE = "restaurant_menu";
    String data;
    String TAG = "dinesh";
    String appId, custId;
    QPayCustomerDatabase QPayCustomerDatabase;
    TextView qpay_balance, subtotal, vat, service_charge, grand_total, total_item, name_restaurant, location_restaurant, delivery_location;
    public static final String MY_PREFS_NAME = "Mybalance";
    SharedPreferences sharePreference_balance, sharedPreferences_location;
    String available_balance = "";
    String total_vat, total_service_charge, total_subtotal, total_grand, location_delivery;
    LinearLayout pay_through_qpay;
    QPayProgressDialog qPayProgressDialog;
    ArrayList<MenuListModel> menuListModels;
    String Id, name, Address, imgUrl;
    ImageView image_restaurant;
    boolean isDialogOpen = false;
    String RecipientAddress;
    ListView cardBillPay;
    BillpayCardAdapter billpayCardAdapter;
    Handler handler = new Handler();
    int counter = 0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.resturant_payment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharePreference_balance = getActivity().getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        sharedPreferences = getActivity().getSharedPreferences(FILE, Context.MODE_PRIVATE);
        sharedPreferences_location = getActivity().getSharedPreferences("deliver", Context.MODE_PRIVATE);
        location_delivery = sharedPreferences_location.getString("location", "1");
        ((CheckOutActivity) getActivity()).setToolbarTitle("Payment Process");
        data = sharedPreferences.getString("data", "");
        total_vat = sharedPreferences.getString("total_vat", "0.00");
        total_service_charge = sharedPreferences.getString("total_service_charge", "0.00");
        total_subtotal = sharedPreferences.getString("total_subtotal", "0.00");
        total_grand = sharedPreferences.getString("total_grand", "0.00");
        available_balance = BalancePref.getAvailableAmount();
        Id = sharedPreferences.getString("id", "");
        name = sharedPreferences.getString("name", "");
        Address = sharedPreferences.getString("address", "");
        imgUrl = sharedPreferences.getString("imgUrl", "");
        RecipientAddress = getArguments().getString("RecipientAddress");
        Log.d(TAG, "onViewCreated: " + data);
        qpay_balance = (TextView) view.findViewById(R.id.qpay_balance);
        subtotal = (TextView) view.findViewById(R.id.subtotal);
        vat = (TextView) view.findViewById(R.id.vat);
        service_charge = (TextView) view.findViewById(R.id.service_charge);
        grand_total = (TextView) view.findViewById(R.id.grand_total);
        pay_through_qpay = (LinearLayout) view.findViewById(R.id.pay_through_qpay);
        total_item = (TextView) view.findViewById(R.id.total_item);
        image_restaurant = (ImageView) view.findViewById(R.id.image_restaurant);
        name_restaurant = (TextView) view.findViewById(R.id.name_restaurant);
        location_restaurant = (TextView) view.findViewById(R.id.location_restaurant);
        delivery_location = (TextView) view.findViewById(R.id.delivery_location);
        delivery_location.setText(location_delivery);
        subtotal.setText(total_subtotal);
        vat.setText(total_vat);
        service_charge.setText(total_service_charge);
        grand_total.setText(total_grand);

        pay_through_qpay.setOnClickListener(this);
        total_item.setText(String.valueOf(menuListModels.size()));
        setIcon(image_restaurant, imgUrl);
        name_restaurant.setText(name);
        location_restaurant.setText(Address);
        cardBillPay = (ListView) view.findViewById(R.id.payment_restaurant_list);
        SQLiteDatabase db = QPayCustomerDatabase.getWritableDatabase();
        Cursor cursor = db.rawQuery(" select * from Profile where act_flag ='V' ", null);
        billpayCardAdapter = new BillpayCardAdapter(getContext(), cursor);
        cardBillPay.setAdapter(billpayCardAdapter);
        cardBillPay.setOnItemClickListener(this);
//        new Getbalance().execute();

        String balance = "AVAILABLE BALANCE :" + "<b>" + "NPR " + new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()) + "</b> ";
        qpay_balance.setText(Html.fromHtml(balance));
    }

    public void setIcon(ImageView icon, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()

                    .load(url)
                    .error(R.drawable.ic_dummy_item)
                    .placeholder(R.drawable.ic_dummy_item)
                    .into(icon);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        appId = QPayCustomerDatabase.getKeyAppId();
        custId = QPayCustomerDatabase.getCustomerID();
        menuListModels = QPayCustomerDatabase.getAllReminders();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_through_qpay:
                if (isDialogOpen == false) {
                    try {
                        if (GeneralPref.getAllowBonus()) {
                            isDialogOpen = true;
                            double available = Double.parseDouble(available_balance);
                            double amount = Double.parseDouble(total_grand);
                            if (available > amount) {
                                confirmPaythroughQpay();
                            } else {
                                isDialogOpen = false;
                                Utility.custumdialogfailureInsufficient("Insufficient Balance!", "Insufficient QPay balance for this transaction. Please try with cards.", getActivity());
                            }
                        } else {
                            custumdialogfailure();
                        }
                    } catch (Exception e) {
                        custumdialogfailure("Restaurant Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");
                    }
                }
                break;
        }
    }

    public void custumdialogfailure() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_custom_dialog_bill_payment_feature_unavailable, null);
        final TextView sure = (TextView) v.findViewById(R.id.sure);
        final TextView conttent_text = (TextView) v.findViewById(R.id.content_text);
        conttent_text.setText("Please perform a sales transaction or add SCT card in order to use your sign-up bonus .");
        sure.setText("Fund Transfer Unavailable!");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }


    public void custumdialogSucess(String display_title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView title = (TextView) v.findViewById(R.id.title);
        title.setText(display_title);

        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
             /*   startActivity(new Intent(getActivity(), ResturantActivity.class));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().finish();
*/
                Intent intent = new Intent(getActivity(), ResturantActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public void confirmPaythroughQpay() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_payingthrough_qpay, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        dialog_balance.setText("Do you want to Pay from your QPay Account?");
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        v.findViewById(R.id.cancel_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
            }
        });

        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Restaurantpayment(null, null, null, "1", 66).execute();
                dialog.dismiss();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (280 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String last_4 = QPayCustomerDatabase.getLastFourForBill(id);
        String card_token = QPayCustomerDatabase.getCardType(String.valueOf(id));
        getPinBillpay(last_4, card_token, "0", id);
    }

    public void getPinBillpay(final String last_4, final String card_token, final String wallet, final long id) {
        final Dialog dialog = new Dialog(getActivity(), R.style.UpdownDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custum_dialog_entrcardpin);
        final EditText aut_editext = (EditText) dialog.findViewById(R.id.enter_pin);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.proceed_pin_enter);
        TextView dialogcancel = (TextView) dialog.findViewById(R.id.cancel_pin_enter);
        if (aut_editext.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String text_from_editext = aut_editext.getText().toString();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(aut_editext.getWindowToken(), 0);
                    if (text_from_editext.length() == 4) {
                        new Restaurantpayment(text_from_editext, last_4, card_token, wallet, id).execute();
                        dialog.dismiss();
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } else {
                        new Utility().showSnackbar(v, "Please Insert Valid Pin Number");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        });
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (320 * density);
        lp.height = (int) (340 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public class Restaurantpayment extends AsyncTask<String, String, String> {
        String pin, last4, cardToken, wallet;
        long id;

        public Restaurantpayment(String pin, String last4, String cardToken, String wallet, long id) {

            this.pin = pin;
            this.last4 = last4;
            this.cardToken = cardToken;
            this.wallet = wallet;
            this.id = id;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String restaurantpayment = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appId", appId);
                jsonObject.put("custId", custId);
                jsonObject.put("orderId", data);
                jsonObject.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                jsonObject.put("isWallet", wallet);
                jsonObject.put("cardToken", cardToken);
                jsonObject.put("pin", pin);
                jsonObject.put("last4", last4);
                Log.d(TAG, "doInBackground: " + jsonObject);
                restaurantpayment = networkAPI.sendHTTPData(Constants.RESTURANT_PAYMENT, jsonObject);
                Log.d(TAG, "doInBackground: " + restaurantpayment);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return restaurantpayment;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject RestaurantPaymentResult = jsonObject.getJSONObject("RestaurantPaymentResult");
                boolean success = RestaurantPaymentResult.getBoolean("success");
                String status = RestaurantPaymentResult.getString("status");
                if (success) {
                    JSONArray data = RestaurantPaymentResult.getJSONArray("data");

                    if (status.equals("CN")) {
                        QPayCustomerDatabase.deletCard(id);
                        qPayProgressDialog.dismiss();
                        custumdialogfailure("Card Not Found!", "Your card details could not be found on our system. Your card will be deleted from application. Please re-register your card. Thank You");

                    } else {
                        JSONObject jsonObject1 = data.getJSONObject(0);
                        String Crrn = jsonObject1.getString("Crrn");
                        String Stan = jsonObject1.getString("Stan");

                        if (pin != null && !pin.isEmpty() && pin.length() > 1) {

                            runtimerResturantPay(Crrn, Stan);
                        } else {
                            qPayProgressDialog.dismiss();
                            new Ackclass(appId, custId, Stan, Crrn).execute();
                            custumdialogSucess("Restaurant Payment Success!", "You have successfully paid your restaurant bill for NPR " + total_grand + ".");
                        }

                    }
                } else {
                    qPayProgressDialog.dismiss();
                    custumdialogfailure("Restaurant Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");
                }
            } catch (Exception e) {
                qPayProgressDialog.dismiss();
                Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", getActivity());

                e.printStackTrace();
            }
        }
    }

    public void runtimerResturantPay(final String crrn, final String stan) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                counter++;
                new CheckStatusResturant(stan, crrn).execute();
            }
        }, 5000);
    }

    public class CheckStatusResturant extends AsyncTask<String, String, String> {
        String stan, crrn;

        public CheckStatusResturant(String stan, String crrn) {
            this.stan = stan;
            this.crrn = crrn;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject_blc = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            String result = null;

            try {
                jsonObject_blc.put("appId", appId);
                jsonObject_blc.put("custId", custId);
                jsonObject_blc.put("stan", stan);
                jsonObject_blc.put("crrn", crrn);
                Log.d(Utility.TAG, "restaurant bill : " + jsonObject_blc.toString());
                result = networkAPI.sendHTTPData(Constants.CHECK_STATUS_RETURANT, jsonObject_blc);
                Log.d(TAG, "doInBackground: " + result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("CheckRestaurantPaymentStatusResult");
                boolean success = jsonObject1.getBoolean("success");
                if (success) {
                    JSONArray data = jsonObject1.getJSONArray("data");
                    JSONObject jsonObject2 = data.getJSONObject(0);
                    String resp_code = jsonObject2.getString("RespCode");
                    if (resp_code.equals("00")) {
                        new Ackclass(appId, custId, stan, crrn).execute();
                        String DisplayMsg = jsonObject2.getString("DisplayMsg");
                        custumdialogSucess("Restaurant Payment Success!", DisplayMsg);

                    } else if (resp_code.equals("LO")) {
                        if (counter < 10) {
                            runtimerResturantPay(crrn, stan);
                        } else {
                            new Ackclass(appId, custId, stan, crrn).execute();
                            custumdialogfailure("Restaurant Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");

                        }

                    } else {

                        new Ackclass(appId, custId, stan, crrn).execute();
                        custumdialogfailure("Restaurant Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");


                    }
                } else {
                    new Ackclass(appId, custId, stan, crrn).execute();
                    custumdialogfailure("Restaurant Payment Failure!", "We are unable to complete your transaction at this time. Please try again later.");

                }


            } catch (Exception e) {
                e.printStackTrace();
                try {

                    Utility.custumdialogWarning("No Response!", "You lost your internet connection while processing your transaction. This transaction may have been completed successfully. Please look for sms and notification for transaction details.", getActivity());


                } catch (Exception e1) {
                    e1.printStackTrace();
                }


            }


        }
    }

    public void custumdialogfailure(String title, String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.custum_dialog_invitefriends_fail, null);
        final TextView dialog_balance = (TextView) v.findViewById(R.id.dialog_msg);
        final TextView dialog_title = (TextView) v.findViewById(R.id.title);
        dialog_title.setText(title);
        dialog_balance.setText(number);
        builder.setView(v);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        v.findViewById(R.id.proceed_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isDialogOpen = false;
                Intent intent = new Intent(getActivity(), ResturantActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();

            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        float density = getResources().getDisplayMetrics().density;
        lp.width = (int) (300 * density);
        lp.height = (int) (320 * density);
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
    }

    public class Ackclass extends AsyncTask<String, String, String> {
        String app_id, cust_id, stan, crrn;

        public Ackclass(String app_id, String cust_id, String stan, String crrn) {
            this.app_id = app_id;
            this.cust_id = cust_id;
            this.stan = stan;
            this.crrn = crrn;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... params) {
            String ack_test = null;
            NetworkAPI networkAPIBillPay = new NetworkAPI();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("stan", stan);
                jsonObject.put("crrn", crrn);
                ack_test = networkAPIBillPay.sendHTTPData(Constants.ACKN, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return ack_test;
        }
    }
}
