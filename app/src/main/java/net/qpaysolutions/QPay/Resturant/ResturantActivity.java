package net.qpaysolutions.QPay.Resturant;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.LatLngPref;

import java.util.List;
import java.util.Locale;

/**
 * Created by deadlydragger on 3/6/17.
 */

public class ResturantActivity extends AppCompatActivity {
    Toolbar toolbaruni;
    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resturant);
        toolbaruni = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbaruni);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textView = (TextView) toolbaruni.findViewById(R.id.title);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_resturant, new ResturantFragment())
                .commit();



    }

    public void setToolbarTitle(String toolbarTitle) {
        textView.setText(toolbarTitle);
    }

    public void setIcon(ImageView icon, String url) {
        if (!url.isEmpty()) {
            Picasso.get()
                    .load(url)
                    .into(icon);
        }

    }

    public void setBackground(String url, final LinearLayout linearLayout) {
        if (!url.isEmpty()) {
            Picasso.get()
                    .load(url).into(new Target() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    linearLayout.setBackground(new BitmapDrawable(bitmap));
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }



                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ResturantActivity.this, MainActivity.class);
        intent.putExtra("Check",6);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void setAdressRestauant() {
        try {
            SharedPreferences sharedPreferences;
            String File = "deliver";
            sharedPreferences = getSharedPreferences(File, MODE_PRIVATE);
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(ResturantActivity.this, Locale.getDefault());

            addresses = geocoder.getFromLocation(Double.valueOf(LatLngPref.getLat()), Double.valueOf(LatLngPref.getLng()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("location", address + "," + city);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class LocationDelivery extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            setAdressRestauant();
            return null;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
