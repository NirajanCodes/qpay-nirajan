package net.qpaysolutions.QPay.Resturant.checkout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Resturant.ResturantActivity;
import net.qpaysolutions.QPay.Resturant.itemMenu.MenuListModel;
import net.qpaysolutions.QPay.Resturant.location.DeliveryLocationActivity;
import net.qpaysolutions.QPay.Resturant.resturantPayment.ResturantPaymentFragment;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/13/17.
 */

public class CheckOutFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    ImageView image_restaurant;
    TextView name_restaurant, location_restaurant, clear_all, subtotal, vat_percent, service_percent, name_delivery, location,phone_no;
    SharedPreferences sharedPreferences;
    String FILE = "restaurant_menu";
    String KEY = "restaurant_menu";
    String TAG = "dinesh";
    String Id, name, Address, imgUrl;
    ListView cartList;
    Cursor todoCursor;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private SQLiteDatabase db;
    CheckOutListCursorAdapter cartListCursorAdapter;
    String VAT = "Vat";
    String SERVICE_CHARGE = "ServiceCharge";
    float Vat, ServiceCharge;
    TextView vat, service_charge, grand_total;
    double sub_total, charge, vat_total;
    String name_user, phone;
    LinearLayout add_location;
    SharedPreferences sharedPreferences_location;
    String location_delivery;
    Button proceed;
    double total, RecipientLat, RecipientLng;
    CheckBox onLocationCheck;
    boolean isOnLocation = false;
    String RecipientAddress;
    String CustId, AppId;
    QPayProgressDialog qPayProgressDialog;
    ArrayList<MenuListModel> menuListModels;
    String total_vat, total_service_charge, total_subtotal, total_grand;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getReadableDatabase();
        QPayCustomerDatabase.getWritableDatabase();
        db = QPayCustomerDatabase.getWritableDatabase();
        name_user = QPayCustomerDatabase.getFirstName() + " " + QPayCustomerDatabase.lastName();
        phone = QPayCustomerDatabase.getCustomerPhone();
        CustId = QPayCustomerDatabase.getCustomerID();
        AppId = QPayCustomerDatabase.getKeyAppId();
        todoCursor = db.rawQuery("SELECT *  FROM cart ORDER BY _id DESC", null);//order by date desc
        Log.d(TAG, "onCreate: " + QPayCustomerDatabase.getSubTotal());
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((CheckOutActivity) getActivity()).setToolbarTitle("Order Summary");
        sharedPreferences = getActivity().getSharedPreferences(FILE, Context.MODE_PRIVATE);
        Id = sharedPreferences.getString("id", null);
        name = sharedPreferences.getString("name", null);
        Address = sharedPreferences.getString("address", null);
        imgUrl = sharedPreferences.getString("imgUrl", null);
        Vat = sharedPreferences.getFloat(VAT, 0);
        ServiceCharge = sharedPreferences.getFloat(SERVICE_CHARGE, 0);
        image_restaurant = (ImageView) view.findViewById(R.id.image_restaurant);
        name_restaurant = (TextView) view.findViewById(R.id.name_restaurant);
        location_restaurant = (TextView) view.findViewById(R.id.location_restaurant);
        cartList = (ListView) view.findViewById(R.id.cartList);
        clear_all = (TextView) view.findViewById(R.id.clear_all);
        subtotal = (TextView) view.findViewById(R.id.subtotal);
        vat = (TextView) view.findViewById(R.id.vat);
        grand_total = (TextView) view.findViewById(R.id.total);
        service_charge = (TextView) view.findViewById(R.id.serviceCharge);
        vat_percent = (TextView) view.findViewById(R.id.vat_percent);
        service_percent = (TextView) view.findViewById(R.id.servicecharge_percent);
        name_delivery = (TextView) view.findViewById(R.id.name);
        location = (TextView) view.findViewById(R.id.location);
        add_location = (LinearLayout) view.findViewById(R.id.add_location);
        proceed = (Button) view.findViewById(R.id.proceed);
        onLocationCheck = (CheckBox) view.findViewById(R.id.onLocationCheck);
        phone_no=(TextView)view.findViewById(R.id.phone);
        onLocationCheck.setOnCheckedChangeListener(this);
        proceed.setOnClickListener(this);
        add_location.setOnClickListener(this);
        name_delivery.setText(name_user);
        vat_percent.setText("Vat " + (int) Vat + "%");
        service_percent.setText("Service Charge " + (int) ServiceCharge + "%");
        clear_all.setOnClickListener(this);
        setIcon(image_restaurant, imgUrl);
        name_restaurant.setText(name);
        location_restaurant.setText(Address);
        cartListCursorAdapter = new CheckOutListCursorAdapter(getContext(), todoCursor, CheckOutFragment.this);
        cartList.setAdapter(cartListCursorAdapter);
        setSubtotal();
        cartList.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
               /* v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;*/
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        setListViewHeightBasedOnChildren(cartList);
        sharedPreferences_location = getActivity().getSharedPreferences("deliver", Context.MODE_PRIVATE);
        location_delivery = sharedPreferences_location.getString("location", "");
        location.setText(location_delivery);
        phone_no.setText(phone);
        Log.d(TAG, "onViewCreated: "+location_delivery+" /n "+phone);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_to_checkout, container, false);
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Nullable

    public void setIcon(ImageView icon, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()

                    .load(url)
                    .error(R.drawable.ic_atm_default_icon)
                    .placeholder(R.drawable.ic_atm_default_icon)
                    .into(icon);
        }

    }

    public void deletItem(String id) {
        QPayCustomerDatabase.deletCart(id);
        cartListCursorAdapter.getCursor().requery();
        cartListCursorAdapter.notifyDataSetChanged();
        Utility.setDynamicHeight(cartList);
        setSubtotal();
        if (cartListCursorAdapter.getCursor().getCount() > 0) {

        } else {
            startActivity(new Intent(getActivity(), ResturantActivity.class));
            getActivity().finish();
        }
    }

    public void setSubtotal() {
        sub_total = QPayCustomerDatabase.getSubTotal();
        subtotal.setText(String.valueOf(new Decimalformate().decimalFormate(String.valueOf(sub_total))));
//        totalVat();
        serViceCharge();
        grandTotal();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear_all:
                QPayCustomerDatabase.deletCartAll();
                startActivity(new Intent(getActivity(), ResturantActivity.class));
                getActivity().finish();
                break;
            case R.id.add_location:
                startActivity(new Intent(getActivity(), DeliveryLocationActivity.class));
                break;
            case R.id.proceed:
                menuListModels = QPayCustomerDatabase.getAllReminders();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("total_vat", new Decimalformate().decimalFormate(String.valueOf(vat_total)));
                editor.putString("total_subtotal", new Decimalformate().decimalFormate(String.valueOf(sub_total)));
                editor.putString("total_service_charge", new Decimalformate().decimalFormate(String.valueOf(charge)));
                editor.putString("total_grand", new Decimalformate().decimalFormate(String.valueOf(total)));
                editor.commit();
                Log.d(TAG, "onClick: " + vat_total + sub_total + charge + total);
                new Postorder(menuListModels).execute();
                break;

        }
    }

    public void totalVat(double service) {
        double vat_prcnt=sub_total+service;
        vat_total = vat_prcnt * (Vat / 100.0);
        vat.setText(new Decimalformate().decimalFormate(String.valueOf(vat_total)));

    }

    public void grandTotal() {
        total = sub_total + charge + vat_total;
        grand_total.setText(new Decimalformate().decimalFormate(String.valueOf(total)));
    }

    public void serViceCharge() {
        charge = sub_total * (ServiceCharge / 100.0);
        service_charge.setText(String.valueOf(charge));
        totalVat(charge);
    }

    @Override
    public void onResume() {
        sharedPreferences_location = getActivity().getSharedPreferences("deliver", Context.MODE_PRIVATE);
        name_user=sharedPreferences_location.getString("name","");
        phone=sharedPreferences_location.getString("phone","");
        RecipientAddress=sharedPreferences_location.getString("streed_address","");

        location_delivery = sharedPreferences_location.getString("location", "");
        location.setText(location_delivery);
       phone_no.setText(phone);

        super.onResume();
        Log.d(TAG, "onResume: ");
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.onLocationCheck:
                if (isChecked) {
                    isOnLocation = true;
                }
                break;
        }
    }

    public class Postorder extends AsyncTask<String, String, String> {
        ArrayList<MenuListModel> menuListModels;

        public Postorder(ArrayList<MenuListModel> menuListModels) {
            this.menuListModels = menuListModels;
        }

        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String postorder = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Id", Id);
                jsonObject.put("CustId", CustId);
                jsonObject.put("AppId", AppId);
                jsonObject.put("SubTotal", sub_total);
                jsonObject.put("ServiceCharge", charge);
                jsonObject.put("Vat", vat_total);
                jsonObject.put("Discount", 0.0);
                jsonObject.put("Total", total);
                jsonObject.put("Lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject.put("Lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                jsonObject.put("TermId", null);
                jsonObject.put("IsCust", true);
                jsonObject.put("IsAtRestaurant", isOnLocation);
                jsonObject.put("BranchId", null);
                jsonObject.put("RecipientName", name_user);
                jsonObject.put("RecipientAddress", /*RecipientAddress*/location_delivery);
                jsonObject.put("RecipientLat", RecipientLat);
                jsonObject.put("RecipientLng", RecipientLng);
                jsonObject.put("RecipientPhone", phone);
                JSONArray Details = new JSONArray();
                jsonObject.put("Details", Details);
                for (int i = 0; i < menuListModels.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("ItemId", menuListModels.get(i).getItemId());
                    jsonObject1.put("Rate", menuListModels.get(i).getRate());
                    jsonObject1.put("Quantity", menuListModels.get(i).getCount());
                    jsonObject1.put("Total", menuListModels.get(i).getRate() * menuListModels.get(i).getCount());
                    Details.put(jsonObject1);
                }
                Log.d(TAG, "post Order doInBackground: " + jsonObject);
                postorder = networkAPI.sendHTTPData(Constants.POST_ORDER, jsonObject);
                Log.d(TAG, "doInBackground: " + postorder);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return postorder;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    JSONArray data = jsonObject.getJSONArray("data");

                    JSONObject jsonObject1=data.getJSONObject(0);

                    String value_data = jsonObject1.getString("OrderId");
                    Log.d(TAG, "onPostExecute: " + value_data);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("data", value_data);
                    editor.commit();
                    ResturantPaymentFragment resturantPaymentFragment = new ResturantPaymentFragment();
                  FragmentTransaction fragmentTransaction =  getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fragment_checkout, resturantPaymentFragment);
                    Bundle bundle = new Bundle();
                    bundle.putString("RecipientAddress",RecipientAddress);
                    resturantPaymentFragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }else {
                    new Utility().showSnackbar(cartList,"Unable to post order");
                }
            } catch (Exception e) {
                e.printStackTrace();
                new Utility().showSnackbar(cartList,"Unable to post order");
            }
        }
    }
}


