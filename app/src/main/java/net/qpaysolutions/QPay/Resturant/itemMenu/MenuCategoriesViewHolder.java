package net.qpaysolutions.QPay.Resturant.itemMenu;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 3/9/17.
 */

public class MenuCategoriesViewHolder extends RecyclerView.ViewHolder{
    ImageView icon;
    TextView name;
    Context context;
    LinearLayout menu_click;
    public MenuCategoriesViewHolder(View itemView, final Context context) {
        super(itemView);
        this.context=context;
        icon=(ImageView)itemView.findViewById(R.id.menu_icon);
        name=(TextView)itemView.findViewById(R.id.menu_text);
        menu_click=(LinearLayout)itemView.findViewById(R.id.menu_click);
       /* itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });*/
    }
}
