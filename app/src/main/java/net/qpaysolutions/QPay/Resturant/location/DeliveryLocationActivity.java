package net.qpaysolutions.QPay.Resturant.location;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.CustomerApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
/**
 * Created by deadlydragger on 3/13/17.
 */

public class DeliveryLocationActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, OnMapReadyCallback {
    Toolbar restaurant_toolbar;
    TextView restaurant_text,location_default,my_location;
    private GoogleMap googleMap;
    Button set_Location,proceed;
    SharedPreferences sharedPreferences,sharedPreferences_location;
    String File="deliver";
    String location;
    AutoCompleteTextView search_location;
    EditText name,streed_address,phone_no;
    QPayCustomerDatabase QPayCustomerDatabase;
    LinearLayout details,selection_visibility;
    private static final String TAG = "dinesh";
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyBlENriv112FlxR-oeDrw6QuV2oCFkTpxA";
    String   location_delivery;
    Button use_default;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_main);
        QPayCustomerDatabase = new QPayCustomerDatabase(this);
        sharedPreferences_location = getSharedPreferences("deliver", Context.MODE_PRIVATE);
        location_delivery = sharedPreferences_location.getString("location", "1");
        QPayCustomerDatabase.getReadableDatabase();
        restaurant_toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(restaurant_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        restaurant_text=(TextView)restaurant_toolbar.findViewById(R.id.title);
        location_default=(TextView)findViewById(R.id.location_default);
        set_Location=(Button)findViewById(R.id.set_Location);
        search_location=(AutoCompleteTextView)findViewById(R.id.search_location) ;
        my_location=(TextView)findViewById(R.id.my_location);
        name=(EditText)findViewById(R.id.name);
        use_default=(Button)findViewById(R.id.use_default);
        use_default.setOnClickListener(this);
        streed_address=(EditText)findViewById(R.id.streed_address);
        phone_no=(EditText)findViewById(R.id.phone_no);
        details=(LinearLayout)findViewById(R.id.details);
        selection_visibility=(LinearLayout)findViewById(R.id.selection_visibility);
        proceed=(Button)findViewById(R.id.proceed);
        proceed.setOnClickListener(this);
        set_Location.setOnClickListener(this);
        restaurant_text.setText("Set Delivery Location");
        initilizeMap();
        sharedPreferences=getSharedPreferences(File,MODE_PRIVATE);
        name.setText(QPayCustomerDatabase.getFirstName()+" "+ QPayCustomerDatabase.lastName());
        phone_no.setText(QPayCustomerDatabase.getCustomerPhone());
        if (!location_delivery.equals("1")){
            location_default.setText(location_delivery);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void initilizeMap() {
        try {


            if (googleMap == null) {
                MapFragment mapFragment = (MapFragment) getFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private void onMapSearch(String location) {
        List<Address> addressList = null;

        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            googleMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    public void setAdress(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(DeliveryLocationActivity.this, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            Log.d("dinesh", "setAdress: "+address + "," + city );
            SharedPreferences.Editor editor= sharedPreferences.edit();
            editor.putString("location",address + "," + city );
            editor.putString("RecipientLat",String.valueOf(latitude));
            editor.putString("RecipientLng",String.valueOf(longitude));
            editor.commit();
            my_location.setText(address + "," + city);
            streed_address.setText(address + "," + city);
            if (!location_delivery.equals("1")){
                location_default.setText(location_delivery);
            }else {
                location_default.setText(address + "," + city);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.set_Location:
//                    onBackPressed();
                selection_visibility.setVisibility(View.GONE);
                details.setVisibility(View.VISIBLE);
                break;
            case R.id.proceed:
                SharedPreferences.Editor editor= sharedPreferences.edit();
                editor.putString("phone",phone_no.getText().toString());
                editor.putString("streed_address",streed_address.getText().toString());
                editor.putString("name",name.getText().toString());
                editor.commit();
                onBackPressed();
                break;
            case R.id.use_default:
                SharedPreferences.Editor editorr= sharedPreferences.edit();
                editorr.putString("streed_address",location_default.getText().toString());
                editorr.commit();
                onBackPressed();
                break;
        }
    }
    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:np");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return resultList;
        } catch (IOException e) {
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
        }

        return resultList;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(((CustomerApplication)DeliveryLocationActivity.this.getApplication()).getLat(), ((CustomerApplication)DeliveryLocationActivity.this.getApplication()).getLng())).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);



        search_location.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
        search_location.setOnItemClickListener(this);
        if (googleMap == null) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }
        @Override
        public int getCount() {
            return resultList.size();
        }
        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }
        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = (String) parent.getItemAtPosition(position);
        String[] addressArr = str.split(", ");
        onMapSearch(addressArr[0]);
    }
}
