package net.qpaysolutions.QPay.Resturant.itemMenu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.NearByShop.NearbyShopListActivity;
import net.qpaysolutions.QPay.Resturant.addtocart.CartListActivity;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deadlydragger on 3/9/17.
 */

public class MenuItemFragment extends Fragment implements View.OnClickListener {
    QPayCustomerDatabase QPayCustomerDatabase;
    String AppId, CustId, Id, Address, name, imgUrl;
    QPayProgressDialog qPayProgressDialog;

    RecyclerView category_menu, menu_list;
    MenuCategoriesAdapter menuCategoriesAdapter;
    MenuListAdapter menuListAdapter;
    ArrayList<MenuCategoriesModel> menuCategoriesModels = new ArrayList<>();
    ArrayList<MenuListModel> menuListModels = new ArrayList<>();

    ImageView imgUrl_menu;
    TextView name_menu, location_menu;
    LinearLayout cart_amount, menu_item_empty;
    int position_category;
    SharedPreferences sharedPreferences;
    String FILE = "restaurant_menu";
    String KEY = "restaurant_menu";
    String VAT = "Vat";
    String SERVICE_CHARGE = "ServiceCharge";
    String TAG = "dinesh";
    private SQLiteDatabase db;
    Cursor todoCursor, cart_item;
    TextView txtViewCount;
    FrameLayout frame_text_cart;
    ImageView background_header;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_item_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(FILE, Context.MODE_PRIVATE);
        Id = getArguments().getString("id");
        name = getArguments().getString("name");
        Address = getArguments().getString("Address");
        imgUrl = getArguments().getString("imgUrl");
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id", Id);
        editor.putString("name", name);
        editor.putString("address", Address);
        editor.putString("imgUrl", imgUrl);
        editor.commit();
        new GetMenuAndCategory().execute();
        imgUrl_menu = (ImageView) view.findViewById(R.id.imgUrl);
        name_menu = (TextView) view.findViewById(R.id.name_menu);
        location_menu = (TextView) view.findViewById(R.id.location_menu);
        cart_amount = (LinearLayout) view.findViewById(R.id.cart_amount);
        ((MenuItemActivity) getActivity()).setTitleToolbar(name);
        menu_item_empty = (LinearLayout) view.findViewById(R.id.menu_item_empty);
        name_menu.setText(name);
        location_menu.setText(Address);
        setIcon(imgUrl_menu, imgUrl);

        menu_list = (RecyclerView) view.findViewById(R.id.menu_list);
        category_menu = (RecyclerView) view.findViewById(R.id.category_menu);
        category_menu.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        category_menu.setHasFixedSize(true);
        menu_list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        menu_list.setHasFixedSize(true);
        cart_amount.setOnClickListener(this);
        background_header = (ImageView) view.findViewById(R.id.background_header);
//        setIcon(background_header,imgUrl);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getContext());
        QPayCustomerDatabase.getReadableDatabase();
        AppId = QPayCustomerDatabase.getKeyAppId();
        CustId = QPayCustomerDatabase.getCustomerID();
        db = QPayCustomerDatabase.getWritableDatabase();
        setHasOptionsMenu(true);

    }

    public void inSertCartList(String CategoryId, String Description, String ImageUrl, String ItemId, String ItemName, double Rate, int Count) {
        QPayCustomerDatabase.insertCartItem(CategoryId, Description, ImageUrl, ItemId, ItemName, Rate, Count);

    }

    public void removeCartList(String id) {
        QPayCustomerDatabase.deletCart(id);
    }

    public void upDateCart(int value, String itemId) {
        QPayCustomerDatabase.updateCartCount(value, itemId);
    }

    public String getCartCount(String id) {
        return QPayCustomerDatabase.getCartCount(id);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_addto_card, menu);
        View notificaitons = menu.findItem(R.id.add_to_cart).getActionView();
        txtViewCount = (TextView) notificaitons.findViewById(R.id.txtCount);
        frame_text_cart = (FrameLayout) notificaitons.findViewById(R.id.frame_text_cart);
        LinearLayout cart_list = (LinearLayout) notificaitons.findViewById(R.id.cart_list);
        cart_list.setOnClickListener(this);

    }

    public void totalItemCart() {
        cart_item = db.rawQuery("SELECT *  FROM cart ", null);
        if (cart_item.getCount() > 0) {
            setCartLayout(true);
            txtViewCount.setText(String.valueOf(cart_item.getCount()));
            frame_text_cart.setVisibility(View.VISIBLE);
        } else {
            frame_text_cart.setVisibility(View.GONE);
            setCartLayout(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setCartLayout(boolean value) {
        if (value) {
            cart_amount.setVisibility(View.VISIBLE);
        } else if (value == false) {
            cart_amount.setVisibility(View.GONE);
        }
    }

    public void setIcon(ImageView icon, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get()

                    .load(url)
                    .error(R.drawable.ic_default_items)
                    .placeholder(R.drawable.ic_default_items)
                    .into(icon);
        }
        else {
            icon.setImageResource(R.drawable.ic_default_items);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cart_amount:
                todoCursor = db.rawQuery("SELECT *  FROM cart ORDER BY _id DESC", null);
                if (todoCursor.getCount() > 0) {
                    startActivity(new Intent(getActivity(), CartListActivity.class));
                } else {
                    new Utility().showSnackbar(v, "Add Some Item..");
                }
                break;
            case R.id.cart_list:
                todoCursor = db.rawQuery("SELECT *  FROM cart ORDER BY _id DESC", null);
                if (todoCursor.getCount() > 0) {
                    startActivity(new Intent(getActivity(), CartListActivity.class));
                } else {
                    new Utility().showSnackbar(v, "Add Some Item..");
                }
                break;
        }
    }

    public class GetMenuAndCategory extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            NetworkAPI networkAPI = new NetworkAPI();
            String menu_category = "";
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Tags.APPID_S, AppId);
                jsonObject.put(Tags.CUST_ID, CustId);
                jsonObject.put(Tags.ID_S, Id);
                menu_category = networkAPI.sendHTTPData(Constants.GETMENUCATRGORY, jsonObject);
                Log.d(TAG, "doInBackground: " + menu_category);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return menu_category;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            qPayProgressDialog = new QPayProgressDialog(getContext());
            qPayProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            qPayProgressDialog.dismiss();

            try {
                Log.d("MenuCategories", "MenuCategories: "+s);
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean(Tags.SUCESS);
                if (success) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    JSONArray MenuCategories = data.getJSONArray("MenuCategories");
                    JSONArray MenuItems = data.getJSONArray("MenuItems");
                    double Vat = data.getDouble("Vat");
                    double ServiceCharge = data.getDouble("ServiceCharge");
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(KEY, MenuItems.toString());
                    editor.putFloat(VAT, (float) Vat);
                    editor.putFloat(SERVICE_CHARGE, (float) 0);
                    editor.commit();
                    Log.d(TAG, "onPostExecute: " + MenuCategories);
                    for (int i = 0; i < MenuCategories.length(); i++) {
                        MenuCategoriesModel menuCategoriesModel = new MenuCategoriesModel();
                        JSONObject jsonObject1 = MenuCategories.getJSONObject(i);
                        menuCategoriesModel.setCategory(jsonObject1.getString("Category"));
                        menuCategoriesModel.setIconUrl(jsonObject1.getString("IconUrl"));
                        menuCategoriesModel.setId(jsonObject1.getString("Id"));
                        menuCategoriesModels.add(menuCategoriesModel);
                    }
                    menuCategoriesAdapter = new MenuCategoriesAdapter(menuCategoriesModels, MenuItemFragment.this);
                    category_menu.setAdapter(menuCategoriesAdapter);
                    category_menu.addOnItemTouchListener(new NearbyShopListActivity.RecyclerTouchListener(getContext(), category_menu, new NearbyShopListActivity.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            ArrayList<MenuListModel> menuListModelsFilter = new ArrayList<>();
                            position_category = position;
                            String category_id = menuCategoriesModels.get(position).getId();
                            String item_list = sharedPreferences.getString(KEY, null);
                            try {
                                JSONArray listArray = new JSONArray(item_list);
                                if (listArray.length() > 0) {
                                    menu_item_empty.setVisibility(View.GONE);
                                    for (int j = 0; j < listArray.length(); j++) {
                                        JSONObject jsonObject11 = listArray.getJSONObject(j);
                                        if (category_id.equals(jsonObject11.getString("CategoryId"))) {
                                            MenuListModel menuListModel = new MenuListModel();
                                            menuListModel.setCategoryId(jsonObject11.getString("CategoryId"));
                                            menuListModel.setImageUrl(jsonObject11.getString("ImageUrl"));
                                            menuListModel.setDescription(jsonObject11.getString("Description"));
                                            menuListModel.setItemName(jsonObject11.getString("ItemName"));
                                            menuListModel.setItemId(jsonObject11.getString("ItemId"));
                                            menuListModel.setRate(jsonObject11.getDouble("Rate"));
                                            menuListModelsFilter.add(menuListModel);
                                        }
                                    }
                                    menuListAdapter = new MenuListAdapter(MenuItemFragment.this, menuListModelsFilter);
                                    menuListAdapter.notifyDataSetChanged();
                                    menu_list.setAdapter(menuListAdapter);
                                } else {
                                    menu_item_empty.setVisibility(View.VISIBLE);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onLongClick(View view, int position) {
                        }
                    }));
                    for (int j = 0; j < MenuItems.length(); j++) {
                        MenuListModel menuListModel = new MenuListModel();
                        JSONObject jsonObject1 = MenuItems.getJSONObject(j);
                        menuListModel.setCategoryId(jsonObject1.getString("CategoryId"));
                        menuListModel.setImageUrl(jsonObject1.getString("ImageUrl"));
                        menuListModel.setDescription(jsonObject1.getString("Description"));
                        menuListModel.setItemName(jsonObject1.getString("ItemName"));
                        menuListModel.setItemId(jsonObject1.getString("ItemId"));
                        menuListModel.setRate(jsonObject1.getDouble("Rate"));
                        menuListModels.add(menuListModel);
                    }
                    menuListAdapter = new MenuListAdapter(MenuItemFragment.this, menuListModels);
                    menu_list.setAdapter(menuListAdapter);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {

        menuCategoriesAdapter = new MenuCategoriesAdapter(menuCategoriesModels, MenuItemFragment.this);
        category_menu.setAdapter(menuCategoriesAdapter);
        menuListAdapter = new MenuListAdapter(MenuItemFragment.this, menuListModels);
        menu_list.setAdapter(menuListAdapter);
        try {
            totalItemCart();

        }catch (Exception e){
            e.printStackTrace();
        }
        super.onResume();
        Log.d(TAG, "onResume: ");
    }


}
