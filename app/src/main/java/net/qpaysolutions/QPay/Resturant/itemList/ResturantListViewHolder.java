package net.qpaysolutions.QPay.Resturant.itemList;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 3/8/17.
 */

public class ResturantListViewHolder extends RecyclerView.ViewHolder {
    ImageView list_image;
    TextView category_item_name,category_item_location,category_rating,category_food_type,category_opening_hour;
    LinearLayout layout_click;

    public ResturantListViewHolder(View itemView) {
        super(itemView);
        list_image=(ImageView)itemView.findViewById(R.id.list_image);
        category_item_name=(TextView)itemView.findViewById(R.id.category_item_name);
        category_item_location=(TextView)itemView.findViewById(R.id.category_item_location);
        category_rating=(TextView)itemView.findViewById(R.id.category_rating);
        category_food_type=(TextView)itemView.findViewById(R.id.category_food_type);
        category_opening_hour=(TextView)itemView.findViewById(R.id.category_opening_hour);
        layout_click=(LinearLayout)itemView.findViewById(R.id.layout_click);
    }
}
