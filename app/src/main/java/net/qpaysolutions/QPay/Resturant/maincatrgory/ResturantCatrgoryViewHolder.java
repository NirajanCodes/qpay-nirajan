package net.qpaysolutions.QPay.Resturant.maincatrgory;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.qpaysolutions.QPay.R;

/**
 * Created by deadlydragger on 3/8/17.
 */

public class ResturantCatrgoryViewHolder extends RecyclerView.ViewHolder {
    LinearLayout background_resturant;
    ImageView icon_resturant;
    TextView category_name,noOrRestro;
    public ResturantCatrgoryViewHolder(View itemView) {
        super(itemView);
        background_resturant=(LinearLayout)itemView.findViewById(R.id.background_resturant);
        icon_resturant=(ImageView)itemView.findViewById(R.id.icon_resturant);
        category_name=(TextView) itemView.findViewById(R.id.category_name);
        noOrRestro=(TextView)itemView.findViewById(R.id.noOrRestro);
    }
}
