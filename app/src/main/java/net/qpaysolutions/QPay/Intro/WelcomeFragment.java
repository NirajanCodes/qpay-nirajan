package net.qpaysolutions.QPay.Intro;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.NFC.AccountStorage;
import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.CustumClasses.QPayProgressDialog;
import net.qpaysolutions.QPay.MyCode.EntercodeFragment;
import net.qpaysolutions.QPay.Register.BlockFragment;
import net.qpaysolutions.QPay.Register.CheckPhoneFragment;
import net.qpaysolutions.QPay.Register.EnterVerificationCodeReactivationFragment;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;

import org.json.JSONObject;

import java.util.Objects;
import java.util.Random;

/**
 * Created by deadlydragger on 12/25/16.
 */

public class WelcomeFragment extends Fragment {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnNext;
    private GeneralPref generalPrefManager;
    private LinearLayout skip;
    private TextView btnSkip;
    private ImageView our_service;
    private QPayCustomerDatabase QPayCustomerDatabase;
    private String appFlag;
    private QPayProgressDialog pDialog;
    private boolean showDialog = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        QPayCustomerDatabase.getWritableDatabase();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pDialog = new QPayProgressDialog(getContext());
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) view.findViewById(R.id.layoutDots);
        btnSkip = (TextView) view.findViewById(R.id.btn_skip);
        btnNext = (Button) view.findViewById(R.id.btn_next);
        skip = (LinearLayout) view.findViewById(R.id.skip);
        our_service = (ImageView) view.findViewById(R.id.our_service);
        generalPrefManager = new GeneralPref(getActivity());
        if (generalPrefManager.isFirstTimeLaunch()) {
            GeneralPref.setRandomNumber(randomNumber());
            GeneralPref.setTutorStatus(true);
        }
        if (!generalPrefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
        }
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.welcome_slide_one,
                R.layout.welcome_slide_two,
                R.layout.welcome_slide_three
        };
        // adding bottom dots
        addBottomDots(0);
        // making notification bar transparent
        changeStatusBarColor();
        myViewPagerAdapter = new MyViewPagerAdapter(getContext(), layouts);
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        btnSkip.setOnClickListener(v -> launchHomeScreen());
        btnNext.setOnClickListener(v -> {
            // checking for last page
            // if last page home screen will be launched
            int current = getItem(+1);
            if (current < layouts.length) {
                // move to next screen
                viewPager.setCurrentItem(current);
            } else {
                launchHomeScreen();
            }
        });
        skip.setOnClickListener(v -> {
            btnSkip.setVisibility(View.GONE);
            launchHomeScreen();
            GeneralPref.setTutorStatus(true);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        showDialog = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        showDialog = false;
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        showDialog = false;
    }

    /**
     * Making notification bar transparent
     */

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private String randomNumber() {

        int i = 0;
        StringBuilder stringAppend = new StringBuilder();
        while (i < 4) {
            Random r = new Random();
            stringAppend.append((String.valueOf(r.nextInt(4))));
            i++;
        }
        String randomNumber = stringAppend.toString();
        return randomNumber;

    }

    private void addBottomDots(int currentPage) {

        dots = new TextView[layouts.length];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getActivity());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        generalPrefManager.setFirstTimeLaunch(false);
       /* getFragmentManager().beginTransaction()
                .replace(R.id.fragment, new SplashFragment())
                .commit();*/
        isRegisterUser();
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
                skip.setVisibility(View.VISIBLE);
            } else if (position == layouts.length) {
                launchHomeScreen();
            } else {
                // still pages are left
                skip.setVisibility(View.GONE);
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_welcome, container, false);
    }


    public void isRegisterUser() {
        try {
            int getCusturdata = QPayCustomerDatabase.checkHasCustData();
            int getRegistered = QPayCustomerDatabase.checkRegData();
            String get_reg_flag = QPayCustomerDatabase.getRegFlag();
            String pin_retries_count = QPayCustomerDatabase.getPin_retrive();
            String flag_pin = QPayCustomerDatabase.getFlagPin();


            if (getCusturdata == 0) {
                if (getRegistered == 0) {
                    new GetCustomerId(GeneralPref.getToken()).execute();
                }
                Log.d("check", "" + getCusturdata);
            } else if (get_reg_flag.equals("A")) {
                if (pin_retries_count == null) {
                    Log.d("check1", "" + get_reg_flag);
                    if (flag_pin != null && !flag_pin.isEmpty() && flag_pin.equals("1")) {
                        try {
                            EntercodeFragment entercodeFragment = new EntercodeFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment, entercodeFragment)
                                    .commit();
                            getActivity().setTitle("Enter Passcode");
                            Log.d("check2", "check1");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.putExtra("Check", 5);
                        startActivity(intent);
                        getActivity().finish();
                        Log.d("check3", "check1");
                    }

                } else {
                    BlockFragment blockFragment = new BlockFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, blockFragment)
                            .commit();
                    Log.d("check4", "check1");
                }


            } else if (get_reg_flag.equals("I")) {
                CheckPhoneFragment registerPhoneFragment = new CheckPhoneFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, registerPhoneFragment)
                        .commit();
                Log.d("check5", "check1");
            } else if (get_reg_flag.equals("R")) {

                EnterVerificationCodeReactivationFragment enterVerificationCodeReactivationFragment = new EnterVerificationCodeReactivationFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment, enterVerificationCodeReactivationFragment)
                        .commit();
                Log.d("check6", "check1");
            } else {
                getActivity().finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetCustomerId extends AsyncTask<Void, Void, JSONObject> {
        String token;

        private GetCustomerId(String token) {
            this.token = token;
        }

        @Override
        protected void onPreExecute() {
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            try {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("dev_token", token);
                    jsonObject.put("device_type", "AND");
                    Log.d(Utility.TAG,"request "+jsonObject.toString() );
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return new JSONObject(NetworkAPI.sendHTTPData(Constants.FIRST_GCM_CALL_URL, jsonObject));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            super.onPostExecute(args);
            pDialog.dismiss();
            try {

                Log.d(Utility.TAG,"response  "+args.toString() );
                JSONObject jsonObject1 = args.getJSONObject("getcustidResult");
                String cust_id = jsonObject1.getString("cust_id");
                String app_id = jsonObject1.getString("app_id");
                String s = jsonObject1.getString("status");
                if (s.equals("00")) {
                    QPayCustomerDatabase.insertCutid(cust_id, app_id, "1", "1");
                    Log.d("custid", "" + cust_id);
                    GeneralPref.setCustId(cust_id);
                    AccountStorage.SetAccount(getActivity(), cust_id);
                    Log.d("custid_get", "" + AccountStorage.GetAccount(getActivity()));
                    QPayCustomerDatabase.createRegFlag("I");
                    CheckPhoneFragment registerPhoneFragment = new CheckPhoneFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment, registerPhoneFragment)
                            .commit();
                } else {
                    Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                    Objects.requireNonNull(getActivity()).finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (isAdded()) {
                    Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
