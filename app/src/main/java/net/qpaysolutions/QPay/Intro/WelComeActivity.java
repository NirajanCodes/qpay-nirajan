package net.qpaysolutions.QPay.Intro;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import net.qpaysolutions.QPay.R;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.Tags;
import net.qpaysolutions.QPay.Utils.Utility;


public class WelComeActivity extends AppCompatActivity {

    private FusedLocationProviderClient mFusedLocationClient;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mIntentFilters;
    private String[][] mTechList;
    String nfc_id;
    public static final int REQUEST_NFC_PERMISSION = 501;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.welcome);

        try {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, new WelcomeFragment(),"welcomeFragment")
                    .commit();
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            if (Build.VERSION.SDK_INT >= 23) {
                String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                if (!Utility.hasExternalStoragePermission(WelComeActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(WelComeActivity.this, PERMISSIONS, Tags.LOCATION);
                } else {
                    setLocation();
                }
            } else {
                setLocation();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

//        mAdapter = NfcAdapter.getDefaultAdapter(this);
//        if (mAdapter == null) {
//            Toast.makeText(WelComeActivity.this,"You need to enable NFC",Toast.LENGTH_SHORT).show();
//            showWirelessSettings();
//        }
//        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
//        IntentFilter filter = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
//        try {
//            filter.addDataType("*/*");
//        } catch (IntentFilter.MalformedMimeTypeException e) {
//            e.printStackTrace();
//        }
//        mIntentFilters = new IntentFilter[]{filter};
//        mTechList = new String[][]{new String[]{MifareClassic.class.getName()}};
//        Intent intent = new Intent();
//        intent.setAction(CardEmulation.ACTION_CHANGE_DEFAULT);
//        intent.putExtra(CardEmulation.EXTRA_SERVICE_COMPONENT,
//                new ComponentName(this, net.qpaysolutions.QPay.Intro.WelComeActivity.class));
//        intent.putExtra(CardEmulation.EXTRA_CATEGORY, CardEmulation.CATEGORY_PAYMENT);
//        startActivity(intent);
//        NfcManager manager = (NfcManager) WelComeActivity.this.getSystemService(WelComeActivity.this.NFC_SERVICE);
//        NfcAdapter adapter = manager.getDefaultAdapter();
//        if (adapter != null && adapter.isEnabled()) {
//
//            //Yes NFC available
//        }else{
//            Toast.makeText(this, "You need to enable NFC", Toast.LENGTH_SHORT).show();
//            //Your device doesn't support NFC
//        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
       /* mAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilters, mTechList);
        if (mAdapter == null) {
            //no nfc on the device
            showWirelessSettings();
        } else {
            checkPermission();
            if (!mAdapter.isEnabled()) {
                //nfc is disabled
                showWirelessSettings();
            } else {
            }
            mAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilters, mTechList);
        }*/
    }

    public void checkPermission() {
        //TODO: check for permissions in the runtime and ask if not granted
        int permission
                = ActivityCompat.checkSelfPermission(WelComeActivity.this, Manifest.permission.NFC);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            //the permission is granted, proceed
        } else {
            //the persmission is not granted, ask for permission
            ActivityCompat.requestPermissions(WelComeActivity.this, new String[]{Manifest.permission.NFC}, REQUEST_NFC_PERMISSION);
        }

    }

    private void showWirelessSettings() {
        Toast.makeText(this, "You need to enable NFC", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (!NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())) {
            return;
        }
//        setIntent(intent);
//        resolveIntent(intent);
    }


    private void setLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mFusedLocationClient.getLastLocation()
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    })
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(final Location location) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (location != null) {
                                                LatLngPref.setLat(String.valueOf(location.getLatitude()));
                                                LatLngPref.setLng(String.valueOf(location.getLongitude()));

                                            } /*else {
                                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                                startActivity(intent);
                                            }*/
                                        }
                                    }, 500);
                                }
                            }
                    );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Tags.LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setLocation();
                }
                break;
            case REQUEST_NFC_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //nfc  permission granted
                } else {
                    //nfc permission not granted
                }
                break;
        }
    }

//    private void resolveIntent(Intent intent) {
//        String action = intent.getAction();
//        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
//                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
//                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
//            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
//            NdefMessage[] msgs;
//
//            if (rawMsgs != null) {
//                msgs = new NdefMessage[rawMsgs.length];
//
//                for (int i = 0; i < rawMsgs.length; i++) {
//                    msgs[i] = (NdefMessage) rawMsgs[i];
//                }
//
//            } else {
//                byte[] empty = new byte[0];
//                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
//                Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//                if (dumpTagData(tag) != null) {
//                    byte[] payload = dumpTagData(tag).getBytes();
//                    Intent i = new Intent(WelComeActivity.this, EnterAmmountForCard.class);
//                    i.putExtra("nfc_id",nfc_id);
//                    startActivity(i);
//                }
//
//            }
//
//
//        }
//    }

//    private String dumpTagData(Tag tag) {
//        StringBuilder sb = new StringBuilder();
//        byte[] id = tag.getId();
//        sb.append("ID (hex): ").append(Utility.toHex(id)).append('\n');
//        sb.append("ID (reversed hex): ").append(Utility.toReversedHex(id)).append('\n');
//        sb.append("ID (dec): ").append(Utility.toDec(id)).append('\n');
//        sb.append("ID (reversed dec): ").append(Utility.toReversedDec(id)).append('\n');
//        Log.d("nfc_id", "" + Utility.toDec(id));
//        nfc_id= String.valueOf(Utility.toDec(id));
//        String prefix = "android.nfc.tech.";
//        sb.append("Technologies: ");
//        for (String tech : tag.getTechList()) {
//            sb.append(tech.substring(prefix.length()));
//            sb.append(", ");
//        }
//
//        sb.delete(sb.length() - 2, sb.length());
//
//        for (String tech : tag.getTechList()) {
//            if (tech.equals(MifareClassic.class.getName())) {
//                sb.append('\n');
//                String type = "Unknown";
//
//                try {
//                    MifareClassic mifareTag = MifareClassic.get(tag);
//
//                    switch (mifareTag.getType()) {
//                        case MifareClassic.TYPE_CLASSIC:
//                            type = "Classic";
//                            break;
//                        case MifareClassic.TYPE_PLUS:
//                            type = "Plus";
//                            break;
//                        case MifareClassic.TYPE_PRO:
//                            type = "Pro";
//                            break;
//                    }
//                    sb.append("Mifare Classic type: ");
//                    sb.append(type);
//                    sb.append('\n');
//
//                    sb.append("Mifare size: ");
//                    sb.append(mifareTag.getSize() + " bytes");
//                    sb.append('\n');
//
//                    sb.append("Mifare sectors: ");
//                    sb.append(mifareTag.getSectorCount());
//                    sb.append('\n');
//
//                    sb.append("Mifare blocks: ");
//                    sb.append(mifareTag.getBlockCount());
//                } catch (Exception e) {
//                    sb.append("Mifare classic error: " + e.getMessage());
//                }
//            }
//
//            if (tech.equals(MifareUltralight.class.getName())) {
//                sb.append('\n');
//                MifareUltralight mifareUlTag = MifareUltralight.get(tag);
//                String type = "Unknown";
//                switch (mifareUlTag.getType()) {
//                    case MifareUltralight.TYPE_ULTRALIGHT:
//                        type = "Ultralight";
//                        break;
//                    case MifareUltralight.TYPE_ULTRALIGHT_C:
//                        type = "Ultralight C";
//                        break;
//                }
//                sb.append("Mifare Ultralight type: ");
//                sb.append(type);
//            }
//        }
//        return sb.toString();
//    }

}