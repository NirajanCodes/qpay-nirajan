package net.qpaysolutions.QPay.dashboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.glide.slider.library.Indicators.PagerIndicator;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.fragment.app.Fragment;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qpaysolutions.QPay.CustumClasses.ChildCarouselAnimation;
import net.qpaysolutions.QPay.R;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.skyfishjy.library.RippleBackground;

import net.qpaysolutions.QPay.Billpayment.BillPayActivity;
import net.qpaysolutions.QPay.Billpayment.LoadFundActivity;
import net.qpaysolutions.QPay.Chat.ChatActivity;
import net.qpaysolutions.QPay.Deal.AllActivityNew;
import net.qpaysolutions.QPay.Deal.GetdealData;
import net.qpaysolutions.QPay.FragmentCollection.Negativebalancefragment;
import net.qpaysolutions.QPay.History.TransactionHistoryActivity;
import net.qpaysolutions.QPay.MainActivity;
import net.qpaysolutions.QPay.QRcodegenerator.Contents;
import net.qpaysolutions.QPay.QRcodegenerator.QRCodeEncoder;
import net.qpaysolutions.QPay.Register.SuspendedUserFragment;
import net.qpaysolutions.QPay.Register.Unauthorizeuser;
import net.qpaysolutions.QPay.SalesMerchants.ReadMerchantQRActivity;
import net.qpaysolutions.QPay.Send.SendMainActivity;
import net.qpaysolutions.QPay.Sqlitedatabases.QPayCustomerDatabase;
import net.qpaysolutions.QPay.Utils.BalancePref;
import net.qpaysolutions.QPay.Utils.Constants;
import net.qpaysolutions.QPay.Utils.CustomerApplication;
import net.qpaysolutions.QPay.Utils.Decimalformate;
import net.qpaysolutions.QPay.Utils.GeneralPref;
import net.qpaysolutions.QPay.Utils.LatLngPref;
import net.qpaysolutions.QPay.Utils.NetworkAPI;
import net.qpaysolutions.QPay.Utils.Utility;
import net.qpaysolutions.QPay.Withdrawal.WithDrawablActivity;
import net.qpaysolutions.QPay.duonavigation.views.DuoMenuView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import static net.qpaysolutions.QPay.Utils.Utility.TAG;

/**
 * Created by deadlydragger on 9/25/18.
 */

public class MainActivityFragment extends Fragment implements DuoMenuView.OnMenuClickListener, View.OnClickListener {

    CustomAdapter productAdapter;
    private View view_view;
    ArrayList productList = new ArrayList<ProductModel>();
    ArrayList bookList = new ArrayList<ProductModel>();
    ArrayList fundList = new ArrayList<ProductModel>();
    private RecyclerView payBillRecyclerView, bookRecyclerView;
    private RelativeLayout main_container;
    private ImageView payBtn, payBtnReal;
    private ImageView refresh, myCode;
    private LinearLayout loadBalance, sendMoney, buttonLayout, llmoney;
    private ArrayList<String> mTitles = new ArrayList<>();
    private BottomNavigationView bottomNavigationView;
    private FrameLayout shimmerContainer;
    private RippleBackground rippleBackground;
    private RelativeLayout image_round;
    private boolean isLongPress = false;
    private LinearLayout withdrawMoney, history;
    private FrameLayout progressBar;
    private String cust_id, app_id;
    private TextView balance;
    private BottomNavigationView bottomNavigation;
    private Calendar calendar;
    private List<GetdealData> url;
    private MainActivity.DuoLock duoLock;
    private QPayCustomerDatabase getQPayCustomerDatabase;
    private FrameLayout notificationFramlayout;
    private String inProgress;
    private SliderAnimated adds;
    private TextView tv;
    private static String bannerurls[] = new String[3];
    private static Bundle bundle = new Bundle();
    private RelativeLayout cardView, cardContainer;
    private SetName setName;
    ProductModel mProductModel = new ProductModel();
    ProductModel mProductModel1 = new ProductModel();
    ProductModel mProductModel2 = new ProductModel();
    ProductModel mProductModel3 = new ProductModel();
    ProductModel mProductModel5 = new ProductModel();
    ProductModel mProductModel4 = new ProductModel();
    ProductModel mProductModel6 = new ProductModel();
    ProductModel mProductModel9 = new ProductModel();
    ProductModel mProductModel12 = new ProductModel();
    ProductModel mProductModel17 = new ProductModel();

    private Bitmap userImageBitmap;
    ImageView upi_icon, qpay_icon;
    private Toolbar toolbar;

    public MainActivityFragment() {

    }

    @SuppressLint("ValidFragment")
    public MainActivityFragment(ImageView refresh, ImageView myCode, Toolbar toolbar, MainActivity.DuoLock duoLock, SetName setName) {
        this.myCode = myCode;
        this.refresh = refresh;
        this.toolbar = toolbar;
        this.duoLock = duoLock;
        this.setName = setName;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (balance != null) {
            balance.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GeneralPref.setAppId(app_id);
        GeneralPref.setCustId(cust_id);

        if (GeneralPref.isTutorShown()) {
            showTutor(view, refresh);
        }

        // cardView = view.findViewById(R.id.bookLayout);
//        cardView.setOnClickListener(this);
        qpay_icon = view.findViewById(R.id.qpay_icon);
        upi_icon = view.findViewById(R.id.upi_icon);
        payBillRecyclerView = view.findViewById(R.id.payBillRecyclerView);
        bookRecyclerView=view.findViewById(R.id.bookRecyclerView);
        main_container = view.findViewById(R.id.main_container);
        payBtnReal = view.findViewById(R.id.pay_btnReal);
        //  bookRecyclerView = view.findViewById(R.id.bookRecyclerView);
        image_round = view.findViewById(R.id.imgLinear);
        shimmerContainer = view.findViewById(R.id.shimmerLayoutReal);
        loadBalance = view.findViewById(R.id.load_balance);
        loadBalance.setOnClickListener(this);
        sendMoney = view.findViewById(R.id.send_money);
        sendMoney.setOnClickListener(this);
        withdrawMoney = view.findViewById(R.id.request_money);
        withdrawMoney.setOnClickListener(this);
        history = view.findViewById(R.id.history);
        history.setOnClickListener(this);

        adds = view.findViewById(R.id.slider);
        adds.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
        main_container = view.findViewById(R.id.main_container);
        llmoney = view.findViewById(R.id.ll_money);

        if (inProgress != null) {

        }

        balance = view.findViewById(R.id.balance_main);
        balance.setText(new Decimalformate().decimalFormate(BalancePref.getAvailableAmount()));
        progressBar = view.findViewById(R.id.progressBar);
        //loadDataBook();
        loadDataPayBills();
        bottomNavigationView = view.findViewById(R.id.bottom_navigation);
        bottomNavigation();
        try {
            refresh.setOnClickListener(this);
            myCode.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        view_view = view;
        url = new ArrayList<>();
        System.out.println("sizes:: " + url.size());
        buttonBar();
        Animation shine = AnimationUtils.loadAnimation(getContext(), R.anim.shine);
        Animation shineThrough = AnimationUtils.loadAnimation(getContext(), R.anim.shine_through);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(shine);
        animationSet.addAnimation(shineThrough);
        payBtnReal.startAnimation(animationSet);
        if (CustomerApplication.open) {
            new CheckStatusBg().execute();
        }
        CustomerApplication.open = false;
        payBtnReal.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ReadMerchantQRActivity.class);
            startActivity(intent);
        });
        mTitles = new ArrayList<>(Arrays.asList(
                getResources().getStringArray(R.array.ld_activityScreenTitles)));
        upi_icon.setOnClickListener(v -> {
//                if(PopUpUpi.isShowing()){
//                    PopUpUpi.dismiss();
//                }else{
//                    PopUpUpi.showAsDropDown(v);
//                }
//                if (popup_upi.getVisibility() == View.VISIBLE) {

//                    popup_upi.setVisibility(View.GONE);jsonArray.getString(i)
//                    // Its visible
//                } else {
//                    popup_upi.setVisibility(View.VISIBLE);
//                    popup_qpay.setVisibility(View.GONE);
//                    // Either gone or invisible
//                }
            showTutorForUpi(v, refresh);
        });
        qpay_icon.setOnClickListener(v -> {
//                if(popup_qpay.isShowing()){
//                    PopUpQpay.dismiss();
//                }else{
//                    PopUpQpay.showAsDropDown(v);
//                }
//                if (popup_qpay.getVisibility() == View.VISIBLE) {
//                    popup_qpay.setVisibility(View.GONE);
//                    // Its visible
//                } else {
//                    popup_qpay.setVisibility(View.VISIBLE);
//                    popup_upi.setVisibility(View.GONE);
//                    // Either gone or invisijsonArray.getString(i)blerequest_money
//                }
            showTutorForQpay(v, refresh);
        });
        promotionImages();
        SliderAnimated.SliderCallBack sliderCallBack = () -> {
            if (bundle != null && !bundle.isEmpty()) {
                Intent bannerIntent = new Intent(getActivity(), BannerActivity.class);
                bannerIntent.putExtra("bundle", bundle);
                startActivity(bannerIntent);
            }
        };
        adds.sliderGet(sliderCallBack);
        loadDataBook();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_activity_fragment, container, false);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getQPayCustomerDatabase = new QPayCustomerDatabase(getActivity());
        getQPayCustomerDatabase.getReadableDatabase();
        getQPayCustomerDatabase.getWritableDatabase();
        cust_id = getQPayCustomerDatabase.getCustomerID();
        app_id = getQPayCustomerDatabase.getKeyAppId();
        GeneralPref.setName(getQPayCustomerDatabase.getUserNameLast() + " " + getQPayCustomerDatabase.getUserNameFirst());
        GeneralPref.setPhone(getQPayCustomerDatabase.getCustomerPhone());
        setHasOptionsMenu(true);
        GeneralPref.setAppId(app_id);
        GeneralPref.setCustId(cust_id);
    }

    private void rotateImage(float degree, int rotations, ImageView imageView, int duration, Boolean stop) {
        try {
            final RotateAnimation rotateAnim = new RotateAnimation(0.00f, degree,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            rotateAnim.setDuration(duration);
            rotateAnim.setFillAfter(true);
            rotateAnim.setRepeatCount(rotations);
            imageView.startAnimation(rotateAnim);
            if (stop) {
                rotateAnim.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void promotionImages() {
        //adds.addSlider(new TextSliderView(getContext()).image(R.drawable.ic_logo_qpay));

        if (GeneralPref.getBannerArray() != null && !GeneralPref.getBannerArray().isEmpty()) {
            JSONArray jsonArray = null;
            ArrayList<String> promotionbanners = new ArrayList();
            try {
                jsonArray = new JSONArray(GeneralPref.getBannerArray());
                for (int i = 0; i < jsonArray.length(); i++) {
                    promotionbanners.add(jsonArray.getString(i));
                    adds.addSlider(new TextSliderView(getContext()).image(promotionbanners.get(i)));
                }
                adds.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
                adds.startAutoCycle();
                adds.setCustomAnimation(new ChildCarouselAnimation());
                adds.getParent().requestDisallowInterceptTouchEvent(false);
                bundle.putStringArrayList("bundle", promotionbanners);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadDataPayBills() {

        productAdapter = new CustomAdapter(getContext(), productList, 0);
        final LinearLayoutManager MyLayoutManager2 = new GridLayoutManager(getActivity(), 5);
        MyLayoutManager2.setOrientation(RecyclerView.VERTICAL);

        payBillRecyclerView.setHasFixedSize(true);
        payBillRecyclerView.setNestedScrollingEnabled(false);

        payBillRecyclerView.setLayoutManager(MyLayoutManager2);
        payBillRecyclerView.setAdapter(productAdapter);

        mProductModel1.setIcon("n");
        mProductModel1.setName("Phone");
        mProductModel1.setId("1");
        mProductModel1.setImage(R.drawable.ic_mobile_bill);
        productList.add(mProductModel1);

        mProductModel.setIcon("j");
        mProductModel.setName("TV");
        mProductModel.setId("0");
        mProductModel.setImage(R.drawable.ic_dish_tv_recharge);
        productList.add(mProductModel);

        mProductModel2.setIcon("m");
        mProductModel2.setName("Internet");
        mProductModel2.setId("2");
        mProductModel2.setImage(R.drawable.ic_internet_bill_blue);
        productList.add(mProductModel2);

        mProductModel3.setIcon("g");
        mProductModel3.setName("NEA");
        mProductModel3.setId("3");
        mProductModel3.setImage(R.drawable.ic_electricity_bill);
        productList.add(mProductModel3);

        mProductModel4.setIcon("k");
        mProductModel4.setName("Water");
        mProductModel4.setId("4");
        mProductModel4.setImage(R.drawable.ic_water_bill);
        productList.add(mProductModel4);

        ViewCompat.setNestedScrollingEnabled(payBillRecyclerView, false);

    }

    private void loadDataBook() {

        productAdapter = new CustomAdapter(getContext(), bookList, 0);
        GridLayoutManager MyLayoutManager3 = new GridLayoutManager(getActivity(), 5);
        MyLayoutManager3.setOrientation(RecyclerView.VERTICAL);

        bookRecyclerView.setHasFixedSize(true);
        bookRecyclerView.setNestedScrollingEnabled(false);

        bookRecyclerView.setLayoutManager(MyLayoutManager3);
        bookRecyclerView.setAdapter(productAdapter);

        mProductModel5.setIcon("d");
        mProductModel5.setName("Flight");
        mProductModel5.setId("5");
        mProductModel5.setImage(R.drawable.ic_flight_book);
        bookList.add(mProductModel5);

      /*  mProductModel.setIcon("j");
        mProductModel.setName("TV");
        mProductModel.setId("0");
        mProductModel.setImage(R.drawable.ic_dish_tv_recharge);
        bookList.add(mProductModel);

        mProductModel1.setIcon("n");
        mProductModel1.setName("Phone");
        mProductModel1.setId("1");
        mProductModel1.setImage(R.drawable.ic_mobile_bill);
        bookList.add(mProductModel1);

        mProductModel2.setIcon("m");
        mProductModel2.setName("Internet");
        mProductModel2.setId("2");
        mProductModel2.setImage(R.drawable.ic_internet_bill_blue);
        bookList.add(mProductModel2);

        mProductModel3.setIcon("g");
        mProductModel3.setName("Electricity");
        mProductModel3.setId("3");
        mProductModel3.setImage(R.drawable.ic_electricity_bill);
        bookList.add(mProductModel3);

        mProductModel4.setIcon("k");
        mProductModel4.setName("Water");
        mProductModel4.setId("4");
        mProductModel4.setImage(R.drawable.ic_water_bill);
        bookList.add(mProductModel4);.
        */

        mProductModel6.setIcon("t");
        mProductModel6.setName("Bus");
        mProductModel6.setId("6");
        mProductModel6.setImage(R.drawable.ic_bus_book);
        bookList.add(mProductModel6);

/*

        mProductModel7.setIcon("f");
        mProductModel7.setName("Movie");
        mProductModel7.setId("7");
        mProductModel7.setImage(R.drawable.ic_buy_movie_ticket);
        bookList.add(mProductModel7);

        mProductModel8.setName("Hotel");
        mProductModel8.setId("8");
        bookList.add(mProductModel8);
*/

        mProductModel9.setIcon("q");
        mProductModel9.setName("Events");
        mProductModel9.setId("9");
        mProductModel9.setImage(R.drawable.ic_event_ticket);
        bookList.add(mProductModel9);

/*
       mProductModel10.setIcon("s");
        mProductModel10.setName("Bank");
        mProductModel10.setId("10");
        mProductModel10.setImage(R.drawable.ic_event_ticket);
        bookList.add(mProductModel10);


        mProductModel11.setIcon("o");
        mProductModel11.setName("COOP");
        mProductModel11.setId("11");
        mProductModel11.setImage(R.drawable.ic_event_ticket);
        bookList.add(mProductModel11);
*/

        mProductModel12.setIcon("u");
        mProductModel12.setName("Card Topup");
        mProductModel12.setId("12");
        mProductModel12.setImage(R.drawable.ic_event_ticket);
        bookList.add(mProductModel12);
        mProductModel12.setIcon("u");

        mProductModel17.setName("Restaurants");
        mProductModel17.setId("13");
        mProductModel17.setImage(R.drawable.ic_merchants);
        bookList.add(mProductModel17);
        ViewCompat.setNestedScrollingEnabled(bookRecyclerView, false);

    }

    /*private void loadBalance() {

        productAdapter = new CustomAdapter(getActivity(), fundList, 1);
        GridLayoutManager MyLayoutManager1 = new GridLayoutManager(getActivity(), 4);
        MyLayoutManager1.setOrientation(RecyclerView.VERTICAL);

       *//* moneyView.setHasFixedSize(true);
        moneyView.setNestedScrollingEnabled(false);

        moneyView.setLayoutManager(MyLayoutManager1);
        moneyView.setAdapter(productAdapter);
*//*
        mProductModel13.setIcon("j");
        mProductModel13.setImage(R.drawable.ic_load_balance);
        mProductModel13.setName("Load balance");
        mProductModel13.setId("0");
        fundList.add(mProductModel13);

        mProductModel14.setIcon("j");
        mProductModel14.setImage(R.drawable.ic_send_money);
        mProductModel14.setName("Send Money");
        mProductModel14.setId("1");
        fundList.add(mProductModel14);

        mProductModel15.setIcon("n");
        mProductModel15.setImage(R.drawable.ic_request_money);
        mProductModel15.setName("Request Money");
        mProductModel15.setId("2");
        fundList.add(mProductModel15);

        mProductModel16.setIcon("m");
        mProductModel16.setImage(R.drawable.ic_transaction_history);
        mProductModel16.setName("History");
        mProductModel16.setId("3");
        fundList.add(mProductModel16);
        ViewCompat.setNestedScrollingEnabled(payBillRecyclerView, false);

    }*/


    @Override
    public void onFooterClicked() {

    }

    @Override
    public void onHeaderClicked() {

    }

    @Override
    public void onOptionClicked(int position, Object objectClicked) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.load_balance:
                Intent loadBalance = new Intent(getActivity(), LoadFundActivity.class);
                startActivity(loadBalance);
                break;
            case R.id.send_money:
                Intent sendMoney = new Intent(getActivity(), SendMainActivity.class);
                startActivity(sendMoney);
                break;
            case R.id.request_money:
                Intent withdrawMoney = new Intent(getActivity(), WithDrawablActivity.class);
                startActivity(withdrawMoney);
                break;
            case R.id.history:
                Intent history = new Intent(getActivity(), TransactionHistoryActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(history);
            case R.id.refresh:
                //new CheckStatusBg().execute();
                new CheckStatus().execute();
                /*Intent intent=new Intent(getActivity(),MainActivity.class);
                startActivity(intent);*/
                break;
            case R.id.my_code:
                myCodeDialog();
                break;
              /*  Intent intent = new In
          tent(getActivity(), MyCodeActivity.class);
                startActivity(intent);*/
           /* case R.id.bookLayout:
                try {
                    if (bundle != null && !bundle.isEmpty()) {
                        Intent bannerIntent = new Intent(getActivity(), BannerActivity.class);
                        bannerIntent.putExtra("bundle", bundle);
                        startActivity(bannerIntent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
        }
    }

    private void myCodeDialog() {

        llmoney.setVisibility(View.INVISIBLE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mycode, null);
        ImageView myImage = v.findViewById(R.id.create_my_qrcode);
        TextView name = v.findViewById(R.id.name_setting);
        final TextView balace = v.findViewById(R.id.balance_setting);
        LinearLayout progressLayout = v.findViewById(R.id.progressLayout);
        ImageView cancel = v.findViewById(R.id.cancel);

        name.setText(GeneralPref.getName());
        balace.setText(BalancePref.getAvailableAmount());
        builder.setView(v);

        final AlertDialog alertDialog = builder.create();

        alertDialog.setCancelable(false);
        cancel.setOnClickListener(v1 -> {
                    llmoney.setVisibility(View.VISIBLE);
                    alertDialog.dismiss();

                }
        );

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().setAttributes(lp);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawableResource(R.color.transparent);
        /*alertDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.WRAP_CONTENT);*/
        alertDialog.show();
        new GETQRCode(progressLayout, myImage).execute();

    }


    public static class BottomNavigationViewHelper {
        @SuppressLint("RestrictedApi")
        public static void disableShiftMode(BottomNavigationView view) {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
            try {
                Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
                shiftingMode.setAccessible(true);
                shiftingMode.setBoolean(menuView, false);
                shiftingMode.setAccessible(false);
                for (int i = 0; i < menuView.getChildCount(); i++) {
                    BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);

                    //noinspection RestrictedApi
                    item.setShifting(false);
                    // set once again checked value, so view will be updated
                    //noinspection RestrictedApi
                    item.setChecked(item.getItemData().isChecked());
                }
            } catch (NoSuchFieldException e) {
                Log.e("BNVHelper", "Unable to get shift mode field", e);
            } catch (IllegalAccessException e) {
                Log.e("BNVHelper", "Unable to change value of shift mode", e);
            }
        }
    }

   /* private void foundDevice() {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(rippleBackground, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(rippleBackground, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        rippleBackground.setVisibility(View.VISIBLE);
        animatorSet.start();

    }*/

    @SuppressLint("RestrictedApi")
    public void buttonBar() {
        bottomNavigationView = view_view.findViewById(R.id.bottom_navigation);
        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.home:
                        return false;

                    case R.id.chat:
                        Intent chat = new Intent(getActivity(), ChatActivity.class);
                        startActivity(chat);
                        return false;

                    case R.id.pay:
                        Intent readMerchant = new Intent(getActivity(), ReadMerchantQRActivity.class);
                        startActivity(readMerchant);
                        return false;

                    case R.id.atm:
                        Intent nearbyAtmsIntent = new Intent(getActivity(), BillPayActivity.class);
                        nearbyAtmsIntent.putExtra("id", "atm");
                        startActivity(nearbyAtmsIntent);
                        return false;

                    case R.id.restaurants:
                        try {
                            Intent taxi = new Intent(getActivity(), AllActivityNew.class);
                            taxi.putExtra("category_name", "Participating Restaurants");
                            startActivity(taxi);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        return false;

                    default:
                        return false;
                }
            }
        };
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        try {

            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);

            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
//                item.setShiftingMode(false);
                item.setEnabled(false);
                //To update view, set the checked value again
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

 /*   public void updateApp(){
        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            String packageName=pInfo.packageName;
            int versionCode = pInfo.versionCode;
            GeneralPref.setPackageName(packageName);
            GeneralPref.setVersionCode(version);
            Log.d("dinesh","version  "+version+"......"+versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }*/

    private void bottomNavigation() {
        try {

            if (GeneralPref.getRestroCount() > 0) {

                Log.d("dinesh", "check3" + GeneralPref.getRestroCount());
                BottomNavigationMenuView bottomNavigationMenuView =
                        (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
                View v = bottomNavigationMenuView.getChildAt(0);
                BottomNavigationItemView itemView = (BottomNavigationItemView) v;

                View badge = LayoutInflater.from(getActivity())
                        .inflate(R.layout.notification_badge, bottomNavigationMenuView, false);
                tv = badge.findViewById(R.id.notification_badge);
                tv.setText(String.valueOf(GeneralPref.getRestroCount()));
                itemView.addView(badge);

            }

            if (GeneralPref.getChatCount() > 0) {

                Log.d("dinesh", "check1" + GeneralPref.getChatCount());
//                    bottomNavigation.setNotification(String.valueOf(GeneralPref.getChatCount()), 1);
                BottomNavigationMenuView bottomNavigationMenuView =
                        (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
                View v = bottomNavigationMenuView.getChildAt(1);
                BottomNavigationItemView itemView = (BottomNavigationItemView) v;
                View badge = LayoutInflater.from(getActivity())
                        .inflate(R.layout.notification_badge, bottomNavigationMenuView, false);
                notificationFramlayout = badge.findViewById(R.id.notificationFramlayout);
                tv = badge.findViewById(R.id.notification_badge);
                tv.setText(String.valueOf(GeneralPref.getChatCount()));
                itemView.addView(badge);

            }

            if (GeneralPref.getTaxi() > 0) {
                Log.d("dinesh", "check2" + GeneralPref.getAtm());
                BottomNavigationMenuView bottomNavigationMenuView =
                        (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
                View v = bottomNavigationMenuView.getChildAt(2);
                BottomNavigationItemView itemView = (BottomNavigationItemView) v;

                View badge = LayoutInflater.from(getActivity())
                        .inflate(R.layout.notification_badge, bottomNavigationMenuView, false);
                tv = badge.findViewById(R.id.notification_badge);
                tv.setText(String.valueOf(GeneralPref.getAtm()));
                itemView.addView(badge);
            }

//            if (GeneralPref.getAtm() > 0) {
//                Log.d("dinesh", "check3" + GeneralPref.getAtm());
//                BottomNavigationMenuView bottomNavigationMenuView =
//                        (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
//                View v = bottomNavigationMenuView.getChildAt(1);
//                BottomNavigationItemView itemView = (BottomNavigationItemView) v;
//
//                View badge = LayoutInflater.from(getActivity())
//                        .inflate(R.layout.notification_badge, bottomNavigationMenuView, false);
//                tv = badge.findViewById(R.id.notification_badge);
//                tv.setText(String.valueOf(GeneralPref.getAtm()));
//                itemView.addView(badge);
//
//            }

            Log.d("dinesh", "count" + GeneralPref.getRestroCount());
            if (GeneralPref.getRestroCount() > 0) {
                Log.d("dinesh", "check3" + GeneralPref.getRestroCount());
                BottomNavigationMenuView bottomNavigationMenuView =
                        (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
                View v = bottomNavigationMenuView.getChildAt(0);
                BottomNavigationItemView itemView = (BottomNavigationItemView) v;
                View badge = LayoutInflater.from(getActivity())
                        .inflate(R.layout.notification_badge, bottomNavigationMenuView, false);
                tv = badge.findViewById(R.id.notification_badge);
                tv.setText(String.valueOf(GeneralPref.getRestroCount()));
                itemView.addView(badge);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class CheckStatusBg extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rotateImage(360, 90, refresh, 500, false);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... uri) {

            String result_auth = null;
            NetworkAPI networkAPI = new NetworkAPI();
            JSONObject jsonObject = new JSONObject();
            try {
//                String customerTLV = Utils.getDukptCustomerId(cust_id);
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                Log.d(TAG, "doInBackground: " + jsonObject);
                result_auth = networkAPI.sendHTTPData(Constants.CHECK_MY_STATUS_BG, jsonObject);
                Log.d(TAG, "doInBackground: " + result_auth);

            } catch (Exception e1) {

                e1.printStackTrace();
            }
            return result_auth;
        }


        @Override
        protected void onPostExecute(String status) {
            super.onPostExecute(status);
            Log.d("dinesh", "response  CheckStatusBg: " + status);
            progressBar.setVisibility(View.GONE);
            rotateImage(360, 100, refresh, 500, true);
            try {

                JSONObject jsonObject2 = new JSONObject(status);
                JSONObject jsonObject1 = jsonObject2.getJSONObject("checkmystatusbgResult");
                JSONArray jsonArray = jsonObject1.getJSONArray("adUrl");
                //  GeneralPref.setImageUrl(jsonObject1.getString("image_url"));

                GeneralPref.setName(jsonObject1.getString("cust_name"));
                setName.setName(jsonObject1.getString("cust_name"));
                Log.d(TAG, "cust_name: " + GeneralPref.getName());

                if (jsonObject1.getString("resp_code").equals("00")) {
                    GeneralPref.setCashback(jsonObject1.optString("rebate"));
                    GeneralPref.setKYC(jsonObject1.getString("kyc_status"));
                    GeneralPref.setPrefCount(jsonObject1.getInt("atm_count"), jsonObject1.getString("kyc_status"), jsonObject1.getInt("newChatMessageCount"), jsonObject1.getInt("number_of_restro"), jsonObject1.getInt("taxiCount"), jsonObject1.getBoolean("allow_bank_deposit"));
                    GeneralPref.setRestroCount(jsonObject1.getInt("number_of_restro"));
                    Log.d("dinesh", "" + GeneralPref.getRestroCount());
                    //   int hours = calendar.get(Calendar.MINUTE) + 5;
                    // GeneralPref.settime(hours);
                }

                bottomNavigation();

                if (GeneralPref.getNoofRestro() > 2) {
                }
              /*
              if (GeneralPref.getKYC().equals("NA") ||GeneralPref.getKYC().equals("R")) {
                    kycButton.setVisibility(View.VISIBLE);
                }
                */
                Log.d("dinesh", "check2" + status);
                ArrayList<String> promotionbanners = new ArrayList();
                JSONArray bannerAdds = new JSONArray();

                for (int i = 0; i < jsonArray.length(); i++) {
                    GeneralPref.setBannerArray(null);
                    promotionbanners.add(jsonArray.getString(i));
                    adds.addSlider(new TextSliderView(getContext()).image(promotionbanners.get(i)));
                    bannerAdds.put(jsonArray.getString(i));
                }
                bundle.putStringArrayList("bundle", promotionbanners);
                GeneralPref.setBannerArray(bannerAdds.toString());

                adds.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
                //  adds.startAutoCycle();
                adds.setCustomAnimation(new ChildCarouselAnimation());
                adds.getParent().requestDisallowInterceptTouchEvent(false);
                GetdealData data = new GetdealData();
                String rebate = jsonObject1.getString("rebate");
                GeneralPref.setRebate(rebate);
/*
                data.setImgUrl1(jsonArray.getString(0));
                data.setImgUrl2(jsonArray.getString(1));
                data.setImgUrl3(jsonArray.getString(2));
*/
                url.add(data);

               /* if (jsonArray.getString(0) != null) {
                    mSliderLayout.setVisibility(View.VISIBLE);
                    DefaultSliderView textViewSlide = new DefaultSliderView(getActivity());

                    textViewSlide
                            .image(jsonArray.getString(0))
                            .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                            .error(R.drawable.image_error)
                            .empty(R.drawable.image_error);
                    mSliderLayout.addSlider(textViewSlide);
                }

                if (jsonArray.getString(1) != null) {
                    DefaultSliderView textViewSlide = new DefaultSliderView(getActivity());

                    textViewSlide
                            .image(jsonArray.getString(1))
                            .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                            .error(R.drawable.image_error)
                            .empty(R.drawable.image_error);
                    mSliderLayout.addSlider(textViewSlide);
                }

                if (jsonArray.getString(2) != null) {
                    DefaultSliderView textViewSlide = new DefaultSliderView(getActivity());

                    textViewSlide
                            .image(jsonArray.getString(2))
                            .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                            .error(R.drawable.image_error)
                            .empty(R.drawable.image_error);`
                    mSliderLayout.addSlider(textViewSlide);
                }
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
//                mSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                mSliderLayout.setDuration(4000);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
            new CheckStatus().execute();
        }
    }

    public class CheckStatus extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String status_check;
            try {
//              String customerTLV = Utils.getDukptCustomerId(cust_id);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("app_id", app_id);
                jsonObject.put("cust_id", cust_id);
                jsonObject.put("lat", LatLngPref.getLat());
                jsonObject.put("lng", LatLngPref.getLng());
                Log.d(TAG, "check ip : " + jsonObject);
                status_check = new NetworkAPI().sendHTTPData(Constants.CHECK_MY_STATUS, jsonObject);
                Log.d(TAG, "check ip : " + jsonObject);
                return status_check;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //  progressBar.setVisibility(View.VISIBLE);
            rotateImage(360, 100, refresh, 500, false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //progressBar.setVisibility(View.GONE);
            rotateImage(360, 0, refresh, 500, true);
            try {
                JSONObject jsonObject = new JSONObject(s);
                Log.d(TAG, "jsonObject : " + jsonObject);
                JSONObject checkmystatusResult = jsonObject.getJSONObject("checkmystatusResult");
                String status = checkmystatusResult.getString("status");
                if (status.equals("00")) {
                    String res_code = checkmystatusResult.getString("resp_code");
                    if (res_code.equals("00")) {
                        balance.setText(checkmystatusResult.getString("avail_bal"));
                        BalancePref.setWalletAmount(checkmystatusResult.getString("avail_bal"));
                        BalancePref.setAvailableAmount(new Decimalformate().decimalFormate(checkmystatusResult.getString("avail_bal")));
                        BalancePref.setDepositeProgress("0.0");
                        BalancePref.setHoldAmount("0.0");
                        BalancePref.setBonusAmount(checkmystatusResult.getString("bonus"));
                        BalancePref.setRewardPoint(checkmystatusResult.getString("reward_point"));
                        // paisaTextView.setText(BalancePref.getRewardPoint());
//                        bonusTextView.setText("NPR "+BalancePref.getBonusAmount());
                        // bonusTextView.setText(BalancePref.getBonusAmount());

                    } else if (res_code.equals("NB")) {
                        toolbar.setVisibility(View.GONE);
                        duoLock.lockNavigation(2);
                        try {
                            Negativebalancefragment negativebalancefragment = new Negativebalancefragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.container, negativebalancefragment)
                                    .commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (res_code.equals("UA")) {
                        toolbar.setVisibility(View.GONE);
                        duoLock.lockNavigation(2);
                        getActivity().requestWindowFeature(Window.FEATURE_NO_TITLE);

                        try {
                            Unauthorizeuser blockedfragment = new Unauthorizeuser();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.container, blockedfragment)
                                    .commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("catch", "" + e);
                        }

                    } else if (res_code.equals("S")) {

                        duoLock.lockNavigation(2);
                        toolbar.setVisibility(View.GONE);
                        try {

                            SuspendedUserFragment blockedfragment = new SuspendedUserFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.container, blockedfragment)
                                    .commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else if (status.equalsIgnoreCase("S")) {
                    duoLock.lockNavigation(2);
                    toolbar.setVisibility(View.GONE);

                    try {

                        SuspendedUserFragment blockedfragment = new SuspendedUserFragment();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.container, blockedfragment)
                                .commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (status.equals("UA")) {

                    duoLock.lockNavigation(2);
                    toolbar.setVisibility(View.GONE);
                    try {
                        Unauthorizeuser blockedfragment = new Unauthorizeuser();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.container, blockedfragment)
                                .commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void showTutor(final View view, ImageView refresh) {

        new TapTargetSequence(getActivity()).targets(
                TapTarget.forView(view.findViewById(R.id.pay_btnReal), "Scan & Pay", "You can pay through QRCode.")
                        .dimColor(R.color.alpha_dim)
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.white)
                        .textTypeface(Typeface.SANS_SERIF)
                        .transparentTarget(true)
                        .targetRadius(70)
                        .cancelable(false),
                TapTarget.forView(refresh, "Refresh", "Refresh your wallet balance.")
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.white)
                        .cancelable(false),
                TapTarget.forView(view.findViewById(R.id.load_balance_imageview), "Load Balance", "Load Balance via eBanking, merchant & card.")
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.white)
                        .transparentTarget(true)
                        .cancelable(false),
                TapTarget.forView(view.findViewById(R.id.send_money_imageview), "Send Money", "Send Money to your friends.")
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.white)
                        .transparentTarget(true)
                        .cancelable(false),
                TapTarget.forView(view.findViewById(R.id.withdraw_imageview), "WithDraw", "Withdraw via QPay wallet or Qpay Merchant")
                        .descriptionTextSize(14)
                        .descriptionTextColor(R.color.white)
                        .transparentTarget(true)
                        .cancelable(false)
        ).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                Toast.makeText(getActivity(), "Tutor Completed", Toast.LENGTH_SHORT).show();
                GeneralPref.setTutorStatus(false);
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private void showTutorForUpi(final View view, ImageView refresh) {

        new TapTargetSequence(getActivity()).targets(
                TapTarget.forView(view.findViewById(R.id.upi_icon), "", "QPay Wallet can be used at any UnionPay QR Code Merchant locations.")
                        .dimColor(R.color.alpha_dim)
                        .descriptionTextSize(16)
                        .descriptionTextColor(R.color.white)
                        .textTypeface(Typeface.SANS_SERIF)
                        .transparentTarget(true)
                        .targetRadius(30)
                        .cancelable(true)
        ).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();

    }

    private void showTutorForQpay(final View view, ImageView refresh) {

        new TapTargetSequence(getActivity()).targets(
                TapTarget.forView(view.findViewById(R.id.qpay_icon), "", " Get NPR 200.00 discount at our Participating Restaurants.")
                        .dimColor(R.color.alpha_dim)
                        .descriptionTextSize(16)
                        .descriptionTextColor(R.color.white)
                        .textTypeface(Typeface.SANS_SERIF)
                        .transparentTarget(true)
                        .targetRadius(30)
                        .cancelable(true)
        ).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();

    }

    public class GETQRCode extends AsyncTask<String, String, String> {
        LinearLayout progressLayout;
        ImageView myImage;


        public GETQRCode(LinearLayout progressLayout, ImageView myImage) {
            this.progressLayout = progressLayout;
            this.myImage = myImage;
        }

        @Override
        protected String doInBackground(final String... uri) {
            String balance_avail = null;
            JSONObject jsonObject_balance = new JSONObject();
            NetworkAPI networkAPI = new NetworkAPI();
            try {
//                String customerTLV = Utils.getDukptCustomerId(cust_id);
                jsonObject_balance.put("id", cust_id);
                jsonObject_balance.put("appId", app_id);
                jsonObject_balance.put("lat", ((CustomerApplication) getActivity().getApplication()).getLat());
                jsonObject_balance.put("lng", ((CustomerApplication) getActivity().getApplication()).getLng());
                Log.d("dinesh", jsonObject_balance.toString());
                return networkAPI.sendHTTPData(Constants.QRGENERATION, jsonObject_balance);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myImage.setVisibility(View.GONE);
            progressLayout.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d(Utility.TAG, "post service : " + s);
                progressLayout.setVisibility(View.GONE);
                myImage.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.getBoolean("success")) {
                    JSONArray data = jsonObject.getJSONArray("data");
                    JSONObject object = data.getJSONObject(0);
                    String emvCpqrcPayload = object.getString("emvCpqrcPayload");
                    create(emvCpqrcPayload, myImage);
                }
            } catch (Exception e) {
                progressLayout.setVisibility(View.GONE);

                e.printStackTrace();
            }
        }
    }

    public void create(String cust_id, ImageView myImage) {
//        String qrInputTextGeneral = sctDatabaseHelper.getEncryptedCustomerId();
        String qrInputText = null;
        String encryptedMsg = cust_id;
        qrInputText = encryptedMsg;
        if (qrInputText != null) {
            WindowManager manager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                if (bitmap != null) {
                    myImage.setImageBitmap(bitmap);
                } else {
                    this.create(cust_id, myImage);
                }


            } catch (WriterException e) {
                e.printStackTrace();
                try {
                    this.create(cust_id, myImage);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        }
    }

    public interface SetName {
        void setName(String name);
    }

}
