package net.qpaysolutions.QPay.dashboard;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;

class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.AdapterViewHolder> {
    ArrayList<String> arrayList;

    public BannerAdapter(ArrayList arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyler_fragment_promotion, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder holder, int position) {
        Picasso.get().load(arrayList.get(position)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class AdapterViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public AdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
        }
    }


}
