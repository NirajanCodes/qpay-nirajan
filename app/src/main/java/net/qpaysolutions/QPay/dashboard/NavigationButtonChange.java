package net.qpaysolutions.QPay.dashboard;

import android.widget.ImageView;

import net.qpaysolutions.QPay.duonavigation.views.DuoDrawerLayout;

public interface NavigationButtonChange {
    void onButtonClick(ImageView imageView);
}
