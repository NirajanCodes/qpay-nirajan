package net.qpaysolutions.QPay.dashboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.Tricks.InfiniteViewPager;

import net.qpaysolutions.QPay.R;

import java.util.Timer;
import java.util.TimerTask;

public class SliderAnimated extends SliderLayout {

    private InfiniteViewPager mViewPager;

    private boolean mCycling;
    private Timer mCycleTimer;

    private boolean mAutoRecover;
    private boolean mAutoCycle;
    private TimerTask mCycleTask;

    private Timer mResumingTimer;
    private TimerTask mResumingTask;
    private SliderCallBack sliderCallBack;

    public SliderAnimated(Context context) {
        super(context);
    }

    @SuppressLint("ClickableViewAccessibility")
    public SliderAnimated(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mViewPager = (InfiniteViewPager) this.findViewById(R.id.glide_slider_viewpager);
    }

    public SliderAnimated(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public void sliderGet(SliderCallBack sliderCallBack) {
        this.sliderCallBack = sliderCallBack;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            return false;
        } else if (action == MotionEvent.ACTION_UP) {
            sliderCallBack.sliderCallBack();
            return true;
        } else {
            return false;
        }
    }

    private void recoverCycle() {
        if (this.mAutoRecover && this.mAutoCycle) {
            if (!this.mCycling) {
                if (this.mResumingTask != null && this.mResumingTimer != null) {
                    this.mResumingTimer.cancel();
                    this.mResumingTask.cancel();
                }

                this.mResumingTimer = new Timer();
                this.mResumingTask = new TimerTask() {
                    public void run() {
                        SliderAnimated.this.startAutoCycle();
                    }
                };
                this.mResumingTimer.schedule(this.mResumingTask, 6000L);
            }

        }
    }

    public interface SliderCallBack {
        void sliderCallBack();
    }
}