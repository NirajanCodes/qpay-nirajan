package net.qpaysolutions.QPay.dashboard;

public class ProductModel {

    String Id,name;
    int res;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    String icon;

    public void setImage(int res){
        this.res=res;
    }

    public int getImage(){
        return res;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public ProductModel() {
    }

    public ProductModel(String id, String icon, String name,int res) {
        Id = id;
        this.name = name;
        this.icon = icon;
        this.res=res;
    }
}
