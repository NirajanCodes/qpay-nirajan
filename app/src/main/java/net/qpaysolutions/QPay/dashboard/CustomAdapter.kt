package net.qpaysolutions.QPay.dashboard

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import net.qpaysolutions.QPay.Billpayment.BillPayActivity
import net.qpaysolutions.QPay.Billpayment.EntertaintActivity
import net.qpaysolutions.QPay.Billpayment.KhanepaniActivity
import net.qpaysolutions.QPay.Billpayment.Internetbillpay.InternetPayActivity
import net.qpaysolutions.QPay.Billpayment.phonepay.PhonePayActivity
import net.qpaysolutions.QPay.Bus.BusMainActivity
import net.qpaysolutions.QPay.Deal.AllActivityNew
import net.qpaysolutions.QPay.Flights.flightfragment.FlightMainActivity
import net.qpaysolutions.QPay.R
import java.util.*

/**
 * Created by deadlydragger on 3/8/17.
 */

class CustomAdapter (internal var context: Context, internal var categoryModels: ArrayList<ProductModel>, internal var flag: Int) : RecyclerView.Adapter<CustomAdapter.PayBillViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PayBillViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_paybill, parent, false)
        return PayBillViewHolder(view)
    }

    override fun onBindViewHolder(holder: PayBillViewHolder, position: Int) {
        holder.fontIcon.setImageResource(categoryModels[position].image)
        holder.name.text = categoryModels[position].name
        when {
            categoryModels[position].id.equals("0") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, InternetPayActivity::class.java)
                i.putExtra("tv", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("1") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, PhonePayActivity::class.java)
                i.putExtra("Phone", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("2") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, InternetPayActivity::class.java)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("3") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, BillPayActivity::class.java)
                i.putExtra("id", "nea")
                context.startActivity(i)
            })
            categoryModels[position].id.equals("4") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, KhanepaniActivity::class.java)
                i.putExtra("tv", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("5") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, FlightMainActivity::class.java)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("6") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, BusMainActivity::class.java)
                i.putExtra("isFromAdapter", true)
                context.startActivity(i)
            })
        /*    categoryModels[position].id.equals("7") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, MovieActivity::class.java)
                context.startActivity(i)
            })*/
            categoryModels[position].id.equals("8") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, EntertaintActivity::class.java)
                i.putExtra("isFromAdapter", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("9") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, EntertaintActivity::class.java)
                i.putExtra("isFromEvents", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("10") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, EntertaintActivity::class.java)
                i.putExtra("isFromEvents", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("11") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, EntertaintActivity::class.java)
                i.putExtra("isFromEvents", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("12") -> holder.main?.setOnClickListener(View.OnClickListener {
                val i = Intent(context, EntertaintActivity::class.java)
                i.putExtra("isFromEvents", true)
                context.startActivity(i)
            })
            categoryModels[position].id.equals("13") -> holder.main?.setOnClickListener(View.OnClickListener {
                val intent = Intent(context, AllActivityNew::class.java)
//                intent.putExtra("mylist", getdealDataListFood)
//                intent.putExtra("feature", getdealDatasFeatured)
                intent.putExtra("category_name", "QPay Restaurants")
                context.startActivity(intent)
            })


        }
    }

    override fun getItemCount(): Int {
        return categoryModels.size
    }

    class PayBillViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var fontIcon: ImageView
        internal var name: TextView
        internal var main: LinearLayout? = null

        init {
            fontIcon = itemView.findViewById<View>(R.id.icon) as ImageView
            name = itemView.findViewById<View>(R.id.name) as TextView
            name.maxLines=1
            main = itemView.findViewById(R.id.main)
        }
    }
}


