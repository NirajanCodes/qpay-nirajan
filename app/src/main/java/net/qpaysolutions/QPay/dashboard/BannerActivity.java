package net.qpaysolutions.QPay.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.qpaysolutions.QPay.R;

import java.util.ArrayList;
import java.util.Objects;

public class BannerActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_promotion);
        recyclerView = findViewById(R.id.quick_recycler_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        TextView toolbarText = toolbar.findViewById(R.id.title);
        toolbarText.setText("Promotions");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Bundle bundle = getIntent().getBundleExtra("bundle");
        ArrayList arrayList = bundle.getIntegerArrayList("bundle");
        BannerAdapter bannerAdpater = new BannerAdapter(arrayList);
        recyclerView.setAdapter(bannerAdpater);
        recyclerView.setLayoutManager(new LinearLayoutManager((this)));
    }

    @Override
    public void onBackPressed() {
        Log.d("dinesh", "check");
        super.onBackPressed();
       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
